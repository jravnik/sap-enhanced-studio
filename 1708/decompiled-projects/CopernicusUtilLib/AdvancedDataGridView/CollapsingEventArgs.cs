﻿// Decompiled with JetBrains decompiler
// Type: AdvancedDataGridView.CollapsingEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;

namespace AdvancedDataGridView
{
  public class CollapsingEventArgs : CancelEventArgs
  {
    private TreeGridNode _node;

    public TreeGridNode Node
    {
      get
      {
        return this._node;
      }
    }

    private CollapsingEventArgs()
    {
    }

    public CollapsingEventArgs(TreeGridNode node)
    {
      this._node = node;
    }
  }
}
