﻿// Decompiled with JetBrains decompiler
// Type: AdvancedDataGridView.TreeGridColumn
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Drawing;
using System.Windows.Forms;

namespace AdvancedDataGridView
{
  public class TreeGridColumn : DataGridViewTextBoxColumn
  {
    internal Image _defaultNodeImage;

    public Image DefaultNodeImage
    {
      get
      {
        return this._defaultNodeImage;
      }
      set
      {
        this._defaultNodeImage = value;
      }
    }

    public TreeGridColumn()
    {
      this.CellTemplate = (DataGridViewCell) new TreeGridCell();
    }

    public override object Clone()
    {
      TreeGridColumn treeGridColumn = (TreeGridColumn) base.Clone();
      treeGridColumn._defaultNodeImage = this._defaultNodeImage;
      return (object) treeGridColumn;
    }
  }
}
