﻿// Decompiled with JetBrains decompiler
// Type: AdvancedDataGridView.CollapsingEventHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace AdvancedDataGridView
{
  public delegate void CollapsingEventHandler(object sender, CollapsingEventArgs e);
}
