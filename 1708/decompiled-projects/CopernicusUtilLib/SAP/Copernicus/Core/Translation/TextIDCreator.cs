﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Translation.TextIDCreator
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Util;

namespace SAP.Copernicus.Core.Translation
{
  public class TextIDCreator
  {
    public string GetTextID_XBO_ExtensionField(TextType textType, string fieldName)
    {
      return RepositoryUtil.GetProxyNameForName((string) null, (string) null, "XF" + this.getXrepTextType(textType) + "_" + fieldName, 32);
    }

    public string GetTextID_BO_Element(string nameSpace, TextType textType, string boName, string nodeName, string elementName, out string mdrsTextName)
    {
      mdrsTextName = RepositoryUtil.GetProxyNameForName(ProjectUtil.GetRuntimeNamespacePrefixForPartnerNamespace(nameSpace), (string) null, boName + (object) '+' + nodeName + (object) '+' + elementName, 26);
      return "E" + this.getXrepTextType(textType) + "_" + mdrsTextName;
    }

    public string getXrepTextType(TextType texttype)
    {
      string str = (string) null;
      switch (texttype)
      {
        case TextType.Label:
          str = "XFLD";
          break;
        case TextType.Tooltip:
          str = "XTOL";
          break;
        case TextType.Message:
          str = "XMSG";
          break;
      }
      return str;
    }
  }
}
