﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.ExternalApplication
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.GUI;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Util
{
  public class ExternalApplication
  {
    public static bool LaunchExternalBrowser(string url)
    {
      try
      {
        Process.Start(url);
        return true;
      }
      catch (FileNotFoundException ex)
      {
        int num = (int) CopernicusMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        return false;
      }
    }

    public static bool LaunchAdobeDesigner(string fullyQualifiedFormName)
    {
      try
      {
        string str = "C:\\Program Files\\Adobe\\Designer 8.0\\FormDesigner.exe";
        if (File.Exists(str))
          Process.Start(str, fullyQualifiedFormName);
        else
          Process.Start(fullyQualifiedFormName);
        return true;
      }
      catch (FileNotFoundException ex)
      {
        int num = (int) CopernicusMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        return false;
      }
    }
  }
}
