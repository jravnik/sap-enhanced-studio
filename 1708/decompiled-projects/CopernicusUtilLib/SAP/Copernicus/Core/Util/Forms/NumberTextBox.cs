﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.Forms.NumberTextBox
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Util.Forms
{
  internal class NumberTextBox : TextBox
  {
    public NumberTextBox()
    {
      this.CausesValidation = true;
      this.Validating += new CancelEventHandler(this.TextBox_Validation);
    }

    private void TextBox_Validation(object sender, CancelEventArgs ce)
    {
      try
      {
        int.Parse(this.Text);
      }
      catch (Exception ex)
      {
        ce.Cancel = true;
        int num = (int) MessageBox.Show(Resource.NumberTextBox_Validation_Error, Resource.NumberTextBox_Validation_Caption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
      }
    }
  }
}
