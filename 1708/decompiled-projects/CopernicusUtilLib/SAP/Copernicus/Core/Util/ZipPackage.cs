﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.ZipPackage
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.IO;
using System.IO.Packaging;

namespace SAP.Copernicus.Core.Util
{
  public class ZipPackage : IDisposable
  {
    private readonly Package package;

    public ZipPackage(Stream stream)
    {
      if (stream == null)
        throw new ArgumentNullException("stream");
      this.package = Package.Open(stream, FileMode.Create);
    }

    public void AddContent(string resourceName, string content, string mediaTypeName = "text/xml")
    {
      using (StreamWriter streamWriter = new StreamWriter(this.package.CreatePart(PackUriHelper.CreatePartUri(new Uri(RepositoryUtil.ShortenFileName(RepositoryUtil.EncodeName(resourceName)), UriKind.Relative)), mediaTypeName).GetStream()))
        streamWriter.Write(content);
    }

    public void Dispose()
    {
      if (this.package == null)
        return;
      foreach (PackagePart part in this.package.GetParts())
        part.GetStream().Dispose();
      ((IDisposable) this.package).Dispose();
    }
  }
}
