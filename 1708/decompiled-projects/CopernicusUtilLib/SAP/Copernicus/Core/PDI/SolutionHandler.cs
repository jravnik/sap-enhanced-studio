﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.PDI.SolutionHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.PDI
{
  public class SolutionHandler : JSONHandler
  {
    public const string SolutionSubType_OneOff = "1O";
    public const string SolutionSubType_Template = "TS";
    public const string SolutionSubType_GlobalScalable = "GS";
    public const string SolutionSubType_LocalScalable = "LS";

    public PDI_S_PRODUCT_V_AND_COMPONENTS GetSolutionVersion(string solutionName, string solutionVersion, out string overallStatus, out string certificationStatus, out string solutionDescription, out string detailedDescription, out string keywords)
    {
      overallStatus = "";
      solutionDescription = "";
      detailedDescription = "";
      keywords = "";
      certificationStatus = "";
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_GET pdiLmProductGet = new PDI_LM_PRODUCT_GET();
      pdiLmProductGet.Importing = new PDI_LM_PRODUCT_GET.ImportingType();
      pdiLmProductGet.Importing.IV_PRODUCT_NAME = solutionName;
      pdiLmProductGet.Importing.IV_MODE = "D";
      if (SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.Scalable)
        pdiLmProductGet.Importing.IV_SOLUTION_TYPE = "1";
      else if (SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.OneOff || SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.miltiCustomer)
        pdiLmProductGet.Importing.IV_SOLUTION_TYPE = "2";
      jsonClient.callFunctionModule((SAPFunctionModule) pdiLmProductGet, false, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) pdiLmProductGet, false, false);
      PDI_S_PRODUCT_V_AND_COMPONENTS productVAndComponents = (PDI_S_PRODUCT_V_AND_COMPONENTS) null;
      if (pdiLmProductGet.Exporting != null)
      {
        PDI_S_PRODUCT_VERS_AND_COMPS esProduct = pdiLmProductGet.Exporting.ES_PRODUCT;
        productVAndComponents = ((IEnumerable<PDI_S_PRODUCT_V_AND_COMPONENTS>) esProduct.VERSIONS).FirstOrDefault<PDI_S_PRODUCT_V_AND_COMPONENTS>((Func<PDI_S_PRODUCT_V_AND_COMPONENTS, bool>) (vers => vers.PRODUCT_VERSION.VERSION == solutionVersion)) ?? ((IEnumerable<PDI_S_PRODUCT_V_AND_COMPONENTS>) esProduct.VERSIONS).FirstOrDefault<PDI_S_PRODUCT_V_AND_COMPONENTS>();
        if (productVAndComponents != null)
        {
          overallStatus = productVAndComponents.PV_OVERALL_STATUS;
          certificationStatus = productVAndComponents.PV_CERTIFICATION_STATUS;
          solutionDescription = esProduct.VERSIONS[0].PRODUCT_VERSION_TEXTS[0].DDTEXT;
          detailedDescription = esProduct.VERSIONS[0].LONG_TEXT;
          keywords = esProduct.VERSIONS[0].KEYWORD;
          if (productVAndComponents.IS_PROD_FIX_ASSIGNED != null && productVAndComponents.IS_PROD_FIX_ASSIGNED.Equals("X"))
            this.reportServerSideProtocolException((SAPFunctionModule) pdiLmProductGet, false, true);
        }
      }
      if (productVAndComponents == null)
      {
        string message = string.Format("Unable to get solution {0} {1}", (object) solutionName, (object) solutionVersion);
        Trace.TraceError(message);
        ProtocolException protocolException = new ProtocolException(ProtocolException.ErrorArea.SERVER, message);
        protocolException.showPopup();
        throw protocolException;
      }
      return productVAndComponents;
    }

    public PDI_S_PRODUCT_V_AND_COMPONENTS GetSolutionVersion(string solutionName, string solutionVersion, out string overallStatus, out string certificationStatus, out string solutionDescription)
    {
      string detailedDescription;
      string keywords;
      return this.GetSolutionVersion(solutionName, solutionVersion, out overallStatus, out certificationStatus, out solutionDescription, out detailedDescription, out keywords);
    }

    public virtual bool PDI_1O_PROD_FIX_CORRECTION(string IV_SOLUTION_NAME, string IV_CORRECTION_CREATE)
    {
      PDI_1O_PROD_FIX_CORRECTION prodFixCorrection = new PDI_1O_PROD_FIX_CORRECTION();
      bool flag = false;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        prodFixCorrection.Importing = new PDI_1O_PROD_FIX_CORRECTION.ImportingType();
        prodFixCorrection.Importing.IV_SOLUTION_NAME = IV_SOLUTION_NAME;
        prodFixCorrection.Importing.IV_CORRECTION_CREATE = IV_CORRECTION_CREATE;
        jsonClient.callFunctionModule((SAPFunctionModule) prodFixCorrection, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) prodFixCorrection, true, false);
        if (prodFixCorrection.Exporting != null && prodFixCorrection.Exporting.EV_SUCCESS == "X")
          flag = true;
        return flag;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public bool isKeyUserSolution(string solutionName)
    {
      bool flag = false;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_1O_KUT_SOLUTION_CHECK kutSolutionCheck = new PDI_1O_KUT_SOLUTION_CHECK();
      kutSolutionCheck.Importing = new PDI_1O_KUT_SOLUTION_CHECK.ImportingType();
      kutSolutionCheck.Importing.IV_SOLUTION_NAME = solutionName;
      if (SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.OneOff)
      {
        jsonClient.callFunctionModule((SAPFunctionModule) kutSolutionCheck, false, false, false);
        if (kutSolutionCheck.Exporting != null && kutSolutionCheck.Exporting.EV_KUT_SOLUTION == "X")
          flag = true;
      }
      return flag;
    }

    public void GetSolutionStatus(string solutionName, out string solutionStatus, out string solutionCertificationStatus, out string solutionEditStatus)
    {
      solutionStatus = "";
      solutionCertificationStatus = "";
      solutionEditStatus = "";
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_UI_GET_STATUS productUiGetStatus = new PDI_LM_PRODUCT_UI_GET_STATUS();
      productUiGetStatus.Importing = new PDI_LM_PRODUCT_UI_GET_STATUS.ImportingType();
      productUiGetStatus.Importing.IV_PRODUCT_NAME = solutionName;
      jsonClient.callFunctionModule((SAPFunctionModule) productUiGetStatus, false, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) productUiGetStatus, false, false);
      if (productUiGetStatus.Exporting == null)
        return;
      solutionStatus = productUiGetStatus.Exporting.EV_OVERALL_STATUS;
      solutionCertificationStatus = productUiGetStatus.Exporting.EV_CERTIFICATION_STATUS;
      solutionEditStatus = productUiGetStatus.Exporting.EV_EDIT_STATUS;
    }

    public PDI_S_PRODUCT_VERS_AND_COMPS[] GetSolutionsFor(string userName)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCTS_GET pdiLmProductsGet = new PDI_LM_PRODUCTS_GET();
      pdiLmProductsGet.Importing = new PDI_LM_PRODUCTS_GET.ImportingType();
      pdiLmProductsGet.Importing.IV_USER = userName.ToUpper();
      pdiLmProductsGet.Importing.IV_GET = "X";
      if (SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.Scalable)
        pdiLmProductsGet.Importing.IV_SOLUTION_TYPE = "1";
      else if (SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.OneOff)
        pdiLmProductsGet.Importing.IV_SOLUTION_TYPE = "2";
      else if (SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.miltiCustomer)
      {
        pdiLmProductsGet.Importing.IV_SOLUTION_TYPE = "3";
        pdiLmProductsGet.Importing.IV_PRODUCT_NAME = SAP.Copernicus.Core.Protocol.Connection.getInstance().ConnectedSystem.MCSKey;
      }
      jsonClient.callFunctionModule((SAPFunctionModule) pdiLmProductsGet, false, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) pdiLmProductsGet, true, false);
      if (pdiLmProductsGet.Exporting == null)
        return new PDI_S_PRODUCT_VERS_AND_COMPS[0];
      List<PDI_S_PRODUCT_VERS_AND_COMPS> list = ((IEnumerable<PDI_S_PRODUCT_VERS_AND_COMPS>) pdiLmProductsGet.Exporting.ET_PRODUCTS).ToList<PDI_S_PRODUCT_VERS_AND_COMPS>();
      list.RemoveAll((Predicate<PDI_S_PRODUCT_VERS_AND_COMPS>) (e => e.PRODUCT.IS_DELETED == "X"));
      return list.ToArray();
    }

    private bool monitorBatchJob(string applogID, string jobCNT, string jobName, bool showPopup = true, IOutputWindowPane outputPane = null)
    {
      bool flag1 = false;
      bool flag2 = true;
      BatchGetStatusHandler getStatusHandler = new BatchGetStatusHandler();
      PDI_LM_BATCH_GET_STATUS.ExportingType exportingType = (PDI_LM_BATCH_GET_STATUS.ExportingType) null;
      PDI_LM_T_MSG_LIST[] msgs = (PDI_LM_T_MSG_LIST[]) null;
      int millisecondsTimeout = 750;
      while (!flag1)
      {
        Cursor.Current = Cursors.WaitCursor;
        Thread.Sleep(millisecondsTimeout);
        if (millisecondsTimeout < 6000)
          millisecondsTimeout *= 2;
        exportingType = getStatusHandler.getBatchStatus(jobName, jobCNT, applogID, out msgs);
        if (exportingType.EV_JOB_STATE.Equals("05") || exportingType.EV_JOB_STATE.Equals("06") || (exportingType.EV_JOB_STATE.Equals("07") || exportingType.EV_JOB_STATE.Equals("09")))
          flag1 = true;
        foreach (PDI_RI_S_MESSAGE pdiRiSMessage in exportingType.ET_MESSAGES)
        {
          outputPane.Writeln(pdiRiSMessage.TEXT);
          CopernicusStatusBar.Instance.ShowMessage(pdiRiSMessage.TEXT);
        }
      }
      ErrorSink.Instance.AddMessages(msgs, Resource.MessagePrefixActivate, Origin.GenericSolutionAction);
      Cursor.Current = Cursors.Default;
      if (exportingType.ET_MESSAGES != null && exportingType.ET_MESSAGES.Length > 0)
      {
        List<SAP.Copernicus.Core.Protocol.JSON.Message> messageList = new List<SAP.Copernicus.Core.Protocol.JSON.Message>();
        foreach (PDI_RI_S_MESSAGE pdiRiSMessage in exportingType.ET_MESSAGES)
        {
          if (pdiRiSMessage.SEVERITY == "E" || pdiRiSMessage.SEVERITY == "A")
          {
            flag2 = false;
            ErrorSink.Instance.AddTask(pdiRiSMessage.TEXT, Origin.Unknown, Severity.Error, (EventHandler) null);
          }
          SAP.Copernicus.Core.Protocol.JSON.Message.MessageSeverity severity = SAP.Copernicus.Core.Protocol.JSON.Message.MessageSeverity.FromABAPCode(pdiRiSMessage.SEVERITY);
          messageList.Add(new SAP.Copernicus.Core.Protocol.JSON.Message(severity, pdiRiSMessage.TEXT));
        }
      }
      if (flag2 && exportingType.EV_PROC_SUCC == "X")
        return exportingType.EV_SUCCESS == "X";
      return false;
    }

    public bool MigrateSolution(string solutionName, IOutputWindowPane outputPane)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_MIGRATE_BATCH productMigrateBatch = new PDI_LM_PRODUCT_MIGRATE_BATCH();
      productMigrateBatch.Importing = new PDI_LM_PRODUCT_MIGRATE_BATCH.ImportingType();
      productMigrateBatch.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      productMigrateBatch.Importing.IV_SOLUTION = solutionName;
      productMigrateBatch.Importing.IV_MODE = "D";
      jsonClient.callFunctionModule((SAPFunctionModule) productMigrateBatch, true, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) productMigrateBatch, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      if (productMigrateBatch.Exporting.ET_MSG_LIST != null && productMigrateBatch.Exporting.ET_MSG_LIST.Length > 0)
        this.DisplayMessages(productMigrateBatch.Exporting.ET_MSG_LIST);
      if (string.IsNullOrEmpty(productMigrateBatch.Exporting.EV_JOBCNT))
        return productMigrateBatch.Exporting.EV_SUCCESS == "X";
      if (productMigrateBatch.Exporting.EV_SUCCESS == "X")
        return this.monitorBatchJob(productMigrateBatch.Exporting.EV_APPLOG_ID, productMigrateBatch.Exporting.EV_JOBCNT, productMigrateBatch.Exporting.EV_JOBNAME, false, outputPane);
      return false;
    }

    public PDI_S_PRODUCT_VERS_AND_COMPS CreateOneOffSolution(string Name, string Description, string DetailedDescription, string Keywords, string SolutionType, string DeploymentUnit, bool IsOneOffSolution, string DevPartner, string ContactPerson, string EMail)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_1O_PRODUCT_CREATE pdi1OProductCreate = new PDI_1O_PRODUCT_CREATE();
      pdi1OProductCreate.Importing = new PDI_1O_PRODUCT_CREATE.ImportingType();
      pdi1OProductCreate.Importing.IV_PRODUCT_NAME = Name;
      pdi1OProductCreate.Importing.IV_PRODUCT_DESCRIPTION = Description;
      pdi1OProductCreate.Importing.IV_LONG_TEXT = DetailedDescription;
      pdi1OProductCreate.Importing.IV_KEYWORDS = Keywords;
      pdi1OProductCreate.Importing.IV_PRODUCT_TYPE = SolutionType;
      pdi1OProductCreate.Importing.IV_DEPLOYMENT_UNIT = DeploymentUnit;
      pdi1OProductCreate.Importing.IV_DEPLOYMENT_MODE = "D";
      pdi1OProductCreate.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      pdi1OProductCreate.Importing.IV_DEV_PARTNER = DevPartner;
      pdi1OProductCreate.Importing.IV_CONTACT_PERSON = ContactPerson;
      pdi1OProductCreate.Importing.IV_EMAIL = EMail;
      if (IsOneOffSolution)
        pdi1OProductCreate.Importing.IV_CREATE_MODE = "G";
      else
        pdi1OProductCreate.Importing.IV_CREATE_MODE = "L";
      jsonClient.callFunctionModule((SAPFunctionModule) pdi1OProductCreate, true, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) pdi1OProductCreate, false, false);
      if (pdi1OProductCreate.Exporting.EV_SUCCESS == "X" && pdi1OProductCreate.Exporting.ET_MESSAGES != null && pdi1OProductCreate.Exporting.ET_MESSAGES.Length > 0)
        this.DisplayMessages(pdi1OProductCreate.Exporting.ET_MESSAGES);
      return pdi1OProductCreate.Exporting.ES_PRODUCT;
    }

    public PDI_S_PRODUCT_VERS_AND_COMPS UpdateOneOffSolution(string Name, string DevPartner, string ContactPerson, string EMail, string DetailedDescription, string Keywords)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_1O_PRODUCT_MODIFY pdi1OProductModify = new PDI_1O_PRODUCT_MODIFY();
      pdi1OProductModify.Importing = new PDI_1O_PRODUCT_MODIFY.ImportingType();
      pdi1OProductModify.Importing.IV_PRODUCT_NAME = Name;
      pdi1OProductModify.Importing.IV_DEV_PARTNER = DevPartner;
      pdi1OProductModify.Importing.IV_CONTACT_PERSON = ContactPerson;
      pdi1OProductModify.Importing.IV_EMAIL = EMail;
      pdi1OProductModify.Importing.IV_LONG_TEXT = DetailedDescription;
      pdi1OProductModify.Importing.IV_KEYWORDS = Keywords;
      jsonClient.callFunctionModule((SAPFunctionModule) pdi1OProductModify, true, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) pdi1OProductModify, false, false);
      if (pdi1OProductModify.Exporting.EV_SUCCESS == "X" && pdi1OProductModify.Exporting.ET_MESSAGES != null && pdi1OProductModify.Exporting.ET_MESSAGES.Length > 0)
        this.DisplayMessages(pdi1OProductModify.Exporting.ET_MESSAGES);
      return pdi1OProductModify.Exporting.ES_PRODUCT;
    }

    public PDI_S_PRODUCT_VERS_AND_COMPS CreateSolution(string Name, string Description, string DetailedDescription, string Keywords, string SolutionType, string DeploymentUnit, bool IsGlobalSolution, string DevPartner, string ContactPerson, string EMail)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_CREATE pdiLmProductCreate = new PDI_LM_PRODUCT_CREATE();
      pdiLmProductCreate.Importing = new PDI_LM_PRODUCT_CREATE.ImportingType();
      pdiLmProductCreate.Importing.IV_PRODUCT_NAME = Name;
      pdiLmProductCreate.Importing.IV_PRODUCT_DESCRIPTION = Description;
      pdiLmProductCreate.Importing.IV_PRODUCT_TYPE = SolutionType;
      pdiLmProductCreate.Importing.IV_DEPLOYMENT_UNIT = DeploymentUnit;
      pdiLmProductCreate.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      pdiLmProductCreate.Importing.IV_DEV_PARTNER = DevPartner;
      pdiLmProductCreate.Importing.IV_CONTACT_PERSON = ContactPerson;
      pdiLmProductCreate.Importing.IV_EMAIL = EMail;
      if (IsGlobalSolution)
        pdiLmProductCreate.Importing.IV_CREATE_MODE = "G";
      else
        pdiLmProductCreate.Importing.IV_CREATE_MODE = "L";
      jsonClient.callFunctionModule((SAPFunctionModule) pdiLmProductCreate, true, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) pdiLmProductCreate, false, false);
      }
      catch (ProtocolException ex)
      {
        return (PDI_S_PRODUCT_VERS_AND_COMPS) null;
      }
      if (pdiLmProductCreate.Exporting.EV_SUCCESS == "X" && pdiLmProductCreate.Exporting.ET_MESSAGES != null && pdiLmProductCreate.Exporting.ET_MESSAGES.Length > 0)
        this.DisplayMessages(pdiLmProductCreate.Exporting.ET_MESSAGES);
      return pdiLmProductCreate.Exporting.ES_PRODUCT;
    }

    private void DisplayMessages(PDI_RI_S_MESSAGE[] messages)
    {
      if (messages == null || messages.Length == 0)
        return;
      List<SAP.Copernicus.Core.Protocol.JSON.Message> messages1 = new List<SAP.Copernicus.Core.Protocol.JSON.Message>();
      foreach (PDI_RI_S_MESSAGE message in messages)
        messages1.Add(new SAP.Copernicus.Core.Protocol.JSON.Message(SAP.Copernicus.Core.Protocol.JSON.Message.MessageSeverity.FromABAPCode(message.SEVERITY), message.TEXT));
      int num = (int) new MessageDialog(messages1).ShowDialog();
    }

    private void DisplayMessages(PDI_LM_S_MSG_LIST[] messages)
    {
      if (messages == null || messages.Length == 0)
        return;
      List<SAP.Copernicus.Core.Protocol.JSON.Message> messages1 = new List<SAP.Copernicus.Core.Protocol.JSON.Message>();
      foreach (PDI_LM_S_MSG_LIST message in messages)
        messages1.Add(new SAP.Copernicus.Core.Protocol.JSON.Message(SAP.Copernicus.Core.Protocol.JSON.Message.MessageSeverity.FromABAPCode(message.SEVERITY), message.TEXT));
      int num = (int) new MessageDialog(messages1).ShowDialog();
    }

    public bool DeleteSolution(string Name)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_DELETE pdiLmProductDelete = new PDI_LM_PRODUCT_DELETE();
      pdiLmProductDelete.Importing = new PDI_LM_PRODUCT_DELETE.ImportingType();
      pdiLmProductDelete.Importing.IV_PRODUCT_NAME = Name;
      pdiLmProductDelete.Importing.IV_CHECK_ONLY = " ";
      pdiLmProductDelete.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      pdiLmProductDelete.Importing.IV_MODE = "D";
      jsonClient.callFunctionModule((SAPFunctionModule) pdiLmProductDelete, false, false, false);
      ErrorSink.Instance.ClearTasksFor(Origin.GenericSolutionAction);
      ErrorSink.Instance.AddMessages(pdiLmProductDelete.Exporting.ET_MSG_LIST, Resource.MessagePrefixDelete, Origin.GenericSolutionAction);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) pdiLmProductDelete, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return pdiLmProductDelete.Exporting.EV_SUCCESS == "X";
    }

    public bool ActivateSolution(string Name, bool deltaMode = false, char caller = '\0', bool isSplitActivate = false)
    {
      Cursor.Current = Cursors.WaitCursor;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_ACTIVATE_BATCH productActivateBatch = new PDI_LM_PRODUCT_ACTIVATE_BATCH();
      productActivateBatch.Importing = new PDI_LM_PRODUCT_ACTIVATE_BATCH.ImportingType();
      productActivateBatch.Importing.IV_PRODUCT_NAME = Name;
      productActivateBatch.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      productActivateBatch.Importing.IV_MODE = "D";
      if (isSplitActivate)
        productActivateBatch.Importing.IV_ACT_SPLIT = "X";
      else
        productActivateBatch.Importing.IV_ACT_SPLIT = " ";
      if ((int) caller == 65)
      {
        productActivateBatch.Importing.IV_DELTA_MODE = "X";
        productActivateBatch.Importing.IV_CALLER = SolutionHandler.Caller.AssembleAndDownload.ToString();
      }
      if (deltaMode)
        productActivateBatch.Importing.IV_DELTA_MODE = "X";
      SAP.Copernicus.Core.Protocol.Connection.getInstance().CheckConnectionChanged();
      jsonClient.callFunctionModule((SAPFunctionModule) productActivateBatch, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) productActivateBatch, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      if (productActivateBatch.Exporting.EV_SUCCESS == "X")
      {
        if (isSplitActivate)
          return true;
        bool flag = false;
        string evApplogId = productActivateBatch.Exporting.EV_APPLOG_ID;
        string evJobcnt = productActivateBatch.Exporting.EV_JOBCNT;
        string evJobname = productActivateBatch.Exporting.EV_JOBNAME;
        BatchGetStatusHandler getStatusHandler = new BatchGetStatusHandler();
        PDI_LM_BATCH_GET_STATUS.ExportingType exportingType = (PDI_LM_BATCH_GET_STATUS.ExportingType) null;
        PDI_LM_T_MSG_LIST[] msgs1 = (PDI_LM_T_MSG_LIST[]) null;
        int millisecondsTimeout = 750;
        while (!flag)
        {
          Cursor.Current = Cursors.WaitCursor;
          Thread.Sleep(millisecondsTimeout);
          if (millisecondsTimeout < 6000)
            millisecondsTimeout *= 2;
          exportingType = getStatusHandler.getBatchStatus(evJobname, evJobcnt, evApplogId, out msgs1);
          if (exportingType.EV_JOB_STATE.Equals("05") || exportingType.EV_JOB_STATE.Equals("07") || exportingType.EV_JOB_STATE.Equals("06"))
            flag = true;
          if (exportingType.ET_MESSAGES.Length > 0)
            CopernicusStatusBar.Instance.ShowMessage(exportingType.ET_MESSAGES[exportingType.ET_MESSAGES.Length - 1].TEXT);
        }
        ErrorSink.Instance.ClearTasksFor(Origin.GenericSolutionAction);
        ErrorSink.Instance.AddMessages(msgs1, Resource.MessagePrefixActivate, Origin.GenericSolutionAction);
        Cursor.Current = Cursors.Default;
        if ((exportingType.EV_SUCCESS == "" || exportingType.EV_PROC_SUCC == "") && (exportingType.ET_MESSAGES != null && exportingType.ET_MESSAGES.Length > 0))
        {
          List<SAP.Copernicus.Core.Protocol.JSON.Message> msgs2 = new List<SAP.Copernicus.Core.Protocol.JSON.Message>();
          foreach (PDI_RI_S_MESSAGE pdiRiSMessage in exportingType.ET_MESSAGES)
          {
            SAP.Copernicus.Core.Protocol.JSON.Message.MessageSeverity severity = SAP.Copernicus.Core.Protocol.JSON.Message.MessageSeverity.FromABAPCode(pdiRiSMessage.SEVERITY);
            msgs2.Add(new SAP.Copernicus.Core.Protocol.JSON.Message(severity, pdiRiSMessage.TEXT));
          }
          new ProtocolException(ProtocolException.ErrorArea.SERVER, msgs2).showPopup();
          return false;
        }
        if (exportingType.EV_PROC_SUCC == "X" && exportingType.EV_SUCCESS == "X")
          return true;
      }
      return false;
    }

    public void DisplayLogs(string Name)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_CHECK_LOGS productCheckLogs = new PDI_LM_PRODUCT_CHECK_LOGS();
      productCheckLogs.Importing = new PDI_LM_PRODUCT_CHECK_LOGS.ImportingType();
      productCheckLogs.Importing.IV_PRODUCT_NAME = Name;
      jsonClient.callFunctionModule((SAPFunctionModule) productCheckLogs, false, false, false);
      ErrorSink.Instance.ClearTasksFor(Origin.GenericSolutionAction);
      this.reportServerSideProtocolException((SAPFunctionModule) productCheckLogs, true, false);
      try
      {
        List<PDI_RI_S_MESSAGE> pdiRiSMessageList = new List<PDI_RI_S_MESSAGE>();
        foreach (PDI_T_APPLOG_MSG pdiTApplogMsg in productCheckLogs.Exporting.ET_APPLOG)
          pdiRiSMessageList.Add(new PDI_RI_S_MESSAGE()
          {
            TEXT = pdiTApplogMsg.LOG_MSG,
            SEVERITY = pdiTApplogMsg.LOG_SEVERITY
          });
        List<SAP.Copernicus.Core.Protocol.JSON.Message> messages = new List<SAP.Copernicus.Core.Protocol.JSON.Message>();
        if (pdiRiSMessageList.Count <= 0)
          return;
        foreach (PDI_RI_S_MESSAGE pdiRiSMessage in pdiRiSMessageList)
          messages.Add(new SAP.Copernicus.Core.Protocol.JSON.Message(SAP.Copernicus.Core.Protocol.JSON.Message.MessageSeverity.FromABAPCode(pdiRiSMessage.SEVERITY), pdiRiSMessage.TEXT));
        int num = (int) new MessageDialog(messages).ShowDialog();
      }
      catch (ProtocolException ex)
      {
        this.reportServerSideProtocolException((SAPFunctionModule) productCheckLogs, false, false);
      }
    }

    public string backgroundModeCheck()
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_1O_CHECK_TOGGLE pdi1OCheckToggle = new PDI_1O_CHECK_TOGGLE();
        pdi1OCheckToggle.Importing = new PDI_1O_CHECK_TOGGLE.ImportingType();
        jsonClient.callFunctionModule((SAPFunctionModule) pdi1OCheckToggle, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdi1OCheckToggle, true, false);
        if (pdi1OCheckToggle.Exporting != null)
          return pdi1OCheckToggle.Exporting.EV_CHK_BATCH_ENABLED;
        return "error";
      }
      catch (ProtocolException ex)
      {
        return "error";
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return "error";
      }
    }

    public bool CheckInBackgroundMode(string name, out IDictionary<string, List<string>> usageErrorList, bool checkRuntime = false, bool backgrndmode_on = false)
    {
      IDictionary<string, List<string>> dictionary = (IDictionary<string, List<string>>) new Dictionary<string, List<string>>();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_CHECK_BATCH productCheckBatch = new PDI_LM_PRODUCT_CHECK_BATCH();
      productCheckBatch.Importing = new PDI_LM_PRODUCT_CHECK_BATCH.ImportingType();
      productCheckBatch.Importing.IV_PRODUCT_NAME = name;
      productCheckBatch.Importing.IV_MODE = "D";
      PDI_S_CHECK_SCOPE pdiSCheckScope = new PDI_S_CHECK_SCOPE();
      pdiSCheckScope.DESIGN_TIME = "X";
      if (checkRuntime)
        pdiSCheckScope.GENERATED_ARTIFACTS = "X";
      productCheckBatch.Importing.IS_CHECK_SCOPE = pdiSCheckScope;
      jsonClient.callFunctionModule((SAPFunctionModule) productCheckBatch, false, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) productCheckBatch, true, false);
      usageErrorList = dictionary;
      try
      {
        return productCheckBatch.Exporting != null && productCheckBatch.Exporting.EV_SUCCESS == "X";
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    public bool CheckSolution(string name, out IDictionary<string, List<string>> usageErrorList, bool checkRuntime = false)
    {
      IDictionary<string, List<string>> dictionary = (IDictionary<string, List<string>>) new Dictionary<string, List<string>>();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_CHECK pdiLmProductCheck = new PDI_LM_PRODUCT_CHECK();
      pdiLmProductCheck.Importing = new PDI_LM_PRODUCT_CHECK.ImportingType();
      pdiLmProductCheck.Importing.IV_PRODUCT_NAME = name;
      pdiLmProductCheck.Importing.IV_MODE = "D";
      PDI_S_CHECK_SCOPE pdiSCheckScope = new PDI_S_CHECK_SCOPE();
      pdiSCheckScope.DESIGN_TIME = "X";
      if (checkRuntime)
        pdiSCheckScope.GENERATED_ARTIFACTS = "X";
      pdiLmProductCheck.Importing.IS_CHECK_SCOPE = pdiSCheckScope;
      jsonClient.callFunctionModule((SAPFunctionModule) pdiLmProductCheck, false, false, false);
      ErrorSink.Instance.ClearTasksFor(Origin.GenericSolutionAction);
      if (pdiLmProductCheck.Exporting != null)
        ErrorSink.Instance.AddMessages(pdiLmProductCheck.Exporting.ET_MSG_LIST, Resource.MessagePrefixCheck, Origin.GenericSolutionAction);
      try
      {
        foreach (PDI_S_USG_INCONSISTENCY usgInconsistency in pdiLmProductCheck.Exporting.ET_INCONSISTENCIES)
        {
          List<string> stringList = new List<string>();
          if (usgInconsistency.MESSAGES != null)
          {
            foreach (PDI_RI_S_MESSAGE pdiRiSMessage in usgInconsistency.MESSAGES)
              stringList.Add(pdiRiSMessage.TEXT);
          }
          if (usgInconsistency.STATUS != "C")
            dictionary.Add(usgInconsistency.ENTITY_PATH, stringList);
        }
        usageErrorList = dictionary;
        this.reportServerSideProtocolException((SAPFunctionModule) pdiLmProductCheck, false, false);
      }
      catch (ProtocolException ex)
      {
        usageErrorList = dictionary;
        return false;
      }
      catch (Exception ex)
      {
        usageErrorList = dictionary;
        return false;
      }
      return pdiLmProductCheck.Exporting.EV_SUCCESS == "X";
    }

    public string CreateFormCode(string entity, string ns)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_PF_CODE_GENERATE pdiPfCodeGenerate = new PDI_PF_CODE_GENERATE();
      pdiPfCodeGenerate.Importing = new PDI_PF_CODE_GENERATE.ImportingType();
      pdiPfCodeGenerate.Importing.IV_ENTITY = entity;
      pdiPfCodeGenerate.Importing.IV_NAMESPACE = ns;
      jsonClient.callFunctionModule((SAPFunctionModule) pdiPfCodeGenerate, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) pdiPfCodeGenerate, false, false);
      }
      catch (ProtocolException ex)
      {
        return "";
      }
      if (pdiPfCodeGenerate.Exporting.EV_SUCCESS == "X")
        return pdiPfCodeGenerate.Exporting.EV_GENERATED_CODE;
      return "";
    }

    public bool CleanUpSolution(string name)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_CLEANUP lmProductCleanup = new PDI_LM_PRODUCT_CLEANUP();
      lmProductCleanup.Importing = new PDI_LM_PRODUCT_CLEANUP.ImportingType();
      lmProductCleanup.Importing.IV_PRODUCT_NAME = name;
      lmProductCleanup.Importing.IV_MODE = "D";
      jsonClient.callFunctionModule((SAPFunctionModule) lmProductCleanup, false, false, false);
      ErrorSink.Instance.ClearTasksFor(Origin.GenericSolutionAction);
      ErrorSink.Instance.AddMessages(lmProductCleanup.Exporting.ET_MSG_LIST, Resource.MessagePrefixClean, Origin.GenericSolutionAction);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) lmProductCleanup, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return lmProductCleanup.Exporting.EV_SUCCESS == "X";
    }

    public bool CheckAssemblyStatusOfSolution(string name)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_CHECK_ASSEMBLY productCheckAssembly = new PDI_LM_PRODUCT_CHECK_ASSEMBLY();
      productCheckAssembly.Importing = new PDI_LM_PRODUCT_CHECK_ASSEMBLY.ImportingType();
      productCheckAssembly.Importing.IV_PRODUCT_NAME = name;
      jsonClient.callFunctionModule((SAPFunctionModule) productCheckAssembly, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) productCheckAssembly, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return productCheckAssembly.Exporting.EV_SUCCESS == "X";
    }

    public bool TestDeploySolution(string name)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_TEST_DEPLOY productTestDeploy = new PDI_LM_PRODUCT_TEST_DEPLOY();
      productTestDeploy.Importing = new PDI_LM_PRODUCT_TEST_DEPLOY.ImportingType();
      productTestDeploy.Importing.IV_PRODUCT_NAME = name;
      productTestDeploy.Importing.IV_CHECK_ONLY = "";
      jsonClient.callFunctionModule((SAPFunctionModule) productTestDeploy, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) productTestDeploy, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return productTestDeploy.Exporting.EV_SUCCESS == "X";
    }

    public bool RequestCertificationSolution(string name)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_REQUEST_CERTI productRequestCerti = new PDI_LM_PRODUCT_REQUEST_CERTI();
      productRequestCerti.Importing = new PDI_LM_PRODUCT_REQUEST_CERTI.ImportingType();
      productRequestCerti.Importing.IV_PRODUCT_NAME = name;
      jsonClient.callFunctionModule((SAPFunctionModule) productRequestCerti, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) productRequestCerti, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return productRequestCerti.Exporting.EV_SUCCESS == "X";
    }

    public bool RequestPatchSolution(string name)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_REQUEST_PATCH productRequestPatch = new PDI_LM_PRODUCT_REQUEST_PATCH();
      productRequestPatch.Importing = new PDI_LM_PRODUCT_REQUEST_PATCH.ImportingType();
      productRequestPatch.Importing.IV_PRODUCT_NAME = name;
      jsonClient.callFunctionModule((SAPFunctionModule) productRequestPatch, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) productRequestPatch, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return productRequestPatch.Exporting.EV_SUCCESS == "X";
    }

    public bool CreateVersionSolution(string name, string lang)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_CREATE_VERSION productCreateVersion = new PDI_LM_PRODUCT_CREATE_VERSION();
      productCreateVersion.Importing = new PDI_LM_PRODUCT_CREATE_VERSION.ImportingType();
      productCreateVersion.Importing.IV_PRODUCT_NAME = name;
      productCreateVersion.Importing.IT_SUPPORTED_LANGUAGE = lang;
      jsonClient.callFunctionModule((SAPFunctionModule) productCreateVersion, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) productCreateVersion, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return productCreateVersion.Exporting.EV_SUCCESS == "X";
    }

    public bool CanDeleteSolution(string Name)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_DELETE pdiLmProductDelete = new PDI_LM_PRODUCT_DELETE();
      pdiLmProductDelete.Importing = new PDI_LM_PRODUCT_DELETE.ImportingType();
      pdiLmProductDelete.Importing.IV_PRODUCT_NAME = Name;
      pdiLmProductDelete.Importing.IV_CHECK_ONLY = "X";
      jsonClient.callFunctionModule((SAPFunctionModule) pdiLmProductDelete, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) pdiLmProductDelete, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return pdiLmProductDelete.Exporting.EV_SUCCESS == "X";
    }

    public void SetKeyUser(string solutionname, string user, string enable, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS)
    {
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SUCCESS = (string) null;
      PDI_LM_PRODUCT_SET_KEY_USER productSetKeyUser = new PDI_LM_PRODUCT_SET_KEY_USER();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        productSetKeyUser.Importing = new PDI_LM_PRODUCT_SET_KEY_USER.ImportingType();
        productSetKeyUser.Importing.IV_PRODUCT = solutionname;
        productSetKeyUser.Importing.IV_USER = user;
        productSetKeyUser.Importing.IV_ENABLE = enable;
        jsonClient.callFunctionModule((SAPFunctionModule) productSetKeyUser, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) productSetKeyUser, false, false);
      }
      catch (Exception ex)
      {
        Trace.TraceError("Severe error in JSONClient: " + ex.Message);
        PDI_RI_S_MESSAGE pdiRiSMessage = new PDI_RI_S_MESSAGE();
        pdiRiSMessage.TEXT = ex.Message;
        ET_MESSAGES = new PDI_RI_S_MESSAGE[1];
        ET_MESSAGES[0] = pdiRiSMessage;
        EV_SUCCESS = "";
        return;
      }
      ET_MESSAGES = productSetKeyUser.Exporting.ET_MESSAGES;
      EV_SUCCESS = productSetKeyUser.Exporting.EV_SUCCESS;
    }

    public void IsKeyUserEnabled(string solutionname, string user, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS, out string EV_ENABLED)
    {
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SUCCESS = (string) null;
      EV_SUCCESS = (string) null;
      PDI_LM_PRD_IS_KEY_USER_ENABLED isKeyUserEnabled = new PDI_LM_PRD_IS_KEY_USER_ENABLED();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        isKeyUserEnabled.Importing = new PDI_LM_PRD_IS_KEY_USER_ENABLED.ImportingType();
        isKeyUserEnabled.Importing.IV_PRODUCT = solutionname;
        isKeyUserEnabled.Importing.IV_USER = user;
        jsonClient.callFunctionModule((SAPFunctionModule) isKeyUserEnabled, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) isKeyUserEnabled, false, false);
      }
      catch (Exception ex)
      {
        Trace.TraceError("Severe error in JSONClient: " + ex.Message);
        PDI_RI_S_MESSAGE pdiRiSMessage = new PDI_RI_S_MESSAGE();
        pdiRiSMessage.TEXT = ex.Message;
        ET_MESSAGES = new PDI_RI_S_MESSAGE[1];
        ET_MESSAGES[0] = pdiRiSMessage;
        EV_SUCCESS = (string) null;
        EV_ENABLED = (string) null;
        return;
      }
      ET_MESSAGES = isKeyUserEnabled.Exporting.ET_MESSAGES;
      EV_SUCCESS = isKeyUserEnabled.Exporting.EV_SUCCESS;
      EV_ENABLED = isKeyUserEnabled.Exporting.EV_ENABLED;
    }

    public void UpdateAccessRights(string solutionname, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS)
    {
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SUCCESS = (string) null;
      PDI_RBAM_UPDATE_BY_SOLUTION updateBySolution = new PDI_RBAM_UPDATE_BY_SOLUTION();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        updateBySolution.Importing = new PDI_RBAM_UPDATE_BY_SOLUTION.ImportingType();
        updateBySolution.Importing.IV_PRODUCT_NAME = solutionname;
        jsonClient.callFunctionModule((SAPFunctionModule) updateBySolution, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) updateBySolution, false, false);
      }
      catch (Exception ex)
      {
        Trace.TraceError("Severe error in JSONClient: " + ex.Message);
        PDI_RI_S_MESSAGE pdiRiSMessage = new PDI_RI_S_MESSAGE();
        pdiRiSMessage.TEXT = ex.Message;
        ET_MESSAGES = new PDI_RI_S_MESSAGE[1];
        ET_MESSAGES[0] = pdiRiSMessage;
        EV_SUCCESS = "";
        return;
      }
      ET_MESSAGES = updateBySolution.Exporting.ET_MESSAGES;
      EV_SUCCESS = updateBySolution.Exporting.EV_SUCCESS;
    }

    public bool CopySolution(string newSolName, string newSolType, string newSolDU, string oldSolName, string desc, out PDI_S_PRODUCT_VERS_AND_COMPS product)
    {
      JSONClient jsonTestClient = Client.getInstance().getJSONTestClient(false);
      PDI_COPY_PARTNER_SOLUTION copyPartnerSolution = new PDI_COPY_PARTNER_SOLUTION();
      copyPartnerSolution.Importing = new PDI_COPY_PARTNER_SOLUTION.ImportingType();
      copyPartnerSolution.Importing.IV_NEW_SOLUTION_NAME = newSolName;
      copyPartnerSolution.Importing.IV_ORIGINAL_SOLUTION_NAME = oldSolName;
      copyPartnerSolution.Importing.IV_NEW_SOLUTION_DESCRIPTION = desc;
      copyPartnerSolution.Importing.IV_NEW_SOL_TYPE = newSolType;
      copyPartnerSolution.Importing.IV_NEW_SOL_DU = newSolDU;
      jsonTestClient.callFunctionModule((SAPFunctionModule) copyPartnerSolution, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) copyPartnerSolution, false, false);
      }
      catch (ProtocolException ex)
      {
        product = (PDI_S_PRODUCT_VERS_AND_COMPS) null;
        return false;
      }
      if (copyPartnerSolution.Exporting.EV_SUCCESS == "X")
      {
        product = copyPartnerSolution.Exporting.ES_NEW_PRODUCT;
        return true;
      }
      product = (PDI_S_PRODUCT_VERS_AND_COMPS) null;
      return false;
    }

    public bool MergeSolution(string sourceSolName, string targetSolName)
    {
      JSONClient jsonTestClient = Client.getInstance().getJSONTestClient(false);
      PDI_MERGE_PARTNER_SOLUTION mergePartnerSolution = new PDI_MERGE_PARTNER_SOLUTION();
      mergePartnerSolution.Importing = new PDI_MERGE_PARTNER_SOLUTION.ImportingType();
      mergePartnerSolution.Importing.IV_ORIGINAL_SOLUTION_NAME = targetSolName;
      mergePartnerSolution.Importing.IV_COPIED_SOLUTION_NAME = sourceSolName;
      jsonTestClient.callFunctionModule((SAPFunctionModule) mergePartnerSolution, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) mergePartnerSolution, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return mergePartnerSolution.Exporting.EV_SUCCESS == "X";
    }

    public bool CreatePatchSolution(string currentlyOpenedSolutionname, string description, string patchtype)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_PRODUCT_CREATE_PATCH productCreatePatch = new PDI_LM_PRODUCT_CREATE_PATCH();
      productCreatePatch.Importing = new PDI_LM_PRODUCT_CREATE_PATCH.ImportingType();
      productCreatePatch.Importing.IV_DESCRIPTION = description;
      productCreatePatch.Importing.IV_PRODUCT_NAME = currentlyOpenedSolutionname;
      productCreatePatch.Importing.IV_PATCH_TYPE = patchtype;
      jsonClient.callFunctionModule((SAPFunctionModule) productCreatePatch, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) productCreatePatch, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return productCreatePatch.Exporting.EV_SUCCESS == "X";
    }

    public bool ImplementationManagerScalable(string solName, string detailLog, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out PDI_S_PRODUCT_STATUS ES_PRODUCT_DATA, out PDI_S_PATCH_STATUS ES_PATCH_DATA, out PDI_T_APPLOG_MSG[] ET_APPLOG)
    {
      PDI_LM_GET_SOLUTION_INFOS getSolutionInfos = new PDI_LM_GET_SOLUTION_INFOS();
      bool flag = false;
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      ES_PRODUCT_DATA = (PDI_S_PRODUCT_STATUS) null;
      ES_PATCH_DATA = (PDI_S_PATCH_STATUS) null;
      ET_APPLOG = (PDI_T_APPLOG_MSG[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        getSolutionInfos.Importing = new PDI_LM_GET_SOLUTION_INFOS.ImportingType();
        getSolutionInfos.Importing.IV_SOLUTION_NAME = solName;
        getSolutionInfos.Importing.IV_DETAIL_LOG = detailLog;
        jsonClient.callFunctionModule((SAPFunctionModule) getSolutionInfos, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) getSolutionInfos, false, false);
        if (getSolutionInfos.Exporting.EV_SUCCESS == "X")
        {
          flag = true;
          ET_MESSAGES = getSolutionInfos.Exporting.ET_MESSAGES;
          ES_PRODUCT_DATA = getSolutionInfos.Exporting.ES_PRODUCT_DATA;
          ES_PATCH_DATA = getSolutionInfos.Exporting.ES_PATCH_DATA;
          ET_APPLOG = getSolutionInfos.Exporting.ET_APPLOG;
        }
      }
      catch (ProtocolException ex)
      {
        flag = false;
        EV_SUCCESS = (string) null;
        ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
        ES_PRODUCT_DATA = (PDI_S_PRODUCT_STATUS) null;
        ES_PATCH_DATA = (PDI_S_PATCH_STATUS) null;
        ET_APPLOG = (PDI_T_APPLOG_MSG[]) null;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        flag = false;
        EV_SUCCESS = (string) null;
        ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
        ES_PRODUCT_DATA = (PDI_S_PRODUCT_STATUS) null;
        ES_PATCH_DATA = (PDI_S_PATCH_STATUS) null;
        ET_APPLOG = (PDI_T_APPLOG_MSG[]) null;
      }
      return flag;
    }

    public bool SpcIncidentCreate(string solName, string subject, string description, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_INCIDENT_ID, out string EV_LINK)
    {
      PDI_LM_SPC_INCIDENT_CREATE spcIncidentCreate = new PDI_LM_SPC_INCIDENT_CREATE();
      bool flag = false;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SUCCESS = string.Empty;
      EV_INCIDENT_ID = string.Empty;
      EV_LINK = string.Empty;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        spcIncidentCreate.Importing = new PDI_LM_SPC_INCIDENT_CREATE.ImportingType();
        spcIncidentCreate.Importing.IV_PRODUCT_NAME = solName;
        spcIncidentCreate.Importing.IV_DESCRIPTION = description;
        spcIncidentCreate.Importing.IV_SUBJECT = subject;
        jsonClient.callFunctionModule((SAPFunctionModule) spcIncidentCreate, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) spcIncidentCreate, false, false);
        EV_SUCCESS = spcIncidentCreate.Exporting.EV_SUCCESS;
        if (spcIncidentCreate.Exporting.EV_SUCCESS == "X")
        {
          flag = true;
          ET_MESSAGES = spcIncidentCreate.Exporting.ET_MESSAGES;
          EV_INCIDENT_ID = spcIncidentCreate.Exporting.EV_INCIDENT_ID;
          EV_LINK = spcIncidentCreate.Exporting.EV_LINK;
        }
      }
      catch (ProtocolException ex)
      {
        flag = false;
        EV_SUCCESS = (string) null;
        ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
        EV_INCIDENT_ID = (string) null;
        EV_LINK = (string) null;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        flag = false;
        EV_SUCCESS = (string) null;
        ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
        EV_INCIDENT_ID = (string) null;
        EV_LINK = (string) null;
      }
      return flag;
    }

    public bool IsBCSetForSAPBCOAllowed(string solutionName)
    {
      try
      {
        PDI_LM_IS_BCS_FOR_SAPBCO_ALLWD bcsForSapbcoAllwd = new PDI_LM_IS_BCS_FOR_SAPBCO_ALLWD();
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        bcsForSapbcoAllwd.Importing = new PDI_LM_IS_BCS_FOR_SAPBCO_ALLWD.ImportingType();
        bcsForSapbcoAllwd.Importing.IV_PRODUCT_NAME = solutionName;
        jsonClient.callFunctionModule((SAPFunctionModule) bcsForSapbcoAllwd, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) bcsForSapbcoAllwd, false, false);
        return bcsForSapbcoAllwd.Exporting.EV_SUCCESS == "X" && bcsForSapbcoAllwd.Exporting.EV_IS_ALLOWED == "X";
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public string checkMCSInstallationKey(string Key, bool type)
    {
      string str = "";
      try
      {
        PDI_10_MC_INSTLN_KEY_CHECK mcInstlnKeyCheck = new PDI_10_MC_INSTLN_KEY_CHECK();
        mcInstlnKeyCheck.Importing = new PDI_10_MC_INSTLN_KEY_CHECK.ImportingType();
        mcInstlnKeyCheck.Importing.IV_INSTALLATION_KEY = Key;
        if (type)
          mcInstlnKeyCheck.Importing.IV_MCS_MODE = "X";
        else
          mcInstlnKeyCheck.Importing.IV_MCS_MODE = "";
        Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) mcInstlnKeyCheck, false, false, false);
        if (mcInstlnKeyCheck.Exporting != null)
        {
          if (mcInstlnKeyCheck.Exporting.EV_SUCCESS.Equals("X"))
            str = mcInstlnKeyCheck.Exporting.EV_SOLUTION_NAME;
          else if (!type)
          {
            string message = "JSON communiation failure.Deploy MCS Mode Login Failure due to Server side error.";
            Trace.TraceError(message);
            throw new ProtocolException(ProtocolException.ErrorArea.SERVER, message);
          }
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return str;
    }

    public enum Caller
    {
      AssembleAndDownload = 65,
    }
  }
}
