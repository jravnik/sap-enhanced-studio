﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.EntityUsageIndex.EntityUsageInfo
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository.EntityUsageIndex
{
  public class EntityUsageInfo
  {
    private string TargetEntityName;
    private EntityUsageType UsageType;
    private bool CrossDUAccess;
    private bool WriteAccessUsed;
    private bool TargetDefinedInSource;
    private string CompilationDateTimeString;
    private string SourceXrepPath;

    public EntityUsageInfo(string TargetEntityName, EntityUsageType UsageType, bool CrossDUAccess, bool WriteAccessUsed, bool TargetDefinedInSource, string CompilationDateTimeString, string SourceXrepPath)
    {
      this.TargetEntityName = TargetEntityName;
      this.UsageType = UsageType;
      this.CrossDUAccess = CrossDUAccess;
      this.WriteAccessUsed = WriteAccessUsed;
      this.TargetDefinedInSource = TargetDefinedInSource;
      this.CompilationDateTimeString = CompilationDateTimeString;
      this.SourceXrepPath = SourceXrepPath;
    }

    public EntityUsageInfo(string TargetEntityName, EntityUsageType UsageType, bool CrossDUAccess, bool WriteAccessUsed, bool TargetDefinedInSource, string CompilationDateTimeString)
    {
      this.TargetEntityName = TargetEntityName;
      this.UsageType = UsageType;
      this.CrossDUAccess = CrossDUAccess;
      this.WriteAccessUsed = WriteAccessUsed;
      this.TargetDefinedInSource = TargetDefinedInSource;
      this.CompilationDateTimeString = CompilationDateTimeString;
    }

    public EntityUsageInfo(string TargetEntityName, EntityUsageType UsageType, bool CrossDUAccess, bool WriteAccessUsed, bool TargetDefinedInSource)
    {
      this.TargetEntityName = TargetEntityName;
      this.UsageType = UsageType;
      this.CrossDUAccess = CrossDUAccess;
      this.WriteAccessUsed = WriteAccessUsed;
      this.TargetDefinedInSource = TargetDefinedInSource;
    }

    public string GetSourceXrepPath()
    {
      return this.SourceXrepPath;
    }

    public string GetTargetEntityName()
    {
      return this.TargetEntityName;
    }

    public EntityUsageType GetUsageType()
    {
      return this.UsageType;
    }

    public string GetUsageTypeCode()
    {
      return ((int) this.UsageType).ToString();
    }

    public string GetUsageTypeName()
    {
      return this.UsageType.ToString();
    }

    public bool GetCrossDU()
    {
      return this.CrossDUAccess;
    }

    public bool GetTargetDefinedInsource()
    {
      return this.TargetDefinedInSource;
    }

    public bool GetWriteAccessUsed()
    {
      return this.WriteAccessUsed;
    }

    public string GetCompilationTimeString()
    {
      return this.CompilationDateTimeString;
    }

    public override bool Equals(object obj)
    {
      EntityUsageInfo entityUsageInfo = obj as EntityUsageInfo;
      return entityUsageInfo != null && !(this.CompilationDateTimeString != entityUsageInfo.CompilationDateTimeString) && (this.CrossDUAccess == entityUsageInfo.CrossDUAccess && !(this.SourceXrepPath != entityUsageInfo.SourceXrepPath)) && (this.TargetDefinedInSource == entityUsageInfo.TargetDefinedInSource && !(this.TargetEntityName != entityUsageInfo.TargetEntityName) && (this.UsageType == entityUsageInfo.UsageType && this.WriteAccessUsed == entityUsageInfo.WriteAccessUsed));
    }

    public override int GetHashCode()
    {
      int num = this.CrossDUAccess.GetHashCode() ^ this.TargetDefinedInSource.GetHashCode() ^ this.WriteAccessUsed.GetHashCode() ^ this.UsageType.GetHashCode();
      if (this.CompilationDateTimeString != null)
        num ^= this.CompilationDateTimeString.GetHashCode();
      if (this.SourceXrepPath != null)
        num ^= this.SourceXrepPath.GetHashCode();
      if (this.TargetEntityName != null)
        num ^= this.TargetEntityName.GetHashCode();
      return num;
    }
  }
}
