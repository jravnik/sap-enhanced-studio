﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.EntityUsageIndex.EntityUsageType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository.EntityUsageIndex
{
  public enum EntityUsageType
  {
    SOURCE_USES_SAP_GDT = 10,
    SOURCE_USES_SAP_BO = 20,
    SOURCE_USES_SAP_BO_NODE = 21,
    SOURCE_USES_SAP_BO_NODE_ELEMENT = 22,
    SOURCE_USES_SAP_BO_NODE_ACTION = 23,
    SOURCE_USES_SAP_BO_NODE_QUERY = 24,
    SOURCE_USES_SAP_BO_ASSOCIATION = 25,
    XBO_USES_SAP_BO = 140,
    XBO_USES_SAP_BO_NODE = 141,
    XBO_USES_SAP_GDT = 142,
    SOURCE_USES_PROCESS_COMPONENT = 152,
  }
}
