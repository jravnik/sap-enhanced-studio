﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeNameExt
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class NodeNameExt
  {
    public string Name { get; set; }

    public string ProxyName { get; set; }

    public string ParentProxyName { get; set; }

    public string ReleasedNode { get; set; }

    public string DisabledNode { get; set; }

    public string BoProxyName { get; set; }

    public string OffEnabled { get; set; }

    public NodeNameExt(string name, string proxyName, string parentProxyName, string boProxyName, string nodeReleased, string nodeDisabled, string offEnabled)
    {
      this.Name = name;
      this.ProxyName = proxyName;
      this.ParentProxyName = parentProxyName;
      this.ReleasedNode = nodeReleased;
      this.DisabledNode = nodeDisabled;
      this.BoProxyName = boProxyName;
      this.OffEnabled = offEnabled;
    }
  }
}
