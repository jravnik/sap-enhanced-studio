﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.UserInterface.BusinessObjectCategory
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.Core.Repository.UserInterface
{
  public struct BusinessObjectCategory
  {
    public static BusinessObjectCategory BusinessTransactionDocument = new BusinessObjectCategory("Business Transaction Document", "1");
    public static BusinessObjectCategory MasterDataObject = new BusinessObjectCategory("Master Data Object", "2");
    public static BusinessObjectCategory MassDataRunObject = new BusinessObjectCategory("Mass Data Run Object", "3");
    public static BusinessObjectCategory TechnicalObject = new BusinessObjectCategory("Technical Object", "4");
    public static BusinessObjectCategory BusinessConfigurationObject = new BusinessObjectCategory("Business Configuration Object", "5");
    public static BusinessObjectCategory MetaObject = new BusinessObjectCategory("Meta Object", "6");
    public static BusinessObjectCategory AnalyticsEnablementObject = new BusinessObjectCategory("AnalyticsEnablement Object", "7");
    public static BusinessObjectCategory KeyFigureBasedPlanObject = new BusinessObjectCategory("Key Figure Based Plan Object", "8");
    public static BusinessObjectCategory BusinessAdministrationObject = new BusinessObjectCategory("Business Administration Object", "9");
    private string text;
    private string code;
    private static BusinessObjectCategory[] businessObjectCategories;

    public string Text
    {
      get
      {
        return this.text;
      }
    }

    public string Code
    {
      get
      {
        return this.code;
      }
    }

    internal BusinessObjectCategory(string text, string code)
    {
      this.text = text;
      this.code = code;
    }

    public override string ToString()
    {
      return this.Text;
    }

    public static BusinessObjectCategory[] getBusinessObjectCategories()
    {
      if (BusinessObjectCategory.businessObjectCategories == null)
        BusinessObjectCategory.businessObjectCategories = new List<BusinessObjectCategory>()
        {
          BusinessObjectCategory.BusinessTransactionDocument,
          BusinessObjectCategory.MasterDataObject,
          BusinessObjectCategory.TechnicalObject,
          BusinessObjectCategory.BusinessConfigurationObject,
          BusinessObjectCategory.BusinessAdministrationObject
        }.ToArray<BusinessObjectCategory>();
      return BusinessObjectCategory.businessObjectCategories;
    }
  }
}
