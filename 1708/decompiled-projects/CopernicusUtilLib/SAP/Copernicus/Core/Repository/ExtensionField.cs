﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtensionField
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.Core.Repository
{
  public class ExtensionField
  {
    private string boNamespace;
    private string boName;
    private string name;
    private string type;
    private string node;
    private string label;
    private string tooltip;
    private string length;
    private string decimals;
    private bool transient;
    private string defaultValue;
    private string defaultCode;
    private PDI_EXT_S_ID_TARGET_ATTRIBUTE relation;

    public ExtensionField(string boNamespace, string boName, string name, string type, string node, string label, string tooltip, string length, string decimals, bool transient, string defaultValue, string defaultCode, PDI_EXT_S_ID_TARGET_ATTRIBUTE relation)
    {
      this.boNamespace = boNamespace;
      this.boName = boName;
      this.name = name;
      this.type = type;
      this.node = node;
      this.label = label;
      this.tooltip = tooltip;
      this.length = length;
      this.decimals = decimals;
      this.transient = transient;
      this.defaultCode = defaultCode;
      this.defaultValue = defaultValue;
      this.relation = relation;
    }

    public string getNamespace()
    {
      return this.boNamespace;
    }

    public string getBoName()
    {
      return this.boName;
    }

    public string getName()
    {
      return this.name;
    }

    public string getType()
    {
      return this.type;
    }

    public string getNode()
    {
      return this.node;
    }

    public string getLabel()
    {
      return this.label;
    }

    public string getTooltip()
    {
      return this.tooltip;
    }

    public string getLength()
    {
      return this.length;
    }

    public string getDecimals()
    {
      return this.decimals;
    }

    public string getTransient()
    {
      if (!this.transient)
        return string.Empty;
      return "X";
    }

    public string getDefaultCode()
    {
      return this.defaultCode;
    }

    public string getDefaultValue()
    {
      return this.defaultValue;
    }

    public PDI_EXT_S_ID_TARGET_ATTRIBUTE getRelationData()
    {
      return this.relation;
    }
  }
}
