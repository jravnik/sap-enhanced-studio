﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeKey
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Repository
{
  internal class NodeKey
  {
    public readonly string boProxyName;
    public readonly string nodeProxyName;

    public NodeKey(string boProxyName, string nodeProxyName)
    {
      this.boProxyName = boProxyName;
      this.nodeProxyName = nodeProxyName;
    }

    public class EqualityComparer : IEqualityComparer<NodeKey>
    {
      public bool Equals(NodeKey x, NodeKey y)
      {
        if (x.boProxyName == y.boProxyName)
          return x.nodeProxyName == y.nodeProxyName;
        return false;
      }

      public int GetHashCode(NodeKey obj)
      {
        return obj.boProxyName.GetHashCode() ^ obj.nodeProxyName.GetHashCode();
      }
    }
  }
}
