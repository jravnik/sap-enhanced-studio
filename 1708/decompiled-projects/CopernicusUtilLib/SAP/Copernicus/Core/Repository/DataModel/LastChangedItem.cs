﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.DataModel.LastChangedItem
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;

namespace SAP.Copernicus.Core.Repository.DataModel
{
  public class LastChangedItem
  {
    public string TransitiveHash;

    public string ProxyName { get; private set; }

    public DateTime LastChangedAt { get; private set; }

    public bool Handled { get; internal set; }

    public LastChangedItem(string proxyName, DateTime lastChangedAt)
    {
      this.ProxyName = proxyName;
      this.LastChangedAt = lastChangedAt;
    }

    public LastChangedItem(string proxyName, DateTime lastChangedAt, string transitiveHash)
    {
      this.ProxyName = proxyName;
      this.LastChangedAt = lastChangedAt;
      this.TransitiveHash = transitiveHash;
    }
  }
}
