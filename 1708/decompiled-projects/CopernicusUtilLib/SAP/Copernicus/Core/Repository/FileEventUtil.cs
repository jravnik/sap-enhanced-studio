﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.FileEventUtil
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public static class FileEventUtil
  {
    public static bool Match(this FileEventType me, FileEventType other)
    {
      return (me & other) != FileEventType.None;
    }

    public static bool IsEventTypeMask(this FileEventType me)
    {
      return (me - 1 & me) != FileEventType.None;
    }
  }
}
