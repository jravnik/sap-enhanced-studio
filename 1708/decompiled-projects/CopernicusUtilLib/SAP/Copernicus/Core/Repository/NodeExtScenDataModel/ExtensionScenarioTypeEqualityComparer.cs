﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeExtScenDataModel.ExtensionScenarioTypeEqualityComparer
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Repository.NodeExtScenDataModel
{
  public class ExtensionScenarioTypeEqualityComparer : IEqualityComparer<ExtensionScenarioType>
  {
    public bool Equals(ExtensionScenarioType s1, ExtensionScenarioType s2)
    {
      return s1.scenario_name == s2.scenario_name;
    }

    public int GetHashCode(ExtensionScenarioType s)
    {
      return s.scenario_name.GetHashCode();
    }
  }
}
