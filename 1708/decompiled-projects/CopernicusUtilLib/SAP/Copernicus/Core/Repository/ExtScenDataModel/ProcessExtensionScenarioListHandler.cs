﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtScenDataModel.ProcessExtensionScenarioListHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Repository.ExtScenDataList;
using System;
using System.IO;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.ExtScenDataModel
{
  public class ProcessExtensionScenarioListHandler
  {
    public ExtensionScenarioList loadModelByString(string content)
    {
      try
      {
        using (StringReader stringReader = new StringReader(content))
          return (ExtensionScenarioList) new XmlSerializer(typeof (ExtensionScenarioList)).Deserialize((TextReader) stringReader);
      }
      catch (Exception ex)
      {
        Console.Out.WriteLine("[ERROR] An exception occurred when trying to deserialize string " + content);
        return (ExtensionScenarioList) null;
      }
    }
  }
}
