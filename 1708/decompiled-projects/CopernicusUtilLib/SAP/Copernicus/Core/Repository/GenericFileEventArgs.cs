﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.GenericFileEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class GenericFileEventArgs
  {
    public bool OverallStatus { get; private set; }

    public bool DeltaMode { get; private set; }

    public bool MigrationMode { get; private set; }

    public GenericFileEventArgs(bool overallStatus, bool deltaMode = false, bool migrationMode = false)
    {
      this.OverallStatus = overallStatus;
      this.DeltaMode = deltaMode;
      this.MigrationMode = migrationMode;
    }

    public override bool Equals(object obj)
    {
      GenericFileEventArgs genericFileEventArgs = obj as GenericFileEventArgs;
      return genericFileEventArgs != null && genericFileEventArgs.DeltaMode == this.DeltaMode && genericFileEventArgs.OverallStatus == this.OverallStatus;
    }

    public override int GetHashCode()
    {
      return this.OverallStatus.GetHashCode() ^ this.DeltaMode.GetHashCode();
    }

    public override string ToString()
    {
      return string.Format("[OverallStatus:{0},DeltaMode:{1}]", (object) this.OverallStatus, (object) this.DeltaMode);
    }
  }
}
