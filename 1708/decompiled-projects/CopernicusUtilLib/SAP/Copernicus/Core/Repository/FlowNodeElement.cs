﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.FlowNodeElement
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class FlowNodeElement
  {
    public string FlowName { get; set; }

    public string BoName { get; set; }

    public string NodeName { get; set; }

    public string ScenarioName { get; set; }

    public FlowNodeElement(string flowName, string boName, string nodeName, string scenarioName)
    {
      this.FlowName = flowName;
      this.BoName = boName;
      this.NodeName = nodeName;
      this.ScenarioName = scenarioName;
    }
  }
}
