﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtScenDataList.ExtensionScenarioType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.ExtScenDataList
{
  [DebuggerStepThrough]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/ExtensionScenarioList")]
  [DesignerCategory("code")]
  [Serializable]
  public class ExtensionScenarioType
  {
    private string scenario_nameField;
    private string scenario_descriptionField;
    private string service_interface_typeField;
    private FlowType[] bo_connectionsField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string scenario_name
    {
      get
      {
        return this.scenario_nameField;
      }
      set
      {
        this.scenario_nameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string scenario_description
    {
      get
      {
        return this.scenario_descriptionField;
      }
      set
      {
        this.scenario_descriptionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string service_interface_type
    {
      get
      {
        return this.service_interface_typeField;
      }
      set
      {
        this.service_interface_typeField = value;
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified)]
    [XmlArrayItem("Flow", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
    public FlowType[] bo_connections
    {
      get
      {
        return this.bo_connectionsField;
      }
      set
      {
        this.bo_connectionsField = value;
      }
    }
  }
}
