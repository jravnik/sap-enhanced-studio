﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.MockResourceAccess
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.ResourceAccess.TestMocks.Core;

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks
{
  internal class MockResourceAccess : SAP.Copernicus.Core.ResourceAccess.ResourceAccess
  {
    public void Initialize(bool usePSM, bool mockMDRS = true)
    {
      this.Connection = (Connection) new MockConnection(usePSM);
      if (!mockMDRS)
        return;
      this.RepositoryDataCache = (RepositoryDataCache) new MockRepositoryDataCache();
    }
  }
}
