﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.TestController
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks
{
  public static class TestController
  {
    private static string testInputRootPath;

    public static bool TestMode { get; set; }

    public static string TestResultStorage { get; set; }

    public static string TestInputRootPath
    {
      get
      {
        return TestController.testInputRootPath;
      }
      set
      {
        TestController.testInputRootPath = value;
        TestController.PathUpdateEventRegistration(TestController.testInputRootPath);
      }
    }

    public static event TestController.EventHandlerOnPathUpdate PathUpdateEventRegistration;

    public delegate void EventHandlerOnPathUpdate(string newFilePath);
  }
}
