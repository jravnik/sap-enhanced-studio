﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.Core.MockRepositoryDataCache
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Repository;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks.Core
{
  public class MockRepositoryDataCache : RepositoryDataCache
  {
    public MockRepositoryDataCache()
    {
      if (!TestController.TestMode)
        throw new Exception("Not in test mode!");
      if (TestController.TestInputRootPath != null)
        this.ReadContentFromFile(TestController.TestInputRootPath + "RepDataCache\\Content.xml");
      RepositoryDataCache.instance = (RepositoryDataCache) this;
      TestController.PathUpdateEventRegistration += new TestController.EventHandlerOnPathUpdate(this.OnPathUpdate);
    }

    public void OnPathUpdate(string newPath)
    {
      this.ReadContentFromFile(newPath + "RepDataCache\\Content.xml");
    }

    private void ReadContentFromFile(string fileName)
    {
      FileInfo fileInfo = new FileInfo(fileName);
      if (!fileInfo.Exists)
      {
        Trace.TraceError("RepositoryDataCache Content.xml could not be found at: " + fileName);
      }
      else
      {
        using (FileStream fileStream = fileInfo.OpenRead())
        {
          this.repositoryDataSet.Clear();
          byte[] numArray = new byte[fileInfo.Length];
          fileStream.Read(numArray, 0, numArray.Length);
          int num = (int) this.repositoryDataSet.ReadXml((XmlReader) new XmlTextReader((TextReader) new StringReader(new UTF8Encoding().GetString(numArray))));
        }
      }
    }
  }
}
