﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Automation.DTEUtil
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using EnvDTE80;
using System;

namespace SAP.Copernicus.Core.Automation
{
  public class DTEUtil
  {
    private static DTEUtil.GetDteDelegate theDelegate;

    public static bool IsInitialized
    {
      get
      {
        return DTEUtil.theDelegate != null;
      }
    }

    public static void Initialize(DTEUtil.GetDteDelegate theDelegate)
    {
      DTEUtil.theDelegate = theDelegate;
    }

    public static DTE2 GetDTE()
    {
      if (DTEUtil.theDelegate == null)
        throw new NotSupportedException("DTEUtil not yet initialized");
      return DTEUtil.theDelegate();
    }

    public delegate DTE2 GetDteDelegate();
  }
}
