﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Automation.HELP_IDS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus.Core.Automation
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  public class HELP_IDS
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) HELP_IDS.resourceMan, (object) null))
          HELP_IDS.resourceMan = new ResourceManager("SAP.Copernicus.Core.Automation.HELP_IDS", typeof (HELP_IDS).Assembly);
        return HELP_IDS.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return HELP_IDS.resourceCulture;
      }
      set
      {
        HELP_IDS.resourceCulture = value;
      }
    }

    public static string BDS_APPROVALPROCESS
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_APPROVALPROCESS", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_B2BWEBSERVICE_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_B2BWEBSERVICE_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_BAC_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_BAC_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_BAC_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_BAC_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_BCO_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_BCO_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_BCO_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_BCO_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_BCSET_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_BCSET_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_BCSET_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_BCSET_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_BCVIEW_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_BCVIEW_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_BOEXTENSION_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_BOEXTENSION_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_BOEXTENSION_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_BOEXTENSION_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_CODELIST_DATATYPE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_CODELIST_DATATYPE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_COMMUNCATION_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_COMMUNCATION_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_COMMUNCATION_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_COMMUNCATION_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_CUSTOMER_SWITCH
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_CUSTOMER_SWITCH", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_DATAMASHUP_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_DATAMASHUP_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_DATAMASHUP_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_DATAMASHUP_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_DATASOURCE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_DATASOURCE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_DATASOURCE_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_DATASOURCE_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_DUMPANALYSIS
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_DUMPANALYSIS", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_ENHANCEMENT_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_ENHANCEMENT_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_ENHANCEMENT_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_ENHANCEMENT_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_EXTENSIBILITY_REFERENCES
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_EXTENSIBILITY_REFERENCES", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_EXTERNALWEBSERVICE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_EXTERNALWEBSERVICE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_FILEINPUT_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_FILEINPUT_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_FILEINPUT_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_FILEINPUT_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_FORM_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_FORM_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_FORM_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_FORM_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_FORMEXTENSION_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_FORMEXTENSION_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_FORMEXTENSION_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_FORMEXTENSION_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_GENERATE_INSTKEY
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_GENERATE_INSTKEY", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_HTMLMASHUP_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_HTMLMASHUP_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_HTMLMASHUP_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_HTMLMASHUP_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_INTERNALCOMMUNICATION_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_INTERNALCOMMUNICATION_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_INTERNALCOMMUNICATION_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_INTERNALCOMMUNICATION_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_MASSDATARUN
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_MASSDATARUN", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_MDRO_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_MDRO_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_MYSOLUTIONS_SELECTION
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_MYSOLUTIONS_SELECTION", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_NOTIFICATIONRULES
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_NOTIFICATIONRULES", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_OO_CUSTOMER_SWITCH
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_OO_CUSTOMER_SWITCH", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_OO_IMPLEMENTATIONMANAGER
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_OO_IMPLEMENTATIONMANAGER", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_OO_IMPLEMENTATIONMANAGER_SELECTION
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_OO_IMPLEMENTATIONMANAGER_SELECTION", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_OO_SOLUTION_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_OO_SOLUTION_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_PROCESSEXTENSION_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_PROCESSEXTENSION_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_PROCESSEXTENSION_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_PROCESSEXTENSION_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_PROJECTTEMPLATE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_PROJECTTEMPLATE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_PROJECTTEMPLATE_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_PROJECTTEMPLATE_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_QUERY_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_QUERY_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_QUERY_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_QUERY_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_QUERY_EXECUTE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_QUERY_EXECUTE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_REPOSITORYEXPLORER_SELECTION
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_REPOSITORYEXPLORER_SELECTION", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_RESTSERVICE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_RESTSERVICE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_RESTSERVICE_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_RESTSERVICE_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_RESTWEBSERVICE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_RESTWEBSERVICE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_REUSELIBRARY_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_REUSELIBRARY_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_RSSATOMSERVICE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_RSSATOMSERVICE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_RSSATOMSERVICE_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_RSSATOMSERVICE_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_SCRIPTFILE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_SCRIPTFILE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_SERVICEOPERATION_TEST
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_SERVICEOPERATION_TEST", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_SESSIONADMINISTRATOR_SELECTION
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_SESSIONADMINISTRATOR_SELECTION", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_SOAPSERVICE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_SOAPSERVICE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_SOAPSERVICE_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_SOAPSERVICE_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_SOAPWEBSERVICE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_SOAPWEBSERVICE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_SOLUTION_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_SOLUTION_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_THINGTYPE_MAP
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_THINGTYPE_MAP", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_TRACEEXPLORER_SELECTION
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_TRACEEXPLORER_SELECTION", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_TRANSLATION_CHECK
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_TRANSLATION_CHECK", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_TRANSLATION_EXPORT
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_TRANSLATION_EXPORT", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_TRANSLATION_IMPORT
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_TRANSLATION_IMPORT", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_UI_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_UI_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_UISWITCH
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_UISWITCH", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_URLMASHUP_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_URLMASHUP_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_URLMASHUP_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_URLMASHUP_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_WEBSERVICE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_WEBSERVICE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_WEBSERVICE_EDITOR
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_WEBSERVICE_EDITOR", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_WEBSERVICEINTREGRATION_EXTEND
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_WEBSERVICEINTREGRATION_EXTEND", HELP_IDS.resourceCulture);
      }
    }

    public static string BDS_XBO_SCRIPTFILE_CREATE
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("BDS_XBO_SCRIPTFILE_CREATE", HELP_IDS.resourceCulture);
      }
    }

    public static string MSDNSTART
    {
      get
      {
        return HELP_IDS.ResourceManager.GetString("MSDNSTART", HELP_IDS.resourceCulture);
      }
    }

    internal HELP_IDS()
    {
    }
  }
}
