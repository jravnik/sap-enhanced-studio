﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Scripting.IScriptFileMetadata
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Scripting
{
  public interface IScriptFileMetadata
  {
    bool OfflineOnlineEnabled { get; set; }

    bool OnlineFileExists { get; set; }

    bool CreateOnlineFile { get; set; }

    SnippetType? Type { get; }

    SnippetSubType? SubType { get; }

    string ProjectNamespace { get; set; }

    string CustomFileName { get; set; }

    bool MassProcessing { get; set; }

    bool Offline { get; set; }

    string BOName { get; set; }

    string NodeName { get; set; }

    string Namespace { get; set; }

    string ActionName { get; }

    string PIName { get; set; }

    bool IsExtensibility { get; set; }

    string ExtBOName { get; set; }

    string FMTName { get; set; }

    string FMTNameSpace { get; set; }

    string FMTRootElementName { get; set; }

    string FMTTypingDataType { get; set; }

    string FMTType { get; set; }

    string RuntimeNamespacePrefix { get; set; }

    string AppExitName { get; set; }

    string AppExitESName { get; set; }

    string AppExitMethodName { get; set; }

    string AppExitBadiProxyName { get; set; }

    string AppExitInputParameterDataType { get; set; }

    string AppExitOutputParameterDataType { get; set; }

    string CRLLibraryName { get; set; }

    string CRLFunctionName { get; set; }

    string CRLFunctionTemplateAdditionalImport { get; set; }

    string CRLFunctionTemplateReturnStatement { get; set; }

    void SetMassProcessing(bool massProc);

    void SetOffline(bool offline);

    string GetCustomValue(string key);

    void SetCustomValue(string key, string value);

    void SetBOContext(string boName, string nodeName, string ns);

    void SetXBOContext(string extBOName, string extNodeName, string projectNS);

    void SetSnippetType(SnippetType type, SnippetSubType subType);

    void SetActionValidationType(string actionName);

    void SetActionType(string actionName);

    void SetPIType(string PIName, SnippetSubType subType, string projectNS);

    void SetFMTType(string fmtName, string fmtNS, string fmtDataType, string runtimeNamespacePrefix);

    void SetAppExitType(string projectNS, string exitName, string exitESName, string exitMethodName, string badiProxyName, string inputParamDTName, string outputParamDTName, string deploymentUnit);

    void ParseFromDict(IDictionary<string, string> attribDict);

    Dictionary<string, string> GetAsDict();

    void Retrieve(string fullPath);

    string GetProjectNamespace();

    string RenderFileName(string customBOName = null, bool offline = false);

    string RenderXRepPath(bool deprecated = false);
  }
}
