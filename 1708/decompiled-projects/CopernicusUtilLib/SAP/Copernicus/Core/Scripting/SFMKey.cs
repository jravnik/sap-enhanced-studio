﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Scripting.SFMKey
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Scripting
{
  public enum SFMKey
  {
    BOName,
    NodeName,
    Namespace,
    SnippetType,
    SnippetSubType,
    ActionName,
    PIName,
    CustomFileName,
    IsExt,
    ExtBOName,
    ProjectNamespace,
    FMTName,
    FMTNamespace,
    FMTNodeName,
    FMTTypingDataType,
    FMTType,
    FMTRootElementName,
    MassProcessing,
    RuntimeNamespacePrefix,
    AppExitName,
    AppExitProxyName,
    AppExitMethodName,
    AppExitBAdIProxyName,
    AppExitInputParameterDataType,
    AppExitOutputParameterDataType,
    DeploymentUnit,
    AppExitESName,
    CRLLibraryName,
    CRLFunctionName,
    CRLTemplateHash,
    Offline,
  }
}
