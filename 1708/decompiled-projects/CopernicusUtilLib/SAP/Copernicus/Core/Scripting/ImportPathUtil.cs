﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Scripting.ImportPathUtil
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SAP.Copernicus.Core.Scripting
{
  public static class ImportPathUtil
  {
    public const char PathSeparatorChar = '.';
    public const char ESRPathSeparatorChar = '/';
    public const string HTTP_PREFIX = "http://";
    public const string OPT_XI_PREFIX = "xi/";

    public static void SplitIntoNSAndName(string fullName, out string ns, out string name)
    {
      ImportPathUtil.SplitIntoNSAndName(fullName, out ns, out name, '.');
    }

    public static void SplitIntoNSAndName(string fullName, out string ns, out string name, char separatorChar)
    {
      ns = (string) null;
      name = fullName;
      int length;
      if ((length = fullName.LastIndexOf(separatorChar)) < 0)
        return;
      ns = fullName.Substring(0, length);
      name = fullName.Substring(length + 1);
    }

    public static string GetFullNameForNSAndName(string ns, string name)
    {
      return ImportPathUtil.GetFullNameForNSAndName(ns, name, '.');
    }

    public static string GetFullNameForNSAndName(string ns, string name, char separatorChar)
    {
      if (ns == null)
        return name;
      StringBuilder stringBuilder = new StringBuilder(ns);
      stringBuilder.Append(separatorChar).Append(name);
      return stringBuilder.ToString();
    }

    public static string ConvertImportFromESRToInternal(string import)
    {
      return ImportPathUtil.ConvertImportFromESRToInternal(import, (string) null);
    }

    public static string ConvertImportFromESRToInternal(string import, string additionalPrefix)
    {
      if (import.StartsWith("http://"))
        import = import.Substring("http://".Length);
      int num = import.IndexOf("/");
      if (num > 0 && num + 1 < import.Length)
        import = import.Substring(num + 1);
      if (import.StartsWith("xi/"))
        import = import.Substring("xi/".Length);
      if (!string.IsNullOrEmpty(additionalPrefix))
      {
        if (additionalPrefix.StartsWith('/'.ToString()) || additionalPrefix.StartsWith('.'.ToString()))
          additionalPrefix = additionalPrefix.Substring(1);
        if (!additionalPrefix.EndsWith('/'.ToString()))
          additionalPrefix += (string) (object) '/';
        if (import.StartsWith(additionalPrefix))
          import = import.Substring(additionalPrefix.Length);
      }
      import = import.Replace('/', '.');
      return import;
    }

    public static string ConvertImportFromInternalToESR(string import)
    {
      List<string> namespaces = RepositoryDataCache.GetInstance().GetNamespaces();
      List<string> stringList = new List<string>();
      foreach (string str in namespaces)
      {
        if (str.Replace('/', '.').EndsWith(import))
          stringList.Add(str);
      }
      string message = (string) null;
      if (stringList.Count > 1)
        message = "Internal namespace does not have unique ESR namespace: " + import;
      if (message != null)
        throw new Exception(message);
      if (stringList.Count != 0)
        return stringList[0];
      return (string) null;
    }
  }
}
