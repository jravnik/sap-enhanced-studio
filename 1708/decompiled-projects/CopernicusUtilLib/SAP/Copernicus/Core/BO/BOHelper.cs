﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.BO.BOHelper
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Repository;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.BO
{
  public class BOHelper
  {
    private BOHelper()
    {
    }

    public static bool BOWasSaved(string boNS, string boName)
    {
      if (!string.IsNullOrEmpty(RepositoryDataCache.GetInstance().GetProxyNameFromBO(boNS, boName)))
        return true;
      int num = (int) CopernicusMessageBox.Show(string.Format(Resource.BOSaveMissing, (object) boName), Resource.BOCheckTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      return false;
    }
  }
}
