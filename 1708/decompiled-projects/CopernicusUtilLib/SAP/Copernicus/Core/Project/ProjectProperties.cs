﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Project.ProjectProperties
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Project
{
  [CLSCompliant(true)]
  public class ProjectProperties
  {
    internal static readonly ProjectProperties EMPTY = new ProjectProperties((IDictionary<string, string>) new Dictionary<string, string>());
    public const string RepositoryNamespaceProperty = "RepositoryNamespace";
    public const string RuntimeNamespacePrefixProperty = "RuntimeNamespacePrefix";
    public const string RepositoryRootFolderProperty = "RepositoryRootFolder";
    public const string DefaultProcessComponentProperty = "DefaultProcessComponent";
    public const string ProjectNameProperty = "Name";
    public const string BCSourceFolderInXRepProperty = "BCSourceFolderInXRep";
    public const string ProjectSourceFolderinXRepProperty = "ProjectSourceFolderinXRep";
    public const string DevelopmentPackageProperty = "DevelopmentPackage";
    public const string XRepSolutionProperty = "XRepSolution";
    public const string DeploymentUnitProperty = "DeploymentUnit";
    public const string CompilerVersionProperty = "CompilerVersion";
    private readonly IDictionary<string, string> props;

    public string RepositoryNamespace
    {
      get
      {
        return this.Get("RepositoryNamespace");
      }
    }

    public string RuntimeNamespacePrefix
    {
      get
      {
        return this.Get("RuntimeNamespacePrefix");
      }
    }

    public string RepositoryRootFolder
    {
      get
      {
        return this.Get("RepositoryRootFolder");
      }
    }

    public string DefaultProcessComponent
    {
      get
      {
        return this.Get("DefaultProcessComponent");
      }
    }

    public string ProjectName
    {
      get
      {
        return this.Get("Name");
      }
    }

    public string BCSourceFolderInXRep
    {
      get
      {
        return this.Get("BCSourceFolderInXRep");
      }
    }

    public string ProjectSourceFolderinXRep
    {
      get
      {
        return this.Get("ProjectSourceFolderinXRep");
      }
    }

    public string DevelopmentPackage
    {
      get
      {
        return this.Get("DevelopmentPackage");
      }
    }

    public string XRepSolution
    {
      get
      {
        return this.Get("XRepSolution");
      }
    }

    public string DeploymentUnit
    {
      get
      {
        return this.Get("DeploymentUnit");
      }
    }

    public string CompilerVersion
    {
      get
      {
        return this.Get("CompilerVersion");
      }
    }

    public ProjectProperties(IDictionary<string, string> initialProps)
    {
      this.props = (IDictionary<string, string>) new Dictionary<string, string>(initialProps);
    }

    public string Get(string name)
    {
      string empty;
      this.props.TryGetValue(name, out empty);
      if (empty == null)
        empty = string.Empty;
      return empty;
    }
  }
}
