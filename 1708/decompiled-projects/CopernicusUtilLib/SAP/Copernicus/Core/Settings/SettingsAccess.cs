﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Settings.SettingsAccess
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.Settings;
using Microsoft.VisualStudio.Shell.Settings;
using SAP.Copernicus.Core.Util;

namespace SAP.Copernicus.Core.Settings
{
  public class SettingsAccess
  {
    private const string collectionPath = "SAP.Copernicus.CopernicusSettings";
    private static SettingsAccess instance;
    private WritableSettingsStore settingsStore;
    private bool? showABSLSignatureAdornment;

    public static SettingsAccess Instance
    {
      get
      {
        if (SettingsAccess.instance == null)
          SettingsAccess.instance = new SettingsAccess();
        return SettingsAccess.instance;
      }
    }

    public bool ShowABSLSignatureAdornment
    {
      get
      {
        if (!this.showABSLSignatureAdornment.HasValue)
          this.showABSLSignatureAdornment = new bool?(this.settingsStore.GetBoolean("SAP.Copernicus.CopernicusSettings", "ShowABSLSignatureAdornment", true));
        return this.showABSLSignatureAdornment.Value;
      }
      set
      {
        try
        {
          this.settingsStore.SetBoolean("SAP.Copernicus.CopernicusSettings", "ShowABSLSignatureAdornment", value);
          this.showABSLSignatureAdornment = new bool?(value);
        }
        catch
        {
        }
      }
    }

    public string ContactPerson
    {
      get
      {
        try
        {
          return this.settingsStore.GetString("SAP.Copernicus.CopernicusSettings", "ContactPerson");
        }
        catch
        {
          return "";
        }
      }
      set
      {
        try
        {
          this.settingsStore.SetString("SAP.Copernicus.CopernicusSettings", "ContactPerson", value);
        }
        catch
        {
        }
      }
    }

    public string SAPDevelopmentPartner
    {
      get
      {
        try
        {
          return this.settingsStore.GetString("SAP.Copernicus.CopernicusSettings", "SAPDevelopmentPartner");
        }
        catch
        {
          return "";
        }
      }
      set
      {
        try
        {
          this.settingsStore.SetString("SAP.Copernicus.CopernicusSettings", "SAPDevelopmentPartner", value);
        }
        catch
        {
        }
      }
    }

    public string EMail
    {
      get
      {
        try
        {
          return this.settingsStore.GetString("SAP.Copernicus.CopernicusSettings", "EMail");
        }
        catch
        {
          return "";
        }
      }
      set
      {
        try
        {
          this.settingsStore.SetString("SAP.Copernicus.CopernicusSettings", "EMail", value);
        }
        catch
        {
        }
      }
    }

    private SettingsAccess()
    {
      this.settingsStore = new ShellSettingsManager(VSPackageUtil.ServiceProvider).GetWritableSettingsStore(SettingsScope.UserSettings);
      if (this.settingsStore.CollectionExists("SAP.Copernicus.CopernicusSettings"))
        return;
      this.settingsStore.CreateCollection("SAP.Copernicus.CopernicusSettings");
    }
  }
}
