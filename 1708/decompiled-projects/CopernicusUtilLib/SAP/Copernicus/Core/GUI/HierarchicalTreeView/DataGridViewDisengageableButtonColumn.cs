﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.HierarchicalTreeView.DataGridViewDisengageableButtonColumn
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.HierarchicalTreeView
{
  public class DataGridViewDisengageableButtonColumn : DataGridViewColumn
  {
    public override DataGridViewCell CellTemplate
    {
      get
      {
        return base.CellTemplate;
      }
      set
      {
        if (value != null && !value.GetType().IsAssignableFrom(typeof (DataGridViewDisengageableButtonCell)))
          throw new InvalidCastException("Must be a DataGridViewDisengageableButtonCell");
        base.CellTemplate = value;
      }
    }

    public DataGridViewDisengageableButtonColumn()
      : base((DataGridViewCell) new DataGridViewDisengageableButtonCell())
    {
    }
  }
}
