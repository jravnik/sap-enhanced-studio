﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.HierarchicalTreeView.DataGridViewDisengageableComboBoxColumn
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.HierarchicalTreeView
{
  public class DataGridViewDisengageableComboBoxColumn : DataGridViewColumn
  {
    public override DataGridViewCell CellTemplate
    {
      get
      {
        return base.CellTemplate;
      }
      set
      {
        if (value != null && !value.GetType().IsAssignableFrom(typeof (DataGridViewDisengageableComboBoxCell)))
          throw new InvalidCastException("Must be a DataGridViewDisengageableComboBoxCell");
        base.CellTemplate = value;
      }
    }

    public DataGridViewDisengageableComboBoxColumn()
      : base((DataGridViewCell) new DataGridViewDisengageableComboBoxCell())
    {
    }
  }
}
