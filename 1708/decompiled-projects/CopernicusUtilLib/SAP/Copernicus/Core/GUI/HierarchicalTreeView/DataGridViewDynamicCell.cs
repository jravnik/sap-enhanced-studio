﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.HierarchicalTreeView.DataGridViewDynamicCell
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.HierarchicalTreeView
{
  public class DataGridViewDynamicCell : DataGridViewCell
  {
    public const int TextBox = 0;
    public const int CheckBox = 1;
    public const int ComboBox = 2;
    public const int Icon = 3;
    private int cellType;
    private Type valueType;

    public override Type FormattedValueType
    {
      get
      {
        if (this.cellType == 1)
          return typeof (bool);
        return typeof (string);
      }
    }

    public override Type ValueType
    {
      get
      {
        if (this.valueType == (Type) null)
          return typeof (string);
        if (this.cellType == 1)
          return typeof (bool);
        return this.valueType;
      }
      set
      {
        this.valueType = value;
      }
    }

    public void setCellType(int newCellType)
    {
      this.cellType = newCellType;
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
      if (this.cellType == 1)
        return;
      if ((cellState & DataGridViewElementStates.Selected) != DataGridViewElementStates.None)
      {
        graphics.FillRectangle((Brush) new SolidBrush(cellStyle.SelectionBackColor), cellBounds);
        graphics.DrawString((string) formattedValue, cellStyle.Font, (Brush) new SolidBrush(cellStyle.SelectionForeColor), (RectangleF) cellBounds, StringFormat.GenericDefault);
      }
      else
      {
        graphics.FillRectangle((Brush) new SolidBrush(cellStyle.BackColor), cellBounds);
        graphics.DrawString((string) formattedValue, cellStyle.Font, (Brush) new SolidBrush(cellStyle.ForeColor), (RectangleF) cellBounds, StringFormat.GenericDefault);
      }
      this.PaintBorder(graphics, clipBounds, cellBounds, cellStyle, advancedBorderStyle);
    }
  }
}
