﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckVariableDataGridCell.CopernicusPopupClassMetaData
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.Core.GUI.CheckVariableDataGridCell
{
  public class CopernicusPopupClassMetaData
  {
    private string strFilterClassName;
    private string strFilterClassNameSpace;
    private PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC[] _CodeListEntry;

    public string FilterClasName
    {
      get
      {
        return this.strFilterClassName;
      }
      set
      {
        if (value == null || value.Equals(this.strFilterClassName))
          return;
        this.strFilterClassName = value;
      }
    }

    public string FilterClassNameSpace
    {
      get
      {
        return this.strFilterClassNameSpace;
      }
      set
      {
        if (value == null || value.Equals(this.strFilterClassNameSpace))
          return;
        this.strFilterClassNameSpace = value;
      }
    }

    public PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC[] FilterCodeListEntry
    {
      get
      {
        return this._CodeListEntry;
      }
      set
      {
        this._CodeListEntry = value;
      }
    }

    public CopernicusPopupClassMetaData(string strFilterClass, string strFilterClassNameSpace, PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC[] codelist)
    {
      this.strFilterClassName = strFilterClass;
      this.strFilterClassNameSpace = strFilterClassNameSpace;
      this._CodeListEntry = codelist;
    }
  }
}
