﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.DecisionDialogBox
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using EnvDTE;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Properties;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI
{
  public class DecisionDialogBox : Form
  {
    private IContainer components;
    private CheckBox checkBoxRemember;
    private Button buttonYes;
    private Button buttonNo;
    private Label labelMessage;
    private Panel panelMessage;
    private PictureBox pictureBoxWarning;
    private Button buttonCancel;

    protected DecisionDialogBox(string message, string caption, string decisionText)
    {
      this.InitializeComponent();
      this.Text = caption;
      this.Icon = Resource.SAPBusinessByDesignStudioIcon;
      this.labelMessage.Text = message;
      this.pictureBoxWarning.Image = (Image) Resource.Warning32x32;
      this.checkBoxRemember.Text = decisionText;
    }

    public static DialogResult CheckDecision(string message, string caption, string decisionText, string propertyName)
    {
      Property property = DTEUtil.GetDTE().get_Properties("SAP", "General").Item((object) propertyName);
      switch ((OptionsTriState) property.Value)
      {
        case OptionsTriState.Never:
          return DialogResult.No;
        case OptionsTriState.Always:
          return DialogResult.Yes;
        default:
          DecisionDialogBox decisionDialogBox = new DecisionDialogBox(message, caption, decisionText);
          int num = (int) decisionDialogBox.ShowDialog();
          DialogResult dialogResult = decisionDialogBox.DialogResult;
          property.Value = (decisionDialogBox.checkBoxRemember.Checked ? OptionsTriState.Always : OptionsTriState.Ask);
          return dialogResult;
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.checkBoxRemember = new CheckBox();
      this.buttonYes = new Button();
      this.buttonNo = new Button();
      this.labelMessage = new Label();
      this.panelMessage = new Panel();
      this.pictureBoxWarning = new PictureBox();
      this.buttonCancel = new Button();
      this.panelMessage.SuspendLayout();
      ((ISupportInitialize) this.pictureBoxWarning).BeginInit();
      this.SuspendLayout();
      this.checkBoxRemember.AutoSize = true;
      this.checkBoxRemember.Location = new Point(15, 83);
      this.checkBoxRemember.MaximumSize = new Size(320, 17);
      this.checkBoxRemember.MinimumSize = new Size(320, 17);
      this.checkBoxRemember.Name = "checkBoxRemember";
      this.checkBoxRemember.Size = new Size(320, 17);
      this.checkBoxRemember.TabIndex = 3;
      this.checkBoxRemember.Text = "decisionText";
      this.checkBoxRemember.UseVisualStyleBackColor = true;
      this.buttonYes.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonYes.DialogResult = DialogResult.Yes;
      this.buttonYes.Location = new Point(96, 106);
      this.buttonYes.Name = "buttonYes";
      this.buttonYes.Size = new Size(75, 23);
      this.buttonYes.TabIndex = 0;
      this.buttonYes.Text = "Yes";
      this.buttonYes.UseVisualStyleBackColor = true;
      this.buttonNo.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonNo.DialogResult = DialogResult.No;
      this.buttonNo.Location = new Point(177, 106);
      this.buttonNo.Name = "buttonNo";
      this.buttonNo.Size = new Size(75, 23);
      this.buttonNo.TabIndex = 1;
      this.buttonNo.Text = "No";
      this.buttonNo.UseVisualStyleBackColor = true;
      this.labelMessage.AutoSize = true;
      this.labelMessage.ImageAlign = ContentAlignment.MiddleLeft;
      this.labelMessage.Location = new Point(62, 9);
      this.labelMessage.MaximumSize = new Size(270, 50);
      this.labelMessage.MinimumSize = new Size(270, 50);
      this.labelMessage.Name = "labelMessage";
      this.labelMessage.Size = new Size(270, 50);
      this.labelMessage.TabIndex = 3;
      this.labelMessage.Text = "labelMessage";
      this.labelMessage.TextAlign = ContentAlignment.MiddleLeft;
      this.panelMessage.BackColor = SystemColors.Window;
      this.panelMessage.Controls.Add((Control) this.pictureBoxWarning);
      this.panelMessage.Controls.Add((Control) this.labelMessage);
      this.panelMessage.Location = new Point(0, 0);
      this.panelMessage.Name = "panelMessage";
      this.panelMessage.Size = new Size(345, 70);
      this.panelMessage.TabIndex = 4;
      this.pictureBoxWarning.Location = new Point(15, 9);
      this.pictureBoxWarning.Name = "pictureBoxWarning";
      this.pictureBoxWarning.Size = new Size(32, 50);
      this.pictureBoxWarning.SizeMode = PictureBoxSizeMode.CenterImage;
      this.pictureBoxWarning.TabIndex = 4;
      this.pictureBoxWarning.TabStop = false;
      this.buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonCancel.DialogResult = DialogResult.Cancel;
      this.buttonCancel.Location = new Point(257, 106);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new Size(75, 23);
      this.buttonCancel.TabIndex = 2;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(345, 141);
      this.Controls.Add((Control) this.buttonCancel);
      this.Controls.Add((Control) this.buttonNo);
      this.Controls.Add((Control) this.checkBoxRemember);
      this.Controls.Add((Control) this.buttonYes);
      this.Controls.Add((Control) this.panelMessage);
      this.MaximizeBox = false;
      this.MaximumSize = new Size(361, 179);
      this.MinimizeBox = false;
      this.MinimumSize = new Size(361, 179);
      this.Name = "DecisionDialogBox";
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "DialogBoxRememberDecision";
      this.panelMessage.ResumeLayout(false);
      this.panelMessage.PerformLayout();
      ((ISupportInitialize) this.pictureBoxWarning).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
