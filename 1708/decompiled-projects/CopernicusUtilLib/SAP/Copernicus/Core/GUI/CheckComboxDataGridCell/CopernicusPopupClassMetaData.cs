﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckComboxDataGridCell.CopernicusPopupClassMetaData
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.GUI.CheckComboxDataGridCell
{
  public class CopernicusPopupClassMetaData
  {
    private DateTime lastCodeValueRefresh = DateTime.Now;
    private PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC[] _CodeListEntry;
    private GetCodeListbyFilterName getCodeListbyFilterName;
    private string filterName;

    public PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC[] FilterCodeListEntry
    {
      get
      {
        return this._CodeListEntry;
      }
      set
      {
        this._CodeListEntry = value;
      }
    }

    public CopernicusPopupClassMetaData(PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC[] codelist, string filterName, GetCodeListbyFilterName getCodeListbyFilterName)
    {
      this._CodeListEntry = codelist;
      this.getCodeListbyFilterName = getCodeListbyFilterName;
      this.filterName = filterName;
    }

    public bool refreshCodeList()
    {
      if ((DateTime.Now - this.lastCodeValueRefresh).TotalSeconds <= 60.0)
        return false;
      this.FilterCodeListEntry = this.getCodeListbyFilterName(this.filterName);
      this.lastCodeValueRefresh = DateTime.Now;
      return true;
    }
  }
}
