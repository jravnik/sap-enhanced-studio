﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CopernicusOutputWindow
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using EnvDTE;
using EnvDTE80;
using System;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI
{
  public class CopernicusOutputWindow : IOutputWindow
  {
    private static IOutputWindowPane DUMMY_PANE = (IOutputWindowPane) new CopernicusOutputWindow.DummyOutputWindowPane();
    public static readonly string SolutionPane = Resource.OutputWindowPaneTitleSolution;
    private static IOutputWindow instance;
    private readonly DTE2 dte;

    public static IOutputWindow Instance
    {
      get
      {
        IOutputWindow outputWindow = CopernicusOutputWindow.instance;
        if (outputWindow == null)
        {
          outputWindow = (IOutputWindow) new CopernicusOutputWindow((DTE2) null);
          CopernicusOutputWindow.instance = outputWindow;
        }
        return outputWindow;
      }
      set
      {
        CopernicusOutputWindow.instance = value;
      }
    }

    public CopernicusOutputWindow(DTE2 dte)
    {
      this.dte = dte;
    }

    private OutputWindowPane GetOutputWindowPane(string name)
    {
      OutputWindowPane outputWindowPane;
      try
      {
        outputWindowPane = this.dte.ToolWindows.OutputWindow.OutputWindowPanes.Item((object) name);
      }
      catch
      {
        outputWindowPane = this.dte.ToolWindows.OutputWindow.OutputWindowPanes.Add(name);
      }
      if (outputWindowPane == null)
        throw new ArgumentException(string.Format("Unable to create Output Window Pane {0}", (object) name));
      return outputWindowPane;
    }

    public IOutputWindowPane GetOutputwindowPane(string name)
    {
      if (string.IsNullOrEmpty(name) || this.dte == null)
        return CopernicusOutputWindow.DUMMY_PANE;
      return (IOutputWindowPane) new CopernicusOutputWindow.CopernicusOutputwindowPane(this.GetOutputWindowPane(name));
    }

    private class DummyOutputWindowPane : IOutputWindowPane
    {
      public string Name
      {
        get
        {
          return "DUMMY";
        }
      }

      public void Writeln(string text)
      {
      }

      public void Activate()
      {
      }

      public void Clear()
      {
      }

      public void Write(string text)
      {
      }
    }

    private class CopernicusOutputwindowPane : IOutputWindowPane
    {
      private readonly OutputWindowPane outputWindowPane;

      public string Name
      {
        get
        {
          return this.outputWindowPane.Name;
        }
      }

      public CopernicusOutputwindowPane(OutputWindowPane outputWindowPane)
      {
        if (outputWindowPane == null)
          throw new ArgumentNullException("outputWindowPane");
        this.outputWindowPane = outputWindowPane;
      }

      public void Writeln(string text)
      {
        this.outputWindowPane.OutputString(text + "\n");
        Application.DoEvents();
      }

      public void Activate()
      {
        this.outputWindowPane.Activate();
      }

      public void Clear()
      {
        this.outputWindowPane.Clear();
      }

      public void Write(string text)
      {
        this.outputWindowPane.OutputString(text);
        Application.DoEvents();
      }
    }
  }
}
