﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Wizard.CopernicusWizardPageEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;

namespace SAP.Copernicus.Core.Wizard
{
  public class CopernicusWizardPageEventArgs : CancelEventArgs
  {
    private string _newPage;

    public string NewPage
    {
      get
      {
        return this._newPage;
      }
      set
      {
        this._newPage = value;
      }
    }
  }
}
