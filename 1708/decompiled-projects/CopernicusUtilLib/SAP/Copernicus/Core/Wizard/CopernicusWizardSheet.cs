﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Wizard.CopernicusWizardSheet
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Automation;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Wizard
{
  public class CopernicusWizardSheet : Form
  {
    protected string helpid = HELP_IDS.MSDNSTART;
    private IList _pages = (IList) new ArrayList();
    private Container components;
    private bool finishing;
    protected Button backButton;
    protected Button nextButton;
    protected Button finishButton;
    protected Button cancelButton;
    protected Panel buttonPanel;
    protected CopernicusEtchedLine etchedLine1;
    protected Panel pagePanel;
    private CopernicusWizardPage _activePage;

    public IList Pages
    {
      get
      {
        return this._pages;
      }
    }

    public CopernicusWizardSheet()
    {
      this.InitializeComponent();
      this.Icon = Resource.SAPBusinessByDesignStudioIcon;
    }

    public CopernicusWizardSheet(string HELPID)
    {
      this.InitializeComponent();
      this.Icon = Resource.SAPBusinessByDesignStudioIcon;
      this.helpid = HELPID;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.backButton = new Button();
      this.nextButton = new Button();
      this.finishButton = new Button();
      this.cancelButton = new Button();
      this.buttonPanel = new Panel();
      this.pagePanel = new Panel();
      this.etchedLine1 = new CopernicusEtchedLine();
      this.buttonPanel.SuspendLayout();
      this.SuspendLayout();
      this.backButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.backButton.FlatStyle = FlatStyle.System;
      this.backButton.Location = new Point(46, 8);
      this.backButton.Name = "backButton";
      this.backButton.Size = new Size(75, 23);
      this.backButton.TabIndex = 0;
      this.backButton.Text = "< &Back";
      this.backButton.Click += new EventHandler(this.backButton_Click);
      this.nextButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.nextButton.FlatStyle = FlatStyle.System;
      this.nextButton.Location = new Point((int) sbyte.MaxValue, 8);
      this.nextButton.Name = "nextButton";
      this.nextButton.Size = new Size(75, 23);
      this.nextButton.TabIndex = 1;
      this.nextButton.Text = "&Next >";
      this.nextButton.Click += new EventHandler(this.nextButton_Click);
      this.finishButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.finishButton.FlatStyle = FlatStyle.System;
      this.finishButton.Location = new Point(208, 8);
      this.finishButton.Name = "finishButton";
      this.finishButton.Size = new Size(75, 23);
      this.finishButton.TabIndex = 2;
      this.finishButton.Text = "&Finish";
      this.finishButton.Click += new EventHandler(this.finishButton_Click);
      this.cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.cancelButton.FlatStyle = FlatStyle.System;
      this.cancelButton.Location = new Point(296, 8);
      this.cancelButton.Name = "cancelButton";
      this.cancelButton.Size = new Size(75, 23);
      this.cancelButton.TabIndex = 3;
      this.cancelButton.Text = "Cancel";
      this.cancelButton.Click += new EventHandler(this.cancelButton_Click);
      this.buttonPanel.Controls.Add((Control) this.finishButton);
      this.buttonPanel.Controls.Add((Control) this.etchedLine1);
      this.buttonPanel.Controls.Add((Control) this.cancelButton);
      this.buttonPanel.Controls.Add((Control) this.backButton);
      this.buttonPanel.Controls.Add((Control) this.nextButton);
      this.buttonPanel.Dock = DockStyle.Bottom;
      this.buttonPanel.Location = new Point(0, 101);
      this.buttonPanel.Name = "buttonPanel";
      this.buttonPanel.Size = new Size(384, 40);
      this.buttonPanel.TabIndex = 4;
      this.pagePanel.AutoScroll = true;
      this.pagePanel.Dock = DockStyle.Fill;
      this.pagePanel.Location = new Point(0, 0);
      this.pagePanel.Name = "pagePanel";
      this.pagePanel.Size = new Size(384, 101);
      this.pagePanel.TabIndex = 5;
      this.etchedLine1.AutoScroll = true;
      this.etchedLine1.AutoSize = true;
      this.etchedLine1.Dock = DockStyle.Top;
      this.etchedLine1.Edge = EtchEdge.Top;
      this.etchedLine1.Location = new Point(0, 0);
      this.etchedLine1.Name = "etchedLine1";
      this.etchedLine1.Size = new Size(384, 0);
      this.etchedLine1.TabIndex = 4;
      this.HelpRequested += new HelpEventHandler(this.OnHelpRequested);
      this.AutoScaleBaseSize = new Size(5, 13);
      this.AutoScroll = true;
      this.AutoSize = true;
      this.ClientSize = new Size(384, 141);
      this.Controls.Add((Control) this.pagePanel);
      this.Controls.Add((Control) this.buttonPanel);
      this.MinimizeBox = false;
      this.Name = "CopernicusWizardSheet";
      this.Text = "Copernicus Wizard";
      this.Closing += new CancelEventHandler(this.WizardSheet_Closing);
      this.Load += new EventHandler(this.WizardSheet_Load);
      this.buttonPanel.ResumeLayout(false);
      this.buttonPanel.PerformLayout();
      this.ResumeLayout(false);
    }

    private void WizardSheet_Load(object sender, EventArgs e)
    {
      if (this._pages.Count == 0)
        return;
      this.ResizeToFit();
      this.SetActivePage(0);
    }

    private void ResizeToFit()
    {
      Size size1 = new Size(this.buttonPanel.Width, 0);
      foreach (CopernicusWizardPage page in (IEnumerable) this._pages)
      {
        if (page.Width > size1.Width)
          size1.Width = page.Width;
        if (page.Height > size1.Height)
          size1.Height = page.Height;
      }
      foreach (Control page in (IEnumerable) this._pages)
        page.Size = size1;
      Size size2 = this.Size - this.pagePanel.Size;
      this.Size = size1 + size2;
    }

    private int GetActiveIndex()
    {
      CopernicusWizardPage activePage = this.GetActivePage();
      for (int index = 0; index < this._pages.Count; ++index)
      {
        if (activePage == this._pages[index])
          return index;
      }
      return -1;
    }

    private CopernicusWizardPage GetActivePage()
    {
      return this._activePage;
    }

    public void SetActivePage(int pageIndex)
    {
      if (pageIndex < 0 || pageIndex >= this._pages.Count)
        throw new ArgumentOutOfRangeException("pageIndex");
      this.SetActivePage((CopernicusWizardPage) this._pages[pageIndex]);
    }

    private CopernicusWizardPage FindPage(string pageName)
    {
      foreach (CopernicusWizardPage page in (IEnumerable) this._pages)
      {
        if (page.Name == pageName)
          return page;
      }
      return (CopernicusWizardPage) null;
    }

    private void SetActivePage(string newPageName)
    {
      CopernicusWizardPage page = this.FindPage(newPageName);
      if (page == null)
        throw new Exception(string.Format("Can't find page named {0}", (object) newPageName));
      this.SetActivePage(page);
    }

    private void SetActivePage(CopernicusWizardPage newPage)
    {
      CopernicusWizardPage activePage = this._activePage;
      if (!this.pagePanel.Controls.Contains((Control) newPage))
        this.pagePanel.Controls.Add((Control) newPage);
      newPage.Visible = true;
      this._activePage = newPage;
      CancelEventArgs e = new CancelEventArgs();
      newPage.OnSetActive(e);
      if (e.Cancel)
      {
        newPage.Visible = false;
        this._activePage = activePage;
      }
      foreach (CopernicusWizardPage page in (IEnumerable) this._pages)
      {
        if (page != this._activePage)
          page.Visible = false;
      }
    }

    internal void SetWizardButtons(WizardPageType type)
    {
      switch (type)
      {
        case WizardPageType.First:
          this.backButton.Enabled = false;
          this.finishButton.Enabled = false;
          this.cancelButton.Enabled = true;
          this.nextButton.Enabled = true;
          this.AcceptButton = (IButtonControl) this.nextButton;
          break;
        case WizardPageType.Middle:
          this.backButton.Enabled = true;
          this.finishButton.Enabled = false;
          this.cancelButton.Enabled = true;
          this.nextButton.Enabled = true;
          this.AcceptButton = (IButtonControl) this.nextButton;
          break;
        case WizardPageType.Last:
          this.backButton.Enabled = true;
          this.finishButton.Enabled = true;
          this.cancelButton.Enabled = true;
          this.nextButton.Enabled = false;
          this.AcceptButton = (IButtonControl) this.finishButton;
          break;
        case WizardPageType.FirstAndLast:
          this.backButton.Visible = false;
          this.nextButton.Visible = false;
          this.finishButton.Enabled = true;
          this.finishButton.Text = "OK";
          this.cancelButton.Enabled = true;
          this.AcceptButton = (IButtonControl) this.finishButton;
          break;
      }
    }

    private CopernicusWizardPageEventArgs PreChangePage(int delta)
    {
      int activeIndex = this.GetActiveIndex();
      int index = activeIndex + delta;
      if (index < 0 || index >= this._pages.Count)
        index = activeIndex;
      CopernicusWizardPage page = (CopernicusWizardPage) this._pages[index];
      CopernicusWizardPageEventArgs wizardPageEventArgs = new CopernicusWizardPageEventArgs();
      wizardPageEventArgs.NewPage = page.Name;
      wizardPageEventArgs.Cancel = false;
      return wizardPageEventArgs;
    }

    private void PostChangePage(CopernicusWizardPageEventArgs e)
    {
      if (e.Cancel)
        return;
      this.SetActivePage(e.NewPage);
    }

    private void nextButton_Click(object sender, EventArgs e)
    {
      CopernicusWizardPageEventArgs e1 = this.PreChangePage(1);
      this._activePage.OnWizardNext(e1);
      this.PostChangePage(e1);
    }

    private void backButton_Click(object sender, EventArgs e)
    {
      CopernicusWizardPageEventArgs e1 = this.PreChangePage(-1);
      this._activePage.OnWizardBack(e1);
      this.PostChangePage(e1);
    }

    public void CloseWithoutSafetyQuestion()
    {
      this.finishing = true;
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void finishButton_Click(object sender, EventArgs e)
    {
      CancelEventArgs e1 = new CancelEventArgs();
      this._activePage.OnWizardFinish(e1);
      if (e1.Cancel)
        return;
      this.finishing = true;
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    internal void PressButton(WizardButtons buttons)
    {
      if ((buttons & WizardButtons.Finish) == WizardButtons.Finish)
        this.finishButton.PerformClick();
      else if ((buttons & WizardButtons.Next) == WizardButtons.Next)
      {
        this.nextButton.PerformClick();
      }
      else
      {
        if ((buttons & WizardButtons.Back) != WizardButtons.Back)
          return;
        this.backButton.PerformClick();
      }
    }

    internal void EnableCancelButton(bool enableCancelButton)
    {
      this.cancelButton.Enabled = enableCancelButton;
    }

    internal void EnableBackButton(bool enableBackButton)
    {
      this.backButton.Enabled = enableBackButton;
    }

    internal void EnableNextButton(bool enableNextButton)
    {
      this.nextButton.Enabled = enableNextButton;
    }

    internal void EnableFinishButton(bool enableFinishButton)
    {
      this.finishButton.Enabled = enableFinishButton;
    }

    protected virtual void cancelButton_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    protected virtual void WizardSheet_Closing(object sender, CancelEventArgs e)
    {
      if (this.finishing || MessageBox.Show(Resource.MsgCancel, Resource.MsgBYD, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
      {
        if (!this.cancelButton.Enabled)
        {
          e.Cancel = true;
        }
        else
        {
          if (this.finishButton.Enabled)
            return;
          this.OnQueryCancel(e);
        }
      }
      else
      {
        this.finishing = false;
        e.Cancel = true;
      }
    }

    protected virtual void OnHelpRequested(object sender, HelpEventArgs hlpevent)
    {
      HelpUtil.DisplayF1Help(this.helpid);
    }

    protected virtual void OnQueryCancel(CancelEventArgs e)
    {
      this._activePage.OnQueryCancel(e);
    }
  }
}
