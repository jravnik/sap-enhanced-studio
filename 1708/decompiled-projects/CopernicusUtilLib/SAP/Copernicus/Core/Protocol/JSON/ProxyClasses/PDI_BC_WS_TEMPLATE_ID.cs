﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_WS_TEMPLATE_ID
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_WS_TEMPLATE_ID : AbstractRemoteFunction<PDI_BC_WS_TEMPLATE_ID.ImportingType, PDI_BC_WS_TEMPLATE_ID.ExportingType, PDI_BC_WS_TEMPLATE_ID.ChangingType, PDI_BC_WS_TEMPLATE_ID.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01151C1EE096BEAEE6AC7A5BA6";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_NAMESPACE;
      [DataMember]
      public string IV_TEMPLATE_ID;
      [DataMember]
      public string IV_TEMPLATE_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_WS_ID;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
