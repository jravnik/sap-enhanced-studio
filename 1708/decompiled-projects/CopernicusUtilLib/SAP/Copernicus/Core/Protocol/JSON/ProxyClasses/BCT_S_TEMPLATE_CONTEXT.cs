﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.BCT_S_TEMPLATE_CONTEXT
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class BCT_S_TEMPLATE_CONTEXT
  {
    [DataMember]
    public string ELEMENT_ID;
    [DataMember]
    public string CONTENT_ID;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string BO_NAMESPACE;
    [DataMember]
    public string BO_NAME;
    [DataMember]
    public string NODE;
    [DataMember]
    public string OPERATION;
    [DataMember]
    public string IS_WORKITEM;
    [DataMember]
    public string IS_INSCOPE;
  }
}
