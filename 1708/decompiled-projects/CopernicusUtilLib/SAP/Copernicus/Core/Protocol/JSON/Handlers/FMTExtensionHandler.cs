﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.FMTExtensionHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class FMTExtensionHandler : JSONHandler
  {
    public bool ListFormTmplInfo(string IV_PATH, out PDI_EXT_S_FORM_TMPL_INFO[] ET_FORM_TMPL)
    {
      PDI_EXT_LIST_FORM_TMPL pdiExtListFormTmpl = new PDI_EXT_LIST_FORM_TMPL();
      ET_FORM_TMPL = (PDI_EXT_S_FORM_TMPL_INFO[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiExtListFormTmpl.Importing = new PDI_EXT_LIST_FORM_TMPL.ImportingType();
        pdiExtListFormTmpl.Importing.IV_PATH = IV_PATH;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiExtListFormTmpl, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiExtListFormTmpl, false, false);
        if (pdiExtListFormTmpl.Exporting != null)
        {
          ET_FORM_TMPL = pdiExtListFormTmpl.Exporting.ET_FORM_TMPL;
          return true;
        }
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      return false;
    }

    public List<SyntaxErrorInfo> CheckScriptSyntax(string nsName, string fmtName, string snippetPath, string partnerNS)
    {
      List<SyntaxErrorInfo> syntaxErrorInfoList = new List<SyntaxErrorInfo>();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_FMTEXT_SYNTAX_CHECK fmtextSyntaxCheck = new PDI_RI_FMTEXT_SYNTAX_CHECK();
        fmtextSyntaxCheck.Importing = new PDI_RI_FMTEXT_SYNTAX_CHECK.ImportingType();
        fmtextSyntaxCheck.Importing.IV_ESR_NAMESPACE = nsName;
        fmtextSyntaxCheck.Importing.IV_ESR_FMT_NAME = fmtName;
        fmtextSyntaxCheck.Importing.IV_FMT_PRXNAME = (string) null;
        fmtextSyntaxCheck.Importing.IV_PARTNER_NAMESPACE = partnerNS;
        fmtextSyntaxCheck.Importing.IV_SNIPPET_PATH = snippetPath;
        fmtextSyntaxCheck.Importing.IV_XREP_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        jsonClient.callFunctionModule((SAPFunctionModule) fmtextSyntaxCheck, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) fmtextSyntaxCheck, false, false);
        syntaxErrorInfoList = JSONParamConverter.ET_MESSAGES2SyntaxErrorInfoList(fmtextSyntaxCheck.Exporting.ET_MESSAGES);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return syntaxErrorInfoList;
    }
  }
}
