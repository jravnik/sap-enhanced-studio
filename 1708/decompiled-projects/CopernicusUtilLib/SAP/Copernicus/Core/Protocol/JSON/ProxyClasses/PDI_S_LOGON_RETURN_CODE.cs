﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_LOGON_RETURN_CODE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_LOGON_RETURN_CODE
  {
    [DataMember]
    public string SEVERITY;
    [DataMember]
    public string ERROR_TYPE;
    [DataMember]
    public string INSTALLED_BYD_RELEASE;
    [DataMember]
    public PDI_S_LOGON_PATCH_LEVEL[] REQUIRED_PATCH_LEVEL;
    [DataMember]
    public PDI_S_LOGON_BUILD_NUMBER[] REQUIRED_UI_BUILD_NUMBER;

    public class SEVERITY_CODE
    {
      public const string Success = "S";
      public const string Info = "I";
      public const string Warning = "W";
      public const string Error = "E";
    }

    public class ERROR_TYPE_CODE
    {
      public const string None = "NONE";
      public const string NoReleaseInfo = "NO_RELEASE_INFO";
      public const string InvalidRelease = "INVALID_RELEASE";
      public const string InvalidPatchLevel = "INVALID_PATCH_LEVEL";
      public const string InvalidBuildNumber = "INVALID_BUILD_NUMBER";
      public const string InvalidPatchLevelAndBuildNumber = "INVALID_PATCH_LEVEL_AND_BUILD_NUMBER";
      public const string CheckNotPossible = "CHECK_NOT_POSSIBLE";
    }
  }
}
