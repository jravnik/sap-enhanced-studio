﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.SODcheckHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class SODcheckHandler : JSONHandler
  {
    public bool IsSODactive()
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_UTIL_SOD_CHECK pdiUtilSodCheck = new PDI_UTIL_SOD_CHECK();
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiUtilSodCheck, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (pdiUtilSodCheck.Exporting != null)
      {
        if (pdiUtilSodCheck.Exporting.EV_SUCCESS == "")
          this.reportServerSideProtocolException((SAPFunctionModule) pdiUtilSodCheck, false, false);
        else
          return pdiUtilSodCheck.Exporting.EV_SOD_ACTIVE == "X";
      }
      return false;
    }
  }
}
