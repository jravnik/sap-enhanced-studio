﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_USAGE_UPDATE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_USAGE_UPDATE : AbstractRemoteFunction<PDI_RI_USAGE_UPDATE.ImportingType, PDI_RI_USAGE_UPDATE.ExportingType, PDI_RI_USAGE_UPDATE.ChangingType, PDI_RI_USAGE_UPDATE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0135861ED0B0B575563E211C23";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public List<PDI_S_USG_DATA> IT_FILE_ATTR;
      [DataMember]
      public string IV_SOLUTION_NAME;
      [DataMember]
      public string IV_FORCE_UPLOAD;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
