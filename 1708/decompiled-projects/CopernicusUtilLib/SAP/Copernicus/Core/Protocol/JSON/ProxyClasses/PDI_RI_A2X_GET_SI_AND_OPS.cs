﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_A2X_GET_SI_AND_OPS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_A2X_GET_SI_AND_OPS : AbstractRemoteFunction<PDI_RI_A2X_GET_SI_AND_OPS.ImportingType, PDI_RI_A2X_GET_SI_AND_OPS.ExportingType, PDI_RI_A2X_GET_SI_AND_OPS.ChangingType, PDI_RI_A2X_GET_SI_AND_OPS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19CE1F8DE2F460CA2";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_NAMESPACE;
      [DataMember]
      public string IV_EXCLUDE_A2A;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_T_SI_AND_OPS[] ET_SI_LIST;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
