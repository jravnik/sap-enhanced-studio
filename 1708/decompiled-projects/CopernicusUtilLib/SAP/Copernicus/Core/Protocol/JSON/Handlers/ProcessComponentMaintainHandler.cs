﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.ProcessComponentMaintainHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class ProcessComponentMaintainHandler : JSONHandler
  {
    public string MaintainProcessComponent(string componentName, string duName, bool deletePC, string pcName, out PDI_PC_MAINTAIN.PDI_RI_T_MESSAGES[] messages, out string success)
    {
      PDI_PC_MAINTAIN pdiPcMaintain = new PDI_PC_MAINTAIN();
      pdiPcMaintain.Importing = new PDI_PC_MAINTAIN.ImportingType();
      pdiPcMaintain.Importing.IV_COMPONENT = componentName;
      pdiPcMaintain.Importing.IV_DU_NAME = duName;
      pdiPcMaintain.Importing.IV_DEL = deletePC;
      pdiPcMaintain.Importing.IV_PC_NAME = pcName;
      messages = (PDI_PC_MAINTAIN.PDI_RI_T_MESSAGES[]) null;
      success = "";
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiPcMaintain, false, false, false);
      }
      catch (ProtocolException ex)
      {
        return (string) null;
      }
      if (pdiPcMaintain.Exporting.EV_SUCCESS == "")
      {
        messages = pdiPcMaintain.Exporting.ET_MESSAGES;
        return (string) null;
      }
      success = pdiPcMaintain.Exporting.EV_SUCCESS;
      return pdiPcMaintain.Exporting.EV_PC_NAME;
    }
  }
}
