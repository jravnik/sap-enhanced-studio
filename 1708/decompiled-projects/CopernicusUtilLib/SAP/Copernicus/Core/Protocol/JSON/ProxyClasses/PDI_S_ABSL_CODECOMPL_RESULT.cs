﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_ABSL_CODECOMPL_RESULT
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_ABSL_CODECOMPL_RESULT
  {
    [DataMember]
    public int ICON_INDEX;
    [DataMember]
    public string DISPLAY_NAME;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string NAME;
  }
}
