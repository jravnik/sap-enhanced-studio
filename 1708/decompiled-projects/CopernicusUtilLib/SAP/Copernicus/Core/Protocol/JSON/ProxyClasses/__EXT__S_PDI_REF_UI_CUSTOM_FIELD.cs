﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.__EXT__S_PDI_REF_UI_CUSTOM_FIELD
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class __EXT__S_PDI_REF_UI_CUSTOM_FIELD
  {
    [DataMember]
    public string SELECTED;
    [DataMember]
    public string BO_NAME;
    [DataMember]
    public string BO_NAMESPACE;
    [DataMember]
    public string BO_NODE_NAME;
    [DataMember]
    public string BO_NODE_NAMESPACE;
    [DataMember]
    public string FIELD_NAME;
    [DataMember]
    public string FIELD_TYPE;
    [DataMember]
    public string FIELD_LENGTH;
    [DataMember]
    public string FIELD_DECIMALS;
    [DataMember]
    public string FIELD_TYPE_DESCRIPTION;
    [DataMember]
    public string FIELD_LABEL;
    [DataMember]
    public string ERROR_OCCURRED;
    [DataMember]
    public string MESSAGE_TEXT;
  }
}
