﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_QUERY_DUMPS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_QUERY_DUMPS : AbstractRemoteFunction<PDI_ABSL_QUERY_DUMPS.ImportingType, PDI_ABSL_QUERY_DUMPS.ExportingType, PDI_ABSL_QUERY_DUMPS.ChangingType, PDI_ABSL_QUERY_DUMPS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0383AC1EE2A1FADE3C1B1513B9";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_DATE_FROM;
      [DataMember]
      public string IV_DATE_TO;
      [DataMember]
      public string IV_TIME_FROM;
      [DataMember]
      public string IV_TIME_TO;
      [DataMember]
      public string IV_SOLUTION;
      [DataMember]
      public string IV_UNAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_ABSL_S_DUMP[] ET_DUMP_LIST;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
