﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_NODE_EXT_S_XFP_FILE_DETAILS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_NODE_EXT_S_XFP_FILE_DETAILS
  {
    [DataMember]
    public string FIELD_NAME;
    [DataMember]
    public string FIELD_TYPE;
    [DataMember]
    public string FIELD_TYPE_CAT;
    [DataMember]
    public string ESR_ND_NAME;
    [DataMember]
    public string XBO_NAME;
    [DataMember]
    public string PRX_BO_NAME;
    [DataMember]
    public string PRX_ND_NAME;
    [DataMember]
    public string MAX_BO_PRX_NAME;
    [DataMember]
    public string PX_SCENARIO_NAME;
    [DataMember]
    public string LAST_ACTIVATE;
  }
}
