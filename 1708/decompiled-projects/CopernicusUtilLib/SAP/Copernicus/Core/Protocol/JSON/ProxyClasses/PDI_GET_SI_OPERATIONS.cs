﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_SI_OPERATIONS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_GET_SI_OPERATIONS : AbstractRemoteFunction<PDI_GET_SI_OPERATIONS.ImportingType, PDI_GET_SI_OPERATIONS.ExportingType, PDI_GET_SI_OPERATIONS.ChangingType, PDI_GET_SI_OPERATIONS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0520391ED389A4E3AE17AFC6F4";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SERVICE_INTERFACE_PRX_NAME;
      [DataMember]
      public string IV_SERVICE_INTERFACE_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public SERVICE_INTERFACE_OPERATIONS[] ET_OPERATION;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
