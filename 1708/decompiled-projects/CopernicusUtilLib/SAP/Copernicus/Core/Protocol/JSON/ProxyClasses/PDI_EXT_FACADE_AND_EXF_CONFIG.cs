﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_FACADE_AND_EXF_CONFIG
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_FACADE_AND_EXF_CONFIG : AbstractRemoteFunction<PDI_EXT_FACADE_AND_EXF_CONFIG.ImportingType, PDI_EXT_FACADE_AND_EXF_CONFIG.ExportingType, PDI_EXT_FACADE_AND_EXF_CONFIG.ChangingType, PDI_EXT_FACADE_AND_EXF_CONFIG.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DEFB1A948793992B61E";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ESR_BO_NAME;
      [DataMember]
      public string IV_ESR_NAMESPACE;
      [DataMember]
      public string IV_XREP_PATH;
      [DataMember]
      public PDI_EXT_S_EXT_FIELDS[] IT_NODES;
      [DataMember]
      public PDI_EXT_S_EXT_FIELD_SCENARIOS[] IT_EXT_FIELD_SCENARIOS;
      [DataMember]
      public PDI_EXT_S_MESSAGES[] IT_EXT_MESSAGES;
      [DataMember]
      public string IV_SESSION_ID;
      [DataMember]
      public bool IV_ABSL;
      [DataMember]
      public bool IV_MAINTENANCE_CHECK;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
