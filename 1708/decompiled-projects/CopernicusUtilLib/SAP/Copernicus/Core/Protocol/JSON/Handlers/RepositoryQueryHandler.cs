﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.RepositoryQueryHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class RepositoryQueryHandler : JSONHandler
  {
    public object lockObject = (object) "";
    public object fileLockObject = (object) "";
    private static HashSet<string> FULL_NAMESPACES_SET = new HashSet<string>();
    private static readonly SESF_SELECTION_PARAMETER ACTIVE_VERSION_ID_PARAMETER = new SESF_SELECTION_PARAMETER() { ATTRIBUTE_NAME = "KEY-VERSION_ID", LOW = "0", OPTION = "EQ", SIGN = "I" };
    private static readonly SESF_SELECTION_PARAMETER INACTIVE_VERSION_ID_PARAMETER = new SESF_SELECTION_PARAMETER() { ATTRIBUTE_NAME = "KEY-VERSION_ID", LOW = "1", OPTION = "EQ", SIGN = "I" };
    private static readonly Encoding encUTF8 = Encoding.UTF8;
    private JSONClient jsonClient;

    private static HashSet<string> NAMESPACES_SET
    {
      get
      {
        if (RepositoryQueryHandler.FULL_NAMESPACES_SET == null || RepositoryQueryHandler.FULL_NAMESPACES_SET.Count == 0)
          RepositoryQueryHandler.FULL_NAMESPACES_SET = new HashSet<string>((IEnumerable<string>) RepositoryQueryHandler.ExecuteQueryForSAPNamespaces());
        return RepositoryQueryHandler.FULL_NAMESPACES_SET;
      }
    }

    private static SESF_SELECTION_PARAMETER[] AP_BAdI_PARAMETER
    {
      get
      {
        HashSet<string> namespacesSet = RepositoryQueryHandler.NAMESPACES_SET;
        SESF_SELECTION_PARAMETER[] selectionParameterArray = new SESF_SELECTION_PARAMETER[namespacesSet.Count + 1];
        int num = 0;
        foreach (string str in namespacesSet)
          selectionParameterArray[num++] = new SESF_SELECTION_PARAMETER()
          {
            ATTRIBUTE_NAME = "NAME_KEY-NAMESPACE",
            LOW = str,
            OPTION = "EQ",
            SIGN = "I"
          };
        return selectionParameterArray;
      }
    }

    private static SESF_SELECTION_PARAMETER[] AP_BO_PARAMETER
    {
      get
      {
        HashSet<string> namespacesSet = RepositoryQueryHandler.NAMESPACES_SET;
        SESF_SELECTION_PARAMETER[] selectionParameterArray1 = new SESF_SELECTION_PARAMETER[namespacesSet.Count + 1];
        int num1 = 0;
        SESF_SELECTION_PARAMETER[] selectionParameterArray2 = selectionParameterArray1;
        int index = num1;
        int num2 = 1;
        int num3 = index + num2;
        SESF_SELECTION_PARAMETER versionIdParameter = RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER;
        selectionParameterArray2[index] = versionIdParameter;
        foreach (string str in namespacesSet)
          selectionParameterArray1[num3++] = new SESF_SELECTION_PARAMETER()
          {
            ATTRIBUTE_NAME = "NAME_KEY-NAMESPACE",
            LOW = str,
            OPTION = "EQ",
            SIGN = "I"
          };
        return selectionParameterArray1;
      }
    }

    private static SESF_SELECTION_PARAMETER[] AP_DT_PARAMETER
    {
      get
      {
        HashSet<string> namespacesSet = RepositoryQueryHandler.NAMESPACES_SET;
        SESF_SELECTION_PARAMETER[] selectionParameterArray1 = new SESF_SELECTION_PARAMETER[namespacesSet.Count + 1];
        int num1 = 0;
        SESF_SELECTION_PARAMETER[] selectionParameterArray2 = selectionParameterArray1;
        int index = num1;
        int num2 = 1;
        int num3 = index + num2;
        SESF_SELECTION_PARAMETER versionIdParameter = RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER;
        selectionParameterArray2[index] = versionIdParameter;
        foreach (string str in namespacesSet)
          selectionParameterArray1[num3++] = new SESF_SELECTION_PARAMETER()
          {
            ATTRIBUTE_NAME = "NAME_KEY-NAMESPACE",
            LOW = str,
            OPTION = "EQ",
            SIGN = "I"
          };
        return selectionParameterArray1;
      }
    }

    private static SESF_SELECTION_PARAMETER[] AP_MT_PARAMETER
    {
      get
      {
        HashSet<string> namespacesSet = RepositoryQueryHandler.NAMESPACES_SET;
        SESF_SELECTION_PARAMETER[] selectionParameterArray1 = new SESF_SELECTION_PARAMETER[namespacesSet.Count + 1];
        int num1 = 0;
        SESF_SELECTION_PARAMETER[] selectionParameterArray2 = selectionParameterArray1;
        int index = num1;
        int num2 = 1;
        int num3 = index + num2;
        SESF_SELECTION_PARAMETER versionIdParameter = RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER;
        selectionParameterArray2[index] = versionIdParameter;
        foreach (string str in namespacesSet)
          selectionParameterArray1[num3++] = new SESF_SELECTION_PARAMETER()
          {
            ATTRIBUTE_NAME = "NAME_KEY-NAMESPACE",
            LOW = str,
            OPTION = "EQ",
            SIGN = "I"
          };
        return selectionParameterArray1;
      }
    }

    public event RepositoryQueryHandler.BORepositoryDataLoadedEventHandler BORepositoryDataLoadedEvent;

    public event RepositoryQueryHandler.BAdIRepositoryDataLoadedEventHandler BAdIRepositoryDataLoadedEvent;

    public event RepositoryQueryHandler.DTRepositoryDataLoadedEventHandler DTRepositoryDataLoadedEvent;

    public event RepositoryQueryHandler.LibRepositoryDataLoadedEventHandler LibRepositoryDataLoadedEvent;

    public event RepositoryQueryHandler.MTRepositoryDataLoadedEventHandler MTRepositoryDataLoadedEvent;

    internal void FireBORepositoryDataLoadedEvent(string resultXml)
    {
      if (this.BORepositoryDataLoadedEvent == null)
        return;
      this.BORepositoryDataLoadedEvent(resultXml);
    }

    internal void FireBAdIRepositoryDataLoadedEvent(string resultXML)
    {
      if (this.BAdIRepositoryDataLoadedEvent == null)
        return;
      this.BAdIRepositoryDataLoadedEvent(resultXML);
    }

    internal void FireDTRepositoryDataLoadedEvent(string resultXml)
    {
      if (this.DTRepositoryDataLoadedEvent == null)
        return;
      this.DTRepositoryDataLoadedEvent(resultXml);
    }

    internal void FireLibRepositoryDataLoadedEvent(string resultXml)
    {
      if (this.LibRepositoryDataLoadedEvent == null)
        return;
      this.LibRepositoryDataLoadedEvent(resultXml);
    }

    internal void FireMTRepositoryDataLoadedEvent(string resultXml)
    {
      if (this.MTRepositoryDataLoadedEvent == null)
        return;
      this.MTRepositoryDataLoadedEvent(resultXml);
    }

    public HashSet<string> RefreshSAPNamespaces()
    {
      RepositoryQueryHandler.FULL_NAMESPACES_SET = new HashSet<string>((IEnumerable<string>) RepositoryQueryHandler.ExecuteQueryForSAPNamespaces());
      return RepositoryQueryHandler.FULL_NAMESPACES_SET;
    }

    public void ResetProxy()
    {
      Client.getInstance().reset();
    }

    private void writeCacheFile(string fullLocalPath, string xmlString)
    {
      lock (this.fileLockObject)
      {
        StreamWriter streamWriter = new StreamWriter(fullLocalPath);
        streamWriter.Write(xmlString);
        streamWriter.Flush();
        streamWriter.Close();
      }
    }

    private string getCacheFilePath(RepositoryQueryHandler.cacheFiles cacheFile)
    {
      SAP.Copernicus.Core.Protocol.Connection instance = SAP.Copernicus.Core.Protocol.Connection.getInstance();
      string host = new Uri(instance.GetJsonHttpURL()).Host;
      DirectoryInfo forConnectionName = MDRSHandler.GetCacheRootForConnectionName(instance.GetConnectionName(), instance.UsePSM);
      if (cacheFile == RepositoryQueryHandler.cacheFiles.ABSL)
        return forConnectionName.FullName + "\\abslLibCache.xml";
      if (cacheFile == RepositoryQueryHandler.cacheFiles.BO)
        return forConnectionName.FullName + "\\boCache.xml";
      if (cacheFile == RepositoryQueryHandler.cacheFiles.DT)
        return forConnectionName.FullName + "\\dtCache.xml";
      if (cacheFile == RepositoryQueryHandler.cacheFiles.QD)
        return forConnectionName.FullName + "\\qdLibCache.xml";
      if (cacheFile == RepositoryQueryHandler.cacheFiles.MT)
        return forConnectionName.FullName + "\\mtLibCache.xml";
      if (cacheFile == RepositoryQueryHandler.cacheFiles.BAdI)
        return forConnectionName.FullName + "\\badiCache.xml";
      return (string) null;
    }

    private string ExecuteBAdIQueryForSAPNamespaces(SESF_SELECTION_PARAMETER[] selectionParameter)
    {
      string xmlString = string.Empty;
      string cacheFilePath = this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.BAdI);
      if (!File.Exists(cacheFilePath))
      {
        xmlString = this.ExecuteBAdIQueryInternal(selectionParameter);
        this.writeCacheFile(cacheFilePath, xmlString);
      }
      else
      {
        lock (this.fileLockObject)
        {
          StreamReader streamReader = new StreamReader(cacheFilePath);
          xmlString = streamReader.ReadToEnd();
          streamReader.Close();
        }
        new Thread(new ParameterizedThreadStart(this.ExecuteBAdIQueryThreaded)).Start((object) selectionParameter);
      }
      return xmlString;
    }

    private void ExecuteBAdIQueryThreaded(object data)
    {
      string jsonHttpUrl = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL();
      SESF_SELECTION_PARAMETER[] selectionParameter = (SESF_SELECTION_PARAMETER[]) data;
      string cacheFilePath = this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.BAdI);
      string str = this.ExecuteBAdIQueryInternal(selectionParameter);
      if (str != null && str.Length > 0)
        this.writeCacheFile(cacheFilePath, str);
      if (!jsonHttpUrl.Equals(SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL()))
        return;
      this.FireBAdIRepositoryDataLoadedEvent(str);
    }

    private string ExecuteBAdIQueryInternal(SESF_SELECTION_PARAMETER[] selectionParameter)
    {
      string empty = string.Empty;
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Query for BAdIs");
      try
      {
        PDI_AE_QUERY_BADI pdiAeQueryBadi = new PDI_AE_QUERY_BADI();
        pdiAeQueryBadi.Importing = new PDI_AE_QUERY_BADI.ImportingType();
        this.jsonClient = Client.getInstance().getJSONClient(false);
        this.jsonClient.callFunctionModule((SAPFunctionModule) pdiAeQueryBadi, false, false, false);
        string evResultSet = pdiAeQueryBadi.Exporting.EV_RESULT_SET;
        if (evResultSet != null)
        {
          byte[] bytes = Convert.FromBase64String(evResultSet);
          empty = RepositoryQueryHandler.encUTF8.GetString(bytes);
        }
      }
      catch (ProtocolException ex)
      {
        Trace.TraceError("JSON HTTP call failed: " + ex.ToString());
      }
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Query executed in: ");
      return empty;
    }

    private string ExecuteBOQueryForSAPNamespaces(SESF_SELECTION_PARAMETER[] selectionParameter)
    {
      string xmlString = (string) null;
      string cacheFilePath = this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.BO);
      if (!File.Exists(cacheFilePath))
      {
        xmlString = this.ExecuteBOQueryInternal(selectionParameter, true);
        this.writeCacheFile(cacheFilePath, xmlString);
      }
      else
      {
        lock (this.fileLockObject)
        {
          StreamReader streamReader = new StreamReader(cacheFilePath);
          xmlString = streamReader.ReadToEnd();
          streamReader.Close();
        }
        new Thread(new ParameterizedThreadStart(this.ExecuteBOQueryThreaded)).Start((object) selectionParameter);
      }
      return xmlString;
    }

    private void ExecuteBOQueryThreaded(object data)
    {
      string jsonHttpUrl = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL();
      SESF_SELECTION_PARAMETER[] selectionParameter = (SESF_SELECTION_PARAMETER[]) data;
      string cacheFilePath = this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.BO);
      string str = this.ExecuteBOQueryInternal(selectionParameter, true);
      if (str != null && str.Length > 0)
        this.writeCacheFile(cacheFilePath, str);
      if (!jsonHttpUrl.Equals(SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL()))
        return;
      this.FireBORepositoryDataLoadedEvent(str);
    }

    private string ExecuteBOQueryInternal(SESF_SELECTION_PARAMETER[] selectionParameter, bool withPsmFilter)
    {
      string str = (string) null;
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Query for Business Objects");
      try
      {
        PDI_RI_QUERY_BO pdiRiQueryBo = new PDI_RI_QUERY_BO();
        pdiRiQueryBo.Importing = new PDI_RI_QUERY_BO.ImportingType();
        pdiRiQueryBo.Importing.IT_SELECTION_PARAMETER = selectionParameter;
        if (withPsmFilter)
          pdiRiQueryBo.Importing.IV_WITH_PSM_FILTER = "X";
        else
          pdiRiQueryBo.Importing.IV_WITH_PSM_FILTER = "";
        this.jsonClient = Client.getInstance().getJSONClient(false);
        this.jsonClient.callFunctionModule((SAPFunctionModule) pdiRiQueryBo, false, false, false);
        string evResultSet = pdiRiQueryBo.Exporting.EV_RESULT_SET;
        if (evResultSet != null)
        {
          byte[] bytes = Convert.FromBase64String(evResultSet);
          str = RepositoryQueryHandler.encUTF8.GetString(bytes);
        }
      }
      catch (SystemException ex)
      {
        Trace.TraceError("JSON HTTP call failed: " + ex.ToString());
      }
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Query executed in: ");
      return str;
    }

    private void ExecuteDTQueryThreaded(object data)
    {
      string jsonHttpUrl = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL();
      SESF_SELECTION_PARAMETER[] selectionParameter = (SESF_SELECTION_PARAMETER[]) data;
      string cacheFilePath = this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.DT);
      string str = this.ExecuteDTQueryInternal(selectionParameter, true);
      if (str != null && str.Length > 0)
        this.writeCacheFile(cacheFilePath, str);
      if (!jsonHttpUrl.Equals(SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL()))
        return;
      this.FireDTRepositoryDataLoadedEvent(str);
    }

    private string ExecuteDTQueryForSAPNamespaces(SESF_SELECTION_PARAMETER[] selectionParameter)
    {
      string xmlString = (string) null;
      SAP.Copernicus.Core.Protocol.Connection instance = SAP.Copernicus.Core.Protocol.Connection.getInstance();
      string host = new Uri(instance.GetJsonHttpURL()).Host;
      string str = MDRSHandler.GetCacheRootForConnectionName(instance.GetConnectionName(), instance.UsePSM).FullName + "\\dtCache.xml";
      if (!File.Exists(str))
      {
        xmlString = this.ExecuteDTQueryInternal(selectionParameter, true);
        this.writeCacheFile(str, xmlString);
      }
      else
      {
        lock (this.fileLockObject)
        {
          StreamReader streamReader = new StreamReader(str);
          xmlString = streamReader.ReadToEnd();
          streamReader.Close();
        }
        new Thread(new ParameterizedThreadStart(this.ExecuteDTQueryThreaded)).Start((object) selectionParameter);
      }
      return xmlString;
    }

    private string ExecuteDTQueryInternal(SESF_SELECTION_PARAMETER[] selectionParameter, bool withPsmFilter)
    {
      string str = (string) null;
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Query for Data Types");
      try
      {
        PDI_RI_QUERY_DT pdiRiQueryDt = new PDI_RI_QUERY_DT();
        pdiRiQueryDt.Importing = new PDI_RI_QUERY_DT.ImportingType();
        pdiRiQueryDt.Importing.IT_SELECTION_PARAMETER = selectionParameter;
        if (withPsmFilter)
          pdiRiQueryDt.Importing.IV_WITH_PSM_FILTER = "X";
        else
          pdiRiQueryDt.Importing.IV_WITH_PSM_FILTER = "";
        this.jsonClient = Client.getInstance().getJSONClient(false);
        this.jsonClient.callFunctionModule((SAPFunctionModule) pdiRiQueryDt, false, false, false);
        string evResultSet = pdiRiQueryDt.Exporting.EV_RESULT_SET;
        if (evResultSet != null)
        {
          byte[] bytes = Convert.FromBase64String(evResultSet);
          str = RepositoryQueryHandler.encUTF8.GetString(bytes);
        }
      }
      catch (SystemException ex)
      {
        Trace.TraceError("JSON HTTP call failed: " + ex.ToString());
      }
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Query executed in: ");
      return str;
    }

    private void ExecuteMTQueryThreaded(object data)
    {
      string jsonHttpUrl = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL();
      SESF_SELECTION_PARAMETER[] selectionParameter = (SESF_SELECTION_PARAMETER[]) data;
      string cacheFilePath = this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.MT);
      string str = this.ExecuteMTQueryInternal(selectionParameter, true);
      if (str != null && str.Length > 0)
        this.writeCacheFile(cacheFilePath, str);
      if (!jsonHttpUrl.Equals(SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL()))
        return;
      this.FireMTRepositoryDataLoadedEvent(str);
    }

    private string ExecuteMTQueryForSAPNamespaces(SESF_SELECTION_PARAMETER[] selectionParameter)
    {
      string xmlString = (string) null;
      SAP.Copernicus.Core.Protocol.Connection instance = SAP.Copernicus.Core.Protocol.Connection.getInstance();
      string host = new Uri(instance.GetJsonHttpURL()).Host;
      string str = MDRSHandler.GetCacheRootForConnectionName(instance.GetConnectionName(), instance.UsePSM).FullName + "\\mtCache.xml";
      if (!File.Exists(str))
      {
        xmlString = this.ExecuteMTQueryInternal(selectionParameter, true);
        this.writeCacheFile(str, xmlString);
      }
      else
      {
        lock (this.fileLockObject)
        {
          StreamReader streamReader = new StreamReader(str);
          xmlString = streamReader.ReadToEnd();
          streamReader.Close();
        }
        new Thread(new ParameterizedThreadStart(this.ExecuteMTQueryThreaded)).Start((object) selectionParameter);
      }
      return xmlString;
    }

    private string ExecuteMTQueryInternal(SESF_SELECTION_PARAMETER[] selectionParameter, bool withPsmFilter)
    {
      string str = (string) null;
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Query for Message Types");
      try
      {
        PDI_RI_QUERY_MT pdiRiQueryMt = new PDI_RI_QUERY_MT();
        pdiRiQueryMt.Importing = new PDI_RI_QUERY_MT.ImportingType();
        pdiRiQueryMt.Importing.IT_SELECTION_PARAMETER = selectionParameter;
        if (withPsmFilter)
          pdiRiQueryMt.Importing.IV_WITH_PSM_FILTER = "X";
        else
          pdiRiQueryMt.Importing.IV_WITH_PSM_FILTER = "";
        this.jsonClient = Client.getInstance().getJSONClient(false);
        this.jsonClient.callFunctionModule((SAPFunctionModule) pdiRiQueryMt, false, false, false);
        string evResultSet = pdiRiQueryMt.Exporting.EV_RESULT_SET;
        if (evResultSet != null)
        {
          byte[] bytes = Convert.FromBase64String(evResultSet);
          str = RepositoryQueryHandler.encUTF8.GetString(bytes);
        }
      }
      catch (SystemException ex)
      {
        Trace.TraceError("JSON HTTP call failed: " + ex.ToString());
      }
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Query executed in: ");
      return str;
    }

    public void QueryAllSAPObjects(out string boResultXML, out string dtResultXML, out string mtResultXML, out string badiResultXML)
    {
      boResultXML = this.ExecuteBOQueryForSAPNamespaces(RepositoryQueryHandler.AP_BO_PARAMETER);
      dtResultXML = this.ExecuteDTQueryForSAPNamespaces(RepositoryQueryHandler.AP_DT_PARAMETER);
      mtResultXML = this.ExecuteMTQueryForSAPNamespaces(RepositoryQueryHandler.AP_MT_PARAMETER);
      badiResultXML = this.ExecuteBAdIQueryForSAPNamespaces(RepositoryQueryHandler.AP_BAdI_PARAMETER);
    }

    private void ExecuteABSLQueryThreaded(object data)
    {
      string jsonHttpUrl = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL();
      SESF_SELECTION_PARAMETER[] selectionParameter = (SESF_SELECTION_PARAMETER[]) data;
      string cacheFilePath = this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.ABSL);
      string str = this.ExecuteDTDQuery(selectionParameter, true);
      if (str != null && str.Length > 0)
        this.writeCacheFile(cacheFilePath, str);
      if (!jsonHttpUrl.Equals(SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL()))
        return;
      this.FireLibRepositoryDataLoadedEvent(str);
    }

    private string ExecuteDTDQuery(SESF_SELECTION_PARAMETER[] selectionParameter, bool withPsmFilter)
    {
      string str = (string) null;
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Query for Data Transformation Definitions");
      try
      {
        PDI_RI_QUERY_DTD pdiRiQueryDtd = new PDI_RI_QUERY_DTD();
        pdiRiQueryDtd.Importing = new PDI_RI_QUERY_DTD.ImportingType();
        pdiRiQueryDtd.Importing.IT_SELECTION_PARAMETER = selectionParameter;
        if (withPsmFilter)
          pdiRiQueryDtd.Importing.IV_WITH_PSM_FILTER = "X";
        else
          pdiRiQueryDtd.Importing.IV_WITH_PSM_FILTER = "";
        this.jsonClient = Client.getInstance().getJSONClient(false);
        this.jsonClient.callFunctionModule((SAPFunctionModule) pdiRiQueryDtd, false, false, false);
        string evResultSet = pdiRiQueryDtd.Exporting.EV_RESULT_SET;
        if (evResultSet != null)
        {
          byte[] bytes = Convert.FromBase64String(evResultSet);
          str = RepositoryQueryHandler.encUTF8.GetString(bytes);
        }
      }
      catch (SystemException ex)
      {
        Trace.TraceError("JSON HTTP call failed: " + ex.ToString());
      }
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Query executed in: ");
      return str;
    }

    private string ExecuteDTDOldQuery(SESF_SELECTION_PARAMETER[] selectionParameter, bool withPsmFilter)
    {
      string str = (string) null;
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Query for Data Transformation Definitions");
      try
      {
        PDI_RI_QUERY_DTD_OLD pdiRiQueryDtdOld = new PDI_RI_QUERY_DTD_OLD();
        pdiRiQueryDtdOld.Importing = new PDI_RI_QUERY_DTD_OLD.ImportingType();
        pdiRiQueryDtdOld.Importing.IT_SELECTION_PARAMETER = selectionParameter;
        if (withPsmFilter)
          pdiRiQueryDtdOld.Importing.IV_WITH_PSM_FILTER = "X";
        else
          pdiRiQueryDtdOld.Importing.IV_WITH_PSM_FILTER = "";
        this.jsonClient = Client.getInstance().getJSONClient(false);
        this.jsonClient.callFunctionModule((SAPFunctionModule) pdiRiQueryDtdOld, false, false, false);
        string evResultSet = pdiRiQueryDtdOld.Exporting.EV_RESULT_SET;
        if (evResultSet != null)
        {
          byte[] bytes = Convert.FromBase64String(evResultSet);
          str = RepositoryQueryHandler.encUTF8.GetString(bytes);
        }
      }
      catch (SystemException ex)
      {
        Trace.TraceError("JSON HTTP call failed: " + ex.ToString());
      }
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Query executed in: ");
      return str;
    }

    public string QueryABSLLibraries()
    {
      return this.QueryABSLLibraries(false);
    }

    public string QueryABSLLibraries(bool activeOnly)
    {
      string xmlString = (string) null;
      SESF_SELECTION_PARAMETER[] selectionParameter = new SESF_SELECTION_PARAMETER[0];
      string cacheFilePath = this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.ABSL);
      if (!File.Exists(cacheFilePath))
      {
        xmlString = this.ExecuteDTDQuery(selectionParameter, true);
        this.writeCacheFile(cacheFilePath, xmlString);
      }
      else
      {
        lock (this.fileLockObject)
        {
          StreamReader streamReader = new StreamReader(cacheFilePath);
          xmlString = streamReader.ReadToEnd();
          streamReader.Close();
        }
        new Thread(new ParameterizedThreadStart(this.ExecuteABSLQueryThreaded)).Start((object) selectionParameter);
      }
      return xmlString;
    }

    public string QueryQDLibraries(bool activeOnly)
    {
      string xmlString = (string) null;
      SESF_SELECTION_PARAMETER[] selectionParameter = new SESF_SELECTION_PARAMETER[1]{ new SESF_SELECTION_PARAMETER() { ATTRIBUTE_NAME = "NAME_KEY-NAMESPACE", LOW = "http://sap.com/xi/Metamodel", OPTION = "EQ", SIGN = "I" } };
      string cacheFilePath = this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.QD);
      if (!File.Exists(cacheFilePath))
      {
        xmlString = this.ExecuteDTDQuery(selectionParameter, true);
        this.writeCacheFile(cacheFilePath, xmlString);
      }
      else
      {
        lock (this.fileLockObject)
        {
          StreamReader streamReader = new StreamReader(cacheFilePath);
          xmlString = streamReader.ReadToEnd();
          streamReader.Close();
        }
        new Thread(new ParameterizedThreadStart(this.ExecuteQDQueryThreaded)).Start((object) selectionParameter);
      }
      return xmlString;
    }

    public string QueryXWebServiceLibraries(bool withPsmFilter)
    {
      SESF_SELECTION_PARAMETER[] selectionParameter = new SESF_SELECTION_PARAMETER[1]{ new SESF_SELECTION_PARAMETER() { ATTRIBUTE_NAME = "TRANS_TYPE", LOW = "14", OPTION = "CP", SIGN = "I" } };
      this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.QD);
      return this.ExecuteDTDOldQuery(selectionParameter, withPsmFilter);
    }

    private void ExecuteQDQueryThreaded(object data)
    {
      string jsonHttpUrl = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL();
      SESF_SELECTION_PARAMETER[] selectionParameter = (SESF_SELECTION_PARAMETER[]) data;
      string cacheFilePath = this.getCacheFilePath(RepositoryQueryHandler.cacheFiles.QD);
      string str = this.ExecuteDTDQuery(selectionParameter, true);
      this.writeCacheFile(cacheFilePath, str);
      if (!jsonHttpUrl.Equals(SAP.Copernicus.Core.Protocol.Connection.getInstance().GetJsonHttpURL()))
        return;
      this.FireLibRepositoryDataLoadedEvent(str);
    }

    public string RefreshBOs(string nsName)
    {
      SESF_SELECTION_PARAMETER selectionParameter = new SESF_SELECTION_PARAMETER();
      selectionParameter.ATTRIBUTE_NAME = "NAME_KEY-NAMESPACE";
      selectionParameter.SIGN = "I";
      selectionParameter.OPTION = "EQ";
      selectionParameter.LOW = nsName;
      string str;
      if (RepositoryQueryHandler.IsSAPNamespace(nsName))
        str = this.ExecuteBOQueryForSAPNamespaces(new SESF_SELECTION_PARAMETER[2]
        {
          selectionParameter,
          RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER
        });
      else
        str = this.ExecuteBOQueryInternal(new SESF_SELECTION_PARAMETER[3]
        {
          selectionParameter,
          RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER,
          RepositoryQueryHandler.INACTIVE_VERSION_ID_PARAMETER
        }, false);
      return str;
    }

    public string RefreshBO(string nsName, string boName)
    {
      SESF_SELECTION_PARAMETER selectionParameter1 = new SESF_SELECTION_PARAMETER();
      selectionParameter1.ATTRIBUTE_NAME = "NAME_KEY-NAMESPACE";
      selectionParameter1.SIGN = "I";
      selectionParameter1.OPTION = "EQ";
      selectionParameter1.LOW = nsName;
      SESF_SELECTION_PARAMETER selectionParameter2 = new SESF_SELECTION_PARAMETER();
      selectionParameter2.ATTRIBUTE_NAME = "NAME_KEY-NAME";
      selectionParameter2.SIGN = "I";
      selectionParameter2.OPTION = "EQ";
      selectionParameter2.LOW = boName;
      string str;
      if (RepositoryQueryHandler.IsSAPNamespace(nsName))
        str = this.ExecuteBOQueryInternal(new SESF_SELECTION_PARAMETER[4]
        {
          selectionParameter1,
          selectionParameter2,
          RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER,
          RepositoryQueryHandler.INACTIVE_VERSION_ID_PARAMETER
        }, true);
      else
        str = this.ExecuteBOQueryInternal(new SESF_SELECTION_PARAMETER[4]
        {
          selectionParameter1,
          selectionParameter2,
          RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER,
          RepositoryQueryHandler.INACTIVE_VERSION_ID_PARAMETER
        }, false);
      return str;
    }

    public static bool IsSAPNamespace(string nsName)
    {
      return RepositoryQueryHandler.NAMESPACES_SET.Contains(nsName);
    }

    private static string[] ExecuteQueryForSAPNamespaces()
    {
      SAP.Copernicus.Core.Util.Util.startMeasurement("Query for SAP Namespaces");
      string[] strArray = (string[]) null;
      try
      {
        PDI_RI_QUERY_SAP_NAMESPACES querySapNamespaces = new PDI_RI_QUERY_SAP_NAMESPACES();
        querySapNamespaces.Importing = new PDI_RI_QUERY_SAP_NAMESPACES.ImportingType();
        Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) querySapNamespaces, false, false, false);
        strArray = new string[querySapNamespaces.Exporting.ET_NAMESPACE.Length];
        for (int index = 0; index < querySapNamespaces.Exporting.ET_NAMESPACE.Length; ++index)
          strArray[index] = querySapNamespaces.Exporting.ET_NAMESPACE[index].NAMESPACEURI;
      }
      catch (SystemException ex)
      {
        Trace.TraceError("JSON HTTP call PDI_RI_QUERY_SAP_NAMESPACES failed: " + ex.ToString());
      }
      return strArray;
    }

    public string RefreshDTs(string nsName)
    {
      SESF_SELECTION_PARAMETER selectionParameter = new SESF_SELECTION_PARAMETER();
      selectionParameter.ATTRIBUTE_NAME = "NAME_KEY-NAMESPACE";
      selectionParameter.SIGN = "I";
      selectionParameter.OPTION = "EQ";
      selectionParameter.LOW = nsName;
      string str;
      if (RepositoryQueryHandler.IsSAPNamespace(nsName))
        str = this.ExecuteDTQueryForSAPNamespaces(new SESF_SELECTION_PARAMETER[2]
        {
          selectionParameter,
          RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER
        });
      else
        str = this.ExecuteDTQueryInternal(new SESF_SELECTION_PARAMETER[3]
        {
          selectionParameter,
          RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER,
          RepositoryQueryHandler.INACTIVE_VERSION_ID_PARAMETER
        }, false);
      return str;
    }

    public string RefreshEnhancementOptions(string nsName)
    {
      SESF_SELECTION_PARAMETER selectionParameter = new SESF_SELECTION_PARAMETER();
      selectionParameter.ATTRIBUTE_NAME = "NAME_KEY-NAMESPACE";
      selectionParameter.SIGN = "I";
      selectionParameter.OPTION = "EQ";
      selectionParameter.LOW = nsName;
      string str;
      if (RepositoryQueryHandler.IsSAPNamespace(nsName))
        str = this.ExecuteBAdIQueryForSAPNamespaces(new SESF_SELECTION_PARAMETER[1]
        {
          selectionParameter
        });
      else
        str = this.ExecuteBAdIQueryInternal(new SESF_SELECTION_PARAMETER[1]
        {
          selectionParameter
        });
      return str;
    }

    public string RefreshBOs(List<string> boProxyNames, bool activeOnly)
    {
      if (boProxyNames == null)
        return (string) null;
      int num1 = 0;
      int num2 = 0;
      SESF_SELECTION_PARAMETER[] selectionParameter;
      int num3;
      if (activeOnly)
      {
        selectionParameter = new SESF_SELECTION_PARAMETER[boProxyNames.Count + 1];
        SESF_SELECTION_PARAMETER[] selectionParameterArray = selectionParameter;
        int index = num2;
        int num4 = 1;
        num3 = index + num4;
        SESF_SELECTION_PARAMETER versionIdParameter = RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER;
        selectionParameterArray[index] = versionIdParameter;
      }
      else
      {
        selectionParameter = new SESF_SELECTION_PARAMETER[boProxyNames.Count + 2];
        SESF_SELECTION_PARAMETER[] selectionParameterArray1 = selectionParameter;
        int index1 = num2;
        int num4 = 1;
        int num5 = index1 + num4;
        SESF_SELECTION_PARAMETER versionIdParameter1 = RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER;
        selectionParameterArray1[index1] = versionIdParameter1;
        SESF_SELECTION_PARAMETER[] selectionParameterArray2 = selectionParameter;
        int index2 = num5;
        int num6 = 1;
        num3 = index2 + num6;
        SESF_SELECTION_PARAMETER versionIdParameter2 = RepositoryQueryHandler.INACTIVE_VERSION_ID_PARAMETER;
        selectionParameterArray2[index2] = versionIdParameter2;
      }
      foreach (string boProxyName in boProxyNames)
        selectionParameter[num1++] = new SESF_SELECTION_PARAMETER()
        {
          ATTRIBUTE_NAME = "KEY-NAME",
          SIGN = "I",
          OPTION = "EQ",
          LOW = boProxyName
        };
      return this.ExecuteBOQueryInternal(selectionParameter, false);
    }

    public string RefreshMTs(string nsName)
    {
      SESF_SELECTION_PARAMETER selectionParameter = new SESF_SELECTION_PARAMETER();
      selectionParameter.ATTRIBUTE_NAME = "NAME_KEY-NAMESPACE";
      selectionParameter.SIGN = "I";
      selectionParameter.OPTION = "EQ";
      selectionParameter.LOW = nsName;
      string str;
      if (RepositoryQueryHandler.IsSAPNamespace(nsName))
        str = this.ExecuteMTQueryForSAPNamespaces(new SESF_SELECTION_PARAMETER[2]
        {
          selectionParameter,
          RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER
        });
      else
        str = this.ExecuteMTQueryInternal(new SESF_SELECTION_PARAMETER[3]
        {
          selectionParameter,
          RepositoryQueryHandler.ACTIVE_VERSION_ID_PARAMETER,
          RepositoryQueryHandler.INACTIVE_VERSION_ID_PARAMETER
        }, false);
      return str;
    }

    public string Get_Docu_Service(string boName, string nodeName = null, string associationName = null, string actionName = null, string queryName = null, string nodeElementName = null, string dtName = null, string badiName = null, string isiname = null, string dtdname = null, string ktdname = null)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_GET_DOCU_SERVICE riGetDocuService = new PDI_RI_GET_DOCU_SERVICE();
      riGetDocuService.Importing = new PDI_RI_GET_DOCU_SERVICE.ImportingType();
      riGetDocuService.Exporting = new PDI_RI_GET_DOCU_SERVICE.ExportingType();
      string str = (string) null;
      if (boName != null)
      {
        riGetDocuService.Importing.IV_BO_NAME = boName;
        riGetDocuService.Importing.IV_NODE_NAME = nodeName;
        riGetDocuService.Importing.IV_ASSOCIATION_NAME = associationName;
        riGetDocuService.Importing.IV_ACTION_NAME = actionName;
        riGetDocuService.Importing.IV_QUERY_NAME = queryName;
        riGetDocuService.Importing.IV_NODE_ELEMENT = nodeElementName;
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) riGetDocuService, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
        if (riGetDocuService.Exporting != null)
        {
          if (riGetDocuService.Exporting.EV_SUCCESS == "" && riGetDocuService.Exporting.ET_MESSAGES != null && riGetDocuService.Exporting.ET_MESSAGES.Length > 0)
            this.reportServerSideProtocolException((SAPFunctionModule) riGetDocuService, false, false);
          else
            str = riGetDocuService.Exporting.EV_RESULT;
        }
      }
      else if (dtName != null)
      {
        riGetDocuService.Importing.IV_DT_NAME = dtName;
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) riGetDocuService, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
        if (riGetDocuService.Exporting != null)
        {
          if (riGetDocuService.Exporting.EV_SUCCESS == "" && riGetDocuService.Exporting.ET_MESSAGES != null && riGetDocuService.Exporting.ET_MESSAGES.Length > 0)
            this.reportServerSideProtocolException((SAPFunctionModule) riGetDocuService, false, false);
          else
            str = riGetDocuService.Exporting.EV_RESULT;
        }
      }
      else if (badiName != null)
      {
        riGetDocuService.Importing.IV_BADI_NAME = badiName;
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) riGetDocuService, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
        if (riGetDocuService.Exporting != null)
        {
          if (riGetDocuService.Exporting.EV_SUCCESS == "" && riGetDocuService.Exporting.ET_MESSAGES != null && riGetDocuService.Exporting.ET_MESSAGES.Length > 0)
            this.reportServerSideProtocolException((SAPFunctionModule) riGetDocuService, false, false);
          else
            str = riGetDocuService.Exporting.EV_RESULT;
        }
      }
      else if (isiname != null)
      {
        riGetDocuService.Importing.IV_ISI_NAME = isiname;
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) riGetDocuService, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
        if (riGetDocuService.Exporting != null)
        {
          if (riGetDocuService.Exporting.EV_SUCCESS == "" && riGetDocuService.Exporting.ET_MESSAGES != null && riGetDocuService.Exporting.ET_MESSAGES.Length > 0)
            this.reportServerSideProtocolException((SAPFunctionModule) riGetDocuService, false, false);
          else
            str = riGetDocuService.Exporting.EV_RESULT;
        }
      }
      else if (dtdname != null)
      {
        riGetDocuService.Importing.IV_DTD_NAME = dtdname;
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) riGetDocuService, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
        if (riGetDocuService.Exporting != null)
        {
          if (riGetDocuService.Exporting.EV_SUCCESS == "" && riGetDocuService.Exporting.ET_MESSAGES != null && riGetDocuService.Exporting.ET_MESSAGES.Length > 0)
            this.reportServerSideProtocolException((SAPFunctionModule) riGetDocuService, false, false);
          else
            str = riGetDocuService.Exporting.EV_RESULT;
        }
      }
      else if (ktdname != null)
      {
        riGetDocuService.Importing.IV_KTD_NAME = ktdname;
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) riGetDocuService, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
        if (riGetDocuService.Exporting != null)
        {
          if (riGetDocuService.Exporting.EV_SUCCESS == "" && riGetDocuService.Exporting.ET_MESSAGES != null && riGetDocuService.Exporting.ET_MESSAGES.Length > 0)
            this.reportServerSideProtocolException((SAPFunctionModule) riGetDocuService, false, false);
          else
            str = riGetDocuService.Exporting.EV_RESULT;
        }
      }
      return str;
    }

    public string[] Query_Functions()
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_QUERY_FUNCTIONS riQueryFunctions = new PDI_RI_QUERY_FUNCTIONS();
      riQueryFunctions.Importing = new PDI_RI_QUERY_FUNCTIONS.ImportingType();
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) riQueryFunctions, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (riQueryFunctions.Exporting != null)
      {
        if (riQueryFunctions.Exporting.EV_SUCCESS == "" && riQueryFunctions.Exporting.ET_MESSAGES != null && riQueryFunctions.Exporting.ET_MESSAGES.Length > 0)
        {
          this.reportServerSideProtocolException((SAPFunctionModule) riQueryFunctions, false, false);
        }
        else
        {
          int length = riQueryFunctions.Exporting.ET_RESULT_FUNCTIONS.Length;
          string[] strArray = new string[length];
          int num = 0;
          while (num < length)
            ++num;
          return strArray;
        }
      }
      return (string[]) null;
    }

    public DeploymentUnit[] Query_DeploymentUnits()
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_QUERY_DU pdiRiQueryDu = new PDI_RI_QUERY_DU();
      pdiRiQueryDu.Importing = new PDI_RI_QUERY_DU.ImportingType();
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiQueryDu, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (pdiRiQueryDu.Exporting != null)
      {
        if (pdiRiQueryDu.Exporting.EV_SUCCESS == "" && pdiRiQueryDu.Exporting.ET_MESSAGES != null && pdiRiQueryDu.Exporting.ET_MESSAGES.Length > 0)
        {
          this.reportServerSideProtocolException((SAPFunctionModule) pdiRiQueryDu, false, false);
        }
        else
        {
          int length = pdiRiQueryDu.Exporting.ET_DEPLOYMENT_UNIT.Length;
          DeploymentUnit[] deploymentUnitArray = new DeploymentUnit[length];
          for (int index = 0; index < length; ++index)
          {
            PDI_DEPLOYMENT_UNIT pdiDeploymentUnit = pdiRiQueryDu.Exporting.ET_DEPLOYMENT_UNIT[index];
            string proxyname = pdiRiQueryDu.Exporting.ET_DEPLOYMENT_UNIT[index].PROXYNAME;
            string name = pdiRiQueryDu.Exporting.ET_DEPLOYMENT_UNIT[index].NAME;
            string semanticalName = pdiRiQueryDu.Exporting.ET_DEPLOYMENT_UNIT[index].SEMANTICAL_NAME;
            string psmReleaseStatus = pdiRiQueryDu.Exporting.ET_DEPLOYMENT_UNIT[index].PSM_RELEASE_STATUS;
            deploymentUnitArray[index] = new DeploymentUnit(proxyname, name, semanticalName, psmReleaseStatus);
          }
          return deploymentUnitArray;
        }
      }
      return (DeploymentUnit[]) null;
    }

    public PDI_REUSE_UI[] get_Reuse_UIs()
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_GET_REUSE_UIS pdiGetReuseUis = new PDI_GET_REUSE_UIS();
        pdiGetReuseUis.Importing = new PDI_GET_REUSE_UIS.ImportingType();
        jsonClient.callFunctionModule((SAPFunctionModule) pdiGetReuseUis, false, false, false);
        if (pdiGetReuseUis.Exporting != null)
          return pdiGetReuseUis.Exporting.ET_REUSE_UIS;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (PDI_REUSE_UI[]) null;
    }

    public string navigate_by_Association(string boName, string boNodeName, string associationName, string[] nodeIDs, char intern)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_NAVIGATE_BY_ASSOCIATION navigateByAssociation = new PDI_NAVIGATE_BY_ASSOCIATION();
        navigateByAssociation.Importing = new PDI_NAVIGATE_BY_ASSOCIATION.ImportingType();
        navigateByAssociation.Importing.IV_BONAME = boName;
        navigateByAssociation.Importing.IV_BONODENAME = boNodeName;
        navigateByAssociation.Importing.IV_ASSOCIATION_NAME = associationName;
        navigateByAssociation.Importing.IT_NODE_IDS = nodeIDs;
        navigateByAssociation.Importing.IV_INTERN = intern;
        jsonClient.callFunctionModule((SAPFunctionModule) navigateByAssociation, false, false, false);
        if (navigateByAssociation.Exporting != null)
          return navigateByAssociation.Exporting.EV_OUT_DATA;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (string) null;
    }

    public string execute_Query(string boName, string boNodeName, string queryName, int maxRows, SESF_SELECTION_PARAMETER[] selectionParameter, char onlyCount, char intern, out int count)
    {
      count = 0;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_EXECUTE_QUERY pdiExecuteQuery = new PDI_EXECUTE_QUERY();
        pdiExecuteQuery.Importing = new PDI_EXECUTE_QUERY.ImportingType();
        pdiExecuteQuery.Importing.IV_BONAME = boName;
        pdiExecuteQuery.Importing.IV_BONODENAME = boNodeName;
        pdiExecuteQuery.Importing.IV_QUERYNAME = queryName;
        pdiExecuteQuery.Importing.IT_SELECTIONPARAMS = selectionParameter;
        pdiExecuteQuery.Importing.IV_MAXROWS = maxRows;
        pdiExecuteQuery.Importing.IV_ONLYCOUNT = onlyCount;
        pdiExecuteQuery.Importing.IV_INTERN = intern;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiExecuteQuery, false, false, false);
        if ((int) onlyCount == 88)
          count = pdiExecuteQuery.Exporting.EV_COUNT;
        if (pdiExecuteQuery.Exporting != null)
          return pdiExecuteQuery.Exporting.EV_OUT_DATA;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (string) null;
    }

    private enum cacheFiles
    {
      ABSL = 0,
      BO = 1,
      DT = 2,
      QD = 4,
      MT = 5,
      BAdI = 6,
    }

    public delegate void BORepositoryDataLoadedEventHandler(string resultXml);

    public delegate void BAdIRepositoryDataLoadedEventHandler(string resultXML);

    public delegate void DTRepositoryDataLoadedEventHandler(string resultXml);

    public delegate void LibRepositoryDataLoadedEventHandler(string resultXml);

    public delegate void MTRepositoryDataLoadedEventHandler(string resultXml);
  }
}
