﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PROD_UPLOAD_STATUS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_LM_PROD_UPLOAD_STATUS : AbstractRemoteFunction<PDI_LM_PROD_UPLOAD_STATUS.ImportingType, PDI_LM_PROD_UPLOAD_STATUS.ExportingType, PDI_LM_PROD_UPLOAD_STATUS.ChangingType, PDI_LM_PROD_UPLOAD_STATUS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0520391EE497EB14E06B2F8FD8";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_UPL_STATUS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
