﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_UPLOAD_TEXTS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_UPLOAD_TEXTS : AbstractRemoteFunction<PDI_UPLOAD_TEXTS.ImportingType, PDI_UPLOAD_TEXTS.ExportingType, PDI_UPLOAD_TEXTS.ChangingType, PDI_UPLOAD_TEXTS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0135861EE0B0FC8BA833BA9588";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_X_STRING;
      [DataMember]
      public string[] IT_FILE_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
