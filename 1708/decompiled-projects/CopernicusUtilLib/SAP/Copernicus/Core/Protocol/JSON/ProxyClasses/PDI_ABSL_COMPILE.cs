﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_COMPILE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_COMPILE : AbstractRemoteFunction<PDI_ABSL_COMPILE.ImportingType, PDI_ABSL_COMPILE.ExportingType, PDI_ABSL_COMPILE.ChangingType, PDI_ABSL_COMPILE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0135771ED0A7C7CA5B052FD12A";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_COMPILE_TO_ABAP;
      [DataMember]
      public string IV_MDRS_INACTIVE;
      [DataMember]
      public string IV_MDRS_NAMESPACE;
      [DataMember]
      public string[] IT_XREP_FILES;
      [DataMember]
      public string IV_ABSL_SOURCE;
      [DataMember]
      public int IV_USAGE_OPERATION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_S_COMPILER_MESSAGE[] ET_COMPILER_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
