﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_PRODUCT_VERS_AND_COMPS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_PRODUCT_VERS_AND_COMPS
  {
    [DataMember]
    public PDI_PRODUCT PRODUCT;
    [DataMember]
    public PDI_S_PRODUCT_V_AND_COMPONENTS[] VERSIONS;
    [DataMember]
    public string PRODUCT_SUBTYPE;
  }
}
