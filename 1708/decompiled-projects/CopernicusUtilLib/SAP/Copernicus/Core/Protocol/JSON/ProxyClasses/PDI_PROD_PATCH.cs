﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PROD_PATCH
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PROD_PATCH
  {
    [DataMember]
    public string PATCH_ID;
    [DataMember]
    public string PRODUCT;
    [DataMember]
    public string VERSION;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string PREVIOUS_PATCH;
    [DataMember]
    public string STATUS;
    [DataMember]
    public string TRKORR;
    [DataMember]
    public string USER_NAME;
    [DataMember]
    public string SDC_DELIVERY;
    [DataMember]
    public string SDC_DLIVERY_UNIT;
    [DataMember]
    public string ASSEMBLY_STATUS;
    [DataMember]
    public string SPS;
  }
}
