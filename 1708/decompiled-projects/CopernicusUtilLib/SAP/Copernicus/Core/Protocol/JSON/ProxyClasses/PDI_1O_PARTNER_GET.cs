﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_PARTNER_GET
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_PARTNER_GET : AbstractRemoteFunction<PDI_1O_PARTNER_GET.ImportingType, PDI_1O_PARTNER_GET.ExportingType, PDI_1O_PARTNER_GET.ChangingType, PDI_1O_PARTNER_GET.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19BBB1E5CE861C1C5";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_1O_PARTNER_LIST ES_CURRENT_PARTNER;
      [DataMember]
      public PDI_1O_PARTNER_LIST[] ET_PARTNER;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
