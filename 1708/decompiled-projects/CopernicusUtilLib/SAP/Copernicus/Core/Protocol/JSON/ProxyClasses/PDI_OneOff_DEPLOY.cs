﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_OneOff_DEPLOY
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_OneOff_DEPLOY : AbstractRemoteFunction<PDI_OneOff_DEPLOY.ImportingType, PDI_OneOff_DEPLOY.ExportingType, PDI_OneOff_DEPLOY.ChangingType, PDI_OneOff_DEPLOY.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01ED09084CBD556024087";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SESSION_ID;
      [DataMember]
      public string IV_SOLUTION_ASSEMBLE;
      [DataMember]
      public string IV_INSTALLATION_KEY;
      [DataMember]
      public string IV_IS_UPLOAD_INTO_PATCH_SOL;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SOLUTION_NAME;
      [DataMember]
      public string EV_MAJOR_VERSION;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
