﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_AE_GET_BADI_DETAIL
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_AE_GET_BADI_DETAIL : AbstractRemoteFunction<PDI_AE_GET_BADI_DETAIL.ImportingType, PDI_AE_GET_BADI_DETAIL.ExportingType, PDI_AE_GET_BADI_DETAIL.ChangingType, PDI_AE_GET_BADI_DETAIL.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0231F61EE192DF969B2A9A96A6";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SPOT_NAME;
      [DataMember]
      public string IV_BADI_DEF_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_RESULT_SET;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
