﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.I_S_BCO_SCHEMA_CONTENT
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class I_S_BCO_SCHEMA_CONTENT
  {
    [DataMember]
    public string NAME_PROXY;
    [DataMember]
    public string NAME;
    [DataMember]
    public string NAME_DESCRIPTION;
    [DataMember]
    public string NAME_DOCUMENTATION;
    [DataMember]
    public string NAME_TYPING_DT_PROXY;
    [DataMember]
    public string TYPE;
    [DataMember]
    public int LENGTH;
    [DataMember]
    public int DECIMALS;
    [DataMember]
    public string PSM_STATUS;
    [DataMember]
    public string PSM_READ_ONLY;
    [DataMember]
    public BCT_S_VALUE[] FIXED_VALUE;
    [DataMember]
    public string FLAG_KEY;
    [DataMember]
    public string FLAG_MANDATORY;
    [DataMember]
    public string FK_BC_NODE;
    [DataMember]
    public BCT_S_FOREIGNKEY[] FK_ATTRIBUTE;
  }
}
