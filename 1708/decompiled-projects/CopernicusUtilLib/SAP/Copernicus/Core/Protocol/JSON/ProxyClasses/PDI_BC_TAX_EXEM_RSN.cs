﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_TAX_EXEM_RSN
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_TAX_EXEM_RSN
  {
    public string TAX_TYPEfield;
    public string TAX_EXEM_RSNfield;
    public string DESCRIPTIONfield;

    [DataMember]
    public string TAX_TYPE
    {
      get
      {
        return this.TAX_TYPEfield;
      }
      set
      {
        this.TAX_TYPEfield = value;
      }
    }

    [DataMember]
    public string TAX_EXEM_RSN
    {
      get
      {
        return this.TAX_EXEM_RSNfield;
      }
      set
      {
        this.TAX_EXEM_RSNfield = value;
      }
    }

    [DataMember]
    public string DESCRIPTION
    {
      get
      {
        return this.DESCRIPTIONfield;
      }
      set
      {
        this.DESCRIPTIONfield = value;
      }
    }
  }
}
