﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_LOGON_DETAILS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_S_LOGON_DETAILS
  {
    [DataMember]
    public string USAGE_MODE;
    [DataMember]
    public string SYSTEM_MODE;
    [DataMember]
    public string RESTRICTION_MODE;
    [DataMember]
    public string TENANT_ROLE_CODE;
    [DataMember]
    public string SAP_SOLUTION;
    [DataMember]
    public string SAP_SOLUTION_OFFICIAL_NAME;
    [DataMember]
    public string SAP_SOLUTION_RELEASE_NAME;

    public class USAGE_MODE_CODE
    {
      public const string Unkown = " ";
      public const string Scalable = "1";
      public const string OneOff = "2";
      public const string miltiCustomer = "3";
    }

    public class SYSTEM_MODE_CODE
    {
      public const string Unkown = " ";
      public const string Development = "D";
      public const string Productive = "P";
    }

    public class RESTRICTION_MODE_CODE
    {
      public const string Unkown = " ";
      public const string ReadAndWrite = "W";
      public const string ReadOnly = "R";
    }

    public class TENANT_ROLECODE
    {
      public const string Unkown = "";
      public const string Administration = "01";
      public const string Master = "02";
      public const string Stock = "03";
      public const string Production = "04";
      public const string Reference = "05";
      public const string Development = "06";
      public const string Test = "07";
      public const string Partner_Development = "08";
    }

    public class DEPLOYMENT_MODE_CODE
    {
      public const string Installation = "I";
      public const string Development = "D";
      public const string Upload = "U";
    }

    public class SOLUTION_CATEGORY
    {
      public const string Local = "L";
      public const string Global = "G";
      public const string OneOff = "G";
    }

    public class SOLUTION_STATUS
    {
      public const string InDevelopment = "In Development";
      public const string InCertification = "In Certification";
      public const string InTestDeployment = "In Test Deployment";
      public const string InMaintenance = "In Maintenance";
      public const string Obsolete = "Obsolete";
      public const string InDeployment = "In Deployment";
      public const string Deployed = "Deployed";
      public const string Assembled = "Assembled";
      public const string Deployed_In_Correction = "Deployed-In Correction";
    }

    public class SAP_SOLUTION_CODE
    {
      public const string BYD_ODS = "BYD_ODS";
      public const string SAP_LEAP = "SAP_LEAP";
      public const string BYD_COD = "BYD_COD";
      public const string BYD_ODT = "BYD_ODT";
      public const string BYD_ODF = "BYD_ODF";
    }
  }
}
