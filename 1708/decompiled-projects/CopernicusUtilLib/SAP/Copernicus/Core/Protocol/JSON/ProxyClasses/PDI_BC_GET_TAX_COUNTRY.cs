﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_GET_TAX_COUNTRY
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_GET_TAX_COUNTRY : AbstractRemoteFunction<PDI_BC_GET_TAX_COUNTRY.ImportingType, PDI_BC_GET_TAX_COUNTRY.ExportingType, PDI_BC_GET_TAX_COUNTRY.ChangingType, PDI_BC_GET_TAX_COUNTRY.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01138A1ED0AEBB2A77FE79C4F3";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_LANGU;
      [DataMember]
      public string IV_TREETYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_BC_TAX_COUNTRY[] ET_COUNTRY_CODES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
