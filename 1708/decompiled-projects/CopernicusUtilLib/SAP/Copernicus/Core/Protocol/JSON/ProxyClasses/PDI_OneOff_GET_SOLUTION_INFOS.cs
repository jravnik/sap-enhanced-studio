﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_OneOff_GET_SOLUTION_INFOS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_OneOff_GET_SOLUTION_INFOS : AbstractRemoteFunction<PDI_OneOff_GET_SOLUTION_INFOS.ImportingType, PDI_OneOff_GET_SOLUTION_INFOS.ExportingType, PDI_OneOff_GET_SOLUTION_INFOS.ChangingType, PDI_OneOff_GET_SOLUTION_INFOS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0123D21EE092C936CC65A49BA4";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION_NAME;
      [DataMember]
      public string IV_LANGUAGE;
      [DataMember]
      public string IV_LOGLEVEL;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_1O_SOLUTION_HEADER ES_SOLUTION_HEADER;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
