﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_DBG_TRACE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_DBG_TRACE : AbstractRemoteFunction<PDI_ABSL_DBG_TRACE.ImportingType, PDI_ABSL_DBG_TRACE.ExportingType, PDI_ABSL_DBG_TRACE.ChangingType, PDI_ABSL_DBG_TRACE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E028DA61EE1B88FDF1C200486E5";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember(Order = 1)]
      public string IV_UNAME;
      [DataMember(Order = 2)]
      public string IV_START_DATE;
      [DataMember(Order = 3)]
      public string IV_START_TIME;
      [DataMember(Order = 4)]
      public PDI_ABSL_DBG_TRC_LOG_S[] IT_TRACE_LOG;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_ABSL_DBG_TRC_S[] ET_TRACE;
      [DataMember]
      public PDI_ABSL_DBG_TRC_LOG_S[] ET_TRACE_LOG;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
