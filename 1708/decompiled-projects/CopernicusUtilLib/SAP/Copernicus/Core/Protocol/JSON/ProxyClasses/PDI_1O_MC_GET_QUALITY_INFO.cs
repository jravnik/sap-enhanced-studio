﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_MC_GET_QUALITY_INFO
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_1O_MC_GET_QUALITY_INFO : AbstractRemoteFunction<PDI_1O_MC_GET_QUALITY_INFO.ImportingType, PDI_1O_MC_GET_QUALITY_INFO.ExportingType, PDI_1O_MC_GET_QUALITY_INFO.ChangingType, PDI_1O_MC_GET_QUALITY_INFO.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E04DC271EE388A3D9D7803D449F";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_GENERATION_NAMESPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_VALID_QUALITY_REVIEW_EXISTS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_QUALITY_IS_VALID_UNTIL;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
