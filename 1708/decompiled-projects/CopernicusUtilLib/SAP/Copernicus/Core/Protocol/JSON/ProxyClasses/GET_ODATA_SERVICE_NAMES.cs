﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.GET_ODATA_SERVICE_NAMES
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class GET_ODATA_SERVICE_NAMES : AbstractRemoteFunction<GET_ODATA_SERVICE_NAMES.ImportingType, GET_ODATA_SERVICE_NAMES.ExportingType, GET_ODATA_SERVICE_NAMES.ChangingType, GET_ODATA_SERVICE_NAMES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F91EE6A79B48DFADDF902A";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string BO_NAME;
      [DataMember]
      public string BO_NODE_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string[] SERVICE_LIST;
      [DataMember]
      public BAPIRET2[] T_BAPIRET2;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
