﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.FM_PDI_GET_WEBSERVICE_INFO
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class FM_PDI_GET_WEBSERVICE_INFO : AbstractRemoteFunction<FM_PDI_GET_WEBSERVICE_INFO.ImportingType, FM_PDI_GET_WEBSERVICE_INFO.ExportingType, FM_PDI_GET_WEBSERVICE_INFO.ChangingType, FM_PDI_GET_WEBSERVICE_INFO.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "FM_PDI_GET_WEBSERVICE_INFO";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BOXS_PROXY_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
