﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.ExternalWebserviceExtensionHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class ExternalWebserviceExtensionHandler : JSONHandler
  {
    public WEBSERVICE_EXTENSION_DATATYPES readXWebservieDatatypes(string xWebserviceName, string solutionNamespace, out string inputDatatypeName, out string inputDatatypeNamespace, out string outputDatatypeName, out string outputDatatypeNamespace)
    {
      inputDatatypeName = (string) null;
      inputDatatypeNamespace = (string) null;
      outputDatatypeName = (string) null;
      outputDatatypeNamespace = (string) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_READ_XWEBSERVICE_DATATYPES xwebserviceDatatypes = new PDI_READ_XWEBSERVICE_DATATYPES();
      xwebserviceDatatypes.Importing = new PDI_READ_XWEBSERVICE_DATATYPES.ImportingType();
      xwebserviceDatatypes.Importing.IV_SOLUTION_NAMESPACE = solutionNamespace;
      xwebserviceDatatypes.Importing.IV_XWEBSERVICE_NAME = xWebserviceName;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) xwebserviceDatatypes, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (xwebserviceDatatypes.Exporting != null)
      {
        if (xwebserviceDatatypes.Exporting.EV_SUCCESS == "" && xwebserviceDatatypes.Exporting.ET_MESSAGES != null && xwebserviceDatatypes.Exporting.ET_MESSAGES.Length > 0)
        {
          this.reportServerSideProtocolException((SAPFunctionModule) xwebserviceDatatypes, false, false);
        }
        else
        {
          inputDatatypeName = xwebserviceDatatypes.Exporting.EV_INPUT_DATATYPE_NAME;
          inputDatatypeNamespace = xwebserviceDatatypes.Exporting.EV_INPUT_DATATYPE_NAMESPACE;
          outputDatatypeName = xwebserviceDatatypes.Exporting.EV_OUTPUT_DATATYPE_NAME;
          outputDatatypeNamespace = xwebserviceDatatypes.Exporting.EV_OUTPUT_DATATYPE_NAMESPACE;
          return xwebserviceDatatypes.Exporting.ES_WEBSERVICE_EXTENSION;
        }
      }
      return (WEBSERVICE_EXTENSION_DATATYPES) null;
    }

    public void readServiceInterfaceWSDL(string ServiceInterfaceProxyName, string ServiceInterfaceType, out string WsdlUri, out string WsdlContent)
    {
      WsdlUri = (string) null;
      WsdlContent = (string) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_GET_SERVICE_INTERFACE_WSDL serviceInterfaceWsdl = new PDI_GET_SERVICE_INTERFACE_WSDL();
      serviceInterfaceWsdl.Importing = new PDI_GET_SERVICE_INTERFACE_WSDL.ImportingType();
      serviceInterfaceWsdl.Importing.IV_SERVICE_INTERFACE_PRX_NAME = ServiceInterfaceProxyName;
      serviceInterfaceWsdl.Importing.IV_SERVICE_INTERFACE_TYPE = ServiceInterfaceType;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) serviceInterfaceWsdl, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (serviceInterfaceWsdl.Exporting == null)
        return;
      if (serviceInterfaceWsdl.Exporting.EV_SUCCESS == "" && serviceInterfaceWsdl.Exporting.ET_MESSAGES != null && serviceInterfaceWsdl.Exporting.ET_MESSAGES.Length > 0)
      {
        this.reportServerSideProtocolException((SAPFunctionModule) serviceInterfaceWsdl, false, false);
      }
      else
      {
        WsdlUri = serviceInterfaceWsdl.Exporting.EV_WSDL_URI;
        WsdlContent = serviceInterfaceWsdl.Exporting.EV_WSDL_CONTENT;
      }
    }

    public void readServiceInterfaceOperations(string ServiceInterfaceProxyName, string ServiceInterfaceType, out SERVICE_INTERFACE_OPERATIONS[] ServiceInterfaceOperations)
    {
      ServiceInterfaceOperations = (SERVICE_INTERFACE_OPERATIONS[]) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_GET_SI_OPERATIONS pdiGetSiOperations = new PDI_GET_SI_OPERATIONS();
      pdiGetSiOperations.Importing = new PDI_GET_SI_OPERATIONS.ImportingType();
      pdiGetSiOperations.Importing.IV_SERVICE_INTERFACE_PRX_NAME = ServiceInterfaceProxyName;
      pdiGetSiOperations.Importing.IV_SERVICE_INTERFACE_TYPE = ServiceInterfaceType;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiGetSiOperations, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (pdiGetSiOperations.Exporting == null)
        return;
      if (pdiGetSiOperations.Exporting.EV_SUCCESS == "" && pdiGetSiOperations.Exporting.ET_MESSAGES != null && pdiGetSiOperations.Exporting.ET_MESSAGES.Length > 0)
        this.reportServerSideProtocolException((SAPFunctionModule) pdiGetSiOperations, false, false);
      else
        ServiceInterfaceOperations = pdiGetSiOperations.Exporting.ET_OPERATION;
    }
  }
}
