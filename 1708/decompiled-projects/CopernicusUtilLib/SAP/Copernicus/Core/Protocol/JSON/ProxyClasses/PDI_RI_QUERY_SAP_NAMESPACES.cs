﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_QUERY_SAP_NAMESPACES
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_QUERY_SAP_NAMESPACES : AbstractRemoteFunction<PDI_RI_QUERY_SAP_NAMESPACES.ImportingType, PDI_RI_QUERY_SAP_NAMESPACES.ExportingType, PDI_RI_QUERY_SAP_NAMESPACES.ChangingType, PDI_RI_QUERY_SAP_NAMESPACES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0123D21EE0AAF30D6D166E1641";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_S_NAMESPACE[] ET_NAMESPACE;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
