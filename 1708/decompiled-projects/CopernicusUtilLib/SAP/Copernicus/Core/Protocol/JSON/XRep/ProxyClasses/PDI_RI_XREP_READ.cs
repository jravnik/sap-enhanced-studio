﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_XREP_READ
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses
{
  [DataContract]
  public class PDI_RI_XREP_READ : AbstractRemoteFunction<PDI_RI_XREP_READ.ImportingType, PDI_RI_XREP_READ.ExportingType, PDI_RI_XREP_READ.ChangingType, PDI_RI_XREP_READ.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194EC88B8EDEC9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PATH;
      [DataMember]
      public string IV_VIRTUAL_VIEW;
      [DataMember]
      public string IV_LAST_SHIPPED_VERSION;
      [DataMember]
      public string IV_WITH_CONTENT;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public OSLS_XREP_FILE_ATTR[] ET_ATTR;
      [DataMember]
      public string EV_CONTENT;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
