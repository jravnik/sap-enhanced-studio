﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ANA_CHECK_BO_ASSOCIATION
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_ANA_CHECK_BO_ASSOCIATION : AbstractRemoteFunction<PDI_ANA_CHECK_BO_ASSOCIATION.ImportingType, PDI_ANA_CHECK_BO_ASSOCIATION.ExportingType, PDI_ANA_CHECK_BO_ASSOCIATION.ChangingType, PDI_ANA_CHECK_BO_ASSOCIATION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194F015C5246C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public PDI_ASSOCIATION[] IT_ASSOCIATIONS;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_ASSOCIATION_TYPE[] ET_ASSOCIATION_TYPES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
