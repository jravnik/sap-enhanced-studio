﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.JSONParamConverter
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.JSON
{
  public static class JSONParamConverter
  {
    public static List<SyntaxErrorInfo> ET_MESSAGES2SyntaxErrorInfoList(PDI_SFW_S_MESSAGE[] etMessages)
    {
      List<SyntaxErrorInfo> syntaxErrorInfoList = new List<SyntaxErrorInfo>();
      foreach (PDI_SFW_S_MESSAGE etMessage in etMessages)
      {
        SyntaxErrorInfo syntaxErrorInfo = new SyntaxErrorInfo(etMessage.TYPE, etMessage.METHOD, etMessage.MESSAGE);
        syntaxErrorInfoList.Add(syntaxErrorInfo);
      }
      return syntaxErrorInfoList;
    }
  }
}
