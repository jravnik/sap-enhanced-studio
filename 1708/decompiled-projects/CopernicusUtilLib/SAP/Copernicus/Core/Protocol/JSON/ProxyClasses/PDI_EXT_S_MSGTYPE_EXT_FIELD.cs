﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_S_MSGTYPE_EXT_FIELD
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_S_MSGTYPE_EXT_FIELD
  {
    [DataMember]
    public string GENERATED_DATA_TYPE_NAME;
    [DataMember]
    public string MSG_TYPE_NAMESPACE;
    [DataMember]
    public string MSG_TYPE_NAME;
    [DataMember]
    public string EXTENDED_DATA_TYPE_NAMESPACE;
    [DataMember]
    public string EXTENDED_DATA_TYPE_NAME;
    [DataMember]
    public string FIELD_NAME;
    [DataMember]
    public string FIELD_TYPE;
    [DataMember]
    public string FIELD_USAGE_CATEGORY;
    [DataMember]
    public string FIELD_MULTIPLICITY;
    [DataMember]
    public string FIELD_LABEL;
    [DataMember]
    public string FIELD_TOOLTIP;
  }
}
