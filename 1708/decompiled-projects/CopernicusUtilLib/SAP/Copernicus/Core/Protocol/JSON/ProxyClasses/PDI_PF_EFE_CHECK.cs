﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PF_EFE_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PF_EFE_CHECK : AbstractRemoteFunction<PDI_PF_EFE_CHECK.ImportingType, PDI_PF_EFE_CHECK.ExportingType, PDI_PF_EFE_CHECK.ChangingType, PDI_PF_EFE_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "PDI_PF_EFE_CHECK";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string[] IT_XREP_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
