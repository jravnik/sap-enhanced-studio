﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.FM_AP_UI_DT_COMP_GENERATOR
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class FM_AP_UI_DT_COMP_GENERATOR : AbstractRemoteFunction<FM_AP_UI_DT_COMP_GENERATOR.ImportingType, FM_AP_UI_DT_COMP_GENERATOR.ExportingType, FM_AP_UI_DT_COMP_GENERATOR.ChangingType, FM_AP_UI_DT_COMP_GENERATOR.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1269661EE68899CF7213B44112";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BO_NAME;
      [DataMember]
      public string IV_BO_NAMESPACE;
      [DataMember]
      public char IV_ASSIGN_WOC_VIEW_TO_USER;
      [DataMember]
      public string IV_XREP_FOLDER_PATH;
      [DataMember]
      public string IV_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
