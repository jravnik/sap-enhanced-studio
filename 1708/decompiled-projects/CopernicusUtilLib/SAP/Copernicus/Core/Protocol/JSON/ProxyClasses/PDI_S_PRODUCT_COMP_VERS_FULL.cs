﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_PRODUCT_COMP_VERS_FULL
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_PRODUCT_COMP_VERS_FULL
  {
    [DataMember]
    public PDI_PRODUCT_SV COMPONENT_VERSION;
    [DataMember]
    public PDI_PRODUCT_SC COMPONENT;
    [DataMember]
    public PDI_PRODUCT_SV_T[] COMPONENT_TEXTS;
  }
}
