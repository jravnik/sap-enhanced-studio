﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_COMPILER_MESSAGE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_S_COMPILER_MESSAGE
  {
    [DataMember]
    public string FILE_NAME;
    [DataMember]
    public string SEVERITY;
    [DataMember]
    public string LINE_NO;
    [DataMember]
    public string COLUMN_NO;
    [DataMember]
    public string LENGTH;
    [DataMember]
    public string TEXT;
  }
}
