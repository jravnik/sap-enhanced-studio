﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_T_SI_OP_DETAIL
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_T_SI_OP_DETAIL
  {
    [DataMember]
    public string OPN_NAME;
    [DataMember]
    public string OPN_TYPE;
    [DataMember]
    public string BO_PRX_NAME;
    [DataMember]
    public string BO_ESR_NAME;
    [DataMember]
    public string BO_NAMESPACE;
    [DataMember]
    public string BO_ROOT_PRX_NAME;
  }
}
