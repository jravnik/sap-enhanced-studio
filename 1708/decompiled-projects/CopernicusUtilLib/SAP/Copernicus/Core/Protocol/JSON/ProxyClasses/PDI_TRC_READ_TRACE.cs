﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_TRC_READ_TRACE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_TRC_READ_TRACE : AbstractRemoteFunction<PDI_TRC_READ_TRACE.ImportingType, PDI_TRC_READ_TRACE.ExportingType, PDI_TRC_READ_TRACE.ChangingType, PDI_TRC_READ_TRACE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194EC88B8EF4C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_TRACE_RUN_ID;
      [DataMember]
      public string[] IT_PROVIDER_ALIAS;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_TRC_S_OVERVIEW[] ET_TRACE_CONTENT;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_ERROR_TYPE;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
