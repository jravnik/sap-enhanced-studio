﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_S_EXT_FORM_FIELDS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_S_EXT_FORM_FIELDS
  {
    [DataMember]
    public string ESR_BO_NAME;
    [DataMember]
    public string ESR_BO_NAMESPACE;
    [DataMember]
    public string ESR_BO_NODE_NAME;
    [DataMember]
    public string ESR_FIELD_NAME;
    [DataMember]
    public string ESR_FIELD_NAMESPACE;
    [DataMember]
    public string UI_TEXT_BO_NODE;
    [DataMember]
    public string UI_TEXT_FIELD_NAME;
    [DataMember]
    public string FIELD_HAS_REFERENCE_BO_NODE;
    [DataMember]
    public string MAX_BO_ESR_NAME;
    [DataMember]
    public string MAX_BO_ESR_NAMESPACE;
  }
}
