﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_QUERY_STACKS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_QUERY_STACKS : AbstractRemoteFunction<PDI_ABSL_QUERY_STACKS.ImportingType, PDI_ABSL_QUERY_STACKS.ExportingType, PDI_ABSL_QUERY_STACKS.ChangingType, PDI_ABSL_QUERY_STACKS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0383AC1EE2A1FADE3C1B1533B9";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_DATE;
      [DataMember]
      public string IV_TIME;
      [DataMember]
      public string IV_TRANSID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_ABSL_S_STACKS[] ET_DETAIL_LIST;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
