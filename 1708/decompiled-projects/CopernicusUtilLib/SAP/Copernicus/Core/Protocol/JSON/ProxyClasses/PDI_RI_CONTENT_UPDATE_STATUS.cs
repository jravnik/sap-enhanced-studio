﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_CONTENT_UPDATE_STATUS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_RI_CONTENT_UPDATE_STATUS : AbstractRemoteFunction<PDI_RI_CONTENT_UPDATE_STATUS.ImportingType, PDI_RI_CONTENT_UPDATE_STATUS.ExportingType, PDI_RI_CONTENT_UPDATE_STATUS.ChangingType, PDI_RI_CONTENT_UPDATE_STATUS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E028DA21EE1BEE15ECD1521C046";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string[] IT_XREP_PATH;
      [DataMember]
      public string IV_UPDATE_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
