﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PC_MAINTAIN
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PC_MAINTAIN : AbstractRemoteFunction<PDI_PC_MAINTAIN.ImportingType, PDI_PC_MAINTAIN.ExportingType, PDI_PC_MAINTAIN.ChangingType, PDI_PC_MAINTAIN.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01EE08AFA8C166ADD5039";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_COMPONENT;
      [DataMember]
      public string IV_DU_NAME;
      [DataMember]
      public bool IV_DEL;
      [DataMember]
      public string IV_PC_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_PC_MAINTAIN.PDI_RI_T_MESSAGES[] ET_MESSAGES;
      [DataMember]
      public string EV_PC_NAME;
    }

    [DataContract]
    public class PDI_RI_T_MESSAGES
    {
      [DataMember]
      public string TEXT;
      [DataMember]
      public string SEVERITY;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
