﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_WSDL_PRX_CHILD_TT
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_WSDL_PRX_CHILD_TT
  {
    [DataMember]
    public string UUID;
    [DataMember]
    public string ES_NAME;
    [DataMember]
    public string PRX_COMPONENT_NAME;
    [DataMember]
    public string PRX_COMPONENT_TYPE;
    [DataMember]
    public string XSD_TYPE;
    [DataMember]
    public string MDRS_DATA_TYPE;
    [DataMember]
    public string MDRS_DATA_TYPE_NAME;
  }
}
