﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_TRC_QUERY_INCIDENT_TRACE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_TRC_QUERY_INCIDENT_TRACE : AbstractRemoteFunction<PDI_TRC_QUERY_INCIDENT_TRACE.ImportingType, PDI_TRC_QUERY_INCIDENT_TRACE.ExportingType, PDI_TRC_QUERY_INCIDENT_TRACE.ChangingType, PDI_TRC_QUERY_INCIDENT_TRACE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "PDI_TRC_QUERY_INCIDENT_TRACE";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SYNCH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_TRACE_LIST;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
