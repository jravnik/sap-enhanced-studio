﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PROJECT_ITEM
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PROJECT_ITEM
  {
    [DataMember]
    public string PROJECT_ITEM_KEY;
    [DataMember]
    public string PROJ_ITEM_TYPE;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string LM_CONTENT_TYPE;
    [DataMember]
    public string KEY_USER_REQUIRED;
    [DataMember]
    public string ADD_ITEM_RELEVANT;
    [DataMember]
    public string DEL_ALLOWED_IN_MAINT;
  }
}
