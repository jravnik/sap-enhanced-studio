﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_ACT_BO_MIGRATION
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_ACT_BO_MIGRATION : AbstractRemoteFunction<PDI_EXT_ACT_BO_MIGRATION.ImportingType, PDI_EXT_ACT_BO_MIGRATION.ExportingType, PDI_EXT_ACT_BO_MIGRATION.ChangingType, PDI_EXT_ACT_BO_MIGRATION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E04DC271ED399F718388802CF38";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION;
      [DataMember]
      public string IV_NODE_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
