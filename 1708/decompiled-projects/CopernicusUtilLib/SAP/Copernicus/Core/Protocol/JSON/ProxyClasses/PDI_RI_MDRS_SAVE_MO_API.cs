﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_MDRS_SAVE_MO_API
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_MDRS_SAVE_MO_API : AbstractRemoteFunction<PDI_RI_MDRS_SAVE_MO_API.ImportingType, PDI_RI_MDRS_SAVE_MO_API.ExportingType, PDI_RI_MDRS_SAVE_MO_API.ChangingType, PDI_RI_MDRS_SAVE_MO_API.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB721CC9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember(Order = 1)]
      public string IV_TYPE;
      [DataMember(Order = 2)]
      public string IV_PROXY_NAME;
      [DataMember(Order = 4)]
      public XML_TABLE[] IT_XML_TABLE;
      [DataMember(Order = 3)]
      public int IV_PROTOCOL_VERSION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public XML_TABLE[] ET_XML_TABLE;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_RESULT_SET;
      [DataMember]
      public string EV_TRANSITIVE_HASH;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
