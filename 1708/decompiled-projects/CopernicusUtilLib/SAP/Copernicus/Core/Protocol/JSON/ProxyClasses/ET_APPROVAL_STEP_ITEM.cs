﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.ET_APPROVAL_STEP_ITEM
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class ET_APPROVAL_STEP_ITEM
  {
    [DataMember]
    public string NAME;
    [DataMember]
    public string TYPE;
    [DataMember]
    public string XREP_PATH;
    [DataMember]
    public string TASK_TYPE;
    [DataMember]
    public string ACTIVE;
  }
}
