﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_XREP_CREATE_OR_UPDATE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses
{
  [DataContract]
  public class PDI_RI_XREP_CREATE_OR_UPDATE : AbstractRemoteFunction<PDI_RI_XREP_CREATE_OR_UPDATE.ImportingType, PDI_RI_XREP_CREATE_OR_UPDATE.ExportingType, PDI_RI_XREP_CREATE_OR_UPDATE.ChangingType, PDI_RI_XREP_CREATE_OR_UPDATE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB7226C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember(Order = 1)]
      public string IV_PATH;
      [DataMember(Order = 7)]
      public string IV_CONTENT;
      [DataMember(Order = 6)]
      public OSLS_XREP_FILE_ATTR[] IT_ATTR;
      [DataMember(Order = 2)]
      public string IV_OVERWRITE;
      [DataMember(Order = 3)]
      public string IV_ACTIVATE;
      [DataMember(Order = 4)]
      public string IV_LANGU;
      [DataMember(Order = 5)]
      public string IV_SESSION_ID;
      [DataMember(Order = 8)]
      public string IV_FORCE_UNLOCK;
      [DataMember(Order = 9)]
      public string IV_SHORT_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public OSLS_XREP_FILE_ATTR[] ET_ATTR;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
