﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_ADM_SESSION_DELETE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_ADM_SESSION_DELETE : AbstractRemoteFunction<PDI_RI_ADM_SESSION_DELETE.ImportingType, PDI_RI_ADM_SESSION_DELETE.ExportingType, PDI_RI_ADM_SESSION_DELETE.ChangingType, PDI_RI_ADM_SESSION_DELETE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DEFB1AB27FBE4B45969";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public PDI_RI_S_ADM_SESSION_USER[] IT_USER;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
