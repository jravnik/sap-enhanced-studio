﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PROXY_PARSER
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_PROXY_PARSER : AbstractRemoteFunction<PDI_PROXY_PARSER.ImportingType, PDI_PROXY_PARSER.ExportingType, PDI_PROXY_PARSER.ChangingType, PDI_PROXY_PARSER.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0225F71EE1929CD30F3EF810F1";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ESR_NAME;
      [DataMember]
      public string IV_ESR_NAMESPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_WSDL_PARSED_DATA_TT[] ET_WSDL_PARSED_DATA;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
