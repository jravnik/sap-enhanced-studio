﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.MessageClassHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class MessageClassHandler : JSONHandler
  {
    private static MessageClassHandler instance;

    public static MessageClassHandler Instance
    {
      get
      {
        if (MessageClassHandler.instance == null)
          MessageClassHandler.instance = new MessageClassHandler();
        return MessageClassHandler.instance;
      }
      protected set
      {
        MessageClassHandler.instance = value;
      }
    }

    public virtual string GenerateMessageClass(string namespaceName, string proxyName, PDI_UTIL_MSG_CLASS_GENERATE.PDI_UTIL_T_MESSAGE[] messages)
    {
      PDI_UTIL_MSG_CLASS_GENERATE msgClassGenerate = new PDI_UTIL_MSG_CLASS_GENERATE();
      msgClassGenerate.Importing = new PDI_UTIL_MSG_CLASS_GENERATE.ImportingType();
      msgClassGenerate.Importing.IV_ESR_NAMESPACE = namespaceName;
      msgClassGenerate.Importing.IV_PROXY_NAME = proxyName;
      msgClassGenerate.Importing.IT_MESSAGES = messages;
      msgClassGenerate.Importing.IV_SMTG_ACTIVATE = true;
      msgClassGenerate.Importing.IV_SMTG_GENERATE = true;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) msgClassGenerate, false, false, false);
      }
      catch (ProtocolException ex)
      {
        return (string) null;
      }
      if (msgClassGenerate.Exporting.EV_SUCCESS == "")
        return (string) null;
      return msgClassGenerate.Exporting.EV_MESSAGE_CLASS;
    }

    public virtual PDI_UTIL_SMTG_GENERATE.PDI_RI_T_MESSAGE[] GenerateMessageClassSemName(string namespaceName, string esrName, string proxyName, PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE[] messages, out string messageClassName, out string success, out string smtgProxyName)
    {
      PDI_UTIL_SMTG_GENERATE utilSmtgGenerate = new PDI_UTIL_SMTG_GENERATE();
      smtgProxyName = (string) null;
      messageClassName = (string) null;
      success = "";
      utilSmtgGenerate.Importing = new PDI_UTIL_SMTG_GENERATE.ImportingType();
      utilSmtgGenerate.Importing.IV_ESR_NAMESPACE = namespaceName;
      utilSmtgGenerate.Importing.IV_SEMANTICAL_NAME = esrName;
      utilSmtgGenerate.Importing.IV_BO_PROXY_NAME = proxyName;
      utilSmtgGenerate.Importing.IT_MESSAGES = messages;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) utilSmtgGenerate, false, false, false);
      }
      catch (ProtocolException ex)
      {
        return (PDI_UTIL_SMTG_GENERATE.PDI_RI_T_MESSAGE[]) null;
      }
      if (utilSmtgGenerate.Exporting.EV_SUCCESS == "")
        return utilSmtgGenerate.Exporting.ET_ERROR_MESSAGES;
      success = utilSmtgGenerate.Exporting.EV_SUCCESS;
      smtgProxyName = utilSmtgGenerate.Exporting.EV_PROXY_NAME;
      messageClassName = utilSmtgGenerate.Exporting.EV_MESSAGE_CLASS;
      return utilSmtgGenerate.Exporting.ET_ERROR_MESSAGES;
    }

    public virtual PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SM[] QuerySMTG(bool messageType, string namespName, string name, PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SM_SEL[] selection, bool conv, out PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SMT[] querySMT)
    {
      PDI_RI_QUERY_SM pdiRiQuerySm = new PDI_RI_QUERY_SM();
      pdiRiQuerySm.Importing = new PDI_RI_QUERY_SM.ImportingType();
      pdiRiQuerySm.Importing.IV_SMT = messageType;
      pdiRiQuerySm.Importing.IV_NAMESPACE = namespName;
      pdiRiQuerySm.Importing.IV_NAME = name;
      pdiRiQuerySm.Importing.IT_SEL = selection;
      pdiRiQuerySm.Importing.IV_CONV = conv;
      querySMT = (PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SMT[]) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiQuerySm, false, false, false);
      }
      catch (ProtocolException ex)
      {
        return (PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SM[]) null;
      }
      if (pdiRiQuerySm.Exporting == null)
        return (PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SM[]) null;
      if (pdiRiQuerySm.Exporting.EV_SUCCESS == "")
        this.reportServerSideProtocolException((SAPFunctionModule) pdiRiQuerySm, false, false);
      else
        querySMT = pdiRiQuerySm.Exporting.ET_PDI_QUERY_SMT;
      return pdiRiQuerySm.Exporting.ET_PDI_QUERY_SM;
    }

    public virtual PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SM[] QuerySMTG(bool messageType, string namespName, string name, bool conv, out PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SMT[] querySMT)
    {
      PDI_RI_QUERY_SM pdiRiQuerySm = new PDI_RI_QUERY_SM();
      pdiRiQuerySm.Importing = new PDI_RI_QUERY_SM.ImportingType();
      pdiRiQuerySm.Importing.IV_SMT = messageType;
      pdiRiQuerySm.Importing.IV_NAMESPACE = namespName;
      pdiRiQuerySm.Importing.IV_NAME = name;
      pdiRiQuerySm.Importing.IV_CONV = conv;
      querySMT = (PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SMT[]) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiQuerySm, false, false, false);
      }
      catch (ProtocolException ex)
      {
        return (PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SM[]) null;
      }
      if (pdiRiQuerySm.Exporting == null)
        return (PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SM[]) null;
      if (pdiRiQuerySm.Exporting.EV_SUCCESS == "")
        this.reportServerSideProtocolException((SAPFunctionModule) pdiRiQuerySm, false, false);
      else
        querySMT = pdiRiQuerySm.Exporting.ET_PDI_QUERY_SMT;
      return pdiRiQuerySm.Exporting.ET_PDI_QUERY_SM;
    }
  }
}
