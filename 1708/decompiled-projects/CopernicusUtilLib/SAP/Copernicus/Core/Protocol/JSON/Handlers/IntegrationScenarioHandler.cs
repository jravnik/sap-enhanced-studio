﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.IntegrationScenarioHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class IntegrationScenarioHandler : JSONHandler
  {
    public PDI_RI_T_SI_AND_OPS[] getSIOPInfo(string partnerNamespace, bool excludePartnerA2A = false)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_A2X_GET_SI_AND_OPS riA2XGetSiAndOps = new PDI_RI_A2X_GET_SI_AND_OPS();
        riA2XGetSiAndOps.Importing = new PDI_RI_A2X_GET_SI_AND_OPS.ImportingType();
        riA2XGetSiAndOps.Importing.IV_NAMESPACE = partnerNamespace;
        if (excludePartnerA2A)
          riA2XGetSiAndOps.Importing.IV_EXCLUDE_A2A = "X";
        jsonClient.callFunctionModule((SAPFunctionModule) riA2XGetSiAndOps, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) riA2XGetSiAndOps, false, false);
        if (riA2XGetSiAndOps.Exporting != null)
        {
          if (riA2XGetSiAndOps.Exporting.EV_SUCCESS.Equals("X"))
            return riA2XGetSiAndOps.Exporting.ET_SI_LIST;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (PDI_RI_T_SI_AND_OPS[]) null;
    }

    public PDI_WSDL_PARSED_DATA_TT[] getOpProxyWSDL(string partnerNamespace, string esrName)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_PROXY_PARSER pdiProxyParser = new PDI_PROXY_PARSER();
        pdiProxyParser.Importing = new PDI_PROXY_PARSER.ImportingType();
        pdiProxyParser.Importing.IV_ESR_NAMESPACE = partnerNamespace;
        pdiProxyParser.Importing.IV_ESR_NAME = esrName;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiProxyParser, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiProxyParser, false, false);
        if (pdiProxyParser.Exporting != null)
        {
          if (pdiProxyParser.Exporting.EV_SUCCESS.Equals("X"))
            return pdiProxyParser.Exporting.ET_WSDL_PARSED_DATA;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (PDI_WSDL_PARSED_DATA_TT[]) null;
    }

    public string getWSIDTemplateRequest(string outboundSI, string opProxyname, string opName, string xmlNs)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_WSID_GET_TEMPLATE_REQUEST getTemplateRequest = new PDI_WSID_GET_TEMPLATE_REQUEST();
        getTemplateRequest.Importing = new PDI_WSID_GET_TEMPLATE_REQUEST.ImportingType();
        getTemplateRequest.Importing.IV_OUTBOUND_SERVICE_INTERFACE = outboundSI;
        getTemplateRequest.Importing.IV_OPERATION_PRX_NAME = opProxyname;
        getTemplateRequest.Importing.IV_OPERATION_NAME = opName;
        getTemplateRequest.Importing.IV_XML_NAMESPACE = xmlNs;
        jsonClient.callFunctionModule((SAPFunctionModule) getTemplateRequest, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) getTemplateRequest, false, false);
        if (getTemplateRequest.Exporting != null)
        {
          if (getTemplateRequest.Exporting.EV_SUCCESS.Equals("X"))
            return getTemplateRequest.Exporting.EV_TEMPLATE_REQUEST;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (string) null;
    }

    public string getServiceResponse(string outboundSI, string opProxyname, string opName, string xmlNs, string request, string bpID, string commScenName)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_WSID_TEST_TOOL pdiWsidTestTool = new PDI_WSID_TEST_TOOL();
        pdiWsidTestTool.Importing = new PDI_WSID_TEST_TOOL.ImportingType();
        pdiWsidTestTool.Importing.IV_OUTBOUND_SERVICE_INTERFACE = outboundSI;
        pdiWsidTestTool.Importing.IV_OPERATION_PRX_NAME = opProxyname;
        pdiWsidTestTool.Importing.IV_OPERATION_NAME = opName;
        pdiWsidTestTool.Importing.IV_XML_NAMESPACE = xmlNs;
        pdiWsidTestTool.Importing.IV_REQUEST = request;
        pdiWsidTestTool.Importing.IV_BUSINESS_PARTNER_ID = bpID;
        pdiWsidTestTool.Importing.IV_COMMUNICATION_SCENARIO_NAME = commScenName;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiWsidTestTool, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiWsidTestTool, false, false);
        if (pdiWsidTestTool.Exporting != null)
        {
          if (pdiWsidTestTool.Exporting.EV_SUCCESS.Equals("X"))
            return pdiWsidTestTool.Exporting.EV_RESPONSE;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (string) null;
    }
  }
}
