﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_S_SCENARIO
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_S_SCENARIO
  {
    [DataMember]
    public string SCEN_NAME_PRX;
    [DataMember]
    public string SCEN_DESCRIPTION;
    [DataMember]
    public string FLOW_DESCRIPTION;
    [DataMember]
    public string SOURCE_BO_PRX;
    [DataMember]
    public string SOURCE_ND_PRX;
    [DataMember]
    public string TARGET_BO_PRX;
    [DataMember]
    public string TARGET_ND_PRX;
    [DataMember]
    public string REF_FIELD_BUNDLE_KEY;
    [DataMember]
    public string REF_FIELD_NAME;
  }
}
