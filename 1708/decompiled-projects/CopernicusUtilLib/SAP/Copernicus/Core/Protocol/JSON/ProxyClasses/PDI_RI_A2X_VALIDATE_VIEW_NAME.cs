﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_A2X_VALIDATE_VIEW_NAME
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_A2X_VALIDATE_VIEW_NAME : AbstractRemoteFunction<PDI_RI_A2X_VALIDATE_VIEW_NAME.ImportingType, PDI_RI_A2X_VALIDATE_VIEW_NAME.ExportingType, PDI_RI_A2X_VALIDATE_VIEW_NAME.ChangingType, PDI_RI_A2X_VALIDATE_VIEW_NAME.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194F015C523EC9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BO_NAME;
      [DataMember]
      public string IV_BOV_NAME;
      [DataMember]
      public string IV_NAME_SPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public ViewNameDetails[] ET_BOV_NAME;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
