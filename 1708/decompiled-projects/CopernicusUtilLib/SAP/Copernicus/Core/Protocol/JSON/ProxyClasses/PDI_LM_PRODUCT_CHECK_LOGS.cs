﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRODUCT_CHECK_LOGS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_PRODUCT_CHECK_LOGS : AbstractRemoteFunction<PDI_LM_PRODUCT_CHECK_LOGS.ImportingType, PDI_LM_PRODUCT_CHECK_LOGS.ExportingType, PDI_LM_PRODUCT_CHECK_LOGS.ChangingType, PDI_LM_PRODUCT_CHECK_LOGS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F51EE5B6DCCA64FA5A552C";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_PASS_CHK_IND;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_T_APPLOG_MSG[] ET_APPLOG;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
