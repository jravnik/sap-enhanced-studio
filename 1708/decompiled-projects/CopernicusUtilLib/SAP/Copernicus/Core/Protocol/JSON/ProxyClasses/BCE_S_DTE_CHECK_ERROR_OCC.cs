﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.BCE_S_DTE_CHECK_ERROR_OCC
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class BCE_S_DTE_CHECK_ERROR_OCC
  {
    [DataMember]
    public string NODE_ID;
    [DataMember]
    public string FIELDNAME;
    [DataMember]
    public BCE_S_DTE_FIELD_VALUE[] T_ROW_KEYS;
    [DataMember]
    public string FIELD_VALUE;
  }
}
