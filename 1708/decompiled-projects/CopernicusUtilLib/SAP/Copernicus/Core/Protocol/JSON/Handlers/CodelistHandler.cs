﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.CodelistHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class CodelistHandler : JSONHandler
  {
    public PDI_RI_S_CODELIST[] GetCodeListForDataType(string dataTypeName, string[] codes, string listID, string langu)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_GET_RUNTIME_CODELIST getRuntimeCodelist = new PDI_RI_GET_RUNTIME_CODELIST();
      getRuntimeCodelist.Importing = new PDI_RI_GET_RUNTIME_CODELIST.ImportingType();
      getRuntimeCodelist.Importing.IV_PROXY_NAME = dataTypeName;
      getRuntimeCodelist.Importing.IT_CODES = codes;
      getRuntimeCodelist.Importing.IV_LANGUAGE_CODE = langu;
      getRuntimeCodelist.Importing.IV_LIST_ID = listID;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) getRuntimeCodelist, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (getRuntimeCodelist.Exporting != null)
      {
        if (!(getRuntimeCodelist.Exporting.EV_SUCCESS == ""))
          return getRuntimeCodelist.Exporting.ET_CODE_LISTS;
        this.reportServerSideProtocolException((SAPFunctionModule) getRuntimeCodelist, false, false);
      }
      return (PDI_RI_S_CODELIST[]) null;
    }
  }
}
