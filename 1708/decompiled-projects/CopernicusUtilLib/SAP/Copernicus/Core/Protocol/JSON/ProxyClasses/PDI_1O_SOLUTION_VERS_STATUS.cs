﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_SOLUTION_VERS_STATUS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_1O_SOLUTION_VERS_STATUS
  {
    [DataMember]
    public string CHANGE_DATETIME;
    [DataMember]
    public string PHASE;
    [DataMember]
    public string PHASE_STATUS;
    [DataMember]
    public string APPL_COMPONENT;
    [DataMember]
    public PDI_1O_SOL_VERS_STATUS_LOG[] SOLUTION_VERS_STATUS_LOG_LIST;
    [DataMember]
    public PDI_1O_SOL_VERS_SUB_STATUS[] SOLUTION_VERS_SUB_STATUS_LIST;
  }
}
