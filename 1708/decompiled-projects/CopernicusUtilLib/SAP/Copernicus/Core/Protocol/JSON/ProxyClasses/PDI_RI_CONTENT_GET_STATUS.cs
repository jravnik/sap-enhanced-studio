﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_CONTENT_GET_STATUS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_CONTENT_GET_STATUS : AbstractRemoteFunction<PDI_RI_CONTENT_GET_STATUS.ImportingType, PDI_RI_CONTENT_GET_STATUS.ExportingType, PDI_RI_CONTENT_GET_STATUS.ChangingType, PDI_RI_CONTENT_GET_STATUS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19CCE30BEB4D01B74";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_XREP_PATH;
      [DataMember]
      public string[] IT_XREP_PATH;
      [DataMember]
      public string IV_SOLUTION_PREFIX;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_S_APPS_CONTENT_STATUS[] ET_CONTENT_STATUS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
