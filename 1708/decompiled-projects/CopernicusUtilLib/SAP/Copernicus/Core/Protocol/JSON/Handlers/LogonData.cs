﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.LogonData
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class LogonData
  {
    public bool UsePSM = true;
    public string backend_user;
    public string ValidatedXRepSessionID;
    public bool IsAdminUser;
    public string PartnerDomain;
    public string PartnerID;
    public string PartnerName;
    public string client;
    public UsageMode UsageMode;
    public SystemMode SystemMode;
    public RestrictionMode RestrictionMode;
    public TenantRoleCode TenantRoleCode;
    public SapSolutionCode SapSolutionCode;
    public string officialName;
    public string releaseName;
    public bool IsMultiCustomerEnabled;
  }
}
