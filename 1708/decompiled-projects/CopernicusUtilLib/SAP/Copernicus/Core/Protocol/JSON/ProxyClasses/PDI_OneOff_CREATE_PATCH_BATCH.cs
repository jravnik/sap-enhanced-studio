﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_OneOff_CREATE_PATCH_BATCH
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_OneOff_CREATE_PATCH_BATCH : AbstractRemoteFunction<PDI_OneOff_CREATE_PATCH_BATCH.ImportingType, PDI_OneOff_CREATE_PATCH_BATCH.ExportingType, PDI_OneOff_CREATE_PATCH_BATCH.ChangingType, PDI_OneOff_CREATE_PATCH_BATCH.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F91EE5B7D7285EE2C105CE";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_USER;
      [DataMember]
      public string IV_SESSION_ID;
      [DataMember]
      public bool IV_DELETION_PATCH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_JOBNAME;
      [DataMember]
      public string EV_JOBCNT;
      [DataMember]
      public string EV_APPLOG_ID;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
