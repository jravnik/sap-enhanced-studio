﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_LOGON_COMPONENT
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_LOGON_COMPONENT
  {
    [DataMember]
    public string UI_RELEASE;
    [DataMember]
    public string UI_BUILD_NUMBER;
    [DataMember]
    public string BASE_COMPONENT;
    [DataMember]
    public string BYD_RELEASE;
    [DataMember]
    public string PATCH_LEVEL;
  }
}
