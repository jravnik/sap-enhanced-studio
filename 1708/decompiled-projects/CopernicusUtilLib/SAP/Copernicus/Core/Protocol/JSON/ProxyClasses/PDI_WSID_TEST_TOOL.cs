﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_WSID_TEST_TOOL
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_WSID_TEST_TOOL : AbstractRemoteFunction<PDI_WSID_TEST_TOOL.ImportingType, PDI_WSID_TEST_TOOL.ExportingType, PDI_WSID_TEST_TOOL.ChangingType, PDI_WSID_TEST_TOOL.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19D807D9E00957700";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_OUTBOUND_SERVICE_INTERFACE;
      [DataMember]
      public string IV_OPERATION_PRX_NAME;
      [DataMember]
      public string IV_OPERATION_NAME;
      [DataMember]
      public string IV_XML_NAMESPACE;
      [DataMember]
      public string IV_REQUEST;
      [DataMember]
      public string IV_BUSINESS_PARTNER_ID;
      [DataMember]
      public string IV_COMMUNICATION_SCENARIO_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_RESPONSE;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
