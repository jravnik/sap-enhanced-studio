﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_MERGE_PARTNER_SOLUTION
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_MERGE_PARTNER_SOLUTION : AbstractRemoteFunction<PDI_MERGE_PARTNER_SOLUTION.ImportingType, PDI_MERGE_PARTNER_SOLUTION.ExportingType, PDI_MERGE_PARTNER_SOLUTION.ChangingType, PDI_MERGE_PARTNER_SOLUTION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "PDI_MERGE_PARTNER_SOLUTION";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ORIGINAL_SOLUTION_NAME;
      [DataMember]
      public string IV_ORIGINAL_SOLUTION_VERSION;
      [DataMember]
      public string IV_COPIED_SOLUTION_NAME;
      [DataMember]
      public string IV_COPIED_SOLUTION_VERSION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
