﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_TEXTS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_GET_TEXTS : AbstractRemoteFunction<PDI_GET_TEXTS.ImportingType, PDI_GET_TEXTS.ExportingType, PDI_GET_TEXTS.ChangingType, PDI_GET_TEXTS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0135861ED0B0B59C030FE3DC51";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string[] IT_FILE_PATH;
      [DataMember]
      public string IV_TARGET_LANG;
      [DataMember]
      public string IV_FILENAME;
      [DataMember]
      public string IV_ADD_DEFTRAN;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_X_STRING;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
