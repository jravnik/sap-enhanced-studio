﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_SPC_INCIDENT_CREATE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_SPC_INCIDENT_CREATE : AbstractRemoteFunction<PDI_LM_SPC_INCIDENT_CREATE.ImportingType, PDI_LM_SPC_INCIDENT_CREATE.ExportingType, PDI_LM_SPC_INCIDENT_CREATE.ChangingType, PDI_LM_SPC_INCIDENT_CREATE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0123D21EE0BE9659FAB5D180A5";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_SUBJECT;
      [DataMember]
      public string IV_DESCRIPTION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_INCIDENT_ID;
      [DataMember]
      public string EV_LINK;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
