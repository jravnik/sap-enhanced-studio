﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.BAC_PDI_S_BAC
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class BAC_PDI_S_BAC
  {
    [DataMember]
    public string ELEMENTID;
    [DataMember]
    public string TYPE;
    [DataMember]
    public string SUBTYPE;
    [DataMember]
    public string PARENTID;
    [DataMember]
    public string VISIBLESCOPING;
    [DataMember]
    public string VISIBLEFINETUNING;
    [DataMember]
    public int SEQUENCE;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string DOCUMENTATION;
    [DataMember]
    public string SCOPINGQUESTION;
    [DataMember]
    public string GOLIVECHECK;
    [DataMember]
    public string GOLIVEPHASE;
    [DataMember]
    public string GOLIVEACTIVITYGROUP;
    [DataMember]
    public string GOLIVEACTIVITYTYPE;
    [DataMember]
    public string GOLIVEACTIVITYDESCRIPTION;
    [DataMember]
    public BCT_S_COUNTRY[] COUNTRY;
    [DataMember]
    public BCT_S_INDUSTRY[] INDUSTRY;
    [DataMember]
    public BCT_S_IMPL_FOCUS[] IMPLFOCUS;
    [DataMember]
    public string CONSTRAINTEXPRESSION;
    [DataMember]
    public string DEFAULTEXPRESSION;
    [DataMember]
    public string OPTIONGROUP;
    [DataMember]
    public string OPTIONEXCLUSIVE;
    [DataMember]
    public string OPTIONMANDATORY;
    [DataMember]
    public string ELEMENTTYPE;
  }
}
