﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_WORKLIST_ITEM
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses
{
  [DataContract]
  public class OSLS_XREP_WORKLIST_ITEM
  {
    [DataMember]
    public string SOLUTION;
    [DataMember]
    public string BRANCH;
    [DataMember]
    public string FILE_PATH;
    [DataMember]
    public string TRKORR;
    [DataMember]
    public string OWNER;
    [DataMember]
    public string USER_CLIENT;
  }
}
