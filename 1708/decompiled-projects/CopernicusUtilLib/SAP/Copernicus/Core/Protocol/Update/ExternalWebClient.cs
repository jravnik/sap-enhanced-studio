﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.ExternalWebClient
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace SAP.Copernicus.Core.Protocol.Update
{
  internal class ExternalWebClient : WebClient
  {
    private X509Certificate cert;

    public ExternalWebClient(X509Certificate cert)
    {
      this.cert = cert;
    }

    protected override WebRequest GetWebRequest(Uri address)
    {
      HttpWebRequest webRequest = (HttpWebRequest) base.GetWebRequest(address);
      webRequest.ClientCertificates.Add(this.cert);
      return (WebRequest) webRequest;
    }
  }
}
