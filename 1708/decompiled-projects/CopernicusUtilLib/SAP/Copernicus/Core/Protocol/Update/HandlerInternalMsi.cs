﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.HandlerInternalMsi
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Properties;
using System.Net;

namespace SAP.Copernicus.Core.Protocol.Update
{
  internal class HandlerInternalMsi : IHandlerMsi
  {
    public bool IsAvailableUpdate()
    {
      return System.IO.File.GetLastWriteTime(this.GetUrl().Replace("file:", "")).ToUniversalTime().AddMinutes(-10.0).CompareTo(RegistryReader.GetBuildDate()) > 0;
    }

    public WebClient GetWebClient()
    {
      return new WebClient();
    }

    public string GetUrl()
    {
      string str1 = "file://production2.wdf.sap.corp/components/Frontend/Frontend_d/BYDSTUDIO/";
      string str2 = "Frontend_d_stream";
      if (!string.IsNullOrWhiteSpace(PropertyAccess.GeneralProps.codeline))
        str2 = PropertyAccess.GeneralProps.codeline;
      string str3 = "/gen/dbg/java/packaged/full/_copernicus/CopernicusIsolatedShell.msi";
      return str1 + str2 + str3;
    }
  }
}
