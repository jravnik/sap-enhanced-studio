﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.HandlerExternalMsi
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Properties;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

namespace SAP.Copernicus.Core.Protocol.Update
{
  internal class HandlerExternalMsi : IHandlerMsi
  {
    public static string urlToTabelle = "https://websmp108.sap-ag.de/~form/handler?_APP=00200682500000001943&_EVENT=LIST&EVENT=LIST&ENR=67837800100200023807&SWTYPSC=SPP&PECCLSC=OS&INCL_PECCLSC1=OS&PECGRSC1=WIN32&V=MAINT&TA=ACTUAL";
    private StringBuilder log = new StringBuilder();
    private X509Certificate certThatWorks;
    private string tabelle;

    public string Tabelle
    {
      get
      {
        if (this.tabelle == null)
          this.tabelle = this.getTabelle();
        return this.tabelle;
      }
    }

    private string getTabelle()
    {
      this.log.AppendLine("Getting Tabelle now....");
      X509Store x509Store = new X509Store("My");
      x509Store.Open(OpenFlags.ReadOnly);
      foreach (X509Certificate2 certificate in x509Store.Certificates)
      {
        this.log.AppendLine("Trying out a certificate now...");
        HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(HandlerExternalMsi.urlToTabelle);
        httpWebRequest.ClientCertificates.Add((X509Certificate) certificate);
        try
        {
          this.log.AppendLine("There was a response.");
          StreamReader streamReader = new StreamReader(httpWebRequest.GetResponse().GetResponseStream());
          this.certThatWorks = (X509Certificate) certificate;
          return streamReader.ReadToEnd();
        }
        catch (Exception ex)
        {
          this.log.AppendLine(ex.Message);
        }
      }
      return "";
    }

    public string parseTabelleForUrl(string content)
    {
      string str1 = "<a href=\"javascript:ms_getObject('";
      string str2 = "&action=DL_DIRECT');";
      int startIndex = content.LastIndexOf(str1) + str1.Length;
      int length = content.LastIndexOf(str2) - startIndex + str2.Length - 3;
      return content.Substring(startIndex, length);
    }

    public string GetUrl()
    {
      this.log.AppendLine(this.Tabelle);
      return this.parseTabelleForUrl(this.Tabelle);
    }

    private bool isTwoWeeksPassedSinceBuild()
    {
      return DateTime.Now.CompareTo(RegistryReader.GetBuildDate().AddDays(14.0)) > 0;
    }

    private DateTime parseTabelleForLatestDate(string content)
    {
      int index = Regex.Matches(content, "[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}").Cast<Match>().Last<Match>().Index;
      return DateTime.Parse(content.Substring(index, 10));
    }

    private int parseTabelleForLatestPatch(string content)
    {
      try
      {
        int index = Regex.Matches(content, "SOLSTUD1511SP[0-9]{2}").Cast<Match>().Last<Match>().Index;
        return Convert.ToInt32(content.Substring(index + 13, 2));
      }
      catch (Exception ex)
      {
        return 0;
      }
    }

    public bool IsAvailableUpdate()
    {
      try
      {
        return this.parseTabelleForLatestPatch(this.Tabelle) > Convert.ToInt32(PropertyAccess.GeneralProps.LogonPatchLevel);
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    public WebClient GetWebClient()
    {
      return (WebClient) new ExternalWebClient(this.certThatWorks);
    }
  }
}
