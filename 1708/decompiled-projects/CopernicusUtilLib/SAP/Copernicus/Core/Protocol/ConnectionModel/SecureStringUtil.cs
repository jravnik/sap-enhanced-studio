﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.ConnectionModel.SecureStringUtil
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace SAP.Copernicus.Core.Protocol.ConnectionModel
{
  public class SecureStringUtil
  {
    private static Rijndael myRijndael = Rijndael.Create();

    public static SecureString stringToSecureString(string unsecureString)
    {
      SecureString secureString = new SecureString();
      if (unsecureString.Length > 0)
      {
        foreach (char c in unsecureString.ToCharArray())
          secureString.AppendChar(c);
      }
      secureString.MakeReadOnly();
      return secureString;
    }

    public static string secureStringToString(SecureString value)
    {
      if (value == null)
        return "";
      IntPtr num = IntPtr.Zero;
      try
      {
        num = Marshal.SecureStringToGlobalAllocUnicode(value);
        return Marshal.PtrToStringUni(num);
      }
      finally
      {
        Marshal.ZeroFreeGlobalAllocUnicode(num);
      }
    }

    public static string encrypt(string stringToEncrypt)
    {
      string str = "";
      if (stringToEncrypt == null || stringToEncrypt.Length == 0)
        return str;
      using (MD5 md5 = MD5.Create())
      {
        ICryptoTransform encryptor = SecureStringUtil.myRijndael.CreateEncryptor(md5.ComputeHash(SecureStringUtil.myRijndael.IV), SecureStringUtil.myRijndael.IV);
        using (MemoryStream memoryStream = new MemoryStream())
        {
          using (CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, encryptor, CryptoStreamMode.Write))
          {
            using (StreamWriter streamWriter = new StreamWriter((Stream) cryptoStream))
              streamWriter.Write(stringToEncrypt);
            return Convert.ToBase64String(SecureStringUtil.myRijndael.IV) + "_" + Convert.ToBase64String(memoryStream.ToArray());
          }
        }
      }
    }

    public static string decrypt(string stringToDecrypt)
    {
      string str = "";
      if (stringToDecrypt == null || stringToDecrypt.Length == 0)
        return str;
      string[] strArray = Regex.Split(stringToDecrypt, "_");
      using (MD5 md5 = MD5.Create())
      {
        ICryptoTransform decryptor = SecureStringUtil.myRijndael.CreateDecryptor(md5.ComputeHash(Convert.FromBase64String(strArray[0])), Convert.FromBase64String(strArray[0]));
        using (MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(strArray[1])))
        {
          using (CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, decryptor, CryptoStreamMode.Read))
          {
            using (StreamReader streamReader = new StreamReader((Stream) cryptoStream))
              return streamReader.ReadToEnd();
          }
        }
      }
    }
  }
}
