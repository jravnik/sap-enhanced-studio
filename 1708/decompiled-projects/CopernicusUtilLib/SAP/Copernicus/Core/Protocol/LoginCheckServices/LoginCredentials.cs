﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginCredentials
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public class LoginCredentials
  {
    private readonly string _username;
    private readonly string _password;
    private readonly bool _isAlias;

    public string Username
    {
      get
      {
        return this._username;
      }
    }

    public string Password
    {
      get
      {
        return this._password;
      }
    }

    public bool IsAlias
    {
      get
      {
        return this._isAlias;
      }
    }

    public LoginCredentials(string username, string password)
      : this(username, password, true)
    {
    }

    protected LoginCredentials(string username, string password, bool isAlias)
    {
      this._username = username != null ? username : string.Empty;
      this._password = password != null ? password : string.Empty;
      this._isAlias = isAlias;
    }
  }
}
