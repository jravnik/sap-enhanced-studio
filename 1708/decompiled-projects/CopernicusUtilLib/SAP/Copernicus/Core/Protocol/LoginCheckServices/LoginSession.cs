﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginSession
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public class LoginSession : ILoginSession
  {
    private readonly string _client;
    private readonly string _userId;
    private readonly string _terminal;
    private readonly string _time;

    public string Client
    {
      get
      {
        return this._client;
      }
    }

    public string UserId
    {
      get
      {
        return this._userId;
      }
    }

    public string Terminal
    {
      get
      {
        return this._terminal;
      }
    }

    public string Time
    {
      get
      {
        return this._time;
      }
    }

    internal LoginSession(string client, string userId, string terminal, string time)
    {
      this._client = client;
      this._userId = userId;
      this._terminal = terminal;
      this._time = time;
    }
  }
}
