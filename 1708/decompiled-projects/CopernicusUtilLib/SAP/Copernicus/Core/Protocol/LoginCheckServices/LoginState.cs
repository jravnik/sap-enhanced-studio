﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginState
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public enum LoginState
  {
    NOT_AUTHENTICATED,
    CREDENTIALS_REQUESTED,
    NEW_PASSWORD_REQUESTED,
    AUTHENTICATED,
    PASSWORD_CHANGED,
    SESSION_QUERY,
    LOGIN_ERROR,
    CERTIFICATE,
  }
}
