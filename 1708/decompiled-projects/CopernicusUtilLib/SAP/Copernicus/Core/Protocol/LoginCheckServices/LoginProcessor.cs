﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginProcessor
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security;
using System.Text;
using System.Web;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public sealed class LoginProcessor
  {
    private const string STATE_AUTHENITCATED = "state=authenticated";
    private string connectionName;
    private string userName;
    private SecureString password;
    private volatile LoginState _state;
    private List<ILoginMessage> messages;
    private bool tryAgain;
    public CookieContainer Cookies;

    public string ConnectionName
    {
      get
      {
        return this.connectionName;
      }
    }

    public LoginState LoginState
    {
      get
      {
        return this._state;
      }
      private set
      {
        this._state = value;
      }
    }

    public string BaseUri
    {
      get
      {
        ConnectionDataSet.SystemDataRow systemDataRow = ((ConnectionDataSet.SystemDataRow[]) Connection.getInstance().Connections.Tables["SystemData"].Select("Name='" + this.ConnectionName + "'"))[0];
        return "https://" + systemDataRow.Host + ":" + (object) systemDataRow.SSLPort;
      }
    }

    public string SapBaseUri
    {
      get
      {
        return this.BaseUri + "/sap";
      }
    }

    internal LoginProcessor()
    {
    }

    public bool PerformLogin(string connectionName, string userName, SecureString password, out List<ILoginMessage> loginMessages, out bool tryAgain)
    {
      this.connectionName = connectionName;
      this.userName = userName;
      this.password = password;
      this.messages = new List<ILoginMessage>();
      this.tryAgain = false;
      LoginAsyncResult asyncResult = new LoginAsyncResult(this);
      bool flag;
      try
      {
        HttpWebRequest loginRequest = new WebRequestFactory(this.SapBaseUri).CreateLoginRequest("GET", (string) null);
        HttpWebResponse response = (HttpWebResponse) loginRequest.GetResponse();
        using (response)
        {
          if (response.StatusCode == HttpStatusCode.OK)
          {
            this.Cookies = loginRequest.CookieContainer;
            using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            {
              if (response.ContentType.StartsWith("text/xml"))
                this.HandleXmlResponse((TextReader) streamReader, asyncResult);
              else if (response.ContentType.StartsWith("text/plain"))
                this.HandlePlainTextResponse((TextReader) streamReader, asyncResult);
              else
                this.LoginState = LoginState.LOGIN_ERROR;
              streamReader.Close();
            }
          }
          else
            this.LoginState = LoginState.LOGIN_ERROR;
          response.Close();
        }
        flag = this.PerformLogin_Callback((ILoginResult) asyncResult);
      }
      catch (Exception ex)
      {
        if (ex is WebException || ex is UriFormatException)
        {
          tryAgain = false;
          asyncResult.Messages.Add((ILoginMessage) new LoginMessage(ex.Message, LoginMessageType.ERROR));
          int num = (int) CopernicusMessageBox.Show(Resource.MsgWebException, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
          flag = false;
        }
        else
        {
          this.LoginState = LoginState.LOGIN_ERROR;
          asyncResult.Messages.Add((ILoginMessage) new LoginMessage(ex.Message, LoginMessageType.ERROR));
          flag = this.PerformLogin_Callback((ILoginResult) asyncResult);
        }
      }
      loginMessages = this.messages;
      tryAgain = this.tryAgain;
      return flag;
    }

    public ILoginAction GetAction(ILoginResult lar, LoginActionType actionType)
    {
      if (!lar.Actions.ContainsKey(actionType))
        throw new InvalidOperationException("Action " + actionType.ToString() + " not possible");
      return lar.Actions[actionType];
    }

    private LoginResponse ParseXbcmlResponse(TextReader reader)
    {
      XbcmlParser xbcmlParser = new XbcmlParser(reader);
      xbcmlParser.Parse();
      return xbcmlParser.Response;
    }

    private void HandlePlainTextResponse(TextReader reader, LoginAsyncResult asyncResult)
    {
      string end = reader.ReadToEnd();
      if (asyncResult.EnableCertificate)
      {
        if (!end.Contains("certstate="))
          return;
        if (end.Contains("certstate=certreq"))
        {
          this.LoginState = LoginState.CERTIFICATE;
          LoginAction loginAction = new LoginAction(LoginActionType.CERTIFICATE, new Uri(this.BaseUri + "/sap/bc/cert/sap/certreq"));
          asyncResult.Actions.Clear();
          asyncResult.Actions.Add(loginAction.Type, (ILoginAction) loginAction);
        }
        else if (end.Contains("certstate=certmap"))
        {
          this.LoginState = LoginState.CERTIFICATE;
          LoginAction loginAction = new LoginAction(LoginActionType.CERTIFICATE, new Uri(this.BaseUri + "/sap/bc/cert/sap/certmap"));
          asyncResult.Actions.Clear();
          asyncResult.Actions.Add(loginAction.Type, (ILoginAction) loginAction);
        }
        else if (end.Contains("certstate=nocert"))
          this.LoginState = LoginState.AUTHENTICATED;
        else
          this.LoginState = LoginState.LOGIN_ERROR;
      }
      else if (!string.IsNullOrEmpty(end) && end.Contains("state=authenticated"))
        this.LoginState = LoginState.AUTHENTICATED;
      else
        this.LoginState = LoginState.LOGIN_ERROR;
    }

    private void HandleXmlResponse(TextReader reader, LoginAsyncResult asyncResult)
    {
      LoginResponse xbcmlResponse = this.ParseXbcmlResponse(reader);
      asyncResult.Actions = xbcmlResponse.Actions;
      asyncResult.Parameters = xbcmlResponse.Parameters;
      asyncResult.Messages.AddRange((IEnumerable<ILoginMessage>) xbcmlResponse.Messages);
      asyncResult.Sessions.AddRange((IEnumerable<ILoginSession>) xbcmlResponse.Sessions);
      if (xbcmlResponse.Actions != null)
      {
        if (xbcmlResponse.Actions.ContainsKey(LoginActionType.LOGIN_CREDENTIALS))
          this.LoginState = LoginState.CREDENTIALS_REQUESTED;
        else if (xbcmlResponse.Actions.ContainsKey(LoginActionType.SESSION_QUERY_CONTINUE))
          this.LoginState = LoginState.AUTHENTICATED;
        else
          this.LoginState = LoginState.LOGIN_ERROR;
      }
      else
        this.LoginState = LoginState.LOGIN_ERROR;
    }

    private bool PerformLogin_Callback(ILoginResult lar)
    {
      switch (lar.State)
      {
        case LoginState.CREDENTIALS_REQUESTED:
          if (lar.Messages.Count <= 0)
            return this.EndPerformLogin(lar, new LoginCredentials(this.userName, SecureStringUtil.secureStringToString(this.password)), this.GetAction(lar, LoginActionType.LOGIN_CREDENTIALS));
          this.messages = lar.Messages;
          return false;
        case LoginState.AUTHENTICATED:
          return true;
        case LoginState.LOGIN_ERROR:
          this.messages = lar.Messages;
          if (lar.Actions.Count == 0)
            this.tryAgain = true;
          return false;
        default:
          return false;
      }
    }

    public bool EndPerformLogin(ILoginResult loginResult, LoginCredentials credentials, ILoginAction action)
    {
      if (loginResult == null)
        throw new ArgumentNullException("loginResult");
      if (action == null)
        throw new ArgumentNullException("action");
      if (credentials == null && action.Type != LoginActionType.PASSWORD_CANCEL)
        throw new ArgumentNullException("credentials");
      LoginAsyncResult loginAsyncResult = (LoginAsyncResult) loginResult;
      loginAsyncResult.PerformingAction = action;
      loginAsyncResult.EnableCertificate = false;
      loginAsyncResult.Messages.Clear();
      loginAsyncResult.Sessions.Clear();
      ILoginParameter loginParameter1;
      if (!loginAsyncResult.Parameters.TryGetValue("sap-system-login-oninputprocessing", out loginParameter1))
      {
        loginParameter1 = (ILoginParameter) new LoginParameter("sap-system-login-oninputprocessing", (string) null);
        loginAsyncResult.Parameters.Add(loginParameter1.Name, loginParameter1);
      }
      loginParameter1.Value = action.Key;
      ILoginParameter loginParameter2;
      if (credentials.IsAlias)
      {
        if (loginAsyncResult.Parameters.ContainsKey("sap-user"))
          loginAsyncResult.Parameters.Remove("sap-user");
        if (loginAsyncResult.Parameters.TryGetValue("sap-alias", out loginParameter2))
        {
          loginParameter2.Value = credentials.Username;
        }
        else
        {
          loginParameter2 = (ILoginParameter) new LoginParameter("sap-alias", credentials.Username);
          loginAsyncResult.Parameters.Add(loginParameter2.Name, loginParameter2);
        }
      }
      else
      {
        if (loginAsyncResult.Parameters.ContainsKey("sap-alias"))
          loginAsyncResult.Parameters.Remove("sap-alias");
        if (loginAsyncResult.Parameters.TryGetValue("sap-user", out loginParameter2))
          loginParameter2.Value = credentials.Username;
      }
      if (loginAsyncResult.Parameters.TryGetValue("sap-password", out loginParameter2))
        loginParameter2.Value = credentials.Password;
      HttpWebRequest loginRequest = new WebRequestFactory(this.SapBaseUri).CreateLoginRequest("POST", "application/x-www-form-urlencoded");
      loginRequest.CookieContainer = this.Cookies;
      KeyValuePair<HttpWebRequest, LoginAsyncResult> keyValuePair = new KeyValuePair<HttpWebRequest, LoginAsyncResult>(loginRequest, loginAsyncResult);
      try
      {
        Stream requestStream = loginRequest.GetRequestStream();
        string formPostContent = this.CreateFormPostContent(loginAsyncResult);
        using (StreamWriter streamWriter = new StreamWriter(requestStream))
        {
          streamWriter.Write(formPostContent);
          streamWriter.Flush();
          streamWriter.Close();
        }
        using (HttpWebResponse response = (HttpWebResponse) loginRequest.GetResponse())
        {
          this.Cookies = loginRequest.CookieContainer;
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
          {
            this.HandlePostResponse((int) response.StatusCode, response.ContentType, (TextReader) streamReader, keyValuePair.Value);
            streamReader.Close();
          }
          response.Close();
        }
        return this.PerformLogin_Callback((ILoginResult) keyValuePair.Value);
      }
      catch (Exception ex)
      {
        this.LoginState = LoginState.LOGIN_ERROR;
        keyValuePair.Value.Messages.Add((ILoginMessage) new LoginMessage(ex.Message, LoginMessageType.ERROR));
        return this.PerformLogin_Callback((ILoginResult) keyValuePair.Value);
      }
    }

    private string CreateFormPostContent(LoginAsyncResult loginAsyncResult)
    {
      StringBuilder stringBuilder = new StringBuilder(512);
      ICollection<ILoginParameter> values = loginAsyncResult.Parameters.Values;
      int count = values.Count;
      int num = 0;
      foreach (ILoginParameter loginParameter in (IEnumerable<ILoginParameter>) values)
      {
        stringBuilder.Append(loginParameter.Name).Append("=").Append(HttpUtility.UrlEncode(loginParameter.Value));
        ++num;
        if (num < count)
          stringBuilder.Append("&");
      }
      return stringBuilder.ToString();
    }

    private void HandlePostResponse(int statusCode, string contentType, TextReader reader, LoginAsyncResult ar)
    {
      switch (statusCode)
      {
        case 200:
          if (contentType.StartsWith("text/plain"))
          {
            this.HandlePlainTextResponse(reader, ar);
            break;
          }
          if (contentType.StartsWith("text/xml"))
          {
            this.HandleXmlResponse(reader, ar);
            break;
          }
          this.LoginState = LoginState.LOGIN_ERROR;
          break;
        case 400:
          ar.Messages.Add((ILoginMessage) new LoginMessage("HTTP Status Code: 400 - Bad Response", LoginMessageType.ERROR));
          this.LoginState = LoginState.LOGIN_ERROR;
          break;
        default:
          ar.Messages.Add((ILoginMessage) new LoginMessage("HTTP Status Code: " + (object) statusCode + "\n" + reader.ReadToEnd(), LoginMessageType.ERROR));
          this.LoginState = LoginState.LOGIN_ERROR;
          break;
      }
    }
  }
}
