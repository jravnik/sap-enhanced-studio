﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.XbcmlParser
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  internal class XbcmlParser
  {
    private const string ON_LOGIN = "onLogin";
    private const string ON_CHANGE_PASSWORD = "onChangePwd";
    private const string ON_DO_CHANGE_PASSWORD = "onDoChangePwd";
    private const string ON_DO_DELETE_PASSWORD = "onDoDeletePwd";
    private const string ON_PASSWORD_CANCEL = "onCancelPwd";
    private const string ON_PASSWORD_CONTINUE = "onContinuePwd";
    private const string ON_SESSION_QUERY = "onSessionQuery";
    private XDocument _document;
    private LoginResponse _response;

    internal LoginResponse Response
    {
      get
      {
        return this._response;
      }
    }

    internal XbcmlParser(TextReader reader)
    {
      this._response = new LoginResponse();
      this._document = XDocument.Load(reader);
    }

    internal void Parse()
    {
      Dictionary<string, List<LoginParameterValue>> dictionary = new Dictionary<string, List<LoginParameterValue>>();
      foreach (XElement descendant in this._document.Descendants(XName.Get("SimpleType")))
      {
        XAttribute xattribute1 = descendant.Attribute(XName.Get("name"));
        if (xattribute1 != null)
        {
          List<LoginParameterValue> loginParameterValueList = new List<LoginParameterValue>();
          foreach (XElement element in descendant.Elements(XName.Get("Enum")))
          {
            XAttribute xattribute2 = element.Attribute(XName.Get("text"));
            XAttribute xattribute3 = element.Attribute(XName.Get("value"));
            if (xattribute2 != null && xattribute3 != null)
            {
              LoginParameterValue loginParameterValue = new LoginParameterValue(xattribute3.Value, xattribute2.Value);
              loginParameterValueList.Add(loginParameterValue);
            }
          }
          if (xattribute1.Value == "LANGUAGES")
          {
            LoginParameterValue loginParameterValue = new LoginParameterValue("", "");
            loginParameterValueList.Add(loginParameterValue);
          }
          dictionary.Add(xattribute1.Value, loginParameterValueList);
        }
      }
      this._response.Parameters = (IDictionary<string, ILoginParameter>) new Dictionary<string, ILoginParameter>();
      foreach (XElement descendant in this._document.Descendants(XName.Get("Element")))
      {
        XAttribute xattribute1 = descendant.Attribute(XName.Get("name"));
        XAttribute xattribute2 = descendant.Attribute(XName.Get("value"));
        if (xattribute1 != null && xattribute2 != null)
        {
          string str = xattribute1.Value;
          LoginParameter loginParameter = new LoginParameter(str, xattribute2.Value);
          if (this._response.Parameters.ContainsKey(str))
            this._response.Parameters.Remove(str);
          this._response.Parameters.Add(loginParameter.Name, (ILoginParameter) loginParameter);
        }
      }
      this._response.Messages = (IList<ILoginMessage>) new List<ILoginMessage>();
      foreach (XElement descendant in this._document.Descendants(XName.Get("Message")))
      {
        XAttribute xattribute1 = descendant.Attribute(XName.Get("type"));
        XAttribute xattribute2 = descendant.Attribute(XName.Get("text"));
        if (xattribute2 != null)
        {
          string text = xattribute2.Value;
          if (!string.IsNullOrEmpty(text))
          {
            LoginMessageType type;
            if (xattribute1 != null)
            {
              switch (xattribute1.Value)
              {
                case "error":
                  type = LoginMessageType.ERROR;
                  break;
                default:
                  type = LoginMessageType.INFO;
                  break;
              }
            }
            else
              type = LoginMessageType.INFO;
            this._response.Messages.Add((ILoginMessage) new LoginMessage(text, type));
          }
        }
      }
      this._response.Actions = (IDictionary<LoginActionType, ILoginAction>) new Dictionary<LoginActionType, ILoginAction>();
      foreach (XElement descendant in this._document.Descendants(XName.Get("Action")))
      {
        XAttribute xattribute = descendant.Attribute(XName.Get("name"));
        if (xattribute != null)
        {
          ILoginAction loginAction;
          switch (xattribute.Value)
          {
            case "onLogin":
              loginAction = (ILoginAction) new LoginAction(LoginActionType.LOGIN_CREDENTIALS, xattribute.Value);
              break;
            case "onChangePwd":
              loginAction = (ILoginAction) new LoginAction(LoginActionType.PASSWORD_CHANGE, xattribute.Value);
              break;
            case "onDoChangePwd":
              loginAction = (ILoginAction) new LoginAction(LoginActionType.PASSWORD_DO_CHANGE, xattribute.Value);
              break;
            case "onCancelPwd":
              loginAction = (ILoginAction) new LoginAction(LoginActionType.PASSWORD_CANCEL, xattribute.Value);
              break;
            case "onContinuePwd":
              loginAction = (ILoginAction) new LoginAction(LoginActionType.PASSWORD_CONTINUE, xattribute.Value);
              break;
            case "onSessionQuery":
              loginAction = (ILoginAction) new LoginAction(LoginActionType.SESSION_QUERY_CONTINUE, xattribute.Value);
              break;
            default:
              loginAction = (ILoginAction) null;
              break;
          }
          if (loginAction != null)
            this._response.Actions.Add(loginAction.Type, loginAction);
        }
      }
      this._response.Sessions = (IList<ILoginSession>) new List<ILoginSession>();
      foreach (XElement descendant in this._document.Descendants(XName.Get("Session")))
      {
        XAttribute xattribute1 = descendant.Attribute(XName.Get("user"));
        XAttribute xattribute2 = descendant.Attribute(XName.Get("terminal"));
        XAttribute xattribute3 = descendant.Attribute(XName.Get("time"));
        this._response.Sessions.Add((ILoginSession) new LoginSession((string) null, xattribute1 != null ? xattribute1.Value : (string) null, xattribute2 != null ? xattribute2.Value : (string) null, xattribute3 != null ? xattribute3.Value : (string) null));
      }
    }
  }
}
