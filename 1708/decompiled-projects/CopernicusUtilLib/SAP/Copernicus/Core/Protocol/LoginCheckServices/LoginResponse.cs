﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginResponse
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  internal sealed class LoginResponse
  {
    internal IList<ILoginMessage> Messages { get; set; }

    internal IDictionary<LoginActionType, ILoginAction> Actions { get; set; }

    internal IDictionary<string, ILoginParameter> Parameters { get; set; }

    internal IList<ILoginSession> Sessions { get; set; }

    internal LoginResponse()
    {
    }
  }
}
