﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.WebReferenceVersion.ZgetVersion
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.WebReferenceVersion
{
  [XmlType(AnonymousType = true, Namespace = "urn:sap-com:document:sap:soap:functions:mc-style")]
  [GeneratedCode("System.Xml", "4.0.30319.17929")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class ZgetVersion
  {
    private string ivPpmsTechnicalNameField;
    private string ivPpmsTechnicalReleaseField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string IvPpmsTechnicalName
    {
      get
      {
        return this.ivPpmsTechnicalNameField;
      }
      set
      {
        this.ivPpmsTechnicalNameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string IvPpmsTechnicalRelease
    {
      get
      {
        return this.ivPpmsTechnicalReleaseField;
      }
      set
      {
        this.ivPpmsTechnicalReleaseField = value;
      }
    }
  }
}
