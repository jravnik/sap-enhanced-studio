﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.WebReferenceVersion.ZgetVersionCompletedEventHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;

namespace SAP.Copernicus.WebReferenceVersion
{
  [GeneratedCode("System.Web.Services", "4.0.30319.17929")]
  public delegate void ZgetVersionCompletedEventHandler(object sender, ZgetVersionCompletedEventArgs e);
}
