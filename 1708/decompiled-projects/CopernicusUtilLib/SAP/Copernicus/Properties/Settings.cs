﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Properties.Settings
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }

    [SpecialSetting(SpecialSetting.WebServiceUrl)]
    [ApplicationScopedSetting]
    [DefaultSettingValue("https://atf-cust810.dev.sapbydesign.com/sap/bc/srt/rfc/sap/zget_version_service/zsn_version/zbn_version?sap-vhost=atf-cust810.dev.sapbydesign.com")]
    [DebuggerNonUserCode]
    public string CopernicusUtilLib_WebReferenceVersion_ZSN_VERSION
    {
      get
      {
        return (string) this["CopernicusUtilLib_WebReferenceVersion_ZSN_VERSION"];
      }
    }
  }
}
