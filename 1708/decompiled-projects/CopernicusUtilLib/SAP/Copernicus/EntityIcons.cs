﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.EntityIcons
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [CompilerGenerated]
  [DebuggerNonUserCode]
  public class EntityIcons
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) EntityIcons.resourceMan, (object) null))
          EntityIcons.resourceMan = new ResourceManager("SAP.Copernicus.EntityIcons", typeof (EntityIcons).Assembly);
        return EntityIcons.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return EntityIcons.resourceCulture;
      }
      set
      {
        EntityIcons.resourceCulture = value;
      }
    }

    public static Bitmap ABSLScriptFile
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ABSLScriptFile", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Action
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Action", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Association
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Association", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap B2BWebServiceIntegration
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("B2BWebServiceIntegration", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BAdIFilter
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BAdIFilter", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BadiFilterInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BadiFilterInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BAdIImplementation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BAdIImplementation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BTM
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BTM", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BTMInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BTMInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessArea
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessArea", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessConfiguration
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessConfiguration", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessConfigurationObject
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessConfigurationObject", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessConfigurationView
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessConfigurationView", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessConfigurationViewInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessConfigurationViewInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessObject
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessObject", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessObjectExtension
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessObjectExtension", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessObjectInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessObjectInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessObjectNode
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessObjectNode", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessOption
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessOption", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessOptionGroup
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessOptionGroup", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessPackage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessPackage", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessTopic
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("BusinessTopic", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CodeValue
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CodeValue", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CombinedDS
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CombinedDS", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CompositionAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CompositionAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CompositionAssociationWithToNCardinality
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CompositionAssociationWithToNCardinality", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CreateApprovalTask
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CreateApprovalTask", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CreateFloorplan
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CreateFloorplan", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CrossBOAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CrossBOAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CrossBOAssociationWithToNCardinality
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CrossBOAssociationWithToNCardinality", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerObjectReferences
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CustomerObjectReferences", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerObjectReferencesInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CustomerObjectReferencesInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerspecificPatchSolutionIcon
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CustomerspecificPatchSolutionIcon", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerspecificSolutionMultiuserIcon
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CustomerspecificSolutionMultiuserIcon", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerspecificSolutionsIcon
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CustomerspecificSolutionsIcon", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerspecificSolutionTemplateIcon
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("CustomerspecificSolutionTemplateIcon", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DataSource
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DataSource", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DataSourceInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DataSourceInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DataType
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DataType", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeleteSolution
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeleteSolution", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DependentObject
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DependentObject", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DependentObjectAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DependentObjectAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeploymentUnit
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeploymentUnit", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedAction
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedAction", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedBusinessObject
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedBusinessObject", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedCompositionAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedCompositionAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedCompositionAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedCompositionAssociationN", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedCrossBOAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedCrossBOAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedCrossBOAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedCrossBOAssociationN", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedDataType
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedDataType", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedDependendObjectAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedDependendObjectAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedElement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedElement", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedInternalAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedInternalAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedInternalAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedInternalAssociationN", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedLibrary
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedLibrary", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedLibraryFunction
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedLibraryFunction", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedNode
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedNode", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedQuery
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("DeprecatedQuery", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Element
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Element", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ElementCollection
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ElementCollection", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EmbeddedComponent
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("EmbeddedComponent", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EmbeddedComponentInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("EmbeddedComponentInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventHandlerAction
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("EventHandlerAction", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventHandlerActionInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("EventHandlerActionInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventHandlerEvents
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("EventHandlerEvents", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventhandlerEventsInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("EventhandlerEventsInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventHandlerValidation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("EventHandlerValidation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventHandlerValidationInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("EventHandlerValidationInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ExtensionElement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ExtensionElement", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ExtensionElementCollection
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ExtensionElementCollection", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ExternalWebServiceExtension
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ExternalWebServiceExtension", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ExternalWebServiceExtensionInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ExternalWebServiceExtensionInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FactSheet
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("FactSheet", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FactSheetInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("FactSheetInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Floorplan
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Floorplan", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Folder
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Folder", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormMessageTypeExtension
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("FormMessageTypeExtension", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormMsgTypeExtInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("FormMsgTypeExtInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormTemplate
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("FormTemplate", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormTemplateGroup
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("FormTemplateGroup", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormTemplateGroupInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("FormTemplateGroupInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormTemplateVariant
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("FormTemplateVariant", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormTemplateVariantInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("FormTemplateVariantInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap GAF
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("GAF", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap GAFInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("GAFInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InboundMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("InboundMessage", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InboundMigrationMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("InboundMigrationMessage", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InboundMixedMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("InboundMixedMessage", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InformationMark
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("InformationMark", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap IntegrationScenario
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("IntegrationScenario", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InternalAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("InternalAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InternalAssociationWithToNCardinality
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("InternalAssociationWithToNCardinality", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap JoinedDS
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("JoinedDS", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap KeyFigure
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("KeyFigure", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Library
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Library", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap LibraryFunction
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("LibraryFunction", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap LibraryInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("LibraryInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap MDRO
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("MDRO", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap MDROInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("MDROInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Message
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Message", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap MessageGroup
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("MessageGroup", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ModalDialog
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ModalDialog", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap NodeExtensionScenario
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("NodeExtensionScenario", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap NodeExtensionScenarioInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("NodeExtensionScenarioInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OIF
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("OIF", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OIFInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("OIFInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OutboundMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("OutboundMessage", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OutboundMigrationMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("OutboundMigrationMessage", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OutboundMixedMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("OutboundMixedMessage", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OVS
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("OVS", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OWL
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("OWL", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap PortTypePackage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("PortTypePackage", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap PortTypePackageInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("PortTypePackageInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Process
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Process", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Processes
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Processes", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ProcessIntegration
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ProcessIntegration", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ProcessScenarioExtension
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ProcessScenarioExtension", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap QAF
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("QAF", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap QAFInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("QAFInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Query
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Query", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap QueryInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("QueryInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap QuickCreate
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("QuickCreate", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap QuickView
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("QuickView", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyBusinessObject
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyBusinessObject", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyCompositionAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyCompositionAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyCompositionAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyCompositionAssociationN", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyCrossBOAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyCrossBOAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyCrossBOAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyCrossBOAssociationN", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyDependendObjectAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyDependendObjectAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyElement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyElement", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyElementCollection
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyElementCollection", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyInternalAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyInternalAssociation", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyInternalAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyInternalAssociationN", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyNode
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ReadonlyNode", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Report
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Report", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ScalableLocalSolutionIcon
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ScalableLocalSolutionIcon", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ServiceIntegration
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ServiceIntegration", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Solution
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("Solution", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap StatusActionMngmtInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("StatusActionMngmtInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap StatusAndActionManagement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("StatusAndActionManagement", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ThingInspector
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ThingInspector", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ThingType_new
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("ThingType_new", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap TransientElement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("TransientElement", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap TransientExtensionElement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("TransientExtensionElement", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap UISwitch
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("UISwitch", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WebService
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("WebService", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WebServiceAuthorization
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("WebServiceAuthorization", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WebServiceInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("WebServiceInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WorkCenter
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("WorkCenter", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WorkCenterInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("WorkCenterInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WorkCenterView
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("WorkCenterView", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WorkCenterViewInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("WorkCenterViewInactive", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap XODataService
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("XODataService", EntityIcons.resourceCulture);
      }
    }

    public static Bitmap XODataServiceInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject("XODataServiceInactive", EntityIcons.resourceCulture);
      }
    }

    internal EntityIcons()
    {
    }
  }
}
