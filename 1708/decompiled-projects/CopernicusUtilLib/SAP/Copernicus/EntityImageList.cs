﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.EntityImageList
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace SAP.Copernicus
{
  public static class EntityImageList
  {
    private static readonly string[] ImageNames = new string[186]
    {
      "Action",
      "Association",
      "BusinessObject",
      "Element",
      "ExtensionElement",
      "Import",
      "Keyword",
      "Library",
      "Message",
      "Method",
      "BusinessObjectNode",
      "Parameter",
      "Query",
      "Variable",
      "Annotation",
      "DataType",
      "MessageGroup",
      "DependentObject",
      "DeploymentUnit",
      "Solution",
      "StatusAndActionManagement",
      "Floorplan",
      "EventHandlerAction",
      "EventHandlerEvents",
      "FormTemplate",
      "FormTemplateGroup",
      "FormTemplateVariant",
      "BusinessObjectExtension",
      "ProcessIntegration",
      "WebService",
      "EmbeddedComponent",
      "FactSheet",
      "GAF",
      "ModalDialog",
      "OIF",
      "OVS",
      "PortTypePackage",
      "QAF",
      "WorkCenter",
      "WorkCenterView",
      "OWL",
      "BusinessConfigurationView",
      "DataSource",
      "ProcessScenarioExtension",
      "WebServiceAuthorization",
      "FloorplanExtension",
      "GAFExtension",
      "FSExtension",
      "OIFExtension",
      "OWLExtension",
      "QAFExtension",
      "BusinessArea",
      "BusinessPackage",
      "BusinessTopic",
      "BusinessOptionGroup",
      "BusinessOption",
      "InformationMark",
      "MashupCategoryExtension",
      "EventHandlerValidation",
      "IntegrationScenario",
      "BadiImplementation",
      "BadiFilter",
      "B2BWebServiceIntegration",
      "ThingType_new",
      "DeprecatedAction",
      "DeprecatedAssociation",
      "DeprecatedBusinessObject",
      "DeprecatedCompositionAssociation",
      "DeprecatedCompositionAssociationN",
      "DeprecatedCrossBOAssociation",
      "DeprecatedCrossBOAssociationN",
      "DeprecatedDependendObjectAssociation",
      "DeprecatedElement",
      "DeprecatedInternalAssociation",
      "DeprecatedInternalAssociationN",
      "DeprecatedLibrary",
      "DeprecatedLibraryFunction",
      "DeprecatedNode",
      "DeprecatedQuery",
      "ElementCollection",
      "ExtensionElementCollection",
      "LibraryFunction",
      "ReadonlyAssociation",
      "ReadonlyBusinessObject",
      "ReadonlyCompositionAssociation",
      "ReadonlyCompositionAssociationN",
      "ReadonlyCrossBOAssociation",
      "ReadonlyCrossBOAssociationN",
      "ReadonlyDependendObjectAssociation",
      "ReadonlyElement",
      "ReadonlyElementCollection",
      "ReadonlyInternalAssociation",
      "ReadonlyInternalAssociationN",
      "ReadonlyNode",
      "TransientElement",
      "TransientExtensionElement",
      "CrossBOAssociation",
      "CrossBOAssociationWithToNCardinality",
      "InternalAssociation",
      "InternalAssociationWithToNCardinality",
      "BusinessConfigurationObject",
      "QuickCreate",
      "QuickView",
      "ThingInspector",
      "InboundMigrationMessage",
      "InboundMixedMessage",
      "OutboundMigrationMessage",
      "OutboundMixedMessage",
      "InboundMessage",
      "OutboundMessage",
      "Processes",
      "MDRO",
      "BusinessObjectInactive",
      "BusinessConfiguration",
      "QueryInactive",
      "DataSourceInactive",
      "QAFInactive",
      "OIFInactive",
      "FactSheetInactive",
      "WorkCenterInactive",
      "GAFInactive",
      "WorkCenterViewInactive",
      "EmbeddedComponentInactive",
      "PortTypePackageInactive",
      "ModalDialogInactive",
      "OVSInactive",
      "OWLInactive",
      "ProcessIntegrationInactive",
      "WebServiceInactive",
      "WebServiceAuthorizationInactive",
      "B2BWebServiceIntegrationInactive",
      "IntegrationScenarioInactive",
      "BusinessObjectExtensionInactive",
      "BadiImplementationInactive",
      "ProcessScenarioExtensionInactive",
      "OIFExtensionInactive",
      "QAFExtensionInactive",
      "OWLExtensionInactive",
      "FSExtensionInactive",
      "GAFExtensionInactive",
      "ThingType_newInactive",
      "QuickViewInactive",
      "QuickCreateInactive",
      "ThingInspectorInactive",
      "CodeList",
      "CodeListInactive",
      "FormTemplateInactive",
      "BusinessConfigurationInactive",
      "NotificationRule",
      "NotificationRuleInactive",
      "EventHandlerEventsInactive",
      "FormTemplateVariantInactive",
      "EventHandlerActionInactive",
      "EventHandlerValidationInactive",
      "BTM",
      "BTMInactive",
      "StatusActionMngmtInactive",
      "BadiFilterInactive",
      "BusinessConfigurationViewInactive",
      "FormTemplateGroupInactive",
      "FormMessageTypeExtension",
      "FormMsgTypeExtInactive",
      "CodeValue",
      "MDROInactive",
      "UISwitch",
      "LibraryInactive",
      "DeprecatedDataType",
      "CustomerObjectReferences",
      "CustomerObjectReferencesInactive",
      "CombinedDS",
      "JoinedDS",
      "KeyFigure",
      "Report",
      "CustomerspecificPatchSolutionIcon",
      "CustomerspecificSolutionMultiuserIcon",
      "CustomerspecificSolutionsIcon",
      "CustomerspecificSolutionTemplateIcon",
      "ScalableLocalSolutionIcon",
      "DeleteSolution",
      "Folder",
      "ExternalWebServiceExtension",
      "ExternalWebServiceExtensionInactive",
      "ODataService",
      "ODataServiceInactive",
      "NodeExtensionScenario",
      "NodeExtensionScenarioInactive"
    };
    private const string ImagePath = "SAP.Copernicus.Resources.Entities.";
    private static Image[] images;
    private static ImageList imageList;

    public static Image[] Images
    {
      get
      {
        if (EntityImageList.images == null)
          EntityImageList.images = EntityImageList.BuildImageList();
        return EntityImageList.images;
      }
    }

    public static ImageList ImageList
    {
      get
      {
        if (EntityImageList.imageList == null)
        {
          EntityImageList.imageList = new ImageList();
          EntityImageList.imageList.Images.AddRange(EntityImageList.Images);
        }
        else if (EntityImageList.imageList.Images == null || EntityImageList.imageList.Images.Count <= 0)
          EntityImageList.imageList.Images.AddRange(EntityImageList.Images);
        return EntityImageList.imageList;
      }
    }

    public static int getImageIndexForSolution(string solName, string solSuBType, bool isPatch)
    {
      if (isPatch)
        return 173;
      if (solSuBType != null && solSuBType.Equals("TS"))
        return 176;
      if (solSuBType != null && solSuBType.Equals("MS"))
        return 174;
      return solName != null && solName.StartsWith("Z") ? 177 : 175;
    }

    private static Image[] BuildImageList()
    {
      List<Image> imageList = new List<Image>();
      Assembly executingAssembly = Assembly.GetExecutingAssembly();
      foreach (string imageName in EntityImageList.ImageNames)
      {
        string name = "SAP.Copernicus.Resources.Entities." + imageName + ".png";
        try
        {
          using (Stream manifestResourceStream = executingAssembly.GetManifestResourceStream(name))
            imageList.Add(Image.FromStream(manifestResourceStream));
        }
        catch (Exception ex)
        {
          Trace.TraceError("Failed to load image from assembly: " + name);
        }
      }
      return imageList.ToArray();
    }
  }
}
