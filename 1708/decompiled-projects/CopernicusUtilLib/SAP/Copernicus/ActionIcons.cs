﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ActionIcons
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  public class ActionIcons
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) ActionIcons.resourceMan, (object) null))
          ActionIcons.resourceMan = new ResourceManager("SAP.Copernicus.ActionIcons", typeof (ActionIcons).Assembly);
        return ActionIcons.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return ActionIcons.resourceCulture;
      }
      set
      {
        ActionIcons.resourceCulture = value;
      }
    }

    public static Bitmap _170DeployBusinessConfiguration16x16
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("_170DeployBusinessConfiguration16x16", ActionIcons.resourceCulture);
      }
    }

    public static Bitmap Activate
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("Activate", ActionIcons.resourceCulture);
      }
    }

    public static Bitmap ActivateAll
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("ActivateAll", ActionIcons.resourceCulture);
      }
    }

    public static Bitmap ActivateChanged
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("ActivateChanged", ActionIcons.resourceCulture);
      }
    }

    public static Bitmap CheckConsistency
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("CheckConsistency", ActionIcons.resourceCulture);
      }
    }

    public static Bitmap CheckRuntime
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("CheckRuntime", ActionIcons.resourceCulture);
      }
    }

    public static Bitmap Clean
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("Clean", ActionIcons.resourceCulture);
      }
    }

    public static Bitmap Delete
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("Delete", ActionIcons.resourceCulture);
      }
    }

    public static Icon Error
    {
      get
      {
        return (Icon) ActionIcons.ResourceManager.GetObject("Error", ActionIcons.resourceCulture);
      }
    }

    public static Bitmap Help
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("Help", ActionIcons.resourceCulture);
      }
    }

    public static Icon OK
    {
      get
      {
        return (Icon) ActionIcons.ResourceManager.GetObject("OK", ActionIcons.resourceCulture);
      }
    }

    public static Bitmap OpenInUIDesigner
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("OpenInUIDesigner", ActionIcons.resourceCulture);
      }
    }

    public static Bitmap Refresh
    {
      get
      {
        return (Bitmap) ActionIcons.ResourceManager.GetObject("Refresh", ActionIcons.resourceCulture);
      }
    }

    internal ActionIcons()
    {
    }
  }
}
