﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.UISwitch
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [XmlType(AnonymousType = true, Namespace = "http://www.sap.com/a1s/cd/oberon/uimodelchange-1.0")]
  [XmlRoot(IsNullable = false, Namespace = "http://www.sap.com/a1s/cd/oberon/uimodelchange-1.0")]
  [DebuggerStepThrough]
  [Serializable]
  public class UISwitch
  {
    private TextBlockType[] textPoolField;
    private string idField;
    private string nameTextPoolIdField;
    private string descriptionTextPoolIdField;

    [XmlArrayItem("TextBlock", IsNullable = false, Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
    [XmlArray(Form = XmlSchemaForm.Unqualified)]
    public TextBlockType[] TextPool
    {
      get
      {
        return this.textPoolField;
      }
      set
      {
        this.textPoolField = value;
      }
    }

    [XmlAttribute]
    public string id
    {
      get
      {
        return this.idField;
      }
      set
      {
        this.idField = value;
      }
    }

    [XmlAttribute]
    public string nameTextPoolId
    {
      get
      {
        return this.nameTextPoolIdField;
      }
      set
      {
        this.nameTextPoolIdField = value;
      }
    }

    [XmlAttribute]
    public string descriptionTextPoolId
    {
      get
      {
        return this.descriptionTextPoolIdField;
      }
      set
      {
        this.descriptionTextPoolIdField = value;
      }
    }
  }
}
