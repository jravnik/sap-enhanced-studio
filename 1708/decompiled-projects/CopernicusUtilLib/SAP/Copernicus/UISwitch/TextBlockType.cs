﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.TextBlockType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
  [DesignerCategory("code")]
  [Serializable]
  public class TextBlockType
  {
    private TextPoolEntryType[] textPoolEntryField;
    private string languageField;
    private bool masterLanguageField;
    private bool currentLanguageField;

    [XmlElement("TextPoolEntry")]
    public TextPoolEntryType[] TextPoolEntry
    {
      get
      {
        return this.textPoolEntryField;
      }
      set
      {
        this.textPoolEntryField = value;
      }
    }

    [XmlAttribute]
    public string language
    {
      get
      {
        return this.languageField;
      }
      set
      {
        this.languageField = value;
      }
    }

    [DefaultValue(false)]
    [XmlAttribute]
    public bool masterLanguage
    {
      get
      {
        return this.masterLanguageField;
      }
      set
      {
        this.masterLanguageField = value;
      }
    }

    [XmlAttribute]
    [DefaultValue(false)]
    public bool currentLanguage
    {
      get
      {
        return this.currentLanguageField;
      }
      set
      {
        this.currentLanguageField = value;
      }
    }

    public TextBlockType()
    {
      this.masterLanguageField = false;
      this.currentLanguageField = false;
    }
  }
}
