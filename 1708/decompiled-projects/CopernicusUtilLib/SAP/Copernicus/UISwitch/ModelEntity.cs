﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.ModelEntity
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [XmlInclude(typeof (NamedModelEntity))]
  [XmlType(Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
  [DesignerCategory("code")]
  [XmlInclude(typeof (PropertyType))]
  [DebuggerStepThrough]
  [XmlInclude(typeof (TextPoolEntryType))]
  [XmlInclude(typeof (PropertyBagType))]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [Serializable]
  public class ModelEntity
  {
    private PropertyBagType propertyBagField;
    private string idField;

    public PropertyBagType PropertyBag
    {
      get
      {
        return this.propertyBagField;
      }
      set
      {
        this.propertyBagField = value;
      }
    }

    [XmlAttribute]
    public string id
    {
      get
      {
        return this.idField;
      }
      set
      {
        this.idField = value;
      }
    }
  }
}
