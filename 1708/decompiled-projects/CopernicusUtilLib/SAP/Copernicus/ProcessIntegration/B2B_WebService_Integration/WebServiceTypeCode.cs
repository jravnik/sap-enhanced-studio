﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.WebServiceTypeCode
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Xml.Serialization;

namespace SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration
{
  [Serializable]
  public enum WebServiceTypeCode
  {
    [XmlEnum("1")] Soap,
    [XmlEnum("2")] Rest,
  }
}
