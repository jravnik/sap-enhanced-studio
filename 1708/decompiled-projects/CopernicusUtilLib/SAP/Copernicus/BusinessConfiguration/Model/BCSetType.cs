﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BCSetType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model
{
  [XmlRoot("BCSet", IsNullable = false, Namespace = "http://sap.com/ByD/PDI/BCSetDefinition")]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCSetDefinition")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [Serializable]
  public class BCSetType
  {
    private HeadType headField;
    private ParamType[] parameterField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public HeadType Head
    {
      get
      {
        return this.headField;
      }
      set
      {
        this.headField = value;
      }
    }

    [XmlElement("Parameter", Form = XmlSchemaForm.Unqualified)]
    public ParamType[] Parameter
    {
      get
      {
        return this.parameterField;
      }
      set
      {
        this.parameterField = value;
      }
    }
  }
}
