﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BAC.BusinessConfigurationType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BAC
{
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BusinessConfigurationDefinition")]
  [XmlRoot("BCPartnerSolution", IsNullable = false, Namespace = "http://sap.com/ByD/PDI/BusinessConfigurationDefinition")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class BusinessConfigurationType
  {
    private HeadType headField;
    private BacType[] bacField;
    private ContentType[] contentField;
    private SolutionVariantType[] solutionVariantField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public HeadType Head
    {
      get
      {
        return this.headField;
      }
      set
      {
        this.headField = value;
      }
    }

    [XmlElement("Bac", Form = XmlSchemaForm.Unqualified)]
    public BacType[] Bac
    {
      get
      {
        return this.bacField;
      }
      set
      {
        this.bacField = value;
      }
    }

    [XmlElement("Content", Form = XmlSchemaForm.Unqualified)]
    public ContentType[] Content
    {
      get
      {
        return this.contentField;
      }
      set
      {
        this.contentField = value;
      }
    }

    [XmlElement("SolutionVariant", Form = XmlSchemaForm.Unqualified)]
    public SolutionVariantType[] SolutionVariant
    {
      get
      {
        return this.solutionVariantField;
      }
      set
      {
        this.solutionVariantField = value;
      }
    }
  }
}
