﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BAC.HeadType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BAC
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BusinessConfigurationDefinition")]
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [Serializable]
  public class HeadType
  {
    private string elementIDField;
    private string elementTypeField;
    private string countryCodeField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ElementID
    {
      get
      {
        return this.elementIDField;
      }
      set
      {
        this.elementIDField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ElementType
    {
      get
      {
        return this.elementTypeField;
      }
      set
      {
        this.elementTypeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string CountryCode
    {
      get
      {
        return this.countryCodeField;
      }
      set
      {
        this.countryCodeField = value;
      }
    }
  }
}
