﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.BCSetParamValue
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.BusinessConfiguration.Model;
using System.Collections.Generic;

namespace SAP.Copernicus.BusinessConfiguration
{
  public class BCSetParamValue : ValueStructureType
  {
    public string ValueID;
    public string ValueDescription;
    public string ParameterID;
    public bool usedOnlyOnce;
    public List<string> BCSetID;

    public BCSetParamValue(string ValueID, string ParameterID, string BCSetID)
    {
      this.BCSetID = new List<string>();
      this.BCSetID.Add(BCSetID);
      this.ValueID = ValueID;
      this.ParameterID = ParameterID;
      this.usedOnlyOnce = true;
    }
  }
}
