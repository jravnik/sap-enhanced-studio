﻿// Decompiled with JetBrains decompiler
// Type: WindowsInstaller.Record
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace WindowsInstaller
{
  [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
  [Guid("000C1093-0000-0000-C000-000000000046")]
  [CompilerGenerated]
  [TypeIdentifier]
  [ComImport]
  public interface Record
  {
    [DispId(1)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.BStr)]
    string get_StringData([In] int Field);

    [DispId(1)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    void set_StringData([In] int Field, [MarshalAs(UnmanagedType.BStr), In] string value);
  }
}
