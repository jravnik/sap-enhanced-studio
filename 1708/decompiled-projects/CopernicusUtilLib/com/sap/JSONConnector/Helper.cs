﻿// Decompiled with JetBrains decompiler
// Type: com.sap.JSONConnector.Helper
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Web.Script.Serialization;

namespace com.sap.JSONConnector
{
  public static class Helper
  {
    public static string toJSON(this object obj)
    {
      return new JavaScriptSerializer().Serialize(obj);
    }
  }
}
