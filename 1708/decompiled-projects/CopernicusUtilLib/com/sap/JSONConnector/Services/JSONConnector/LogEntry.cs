﻿// Decompiled with JetBrains decompiler
// Type: com.sap.JSONConnector.Services.JSONConnector.LogEntry
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace com.sap.JSONConnector.Services.JSONConnector
{
  [DataContract]
  public class LogEntry
  {
    [DataMember(Name = "ID")]
    public string Id { get; set; }

    [DataMember(Name = "TYPE")]
    public string Type { get; set; }

    [DataMember(Name = "TIMESTAMPL")]
    public string Timestamp { get; set; }

    [DataMember(Name = "DESCRIPTION")]
    public string Description { get; set; }
  }
}
