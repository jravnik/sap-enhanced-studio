﻿// Decompiled with JetBrains decompiler
// Type: com.sap.JSONConnector.Connection
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Net;
using System.Security;

namespace com.sap.JSONConnector
{
  public struct Connection
  {
    public string url;
    public string client;
    public string user;
    public SecureString pass;
    public bool stateful;
    public int timeout;

    static Connection()
    {
      if (ServicePointManager.DefaultConnectionLimit >= 10)
        return;
      ServicePointManager.DefaultConnectionLimit = 10;
    }

    public Connection(string url, string client, string user, SecureString pass, bool stateful)
    {
      this.url = url;
      this.client = client;
      this.user = user;
      this.pass = pass;
      this.stateful = stateful;
      this.timeout = 0;
    }

    public Connection(string url, string client, string user, SecureString pass, bool stateful, int timeout)
    {
      this.url = url;
      this.client = client;
      this.user = user;
      this.pass = pass;
      this.stateful = stateful;
      this.timeout = timeout;
    }
  }
}
