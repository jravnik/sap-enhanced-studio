using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class SystemsComboBoxItem {

    public string Group { get; set; }
    public string Value { get; set; }
    public string Display { get; set; }

    public override string ToString() {
        return Display;
    }

}