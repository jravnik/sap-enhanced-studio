﻿// Decompiled with JetBrains decompiler
// Type: Microsoft.VisualStudio.VSHelp80.Help2
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.VSHelp;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Microsoft.VisualStudio.VSHelp80
{
  [Guid("78413D2D-0492-4A9B-AB25-730633679977")]
  [CompilerGenerated]
  [TypeIdentifier]
  [ComImport]
  public interface Help2 : Help
  {
    [SpecialName]
    void _VtblGap1_9();

    [DispId(10)]
    void DisplayTopicFromF1Keyword([MarshalAs(UnmanagedType.BStr), In] string pszKeyword);
  }
}
