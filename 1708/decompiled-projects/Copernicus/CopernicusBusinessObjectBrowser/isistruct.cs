﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.isistruct
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace CopernicusBusinessObjectBrowser
{
  public struct isistruct
  {
    public string isiname;
    public string isinamespace;
    public string isidirection;
    public string isiproxyname;
    public PDI_RI_T_OP[] isioperations;
  }
}
