﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.ABSLCodeView
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core;
using SAP.Copernicus.Core.Automation;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace CopernicusBusinessObjectBrowser
{
  public class ABSLCodeView : BaseForm
  {
    private new IContainer components;
    private Button buttoncopyToClipboard;
    private RichTextBox richTextBox1;
    private Button button1;
    private Button button2;

    public ABSLCodeView(string code)
      : base(HELP_IDS.BDS_QUERY_EXECUTE)
    {
      this.InitializeComponent();
      this.richTextBox1.Text = code;
      this.HelpID = HELP_IDS.BDS_QUERY_EXECUTE;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.buttoncopyToClipboard = new Button();
      this.richTextBox1 = new RichTextBox();
      this.button1 = new Button();
      this.button2 = new Button();
      this.SuspendLayout();
      this.buttonCancel.Location = new Point(523, 383);
      this.buttonOk.Location = new Point(442, 383);
      this.buttoncopyToClipboard.Location = new Point(21, 349);
      this.buttoncopyToClipboard.Name = "buttoncopyToClipboard";
      this.buttoncopyToClipboard.Size = new Size(101, 23);
      this.buttoncopyToClipboard.TabIndex = 0;
      this.buttoncopyToClipboard.Text = "Copy to Clipboard";
      this.buttoncopyToClipboard.UseVisualStyleBackColor = true;
      this.buttoncopyToClipboard.Click += new EventHandler(this.buttoncopyToClipboard_Click);
      this.richTextBox1.Location = new Point(21, 77);
      this.richTextBox1.Name = "richTextBox1";
      this.richTextBox1.Size = new Size(550, 266);
      this.richTextBox1.TabIndex = 1;
      this.button1.Location = new Point(431, 349);
      this.button1.Name = "button1";
      this.button1.Size = new Size(62, 23);
      this.button1.TabIndex = 2;
      this.button1.Text = "OK";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Location = new Point(509, 349);
      this.button2.Name = "button2";
      this.button2.Size = new Size(62, 23);
      this.button2.TabIndex = 3;
      this.button2.Text = "Cancel";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BannerVisible = true;
      this.ClientSize = new Size(590, 382);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.richTextBox1);
      this.Controls.Add((Control) this.buttoncopyToClipboard);
      this.Name = "ABSLCodeView";
      this.Subtitle = "Scripting language code to execute the query with the selection parameters used.";
      this.Text = "Generate Code";
      this.Controls.SetChildIndex((Control) this.buttoncopyToClipboard, 0);
      this.Controls.SetChildIndex((Control) this.richTextBox1, 0);
      this.Controls.SetChildIndex((Control) this.buttonOk, 0);
      this.Controls.SetChildIndex((Control) this.buttonCancel, 0);
      this.Controls.SetChildIndex((Control) this.button1, 0);
      this.Controls.SetChildIndex((Control) this.button2, 0);
      this.ResumeLayout(false);
    }

    private void buttoncopyToClipboard_Click(object sender, EventArgs e)
    {
      Clipboard.SetText(this.richTextBox1.Text);
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}
