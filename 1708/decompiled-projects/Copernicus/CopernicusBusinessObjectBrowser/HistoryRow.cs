﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.HistoryRow
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Model.BusinessObject;
using System.Collections.Generic;

namespace CopernicusBusinessObjectBrowser
{
  public class HistoryRow
  {
    public List<string[]> resultTable { get; set; }

    public BusinessObject_Node actBoNode { get; set; }

    public HistoryRow.NavigateAttributes navigateAttributes { get; set; }

    public struct NavigateAttributes
    {
      public BusinessObject_Node associatedBONode { get; set; }

      public string boName { get; set; }

      public string boNodeName { get; set; }

      public string associationName { get; set; }

      public string[] nodeIDs { get; set; }
    }
  }
}
