﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ErrorList.ErrorListPresenterFactory
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text.Adornments;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;
using System;
using System.ComponentModel.Composition;

namespace SAP.Copernicus.Core.ErrorList
{
  [Export(typeof (IWpfTextViewCreationListener))]
  [ContentType("any")]
  [TextViewRole("DOCUMENT")]
  internal class ErrorListPresenterFactory : IWpfTextViewCreationListener
  {
    [Import]
    private IErrorProviderFactory SquiggleProviderFactory { get; set; }

    [Import(typeof (SVsServiceProvider))]
    private IServiceProvider ServiceProvider { get; set; }

    public void TextViewCreated(IWpfTextView textView)
    {
      textView.Properties.GetOrCreateSingletonProperty<ErrorListPresenter>((Func<ErrorListPresenter>) (() => new ErrorListPresenter(textView, this.SquiggleProviderFactory, this.ServiceProvider)));
    }
  }
}
