﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BOActionNode
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BOActionNode : BaseNode
  {
    public string boActionName;
    public string boActionProxyName;

    public BOActionNode(RepositoryDataSet.ActionsRow actionRow)
      : base((DataRow) actionRow)
    {
      this.boActionName = actionRow.Name;
      this.boActionProxyName = actionRow.ProxyName;
      this.Text = this.boActionName;
      this.ImageIndex = 5;
      this.SelectedImageIndex = 5;
    }

    public RepositoryDataSet.ActionsRow getData()
    {
      return (RepositoryDataSet.ActionsRow) this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.BOAction;
    }
  }
}
