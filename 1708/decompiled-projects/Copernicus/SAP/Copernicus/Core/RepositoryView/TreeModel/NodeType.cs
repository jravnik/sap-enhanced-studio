﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.NodeType
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public enum NodeType
  {
    SAPContent,
    MySolutions,
    Solution,
    Namespace,
    BOFolder,
    DTFolder,
    BusinessObject,
    DataType,
    BONode,
    BOAction,
    ActionFolder,
    DeterminationFolder,
    BODetermination,
    EnhancementOption,
    EnhancementOptionFolder,
  }
}
