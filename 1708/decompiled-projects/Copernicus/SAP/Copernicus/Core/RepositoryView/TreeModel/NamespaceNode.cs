﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.NamespaceNode
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System;
using System.Data;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class NamespaceNode : BaseNode
  {
    private string nsName;

    public override string Namespace
    {
      get
      {
        return this.nsName;
      }
    }

    public NamespaceNode(RepositoryDataSet.NamespacesRow nsRow)
      : base((DataRow) nsRow)
    {
      this.nsName = nsRow.NSName;
      this.Text = this.nsName;
      this.ImageIndex = 9;
      this.SelectedImageIndex = 9;
    }

    public RepositoryDataSet.NamespacesRow getData()
    {
      return (RepositoryDataSet.NamespacesRow) this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.Namespace;
    }

    public override void refresh()
    {
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Refreshing " + (object) this.getNodeType() + " (" + this.nsName + ")...");
      foreach (TreeNode node in this.Nodes)
      {
        if (node is BaseNode)
        {
          BaseNode baseNode = (BaseNode) node;
          if (baseNode.isNodeType(NodeType.BOFolder) || baseNode.isNodeType(NodeType.DTFolder))
            baseNode.refresh();
        }
      }
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Refreshed Namespace in: ");
    }
  }
}
