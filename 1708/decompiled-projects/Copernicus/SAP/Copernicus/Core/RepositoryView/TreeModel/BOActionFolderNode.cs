﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BOActionFolderNode
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BOActionFolderNode : BaseNode
  {
    public string nodeProxyName;
    public string nodeName;

    public BOActionFolderNode(string nodeProxyName, string nodeName)
      : this((DataRow) null)
    {
      this.nodeProxyName = nodeProxyName;
      this.nodeName = nodeName;
      this.ImageIndex = 5;
      this.SelectedImageIndex = 5;
    }

    public BOActionFolderNode(DataRow dataRow)
      : base(dataRow)
    {
      this.Text = CopernicusResources.TreeNodeTextActionFolder;
      this.ImageIndex = 5;
      this.SelectedImageIndex = 5;
    }

    public override NodeType getNodeType()
    {
      return NodeType.ActionFolder;
    }
  }
}
