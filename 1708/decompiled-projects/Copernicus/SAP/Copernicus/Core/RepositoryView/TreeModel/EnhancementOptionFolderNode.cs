﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.EnhancementOptionFolderNode
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository;
using System;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class EnhancementOptionFolderNode : BaseNode
  {
    public EnhancementOptionFolderNode()
      : this((DataRow) null)
    {
    }

    public EnhancementOptionFolderNode(DataRow dataRow)
      : base(dataRow)
    {
      this.Text = CopernicusResources.BAdIFolderName;
      this.ImageIndex = 9;
      this.SelectedImageIndex = 9;
    }

    public override NodeType getNodeType()
    {
      return NodeType.EnhancementOptionFolder;
    }

    public override void refresh()
    {
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Refreshing " + (object) this.getNodeType() + " (" + this.Namespace + ") ...");
      string nsName = this.Namespace;
      RepositoryDataCache.GetInstance().RefreshEnhancementOptionFolderNode(nsName);
      this.getRepositoryViewControl().getTreeBuilder().refreshEnhancementOptionsFolder((BaseNode) this, nsName);
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Refreshed in: ");
    }
  }
}
