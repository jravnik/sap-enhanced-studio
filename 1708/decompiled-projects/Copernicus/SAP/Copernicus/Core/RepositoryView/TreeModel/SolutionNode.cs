﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.SolutionNode
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Protocol;
using System;
using System.Data;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  [ComVisible(true)]
  [CLSCompliant(false)]
  public class SolutionNode : BaseNode
  {
    public string solutionName;
    public string solutionDescription;
    public string solutionType;
    public string pdiSubType;
    public string solutionVersion;
    public string devPartner;
    public string contactPerson;
    public string deploymentUnit;
    public string eMail;
    public bool patchSolution;

    public SolutionNode(string solutionNodeName, string solutionDescription, string version, string solutionType, string pdiSubType, string patch, bool deleted, string devPartner, string contactPerson, string email, string deploymentUnit)
      : base((DataRow) null)
    {
      this.solutionName = solutionNodeName;
      this.solutionDescription = solutionDescription;
      this.pdiSubType = pdiSubType;
      this.solutionType = solutionType;
      this.solutionVersion = version;
      this.devPartner = devPartner;
      this.contactPerson = contactPerson;
      this.eMail = email;
      this.deploymentUnit = deploymentUnit;
      this.patchSolution = !string.IsNullOrEmpty(patch);
      if (Connection.getInstance().UsageMode == UsageMode.OneOff || Connection.getInstance().UsageMode == UsageMode.miltiCustomer)
      {
        if (string.IsNullOrEmpty(solutionDescription))
          this.Text = this.solutionName;
        else
          this.Text = solutionDescription + " (" + this.solutionName + ")";
      }
      else
        this.Text = this.solutionName;
      this.ImageIndex = EntityImageList.getImageIndexForSolution(this.solutionName, pdiSubType, this.patchSolution);
      this.SelectedImageIndex = this.ImageIndex;
      if (deleted)
        this.ToolTipText = Resource.TTDelPending;
      else if (this.solutionName.StartsWith("Z"))
        this.ToolTipText = Resource.TTSandboxSol;
      else if (this.pdiSubType == "10" && this.patchSolution)
        this.ToolTipText = Resource.TTPatchSolution;
      else if (this.pdiSubType == "TS")
      {
        this.ToolTipText = Resource.TTTemplateSolution;
      }
      else
      {
        if (!(this.pdiSubType == "GS"))
          return;
        this.ToolTipText = Resource.TTGlobalSol;
      }
    }

    public override NodeType getNodeType()
    {
      return NodeType.Solution;
    }
  }
}
