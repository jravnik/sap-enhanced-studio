﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.ContextMenu.ContextMenuBuilder
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.RepositoryView.TreeModel;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.RepositoryView.ContextMenu
{
  public static class ContextMenuBuilder
  {
    private static readonly ContextMenuHandler handler = new ContextMenuHandler();
    private static readonly ContextMenuStrip NamespaceContextMenuStrip = ContextMenuBuilder.CreateContextMenuStripForNamespace();
    private static readonly ContextMenuStrip BOFolderContextMenuStrip = ContextMenuBuilder.CreateContextMenuStripForBOFolder();
    private static readonly ContextMenuStrip DTFolderContextMenuStrip = ContextMenuBuilder.CreateContextMenuStripForDTFolder();
    private static readonly ContextMenuStrip DTContextMenuStrip = ContextMenuBuilder.CreateContextMenuStripForDT();
    private static readonly ContextMenuStrip BOContextMenuStrip = ContextMenuBuilder.CreateContextMenuStripForBO();
    private static readonly ContextMenuStrip BAdIFolderContextMenuStrip = ContextMenuBuilder.CreateContextMenuStripForBAdI();
    private static readonly ContextMenuStrip MySolutionsContextMenuStrip = ContextMenuBuilder.CreateContextMenuStripForMySolutions();

    private static ToolStripMenuItem CreateMenuItem(string name, string text, Bitmap image, bool enabled, EventHandler handlerTarget)
    {
      ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem();
      toolStripMenuItem.Name = name;
      toolStripMenuItem.Text = text;
      if (image != null)
        toolStripMenuItem.Image = (Image) image;
      toolStripMenuItem.Enabled = enabled;
      toolStripMenuItem.Click += handlerTarget;
      return toolStripMenuItem;
    }

    private static ToolStripMenuItem CreateRefreshMenuItem(bool enabled)
    {
      return ContextMenuBuilder.CreateMenuItem("refresh", CopernicusResources.TNCtxMenuRefresh, CopernicusResources.Refresh, enabled, new EventHandler(ContextMenuBuilder.handler.refresh_Click));
    }

    private static ToolStripMenuItem CreateNewPartnerSolution(bool enabled)
    {
      return ContextMenuBuilder.CreateMenuItem("Create Solution", CopernicusResources.MySolCtxMenuCreateSol, CopernicusResources.CreateSolution, enabled, new EventHandler(ContextMenuBuilder.handler.CreateSolution_Click));
    }

    private static ToolStripMenuItem OpenPartnerSolution(bool enabled)
    {
      return ContextMenuBuilder.CreateMenuItem("Open Solution", Resource.SolutionCtxMenuOpen, CopernicusResources.OpenSolution, enabled, new EventHandler(ContextMenuBuilder.handler.OpenSolution_Click));
    }

    private static ToolStripMenuItem PartnerSolutionProperties(bool enabled)
    {
      return ContextMenuBuilder.CreateMenuItem("Solution Properties", Resource.SolutionCtxMenuSolutionProperties, CopernicusResources.Properties, enabled, new EventHandler(ContextMenuBuilder.handler.SolutionProperty_Click));
    }

    private static ToolStripMenuItem DeletePartnerSolution(bool enabled)
    {
      return ContextMenuBuilder.CreateMenuItem("Delete Solution", CopernicusResources.TNCtxMenuDeleteSolution, CopernicusResources.DeleteSolution, enabled, new EventHandler(ContextMenuBuilder.handler.DeleteSolution_Click));
    }

    private static ContextMenuStrip CreateContextMenuStripForNamespace()
    {
      ContextMenuStrip contextMenuStrip = (ContextMenuStrip) new RepositoryContextMenuStrip();
      contextMenuStrip.Name = "contextMenuStripForNamespace";
      contextMenuStrip.Items.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) ContextMenuBuilder.CreateRefreshMenuItem(true)
      });
      return contextMenuStrip;
    }

    private static ContextMenuStrip CreateContextMenuStripForBOFolder()
    {
      ContextMenuStrip contextMenuStrip = (ContextMenuStrip) new RepositoryContextMenuStrip();
      contextMenuStrip.Name = "contextMenuStripForBOFolder";
      contextMenuStrip.Items.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) ContextMenuBuilder.CreateRefreshMenuItem(true)
      });
      return contextMenuStrip;
    }

    private static ContextMenuStrip CreateContextMenuStripForDTFolder()
    {
      ContextMenuStrip contextMenuStrip = (ContextMenuStrip) new RepositoryContextMenuStrip();
      contextMenuStrip.Name = "contextMenuStripForDTFolder";
      contextMenuStrip.Items.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) ContextMenuBuilder.CreateRefreshMenuItem(true)
      });
      return contextMenuStrip;
    }

    private static ContextMenuStrip CreateContextMenuStripForDT()
    {
      ContextMenuStrip contextMenuStrip = (ContextMenuStrip) new RepositoryContextMenuStrip();
      contextMenuStrip.Name = "contextMenuStripForDT";
      contextMenuStrip.Items.AddRange(new ToolStripItem[0]);
      return contextMenuStrip;
    }

    private static ContextMenuStrip CreateContextMenuStripForBAdI()
    {
      ContextMenuStrip contextMenuStrip = (ContextMenuStrip) new RepositoryContextMenuStrip();
      contextMenuStrip.Name = "contextMenuStripForBAdIFolder";
      contextMenuStrip.Items.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) ContextMenuBuilder.CreateRefreshMenuItem(true)
      });
      return contextMenuStrip;
    }

    private static ContextMenuStrip CreateContextMenuStripForBO()
    {
      ContextMenuStrip contextMenuStrip = (ContextMenuStrip) new RepositoryContextMenuStrip();
      contextMenuStrip.Name = "contextMenuStripForBO";
      contextMenuStrip.Items.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) ContextMenuBuilder.CreateRefreshMenuItem(true)
      });
      return contextMenuStrip;
    }

    private static ContextMenuStrip CreateContextMenuStripForMySolutions()
    {
      ContextMenuStrip contextMenuStrip = (ContextMenuStrip) new RepositoryContextMenuStrip();
      contextMenuStrip.Name = "contextMenuStripForMySolutions";
      bool enabled = true;
      if ((Connection.getInstance().UsageMode == UsageMode.OneOff || Connection.getInstance().UsageMode == UsageMode.miltiCustomer) && Connection.getInstance().SystemMode == SystemMode.Productive)
        enabled = false;
      contextMenuStrip.Items.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) ContextMenuBuilder.CreateNewPartnerSolution(enabled)
      });
      return contextMenuStrip;
    }

    private static ContextMenuStrip CreateContextMenuStripForSolution(BaseNode treeNode)
    {
      ContextMenuStrip contextMenuStrip = (ContextMenuStrip) new RepositoryContextMenuStrip();
      contextMenuStrip.Name = "contextMenuStripForSolution";
      SolutionNode solutionNode = (SolutionNode) treeNode;
      int num = treeNode.Text.IndexOf('(');
      string solution = treeNode.Text.Substring(num + 1, 10);
      bool enabled;
      if (Connection.getInstance().UsageMode == UsageMode.OneOff || Connection.getInstance().UsageMode == UsageMode.miltiCustomer)
      {
        if (Connection.getInstance().SystemMode == SystemMode.Productive)
        {
          enabled = false;
        }
        else
        {
          enabled = true;
          if (XRepMapper.GetInstance().isKeyUserSolution(solution))
            enabled = false;
        }
      }
      else
        enabled = Connection.getInstance().UsageMode == UsageMode.Scalable && RepositoryDataCache.GetInstance().getSolutionStatus(solutionNode.solutionName, solutionNode.solutionVersion) == "A";
      if (treeNode.SelectedImageIndex == 14)
        contextMenuStrip.Items.AddRange(new ToolStripItem[2]
        {
          (ToolStripItem) ContextMenuBuilder.DeletePartnerSolution(enabled),
          (ToolStripItem) ContextMenuBuilder.PartnerSolutionProperties(true)
        });
      else
        contextMenuStrip.Items.AddRange(new ToolStripItem[3]
        {
          (ToolStripItem) ContextMenuBuilder.OpenPartnerSolution(true),
          (ToolStripItem) ContextMenuBuilder.DeletePartnerSolution(enabled),
          (ToolStripItem) ContextMenuBuilder.PartnerSolutionProperties(true)
        });
      return contextMenuStrip;
    }

    public static void AddContextMenuStrip(BaseNode treeNode)
    {
      switch (treeNode.getNodeType())
      {
        case NodeType.MySolutions:
          treeNode.ContextMenuStrip = ContextMenuBuilder.MySolutionsContextMenuStrip;
          break;
        case NodeType.Solution:
          treeNode.ContextMenuStrip = ContextMenuBuilder.CreateContextMenuStripForSolution(treeNode);
          break;
        case NodeType.Namespace:
          treeNode.ContextMenuStrip = ContextMenuBuilder.NamespaceContextMenuStrip;
          break;
        case NodeType.BOFolder:
          treeNode.ContextMenuStrip = ContextMenuBuilder.BOFolderContextMenuStrip;
          break;
        case NodeType.DTFolder:
          treeNode.ContextMenuStrip = ContextMenuBuilder.DTFolderContextMenuStrip;
          break;
        case NodeType.DataType:
          treeNode.ContextMenuStrip = ContextMenuBuilder.DTContextMenuStrip;
          break;
        case NodeType.EnhancementOptionFolder:
          treeNode.ContextMenuStrip = ContextMenuBuilder.BAdIFolderContextMenuStrip;
          break;
      }
    }

    public static void RegisterMouseClickHandler(TreeView treeView)
    {
      treeView.NodeMouseClick -= new TreeNodeMouseClickEventHandler(ContextMenuBuilder.handler.treeNodeMouseClick);
      treeView.NodeMouseDoubleClick -= new TreeNodeMouseClickEventHandler(ContextMenuBuilder.handler.treeView_NodeMouseDoubleClick);
      treeView.KeyPress -= new KeyPressEventHandler(ContextMenuBuilder.handler.treeView_KeyPress);
      treeView.NodeMouseClick += new TreeNodeMouseClickEventHandler(ContextMenuBuilder.handler.treeNodeMouseClick);
      treeView.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(ContextMenuBuilder.handler.treeView_NodeMouseDoubleClick);
      treeView.KeyPress += new KeyPressEventHandler(ContextMenuBuilder.handler.treeView_KeyPress);
    }
  }
}
