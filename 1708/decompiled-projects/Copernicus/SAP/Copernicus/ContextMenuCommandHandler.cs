﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ContextMenuCommandHandler
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using CopernicusBusinessObjectBrowser;
using EnvDTE80;
using Microsoft.VisualStudio.Modeling;
using Microsoft.VisualStudio.Modeling.Validation;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.ApprovalStepNS.ApprovalTaskAPI;
using SAP.Copernicus.ApprovalStepNS.Wizard;
using SAP.Copernicus.ASUtil;
using SAP.Copernicus.BOXSGeneration.BO_Service;
using SAP.Copernicus.BOXSGeneration.ExternalUIEditor.Wizard;
using SAP.Copernicus.BusinessConfiguration;
using SAP.Copernicus.BusinessConfiguration.BusinessConfigurationNode;
using SAP.Copernicus.BusinessConfiguration.Wizard;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.PDI;
using SAP.Copernicus.Core.ProductionBugFix;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Protocol.JSON;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Core.Util.Forms;
using SAP.Copernicus.CustomReuseLibrary.LibraryModel;
using SAP.Copernicus.CustomReuseLibrary.Utilities;
using SAP.Copernicus.CustomReuseLibrary.Wizard;
using SAP.Copernicus.Extension.EmbeddedComponent;
using SAP.Copernicus.Extension.ScriptFilesSelection;
using SAP.Copernicus.Extension.SelectionUtil;
using SAP.Copernicus.Extension.UIDesignerHelper;
using SAP.Copernicus.IntegrationScenario;
using SAP.Copernicus.IntegrationScenario.IntegrationScenario;
using SAP.Copernicus.MassDataRun.MDRO;
using SAP.Copernicus.Model;
using SAP.Copernicus.Model.BusinessObject;
using SAP.Copernicus.NotificationRules;
using SAP.Copernicus.NotificationRules.Wizard;
using SAP.Copernicus.OneOff.MultiCustomer;
using SAP.Copernicus.PrintForms.FormUtil;
using SAP.Copernicus.ProcessIntegration;
using SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.Controls;
using SAP.Copernicus.ProcessIntegration.ExternalWebServiceExtension.ReuseLibrary;
using SAP.Copernicus.QueryDefinition;
using SAP.Copernicus.QueryDefinition.Model;
using SAP.Copernicus.QueryDefinition.Wizard;
using SAP.Copernicus.ScreenUtil.Wizard.TTReferenceWizard;
using SAP.Copernicus.Translation;
using SAP.Copernicus.Translation.Resources;
using SAP.Copernicus.uidesigner.integration.model;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.CompilerVersionDialog;
using SAP.CopernicusProjectView.Templates.ProjectItems.ODataService;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SAP.Copernicus
{
  internal class ContextMenuCommandHandler
  {
    public static bool background_mode_on = false;
    public static SolutionHandler solutionHandler = new SolutionHandler();
    private static bool isGenericTask = false;
    private static string webServiceUrl = string.Empty;
    private static string serviceXRepPath = string.Empty;
    internal const string ImplementationProjectTemplateOverviewUI = "/SAP_BYD_APPLICATION_UI/BCTools/WorkCentre/WOC_ProjectOverview_FS.FS.uicomponent";
    internal const string ImplementationProjectTemplateOverviewUIInport = "ReadProject";
    internal const string WorkSpaceID = "WorkSpaceID";
    internal const string OperationType_Generate_BO = "generate_for_bo";

    public static void QuerySolExpMenuCommand_BeforeQueryStatus(object sender, EventArgs e)
    {
      OleMenuCommand command = sender as OleMenuCommand;
      if (command == null)
        return;
      uint pcItems = 0;
      int pfSingleHierarchy = 0;
      IntPtr ppHier;
      uint pitemid;
      IVsMultiItemSelect ppMIS;
      IntPtr ppSC;
      ((IVsMonitorSelection) Package.GetGlobalService(typeof (SVsShellMonitorSelection))).GetCurrentSelection(out ppHier, out pitemid, out ppMIS, out ppSC);
      if (ppMIS != null)
      {
        ppMIS.GetSelectionInfo(out pcItems, out pfSingleHierarchy);
        if (pcItems > 1U)
        {
          command.Visible = false;
          return;
        }
      }
      if (ppHier.ToInt32() != 0)
      {
        IVsHierarchy objectForIunknown = Marshal.GetTypedObjectForIUnknown(ppHier, typeof (IVsHierarchy)) as IVsHierarchy;
        if (objectForIunknown != null)
        {
          object pvar;
          objectForIunknown.GetProperty(pitemid, -2012, out pvar);
          if (pvar == null)
            return;
          string str = pvar.ToString();
          string extension = "";
          if ((int) pitemid != -2 && str.Contains("."))
            extension = str.Substring(str.LastIndexOf('.'));
          ContextMenuCommandHandler.DecideCommandStatusOnExtension(extension, command);
        }
        else
          command.Visible = false;
      }
      else
        command.Visible = false;
    }

    private static bool IsCheckinEnabled()
    {
      if ((!Path.GetExtension(CopernicusProjectSystemUtil.GetXRepPathforSelectedNode()).Equals(".absl") || !CopernicusProjectSystemUtil.isProductionChangeAllowed()) && !XRepMapper.GetInstance().IsSolutionEditable())
        return false;
      FileNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode() as FileNode;
      if (selectedNode == null)
        return false;
      CopernicusProjectNode projectMgr = selectedNode.ProjectMgr as CopernicusProjectNode;
      return projectMgr != null && !(selectedNode is CopernicusVirtualNode) && projectMgr.IsLocked(selectedNode) == LockStatus.LockedByMe;
    }

    private static bool IsCheckoutEnabled()
    {
      if ((!Path.GetExtension(CopernicusProjectSystemUtil.GetXRepPathforSelectedNode()).Equals(".absl") || !CopernicusProjectSystemUtil.isProductionChangeAllowed()) && !XRepMapper.GetInstance().IsSolutionEditable())
        return false;
      FileNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode() as FileNode;
      if (selectedNode == null)
        return false;
      CopernicusProjectNode projectMgr = selectedNode.ProjectMgr as CopernicusProjectNode;
      return projectMgr != null && !(selectedNode is CopernicusVirtualNode) && projectMgr.IsLocked(selectedNode) != LockStatus.LockedByMe;
    }

    public static void QuerySolExpToolbarCommand_BeforeQueryStatus(object sender, EventArgs e)
    {
      OleMenuCommand oleMenuCommand = sender as OleMenuCommand;
      oleMenuCommand.Visible = true;
      if (oleMenuCommand.CommandID.ID == 402 && XRepMapper.GetInstance().Iskeyusersoln)
        oleMenuCommand.Visible = false;
      else if (SAP.Copernicus.Core.Protocol.Connection.getInstance().SystemMode == SystemMode.Productive && PkgCmdIDList.CommandsCausingChange().Contains((uint) oleMenuCommand.CommandID.ID) && !PkgCmdIDList.CommandsEnabledForProductionBugFix().Contains((uint) oleMenuCommand.CommandID.ID))
        oleMenuCommand.Visible = false;
      else if ((SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.OneOff || SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.miltiCustomer) && !PkgCmdIDList.CommandsEnabledForOneOff().Contains((uint) oleMenuCommand.CommandID.ID))
        oleMenuCommand.Visible = false;
      else if ((SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.OneOff || SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.miltiCustomer) && (XRepMapper.GetInstance().GetOpenSolutionStatus() != "In Development" && PkgCmdIDList.CommandsCausingChange().Contains((uint) oleMenuCommand.CommandID.ID)) && !PkgCmdIDList.CommandsEnabledForProductionBugFix().Contains((uint) oleMenuCommand.CommandID.ID))
      {
        oleMenuCommand.Visible = false;
      }
      else
      {
        CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
        if (selectedProject == null || (SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.OneOff || SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.miltiCustomer) && ((long) oleMenuCommand.CommandID.ID == 403L && NewCompilerVersionDialog.useBBC(selectedProject.CompilerVersion)))
          oleMenuCommand.Visible = false;
        else if (SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.Scalable && (XRepMapper.GetInstance().GetSolutionEditStatus() != XRepMapper.EditModes.InMaintenance || !PkgCmdIDList.CommandsCausingChange().Contains((uint) oleMenuCommand.CommandID.ID)) && (XRepMapper.GetInstance().GetOpenSolutionStatus() != "In Development" && PkgCmdIDList.CommandsCausingChange().Contains((uint) oleMenuCommand.CommandID.ID)))
        {
          oleMenuCommand.Visible = false;
        }
        else
        {
          if (!PkgCmdIDList.CommandsEnabledForProductionBugFix().Contains((uint) oleMenuCommand.CommandID.ID))
            return;
          if (!XRepMapper.GetInstance().IsProductionBugFixEnabled)
          {
            oleMenuCommand.Visible = false;
          }
          else
          {
            oleMenuCommand.Enabled = false;
            if (oleMenuCommand.CommandID.ID == 406)
            {
              if (!(XRepMapper.GetInstance().GetOpenSolutionStatus() == "Deployed"))
                return;
              oleMenuCommand.Enabled = true;
            }
            else
            {
              if (oleMenuCommand.CommandID.ID != 407 || !(XRepMapper.GetInstance().GetOpenSolutionStatus() == "Deployed-In Correction"))
                return;
              oleMenuCommand.Enabled = true;
            }
          }
        }
      }
    }

    public static void DecideCommandStatusOnExtension(string extension, OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      command.Visible = false;
      command.Enabled = false;
      switch (extension)
      {
        case "":
          ContextMenuCommandHandler.CheckCommandStatusForProjectNode(command);
          break;
        case ".bo":
          ContextMenuCommandHandler.CheckCommandStatusForBO(command);
          break;
        case ".absl":
          ContextMenuCommandHandler.CheckCommandStatusForScriptFile(command);
          break;
        case ".xbo":
          ContextMenuCommandHandler.CheckCommandStatusForXBO(command);
          break;
        case ".approvalprocess":
          ContextMenuCommandHandler.CheckCommandStatusForApprovalProcess(command);
          break;
        case ".notification":
          ContextMenuCommandHandler.CheckCommandStatusForNotification(command);
          break;
        case ".approval":
          ContextMenuCommandHandler.CheckCommandStatusForApproval(command);
          break;
        case ".sam":
        case ".diagram":
          ContextMenuCommandHandler.CheckCommandStatusForSAM(command);
          break;
        case ".webservice":
          ContextMenuCommandHandler.CheckCommandStatusForWSDefinition(command);
          break;
        case ".xodata":
          ContextMenuCommandHandler.CheckCommandStatusForXODataService(command);
          break;
        case ".wsauth":
          ContextMenuCommandHandler.CheckCommandStatusForWSAUTH(command);
          break;
        case ".wsdl":
          ContextMenuCommandHandler.CheckCommandStatusForWSDL(command);
          break;
        case ".wsid":
          ContextMenuCommandHandler.CheckCommandStatusForWSID(command);
          break;
        case ".xuicomponent":
          ContextMenuCommandHandler.CheckCommandStatusForXUINode(command);
          break;
        case ".EC.uicomponent":
          ContextMenuCommandHandler.CheckCommandStatusForEmbComp(command);
          break;
        case ".uicomponent":
          ContextMenuCommandHandler.CheckCommandStatusForUINode(command);
          break;
        case ".uiwocview":
          ContextMenuCommandHandler.CheckCommandStatusForUIWOCVIEW(command);
          break;
        case ".uiwoc":
        case ".uidataobject":
          ContextMenuCommandHandler.CheckCommandStatusForUIWOC(command);
          break;
        case ".uiswitch":
          ContextMenuCommandHandler.CheckCommandStatusForUISwitch(command);
          break;
        case ".xdp":
          ContextMenuCommandHandler.CheckCommandStatusForFormsXDP(command);
          break;
        case ".fthd":
          ContextMenuCommandHandler.CheckCommandStatusForFormsFTHD(command);
          break;
        case ".ftgd":
          ContextMenuCommandHandler.CheckCommandStatusForFormsFTGD(command);
          break;
        case ".xft":
          ContextMenuCommandHandler.CheckCommandStatusForFormsXFT(command);
          break;
        case ".qry":
          ContextMenuCommandHandler.CheckCommandStatusForQueryNode(command);
          break;
        case ".ds":
          ContextMenuCommandHandler.CheckCommandStatusForDataSourceNode(command);
          break;
        case ".report":
          ContextMenuCommandHandler.CheckCommandStatusForAnalyticsReportNode(command);
          break;
        case ".kf":
          ContextMenuCommandHandler.CheckCommandStatusForAnalyticsKeyFigureNode(command);
          break;
        case ".cds":
          ContextMenuCommandHandler.CheckCommandStatusForAnalyticsCombinedDSNode(command);
          break;
        case ".jds":
          ContextMenuCommandHandler.CheckCommandStatusForAnalyticsJoinDSNode(command);
          break;
        case ".pid":
          ContextMenuCommandHandler.CheckCommandStatusForPINode(command);
          break;
        case ".csd":
          ContextMenuCommandHandler.CheckCommandStatusForCSDNode(command);
          break;
        case ".run":
          ContextMenuCommandHandler.CheckCommandStatusForMDRO(command);
          break;
        case ".enht":
          ContextMenuCommandHandler.CheckCommandStatusForBADI_IMPL(command);
          break;
        case ".fltr":
          ContextMenuCommandHandler.CheckCommandStatusForBADI_FILTER(command);
          break;
        case ".xs":
          ContextMenuCommandHandler.CheckCommandStatusForExtensionScenario(command);
          break;
        case ".nxs":
          ContextMenuCommandHandler.CheckCommandStatusForNodeExtensionScenario(command);
          break;
        case ".bac":
          ContextMenuCommandHandler.CheckCommandStatusForBCNode(command);
          break;
        case ".bco":
          ContextMenuCommandHandler.CheckCommandStatusForBCONode(command);
          break;
        case ".bcc":
          ContextMenuCommandHandler.CheckCommandStatusForBCSetNode(command);
          break;
        case ".bcctax":
          ContextMenuCommandHandler.CheckCommandStatusForBCSetNode(command);
          break;
        case ".bct":
          ContextMenuCommandHandler.CheckCommandStatusForImplProjTemplNode(command);
          break;
        case ".codelist":
          ContextMenuCommandHandler.CheckCommandStatusForCodeList(command);
          break;
        case ".library":
          ContextMenuCommandHandler.CheckCommandStatusForCustomReuseLibrary(command);
          break;
        case ".ref":
          ContextMenuCommandHandler.CheckCommandStatusForCustomerObjectReferences(command);
          break;
        case ".bott":
          ContextMenuCommandHandler.CheckCommandStatusForBO_TTReference(command);
          break;
        case ".xwebservice":
          ContextMenuCommandHandler.CheckCommandStatusForExternalWebserviceExtension(command);
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForXODataService(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 1862:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 8504:
          command.Visible = false;
          command.Enabled = false;
          break;
        case 8505:
          command.Visible = false;
          command.Enabled = false;
          break;
        case 24577:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForExternalWebserviceExtension(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 4472:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.isSelectedObjectsRuntimeObjectUptoDate() && XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForUISwitch(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      switch (id)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForCodeList(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      switch (id)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void SetCommandvisibilityforSolExpToolBar(OleMenuCommand command)
    {
      if (CopernicusProjectSystemUtil.getSelectedProject() != null)
        command.Visible = true;
      else
        command.Visible = false;
    }

    private static bool CheckTransaltionCommandStatusByEditMode(int commandID)
    {
      switch (commandID)
      {
        case 13668:
        case 13670:
        case 13671:
        case 13673:
          return true;
        case 13669:
        case 13672:
          return XRepMapper.GetInstance().GetSolutionEditStatus() != XRepMapper.EditModes.Locked;
        default:
          return false;
      }
    }

    private static void CheckCommandStatusForXBO(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      switch (id)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 261:
          if (XRepMapper.GetInstance().isFeatureAvailable("XBO_ENHANCE_SCREEN"))
          {
            command.Visible = true;
            command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
            break;
          }
          command.Visible = false;
          command.Enabled = false;
          break;
        case 306:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 320:
          if (XRepMapper.GetInstance().isFeatureAvailable("XBO_ENHANCE_FORM"))
          {
            command.Visible = true;
            command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
            break;
          }
          command.Visible = false;
          command.Enabled = false;
          break;
        case 321:
          if (XRepMapper.GetInstance().isFeatureAvailable("XBO_ENHANCE_REPORT"))
          {
            command.Visible = true;
            command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
            break;
          }
          command.Visible = false;
          command.Enabled = false;
          break;
        case 322:
          if (XRepMapper.GetInstance().isFeatureAvailable("XBO_ENHANCE_ENTERPRISE_SEARCH"))
          {
            command.Visible = true;
            command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
            break;
          }
          command.Visible = false;
          command.Enabled = false;
          break;
        case 323:
          command.Visible = ContextMenuCommandHandler.CheckXBOMigrateCommandStatusByExtendingBOName();
          command.Enabled = ContextMenuCommandHandler.CheckXBOMigrateCommandStatusByExtendingBOName();
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static bool CheckXBOMigrateCommandStatusByExtendingBOName()
    {
      bool flag = true;
      if (!XRepMapper.GetInstance().IsSolutionEditable())
        return false;
      string pathforSelectedNode = CopernicusProjectSystemUtil.GetXRepPathforSelectedNode();
      if (!XREPattribs.getAttrib(pathforSelectedNode, "PRX_BO_NAME").Equals("BUSINESS_ACTIVITY") || !XREPattribs.getAttrib(pathforSelectedNode, "esrBOName").Equals("Activity") || !XREPattribs.getAttrib(pathforSelectedNode, "esrBONameSpace").Equals("AP.PC.ActivityManagement.Global"))
        flag = false;
      return flag;
    }

    private static void CheckCommandStatusForEmbComp(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      switch (id)
      {
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 293:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.isFloorPlanEditable();
          break;
        case 352:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForBO(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      CopernicusProjectFileNode boNode = (CopernicusProjectFileNode) null;
      string boProxyName = "";
      RepositoryDataCache instance1 = RepositoryDataCache.GetInstance();
      switch (id)
      {
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 4471:
          bool flag1 = ContextMenuCommandHandler.checkIfSelectedBOISActivated(instance1, ref boProxyName, ref boNode);
          command.Visible = true;
          command.Enabled = flag1;
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 535:
          command.Visible = true;
          CopernicusProjectFileNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode() as CopernicusProjectFileNode;
          RepositoryDataCache instance2 = RepositoryDataCache.GetInstance();
          string proxyNameFromBo = instance2.GetProxyNameFromBO(CopernicusProjectSystemUtil.GetNamespaceForSelectedProject(), Path.GetFileNameWithoutExtension(selectedNode.FileName));
          if (ContextMenuCommandHandler.CheckBOHasMSApproval() || instance2.GetLifeCycleStatusForBO(proxyNameFromBo) != "1" || !XRepMapper.GetInstance().IsSolutionEditable())
            command.Enabled = false;
          else
            command.Enabled = true;
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 1299:
        case 4133:
        case 292:
        case 313:
        case 336:
        case 277:
        case 278:
        case 279:
        case 281:
        case 288:
          if (id == 313 && !XRepMapper.GetInstance().isItemAvailable("NOTIFICATION_RULE"))
            command.Visible = false;
          else if (id == 278 && !XRepMapper.GetInstance().isItemAvailable("FORMS"))
            command.Visible = false;
          else if (id == 1299 && !XRepMapper.GetInstance().isItemAvailable("MDRO"))
            command.Visible = false;
          else
            command.Visible = true;
          bool flag2 = ContextMenuCommandHandler.checkIfSelectedBOISActivated(instance1, ref boProxyName, ref boNode);
          command.Enabled = flag2;
          if (id == 313 && command.Enabled)
            command.Enabled = !NotificationUtils.checkNotifExists(boProxyName);
          if (id == 1299 && command.Enabled)
            command.Enabled = MDROUtils.getValidBONodes(boProxyName).Count > 0;
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 260:
          command.Visible = true;
          if (XRepMapper.GetInstance().IsSolutionEditable())
          {
            bool flag3 = ContextMenuCommandHandler.checkIfSelectedBOISActivated(instance1, ref boProxyName, ref boNode);
            command.Enabled = flag3;
            break;
          }
          command.Enabled = false;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static bool checkIfSelectedBOISActivated(RepositoryDataCache repositoryDataCache, ref string boProxyName, ref CopernicusProjectFileNode boNode)
    {
      if (boNode == null)
        boNode = CopernicusProjectSystemUtil.getSelectedNode() as CopernicusProjectFileNode;
      boProxyName = repositoryDataCache.GetProxyNameFromBO(CopernicusProjectSystemUtil.GetNamespaceForSelectedProject(), Path.GetFileNameWithoutExtension(boNode.FileName));
      return repositoryDataCache.GetLifeCycleStatusForBO(boProxyName) == "1" && XRepMapper.GetInstance().IsSolutionEditable();
    }

    private static void CheckCommandStatusForBCSetNode(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      switch (id)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForImplProjTemplNode(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      CopernicusBusinessConfigurationUtil.ImplementationProjectTemplate template = CopernicusBusinessConfigurationUtil.LoadFileToTemplate(CopernicusProjectSystemUtil.getSelectedNode().Url);
      string str1 = "New";
      string str2 = "Active";
      string str3 = "Complete";
      string str4 = string.Concat((object) 10);
      switch (id)
      {
        case 769:
          if (template.Type.Equals(str4))
          {
            command.Visible = false;
            break;
          }
          command.Visible = true;
          if (template.Status.Equals(str1))
          {
            command.Enabled = true;
            break;
          }
          command.Enabled = false;
          break;
        case 770:
          if (template.Type.Equals(str4))
          {
            command.Visible = false;
            break;
          }
          command.Visible = true;
          if (template.Status.Equals(str2))
          {
            command.Enabled = true;
            break;
          }
          command.Enabled = false;
          break;
        case 771:
          if (template.Type.Equals(str4))
          {
            command.Visible = false;
            break;
          }
          command.Visible = true;
          if (template.Status.Equals(str3))
          {
            command.Enabled = true;
            break;
          }
          command.Enabled = false;
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForBCONode(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 272:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 273:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.isSelectedObjectsRuntimeObjectUptoDate() && XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForBCNode(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      switch (id)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForScriptFile(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 2457:
          if (CopernicusProjectSystemUtil.isProductionChangeAllowed())
          {
            command.Visible = true;
            command.Enabled = true;
            break;
          }
          command.Visible = false;
          command.Enabled = false;
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
          command.Visible = true;
          if (CopernicusProjectSystemUtil.isProductionChangeAllowed())
          {
            command.Enabled = true;
            break;
          }
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 24580:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForCustomReuseLibrary(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 257:
          if (ContextMenuCommandHandler.isParentXWebservice())
          {
            command.Visible = false;
            command.Enabled = false;
            break;
          }
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 8504:
          if (ContextMenuCommandHandler.isParentXWebservice())
          {
            command.Visible = false;
            command.Enabled = false;
            break;
          }
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          if (ContextMenuCommandHandler.isParentXWebservice())
          {
            command.Visible = false;
            command.Enabled = false;
            break;
          }
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static bool isParentXWebservice()
    {
      return Path.GetExtension(CopernicusProjectSystemUtil.getSelectedNode().Parent.Url) == ".xwebservice";
    }

    private static void CheckCommandStatusForMDRO(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 1298:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.isSelectedObjectsRuntimeObjectUptoDate();
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForBADI_IMPL(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForBADI_FILTER(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForExtensionScenario(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForNodeExtensionScenario(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForCustomerObjectReferences(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static bool CheckBOHasMSApproval()
    {
      return ContextMenuCommandHandler.CheckBOHasMSApprovalRecursive(CopernicusProjectSystemUtil.getSelectedNode());
    }

    private static bool CheckBOHasMSApprovalRecursive(HierarchyNode node)
    {
      if (node.Url.EndsWith(".approvalprocess") || node.Url.EndsWith(".approval"))
        return true;
      for (HierarchyNode node1 = node.FirstChild; node1 != null; node1 = node1.NextSibling)
      {
        if (ContextMenuCommandHandler.CheckBOHasMSApprovalRecursive(node1))
          return true;
      }
      return false;
    }

    private static void CheckCommandStatusForApprovalProcess(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 310:
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForNotification(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      switch (id)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForApproval(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 310:
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForSAM(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForWSDefinition(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 4181:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.isSelectedObjectsRuntimeObjectUptoDate();
          break;
        case 5476:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.hasNoChildNode();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForWSAUTH(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static bool hasNoChildNode()
    {
      return CopernicusProjectSystemUtil.getSelectedNode().Url.EndsWith(".webservice") && CopernicusProjectSystemUtil.getSelectedNode().FirstChild == null;
    }

    private static void CheckCommandStatusForWSDL(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForWSID(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 5480:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 5490:
          string empty = string.Empty;
          if (SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(CopernicusProjectSystemUtil.getSelectedNode().Url).TryGetValue(SAP.Copernicus.IntegrationScenario.Resources.WebServiceType, out empty) && empty == WebServiceTypeCode.Rest.ToString())
          {
            command.Visible = false;
            command.Enabled = false;
            break;
          }
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.isSelectedObjectsRuntimeObjectUptoDate() && XRepMapper.GetInstance().IsSolutionEditable();
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForXUINode(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 293:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.isFloorPlanEditable();
          break;
        case 294:
          command.Visible = !ContextMenuCommandHandler.IsSelectedNodeBCEntity();
          command.Enabled = ContextMenuCommandHandler.isRunFloorplanAllowed();
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForUINode(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      switch (id)
      {
        case 293:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.isFloorPlanEditable();
          break;
        case 294:
          command.Visible = !ContextMenuCommandHandler.IsSelectedNodeBCEntity();
          command.Enabled = ContextMenuCommandHandler.isRunFloorplanAllowed();
          break;
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static bool isFloorPlanEditable()
    {
      return (!(XRepMapper.GetInstance().GetOpenSolutionStatus() != "In Development") || !(XRepMapper.GetInstance().GetOpenSolutionStatus() != "In Maintenance")) && SAP.Copernicus.Core.Protocol.Connection.getInstance().SystemMode != SystemMode.Productive;
    }

    private static bool isRunFloorplanAllowed()
    {
      return SAP.Copernicus.Core.Protocol.Connection.getInstance().SystemMode != SystemMode.Productive;
    }

    private static void CheckCommandStatusForUIWOCVIEW(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      switch (id)
      {
        case 293:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.isFloorPlanEditable();
          break;
        case 294:
          command.Visible = !ContextMenuCommandHandler.IsSelectedNodeBCEntity();
          command.Enabled = ContextMenuCommandHandler.isRunFloorplanAllowed();
          break;
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static bool IsSelectedNodeBCEntity()
    {
      return CopernicusProjectSystemUtil.GetXRepPathforSelectedNode().StartsWith(XRepMapper.GetInstance().GetBCFolderXRepPath());
    }

    private static void CheckCommandStatusForUIWOC(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      switch (id)
      {
        case 293:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.isFloorPlanEditable();
          break;
        case 13668:
        case 13669:
        case 13670:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForCSDNode(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 368:
          if (IntegrationScenario.XMLSerializable<IntegrationScenarioDefinition>.DeserializeFromXML(CopernicusProjectSystemUtil.GetFilepathforSelectedNode()).CommunicationType == CommunicationPartnerTypeCode.ApplicationIntegration)
          {
            command.Visible = true;
            command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
            break;
          }
          command.Visible = false;
          command.Enabled = false;
          break;
        case 369:
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForPINode(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 312:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForFormsXDP(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 4403:
        case 4409:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 4408:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForFormsXFT(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForFormsFTGD(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForFormsFTHD(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForQueryNode(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      string boProxyName = "";
      RepositoryDataCache instance = RepositoryDataCache.GetInstance();
      switch (id)
      {
        case 4471:
          CopernicusProjectFileNode parent = (CopernicusProjectSystemUtil.getSelectedNode() as CopernicusDependentFileNode).Parent as CopernicusProjectFileNode;
          bool flag = ContextMenuCommandHandler.checkIfSelectedBOISActivated(instance, ref boProxyName, ref parent);
          command.Visible = true;
          command.Enabled = flag;
          break;
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForDataSourceNode(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 24577:
        case 24578:
          command.Visible = true;
          command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
          if (!XRepMapper.GetInstance().Iskeyusersoln)
            break;
          command.Enabled = false;
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 297:
        case 304:
          if (XRepMapper.GetInstance().isItemAvailable("ANALYTICAL_REPORT"))
          {
            command.Visible = true;
            command.Enabled = ContextMenuCommandHandler.isSelectedObjectsRuntimeObjectUptoDate() && XRepMapper.GetInstance().IsSolutionEditable();
            break;
          }
          command.Visible = false;
          command.Enabled = false;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForAnalyticsReportNode(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 5481:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 5488:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForAnalyticsKeyFigureNode(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 5481:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForAnalyticsCombinedDSNode(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 5481:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForAnalyticsJoinDSNode(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 5481:
          command.Visible = true;
          command.Enabled = true;
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static void CheckCommandStatusForBO_TTReference(OleMenuCommand command)
    {
      switch (command.CommandID.ID)
      {
        case 8504:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckoutEnabled();
          break;
        case 8505:
          command.Visible = true;
          command.Enabled = ContextMenuCommandHandler.IsCheckinEnabled();
          break;
        case 24579:
          command.Visible = true;
          command.Enabled = true;
          break;
        default:
          command.Visible = false;
          command.Enabled = false;
          break;
      }
    }

    private static bool isSelectedObjectsRuntimeObjectUptoDate()
    {
      PDI_S_APPS_CONTENT_STATUS[] status = XRepHandler.Instance.ContentGetStatus(CopernicusProjectSystemUtil.GetXRepPathforSelectedNode(), (string[]) null, (string) null);
      if (status != null)
      {
        foreach (PDI_S_APPS_CONTENT_STATUS appsContentStatus in status)
        {
          if (appsContentStatus.STATUS_RUN_TIME == "2")
            return true;
        }
      }
      return false;
    }

    private static void CheckCommandStatusForProjectNode(OleMenuCommand command)
    {
      int id = command.CommandID.ID;
      if (!(CopernicusProjectSystemUtil.getSelectedNode() is CopernicusProjectNode))
      {
        command.Visible = false;
      }
      else
      {
        switch (id)
        {
          case 13671:
          case 13672:
          case 13673:
            command.Visible = true;
            command.Enabled = ContextMenuCommandHandler.CheckTransaltionCommandStatusByEditMode(id);
            break;
          case 13680:
            command.Visible = true;
            command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
            if (!command.Enabled || !XRepMapper.GetInstance().Iskeyusersoln)
              break;
            command.Enabled = false;
            break;
          case 13681:
          case 13682:
          case 13696:
          case 13697:
          case 12612:
            command.Visible = true;
            command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
            break;
          case 13698:
            if (!ContextMenuCommandHandler.background_mode_on)
            {
              command.Visible = true;
              command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
              break;
            }
            command.Visible = false;
            break;
          case 13699:
            if (ContextMenuCommandHandler.background_mode_on)
            {
              command.Visible = true;
              command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
              break;
            }
            command.Visible = false;
            break;
          case 13700:
            if (ContextMenuCommandHandler.background_mode_on)
            {
              command.Visible = true;
              command.Enabled = XRepMapper.GetInstance().IsSolutionEditable();
              break;
            }
            command.Visible = false;
            break;
          case 13701:
            if (SAP.Copernicus.Core.Protocol.Connection.getInstance().GetSAPSolution() == SapSolutionCode.BYD_ODS)
            {
              command.Visible = true;
              command.Enabled = true;
              break;
            }
            command.Visible = false;
            command.Enabled = false;
            break;
          case 12608:
            command.Visible = true;
            command.Enabled = true;
            if (XRepMapper.GetInstance().IsSolutionEditable())
            {
              XRepHandler xrepHandler = new XRepHandler();
              CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
              if (selectedProject == null)
                command.Enabled = false;
              if (string.IsNullOrEmpty(XRepMapper.GetInstance().GetXrepPathforLocalFile(selectedProject.ProjectFolder)))
                command.Enabled = false;
              if (XRepMapper.GetInstance().GetListOfLocksForLoggedInUserInCurrentProject().Count != 0)
                break;
              command.Enabled = false;
              break;
            }
            command.Enabled = false;
            break;
          default:
            command.Visible = false;
            command.Enabled = false;
            break;
        }
      }
    }

    public static void ActivateAll(object sender, EventArgs e)
    {
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      if (selectedProject == null || string.IsNullOrEmpty(XRepMapper.GetInstance().GetXrepPathforLocalFile(selectedProject.ProjectFolder)))
        return;
      XRepHandler xrepHandler = new XRepHandler();
      try
      {
        HashSet<string> stringSet = new HashSet<string>();
        IList<string> inCurrentProject = (IList<string>) XRepMapper.GetInstance().GetListOfLocksForLoggedInUserInCurrentProject();
        IList<string> openWorkList = (IList<string>) xrepHandler.getOpenWorkList((string) null);
        foreach (string str in (IEnumerable<string>) inCurrentProject)
          stringSet.Add(str);
        foreach (string str in (IEnumerable<string>) openWorkList)
          stringSet.Add(str);
        foreach (string str1 in stringSet)
        {
          HierarchyNode nodeObjectForPath = CopernicusProjectSystemUtil.getSelectedProject().GetNodeObjectForPath(XRepMapper.GetInstance().GetLocalPathforXRepProjectEntities(str1));
          if (nodeObjectForPath != null)
          {
            string mkDocument = nodeObjectForPath.GetMkDocument();
            CopernicusProjectSystemUtil.GetXRepPathForNode(nodeObjectForPath);
            IntPtr docData = new IntPtr();
            uint id = nodeObjectForPath.ID;
            IVsPersistDocData ppDocData = (IVsPersistDocData) null;
            CopernicusProjectSystemUtil.getSelectedProject().GetDocDataObject(mkDocument, id, out ppDocData);
            bool flag = false;
            if (ppDocData != null)
            {
              docData = Marshal.GetIUnknownForObject((object) ppDocData);
              int isDirty = 0;
              nodeObjectForPath.IsItemDirty(id, docData, out isDirty);
              flag = isDirty != 0;
            }
            if (flag)
            {
              if (MessageBox.Show(string.Format(CopernicusResources.SaveFile, (object) nodeObjectForPath.Caption), CopernicusResources.DialogTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
              {
                int cancelled;
                selectedProject.SaveItem(VSSAVEFLAGS.VSSAVE_Save, mkDocument, id, docData, out cancelled);
              }
              else if (nodeObjectForPath.GetType() == typeof (CopernicusProjectFileNode))
              {
                CopernicusProjectFileNode copernicusProjectFileNode = (CopernicusProjectFileNode) nodeObjectForPath;
                copernicusProjectFileNode.CloseStandardEditor();
                copernicusProjectFileNode.OpenWithStandardEditor();
              }
              else if (nodeObjectForPath.GetType() == typeof (CopernicusDependentFileNode))
              {
                CopernicusDependentFileNode dependentFileNode = (CopernicusDependentFileNode) nodeObjectForPath;
                dependentFileNode.CloseStandardEditor();
                dependentFileNode.OpenWithStandardEditor();
              }
            }
            CopernicusProjectSystemUtil.getSelectedProject().CheckInNode(nodeObjectForPath);
          }
          else
          {
            string str2 = XRepMapper.GetInstance().GetBCFolderXRepPath().Replace("/SRC", "/GEN");
            if (!str1.StartsWith(str2))
              xrepHandler.Activate(str1, false, (string) null);
          }
        }
      }
      catch (ProtocolException ex)
      {
        int num = (int) CopernicusMessageBox.Show(CopernicusResources.CheckInAllFailed, CopernicusResources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      finally
      {
        CopernicusProjectSystemUtil.getSelectedProject().SccGlyphChanged(0, (uint[]) null, (VsStateIcon[]) null, (uint[]) null);
      }
    }

    public static void CopySolution(object sender, EventArgs e)
    {
      int num = (int) new CopySolutionWizard().ShowDialog();
    }

    public static void CreateWsAuthFile(object sender, EventArgs e)
    {
      int num = (int) new CreateExtUIWizard().ShowDialog();
    }

    public static void CreateCSDFile(object sender, EventArgs e)
    {
      int num = (int) new IntegrationScenarioWizard(CopernicusProjectSystemUtil.getSelectedNode().Caption).ShowDialog();
    }

    public static void CreateTTReference(object sender, EventArgs e)
    {
      int num = (int) new TTReferenceCreationForm(CopernicusProjectSystemUtil.getSelectedNode().Url).ShowDialog();
    }

    public static void ShowWsidTestTool(object sender, EventArgs e)
    {
      int num = (int) new WsidTestForm(CopernicusProjectSystemUtil.getSelectedNode().Url).ShowDialog();
    }

    public static void MergeSolution(object sender, EventArgs e)
    {
      int num = (int) new MergeSolutionWizard().ShowDialog();
    }

    public static void DeleteProject(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      string currentSolutionName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
      string[] strArray = CopernicusProjectSystemUtil.GetCurrentSolutionFilePath().Split('\\');
      string str = strArray[strArray.Length - 2];
      string solutionVersion = str.Substring(str.IndexOf(" ") + 1);
      XRepMapper instance = XRepMapper.GetInstance();
      CustomDialog customDialog = new CustomDialog(CopernicusResources.WaitCaption, CopernicusResources.WaitDeleteSolution);
      bool flag;
      try
      {
        RepositoryDataCache.GetInstance().DeleteSolution(currentSolutionName, solutionVersion);
        flag = instance.DeleteSolutionFromXRep(currentSolutionName, solutionVersion);
      }
      finally
      {
        if (customDialog != null)
          customDialog.destroy();
      }
      if (!flag || currentSolutionName == null)
        return;
      CopernicusProjectSystemUtil.CloseCurrentSolution(true);
    }

    public static void ActivateSolution(object sender, EventArgs e)
    {
      string currentSolutionName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
      XRepMapper instance = XRepMapper.GetInstance();
      ContextMenuCommandHandler.checkForUnsavedFiles(currentSolutionName);
      instance.ActivateSolution(currentSolutionName, false, char.MinValue, false);
      CopernicusFileNodeProperties.clearBuffer();
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      selectedProject.refreshProperties();
      selectedProject.RedrawIconsOfHierarchy();
    }

    private static void checkForUnsavedFiles(string name)
    {
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      if (selectedProject == null)
        return;
      List<string> stringList = new List<string>();
      if (string.IsNullOrEmpty(XRepMapper.GetInstance().GetXrepPathforLocalFile(selectedProject.ProjectFolder)))
        return;
      XRepHandler xrepHandler = new XRepHandler();
      try
      {
        HashSet<string> stringSet = new HashSet<string>();
        IList<string> inCurrentProject = (IList<string>) XRepMapper.GetInstance().GetListOfLocksForLoggedInUserInCurrentProject();
        IList<string> openWorkList = (IList<string>) xrepHandler.getOpenWorkList((string) null);
        foreach (string str in (IEnumerable<string>) inCurrentProject)
          stringSet.Add(str);
        foreach (string str in (IEnumerable<string>) openWorkList)
          stringSet.Add(str);
        foreach (string xrepURI in stringSet)
        {
          HierarchyNode nodeObjectForPath = CopernicusProjectSystemUtil.getSelectedProject().GetNodeObjectForPath(XRepMapper.GetInstance().GetLocalPathforXRepProjectEntities(xrepURI));
          if (nodeObjectForPath != null)
          {
            string mkDocument = nodeObjectForPath.GetMkDocument();
            CopernicusProjectSystemUtil.GetXRepPathForNode(nodeObjectForPath);
            IntPtr num = new IntPtr();
            uint id = nodeObjectForPath.ID;
            IVsPersistDocData ppDocData = (IVsPersistDocData) null;
            CopernicusProjectSystemUtil.getSelectedProject().GetDocDataObject(mkDocument, id, out ppDocData);
            bool flag = false;
            if (ppDocData != null)
            {
              IntPtr iunknownForObject = Marshal.GetIUnknownForObject((object) ppDocData);
              int isDirty = 0;
              nodeObjectForPath.IsItemDirty(id, iunknownForObject, out isDirty);
              flag = isDirty != 0;
            }
            if (flag && !stringList.Contains(nodeObjectForPath.Caption))
              stringList.Add(nodeObjectForPath.Caption);
          }
        }
        if (stringList.Count <= 0)
          return;
        string str1 = "";
        foreach (string str2 in stringList)
          str1 = str1 + str2 + "\n";
        int num1 = (int) MessageBox.Show("The unsaved changes will not be considered during activation.The following files have unsaved changes:\n" + str1, CopernicusResources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
      }
      catch (ProtocolException ex)
      {
        int num = (int) CopernicusMessageBox.Show(CopernicusResources.CheckInAllFailed, CopernicusResources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    public static void ActivateSolutionDelta(object sender, EventArgs e)
    {
      XRepMapper.GetInstance().ActivateSolution(CopernicusProjectSystemUtil.GetCurrentSolutionName(), true, char.MinValue, false);
      CopernicusFileNodeProperties.clearBuffer();
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      selectedProject.refreshProperties();
      selectedProject.RedrawIconsOfHierarchy();
    }

    public static void CleanUpSolution(object sender, EventArgs e)
    {
      if (XRepMapper.GetInstance().CleanUpSolution(CopernicusProjectSystemUtil.GetCurrentSolutionName()) && InteractionWithUIDesigner.ToCloseUIDesigner())
        Trace.WriteLine(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "Close UI Designer when Solution will be clean up", new object[0]));
      XREPattribs.resetBuffer((string) null);
      CopernicusFileNodeProperties.clearBuffer();
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      selectedProject.refreshProperties();
      selectedProject.RedrawIconsOfHierarchy();
    }

    public static void CheckSolutionItems(object sender, EventArgs e)
    {
      ContextMenuCommandHandler.CheckSolution(false, ContextMenuCommandHandler.background_mode_on);
    }

    public static void CheckSolutionItemsAndRuntime(object sender, EventArgs e)
    {
      ContextMenuCommandHandler.CheckSolution(true, ContextMenuCommandHandler.background_mode_on);
    }

    public static void CheckHTML5(object sender, EventArgs e)
    {
      IDictionary<string, IDictionary<string, string>> attributesofAllFiles = (IDictionary<string, IDictionary<string, string>>) null;
      string projectFolderXrepPath = XRepMapper.GetInstance().GetProjectFolderXRepPath();
      XRepHandler xrepHandler = new XRepHandler();
      List<string> pathList;
      List<string> subDirectories;
      xrepHandler.ListContentwithAttributes(projectFolderXrepPath, false, out attributesofAllFiles, out pathList, out subDirectories);
      List<string> list = pathList.Where<string>((Func<string, bool>) (b => b.Contains(".uicomponent"))).ToList<string>();
      xrepHandler.CheckHTML5(list);
    }

    public static void EnableBackgroundMode(object sender, EventArgs e)
    {
      if (ContextMenuCommandHandler.solutionHandler.backgroundModeCheck().Equals("X"))
      {
        ContextMenuCommandHandler.background_mode_on = true;
        CopernicusStatusBar.Instance.ShowMessage(Resource.BackgroundCheckEnabled);
      }
      else
        ContextMenuCommandHandler.background_mode_on = false;
    }

    public static void DisableBackgroundMode(object sender, EventArgs e)
    {
      if (ContextMenuCommandHandler.solutionHandler.backgroundModeCheck().Equals(""))
      {
        ContextMenuCommandHandler.background_mode_on = false;
        CopernicusStatusBar.Instance.ShowMessage(Resource.BackgroundCheckDisabled);
      }
      else
        ContextMenuCommandHandler.background_mode_on = true;
    }

    public static void Displaylogs(object sender, EventArgs e)
    {
      string currentSolutionName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
      ContextMenuCommandHandler.solutionHandler.DisplayLogs(currentSolutionName);
      ContextMenuCommandHandler.background_mode_on = true;
    }

    private static void CheckSolution(bool checkRuntime, bool backgrndmode_on = false)
    {
      IDictionary<string, List<string>> usageErrorList;
      XRepMapper.GetInstance().CheckSolution(CopernicusProjectSystemUtil.GetCurrentSolutionName(), out usageErrorList, checkRuntime, backgrndmode_on);
      Solution2 solution = DTEUtil.GetDTE().Solution as Solution2;
      if (!solution.IsOpen)
        return;
      foreach (EnvDTE.Project project in solution.Projects)
      {
        CopernicusProjectNode copernicusProjectNode = project.Object as CopernicusProjectNode;
        if (copernicusProjectNode != null)
          copernicusProjectNode.AssignUsageErrors(usageErrorList);
      }
    }

    public static void CallVS(object sender, EventArgs e)
    {
      try
      {
        new System.Diagnostics.Process()
        {
          StartInfo = {
            FileName = PropertyAccess.GeneralProps.VSPath
          },
          EnableRaisingEvents = true
        }.Start();
      }
      catch
      {
        int num = (int) CopernicusMessageBox.Show(CopernicusResources.Call_VS_Failed, CopernicusResources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    public static void CreateUI(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      bool flag = false;
      HierarchyNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode();
      string caption = selectedNode.Caption;
      string url = selectedNode.Url;
      if (caption.EndsWith(".bo"))
        flag = LocalFileHandler.ReadFileFromDisk(url).Contains("[OfflineEnabled]");
      if (flag)
      {
        if (new FileInfo(url.Replace(".bo", ".EC.uicomponent")).Exists || new FileInfo(url.Replace(".bo", ".OVS.uicomponent")).Exists || (new FileInfo(url.Replace(".bo", ".OWL.uicomponent")).Exists || new FileInfo(url.Replace(".bo", ".QC.uicomponent")).Exists) || (new FileInfo(url.Replace(".bo", ".QV.uicomponent")).Exists || new FileInfo(url.Replace(".bo", ".TI.uicomponent")).Exists || (new FileInfo(url.Replace(".bo", ".TT.uidataobject")).Exists || new FileInfo(url.Replace(".bo", ".WCF.uiwoc")).Exists)) || new FileInfo(url.Replace(".bo", ".WCVIEW.uiwocview")).Exists)
        {
          if (MessageBox.Show("File already exists. Do you want to overwrite it?", "File Exists", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) != DialogResult.Yes)
            return;
          ContextMenuCommandHandler.CreateUIComponentsForOfflineEnabledBO(selectedNode);
        }
        else
          ContextMenuCommandHandler.CreateUIComponentsForOfflineEnabledBO(selectedNode);
      }
      else
      {
        int num = (int) new SAP.Copernicus.Core.CreateScreenWizard.CreateScreenWizard().ShowDialog();
      }
    }

    public static void CreateUIComponentsForOfflineEnabledBO(HierarchyNode boNode)
    {
      string caption = boNode.Caption;
      char ch = 'X';
      string str1 = caption.Split('.')[0];
      string forSelectedProject = CopernicusProjectSystemUtil.GetNamespaceForSelectedProject();
      string str2 = CopernicusProjectSystemUtil.GetXRepPathforSelectedNode().Replace("/" + caption, "");
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      FM_AP_UI_DT_COMP_GENERATOR uiDtCompGenerator = new FM_AP_UI_DT_COMP_GENERATOR();
      uiDtCompGenerator.Importing = new FM_AP_UI_DT_COMP_GENERATOR.ImportingType();
      uiDtCompGenerator.Importing.IV_BO_NAME = str1;
      uiDtCompGenerator.Importing.IV_BO_NAMESPACE = forSelectedProject;
      uiDtCompGenerator.Importing.IV_ASSIGN_WOC_VIEW_TO_USER = ch;
      uiDtCompGenerator.Importing.IV_XREP_FOLDER_PATH = str2;
      uiDtCompGenerator.Importing.IV_TYPE = "generate_for_bo";
      CustomDialog customDialog = new CustomDialog("Please Wait", "Generating screens with Thing-based Navigation ");
      jsonClient.callFunctionModule((SAPFunctionModule) uiDtCompGenerator, false, false, false);
      if (uiDtCompGenerator.Exporting == null)
      {
        customDialog.destroy();
        int num = (int) CopernicusMessageBox.Show("Issue with FM AP_UI_DT_COMP_GENERATOR", CopernicusResources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        XRepMapper.GetInstance().RefreshSolution(false);
        customDialog.destroy();
      }
    }

    public static void RefreshSolution(object sender, EventArgs e)
    {
      XRepMapper instance = XRepMapper.GetInstance();
      CustomDialog customDialog = new CustomDialog(CopernicusResources.Refresh, CopernicusResources.RefreshSolutionTitle, CopernicusResources.WaitRefresh);
      instance.RefreshSolution(true);
      customDialog.destroy();
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      CopernicusFileNodeProperties.clearBuffer();
      if (selectedProject == null)
        return;
      selectedProject.RedrawIconsOfHierarchy();
    }

    public static void SearchSolution(object sender, EventArgs e)
    {
      SearchForm searchForm = new SearchForm();
      searchForm.TopMost = true;
      searchForm.Show();
    }

    public static void CreatePatchOpen(object sender, EventArgs e)
    {
      if (new Disclaimer().ShowDialog() != DialogResult.OK)
        return;
      if (new SolutionHandler().PDI_1O_PROD_FIX_CORRECTION(CopernicusProjectSystemUtil.GetCurrentSolutionName(), "X"))
      {
        XRepMapper.GetInstance().RefreshSolution(false);
        CopernicusStatusBar.Instance.ShowMessage(Resource.OpenCorrection);
      }
      else
        CopernicusStatusBar.Instance.ShowMessage(Resource.OpenCorrectionFailed);
    }

    public static void CreatePatchClose(object sender, EventArgs e)
    {
      if (new SolutionHandler().PDI_1O_PROD_FIX_CORRECTION(CopernicusProjectSystemUtil.GetCurrentSolutionName(), ""))
      {
        XRepMapper.GetInstance().RefreshSolution(false);
        CopernicusStatusBar.Instance.ShowMessage(Resource.CloseCorrection);
      }
      else
        CopernicusStatusBar.Instance.ShowMessage(Resource.CloseCorrectionFailed);
    }

    public static void AddNewItem(object sender, EventArgs e)
    {
      HierarchyNode hierarchyNode = (HierarchyNode) null;
      try
      {
        hierarchyNode = CopernicusProjectSystemUtil.getSelectedNode();
      }
      catch (ArgumentException ex)
      {
      }
      if (hierarchyNode != null && hierarchyNode is CopernicusFolderNode)
      {
        ((CopernicusFolderNode) hierarchyNode).StartAddNewItemDialog();
      }
      else
      {
        Solution2 solution = (Solution2) DTEUtil.GetDTE().Solution;
        if (solution == null || !solution.IsOpen)
          return;
        foreach (EnvDTE.Project project in solution.Projects)
          (project.Object as CopernicusProjectNode).StartAddNewItemDialog();
      }
    }

    public static void EnhScreen(object sender, EventArgs e)
    {
      int num = (int) new UIModelSelectionWizard().ShowDialog();
    }

    public static void EmbCompEdit(object sender, EventArgs e)
    {
      int num = (int) CopernicusMessageBox.Show("Not yet implemented");
    }

    public static void CreateMDROScr(object sender, EventArgs e)
    {
      MDROUtils.GenerateUI();
    }

    public static void EmbCompDelete(object sender, EventArgs e)
    {
      int num = (int) CopernicusMessageBox.Show("Not yet implemented");
    }

    public static void EmbCompUploadDLL(object sender, EventArgs e)
    {
      int num = (int) new EmbCompUploadWizard().ShowDialog();
    }

    public static void CreateScriptFiles(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      ScriptGenerator.Instance.CreateScriptFilesForSelectedBO();
    }

    public static void CreateXBOScriptFiles(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      ScriptFileGenerator.Instance.CreateScriptFilesForSelectedXBO();
    }

    public static void ConfigurePI(object sender, EventArgs e)
    {
      new ProcessIntegrationHandler().ConfigurePI(CopernicusProjectSystemUtil.GetXRepPathforSelectedNode(), true);
    }

    public static void GenerateBOService(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) new WebServiceWizard().ShowDialog();
    }

    public static void CheckWebService(object sender, EventArgs e)
    {
      IDictionary<string, List<string>> usageErrorList;
      bool flag = new XRepHandler().CheckContent("A2X", CopernicusProjectSystemUtil.GetXRepPathforSelectedNode(), out usageErrorList);
      Solution2 solution = DTEUtil.GetDTE().Solution as Solution2;
      if (!solution.IsOpen)
        return;
      foreach (EnvDTE.Project project in solution.Projects)
      {
        CopernicusProjectNode copernicusProjectNode = project.Object as CopernicusProjectNode;
        if (copernicusProjectNode != null)
          copernicusProjectNode.AssignUsageErrors(usageErrorList);
      }
      if (!flag)
        return;
      int num = (int) CopernicusMessageBox.Show(CopernicusResources.WebServiceCheckedSuccesfully, CopernicusResources.SuccessMessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    public static void CleanWebService(object sender, EventArgs e)
    {
      if (!new XRepHandler().CleanContent("A2X", CopernicusProjectSystemUtil.GetXRepPathforSelectedNode()))
        return;
      int num = (int) CopernicusMessageBox.Show(CopernicusResources.WebServiceCleanedSuccesfully, CopernicusResources.SuccessMessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    public static void ActivateWebService(object sender, EventArgs e)
    {
      Cursor current = Cursor.Current;
      XRepHandler xrepHandler = new XRepHandler();
      BOXSHandler boxsHandler = new BOXSHandler();
      XRepMapper instance = XRepMapper.GetInstance();
      string pathforSelectedNode = CopernicusProjectSystemUtil.GetXRepPathforSelectedNode();
      string xrepProjectEntities = instance.GetLocalPathforXRepProjectEntities(pathforSelectedNode);
      bool flag;
      try
      {
        Cursor.Current = Cursors.WaitCursor;
        flag = xrepHandler.ActivateContent("A2X", pathforSelectedNode, (string[]) null);
      }
      catch (ProtocolException ex)
      {
        string content;
        IDictionary<string, string> attribs;
        xrepHandler.Read(pathforSelectedNode, out content, out attribs);
        if (content != null)
          LocalFileHandler.CreateFileOnDisk(content, xrepProjectEntities, true);
        flag = false;
      }
      finally
      {
        Cursor.Current = current;
      }
      if (!flag)
        return;
      int num = (int) CopernicusMessageBox.Show(CopernicusResources.WebServiceActivatedSuccesfully, CopernicusResources.SuccessMessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    public static void DownloadWSDL(object sender, EventArgs e)
    {
      WebServiceUtil.DownloadWSDL();
    }

    public static void PreviewUiComponentInBrowser(string pstrUiComponentXrepPath)
    {
      ByDUIUtil.LaunchUIComponent(pstrUiComponentXrepPath);
    }

    public static void CreateCommunicationSystem(object sender, EventArgs e)
    {
      ContextMenuCommandHandler.PreviewUiComponentInBrowser("/SAP_BYD_APPLICATION_UI/base/om/CommunicationSystemOWL.OWL.uicomponent");
    }

    public static void CreateCommunicationArrangement(object sender, EventArgs e)
    {
      ContextMenuCommandHandler.PreviewUiComponentInBrowser("/SAP_BYD_APPLICATION_UI/base/b2b/MessageBasedCommunicationArrangement_OWL.OWL.uicomponent");
    }

    public static void OpenFloorPlan(object sender, EventArgs e)
    {
      if (CopernicusProjectSystemUtil.getSelectedNodeName().EndsWith(".xuicomponent"))
        ContextMenuCommandHandler.OpenFloorPlanForXUIComponent(sender, e);
      else
        UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(CopernicusProjectSystemUtil.getSelectedProject().GetXrepPathforSelectedNode(), false);
    }

    public static void AnalyticalObjectHandler(object sender, EventArgs e)
    {
      string selectedNodeName = CopernicusProjectSystemUtil.getSelectedNodeName();
      switch (selectedNodeName.Substring(selectedNodeName.IndexOf('.')))
      {
        case ".report":
        case ".kf":
        case ".cds":
        case ".jds":
          KeyUserUtil.handleDoubleClick(CopernicusProjectSystemUtil.getSelectedNode());
          break;
      }
    }

    public static void PreviewReport(object sender, EventArgs e)
    {
      string selectedNodeName = CopernicusProjectSystemUtil.getSelectedNodeName();
      switch (selectedNodeName.Substring(selectedNodeName.IndexOf('.')))
      {
        case ".report":
          HierarchyNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode();
          KeyUserUtil.PreviewReport(selectedNode.Url.Substring(selectedNode.Url.LastIndexOf("\\") + 1));
          break;
      }
    }

    public static void OpenUIFloorPlan_UIDesigner(HierarchyNode node)
    {
      if (CopernicusProjectSystemUtil.getSelectedNodeName().EndsWith(".xuicomponent"))
      {
        IDictionary<string, string> attribs = (IDictionary<string, string>) null;
        XRepHandler xrepHandler = new XRepHandler();
        string content = "";
        string pathforSelectedNode = CopernicusProjectSystemUtil.getSelectedProject().GetXrepPathforSelectedNode();
        xrepHandler.Read(pathforSelectedNode, out content, out attribs);
        string strXRepPathforUIComponent;
        if (!attribs.TryGetValue("XUIComponent", out strXRepPathforUIComponent))
          return;
        UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(strXRepPathforUIComponent, false);
      }
      else
      {
        string empty = string.Empty;
        UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(!node.Url.EndsWith(".PB.uimashup") ? CopernicusProjectSystemUtil.getSelectedProject().GetXrepPathforSelectedNode() : CopernicusProjectSystemUtil.getSelectedProject().GetXrepPathforMashupNodes(node.Url), false);
      }
    }

    private static void OpenFloorPlanForXUIComponent(object sender, EventArgs e)
    {
      IDictionary<string, string> attribs = (IDictionary<string, string>) null;
      XRepHandler xrepHandler = new XRepHandler();
      string content = "";
      string pathforSelectedNode = CopernicusProjectSystemUtil.getSelectedProject().GetXrepPathforSelectedNode();
      xrepHandler.Read(pathforSelectedNode, out content, out attribs);
      string strXRepPathforUIComponent;
      if (!attribs.TryGetValue("XUIComponent", out strXRepPathforUIComponent))
        return;
      UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(strXRepPathforUIComponent, false);
    }

    public static void XOdataDoubleClick(HierarchyNode node)
    {
      ODataUtil.OpenXOData();
    }

    public static void RunFloorPlanForXUIComponent(object sender, EventArgs e)
    {
      IDictionary<string, string> attribs = (IDictionary<string, string>) null;
      XRepHandler xrepHandler = new XRepHandler();
      ConnectionDataSet.SystemDataRow connectedSystem = SAP.Copernicus.Core.Protocol.Connection.getInstance().getConnectedSystem();
      string name = connectedSystem.Name;
      string host = connectedSystem.Host;
      int httpPort = connectedSystem.HttpPort;
      int client = connectedSystem.Client;
      string str1;
      if (client < 100)
      {
        str1 = "0" + (object) client;
        if (client < 10)
          str1 = "0" + str1;
      }
      else
        str1 = client.ToString();
      string language = connectedSystem.Language;
      string user = connectedSystem.User;
      string str2 = "/sap/ap/ui/repository/SAP_BYD_UI/Runtime/StartPage.html?";
      string str3 = "sap-client=" + str1;
      string str4 = "&sap-system-login-basic_auth=X";
      string str5 = "&sap-language=" + language;
      string str6 = "&app.component=";
      string pathforSelectedNode = CopernicusProjectSystemUtil.getSelectedProject().GetXrepPathforSelectedNode();
      string content = "";
      xrepHandler.Read(pathforSelectedNode, out content, out attribs);
      string str7;
      if (!attribs.TryGetValue("XUIComponent", out str7))
        return;
      ExternalApplication.LaunchExternalBrowser(WebUtil.BuildURL(SAP.Copernicus.Core.Protocol.Connection.getInstance().getConnectedSystem(), str2 + str3 + str4 + str5 + str6 + str7));
    }

    public static void OpenUIFloorPlan(HierarchyNode node)
    {
      ConnectionDataSet.SystemDataRow connectedSystem = SAP.Copernicus.Core.Protocol.Connection.getInstance().getConnectedSystem();
      int client = connectedSystem.Client;
      string str1;
      if (client < 100)
      {
        str1 = "0" + (object) client;
        if (client < 10)
          str1 = "0" + str1;
      }
      else
        str1 = string.Concat((object) client);
      string lower = connectedSystem.Language.ToLower();
      string user = connectedSystem.User;
      string str2 = "/sap(" + Convert.ToBase64String(Encoding.ASCII.GetBytes("c=" + str1 + "&l=" + lower)) + ")";
      string str3 = connectedSystem.Host.Contains("qq6") ? "/ap/XREP/pub/VIRTUAL/SAP_BYD_UI_DT/UIDesigner.application?" : "/public/ap/ui/xrep/VIRTUAL/SAP_BYD_UI_DT/UIDesigner.application?";
      string str4 = "startcomponent=";
      string str5 = "&disableConfigExplorer=true";
      string str6 = CopernicusProjectSystemUtil.getSelectedProject().GetXrepPathforSelectedNode().Replace("/", "%2F");
      ExternalApplication.LaunchExternalBrowser(WebUtil.BuildURL(SAP.Copernicus.Core.Protocol.Connection.getInstance().getConnectedSystem(), str2 + str3 + str4 + str6 + str5 + "&Username=" + user));
    }

    public static void CreateUIScriptFile(object sender, EventArgs e)
    {
    }

    public static void ExportUITransText(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      List<string> pathList = new List<string>();
      pathList.Add(CopernicusProjectSystemUtil.GetXRepPathforSelectedNode());
      if (pathList.Count == 0)
      {
        int num1 = (int) CopernicusMessageBox.Show(TranslationResource.MsgChkExportNoUIComp, "Export Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        string selectedNodeName = CopernicusProjectSystemUtil.getSelectedNodeName();
        string[] strArray = selectedNodeName.Split('.');
        if (!string.IsNullOrEmpty(strArray[0]))
          selectedNodeName = strArray[0];
        int num2 = (int) new ExportText(pathList) { FileName = selectedNodeName }.ShowDialog();
      }
    }

    public static void ImportUITransText(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      List<string> pathlist = new List<string>();
      pathlist.Add(CopernicusProjectSystemUtil.GetXRepPathforSelectedNode());
      if (pathlist.Count == 0)
      {
        int num1 = (int) CopernicusMessageBox.Show(TranslationResource.MsgChkExportNoUIComp, "Import Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        int num2 = (int) new ImportText(pathlist).ShowDialog();
      }
    }

    public static void CheckUITransText(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      TranslationHandler translationHandler = new TranslationHandler();
      List<string> xRepPathlist = new List<string>();
      xRepPathlist.Add(CopernicusProjectSystemUtil.GetXRepPathforSelectedNode());
      if (xRepPathlist.Count == 0)
      {
        int num1 = (int) CopernicusMessageBox.Show(TranslationResource.MsgChkUIComp, "Check Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        int num2 = (int) new CheckTranslation(xRepPathlist, false).ShowDialog();
      }
    }

    public static void ExportSolTransText(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      List<HierarchyNode> allChildrenNodes = CopernicusProjectSystemUtil.getSelectedProject().GetAllChildrenNodes(CopernicusProjectSystemUtil.getSelectedNode());
      List<string> pathList = new List<string>();
      if (allChildrenNodes != null)
      {
        foreach (HierarchyNode node in allChildrenNodes)
        {
          string caption = node.Caption;
          string str1 = caption.Substring(caption.LastIndexOf('.') + 1);
          if (str1.Equals("Business Configuration"))
          {
            if (CopernicusBusinessConfigurationUtil.CheckBusinessConfigurationFile())
            {
              string str2 = CopernicusProjectSystemUtil.GetXRepPathForNode(node);
              if (string.IsNullOrEmpty(str2))
                str2 = XRepMapper.GetInstance().GetXrepPathforLocalFile(node.Url);
              pathList.Add(str2);
            }
          }
          else if (str1.Equals("uicomponent") || str1.Equals("xuicomponent") || (str1.Equals("uiwoc") || str1.Equals("uiwocview")) || (str1.Equals("xbo") || str1.Equals("bcc") || (str1.Equals("bo") || str1.Equals("uidataobject"))) || (str1.Equals("bac") || str1.Equals("codelist")))
          {
            string xrepPathForNode = CopernicusProjectSystemUtil.GetXRepPathForNode(node);
            if (!string.IsNullOrEmpty(xrepPathForNode))
              pathList.Add(xrepPathForNode);
          }
        }
      }
      if (pathList.Count == 0)
      {
        int num1 = (int) CopernicusMessageBox.Show(TranslationResource.MsgChkExportNoUIComp, "Export Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        int num2 = (int) new ExportText(pathList) { FileName = CopernicusProjectSystemUtil.getSelectedProjectName() }.ShowDialog();
      }
    }

    public static void ImportSolTransText(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      List<HierarchyNode> allChildrenNodes = CopernicusProjectSystemUtil.getSelectedProject().GetAllChildrenNodes(CopernicusProjectSystemUtil.getSelectedNode());
      List<string> pathlist = new List<string>();
      if (allChildrenNodes != null)
      {
        foreach (HierarchyNode node in allChildrenNodes)
        {
          string caption = node.Caption;
          string str1 = caption.Substring(caption.LastIndexOf('.') + 1);
          if (str1.Equals("Business Configuration"))
          {
            if (CopernicusBusinessConfigurationUtil.CheckBusinessConfigurationFile())
            {
              string str2 = CopernicusProjectSystemUtil.GetXRepPathForNode(node);
              if (string.IsNullOrEmpty(str2))
                str2 = XRepMapper.GetInstance().GetXrepPathforLocalFile(node.Url);
              pathlist.Add(str2);
            }
          }
          else if (str1.Equals("uicomponent") || str1.Equals("xuicomponent") || (str1.Equals("uiwoc") || str1.Equals("uiwocview")) || (str1.Equals("xbo") || str1.Equals("bcc") || (str1.Equals("bo") || str1.Equals("uidataobject"))) || (str1.Equals("bac") || str1.Equals("codelist")))
          {
            string xrepPathForNode = CopernicusProjectSystemUtil.GetXRepPathForNode(node);
            if (!string.IsNullOrEmpty(xrepPathForNode))
              pathlist.Add(xrepPathForNode);
          }
        }
      }
      if (pathlist.Count == 0)
      {
        int num1 = (int) CopernicusMessageBox.Show(TranslationResource.MsgChkExportNoUIComp, "Import Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        int num2 = (int) new ImportText(pathlist).ShowDialog();
      }
    }

    public static void CheckSolTransText(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      TranslationHandler translationHandler = new TranslationHandler();
      List<HierarchyNode> allChildrenNodes = CopernicusProjectSystemUtil.getSelectedProject().GetAllChildrenNodes(CopernicusProjectSystemUtil.getSelectedNode());
      List<string> xRepPathlist = new List<string>();
      if (allChildrenNodes != null)
      {
        foreach (HierarchyNode node in allChildrenNodes)
        {
          string caption = node.Caption;
          string str1 = caption.Substring(caption.LastIndexOf('.') + 1);
          if (str1.Equals("Business Configuration"))
          {
            if (CopernicusBusinessConfigurationUtil.CheckBusinessConfigurationFile())
            {
              string str2 = CopernicusProjectSystemUtil.GetXRepPathForNode(node);
              if (string.IsNullOrEmpty(str2))
                str2 = XRepMapper.GetInstance().GetXrepPathforLocalFile(node.Url);
              xRepPathlist.Add(str2);
            }
          }
          else if (str1.Equals("uicomponent") || str1.Equals("xuicomponent") || (str1.Equals("uiwoc") || str1.Equals("uiwocview")) || (str1.Equals("xbo") || str1.Equals("bcc") || (str1.Equals("bo") || str1.Equals("uidataobject"))))
          {
            string xrepPathForNode = CopernicusProjectSystemUtil.GetXRepPathForNode(node);
            if (!string.IsNullOrEmpty(xrepPathForNode))
              xRepPathlist.Add(xrepPathForNode);
          }
        }
      }
      if (xRepPathlist.Count == 0)
      {
        int num1 = (int) CopernicusMessageBox.Show(TranslationResource.MsgChkExportNoUIComp, "Check Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        int num2 = (int) new CheckTranslation(xRepPathlist, true).ShowDialog();
      }
    }

    public static void RunFloorPlan(object sender, EventArgs e)
    {
      if (CopernicusProjectSystemUtil.getSelectedNodeName().EndsWith(".xuicomponent"))
      {
        ContextMenuCommandHandler.RunFloorPlanForXUIComponent(sender, e);
      }
      else
      {
        ConnectionDataSet.SystemDataRow connectedSystem = SAP.Copernicus.Core.Protocol.Connection.getInstance().getConnectedSystem();
        string name = connectedSystem.Name;
        string host = connectedSystem.Host;
        int httpPort = connectedSystem.HttpPort;
        int client = connectedSystem.Client;
        string str1;
        if (client < 100)
        {
          str1 = "0" + (object) client;
          if (client < 10)
            str1 = "0" + str1;
        }
        else
          str1 = client.ToString();
        string language = connectedSystem.Language;
        string user = connectedSystem.User;
        string str2 = "/sap/ap/ui/repository/SAP_BYD_UI/Runtime/StartPage.html?";
        string str3 = "sap-client=" + str1;
        string str4 = "&sap-system-login-basic_auth=X";
        string str5 = "&sap-language=" + language;
        string str6 = "&app.component=";
        string str7 = CopernicusProjectSystemUtil.getSelectedProject().GetXrepPathforSelectedNode().Replace("/", "%2F");
        string path;
        if (str7.EndsWith("uiwoc"))
          path = str2 + str3 + str5 + "&app.component=/SAP_BYD_UI_CT/Main/root.uiccwoc";
        else
          path = str2 + str3 + str4 + str5 + str6 + str7;
        ExternalApplication.LaunchExternalBrowser(WebUtil.BuildURL(SAP.Copernicus.Core.Protocol.Connection.getInstance().getConnectedSystem(), path));
      }
    }

    public static void ConvertToCodelist(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      string filepathforSelectedNode = CopernicusProjectSystemUtil.GetFilepathforSelectedNode();
      List<string> reasonsAgainstConversion = new List<string>();
      bool ConversionPossible;
      CopernicusBusinessConfigurationUtil.CheckConversionToCodelistPossible(filepathforSelectedNode, out ConversionPossible, out reasonsAgainstConversion);
      string str1 = filepathforSelectedNode.Substring(filepathforSelectedNode.LastIndexOf('.'));
      string text = (string) null;
      if (ConversionPossible)
      {
        switch (str1)
        {
          case ".bco":
            text = (SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvBCOExplanation + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvPossible + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvBCOPrerequisites + Environment.NewLine + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvConfirmation).Replace("{0}", ".bco");
            break;
          case ".bcc":
            text = (SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvBCCExplanation + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvPossible + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvBCCPrerequisites + Environment.NewLine + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvConfirmation).Replace("{0}", ".bcc");
            break;
        }
        if (CopernicusMessageBox.Show(text, Resource.MsgBYD, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
          return;
        string codelist = CopernicusBusinessConfigurationUtil.convertToCodelist();
        if (string.IsNullOrWhiteSpace(codelist))
          return;
        int num = (int) CopernicusMessageBox.Show(codelist, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
      }
      else
      {
        switch (str1)
        {
          case ".bco":
            text = (SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvBCOExplanation + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvPrerequIntro + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvBCOPrerequisites + Environment.NewLine + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvImpossible + Environment.NewLine).Replace("{0}", ".bco");
            break;
          case ".bcc":
            text = (SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvBCCExplanation + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvPrerequIntro + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvBCCPrerequisites + Environment.NewLine + Environment.NewLine + SAP.Copernicus.BusinessConfiguration.Resources.MsgCdlstConvImpossible + Environment.NewLine).Replace("{0}", ".bcc");
            break;
        }
        foreach (string str2 in reasonsAgainstConversion)
          text = text + str2 + Environment.NewLine;
        int num = (int) CopernicusMessageBox.Show(text, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    public static void CreateBCSetFromPartnerBCO(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) CopernicusMessageBox.Show("CreateBCSetFromPartnerBCO");
    }

    public static void CreateBCViewFromPartnerBCO(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) CopernicusMessageBox.Show("CreateBCViewFromPartnerBCO");
    }

    public static void CreateForm(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) new CreateFormWizardSheet().ShowDialog();
    }

    public static void EnhanceForm(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) new FormExtFieldsSelectionWizard().ShowDialog();
    }

    public static void EnhanceEnterpriseSearch(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) new FurtherUsageSelectionWizard(selTabs.EntSearch).ShowDialog();
    }

    public static void EnhanceReports(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) new FurtherUsageSelectionWizard(selTabs.Reports).ShowDialog();
    }

    public static void MigrateXbo(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      if (!new XBOHandler().MigrateXBO(CopernicusProjectSystemUtil.GetCurrentSolutionName(), Path.GetFileNameWithoutExtension((CopernicusProjectSystemUtil.getSelectedNode() as CopernicusProjectFileNode).FileName)))
        return;
      int num = (int) CopernicusMessageBox.Show(CopernicusResources.XBOMigrationSuccessfully, CopernicusResources.SuccessMessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    public static void RevertProdChange(object sender, EventArgs e)
    {
      ProductionBugFixHandler productionBugFixHandler = new ProductionBugFixHandler();
      HierarchyNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode();
      string currentSolutionName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
      string overallStatus = (string) null;
      string certificationStatus = (string) null;
      string solutionDescription = (string) null;
      bool flag = false;
      if (selectedNode != null)
      {
        string xrepBranch = new SolutionHandler().GetSolutionVersion(currentSolutionName, "100", out overallStatus, out certificationStatus, out solutionDescription).COMP_VERSIONS[0].COMPONENT_VERSION.XREP_BRANCH;
        flag = productionBugFixHandler.productionBugFixRevert(CopernicusProjectSystemUtil.GetXRepPathForNode(selectedNode), xrepBranch);
      }
      if (flag)
      {
        CopernicusStatusBar.Instance.ShowMessage(Resource.ScriptFileRevertSuccessful);
        CopernicusProjectSystemUtil.getSelectedProject().SyncFile(CopernicusProjectSystemUtil.GetFilepathforSelectedNode(), false, false);
      }
      else
        CopernicusStatusBar.Instance.ShowMessage(Resource.ScriptFileReverFailed);
    }

    public static void PerformanceCheck(object sender, EventArgs e)
    {
      ProductionBugFixHandler productionBugFixHandler = new ProductionBugFixHandler();
      HierarchyNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode();
      if (selectedNode == null)
        return;
      productionBugFixHandler.PerformanceCheck(CopernicusProjectSystemUtil.GetXRepPathForNode(selectedNode));
    }

    public static void XODataOpen(object sender, EventArgs e)
    {
      ODataUtil.OpenXOData();
    }

    public static void CreateMSApprovalTask(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      ContextMenuCommandHandler.isGenericTask = false;
      int num = (int) new MSBTMWizard(true).ShowDialog();
    }

    public static void CreateApprovalTask(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      ContextMenuCommandHandler.isGenericTask = false;
      int num = (int) new BTMWizard(true).ShowDialog();
    }

    public static void AddBusinessTask(object sender, EventArgs e)
    {
    }

    public static void DeployTask(object sender, EventArgs e)
    {
      XRepHandler xrepHandler = new XRepHandler();
      Cursor current = Cursor.Current;
      bool flag = xrepHandler.ActivateContent("MSAPPROVALSTEP", CopernicusProjectSystemUtil.GetXRepPathforSelectedNode(), (string[]) null);
      Cursor.Current = current;
      if (!flag)
        return;
      int num = (int) CopernicusMessageBox.Show(CopernicusResources.TaskDepolyedSuccessfully, CopernicusResources.SuccessMessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    public static void CheckTask(object sender, EventArgs e)
    {
      if (!new XRepHandler().CheckContent("MSAPPROVALSTEP", CopernicusProjectSystemUtil.GetXRepPathforSelectedNode()))
        return;
      int num = (int) CopernicusMessageBox.Show(CopernicusResources.TaskCheckedSuccessfully, CopernicusResources.SuccessMessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    public static void CleanTask(object sender, EventArgs e)
    {
      if (!new XRepHandler().CleanContent("MSAPPROVALSTEP", CopernicusProjectSystemUtil.GetXRepPathforSelectedNode()))
        return;
      int num = (int) CopernicusMessageBox.Show(CopernicusResources.TaskCleanedSuccessfully, CopernicusResources.SuccessMessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    public static void CreateProcessIntegration(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) new ProcessIntegrationWizard().ShowDialog();
    }

    public static void CreateQD(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) new QueryDefinitionWizard(QueryDefinitionModel.QueryTypes.ListQuery).ShowDialog();
    }

    public static void EnableNR(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) new NotificationRuleWizard().ShowDialog();
    }

    public static void CreateMDRO(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) new MDROWizard().ShowDialog();
    }

    public static void CreateDataSource(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      int num = (int) new QueryDefinitionWizard(QueryDefinitionModel.QueryTypes.AnalyticalQuery).ShowDialog();
    }

    public static void PreviewQuery(object sender, EventArgs e)
    {
      if (!(sender is OleMenuCommand))
        return;
      IntPtr ppHier;
      uint pitemid;
      IVsMultiItemSelect ppMIS;
      IntPtr ppSC;
      ((IVsMonitorSelection) Package.GetGlobalService(typeof (SVsShellMonitorSelection))).GetCurrentSelection(out ppHier, out pitemid, out ppMIS, out ppSC);
      IVsHierarchy objectForIunknown = Marshal.GetTypedObjectForIUnknown(ppHier, typeof (IVsHierarchy)) as IVsHierarchy;
      if (objectForIunknown == null)
        return;
      object pvar;
      objectForIunknown.GetProperty(pitemid, -2012, out pvar);
    }

    public static void ManageReportsQuery(object sender, EventArgs e)
    {
      string filepathforSelectedNode = CopernicusProjectSystemUtil.GetFilepathforSelectedNode();
      if (!ProjectUtil.GetRuntimeNamespacePrefixForPartnerNamespace(CopernicusProjectSystemUtil.GetNamespaceForSelectedProject()).StartsWith("Z"))
      {
        string EV_SUCCESS;
        string EV_ENABLED;
        CopernicusProjectSystemUtil.SwitchKeyUser(out EV_SUCCESS, out EV_ENABLED);
        if (EV_SUCCESS != "X")
          return;
        if (EV_ENABLED != "X")
          return;
      }
      try
      {
        QueryDefinitionAnnotation definitionAnnotation;
        using (FileStream fileStream = new FileStream(filepathforSelectedNode, FileMode.Open, FileAccess.Read))
          definitionAnnotation = (QueryDefinitionAnnotation) new XmlSerializer(typeof (QueryDefinitionAnnotation)).Deserialize((Stream) fileStream);
        string proxyName = definitionAnnotation.MdavAnnotationElements.ProxyName;
        string pathforLocalFile = XRepMapper.GetInstance().GetXrepPathforLocalFile(filepathforSelectedNode);
        if (new QueryDefinitionHandler().getMDAVStatus(proxyName, pathforLocalFile))
        {
          ByDUIUtil.LaunchUIComponent("/SAP_BYD_TF/Analytics/KeyUserAnalytics/ana_reports_view_kua_prefilled_mdav.WCVIEW.uiwocview", "KuaListWithPrefilledMdavSelection", "Mdavid", proxyName, (string) null, (string) null, (string) null, (string) null);
        }
        else
        {
          int num = (int) MessageBox.Show(SAP.Copernicus.QueryDefinition.Resources.MSG_DS_INACTIVE, SAP.Copernicus.QueryDefinition.Resources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(SAP.Copernicus.QueryDefinition.Resources.MSG_CREATE_REPORT_ERROR, SAP.Copernicus.QueryDefinition.Resources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    public static void CreateReportQuery(object sender, EventArgs e)
    {
      string filepathforSelectedNode = CopernicusProjectSystemUtil.GetFilepathforSelectedNode();
      if (!ProjectUtil.GetRuntimeNamespacePrefixForPartnerNamespace(CopernicusProjectSystemUtil.GetNamespaceForSelectedProject()).StartsWith("Z"))
      {
        string EV_SUCCESS;
        string EV_ENABLED;
        CopernicusProjectSystemUtil.SwitchKeyUser(out EV_SUCCESS, out EV_ENABLED);
        if (EV_SUCCESS != "X")
          return;
        if (EV_ENABLED != "X")
          return;
      }
      try
      {
        string mdavProxyName = (string) null;
        if (ContextMenuCommandHandler.isDataSourceActive(filepathforSelectedNode, out mdavProxyName))
        {
          ByDUIUtil.LaunchUIComponent("/SAP_BYD_TF/Analytics/KeyUserAnalytics/ANA_KUA_ReportFaceWizard_GAF.GAF.uicomponent", "createReportFaceWithPrefilledMdavID", "MdavId", mdavProxyName, (string) null, (string) null, (string) null, (string) null);
        }
        else
        {
          int num = (int) MessageBox.Show(SAP.Copernicus.QueryDefinition.Resources.MSG_DS_INACTIVE, SAP.Copernicus.QueryDefinition.Resources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(SAP.Copernicus.QueryDefinition.Resources.MSG_CREATE_REPORT_ERROR, SAP.Copernicus.QueryDefinition.Resources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private static bool isDataSourceActive(string queryPath, out string mdavProxyName)
    {
      QueryDefinitionAnnotation definitionAnnotation;
      using (FileStream fileStream = new FileStream(queryPath, FileMode.Open, FileAccess.Read))
        definitionAnnotation = (QueryDefinitionAnnotation) new XmlSerializer(typeof (QueryDefinitionAnnotation)).Deserialize((Stream) fileStream);
      mdavProxyName = definitionAnnotation.MdavAnnotationElements.ProxyName;
      string pathforLocalFile = XRepMapper.GetInstance().GetXrepPathforLocalFile(queryPath);
      return new QueryDefinitionHandler().getMDAVStatus(mdavProxyName, pathforLocalFile);
    }

    public static void EnablePCD(object sender, EventArgs e)
    {
      CopernicusProjectSystemUtil.getSelectedProject().CheckOutNode(CopernicusProjectSystemUtil.getSelectedNode());
      XRepHandler xrepHandler = new XRepHandler();
      string content = (string) null;
      IDictionary<string, string> attribs = (IDictionary<string, string>) new Dictionary<string, string>();
      string filepathforSelectedNode = CopernicusProjectSystemUtil.GetFilepathforSelectedNode();
      string pathforSelectedNode = CopernicusProjectSystemUtil.GetXRepPathforSelectedNode();
      xrepHandler.Read(pathforSelectedNode, out content, out attribs);
      SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer instance = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance();
      string proxyNameFromBo = RepositoryDataCache.GetInstance().GetProxyNameFromBO(CopernicusProjectSystemUtil.GetNamespaceForSelectedProject(), Path.GetFileNameWithoutExtension((CopernicusProjectSystemUtil.getSelectedNode() as CopernicusProjectFileNode).FileName));
      if (!attribs.ContainsKey("PCD_" + proxyNameFromBo))
      {
        attribs.Add("PCD_" + proxyNameFromBo, "A");
      }
      else
      {
        string str;
        attribs.TryGetValue("PCD_" + proxyNameFromBo, out str);
        if (str == "Off")
        {
          attribs.Remove("PCD_" + proxyNameFromBo);
          attribs.Add("PCD_" + proxyNameFromBo, "A");
        }
      }
      instance.StoreAttributesInmemory(filepathforSelectedNode, attribs);
      XRepMapper.GetInstance().SaveLocalFileToXRep(filepathforSelectedNode, content, attribs, false, (string) null, false);
      CopernicusStatusBar.Instance.ShowMessage(CopernicusResources.PCDenabled);
    }

    public static void DisablePCD(object sender, EventArgs e)
    {
      CopernicusProjectSystemUtil.getSelectedProject().CheckOutNode(CopernicusProjectSystemUtil.getSelectedNode());
      XRepHandler xrepHandler = new XRepHandler();
      string content = (string) null;
      IDictionary<string, string> attribs = (IDictionary<string, string>) new Dictionary<string, string>();
      string filepathforSelectedNode = CopernicusProjectSystemUtil.GetFilepathforSelectedNode();
      string pathforSelectedNode = CopernicusProjectSystemUtil.GetXRepPathforSelectedNode();
      xrepHandler.Read(pathforSelectedNode, out content, out attribs);
      SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer instance = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance();
      string proxyNameFromBo = RepositoryDataCache.GetInstance().GetProxyNameFromBO(CopernicusProjectSystemUtil.GetNamespaceForSelectedProject(), Path.GetFileNameWithoutExtension((CopernicusProjectSystemUtil.getSelectedNode() as CopernicusProjectFileNode).FileName));
      if (attribs.ContainsKey("PCD_" + proxyNameFromBo))
      {
        attribs.Remove("PCD_" + proxyNameFromBo);
        attribs.Add("PCD_" + proxyNameFromBo, "Off");
      }
      instance.StoreAttributesInmemory(filepathforSelectedNode, attribs);
      XRepMapper.GetInstance().SaveLocalFileToXRep(filepathforSelectedNode, content, attribs, false, (string) null, false);
      CopernicusStatusBar.Instance.ShowMessage(CopernicusResources.PCDdisabled);
    }

    public static void OpenAdobeDesigner(object sender, EventArgs e)
    {
      string fullyQualifiedFormName = "\"" + CopernicusProjectSystemUtil.GetFilepathforSelectedNode() + "\"";
      CopernicusProjectSystemUtil.getSelectedProject().CheckOutNode(CopernicusProjectSystemUtil.getSelectedNode());
      ExternalApplication.LaunchAdobeDesigner(fullyQualifiedFormName);
    }

    public static void ActivateFTG(object sender, EventArgs e)
    {
      XRepHandler xrepHandler = new XRepHandler();
      CustomDialog customDialog = new CustomDialog(CopernicusResources.Activate, CopernicusResources.WaitCaption, CopernicusResources.ActivateFormsGroup);
      try
      {
        xrepHandler.ActivateContent("FORMS", CopernicusProjectSystemUtil.GetXRepPathforSelectedNode(), (string[]) null);
      }
      catch (ProtocolException ex)
      {
      }
      customDialog.destroy();
    }

    public static void ActivateFT(object sender, EventArgs e)
    {
      XRepHandler xrepHandler = new XRepHandler();
      CustomDialog customDialog = new CustomDialog(CopernicusResources.Activate, CopernicusResources.WaitCaption, CopernicusResources.ActivatePrintForms);
      try
      {
        xrepHandler.ActivateContent("FORMS", CopernicusProjectSystemUtil.GetXRepPathforSelectedNode(), (string[]) null);
      }
      catch (ProtocolException ex)
      {
      }
      customDialog.destroy();
    }

    public static void ShowFTVProps(object sender, EventArgs e)
    {
    }

    public static void CreateFTV(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      HierarchyNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode();
      string filepathforSelectedNode = CopernicusProjectSystemUtil.GetFilepathforSelectedNode();
      FileNode parent = selectedNode.Parent as FileNode;
      if (parent == null)
        return;
      string filepathForNode = CopernicusProjectSystemUtil.GetFilepathForNode((HierarchyNode) parent);
      CopernicusProjectSystemUtil.getSelectedProject().CheckOutNode((HierarchyNode) parent);
      if (new FileInfo(filepathForNode).Attributes.ToString().Contains(FileAttributes.ReadOnly.ToString()))
      {
        int num1 = (int) MessageBox.Show("Please Check Out the Form Template before adding new Variants", "Form Template Not Checked Out");
      }
      else
      {
        FormTemplateHeader formTemplateHeader = IntegrationScenario.XMLSerializable<FormTemplateHeader>.DeserializeFromXML(filepathForNode);
        FormVariant variantById = formTemplateHeader.GetVariantByID(filepathforSelectedNode);
        try
        {
          FormVariant toEdit = new FormVariant(variantById.Language, variantById.Country, variantById.Title);
          formTemplateHeader.Variants.Add((object) toEdit);
          if (!new VariantEditorForm(formTemplateHeader, toEdit).ShowDialog().Equals((object) DialogResult.OK))
            return;
          string FormTemplateVariantContent = LocalFileHandler.ReadFileFromDisk(new FileInfo(filepathforSelectedNode));
          CopernicusProjectSystemUtil.getSelectedProject().CreateFormTemplateVariantNode(parent, toEdit.ID, FormTemplateVariantContent);
          IntegrationScenario.XMLSerializable<FormTemplateHeader>.SerializeToXML(formTemplateHeader, filepathForNode);
          string xmlString = IntegrationScenario.XMLSerializable<FormTemplateHeader>.SerializeToXMLString(formTemplateHeader);
          XRepMapper.GetInstance().SaveLocalFileToXRep(filepathForNode, xmlString, false, (string) null, false);
        }
        catch (Exception ex)
        {
          int num2 = (int) CopernicusMessageBox.Show(ex.Message, "Variant Creation not possible.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
    }

    public static void DeleteFTV(object sender, EventArgs e)
    {
      XRepHandler xrepHandler = new XRepHandler();
      bool flag;
      try
      {
        flag = xrepHandler.DeleteContent("FORMS", CopernicusProjectSystemUtil.GetXRepPathforSelectedNode());
      }
      catch (ProtocolException ex)
      {
        flag = false;
      }
      if (!flag)
        return;
      int num = (int) CopernicusMessageBox.Show("Form Template deletion successful.");
    }

    public static void DeployBC(object sender, EventArgs e)
    {
      bool bacDefined = false;
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      if (selectedProject.BCNode != null)
      {
        BCDeploymentForm bcDeploymentForm = new BCDeploymentForm();
        if (bcDeploymentForm.ShowDialog() != DialogResult.OK)
          return;
        bacDefined = bcDeploymentForm.getBacDefined();
      }
      new BusinessConfigurationHandler().deployBusinessConfiguration(selectedProject.Projectlabel, SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID(), bacDefined);
    }

    public static void CheckEFECompliance(object sender, EventArgs e)
    {
      XRepHandler xrepHandler = new XRepHandler();
      bool flag;
      try
      {
        flag = xrepHandler.CheckContent("FORMS", CopernicusProjectSystemUtil.GetXRepPathforSelectedNode());
      }
      catch (ProtocolException ex)
      {
        flag = false;
      }
      if (!flag)
        return;
      int num = (int) CopernicusMessageBox.Show("Form Template check successful.");
    }

    public static void CleanUpFTV(object sender, EventArgs e)
    {
      XRepHandler xrepHandler = new XRepHandler();
      bool flag;
      try
      {
        flag = xrepHandler.CleanContent("FORMS", CopernicusProjectSystemUtil.GetXRepPathforSelectedNode());
      }
      catch (ProtocolException ex)
      {
        flag = false;
      }
      if (!flag)
        return;
      int num = (int) CopernicusMessageBox.Show("Form Template cleanup successful.");
    }

    public static void ManageFTV(object sender, EventArgs e)
    {
      try
      {
        ((CopernicusDependentFileNode) CopernicusProjectSystemUtil.getSelectedNode().Parent).OpenWithStandardEditor();
      }
      catch (InvalidCastException ex)
      {
        ((CopernicusProjectFileNode) CopernicusProjectSystemUtil.getSelectedNode().Parent).OpenWithStandardEditor();
      }
    }

    private static bool XRepFileExists(string xRepPath)
    {
      XRepHandler xrepHandler = new XRepHandler();
      string content = "";
      IDictionary<string, string> attribs = (IDictionary<string, string>) null;
      return xrepHandler.Read(xRepPath, out content, out attribs);
    }

    public static void EditContent(object sender, EventArgs e)
    {
      CopernicusProjectSystemUtil.getSelectedProject().CheckOutNode(CopernicusProjectSystemUtil.getSelectedNode());
    }

    public static void ActivateContent(object sender, EventArgs e)
    {
      bool flag = false;
      int isDirty = 0;
      IVsPersistDocData ppDocData = (IVsPersistDocData) null;
      XRepHandler xrepHandler = new XRepHandler();
      XRepMapper.GetInstance();
      try
      {
        HierarchyNode selectedNode1 = CopernicusProjectSystemUtil.getSelectedNode();
        string mkDocument = selectedNode1.GetMkDocument();
        CopernicusProjectSystemUtil.GetXRepPathForNode(selectedNode1);
        IntPtr docData = new IntPtr();
        uint id = CopernicusProjectSystemUtil.getSelectedNode().ID;
        CopernicusProjectSystemUtil.getSelectedProject().GetDocDataObject(mkDocument, id, out ppDocData);
        if (ppDocData != null)
        {
          docData = Marshal.GetIUnknownForObject((object) ppDocData);
          CopernicusProjectSystemUtil.getSelectedNode().IsItemDirty(id, docData, out isDirty);
          flag = isDirty != 0;
        }
        if (flag)
        {
          if (MessageBox.Show(string.Format(CopernicusResources.SaveFile, (object) selectedNode1.Caption), CopernicusResources.DialogTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          {
            int cancelled;
            (ProjectUtil.GetProjectForFile(mkDocument).Object as CopernicusProjectNode).SaveItem(VSSAVEFLAGS.VSSAVE_Save, mkDocument, id, docData, out cancelled);
            CopernicusProjectSystemUtil.getSelectedProject().CheckInNode(CopernicusProjectSystemUtil.getSelectedNode());
          }
          else
          {
            CopernicusProjectSystemUtil.getSelectedProject().CheckInNode(CopernicusProjectSystemUtil.getSelectedNode());
            HierarchyNode selectedNode2 = CopernicusProjectSystemUtil.getSelectedNode();
            if (selectedNode2.GetType() == typeof (CopernicusProjectFileNode))
            {
              CopernicusProjectFileNode copernicusProjectFileNode = (CopernicusProjectFileNode) selectedNode2;
              copernicusProjectFileNode.CloseStandardEditor();
              copernicusProjectFileNode.OpenWithStandardEditor();
            }
            else
            {
              if (!(selectedNode2.GetType() == typeof (CopernicusDependentFileNode)))
                return;
              CopernicusDependentFileNode dependentFileNode = (CopernicusDependentFileNode) selectedNode2;
              dependentFileNode.CloseStandardEditor();
              dependentFileNode.OpenWithStandardEditor();
            }
          }
        }
        else
          CopernicusProjectSystemUtil.getSelectedProject().CheckInNode(CopernicusProjectSystemUtil.getSelectedNode());
      }
      catch (ProtocolException ex)
      {
      }
    }

    public static void SwitchKeyUser(object sender, EventArgs e)
    {
      string EV_SUCCESS;
      string EV_ENABLED;
      CopernicusProjectSystemUtil.isKeyUserEnabled(out EV_SUCCESS, out EV_ENABLED);
      if (EV_SUCCESS == "" || !CopernicusMessageBox.Show(!(EV_ENABLED == "X") ? Resource.MsgEnableKeyUser : Resource.MsgDisableKeyUser, Resource.MsgBYD, MessageBoxButtons.YesNo).Equals((object) DialogResult.Yes))
        return;
      string enable = "X";
      if (EV_ENABLED == "X")
        enable = "";
      CopernicusProjectSystemUtil.setKeyUser(enable);
    }

    public static void UpdateAccessRights(object sender, EventArgs e)
    {
      PDI_RI_S_MESSAGE[] ET_MESSAGES;
      string EV_SUCCESS;
      new SolutionHandler().UpdateAccessRights(CopernicusProjectSystemUtil.GetCurrentSolutionName(), out ET_MESSAGES, out EV_SUCCESS);
      List<SAP.Copernicus.Core.ErrorList.Task> taskList = new List<SAP.Copernicus.Core.ErrorList.Task>();
      foreach (PDI_RI_S_MESSAGE pdiRiSMessage in ET_MESSAGES)
        taskList.Add(new SAP.Copernicus.Core.ErrorList.Task(pdiRiSMessage.TEXT, Origin.Unknown, Severity.Error, (EventHandler) null));
      ErrorSink.Instance.AddTasks((IEnumerable<SAP.Copernicus.Core.ErrorList.Task>) taskList);
    }

    public static void MCGetInstallationKey(object sender, EventArgs e)
    {
      string currentSolutionName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
      if (currentSolutionName == null || !new McInstallationKey().isCreationPossible(currentSolutionName))
        return;
      new McCreateInstallationKeyDialog(HELP_IDS.BDS_GENERATE_INSTKEY, currentSolutionName).Show();
    }

    public static void AddTaskExeUI(object sender, EventArgs e)
    {
      MSApprovalStep appStep = ApprovalStepNS.ApprovalTaskAPI.XMLSerializable<MSApprovalStep>.DeserializeFromXML(CopernicusProjectSystemUtil.GetFilepathforSelectedNode());
      SAP.Copernicus.Model.BusinessObject.BusinessObject bobyName = BTMUtil.getBObyName(appStep.ApprovalTask.TaskType.taskAttributes.attribReferenceBO.ProxyName);
      string node = appStep.ApprovalTask.TaskType.taskAttributes.attribReferenceBO.Node;
      string name1 = BTMUtil.getBONodebyName(bobyName, node).Name;
      string name2 = bobyName.RootNode.Name;
      string isSubNode = "";
      if (!name2.Equals(name1))
        isSubNode = "X";
      if (appStep.ApprovalTask.TaskExecutionUI == null || appStep.ApprovalTask.TaskExecutionUI == string.Empty)
        appStep.ApprovalTask.TaskExecutionUI = bobyName.NameKey.Name + "-" + name1 + "-" + appStep.ApprovalStepID + ".QA.uicomponent";
      CustomDialog customDialog = new CustomDialog("Please Wait", "Generating the Approval Task UI");
      BTMMSTaskExecutionUI btmmsTaskExecutionUi = new BTMMSTaskExecutionUI(appStep, bobyName);
      btmmsTaskExecutionUi.createApprovalTaskUI(isSubNode, node);
      customDialog.destroy();
      if (appStep.RevisionTask != null)
      {
        customDialog = new CustomDialog("Please Wait", "Generating the Revision Task UI");
        btmmsTaskExecutionUi.createRevisionTaskUI(isSubNode, node);
        customDialog.destroy();
      }
      if (appStep.ApprovalNotification != null)
      {
        customDialog.destroy();
        customDialog = new CustomDialog("Please Wait", "Generating the Notification factsheet");
        btmmsTaskExecutionUi.createNotificationUI(isSubNode, node);
        customDialog.destroy();
      }
      customDialog.destroy();
    }

    public static void DeleteTaskExeUI(object sender, EventArgs e)
    {
      MSApprovalStep msApprovalStep = ApprovalStepNS.ApprovalTaskAPI.XMLSerializable<MSApprovalStep>.DeserializeFromXML(CopernicusProjectSystemUtil.GetFilepathforSelectedNode());
      string taskExecutionUi = msApprovalStep.ApprovalTask.TaskExecutionUI;
      string str1 = string.Empty;
      if (msApprovalStep.RevisionTask != null)
        str1 = msApprovalStep.RevisionTask.TaskExecutionUI;
      string str2 = string.Empty;
      if (msApprovalStep.ApprovalNotification != null)
        str2 = msApprovalStep.ApprovalNotification.TaskExecutionUI;
      string str3 = "The following task execution UI(s) will be deleted :\r\n\r\n" + taskExecutionUi + "\r\n";
      if (!string.IsNullOrEmpty(str1))
        str3 = str3 + str1 + "\r\n";
      if (!string.IsNullOrEmpty(str2))
        str3 = str3 + str2 + "\r\n";
      if (!CopernicusMessageBox.Show(str3 + "\r\n Do you want to proceed?\r\n", "Delete Task execution UI", MessageBoxButtons.YesNoCancel).Equals((object) DialogResult.Yes))
        return;
      try
      {
        foreach (HierarchyNode allChildrenNode in CopernicusProjectSystemUtil.getSelectedProject().GetAllChildrenNodes((HierarchyNode) CopernicusProjectSystemUtil.getSelectedProject()))
        {
          if (allChildrenNode != null && (taskExecutionUi.Equals(allChildrenNode.Caption) || !string.IsNullOrEmpty(str1) && str1.Equals(allChildrenNode.Caption) || !string.IsNullOrEmpty(str2) && str2.Equals(allChildrenNode.Caption)))
            allChildrenNode.Remove(true);
        }
      }
      catch (Exception ex)
      {
      }
    }

    private static bool CheckCommandEnablementForSAPMenuSignOn()
    {
      return !SAP.Copernicus.Core.Protocol.Connection.getInstance().isConnected();
    }

    private static bool CheckCommandEnablementForSAPMenuSignOff()
    {
      return SAP.Copernicus.Core.Protocol.Connection.getInstance().isConnected();
    }

    public static void QuerySAPMenuCommand_BeforeQueryStatus(object sender, EventArgs e)
    {
      OleMenuCommand oleMenuCommand = sender as OleMenuCommand;
      int id = oleMenuCommand.CommandID.ID;
      if (SAP.Copernicus.Core.Protocol.Connection.getInstance().SystemMode == SystemMode.Productive && (id == 20481 || id == 257))
        oleMenuCommand.Enabled = false;
      else if ((SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.OneOff || SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.miltiCustomer) && (XRepMapper.GetInstance().GetOpenSolutionStatus() != "In Development" && id == 257))
        oleMenuCommand.Enabled = false;
      else if (SAP.Copernicus.Core.Protocol.Connection.getInstance().UsageMode == UsageMode.Scalable && XRepMapper.GetInstance().GetSolutionEditStatus() != XRepMapper.EditModes.InMaintenance && (XRepMapper.GetInstance().GetOpenSolutionStatus() != "In Development" && id == 257))
      {
        oleMenuCommand.Enabled = false;
      }
      else
      {
        switch (id)
        {
          case 32801:
            oleMenuCommand.Enabled = ContextMenuCommandHandler.CheckCommandEnablementForSAPMenuSignOff();
            break;
          case 36999:
            oleMenuCommand.Enabled = ContextMenuCommandHandler.isInSolution() && SAP.Copernicus.Core.Protocol.Connection.getInstance().SystemMode != SystemMode.Productive;
            break;
          case 37001:
            oleMenuCommand.Enabled = ContextMenuCommandHandler.CheckCommandEnablementForSAPMenuSignOff();
            break;
          case 28673:
          case 307:
            if (CopernicusProjectSystemUtil.GetCurrentSolutionName() != null)
            {
              oleMenuCommand.Enabled = true;
              break;
            }
            oleMenuCommand.Enabled = false;
            break;
          case 32785:
            oleMenuCommand.Enabled = ContextMenuCommandHandler.CheckCommandEnablementForSAPMenuSignOn();
            break;
          case 257:
            DTE2 dte = DTEUtil.GetDTE();
            if (dte.ActiveDocument != null && dte.ActiveDocument.ProjectItem != null && dte.ActiveDocument.ProjectItem.Object != null)
            {
              HierarchyNode hierarchyNode = dte.ActiveDocument.ProjectItem.Object as HierarchyNode;
              string nodeExtension = hierarchyNode.Caption.Substring(hierarchyNode.Caption.LastIndexOf('.'));
              oleMenuCommand.Enabled = !CopernicusFileExtensions.NoDirectActivation(nodeExtension);
              break;
            }
            oleMenuCommand.Enabled = false;
            break;
          case 20481:
            if (CopernicusProjectSystemUtil.GetCurrentSolutionName() == null || XRepMapper.GetInstance().CurrentOpenSolutionSubType == "TS")
            {
              oleMenuCommand.Enabled = false;
              break;
            }
            oleMenuCommand.Enabled = true;
            break;
          case 20483:
            string currentSolutionName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
            if (currentSolutionName == null)
            {
              oleMenuCommand.Enabled = false;
              break;
            }
            try
            {
              string EV_SUCCESS;
              PDI_RI_S_MESSAGE[] ET_MESSAGES;
              PDI_1O_SOLUTION_HEADER ES_SOLUTION_HEADER;
              if (new OneOff_Handler().PDI_OneOff_GET_SOLUTION_INFOS(currentSolutionName, (string) null, true, out EV_SUCCESS, out ET_MESSAGES, out ES_SOLUTION_HEADER))
              {
                if (!(ES_SOLUTION_HEADER.PROJECT_TYPE != "MULTI_CUST"))
                  goto label_26;
              }
              oleMenuCommand.Enabled = false;
              break;
            }
            catch (ApplicationException ex)
            {
              oleMenuCommand.Enabled = false;
              break;
            }
            catch (ArgumentException ex)
            {
              oleMenuCommand.Enabled = false;
              break;
            }
label_26:
            oleMenuCommand.Enabled = true;
            break;
        }
      }
    }

    private static bool isInSolution()
    {
      if (CopernicusProjectSystemUtil.GetCurrentSolutionName() != null)
        return XRepMapper.GetInstance().IsSolutionEditable();
      return false;
    }

    public static void ImplProjectTemplateNodeStatusSetReopened(ImplProjectTemplateNode node)
    {
      if (!node.UseStateChangeHandler())
      {
        CopernicusBusinessConfigurationUtil.ImplementationProjectTemplate template = CopernicusBusinessConfigurationUtil.LoadFileToTemplate(node.Url);
        template.Status = "Active";
        template.CompletionDate = (string) null;
        CopernicusBusinessConfigurationUtil.SaveTemplate(template, node.Url);
      }
      else
        node.HandleStatusChange("Reopened");
    }

    private static string GetImplementationProjectTemplateType(HierarchyNode node)
    {
      return CopernicusBusinessConfigurationUtil.LoadFileToTemplate(node.Url).Type;
    }

    public static void ImplementationProjectTemplateOverviewHandler(object sender, EventArgs e)
    {
      HierarchyNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode();
      string projectTemplateId = new BusinessConfigurationHandler().getImplementationProjectTemplateID(CopernicusProjectSystemUtil.getSelectedProject().RuntimeNamespacePrefix, selectedNode.Caption.Substring(0, selectedNode.Caption.LastIndexOf(".")), ContextMenuCommandHandler.GetImplementationProjectTemplateType(selectedNode));
      if (projectTemplateId == null)
        return;
      ByDUIUtil.LaunchUIComponent("/SAP_BYD_APPLICATION_UI/BCTools/WorkCentre/WOC_ProjectOverview_FS.FS.uicomponent", "ReadProject", "WorkSpaceID", projectTemplateId, (string) null, (string) null, (string) null, (string) null);
    }

    public static void ImplementationProjectTemplateReOpenHandler(object sender, EventArgs e)
    {
      HierarchyNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode();
      string templateID = selectedNode.Caption.Substring(0, selectedNode.Caption.LastIndexOf("."));
      new BusinessConfigurationHandler().getImplementationProjectTemplateID(CopernicusProjectSystemUtil.getSelectedProject().RuntimeNamespacePrefix, templateID, ContextMenuCommandHandler.GetImplementationProjectTemplateType(selectedNode));
      if (!CopernicusProjectSystemUtil.getSelectedProject().CheckOutNode(selectedNode))
        return;
      if (new BusinessConfigurationHandler().reopenImplementationProjectTemplate(CopernicusProjectSystemUtil.getSelectedProject().RuntimeNamespacePrefix, templateID, ContextMenuCommandHandler.GetImplementationProjectTemplateType(selectedNode)))
        ((ImplProjectTemplateNode) selectedNode).StatusSetReopened();
      CopernicusProjectSystemUtil.getSelectedProject().CheckInNode(selectedNode);
    }

    public static void ImplementationProjectTemplateCompleteHandler(object sender, EventArgs e)
    {
      HierarchyNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode();
      string templateID = selectedNode.Caption.Substring(0, selectedNode.Caption.LastIndexOf("."));
      new BusinessConfigurationHandler().getImplementationProjectTemplateID(CopernicusProjectSystemUtil.getSelectedProject().RuntimeNamespacePrefix, templateID, ContextMenuCommandHandler.GetImplementationProjectTemplateType(selectedNode));
      if (!CopernicusProjectSystemUtil.getSelectedProject().CheckOutNode(selectedNode))
        return;
      if (new BusinessConfigurationHandler().completeImplementationProjectTemplate(CopernicusProjectSystemUtil.getSelectedProject().RuntimeNamespacePrefix, templateID, ContextMenuCommandHandler.GetImplementationProjectTemplateType(selectedNode)))
        ((ImplProjectTemplateNode) selectedNode).StatusSetCompleted();
      CopernicusProjectSystemUtil.getSelectedProject().CheckInNode(selectedNode);
    }

    public static void SignToBackendHandler(object sender, EventArgs e)
    {
      if (SAP.Copernicus.Core.Protocol.Connection.getInstance().isConnected())
        return;
      ContextMenuCommandHandler.GetRepositoryViewControl(true).LogOn();
    }

    public static void SignOffBackendHandler(object sender, EventArgs e)
    {
      if (!SAP.Copernicus.Core.Protocol.Connection.getInstance().isConnected())
        return;
      ContextMenuCommandHandler.GetRepositoryViewControl(false).Logoff();
    }

    public static void OpenUIDesignerHandler(object sender, EventArgs e)
    {
      UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner("", false);
    }

    public static void LogonToBrowserHandler(object sender, EventArgs e)
    {
      ByDUIUtil.LaunchLogonUI(SAP.Copernicus.Core.Protocol.Connection.getInstance().ConnectedSystem);
    }

    public static void SaveActivateHandler(object sender, EventArgs e)
    {
      DTE2 dte = DTEUtil.GetDTE();
      if (dte.ActiveDocument == null)
        return;
      dte.ActiveDocument.ProjectItem.Save("");
      HierarchyNode node = dte.ActiveDocument.ProjectItem.Object as HierarchyNode;
      if (node == null)
        return;
      GenricNodeActions.GenericActivate(node);
    }

    public static void ExecuteQuery(object sender, EventArgs e)
    {
      HierarchyNode boNode = CopernicusProjectSystemUtil.getSelectedNode();
      string caption = boNode.Caption;
      BusinessObject_Query businessObjectQuery = (BusinessObject_Query) null;
      if (caption.EndsWith(".bo"))
      {
        SAP.Copernicus.Model.BusinessObject.BusinessObject bo = ContextMenuCommandHandler.loadBusinessObject(ContextMenuCommandHandler.getAllowedBOsInNamespace().Find((Predicate<RepositoryDataSet.BusinessObjectsRow>) (item => item.Name == caption.Replace(".bo", ""))).ProxyName);
        businessObjectQuery = ChooseQueryUI.getAllQueries(bo).Count != 2 ? new ChooseQueryUI(bo).ShowDialog() : bo.RootNode.Queries[1];
      }
      else if (caption.EndsWith(".qry"))
        businessObjectQuery = ContextMenuCommandHandler.loadBusinessObject(ContextMenuCommandHandler.getAllowedBOsInNamespace().Find((Predicate<RepositoryDataSet.BusinessObjectsRow>) (item => item.Name == boNode.Parent.Caption.Replace(".bo", ""))).ProxyName).GetNodeByName(boNode.Caption.Replace(".qry", ""), "").Queries.Find((Predicate<BusinessObject_Query>) (item => item.Name == "QueryByElements"));
      if (businessObjectQuery == null)
        return;
      QuerySelection1 querySelectionScreen = new QuerySelection1(businessObjectQuery);
      List<string[]> queryResult = querySelectionScreen.ShowDialog();
      if (queryResult.Count == 0)
      {
        int num1 = (int) CopernicusMessageBox.Show("There are no Query results");
      }
      QueryResultView queryResultView = new QueryResultView(queryResult, businessObjectQuery, querySelectionScreen);
      Form form = new Form();
      form.Size = new Size(800, 600);
      form.Text = Resource.MsgBYD;
      form.Controls.Add((Control) queryResultView);
      queryResultView.Dock = DockStyle.Fill;
      queryResultView.Show();
      int num2 = (int) form.ShowDialog();
    }

    private static List<RepositoryDataSet.BusinessObjectsRow> getAllowedBOsInNamespace()
    {
      string forSelectedProject = CopernicusProjectSystemUtil.GetNamespaceForSelectedProject();
      List<RepositoryDataSet.BusinessObjectsRow> businessObjectsRowList = new List<RepositoryDataSet.BusinessObjectsRow>();
      foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in (RepositoryDataSet.BusinessObjectsRow[]) RepositoryDataCache.GetInstance().RepositoryDataSet.BusinessObjects.Select("NSName='" + forSelectedProject + "'"))
      {
        if (businessObjectsRow.TechCategory == Convert.ToString(1) || businessObjectsRow.TechCategory == Convert.ToString(5))
          businessObjectsRowList.Add(businessObjectsRow);
      }
      return businessObjectsRowList;
    }

    public static SAP.Copernicus.Model.BusinessObject.BusinessObject loadBusinessObject(string boProxyName)
    {
      URLStore urlStore = new URLStore();
      SAP.Copernicus.Model.BusinessObject.BusinessObject businessObject;
      using (Transaction transaction = urlStore.TransactionManager.BeginTransaction("Read " + boProxyName))
      {
        businessObject = MetamodelSerializationHelper.Instance.LoadModelFromUrl<SAP.Copernicus.Model.BusinessObject.BusinessObject>(new SerializationResult(), (Store) urlStore, boProxyName, (ISchemaResolver) null, (ValidationController) null);
        transaction.Commit();
      }
      return businessObject;
    }

    public static void ShowSolExpHandler(object sender, EventArgs e)
    {
      IVsWindowFrame ppWindowFrame = (IVsWindowFrame) null;
      IVsUIShell globalService = Package.GetGlobalService(typeof (SVsUIShell)) as IVsUIShell;
      Guid rguidPersistenceSlot = new Guid("{3AE79031-E1BC-11D0-8F78-00A0C9110057}");
      globalService.FindToolWindow(524288U, ref rguidPersistenceSlot, out ppWindowFrame);
      ppWindowFrame.Show();
      string str = "Solution Explorer";
      ppWindowFrame.SetProperty(-3004, (object) str);
    }

    public static RepositoryViewControl GetRepositoryViewControl(bool loadRepositoryView = true)
    {
      if (loadRepositoryView)
      {
        IVsWindowFrame ppWindowFrame = (IVsWindowFrame) null;
        IVsUIShell globalService = Package.GetGlobalService(typeof (SVsUIShell)) as IVsUIShell;
        Guid rguidPersistenceSlot = new Guid("0128a988-1402-4c49-a09b-df226597dc6c");
        globalService.FindToolWindow(8388608U, ref rguidPersistenceSlot, out ppWindowFrame);
        if (ppWindowFrame == null)
          globalService.FindToolWindow(524288U, ref rguidPersistenceSlot, out ppWindowFrame);
        if (ppWindowFrame.IsVisible() != 0)
          Microsoft.VisualStudio.ErrorHandler.ThrowOnFailure(ppWindowFrame.Show());
      }
      ToolWindowPane toolWindow = (VSPackageUtil.ServiceProvider as CopernicusPackage).FindToolWindow(typeof (RepositoryViewWindow), 0, false);
      if (toolWindow == null)
        throw new NotSupportedException(CopernicusResources.LoadRepositoryFailed);
      return (toolWindow as RepositoryViewWindow).Window as RepositoryViewControl;
    }

    public static void GenericActivate(object sender, EventArgs e)
    {
      GenricNodeActions.GenericActivate(CopernicusProjectSystemUtil.getSelectedNode());
    }

    public static void GenericCheck(object sender, EventArgs e)
    {
      GenricNodeActions.GenericCheck(CopernicusProjectSystemUtil.getSelectedNode());
    }

    public static void GenericClean(object sender, EventArgs e)
    {
      GenricNodeActions.GenericClean(CopernicusProjectSystemUtil.getSelectedNode());
    }

    private static void GetFileSpecFromNode(HierarchyNode node, out string filePath, out string xRepPath, out string contentType)
    {
      XRepMapper instance = XRepMapper.GetInstance();
      XRepHandler xrepHandler = new XRepHandler();
      filePath = node.Url;
      xRepPath = instance.GetXrepPathforLocalFile(filePath);
      string extn = filePath.Substring(filePath.LastIndexOf(".") + 1);
      contentType = ContentTypeMapping.fromExtn(extn).ToString();
    }

    public static void GenericActivateABSL(HierarchyNode selectedNode)
    {
      CopernicusProjectNode projectMgr = selectedNode.ProjectMgr as CopernicusProjectNode;
      if (projectMgr == null)
        return;
      foreach (HierarchyNode node in projectMgr.GetAllChildrenNodes(selectedNode).Where<HierarchyNode>((Func<HierarchyNode, bool>) (child => child.Caption.EndsWith(".absl"))))
        GenricNodeActions.GenericActivate(node);
    }

    internal static void getXrepPathRecursiveByExtension(HierarchyNode node, Dictionary<string, List<string>> dictionary)
    {
      List<string> xrepPaths = new List<string>();
      ContextMenuCommandHandler.CollectXrepPath(node, xrepPaths);
      ContextMenuCommandHandler.getXrepPathRecursive(node, xrepPaths);
      foreach (string str in xrepPaths)
      {
        string key = str.Substring(str.LastIndexOf(".") + 1);
        if (dictionary.ContainsKey(key))
        {
          List<string> stringList = dictionary[key];
          if (!stringList.Contains(str))
            stringList.Add(str);
        }
        else
          dictionary.Add(key, new List<string>() { str });
      }
    }

    internal static void getXrepPathRecursive(HierarchyNode node, List<string> xrepPaths)
    {
      HierarchyNode node1 = node.FirstChild;
      if (node1 == null)
        return;
      ContextMenuCommandHandler.CollectXrepPath(node1, xrepPaths);
      while (node1.NextSibling != null)
      {
        node1 = node1.NextSibling;
        ContextMenuCommandHandler.CollectXrepPath(node1, xrepPaths);
      }
    }

    internal static void CollectXrepPath(HierarchyNode node, List<string> xrepPaths)
    {
      string filePath;
      string xRepPath;
      string contentType;
      ContextMenuCommandHandler.GetFileSpecFromNode(node, out filePath, out xRepPath, out contentType);
      xrepPaths.Add(xRepPath);
      if (node.FirstChild == null)
        return;
      ContextMenuCommandHandler.getXrepPathRecursive(node, xrepPaths);
    }

    public static void AddCustomReuseFunction(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      CRLUtils crlUtils = new CRLUtils();
      string filepathforSelectedNode = CopernicusProjectSystemUtil.GetFilepathforSelectedNode();
      crlUtils.SaveDirtyModel(filepathforSelectedNode);
      CustomReuseLibraryType model = crlUtils.getModel(filepathforSelectedNode);
      if (model == null)
        return;
      int num = (int) new CustomReuseFunctionWizard(model).ShowDialog();
    }

    public static void CreateMappingScript(object sender, EventArgs e)
    {
      if (!CopernicusProjectSystemUtil.HandleProjectOutOfSync())
        return;
      ReuseLibraryGenerator.GenerateReuseLibrary((CopernicusProjectFileNode) CopernicusProjectSystemUtil.getSelectedNode());
    }
  }
}
