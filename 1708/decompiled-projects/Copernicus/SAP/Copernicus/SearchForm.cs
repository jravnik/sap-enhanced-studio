﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.SearchForm
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Project;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus
{
  public class SearchForm : Form
  {
    private string searchString = "";
    private int resultIndex = -1;
    private string prevstring = "";
    private int index = -1;
    private IContainer components;
    private TextBox SearchBox;
    private Button Search_Botton;
    private TreeView treeViewRepository;
    private ToolStripTextBox toolStripTextBoxSearch;
    private ImageList repositoryTreeViewImages;
    private Button prev_button;
    private CopernicusProjectNode currentProject;
    private IList<HierarchyNode> childNodes;
    public HierarchyNode childNode;
    private HierarchyNode tempnode;
    private int c;

    public SearchForm()
    {
      this.InitializeComponent();
      this.SearchBox.KeyPress += new KeyPressEventHandler(this.checkForEnter_KeyPress);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.SearchBox = new TextBox();
      this.Search_Botton = new Button();
      this.prev_button = new Button();
      this.SuspendLayout();
      this.SearchBox.AccessibleDescription = "SearchBox";
      this.SearchBox.AccessibleName = "SearchBox";
      this.SearchBox.AccessibleRole = AccessibleRole.Text;
      this.SearchBox.Location = new Point(12, 12);
      this.SearchBox.Name = "SearchBox";
      this.SearchBox.Size = new Size(234, 20);
      this.SearchBox.TabIndex = 0;
      this.SearchBox.TextChanged += new EventHandler(this.textBox1_TextChanged);
      this.Search_Botton.AccessibleDescription = "Search_Botton";
      this.Search_Botton.AccessibleName = "Search_Botton";
      this.Search_Botton.Location = new Point(171, 38);
      this.Search_Botton.Name = "Search_Botton";
      this.Search_Botton.Size = new Size(75, 23);
      this.Search_Botton.TabIndex = 2;
      this.Search_Botton.Text = "Next";
      this.Search_Botton.UseVisualStyleBackColor = true;
      this.Search_Botton.Click += new EventHandler(this.button1_Click);
      this.prev_button.Location = new Point(12, 38);
      this.prev_button.Name = "prev_button";
      this.prev_button.Size = new Size(75, 23);
      this.prev_button.TabIndex = 3;
      this.prev_button.Text = "Previous";
      this.prev_button.UseVisualStyleBackColor = true;
      this.prev_button.Click += new EventHandler(this.prev_button_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(258, 71);
      this.Controls.Add((Control) this.prev_button);
      this.Controls.Add((Control) this.Search_Botton);
      this.Controls.Add((Control) this.SearchBox);
      this.Name = "SearchForm";
      this.Text = "SearchForm";
      this.Load += new EventHandler(this.SearchForm_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.SolutionSearch(true);
    }

    private void searchStringChangedCheck()
    {
      this.currentProject = CopernicusProjectSystemUtil.getSelectedProject();
      this.childNodes = (IList<HierarchyNode>) this.currentProject.GetAllChildrenNodes((HierarchyNode) this.currentProject);
      string text = this.SearchBox.Text;
      if (this.SearchBox.Text.ToUpper().Equals(this.searchString.ToUpper()))
        return;
      this.searchString = this.SearchBox.Text.ToUpper();
      this.resultIndex = -1;
    }

    private void checkForEnter_KeyPress(object sender, KeyPressEventArgs e)
    {
      if ((int) e.KeyChar != 13)
        return;
      e.Handled = true;
      if (!this.SearchBox.Focus())
        return;
      this.SolutionSearch(true);
    }

    private void SolutionSearch(bool next)
    {
      this.searchStringChangedCheck();
      if (next)
      {
        int num1 = -1;
        foreach (HierarchyNode childNode in (IEnumerable<HierarchyNode>) this.childNodes)
        {
          ++num1;
          if (childNode is FileNode && childNode.Caption.ToUpper().Contains(this.searchString) && (num1 != this.resultIndex && num1 >= this.resultIndex))
          {
            CopernicusProjectSystemUtil.SelectItemInHierarchy(childNode.GetMkDocument());
            this.SearchBox.Focus();
            this.resultIndex = num1;
            break;
          }
          if (num1 + 1 == this.childNodes.Count)
          {
            int num2 = (int) MessageBox.Show("Search reached end of the project.No more matches");
          }
        }
      }
      else
      {
        for (int index = 1 - 1; index < this.childNodes.Count; ++index)
        {
          this.childNode = this.childNodes[index];
          if (this.childNode is FileNode && this.childNode.Caption.ToUpper().Contains(this.searchString) && index < this.resultIndex)
            this.index = index;
        }
        int num1 = -1;
        foreach (HierarchyNode childNode in (IEnumerable<HierarchyNode>) this.childNodes)
        {
          ++num1;
          if (childNode is FileNode && childNode.Caption.ToUpper().Contains(this.searchString) && num1 == this.index)
          {
            CopernicusProjectSystemUtil.SelectItemInHierarchy(childNode.GetMkDocument());
            this.resultIndex = num1;
            this.SearchBox.Focus();
            break;
          }
        }
        if (this.resultIndex != 0)
          return;
        int num2 = (int) MessageBox.Show("Search reached end of the project. No more matches.");
      }
    }

    private void textBox1_TextChanged(object sender, EventArgs e)
    {
    }

    private void SearchForm_Load(object sender, EventArgs e)
    {
    }

    private void prev_button_Click(object sender, EventArgs e)
    {
      this.SolutionSearch(false);
    }
  }
}
