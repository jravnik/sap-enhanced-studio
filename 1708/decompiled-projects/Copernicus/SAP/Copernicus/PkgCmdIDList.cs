﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.PkgCmdIDList
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Protocol;
using System.Collections.Generic;

namespace SAP.Copernicus
{
  internal static class PkgCmdIDList
  {
    public const uint cmdidShowSolExp = 369;
    public const uint cmdidSaveAct = 257;
    public const uint cmdidExportText = 295;
    public const uint cmdidCopernicusRepositoryView = 257;
    public const uint cmdidCopernicusBusinessObjectBrowser = 258;
    public const uint cmdidGenerateBO = 260;
    public const uint cmdidEnhScreen = 261;
    public const uint cmdidCreateUI = 277;
    public const uint cmdidCreateForm = 278;
    public const uint cmdidEnhForm = 320;
    public const uint cmdidEnhanceReports = 321;
    public const uint cmdidEnhanceEnterpriseSearch = 322;
    public const uint cmdidMigXBO = 323;
    public const uint cmdidCheckFMT = 5217;
    public const uint cmdidActivateFMT = 5121;
    public const uint cmdidStartDebugger = 256;
    public const uint cmdidDebuggerStepOver = 257;
    public const uint cmdidDumpAnalysis = 258;
    public const uint cmdidToggleABSLSignatureAdornment = 38145;
    public const uint cmdidCreateTTReference = 1313;
    public const uint cmdidConvertToCodelist = 1312;
    public const uint cmdidCreateBCSetFromPartnerBCO = 272;
    public const uint cmdidCreateBCViewFromPartnerBCO = 273;
    public const uint cmdidEmbCompUpload = 352;
    public const uint cmdidEmbCompEdit = 353;
    public const uint cmdidEmbCompDelete = 354;
    public const uint cmdidApprovalTask = 279;
    public const uint cmdidMSApprovalTask = 535;
    public const uint cmdidDeployTask = 280;
    public const uint cmdidCheckTask = 308;
    public const uint cmdidCleanTask = 309;
    public const uint cmdidAddBusinessTask = 281;
    public const uint cmdidAddTaskExecutionUI = 310;
    public const uint cmdidCreateProcessIntegration = 288;
    public const uint cmdidCreateQD = 292;
    public const uint cmdidEnableNR = 313;
    public const uint cmdidCreateDataSource = 336;
    public const uint cmdidPreviewQuery = 296;
    public const uint cmdidCreateReportQuery = 297;
    public const uint cmdidManageReportsQuery = 304;
    public const uint cmdidEnhBO = 306;
    public const uint cmdidUIScriptFiles = 13665;
    public const uint cmdidConfigurePI = 312;
    public const uint cmdidCreateCommunicationSystem = 368;
    public const uint cmdidCreateCommunicationArrangement = 369;
    public const uint cmdidDownloadWSDL = 4181;
    public const uint cmdidAddToWoCView = 4438;
    public const uint cmdidActivateExtUI = 4470;
    public const uint cmdidGenerateBOService = 4129;
    public const uint cmdidDisplayWSDLURL = 4133;
    public const uint cmdidAddWSAuthFile = 5476;
    public const uint cmdidAddCSDFile = 5480;
    public const uint cmdidOpenKeyUserUI = 5481;
    public const uint cmdidPreviewReport = 5488;
    public const uint cmdidWsidTestTool = 5490;
    public const uint cmdidCallBydStudioHelp = 36865;
    public const uint cmdidHelpContentInstallation = 36867;
    public const uint cmdidCallAboutBox = 39321;
    public const uint cmdidCallSCN = 36869;
    public const uint cmdidCallCheckForUpdate = 39320;
    public const uint cmdidOpenFloorPlan = 293;
    public const uint cmdidRunFloorPlan = 294;
    public const uint cmdidRefreshSolution = 401;
    public const uint cmdidAddNewItem = 402;
    public const uint cmdidMigrateSolution = 403;
    public const uint cmdidSearchSolution = 405;
    public const uint cmdidCreatePatchOpen = 406;
    public const uint cmdidCreatePatchClose = 407;
    public const uint cmdidOpenFTV = 4403;
    public const uint cmdidManageFTV = 4408;
    public const uint cmdidCheckEFE = 4409;
    public const uint cmdidCheckout = 8504;
    public const uint cmdidCheckin = 8505;
    public const uint cmdidEditProject = 12600;
    public const uint cmdidActivateAll = 12608;
    public const uint cmdidCleanProject = 12609;
    public const uint cmdidCopySolution = 12610;
    public const uint cmdidMergeSolution = 12624;
    public const uint cmdidTransExpUIText = 13668;
    public const uint cmdidTransImpUIText = 13669;
    public const uint cmdidTransUICheck = 13670;
    public const uint cmdidTransExpSolText = 13671;
    public const uint cmdidTransImpSolText = 13672;
    public const uint cmdidTransSolCheck = 13673;
    public const uint cmdidDeployBC = 13680;
    public const uint cmdidSolutionActivate = 13681;
    public const uint cmdidSolutionActivateDelta = 13682;
    public const uint cmdidCheckItems = 13696;
    public const uint cmdidCheckItemsAndRuntime = 13697;
    public const uint cmdidCheckHTML5 = 13701;
    public const uint cmdidEnableBackgroundMode = 13698;
    public const uint cmdidDisableBackgroundMode = 13700;
    public const uint cmdidDisplaylogs = 13699;
    public const uint cmdidCleanUpSolution = 12612;
    public const uint cmdidCreateMDROScr = 1298;
    public const uint cmdidCreateMDRO = 1299;
    public const uint cmdidGenericActivate = 24577;
    public const uint cmdidGenericClean = 24578;
    public const uint cmdidGenericCheck = 24579;
    public const uint cmdidBCImplProjectTemplOverview = 769;
    public const uint cmdidBCImplProjectTemplComplete = 770;
    public const uint cmdidBCImplProjectTemplReopen = 771;
    public const uint cmdidAddCustomReuseFunction = 257;
    public const uint cmdidExecuteQuery = 4471;
    public const uint cmdidRevertProdChange = 2457;
    public const uint cmdidPerformanceCheck = 24580;
    public const uint cmdidXODataOpen = 1862;
    public const uint cmdidCreateMappingScript = 4472;
    public const int cmdidSignToBackend = 32785;
    public const int cmdidSignOutFromBackend = 32801;
    public const int cmdidOpenUIDesigner = 36999;
    public const int cmdidLogonToBrowser = 37001;
    public const int cmdidOptionSAP = 37009;
    public const int cmdidUpdateAccessRights = 28673;
    public const int cmdidSwitchKeyUser = 20481;
    public const int cmdidCallVS = 307;
    public const int cmdidMCGetInstallationKey = 20483;

    public static List<uint> CommandsCausingChange()
    {
      return new List<uint>() { 402U, 403U, 260U, 261U, 277U, 278U, 320U, 321U, 322U, 1313U, 323U, 352U, 353U, 354U, 279U, 535U, 280U, 281U, 310U, 288U, 292U, 313U, 336U, 297U, 304U, 307U, 306U, 13665U, 293U, 312U, 368U, 369U, 4438U, 4129U, 4408U, 8504U, 8505U, 12600U, 12608U, 13681U, 13682U, 12610U, 12624U, 12612U, 28673U, 13680U, 1298U, 4472U, 406U, 407U };
    }

    public static List<uint> CommandsEnabledForOneOff()
    {
      List<uint> uintList = new List<uint>();
      uintList.Add(257U);
      uintList.Add(258U);
      uintList.Add(260U);
      uintList.Add(261U);
      uintList.Add(277U);
      uintList.Add(278U);
      uintList.Add(320U);
      uintList.Add(321U);
      uintList.Add(322U);
      uintList.Add(292U);
      uintList.Add(313U);
      uintList.Add(296U);
      uintList.Add(297U);
      uintList.Add(304U);
      uintList.Add(5217U);
      uintList.Add(5121U);
      uintList.Add(336U);
      uintList.Add(13665U);
      uintList.Add(306U);
      uintList.Add(293U);
      uintList.Add(294U);
      uintList.Add(401U);
      uintList.Add(403U);
      uintList.Add(402U);
      uintList.Add(405U);
      uintList.Add(406U);
      uintList.Add(407U);
      uintList.Add(4403U);
      uintList.Add(4408U);
      uintList.Add(4409U);
      uintList.Add(8504U);
      uintList.Add(8505U);
      uintList.Add(12600U);
      uintList.Add(12608U);
      uintList.Add(12609U);
      uintList.Add(12610U);
      uintList.Add(12624U);
      uintList.Add(12612U);
      uintList.Add(20481U);
      uintList.Add(28673U);
      uintList.Add(20483U);
      uintList.Add(36865U);
      uintList.Add(36867U);
      uintList.Add(39321U);
      uintList.Add(39320U);
      uintList.Add(5476U);
      uintList.Add(5480U);
      uintList.Add(5490U);
      uintList.Add(4181U);
      uintList.Add(4129U);
      uintList.Add(369U);
      uintList.Add(368U);
      uintList.Add(13696U);
      uintList.Add(13697U);
      uintList.Add(13701U);
      uintList.Add(13698U);
      uintList.Add(13700U);
      uintList.Add(13699U);
      uintList.Add(13681U);
      uintList.Add(13682U);
      uintList.Add(13680U);
      if (Connection.getInstance().RestrictionMode == RestrictionMode.ReadAndWrite)
        uintList.Add(288U);
      uintList.Add(279U);
      uintList.Add(535U);
      uintList.Add(280U);
      uintList.Add(281U);
      uintList.Add(307U);
      uintList.Add(309U);
      uintList.Add(281U);
      uintList.Add(310U);
      uintList.Add(13668U);
      uintList.Add(13669U);
      uintList.Add(13670U);
      uintList.Add(13671U);
      uintList.Add(13672U);
      uintList.Add(13673U);
      uintList.Add(352U);
      uintList.Add(353U);
      uintList.Add(354U);
      uintList.Add(4472U);
      uintList.Add(323U);
      uintList.Add(2457U);
      uintList.Add(24580U);
      uintList.Add(1862U);
      return uintList;
    }

    public static List<uint> CommandsEnabledForProductionBugFix()
    {
      return new List<uint>() { 406U, 407U };
    }
  }
}
