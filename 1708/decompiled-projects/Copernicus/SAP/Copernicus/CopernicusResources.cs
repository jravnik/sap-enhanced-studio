﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusResources
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [CompilerGenerated]
  [DebuggerNonUserCode]
  public class CopernicusResources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) CopernicusResources.resourceMan, (object) null))
          CopernicusResources.resourceMan = new ResourceManager("SAP.Copernicus.CopernicusResources", typeof (CopernicusResources).Assembly);
        return CopernicusResources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return CopernicusResources.resourceCulture;
      }
      set
      {
        CopernicusResources.resourceCulture = value;
      }
    }

    public static Bitmap _180Warning
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("180Warning", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap _21BONode_16x16
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("21BONode_16x16", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap _22Association16x16
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("22Association16x16", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap _254HorizontalWindows16x16
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("254HorizontalWindows16x16", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap _255VerticalWindows16x16
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("255VerticalWindows16x16", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap _271help16x16
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("271help16x16", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap Action
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("Action", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap Activate
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("Activate", CopernicusResources.resourceCulture);
      }
    }

    public static string ActivateContentTitle
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("ActivateContentTitle", CopernicusResources.resourceCulture);
      }
    }

    public static string ActivateFormsGroup
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("ActivateFormsGroup", CopernicusResources.resourceCulture);
      }
    }

    public static string ActivatePrintForms
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("ActivatePrintForms", CopernicusResources.resourceCulture);
      }
    }

    public static string BAdIFolderName
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("BAdIFolderName", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap BOInstance16x16
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("BOInstance16x16", CopernicusResources.resourceCulture);
      }
    }

    public static string Btn_Cancel
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("Btn_Cancel", CopernicusResources.resourceCulture);
      }
    }

    public static string Btn_Ok
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("Btn_Ok", CopernicusResources.resourceCulture);
      }
    }

    public static string BYD_STUDIO
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("BYD_STUDIO", CopernicusResources.resourceCulture);
      }
    }

    public static string Call_VS_Failed
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("Call_VS_Failed", CopernicusResources.resourceCulture);
      }
    }

    public static string CanNotCreateWindow
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("CanNotCreateWindow", CopernicusResources.resourceCulture);
      }
    }

    public static string CheckContentTitle
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("CheckContentTitle", CopernicusResources.resourceCulture);
      }
    }

    public static string CheckInAllFailed
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("CheckInAllFailed", CopernicusResources.resourceCulture);
      }
    }

    public static string CleanContentTitle
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("CleanContentTitle", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap ClearSearch16x16
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("ClearSearch16x16", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap CloseSolution
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("CloseSolution", CopernicusResources.resourceCulture);
      }
    }

    public static string copyright2008MS
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("copyright2008MS", CopernicusResources.resourceCulture);
      }
    }

    public static string copyright2008SAP
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("copyright2008SAP", CopernicusResources.resourceCulture);
      }
    }

    public static string copyright2010MS
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("copyright2010MS", CopernicusResources.resourceCulture);
      }
    }

    public static string copyright2010SAP
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("copyright2010SAP", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap CreateBO
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("CreateBO", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap CreateSolution
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("CreateSolution", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap Delete
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("Delete", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap DeleteSolution
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("DeleteSolution", CopernicusResources.resourceCulture);
      }
    }

    public static string DialogTitle
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("DialogTitle", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap Edit
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("Edit", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap EditImpl
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("EditImpl", CopernicusResources.resourceCulture);
      }
    }

    public static string FormCreateBO_ExistenceValidation_Error
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("FormCreateBO_ExistenceValidation_Error", CopernicusResources.resourceCulture);
      }
    }

    public static string FormCreateBO_NameValidation_Caption
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("FormCreateBO_NameValidation_Caption", CopernicusResources.resourceCulture);
      }
    }

    public static string FormCreateBO_NameValidation_Error
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("FormCreateBO_NameValidation_Error", CopernicusResources.resourceCulture);
      }
    }

    public static string FormCreateBOName
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("FormCreateBOName", CopernicusResources.resourceCulture);
      }
    }

    public static string FormCreateBOTitle
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("FormCreateBOTitle", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap GenerateBO
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("GenerateBO", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap GoToNextHS
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("GoToNextHS", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap GoToPreviousHS
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("GoToPreviousHS", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap Help
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("Help", CopernicusResources.resourceCulture);
      }
    }

    public static string labelBuildDate
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("labelBuildDate", CopernicusResources.resourceCulture);
      }
    }

    public static string labelBuildNumber
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("labelBuildNumber", CopernicusResources.resourceCulture);
      }
    }

    public static string LoadRepositoryFailed
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("LoadRepositoryFailed", CopernicusResources.resourceCulture);
      }
    }

    public static string Logoff
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("Logoff", CopernicusResources.resourceCulture);
      }
    }

    public static string Logon
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("Logon", CopernicusResources.resourceCulture);
      }
    }

    public static string LogonPatchLevel
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("LogonPatchLevel", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap Method
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("Method", CopernicusResources.resourceCulture);
      }
    }

    public static string MySolCtxMenuCreateSol
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("MySolCtxMenuCreateSol", CopernicusResources.resourceCulture);
      }
    }

    public static string NameCharError
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("NameCharError", CopernicusResources.resourceCulture);
      }
    }

    public static string NameDescError
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("NameDescError", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap OpenSolution
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("OpenSolution", CopernicusResources.resourceCulture);
      }
    }

    public static string PCDdisabled
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("PCDdisabled", CopernicusResources.resourceCulture);
      }
    }

    public static string PCDenabled
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("PCDenabled", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap ProcessIntegration
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("ProcessIntegration", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap ProcessIntegrationActivate
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("ProcessIntegrationActivate", CopernicusResources.resourceCulture);
      }
    }

    public static string ProdBugFixDisclaimer
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("ProdBugFixDisclaimer", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap Properties
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("Properties", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap Query
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("Query", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap Refresh
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("Refresh", CopernicusResources.resourceCulture);
      }
    }

    public static string RefreshSolutionTitle
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("RefreshSolutionTitle", CopernicusResources.resourceCulture);
      }
    }

    public static string RefreshText
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("RefreshText", CopernicusResources.resourceCulture);
      }
    }

    public static string RepositoryLoading
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("RepositoryLoading", CopernicusResources.resourceCulture);
      }
    }

    public static string RepositoryLogon
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("RepositoryLogon", CopernicusResources.resourceCulture);
      }
    }

    public static string RepositoryVersion
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("RepositoryVersion", CopernicusResources.resourceCulture);
      }
    }

    public static string SaveFile
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("SaveFile", CopernicusResources.resourceCulture);
      }
    }

    public static string SuccessMessageTitle
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("SuccessMessageTitle", CopernicusResources.resourceCulture);
      }
    }

    public static string TaskCheckedSuccessfully
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TaskCheckedSuccessfully", CopernicusResources.resourceCulture);
      }
    }

    public static string TaskCleanedSuccessfully
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TaskCleanedSuccessfully", CopernicusResources.resourceCulture);
      }
    }

    public static string TaskDepolyedSuccessfully
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TaskDepolyedSuccessfully", CopernicusResources.resourceCulture);
      }
    }

    public static string titleAboutBox
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("titleAboutBox", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuCreateBO
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuCreateBO", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuCreateScript
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuCreateScript", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuDeleteBO
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuDeleteBO", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuDeleteScript
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuDeleteScript", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuDeleteSolution
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuDeleteSolution", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuEditBO
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuEditBO", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuGenerateBO
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuGenerateBO", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuReadScript
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuReadScript", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuRefresh
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuRefresh", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuViewBO
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuViewBO", CopernicusResources.resourceCulture);
      }
    }

    public static string TNCtxMenuViewDT
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TNCtxMenuViewDT", CopernicusResources.resourceCulture);
      }
    }

    public static string ToolWindowTitle
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("ToolWindowTitle", CopernicusResources.resourceCulture);
      }
    }

    public static string TreeNodeTextActionFolder
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TreeNodeTextActionFolder", CopernicusResources.resourceCulture);
      }
    }

    public static string TreeNodeTextBO
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TreeNodeTextBO", CopernicusResources.resourceCulture);
      }
    }

    public static string TreeNodeTextDeterminationFolder
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TreeNodeTextDeterminationFolder", CopernicusResources.resourceCulture);
      }
    }

    public static string TreeNodeTextDT
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("TreeNodeTextDT", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap View
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("View", CopernicusResources.resourceCulture);
      }
    }

    public static Bitmap View1
    {
      get
      {
        return (Bitmap) CopernicusResources.ResourceManager.GetObject("View1", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitActivate
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitActivate", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitBOGenerate
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitBOGenerate", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitCaption
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitCaption", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitCheck
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitCheck", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitClean
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitClean", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitDeleteSolution
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitDeleteSolution", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitOpenSolution
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitOpenSolution", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitPIActivate
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitPIActivate", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitPIClean
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitPIClean", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitRefresh
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitRefresh", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitScriptCreate
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitScriptCreate", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitScriptDelete
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitScriptDelete", CopernicusResources.resourceCulture);
      }
    }

    public static string WaitScriptRead
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WaitScriptRead", CopernicusResources.resourceCulture);
      }
    }

    public static string WebServiceActivatedSuccesfully
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WebServiceActivatedSuccesfully", CopernicusResources.resourceCulture);
      }
    }

    public static string WebServiceCheckedSuccesfully
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WebServiceCheckedSuccesfully", CopernicusResources.resourceCulture);
      }
    }

    public static string WebServiceCleanedSuccesfully
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("WebServiceCleanedSuccesfully", CopernicusResources.resourceCulture);
      }
    }

    public static string XBOMigrationSuccessfully
    {
      get
      {
        return CopernicusResources.ResourceManager.GetString("XBOMigrationSuccessfully", CopernicusResources.resourceCulture);
      }
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    internal CopernicusResources()
    {
    }
  }
}
