﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusOptionPage
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.ToolOptions;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAP.Copernicus
{
  [ComVisible(true)]
  [ClassInterface(ClassInterfaceType.AutoDual)]
  [Guid("1A0791D6-3987-4f49-97B6-3AC92E59D9D6")]
  [CLSCompliant(false)]
  public class CopernicusOptionPage : OptionPageFields
  {
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    protected override IWin32Window Window
    {
      get
      {
        CopernicusOptionDialog copernicusOptionDialog = new CopernicusOptionDialog(this);
        copernicusOptionDialog.Initialize();
        return (IWin32Window) copernicusOptionDialog;
      }
    }
  }
}
