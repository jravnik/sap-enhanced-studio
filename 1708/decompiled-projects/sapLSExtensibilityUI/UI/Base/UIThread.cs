﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Base.UIThread
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using System;
using System.Windows.Threading;

namespace SAP.BYD.LS.UI.Base
{
  public class UIThread
  {
    public static Dispatcher Dispatcher { get; set; }

    public static bool CheckAccess()
    {
      if (UIThread.Dispatcher == null)
        return true;
      return UIThread.Dispatcher.CheckAccess();
    }

    public static void Invoke(Delegate d, params object[] args)
    {
      if ((object) d == null)
        throw new ArgumentNullException("d");
      if (UIThread.Dispatcher.CheckAccess())
        d.DynamicInvoke(args);
      else
        UIThread.Dispatcher.BeginInvoke(d, args);
    }
  }
}
