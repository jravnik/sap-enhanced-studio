﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Controls.ByDMessage
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Controls.Core;

namespace SAP.BYD.LS.UI.Controls
{
  public class ByDMessage : IByDMessage
  {
    public Severity Severity { get; set; }

    public string Description { get; set; }

    public string Key { get; set; }

    public string HelpLink { get; set; }

    public string HelpLinkDisplayText { get; set; }

    public override string ToString()
    {
      return ((int) this.Severity).ToString() + " " + this.Description;
    }
  }
}
