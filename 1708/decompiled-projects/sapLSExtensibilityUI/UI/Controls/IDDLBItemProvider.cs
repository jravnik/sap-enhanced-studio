﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Controls.IDDLBItemProvider
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

namespace SAP.BYD.LS.UI.Controls
{
  public interface IDDLBItemProvider
  {
    string Key { get; }

    string VisualKey { get; }

    string Value { get; }

    bool Matches(string match, bool matchKey, bool matchValue);
  }
}
