﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.ExtAppearanceInfo
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar
{
  public class ExtAppearanceInfo
  {
    public FlexAnchorEnumType Type { get; set; }

    public string XrepPath { get; set; }

    public string AppearanceKey { get; set; }

    public string AppearanceText { get; set; }
  }
}
