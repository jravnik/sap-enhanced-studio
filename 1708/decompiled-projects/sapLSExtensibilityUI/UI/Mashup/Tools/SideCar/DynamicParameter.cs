﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.DynamicParameter
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Mashup.Common;
using System.Collections.ObjectModel;

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar
{
  public class DynamicParameter : ObservableBase
  {
    private string name;
    private string binding;
    private string bindingText;
    private bool isMandatory;
    private ObservableCollection<ExtBindingOption> bindingOptions;

    public bool Bound { get; set; }

    public bool IsOutput { get; set; }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
        this.NotifyPropertyChanged("Name");
      }
    }

    public string Binding
    {
      get
      {
        return this.binding;
      }
      set
      {
        this.binding = value;
        this.NotifyPropertyChanged("Binding");
      }
    }

    public string BindingText
    {
      get
      {
        return this.bindingText;
      }
      set
      {
        this.bindingText = value;
        this.NotifyPropertyChanged("BindingText");
      }
    }

    public bool IsMandatory
    {
      get
      {
        return this.isMandatory;
      }
      set
      {
        this.isMandatory = value;
        this.NotifyPropertyChanged("IsMandatory");
      }
    }

    public bool IsPropertyPanelEnabled { get; set; }

    public ObservableCollection<ExtBindingOption> BindingOptions
    {
      get
      {
        return this.bindingOptions;
      }
      set
      {
        this.bindingOptions = value;
        this.NotifyPropertyChanged("BindingOptions");
      }
    }

    public void Terminate()
    {
      if (this.BindingOptions == null)
        return;
      this.BindingOptions.Clear();
      this.BindingOptions = (ObservableCollection<ExtBindingOption>) null;
    }
  }
}
