﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord.InsertPaneContainerHandler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base.Exceptions;
using SAP.BYD.LS.UI.Core.Model;
using System.Collections.Generic;

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord
{
  public class InsertPaneContainerHandler
  {
    private string embeddedComponentName;
    private string headerText;
    private string visibilityBindingExp;

    public InsertPaneContainerHandler(string embeddedComponentName, string headerText, string visibilityBindingExp)
    {
      this.embeddedComponentName = embeddedComponentName;
      this.headerText = headerText;
      this.visibilityBindingExp = visibilityBindingExp;
    }

    public PaneContainerType ConstructPaneContainer()
    {
      if (string.IsNullOrEmpty(this.embeddedComponentName))
        throw new InvalidArgumentException("embeddedComponentName could not be empty or null.");
      if (string.IsNullOrEmpty(this.headerText))
        throw new InvalidArgumentException("headerText could not be null or empty.");
      PaneContainerType paneContainerType1 = new PaneContainerType();
      paneContainerType1.id = CommonUtil.CreateElementID();
      paneContainerType1.row = 0;
      paneContainerType1.column = 0;
      PaneContainerType paneContainerType2 = paneContainerType1;
      List<ModelEntity> modelEntityList1 = new List<ModelEntity>();
      List<ModelEntity> modelEntityList2 = modelEntityList1;
      EmbedComponentPaneType componentPaneType1 = new EmbedComponentPaneType();
      componentPaneType1.bodyStyle = PaneBodyStyleType.Fill;
      componentPaneType1.id = CommonUtil.CreateElementID();
      componentPaneType1.embedName = this.embeddedComponentName;
      componentPaneType1.collapseIconVisible = true;
      EmbedComponentPaneType componentPaneType2 = componentPaneType1;
      PaneHeaderType paneHeaderType1 = new PaneHeaderType();
      paneHeaderType1.id = CommonUtil.CreateElementID();
      paneHeaderType1.fallbackValue = this.headerText ?? string.Empty;
      paneHeaderType1.fallbackValueType = DataFieldTypes.@string;
      PaneHeaderType paneHeaderType2 = paneHeaderType1;
      componentPaneType2.Header = paneHeaderType2;
      EmbedComponentPaneType componentPaneType3 = componentPaneType1;
      DependentPropertyType dependentPropertyType1;
      if (string.IsNullOrEmpty(this.visibilityBindingExp))
      {
        dependentPropertyType1 = (DependentPropertyType) null;
      }
      else
      {
        DependentPropertyType dependentPropertyType2 = new DependentPropertyType();
        dependentPropertyType2.id = CommonUtil.CreateElementID();
        dependentPropertyType2.bindingExpression = this.visibilityBindingExp;
        dependentPropertyType1 = dependentPropertyType2;
      }
      componentPaneType3.Visible = dependentPropertyType1;
      EmbedComponentPaneType componentPaneType4 = componentPaneType1;
      modelEntityList2.Add((ModelEntity) componentPaneType4);
      List<ModelEntity> modelEntityList3 = modelEntityList1;
      paneContainerType2.Items = modelEntityList3;
      return paneContainerType1;
    }
  }
}
