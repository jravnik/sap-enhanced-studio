﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord.InsertMashupHelper
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using System;
using System.Collections.Generic;

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord
{
  public class InsertMashupHelper
  {
    internal static EmbeddedComponentType ConstructEmbeddedComponent(string targetComponentName, string mashupPipeID, string outPortName, string inPortName, string displayText, string appearanceKey, bool fullColumnSpan, MashupViewModel changeRecord = null)
    {
      if (string.IsNullOrEmpty(targetComponentName))
        throw new ArgumentException("targetComponentName could not be null or empty.");
      if (string.IsNullOrEmpty(mashupPipeID))
        throw new ArgumentException("mashupPipeID could not be null or empty.");
      if (string.IsNullOrEmpty(displayText))
        throw new ArgumentException("displayText could not be null or empty.");
      if (string.IsNullOrEmpty(appearanceKey))
        throw new ArgumentException("appearanceKey could not be null or empty.");
      List<UXParameterType> uxParameterTypeList1 = new List<UXParameterType>();
      List<UXParameterType> uxParameterTypeList2 = uxParameterTypeList1;
      UXParameterType uxParameterType1 = new UXParameterType();
      uxParameterType1.id = CommonUtil.CreateElementID();
      uxParameterType1.name = "SourceOutPort";
      uxParameterType1.constant = outPortName ?? string.Empty;
      UXParameterType uxParameterType2 = uxParameterType1;
      uxParameterTypeList2.Add(uxParameterType2);
      List<UXParameterType> uxParameterTypeList3 = uxParameterTypeList1;
      UXParameterType uxParameterType3 = new UXParameterType();
      uxParameterType3.id = CommonUtil.CreateElementID();
      uxParameterType3.name = "TargetInPort";
      uxParameterType3.constant = inPortName ?? string.Empty;
      UXParameterType uxParameterType4 = uxParameterType3;
      uxParameterTypeList3.Add(uxParameterType4);
      List<UXParameterType> uxParameterTypeList4 = uxParameterTypeList1;
      UXParameterType uxParameterType5 = new UXParameterType();
      uxParameterType5.id = CommonUtil.CreateElementID();
      uxParameterType5.name = "MashupDisplayText";
      uxParameterType5.constant = displayText;
      UXParameterType uxParameterType6 = uxParameterType5;
      uxParameterTypeList4.Add(uxParameterType6);
      List<UXParameterType> uxParameterTypeList5 = uxParameterTypeList1;
      UXParameterType uxParameterType7 = new UXParameterType();
      uxParameterType7.id = CommonUtil.CreateElementID();
      uxParameterType7.name = "MashupAppearanceKey";
      uxParameterType7.constant = appearanceKey;
      UXParameterType uxParameterType8 = uxParameterType7;
      uxParameterTypeList5.Add(uxParameterType8);
      List<UXParameterType> uxParameterTypeList6 = uxParameterTypeList1;
      UXParameterType uxParameterType9 = new UXParameterType();
      uxParameterType9.id = CommonUtil.CreateElementID();
      uxParameterType9.name = "FullColumnSpan";
      uxParameterType9.constant = fullColumnSpan.ToString();
      UXParameterType uxParameterType10 = uxParameterType9;
      uxParameterTypeList6.Add(uxParameterType10);
      List<UXParameterType> uxParameterTypeList7 = uxParameterTypeList1;
      UXParameterType uxParameterType11 = new UXParameterType();
      uxParameterType11.id = CommonUtil.CreateElementID();
      uxParameterType11.name = "VisibilityBinding";
      uxParameterType11.constant = changeRecord.VisibilityBindingExpression;
      UXParameterType uxParameterType12 = uxParameterType11;
      uxParameterTypeList7.Add(uxParameterType12);
      List<UXParameterType> uxParameterTypeList8 = uxParameterTypeList1;
      EmbeddedComponentType embeddedComponentType1 = new EmbeddedComponentType();
      embeddedComponentType1.id = CommonUtil.CreateElementID();
      embeddedComponentType1.targetComponentID = targetComponentName;
      embeddedComponentType1.embeddingStyle = EmbeddingStyleType.mashup;
      embeddedComponentType1.embedName = string.Format("$Mashup{0}/$MashupPipeID={1}", (object) (CommonUtil.CreateElementID() + CommonUtil.CreateElementID()), (object) mashupPipeID);
      embeddedComponentType1.Parameter = uxParameterTypeList8;
      EmbeddedComponentType embeddedComponentType2 = embeddedComponentType1;
      bool flag = outPortName != "ExtensionFieldsOutPort";
      if (changeRecord.DynamicBindings.Count > 0)
      {
        foreach (KeyValuePair<string, ExtBindingOption> dynamicBinding in changeRecord.DynamicBindings)
        {
          if (flag || dynamicBinding.Value.IsOutput)
          {
            List<UXParameterType> uxParameterTypeList9 = uxParameterTypeList8;
            UXParameterType uxParameterType13 = new UXParameterType();
            uxParameterType13.id = CommonUtil.CreateElementID();
            uxParameterType13.name = dynamicBinding.Value.ParameterName;
            uxParameterType13.bind = dynamicBinding.Value.ParameterBind;
            UXParameterType uxParameterType14 = uxParameterType13;
            uxParameterTypeList9.Add(uxParameterType14);
          }
          List<UXParameterType> uxParameterTypeList10 = uxParameterTypeList8;
          UXParameterType uxParameterType15 = new UXParameterType();
          uxParameterType15.id = CommonUtil.CreateElementID();
          uxParameterType15.name = dynamicBinding.Key;
          uxParameterType15.bind = dynamicBinding.Value.ParameterName;
          UXParameterType uxParameterType16 = uxParameterType15;
          uxParameterTypeList10.Add(uxParameterType16);
        }
      }
      return embeddedComponentType2;
    }

    internal static FlexBaseAnchorType ConstructLeadingAnchor(MashupViewModel changeRecord, FlexBaseAnchorType floorplanAnchor)
    {
      string appearanceKey = changeRecord.AppearanceKey;
      foreach (KeyValuePair<string, ExtBindingOption> dynamicBinding in changeRecord.DynamicBindings)
      {
        Dictionary<string, ExtAppearanceInfo> appearanceInfo = dynamicBinding.Value.AppearanceInfo;
        if (appearanceInfo[changeRecord.ViewStyle].AppearanceKey == appearanceKey)
          return new FlexBaseAnchorType()
          {
            type = appearanceInfo[changeRecord.ViewStyle].Type,
            xrepPath = appearanceInfo[changeRecord.ViewStyle].XrepPath
          };
      }
      if (appearanceKey == CommonUtil.FunctionBarAsString && floorplanAnchor != null && floorplanAnchor.type == FlexAnchorEnumType.FloorPlanAnchor || (appearanceKey.StartsWith("V-") || appearanceKey.StartsWith(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ViewSwitch))))
        return floorplanAnchor;
      return (FlexBaseAnchorType) null;
    }

    internal static OnNavigateType ConstructOnNavigate(string sourceComponentId, string outPlugName, string targetComponentName, string inPlugName, UXPortType port, MashupViewModel changeRecord = null)
    {
      if (string.IsNullOrEmpty(sourceComponentId))
        throw new ArgumentException("sourceComponentId could not be null or empty.");
      if (string.IsNullOrEmpty(outPlugName))
        throw new ArgumentException("outPlugName could not be null or empty.");
      if (string.IsNullOrEmpty(targetComponentName))
        throw new ArgumentException("targetComponentName could not be null or empty.");
      if (string.IsNullOrEmpty(inPlugName))
        throw new ArgumentException("inPlugName could not be null or empty.");
      OnNavigateType onNavigateType1 = new OnNavigateType();
      onNavigateType1.id = CommonUtil.CreateElementID();
      onNavigateType1.sourceComponentID = sourceComponentId;
      onNavigateType1.outPlug = outPlugName;
      onNavigateType1.targetComponentID = targetComponentName;
      onNavigateType1.inPlug = inPlugName;
      onNavigateType1.style = NavigateStyle.inscreen_dataflow;
      OnNavigateType onNavigateType2 = onNavigateType1;
      List<PassParameterType> passParameterTypeList1 = new List<PassParameterType>();
      List<PassParameterType> passParameterTypeList2 = passParameterTypeList1;
      PassParameterType passParameterType1 = new PassParameterType();
      passParameterType1.id = CommonUtil.CreateElementID();
      passParameterType1.inParamName = "$MashupPipeID";
      passParameterType1.outParamName = "$MashupPipeID";
      PassParameterType passParameterType2 = passParameterType1;
      passParameterTypeList2.Add(passParameterType2);
      List<PassParameterType> passParameterTypeList3 = passParameterTypeList1;
      PassParameterType passParameterType3 = new PassParameterType();
      passParameterType3.id = CommonUtil.CreateElementID();
      passParameterType3.inParamName = "$MashupEmbeddedName";
      passParameterType3.outParamName = "$MashupEmbeddedName";
      PassParameterType passParameterType4 = passParameterType3;
      passParameterTypeList3.Add(passParameterType4);
      List<PassParameterType> passParameterTypeList4 = passParameterTypeList1;
      onNavigateType2.PassParameter = passParameterTypeList4;
      onNavigateType1.PassListParameter = new List<PassListParameterType>();
      OnNavigateType onNavigateType3 = onNavigateType1;
      if (port != null)
      {
        if (outPlugName != "ExtensionFieldsOutPort")
        {
          if (port.Parameter != null)
          {
            foreach (UXParameterType uxParameterType in port.Parameter)
            {
              PassParameterType passParameterType5 = new PassParameterType();
              passParameterType5.id = CommonUtil.CreateElementID();
              passParameterType5.inParamName = uxParameterType.name;
              passParameterType5.outParamName = uxParameterType.name;
              PassParameterType passParameterType6 = passParameterType5;
              onNavigateType3.PassParameter.Add(passParameterType6);
            }
          }
          if (port.ListParameter != null)
          {
            foreach (UXListParameterType listParameterType1 in port.ListParameter)
            {
              List<PassParameterType> passParameterTypeList5 = new List<PassParameterType>();
              if (listParameterType1.Parameter != null)
              {
                foreach (UXParameterType uxParameterType in listParameterType1.Parameter)
                {
                  PassParameterType passParameterType5 = new PassParameterType();
                  passParameterType5.id = CommonUtil.CreateElementID();
                  passParameterType5.inParamName = uxParameterType.name;
                  passParameterType5.outParamName = uxParameterType.name;
                  PassParameterType passParameterType6 = passParameterType5;
                  passParameterTypeList5.Add(passParameterType6);
                }
              }
              PassListParameterType listParameterType2 = new PassListParameterType();
              listParameterType2.id = CommonUtil.CreateElementID();
              listParameterType2.inListName = listParameterType1.name;
              listParameterType2.outListName = listParameterType1.name;
              listParameterType2.PassParameter = passParameterTypeList5;
              PassListParameterType listParameterType3 = listParameterType2;
              onNavigateType3.PassListParameter.Add(listParameterType3);
            }
          }
        }
        if (changeRecord != null && changeRecord.DynamicBindings.Count > 0)
        {
          foreach (KeyValuePair<string, ExtBindingOption> dynamicBinding in changeRecord.DynamicBindings)
          {
            List<PassParameterType> passParameter = onNavigateType3.PassParameter;
            PassParameterType passParameterType5 = new PassParameterType();
            passParameterType5.id = CommonUtil.CreateElementID();
            passParameterType5.inParamName = dynamicBinding.Key;
            passParameterType5.outParamName = dynamicBinding.Value.ParameterName;
            PassParameterType passParameterType6 = passParameterType5;
            passParameter.Add(passParameterType6);
          }
        }
      }
      else if (changeRecord != null && changeRecord.DynamicBindings.Count > 0 && changeRecord.PipeIDWithoutSlashes == inPlugName)
      {
        foreach (KeyValuePair<string, ExtBindingOption> dynamicBinding in changeRecord.DynamicBindings)
        {
          if (dynamicBinding.Value.IsOutput)
          {
            List<PassParameterType> passParameter = onNavigateType3.PassParameter;
            PassParameterType passParameterType5 = new PassParameterType();
            passParameterType5.id = CommonUtil.CreateElementID();
            passParameterType5.outParamName = dynamicBinding.Key;
            passParameterType5.inParamName = dynamicBinding.Value.ParameterName;
            PassParameterType passParameterType6 = passParameterType5;
            passParameter.Add(passParameterType6);
          }
        }
      }
      return onNavigateType3;
    }

    internal static UXEventHandlerType ConstructFireOutPlugEventHandler(string outPlugName, string embedName)
    {
      if (string.IsNullOrEmpty(outPlugName))
        throw new ArgumentException("outPlugName could not be null or empty.");
      if (string.IsNullOrEmpty(embedName))
        throw new ArgumentException("embedName could not be null or empty.");
      UXEventHandlerType eventHandlerType1 = new UXEventHandlerType();
      eventHandlerType1.id = CommonUtil.CreateElementID();
      UXEventHandlerType eventHandlerType2 = eventHandlerType1;
      List<UXEventHandlerOperationType> handlerOperationTypeList1 = new List<UXEventHandlerOperationType>();
      List<UXEventHandlerOperationType> handlerOperationTypeList2 = handlerOperationTypeList1;
      UXEventHandlerOperationTypeSyncDataContainer syncDataContainer1 = new UXEventHandlerOperationTypeSyncDataContainer();
      syncDataContainer1.id = CommonUtil.CreateElementID();
      syncDataContainer1.name = "syncDataContainer";
      UXEventHandlerOperationTypeSyncDataContainer syncDataContainer2 = syncDataContainer1;
      handlerOperationTypeList2.Add((UXEventHandlerOperationType) syncDataContainer2);
      List<UXEventHandlerOperationType> handlerOperationTypeList3 = handlerOperationTypeList1;
      UXEventHandlerOperationTypeFirePlug operationTypeFirePlug1 = new UXEventHandlerOperationTypeFirePlug();
      operationTypeFirePlug1.id = CommonUtil.CreateElementID();
      operationTypeFirePlug1.name = "mashup_fire_out_plug" + CommonUtil.CreateElementID();
      operationTypeFirePlug1.outPlug = outPlugName;
      UXEventHandlerOperationTypeFirePlug operationTypeFirePlug2 = operationTypeFirePlug1;
      handlerOperationTypeList3.Add((UXEventHandlerOperationType) operationTypeFirePlug2);
      List<UXEventHandlerOperationType> handlerOperationTypeList4 = handlerOperationTypeList1;
      eventHandlerType2.Items = handlerOperationTypeList4;
      eventHandlerType1.name = embedName;
      return eventHandlerType1;
    }

    internal static string AddMashupButtonintoFunctionBar(MashupViewModel mashupInsertionInfo, bool isWritebackCase, OnNavigateType outOnNavigate, OnNavigateType inOnNavigate, EmbeddedComponentType embeddedComponent, FlexBaseAnchorType leadingAnchor, FlexBaseAnchorType inPortAnchor, List<string> extRefs, UXEventHandlerType eventHandler, MashupFlexibilityHandler flexibilityHandler, FlexBaseAnchorType floorPlanAnchor, string targetFile)
    {
      ButtonGroupType buttonGroup = new InsertMenuButtonHandler(mashupInsertionInfo.DisplayText, mashupInsertionInfo.PipeID, eventHandler.name, mashupInsertionInfo.VisibilityBindingExpression).ConstructButtonGroup();
      List<UXParameterType> parameter = embeddedComponent.Parameter;
      UXParameterType uxParameterType1 = new UXParameterType();
      uxParameterType1.id = CommonUtil.CreateElementID();
      uxParameterType1.name = "ElementID";
      uxParameterType1.constant = buttonGroup.id;
      UXParameterType uxParameterType2 = uxParameterType1;
      parameter.Add(uxParameterType2);
      string str;
      if (isWritebackCase)
        str = flexibilityHandler.AddMashupButtonIntoFunctionBarWithWriteback(targetFile, leadingAnchor, new List<FlexBaseAnchorType>()
        {
          inPortAnchor,
          floorPlanAnchor
        }, mashupInsertionInfo.PipeID, embeddedComponent, outOnNavigate, inOnNavigate, eventHandler, buttonGroup, extRefs);
      else
        str = flexibilityHandler.AddMashupButtonIntoFunctionBar(targetFile, leadingAnchor, new List<FlexBaseAnchorType>()
        {
          floorPlanAnchor
        }, mashupInsertionInfo.PipeID, embeddedComponent, outOnNavigate, eventHandler, buttonGroup, extRefs);
      return str;
    }

    internal static string AddMashupButtonIntoListToolbar(MashupViewModel mashupInsertionInfo, bool isWritebackCase, OnNavigateType outOnNavigate, OnNavigateType inOnNavigate, EmbeddedComponentType embeddedComponent, FlexBaseAnchorType leadingAnchor, FlexBaseAnchorType inPortAnchor, List<string> extRefs, UXEventHandlerType eventHandler, FlexBaseAnchorType listAnchor, MashupFlexibilityHandler mashupFlexHandler, string targetFile)
    {
      ButtonGroupType buttonGroup = new InsertMenuButtonHandler(mashupInsertionInfo.DisplayText, mashupInsertionInfo.PipeID, eventHandler.name, mashupInsertionInfo.VisibilityBindingExpression).ConstructButtonGroup();
      List<UXParameterType> parameter = embeddedComponent.Parameter;
      UXParameterType uxParameterType1 = new UXParameterType();
      uxParameterType1.id = CommonUtil.CreateElementID();
      uxParameterType1.name = "ElementID";
      uxParameterType1.constant = buttonGroup.id;
      UXParameterType uxParameterType2 = uxParameterType1;
      parameter.Add(uxParameterType2);
      if (string.IsNullOrEmpty(mashupInsertionInfo.ContainerID))
        throw new InvalidOperationException("list toolbar id is null.");
      string str;
      if (isWritebackCase)
        str = mashupFlexHandler.AddMashupButtonIntoListToolbarWithWriteback(targetFile, leadingAnchor, new List<FlexBaseAnchorType>()
        {
          inPortAnchor,
          listAnchor
        }, mashupInsertionInfo.PipeID, embeddedComponent, outOnNavigate, inOnNavigate, eventHandler, buttonGroup, mashupInsertionInfo.ContainerID, extRefs);
      else
        str = mashupFlexHandler.AddMashupButtonIntoListToolbar(targetFile, leadingAnchor, new List<FlexBaseAnchorType>()
        {
          listAnchor
        }, mashupInsertionInfo.PipeID, embeddedComponent, outOnNavigate, eventHandler, buttonGroup, mashupInsertionInfo.ContainerID, extRefs);
      return str;
    }

    internal static string AddMashupLinkIntoSectionGroup(MashupViewModel mashupInsertionInfo, bool isWritebackCase, OnNavigateType outOnNavigate, OnNavigateType inOnNavigate, EmbeddedComponentType embeddedComponent, FlexBaseAnchorType leadingAnchor, FlexBaseAnchorType inPortAnchor, List<string> extRefs, UXEventHandlerType eventHandler, FlexBaseAnchorType sectionGroupAnchor, MashupFlexibilityHandler mashupFlexHandler, string targetFile)
    {
      if (sectionGroupAnchor == null)
        throw new InvalidOperationException("Stable anchor for sectiongroup is null.");
      SectionGroupItemType sectionGroupItem = new InsertSectionGroupLinkHandler(mashupInsertionInfo.DisplayText, mashupInsertionInfo.PipeID, eventHandler.name, mashupInsertionInfo.VisibilityBindingExpression).ConstructSectionGroupLink();
      List<UXParameterType> parameter = embeddedComponent.Parameter;
      UXParameterType uxParameterType1 = new UXParameterType();
      uxParameterType1.id = CommonUtil.CreateElementID();
      uxParameterType1.name = "ElementID";
      uxParameterType1.constant = sectionGroupItem.id;
      UXParameterType uxParameterType2 = uxParameterType1;
      parameter.Add(uxParameterType2);
      string str;
      if (isWritebackCase)
        str = mashupFlexHandler.AddMashupLinkIntoSectionGroupWithWriteback(targetFile, leadingAnchor, new List<FlexBaseAnchorType>()
        {
          inPortAnchor,
          sectionGroupAnchor
        }, mashupInsertionInfo.PipeID, embeddedComponent, outOnNavigate, inOnNavigate, eventHandler, sectionGroupItem, mashupInsertionInfo.ContainerID, extRefs);
      else
        str = mashupFlexHandler.AddMashupLinkIntoSectionGroup(targetFile, leadingAnchor, new List<FlexBaseAnchorType>()
        {
          sectionGroupAnchor
        }, mashupInsertionInfo.PipeID, embeddedComponent, outOnNavigate, eventHandler, sectionGroupItem, mashupInsertionInfo.ContainerID, extRefs);
      return str;
    }

    internal static string AddMashupNewPaneContainer(MashupViewModel mashupInsertionInfo, bool isWithoutOutPort, OnNavigateType outOnNavigate, OnNavigateType inOnNavigate, EmbeddedComponentType embeddedComponent, List<string> extRefs, MashupFlexibilityHandler mashupFlexHandler, string targetFile, FlexBaseAnchorType floorPlanAnchor)
    {
      string str = string.Empty;
      PaneContainerType paneContainer = new InsertPaneContainerHandler(embeddedComponent.embedName, mashupInsertionInfo.DisplayText, mashupInsertionInfo.VisibilityBindingExpression).ConstructPaneContainer();
      List<UXParameterType> parameter = embeddedComponent.Parameter;
      UXParameterType uxParameterType1 = new UXParameterType();
      uxParameterType1.id = CommonUtil.CreateElementID();
      uxParameterType1.name = "ElementID";
      uxParameterType1.constant = paneContainer.id;
      UXParameterType uxParameterType2 = uxParameterType1;
      parameter.Add(uxParameterType2);
      if (isWithoutOutPort)
        str = mashupFlexHandler.AddMashupPaneWithoutPort(targetFile, floorPlanAnchor, mashupInsertionInfo.PipeID, embeddedComponent, paneContainer, mashupInsertionInfo.ContainerID, mashupInsertionInfo.FullColumnSpan, extRefs);
      else if (mashupInsertionInfo.IsDynamic)
        str = mashupFlexHandler.AddNewDynamicMashupPane(targetFile, floorPlanAnchor, mashupInsertionInfo.PipeID, embeddedComponent, outOnNavigate, inOnNavigate, paneContainer, mashupInsertionInfo.ContainerID, mashupInsertionInfo.FullColumnSpan, extRefs);
      return str;
    }

    internal static string AddMashupNearPaneContainer(MashupViewModel mashupInsertionInfo, bool isWritebackCase, OnNavigateType outOnNavigate, OnNavigateType inOnNavigate, EmbeddedComponentType embeddedComponent, FlexBaseAnchorType leadingAnchor, FlexBaseAnchorType inPortAnchor, List<string> extRefs, FlexBaseAnchorType paneContainerAnchor, MashupFlexibilityHandler mashupFlexHandler, string targetFile)
    {
      PaneContainerType paneContainer = new InsertPaneContainerHandler(embeddedComponent.embedName, mashupInsertionInfo.DisplayText, mashupInsertionInfo.VisibilityBindingExpression).ConstructPaneContainer();
      List<UXParameterType> parameter = embeddedComponent.Parameter;
      UXParameterType uxParameterType1 = new UXParameterType();
      uxParameterType1.id = CommonUtil.CreateElementID();
      uxParameterType1.name = "ElementID";
      uxParameterType1.constant = paneContainer.id;
      UXParameterType uxParameterType2 = uxParameterType1;
      parameter.Add(uxParameterType2);
      string str;
      if (isWritebackCase)
      {
        List<FlexBaseAnchorType> referencedAnchorList = new List<FlexBaseAnchorType>()
        {
          inPortAnchor,
          paneContainerAnchor
        };
        str = mashupFlexHandler.AddMashupPaneWithPortWithWriteback(targetFile, leadingAnchor, referencedAnchorList, mashupInsertionInfo.PipeID, embeddedComponent, outOnNavigate, inOnNavigate, paneContainer, mashupInsertionInfo.ContainerID, mashupInsertionInfo.RefPaneContainerID, mashupInsertionInfo.FullColumnSpan, extRefs);
      }
      else
      {
        List<FlexBaseAnchorType> referencedAnchorList = new List<FlexBaseAnchorType>()
        {
          paneContainerAnchor
        };
        str = mashupFlexHandler.AddMashupPaneWithPort(targetFile, leadingAnchor, referencedAnchorList, mashupInsertionInfo.PipeID, embeddedComponent, outOnNavigate, paneContainer, mashupInsertionInfo.ContainerID, mashupInsertionInfo.RefPaneContainerID, mashupInsertionInfo.FullColumnSpan, extRefs);
      }
      return str;
    }

    internal static string AddMashupViewSwitch(string targetFile, MashupFlexibilityHandler mashupFlexHandler, FlexBaseAnchorType leadingAnchor, FlexBaseAnchorType viewSwitchNavigationAnchor, EmbeddedComponentType embeddedComponent, MashupViewModel mashupInsertionInfo, OnNavigateType outOnNavigate, OnNavigateType inOnNavigate)
    {
      PaneContainerType paneContainerType = new InsertPaneContainerHandler(embeddedComponent.embedName, mashupInsertionInfo.DisplayText, (string) null).ConstructPaneContainer();
      paneContainerType.column = 0;
      paneContainerType.row = 0;
      if (mashupInsertionInfo.FullColumnSpan)
      {
        paneContainerType.colSpan = 2;
        paneContainerType.colSpanSpecified = true;
      }
      List<UXParameterType> parameter = embeddedComponent.Parameter;
      UXParameterType uxParameterType1 = new UXParameterType();
      uxParameterType1.id = CommonUtil.CreateElementID();
      uxParameterType1.name = "ElementID";
      uxParameterType1.constant = paneContainerType.id;
      UXParameterType uxParameterType2 = uxParameterType1;
      parameter.Add(uxParameterType2);
      string elementId = CommonUtil.CreateElementID();
      NavigationItemType navigationItemType = new NavigationItemType();
      navigationItemType.id = CommonUtil.CreateElementID();
      navigationItemType.Title = new DependentPropertyType()
      {
        textPoolId = elementId
      };
      navigationItemType.navigationItemId = embeddedComponent.embedName;
      NavigationItemType navigationItem = navigationItemType;
      ViewType viewType = new ViewType();
      viewType.id = CommonUtil.CreateElementID();
      viewType.PaneContainer = new List<PaneContainerType>()
      {
        paneContainerType
      };
      viewType.navigationItemId = embeddedComponent.embedName;
      viewType.columns = 2;
      viewType.columnsSpecified = true;
      viewType.rows = 1;
      viewType.rowsSpecified = true;
      ViewType view = viewType;
      TextPoolEntryType textPoolEntryType = new TextPoolEntryType();
      textPoolEntryType.id = CommonUtil.CreateElementID();
      textPoolEntryType.textUuid = elementId;
      textPoolEntryType.text = mashupInsertionInfo.DisplayText;
      textPoolEntryType.textCategory = TextCategoryType.XTBS;
      TextPoolEntryType textPoolEntry = textPoolEntryType;
      return mashupFlexHandler.AddMashupViewSwitch(targetFile, leadingAnchor, viewSwitchNavigationAnchor, embeddedComponent, outOnNavigate, inOnNavigate, navigationItem, view, mashupInsertionInfo.PipeID, textPoolEntry);
    }
  }
}
