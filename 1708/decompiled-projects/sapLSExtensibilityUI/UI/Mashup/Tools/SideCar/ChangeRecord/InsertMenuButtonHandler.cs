﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord.InsertMenuButtonHandler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base.Exceptions;
using SAP.BYD.LS.UI.Core.Model;
using System.Collections.Generic;

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord
{
  public class InsertMenuButtonHandler
  {
    private string label;
    private string mashupPipeIDArgument;
    private string eventHandlerName;
    private string visibilityBindingExp;

    public InsertMenuButtonHandler(string label, string mashupPipeIDArgument, string eventHandlerName, string visibilityBindingExp)
    {
      this.label = label;
      this.mashupPipeIDArgument = mashupPipeIDArgument;
      this.eventHandlerName = eventHandlerName;
      this.visibilityBindingExp = visibilityBindingExp;
    }

    public ButtonGroupType ConstructButtonGroup()
    {
      if (string.IsNullOrEmpty(this.label))
        throw new InvalidArgumentException("label could not be null or empty.");
      if (string.IsNullOrEmpty(this.mashupPipeIDArgument))
        throw new InvalidArgumentException("mashupPipeIDArgument could not be null or empty.");
      if (string.IsNullOrEmpty(this.eventHandlerName))
        throw new InvalidArgumentException("eventHandlerName could not be null or empty.");
      NavigationItemType navigationItemType1 = new NavigationItemType();
      navigationItemType1.id = CommonUtil.CreateElementID();
      NavigationItemType navigationItemType2 = navigationItemType1;
      DependentPropertyType dependentPropertyType1 = new DependentPropertyType();
      dependentPropertyType1.id = CommonUtil.CreateElementID();
      dependentPropertyType1.fallbackValueType = DataFieldTypes.@string;
      dependentPropertyType1.fallbackValue = this.label;
      DependentPropertyType dependentPropertyType2 = dependentPropertyType1;
      navigationItemType2.Title = dependentPropertyType2;
      navigationItemType1.onClick = this.eventHandlerName;
      NavigationItemType navigationItemType3 = navigationItemType1;
      EventArgumentsType eventArgumentsType1 = new EventArgumentsType();
      eventArgumentsType1.id = CommonUtil.CreateElementID();
      EventArgumentsType eventArgumentsType2 = eventArgumentsType1;
      List<EventArgumentType> eventArgumentTypeList1 = new List<EventArgumentType>();
      List<EventArgumentType> eventArgumentTypeList2 = eventArgumentTypeList1;
      EventArgumentType eventArgumentType1 = new EventArgumentType();
      eventArgumentType1.id = CommonUtil.CreateElementID();
      eventArgumentType1.argumentName = "$MashupPipeID";
      eventArgumentType1.constant = this.mashupPipeIDArgument;
      EventArgumentType eventArgumentType2 = eventArgumentType1;
      eventArgumentTypeList2.Add(eventArgumentType2);
      List<EventArgumentType> eventArgumentTypeList3 = eventArgumentTypeList1;
      eventArgumentsType2.EventArgument = eventArgumentTypeList3;
      EventArgumentsType eventArgumentsType3 = eventArgumentsType1;
      navigationItemType3.EventArguments = eventArgumentsType3;
      NavigationItemType navigationItemType4 = navigationItemType1;
      if (!string.IsNullOrEmpty(this.visibilityBindingExp))
      {
        NavigationItemType navigationItemType5 = navigationItemType4;
        DependentPropertyType dependentPropertyType3 = new DependentPropertyType();
        dependentPropertyType3.id = CommonUtil.CreateElementID();
        dependentPropertyType3.bindingExpression = this.visibilityBindingExp;
        DependentPropertyType dependentPropertyType4 = dependentPropertyType3;
        navigationItemType5.Visible = dependentPropertyType4;
      }
      List<NavigationItemType> navigationItemTypeList = new List<NavigationItemType>()
      {
        navigationItemType4
      };
      NavigationType navigationType1 = new NavigationType();
      navigationType1.id = CommonUtil.CreateElementID();
      navigationType1.NavigationItem = navigationItemTypeList;
      NavigationType navigationType2 = navigationType1;
      ButtonType buttonType1 = new ButtonType();
      buttonType1.id = CommonUtil.CreateElementID();
      buttonType1.name = "btnMashupWebServices";
      buttonType1.style = ButtonStyles.Standard;
      ButtonType buttonType2 = buttonType1;
      DependentPropertyType dependentPropertyType5 = new DependentPropertyType();
      dependentPropertyType5.id = CommonUtil.CreateElementID();
      dependentPropertyType5.fallbackValueType = DataFieldTypes.@string;
      dependentPropertyType5.fallbackValue = "Web Services";
      DependentPropertyType dependentPropertyType6 = dependentPropertyType5;
      buttonType2.Text = dependentPropertyType6;
      buttonType1.Menu = navigationType2;
      List<ButtonType> buttonTypeList = new List<ButtonType>()
      {
        buttonType1
      };
      ButtonGroupType buttonGroupType = new ButtonGroupType();
      buttonGroupType.id = CommonUtil.CreateElementID();
      buttonGroupType.Button = buttonTypeList;
      buttonGroupType.name = "btnGroupMashupWebServices";
      return buttonGroupType;
    }
  }
}
