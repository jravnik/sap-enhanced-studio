﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord.InsertSectionGroupLinkHandler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base.Exceptions;
using SAP.BYD.LS.UI.Core.Model;
using System.Collections.Generic;

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord
{
  public class InsertSectionGroupLinkHandler
  {
    private string linkDisplayName;
    private string mashupPipeIDArgument;
    private string eventHandlerName;
    private string visibilityBindingExp;

    public InsertSectionGroupLinkHandler(string linkDisplayName, string mashupPipeIDArgument, string eventHandlerName, string visibilityBindingExp)
    {
      this.linkDisplayName = linkDisplayName;
      this.mashupPipeIDArgument = mashupPipeIDArgument;
      this.eventHandlerName = eventHandlerName;
      this.visibilityBindingExp = visibilityBindingExp;
    }

    public SectionGroupItemType ConstructSectionGroupLink()
    {
      if (string.IsNullOrEmpty(this.linkDisplayName))
        throw new InvalidArgumentException("linkDisplayName could not be null or empty.");
      if (string.IsNullOrEmpty(this.mashupPipeIDArgument))
        throw new InvalidArgumentException("mashupPipeIDArgument could not be null or empty.");
      if (string.IsNullOrEmpty(this.eventHandlerName))
        throw new InvalidArgumentException("eventHandlerName could not be null or empty.");
      LinkType linkType1 = new LinkType();
      linkType1.id = CommonUtil.CreateElementID();
      linkType1.name = "MashupNewLink" + CommonUtil.CreateElementID();
      linkType1.onClick = this.eventHandlerName;
      LinkType linkType2 = linkType1;
      EventArgumentsType eventArgumentsType1 = new EventArgumentsType();
      eventArgumentsType1.id = CommonUtil.CreateElementID();
      EventArgumentsType eventArgumentsType2 = eventArgumentsType1;
      List<EventArgumentType> eventArgumentTypeList1 = new List<EventArgumentType>();
      List<EventArgumentType> eventArgumentTypeList2 = eventArgumentTypeList1;
      EventArgumentType eventArgumentType1 = new EventArgumentType();
      eventArgumentType1.id = CommonUtil.CreateElementID();
      eventArgumentType1.argumentName = "$MashupPipeID";
      eventArgumentType1.constant = this.mashupPipeIDArgument;
      EventArgumentType eventArgumentType2 = eventArgumentType1;
      eventArgumentTypeList2.Add(eventArgumentType2);
      List<EventArgumentType> eventArgumentTypeList3 = eventArgumentTypeList1;
      eventArgumentsType2.EventArgument = eventArgumentTypeList3;
      EventArgumentsType eventArgumentsType3 = eventArgumentsType1;
      linkType2.EventArguments = eventArgumentsType3;
      LinkType linkType3 = linkType1;
      DependentPropertyType dependentPropertyType1 = new DependentPropertyType();
      dependentPropertyType1.id = CommonUtil.CreateElementID();
      dependentPropertyType1.fallbackValueType = DataFieldTypes.@string;
      dependentPropertyType1.fallbackValue = this.linkDisplayName;
      DependentPropertyType dependentPropertyType2 = dependentPropertyType1;
      linkType3.Text = dependentPropertyType2;
      LinkType linkType4 = linkType1;
      if (!string.IsNullOrEmpty(this.visibilityBindingExp))
      {
        LinkType linkType5 = linkType4;
        DependentPropertyType dependentPropertyType3 = new DependentPropertyType();
        dependentPropertyType3.id = CommonUtil.CreateElementID();
        dependentPropertyType3.bindingExpression = this.visibilityBindingExp;
        DependentPropertyType dependentPropertyType4 = dependentPropertyType3;
        linkType5.Visible = dependentPropertyType4;
      }
      SectionGroupItemType sectionGroupItemType1 = new SectionGroupItemType();
      sectionGroupItemType1.id = CommonUtil.CreateElementID();
      sectionGroupItemType1.noLabel = true;
      sectionGroupItemType1.Item = (AbstractControlType) linkType4;
      SectionGroupItemType sectionGroupItemType2 = sectionGroupItemType1;
      DependentPropertyType dependentPropertyType5 = new DependentPropertyType();
      dependentPropertyType5.id = CommonUtil.CreateElementID();
      dependentPropertyType5.fallbackValueType = DataFieldTypes.@string;
      dependentPropertyType5.fallbackValue = string.Empty;
      DependentPropertyType dependentPropertyType6 = dependentPropertyType5;
      sectionGroupItemType2.Label = dependentPropertyType6;
      return sectionGroupItemType1;
    }
  }
}
