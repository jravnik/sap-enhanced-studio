﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar
{
  public class MashupPipeInfo
  {
    public string PipeID { get; set; }

    public string FilePath { get; set; }

    public string MashupType { get; set; }

    public string SemanticCategory { get; set; }

    public string MashupCategory { get; set; }

    public string InPort { get; set; }

    public string OutPort { get; set; }

    public string DomainUrl { get; set; }

    public string Description { get; set; }

    public string DisplayName { get; set; }

    public string ViewStyle { get; set; }

    public bool IsActive { get; set; }

    public bool IsModifiable { get; set; }

    public bool IsRemovable { get; set; }

    public bool IsManageable { get; set; }

    public string CreatedOn { get; set; }

    public string CreatedBy { get; set; }

    public string LastChangedOn { get; set; }

    public string LastChangedBy { get; set; }

    public bool HasKeys { get; set; }

    public bool HasKeysEntered { get; set; }

    public string MandParams { get; set; }

    public string DynParams { get; set; }

    public string DynOutputs { get; set; }

    public bool FullColumnSpan { get; set; }
  }
}
