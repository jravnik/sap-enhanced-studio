﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.Service.LoadMashupPipesWorker
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base;
using SAP.BYD.LS.UI.Core.Connector.Services.Mashup;
using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Mashup.Common;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar;
using SAP.BYD.LS.UIDesigner.Connector;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.BYD.LS.UI.Mashup.Tools.Service
{
  public class LoadMashupPipesWorker
  {
    private static LoadMashupPipesWorker instance = new LoadMashupPipesWorker();

    public static LoadMashupPipesWorker Instance
    {
      get
      {
        return LoadMashupPipesWorker.instance;
      }
    }

    public List<SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo> LoadWebWidgetMashups()
    {
      return this.Load(new List<string>() { "Web_Widget" }, new List<MashupTypeCategories>()
      {
        MashupTypeCategories.Html_Mashup,
        MashupTypeCategories.Data_Mashup,
        MashupTypeCategories.Custom_Mashup
      }, 1 != 0);
    }

    public List<SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo> Load(List<string> mashupCategories, List<MashupTypeCategories> mashupTypes, bool onlyActive)
    {
      bool needBackendCall;
      MashupPipeQueryFunction function;
      LoadMashupPipesWorker.LoadMashupHelper(mashupCategories, mashupTypes, onlyActive, (string) null, out needBackendCall, out function, true);
      if (needBackendCall)
        MashupProxy.Instance.CallFunctionModule((IRemoteFunction) function);
      else
        function.Exporting = new MashupPipeQueryFunction.ExportingType()
        {
          ET_PIPE = new MashupPipeQueryFunction.MashupPipeInfo[0]
        };
      return this.MashupsLoaded(function);
    }

    private List<SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo> MashupsLoaded(MashupPipeQueryFunction function)
    {
      List<SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo> mashupPipeInfoList = new List<SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo>();
      List<string> messageList;
      if (CommonUtil.IsFunctionCallSuccess(function.Exporting.EV_RETURN_CODE, function.Exporting.ET_RETURN_MSG, out messageList) && function.Exporting.ET_PIPE != null)
        mashupPipeInfoList = ((IEnumerable<MashupPipeQueryFunction.MashupPipeInfo>) function.Exporting.ET_PIPE).Select<MashupPipeQueryFunction.MashupPipeInfo, SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo>((Func<MashupPipeQueryFunction.MashupPipeInfo, SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo>) (p =>
        {
          string mashupCategory = p.MashupCategory;
          string semanticCategory = p.SemanticCategory;
          string str = string.IsNullOrEmpty(semanticCategory) ? SemanticCategory.Instance.FromPortBindingAsString(mashupCategory) : semanticCategory;
          return new SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo()
          {
            PipeID = p.PipeID,
            FilePath = p.FilePath,
            InPort = p.InPort,
            OutPort = p.OutPort,
            MashupType = p.MashupType,
            MashupCategory = p.MashupCategory,
            SemanticCategory = str,
            Description = p.Description,
            DisplayName = p.DisplayName,
            ViewStyle = p.ViewStyle,
            DomainUrl = p.DomainUrl,
            CreatedOn = p.CreatedOn,
            CreatedBy = p.CreatedBy,
            LastChangedOn = p.LastChangedOn,
            LastChangedBy = p.LastChangedBy,
            IsActive = p.IsActive,
            IsModifiable = p.IsModifiable,
            IsRemovable = p.IsRemovable,
            IsManageable = p.IsManageable,
            HasKeys = p.HasKeys,
            HasKeysEntered = p.HasKeysEntered,
            MandParams = p.MandParams ?? string.Empty,
            DynParams = p.DynParams ?? string.Empty,
            DynOutputs = p.DynOutputs ?? string.Empty
          };
        })).ToList<SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo>();
      return mashupPipeInfoList;
    }

        private static void LoadMashupHelper(List<string> mashupCategories, List<MashupTypeCategories> mashupTypes, bool onlyActive, string searchText, out bool needBackendCall, out MashupPipeQueryFunction function, bool fromSidecar = false)
        {
            needBackendCall = false;
            function = (fromSidecar ? new MashupPipeQuerySidecar
            {
                Importing = new MashupPipeQueryFunction.ImportingType()
            } : new MashupPipeQueryFunction
            {
                Importing = new MashupPipeQueryFunction.ImportingType()
            });
            if (mashupCategories != null && mashupCategories.Count > 0)
            {
                function.Importing.IT_MASHUP_CATEGORY_RANGE = (from mashupCategory in mashupCategories
                                                               select new MashupPipeQueryFunction.FLX_MASH_S_MASHUP_CATEGORY_RANGE
                                                               {
                                                                   SIGN = "I",
                                                                   OPTION = "EQ",
                                                                   LOW = mashupCategory,
                                                                   HIGH = string.Empty
                                                               }).ToArray<MashupPipeQueryFunction.FLX_MASH_S_MASHUP_CATEGORY_RANGE>();
                needBackendCall = true;
            }
            if (mashupTypes != null && mashupTypes.Count > 0)
            {
                function.Importing.IT_MASHUP_TYPE_RANGE = (from mashupType in mashupTypes
                                                           select new MashupPipeQueryFunction.FLX_MASH_S_MASHUP_TYPE_RANGE
                                                           {
                                                               SIGN = "I",
                                                               OPTION = "EQ",
                                                               LOW = Enum.GetName(typeof(MashupTypeCategories), mashupType),
                                                               HIGH = string.Empty
                                                           }).ToArray<MashupPipeQueryFunction.FLX_MASH_S_MASHUP_TYPE_RANGE>();
                needBackendCall = true;
            }
            if (onlyActive)
            {
                function.Importing.NEED_ACTIVE = 'X';
            }
            if (searchText != null)
            {
                function.Importing.IV_SEARCH_TEXT = searchText;
            }
        }
    }
}
