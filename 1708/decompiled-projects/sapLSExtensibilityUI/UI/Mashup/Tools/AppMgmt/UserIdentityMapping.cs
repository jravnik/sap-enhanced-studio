﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.AppMgmt.UserIdentityMapping
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using System.Collections.Generic;

namespace SAP.BYD.LS.UI.Mashup.Tools.AppMgmt
{
  public class UserIdentityMapping
  {
    private readonly Dictionary<string, string> _mapping = new Dictionary<string, string>();
    private static readonly UserIdentityMapping _instance = new UserIdentityMapping();

    public static UserIdentityMapping Instance
    {
      get
      {
        return UserIdentityMapping._instance;
      }
    }

    private UserIdentityMapping()
    {
    }

    public void Add(string userId, string formattedName)
    {
      lock (this._mapping)
      {
        if (this._mapping.ContainsKey(userId))
          return;
        this._mapping.Add(userId, formattedName);
      }
    }

    public bool TryGetFormattedName(string userId, out string formattedName)
    {
      formattedName = (string) null;
      lock (this._mapping)
        this._mapping.TryGetValue(userId, out formattedName);
      return formattedName != null;
    }
  }
}
