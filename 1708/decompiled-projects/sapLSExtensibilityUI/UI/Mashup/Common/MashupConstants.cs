﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Common.MashupConstants
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base.Exceptions;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SAP.BYD.LS.UI.Mashup.Common
{
  public static class MashupConstants
  {
    public static XNamespace ViewNameSpace = (XNamespace) "http://www.sap.com/a1s/cd/oberon/uxview-1.0";
    public static Dictionary<string, string> MashupDic = new Dictionary<string, string>()
    {
      {
        "All_Mashup",
        "All Mashups"
      },
      {
        "Custom_Mashup",
        "Custom Mashups"
      },
      {
        "Data_Mashup",
        "Data Mashups"
      },
      {
        "Html_Mashup",
        "HTML Mashups"
      },
      {
        "Url_Mashup",
        "URL Mashups"
      },
      {
        "Link.Near.Field",
        "Link near {0} Field"
      },
      {
        "Embed.Near.Field",
        "Embedded near {0} Field"
      },
      {
        "Untitled",
        "Untitled"
      },
      {
        "Menu_In_Function_Bar",
        "Web Services Menu"
      }
    };
    public const char Slash = '/';
    public const string MASHUP_PIPE = "Pipe";
    public const string MASHUP_SERVICE = "Service";
    public const string EXTENSION_FIELD_PORT_PREFIX = "ExtensionFields";
    public const string EXTENSION_FIELD_OUTPORT = "ExtensionFieldsOutPort";
    public const string EXTENSION_FIELD_INPORT = "ExtensionFieldsInPort";
    public const string MASHUP_PORT_TYPE_PACKAGE = "/SAP_BYD_TF/Mashups/globalmashupporttypes.PTP.uicomponent";
    public const string MASHUP_PIPE_ID_STR = "MashupPipeID";
    public const string MASHUP_PIPE_ID = "$MashupPipeID";
    public const string MASHUP_COMPONENT_PATH_REGEX_GROUP = "MashupComponentPath";
    public const string MASHUP_COMPONENT_PATH = "$MashupComponentPath";
    public const string MASHUP_EMBEDDED_COMPONENT_NAME_GROUP = "MashupEmbeddedComponentName";
    public const string MASHUP_EMBEDDED_COMPONENT_NAME_REGEX = "(?<MashupEmbeddedComponentName>\\$Mashup.+)/\\$MashupPipeID=(?<MashupPipeID>.+)";
    public const string MASHUP_EMBEDDED_COMPONENT_NAME_PATTERN = "$Mashup{0}/$MashupPipeID={1}";
    public const string MASHUP_EMBEDDED_PARAMETER_DISPLAY_TEXT = "MashupDisplayText";
    public const string MASHUP_EMBEDDED_PARAMETER_APPEARANCE_KEY = "MashupAppearanceKey";
    public const string MASHUP_EMBEDDED_PARAMETER_FULLCOLUMNSPAN = "FullColumnSpan";
    public const string MASHUP_EMBEDDED_PARAMETER_VISIBILITYBINDING = "VisibilityBinding";
    public const string MASHUP_EMBEDDED_PARAMETER_OUTPORT_NAME = "SourceOutPort";
    public const string MASHUP_EMBEDDED_PARAMETER_INPORT_NAME = "TargetInPort";
    public const string MASHUP_PIPE_ID_ARGUMENT_REGEX = "(?<MashupComponentPath>.+)/\\$MashupPipeID=(?<MashupPipeID>.+)";
    public const string MASHUP_EMBEDDED_NAME_STR = "MashupEmbeddedName";
    public const string MASHUP_EMBEDDED_NAME = "$MashupEmbeddedName";
    public const string MASHUP_EVENTHANDLER_NAME_PATTERN = "$MashupEventHandler${0}${1}${2}";
    public const string MASHUP_EVENTHANDLER_NAME_REGEX = "\\$MashupEventHandler\\$(?<MashupEmbeddedName>.+)\\$(.+)\\$(\\d{1})";
    public const string WEB_WIDGET = "Web_Widget";
    public const string CHECK_API_KEYS = "CheckApiKeys";
    public const string LAYER_CUST = "LAYER_CUST";
    public const string NO_ENCODING_PREFIX = "<noencoding>";
    public const string NO_ENCODING_SUBFIX = "</noencoding>";
    public const string NO_ENCODING_TAG_PATTERN = "<noencoding>{0}</noencoding>";
    public const string NO_ENCODING_PART = "noencodingpart";
    public const string NO_ENCODING_TAG_REGEX = "<noencoding>(?<noencodingpart>(((?!<noencoding>).)*))</noencoding>";
    private const string MASHUP_COMPONENT_FOLDER = "/SAP_BYD_TF/Mashups/";
    public const string ALL_MASHUP = "All_Mashup";
    public const string X = "X";
    public const string INTERNAL_PORTTYPE_PACKAGE = "/SAP_BYD_TF/Mashups/internal_porttype_package.uicomponent";
    public const string ACTIVE_KEY = "Active";
    public const string INACTIVE_KEY = "Inactive";
    public const string URL_MODE = "Basic";
    public const string HTML_CODE_MODE = "Advanced";
    public const string UI_MASHUP_POST_FIX = ".uimashup";
    public const string MASHUP_COMPONENT_FORMAT = "/SAP_BYD_TF/Mashups/Pipes/{0}.MC.uimashup";
    public const string MASHUP_SERVICE_FORMAT = "/SAP_BYD_TF/Mashups/Services/{0}.WS.uimashup";
    private const string MASHUP_PARTNER_COMPONENT_FOLDER = "/{0}/SRC/Mashups/";
    public const string MASHUP_PARTNER_PIPE_FORMAT = "/{0}/SRC/Mashups/Pipes/{1}.MC.uimashup";
    public const string MASHUP_PARTNER_SERVICE_FORMAT = "/{0}/SRC/Mashups/Services/{1}.WS.uimashup";
    public const string MashupPortTypePackage = "/SAP_BYD_TF/Mashups/globalmashupporttypes.PTP.uicomponent";
    public const string Untitled = "Untitled";
    public const string WebWidget = "Web_Widget";
    public const string OUTPORTS = "$OutPorts";
    public const string OUTPORT_NAME = "$OutPortName";
    public const string OUTPORT_REFERENCE = "$OutPortReference";
    public const string NAVIGATION_PARAMS = "$NavigationParameters";
    public const string INPORTS = "$InPorts";
    public const string INPORT_NAME = "$InPortName";
    public const string INPORT_REFERENCE = "$InPortReference";
    public const string MASHUP_TOOLBAR_BTN_GROUP_NAME = "btnGroupMashupWebServices";
    public const string MASHUP_TOOLBAR_BTN_NAME = "btnMashupWebServices";
    public const string MASHUP_RESULT = "$MashupResult";
    public const string MASHUP_VIEW_STYLE = "$MashupViewStyle";
    public const string MASHUP_RESULT_FIELD_PATH = "/Root/$MashupResult";
    public const string MASHUP_VIEW_STYLE_FIELD_PATH = "/Root/$MashupViewStyle";
    public const string TOOLKIT_DLL_PATH = "/SAP_BYD_TF/Analytics/AnalysisPattern/System.Windows.Controls.Toolkit.dll";
    public const string DATA_VISUAL_DLL_PATH = "/SAP_BYD_TF/Analytics/AnalysisPattern/System.Windows.Controls.DataVisualization.Toolkit.dll";
    public const string MAP_CONTROL_DLL_PATH = "/SAP_BYD_TF/Mashups/Bin/Microsoft.Maps.MapControl.dll";
    public const string MAP_CONTROL_COMMON_DLL_PATH = "/SAP_BYD_TF/Mashups/Bin/Microsoft.Maps.MapControl.Common.dll";
    public const string WRITEBACK_INPORT = "Render_Result_In";
    public const string GENERIC_URL_REGEXP = "^((?<domain>\\w+):(//)?([\\w-{}]+[\\.]*)+[\\w-{}]*(:[0-9]+)?)(/[\\w\\d-./#?!%&=;,]*)?";
    public const string GENERIC_URL_NO_PARAM_REGEXP = "^((?<domain>\\w+):(//)?([\\w-{}]+[\\.]*)+[\\w-{}]*(:[0-9]+)?)(/[\\w-=!#%&;,{}\\./\\u0040]*)?";
    public const string URL_REGEXP = "^(?<domain>http(s)?://([\\w-{}]+[\\.]*)+[\\w-{}]*(:[0-9]+)?)(/[\\w\\d-./#?!%&=;,]*)?";
    public const string URL_WITHOUT_PARAMS_REGEXP = "^(http(s)?://([\\w-{}]+[\\.]*)+[\\w-{}]*(:[0-9]+)?)(/[\\w-=!#%&;,{}\\./\\u0040]*)?";
    public const string GADGET_SCRIPT_TEMPLATE = "<script src=\"{0}\"></script>";
    public const string GADGET_SCRIPT_REGEX = "^<script src=\"(?<gadgetSource>.+)\"></script>";
    public const string GADGET_XMLURL_REGEX = "^(?<first>.+url=)(?<url>.+)(?<last>&amp;.+)";
    public const string GADGET_TITLE_REGEX = "^(?<first>.+&amp;title=)(?<title>.+)(?<last>&amp;.+)";
    public const string GADGET_WIDTH_REGEX = "^(?<first>.+&amp;w=)(?<width>\\d*)(?<last>&amp;.+)";
    public const string GADGET_HEIGHT_REGEX = "^(?<first>.+&amp;h=)(?<height>\\d*)(?<last>&amp;.+)";
    public const string RSS_VIEW = "RssView";
    public const string GRID_VIEW = "GridView";
    public const string TEXT_VIEW = "TextView";
    public const string MAP_VIEW = "MapView";
    public const string FORM_VIEW = "FormView";
    public const string CHART_VIEW = "ChartView";
    public const string MEDIA_VIEW = "MediaView";
    public const string TWITTER_VIEW = "TwitterView";
    public const string RSS_VIEW_TYPE = "MashupRssViewType";
    public const string TWITTER_VIEW_TYPE = "MashupRealTimeRssViewType";
    public const string MAP_VIEW_TYPE = "MashupMapViewType";
    public const string CHART_VIEW_TYPE = "MashupChartViewType";
    public const string FORM_VIEW_TYPE = "MashupFormViewType";
    public const string TEXT_VIEW_TYPE = "MashupTextViewType";
    public const string GRID_VIEW_TYPE = "MashupTableViewType";
    public const string MEDIA_VIEW_TYPE = "MashupMediaRssViewType";
    public const string FUNC_RETURN_CODE_MESSAGE = "Return Code is {0}";
    public const string GOOGLE_MAP_PIPE_ID = "SM00047";
    public const string RSS_SERVICE_TYPE = "Feed_Service";
    public const string ERROR_CODE_API_KEYS = "4";
    public const string ERROR_CODE_DEACTIVATED = "5";
    public const string ERROR_CODE_BING_MAPS_API_KEYS = "7";
    public const string ERROR_CODE_ODATA_QUERY_NOT_SUPPORTED = "8";
    public const string EMPTY_REGEX_STRING_FOR_PARAMETER_NAME = "\\s+";
    public const string METADATA_WSDL_URI = "WSDL_URI";
    public const string OAUTH_CONSUMER_KEY = "oauth_consumer_key";
    public const string OAUTH_CONSUMER_SECRET = "oauth_consumer_secret";
    public const string OAUTH_REQUEST_TOKEN_URL = "oauth_request_token_url";
    public const string OAUTH_AUTHORIZE_URL = "oauth_authorize_url";
    public const string OAUTH_ACCESS_TOKEN_URL = "oauth_access_token_url";
    public const string OAUTH_ACCESS_TOKEN = "oauth_access_token";
    public const string OAUTH_ACCESS_TOKEN_SECRET = "oauth_access_token_secret";
    public const string OAUTH_VERIFIER = "oauth_verifier";
    public const string AUTHORIZATION_URL = "AuthorizationUrl";
    public const string OAUTH = "OAuth";
    public const string BASIC_AUTH_USERNAME = "basic_username";
    public const string BASIC_AUTH_PASSWORD = "basic_password";
    public const string DOT_SVC = ".svc";

    public static string GetMashupPath(string mashupId)
    {
      if (string.IsNullOrEmpty(mashupId))
        throw new ArgumentNullException("mashupId");
      if (!mashupId.Contains("/"))
        return string.Format("/SAP_BYD_TF/Mashups/Pipes/{0}.MC.uimashup", (object) mashupId);
      string[] strArray = mashupId.Split('/');
      if (strArray.Length != 2)
        throw new ArgumentException("pipeId is not well formatted", "mashupId");
      return string.Format("/{0}/SRC/Mashups/Pipes/{1}.MC.uimashup", (object) strArray[0], (object) strArray[1]);
    }

    public static string GetServicePath(string serviceId)
    {
      if (string.IsNullOrEmpty(serviceId))
        throw new InvalidArgumentException("serviceId shall not be empty");
      if (!serviceId.Contains("/"))
        return string.Format("/SAP_BYD_TF/Mashups/Services/{0}.WS.uimashup", (object) serviceId);
      string[] strArray = serviceId.Split('/');
      if (strArray.Length != 2)
        throw new InvalidArgumentException("serviceId is not well formatted");
      return string.Format("/{0}/SRC/Mashups/Services/{1}.WS.uimashup", (object) strArray[0], (object) strArray[1]);
    }
  }
}
