﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Common.ObservableBase
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base;
using System;
using System.ComponentModel;

namespace SAP.BYD.LS.UI.Mashup.Common
{
  public abstract class ObservableBase : INotifyPropertyChanged
  {
    public event PropertyChangedEventHandler PropertyChanged;

    public void NotifyPropertyChanged(string propertyName)
    {
      if (this.PropertyChanged == null)
        return;
      if (UIThread.CheckAccess())
        this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
      else
        UIThread.Invoke(new Action(delegate { this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName)); }), new object[0]);
    }
  }
}
