﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Common.MashupValueHelpListModel
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Controls;
using System;

namespace SAP.BYD.LS.UI.Mashup.Common
{
  public class MashupValueHelpListModel : IDDLBItemProvider
  {
    public string Text { get; set; }

    public string Code { get; set; }

    public string Key
    {
      get
      {
        return this.Code;
      }
    }

    public string VisualKey
    {
      get
      {
        return this.Code;
      }
    }

    public string Value
    {
      get
      {
        return this.Text;
      }
    }

    public bool Matches(string match, bool matchKey, bool matchValue)
    {
      bool flag = false;
      if (matchValue)
        flag |= this.Value.StartsWith(match, StringComparison.CurrentCultureIgnoreCase);
      if (matchKey && !flag)
        flag |= this.VisualKey.StartsWith(match, StringComparison.CurrentCultureIgnoreCase);
      return flag;
    }
  }
}
