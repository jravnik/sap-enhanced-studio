﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.ExtensibilityResource
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class ExtensibilityResource
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) ExtensibilityResource.resourceMan, (object) null))
          ExtensibilityResource.resourceMan = new ResourceManager("SAP.BYD.LS.UIDesigner.ExtensibilityUI.ExtensibilityResource", typeof (ExtensibilityResource).Assembly);
        return ExtensibilityResource.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return ExtensibilityResource.resourceCulture;
      }
      set
      {
        ExtensibilityResource.resourceCulture = value;
      }
    }

    internal static string AnchorControllerInfo
    {
      get
      {
        return ExtensibilityResource.ResourceManager.GetString("AnchorControllerInfo", ExtensibilityResource.resourceCulture);
      }
    }

    internal ExtensibilityResource()
    {
    }
  }
}
