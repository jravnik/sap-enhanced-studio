﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.ExtensibilityUIBinding
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI
{
  public class ExtensibilityUIBinding
  {
    public string Image { get; set; }

    public string DisplayText { get; set; }

    public string XRepPathName { get; set; }

    public FlexBaseAnchor AnchorName { get; set; }

    public string ExtensibleModelType { get; set; }
  }
}
