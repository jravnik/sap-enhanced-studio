﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.BaseChangeTransactionModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI
{
  public abstract class BaseChangeTransactionModeler : Window
  {
    public ModelEntity ExtensibleModel { get; private set; }

    public UXComponent UIComponent { get; private set; }

    public IModelObject DTExtensibleModel { get; private set; }

    public IFloorplan DTComponent { get; private set; }

    public FlexBaseAnchor DTAnchor { get; private set; }

    public FlexBaseAnchorType Anchor { get; private set; }

    public ChangeTransaction ChangeTransaction { get; protected set; }

    public string UIComponentPath { get; private set; }

    public abstract bool RequiresReload { get; }

    public string AnchorPath { get; private set; }

    public string Solution { get; private set; }

    public string TopLevelFolder { get; private set; }

    protected bool IsChangeApplied { get; set; }

    protected abstract void revertChanges();

    public bool CreateChangeTransaction(IModelObject modelToExtend, IFloorplan component, FlexBaseAnchor anchor)
    {
      this.DTExtensibleModel = modelToExtend;
      this.DTComponent = component;
      this.DTAnchor = anchor;
      this.ExtensibleModel = this.DTExtensibleModel.GetOberonModel();
      this.UIComponent = this.DTComponent.GetOberonModel() as UXComponent;
      this.UIComponentPath = this.DTComponent.UniqueId;
      this.Anchor = this.DTAnchor.FlexBaseAnchorType;
      this.AnchorPath = this.DTAnchor.XRepPath;
      this.Solution = LoginManager.Instance.Solution;
      this.TopLevelFolder = LoginManager.Instance.TopLevelFolder;
      this.InitializeModeler();
      this.ShowDialog();
      return this.IsChangeApplied;
    }

    protected virtual void InitializeModeler()
    {
    }

    protected FlexibilityHandler getFlexibilityHandler()
    {
      FlexUserEnumType userType = FlexUserEnumType.SAP;
      if (LoginManager.Instance.DTMode == DTModes.Partner)
        userType = FlexUserEnumType.Partner;
      return FlexibilityHandler.GetInstance(this.UIComponentPath, this.UIComponent, LoginManager.Instance.User, userType, "E", this.TopLevelFolder, (string) null, false);
    }

    protected FlexBaseAnchorType getComponentAnchor(UXComponent component)
    {
      if (component != null && component.StableAnchor != null && component.StableAnchor.Count > 0)
        return component.StableAnchor[0];
      return (FlexBaseAnchorType) null;
    }

    protected string getText(DependentPropertyType textProperty)
    {
      if (textProperty == null)
        return string.Empty;
      if (string.IsNullOrEmpty(textProperty.textPoolId))
        return textProperty.fallbackValue;
      foreach (TextBlockType textBlockType in this.UIComponent.TextPool)
      {
        if (textBlockType.language == LoginManager.Instance.Language)
        {
          foreach (TextPoolEntryType textPoolEntryType in textBlockType.TextPoolEntry)
          {
            if (textPoolEntryType.textUuid == textProperty.textPoolId)
            {
              if (!string.IsNullOrEmpty(textPoolEntryType.text))
                return textPoolEntryType.text;
              return textProperty.fallbackValue;
            }
          }
        }
      }
      return (string) null;
    }
  }
}
