﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.ChangeTransactionInfo
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI
{
  public class ChangeTransactionInfo
  {
    [XmlAttribute]
    public string Name { get; set; }

    [XmlAttribute]
    public string EditorTypeName { get; set; }

    [XmlAttribute]
    [DefaultValue(false)]
    public bool IsAllowedToAddInSameLayer { get; set; }

    [XmlArray]
    public List<ChangeTransactionType> SupportedChangeTransactionTypes { get; set; }

    public ChangeTransactionInfo()
    {
      this.SupportedChangeTransactionTypes = new List<ChangeTransactionType>();
    }
  }
}
