﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.Dialogs.BOFieldSelectorDialog
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UIDesigner.BOConnector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.Dialogs
{
  public partial class BOFieldSelectorDialog : Window, IComponentConnector
  {
    private VisualBOBrowser boBrowser;
    //internal Grid rootGrid;
    //internal StackPanel toolBarpanel;
    //internal Button okButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public string SelectedBO
    {
      get
      {
        if (this.boBrowser == null)
          return string.Empty;
        return this.boBrowser.ESIBO;
      }
    }

    public string SelectedNamespace
    {
      get
      {
        if (this.boBrowser == null)
          return string.Empty;
        return this.boBrowser.ESINamespace;
      }
    }

    public string SelectedPath
    {
      get
      {
        if (this.boBrowser == null)
          return string.Empty;
        return this.boBrowser.ESIBindingPath;
      }
    }

    public string SelectedFieldExtensionNamespace
    {
      get
      {
        string str = string.Empty;
        if (this.boBrowser != null)
        {
          if (this.boBrowser.SelectedElement is BOElementAttribute)
          {
            BOElementAttribute selectedElement = this.boBrowser.SelectedElement as BOElementAttribute;
            str = selectedElement.ParentElement != null ? selectedElement.ParentElement.FieldExtensionNamespace : string.Empty;
          }
          else if (this.boBrowser.SelectedElement is BOElement)
            str = (this.boBrowser.SelectedElement as BOElement).FieldExtensionNamespace;
        }
        return str;
      }
    }

    public BOFieldSelectorDialog(string boNamespace, string bo, string fieldPath, bool isFilteringEnabled)
    {
      this.InitializeComponent();
      this.LoadBOBrowserControl(boNamespace, bo, fieldPath, isFilteringEnabled);
    }

    private void LoadBOBrowserControl(string boNamespace, string bo, string fieldPath, bool isFilteringEnabled)
    {
      this.boBrowser = new VisualBOBrowser();
      this.boBrowser.SetValue(Grid.ColumnProperty, (object) 0);
      this.boBrowser.SetValue(Grid.RowProperty, (object) 0);
      this.rootGrid.Children.Add((UIElement) this.boBrowser);
      this.boBrowser.DataSource = BODataSource.BO;
      this.boBrowser.ShowDetails = true;
      this.boBrowser.ControlMode = BOBrowserControlMode.elements;
      this.boBrowser.ShowModeSelector = false;
      this.boBrowser.ShowNSBOSelector = false;
      this.boBrowser.ShowParentTree = true;
      this.boBrowser.ShowToolbar = false;
      if (isFilteringEnabled)
      {
        this.boBrowser.EnableElementFiltering = true;
        this.boBrowser.ElementType = CCTSTypeFilter.indicator;
      }
      this.boBrowser.ReselectElement(bo, boNamespace, fieldPath, (IBOExtensionProvider) null);
    }

    private void OkButton_Click(object sender, RoutedEventArgs e)
    {
      this.DialogResult = new bool?(true);
      this.Close();
    }

    private void CancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/dialogs/bofieldselectordialog.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.rootGrid = (Grid) target;
    //      break;
    //    case 2:
    //      this.toolBarpanel = (StackPanel) target;
    //      break;
    //    case 3:
    //      this.okButton = (Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.OkButton_Click);
    //      break;
    //    case 4:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.CancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
