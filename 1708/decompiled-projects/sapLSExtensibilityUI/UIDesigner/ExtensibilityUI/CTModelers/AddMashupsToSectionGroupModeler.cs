﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddMashupsToSectionGroupModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Flexibility.StableAnchor;
using SAP.BYD.LS.UI.Mashup.Common;
using SAP.BYD.LS.UI.Mashup.Tools.Service;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddMashupsToSectionGroupModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private bool isOverViewOrAssignedObjects;
    private FlexibilityHandler flexHandler;
    private Dictionary<string, ExistingMashupsInfo> existingMashups;
    private Dictionary<string, string> changeHistory;
    private LoadMashupPipesWorker loader;
    private List<MashupViewModel> mashupViewModels;
    private List<SemanticGroupViewModel> semanticGroupViewModels;
    private MashupFlexibilityHandler mashupFlexHandler;
    private List<string> changeIDs;
    private FlexBaseAnchorType floorplanAnchor;
    private bool changesApplied;
    private Dictionary<string, ReferencedAnchorAndAppearance> appearances;
    private MashupViewHelper mashupViewHelper;
    private UXOutPortType extensionFieldOutPort;
    //internal AddMashupsBaseControl BaseControl;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return false;
      }
    }

    public AddMashupsToSectionGroupModeler()
    {
      this.InitializeComponent();
      this.Title = "Mashups Management";
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.flexHandler = this.getFlexibilityHandler();
      this.mashupFlexHandler = this.flexHandler.GetMashupFlexibilityHandler();
      this.floorplanAnchor = this.flexHandler.GetFloorPlanAnchor(this.UIComponent);
      if (this.flexHandler != null && this.mashupFlexHandler != null)
      {
        this.isOverViewOrAssignedObjects = StableAnchorHelper.GetStableOverviewAnchor(this.UIComponent) != null || StableAnchorHelper.GetStableAssignedObjectAnchor(this.UIComponent) != null;
        this.semanticGroupViewModels = new List<SemanticGroupViewModel>();
        this.mashupViewModels = new List<MashupViewModel>();
        this.changeHistory = new Dictionary<string, string>();
        this.existingMashups = new Dictionary<string, ExistingMashupsInfo>();
        this.changeIDs = new List<string>();
        Cursor cursor = this.Cursor;
        this.Cursor = Cursors.Wait;
        this.Render();
        this.Cursor = cursor;
      }
      this.UpdateExtensionFieldOutPort();
      this.mashupViewHelper = new MashupViewHelper(this.BaseControl, this.changeHistory, this.mashupFlexHandler, this.changeIDs, this.flexHandler, this.UIComponent, this.UIComponentPath, this.Anchor, this.extensionFieldOutPort);
    }

    private void UpdateExtensionFieldOutPort()
    {
      this.extensionFieldOutPort = (UXOutPortType) null;
      UXComponent uiComponent = this.UIComponent;
      if (uiComponent == null || uiComponent.Interface == null || uiComponent.Interface.OutPorts == null)
        return;
      List<UXOutPortType> outPort = uiComponent.Interface.OutPorts.OutPort;
      if (outPort == null)
        return;
      foreach (UXOutPortType uxOutPortType in outPort.Reverse<UXOutPortType>())
      {
        if (uxOutPortType != null && uxOutPortType.name == "ExtensionFieldsOutPort")
        {
          this.extensionFieldOutPort = uxOutPortType;
          break;
        }
      }
    }

    private void Render()
    {
      this.SubscribeEvents();
      this.LoadMashups();
      this.BaseControl.MashupGroups.ItemsSource = (IEnumerable) this.semanticGroupViewModels;
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      foreach (string changeId in this.changeIDs)
        this.mashupFlexHandler.RevertChangeTransactionInSession(changeId);
      this.IsChangeApplied = false;
    }

    private void LoadMashups()
    {
      if (StableAnchorHelper.GetStableFloorPlanAnchor(this.UIComponent) == null && !this.isOverViewOrAssignedObjects)
        return;
      this.loader = new LoadMashupPipesWorker();
      List<MashupPipeInfo> mashupPipeInfoList = this.loader.Load(new List<string>()
      {
        "Web_Widget"
      }, (List<MashupTypeCategories>) null, 1 != 0);
      if (mashupPipeInfoList != null)
      {
        AddMashupsBaseControl.LoadExistingChangeTransaction(this.UIComponentPath, this.UIComponent, this.AnchorPath, ref this.existingMashups, ref this.changeHistory);
        foreach (MashupPipeInfo mashupPipeInfo in mashupPipeInfoList)
        {
          if (!string.IsNullOrEmpty(mashupPipeInfo.DynParams))
          {
            bool flag1 = this.existingMashups.ContainsKey(mashupPipeInfo.PipeID);
            bool flag2 = flag1 && this.existingMashups[mashupPipeInfo.PipeID].FullColumnSpan;
            string str = !string.IsNullOrEmpty(mashupPipeInfo.DomainUrl) ? mashupPipeInfo.DomainUrl.Trim() : string.Empty;
            MashupViewModel mashupViewModel = new MashupViewModel()
            {
              DisplayName = mashupPipeInfo.DisplayName,
              Description = mashupPipeInfo.Description,
              IsVisible = flag1,
              ComponentPath = mashupPipeInfo.FilePath,
              SemanticCategory = mashupPipeInfo.SemanticCategory,
              MashupCategory = mashupPipeInfo.MashupCategory,
              MashupCategoryDescription = MashupRegistry.GetMashupCategoryDisplayNameByName(mashupPipeInfo.MashupCategory),
              PipeID = mashupPipeInfo.PipeID,
              IconUrl = str,
              FullColumnSpan = flag2,
              MashupType = mashupPipeInfo.MashupType,
              ViewStyle = mashupPipeInfo.ViewStyle,
              MashupInPlug = mashupPipeInfo.InPort,
              MashupOutPlug = mashupPipeInfo.OutPort,
              SourceOutPlug = string.Empty,
              TargetInPlug = string.Empty,
              ServiceAppearance = string.Empty,
              AppearanceOptions = new ObservableCollection<MashupValueHelpListModel>(),
              NotExisting = !flag1,
              MandatoryParams = mashupPipeInfo.MandParams ?? string.Empty,
              DynParams = mashupPipeInfo.DynParams ?? string.Empty,
              DynOutputs = mashupPipeInfo.DynOutputs ?? string.Empty,
              NoDynamicParaBinded = true
            };
            mashupViewModel.SetDynamicBindingOptionandVisibleAppearance(this.UIComponent, this.existingMashups, this.Anchor.xrepPath);
            if (mashupViewModel.IsDynamic)
              mashupViewModel.SourceOutPlug = "ExtensionFieldsOutPort";
            mashupViewModel.SetEntensionAppearance();
            this.mashupViewModels.Add(mashupViewModel);
          }
        }
      }
      this.semanticGroupViewModels = this.mashupViewModels.GroupBy<MashupViewModel, string>((Func<MashupViewModel, string>) (serviceViewModel => serviceViewModel.SemanticCategory)).OrderBy<IGrouping<string, MashupViewModel>, string>((Func<IGrouping<string, MashupViewModel>, string>) (g => g.ToArray<MashupViewModel>()[0].SemanticCategory)).Select<IGrouping<string, MashupViewModel>, SemanticGroupViewModel>((Func<IGrouping<string, MashupViewModel>, SemanticGroupViewModel>) (g => new SemanticGroupViewModel()
      {
        SemanticCategory = g.Key,
        DisplayName = SemanticCategory.Instance[g.Key],
        Mashups = g.ToList<MashupViewModel>()
      })).ToList<SemanticGroupViewModel>();
    }

    private void ButtonApply_Click(object sender, RoutedEventArgs e)
    {
      this.changesApplied = true;
      this.IsChangeApplied = true;
      this.Close();
    }

    private void ButtonCancel_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void MashupVisibilityCheckbox_Checked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.VisibilityCheckbox_Checked(sender, (List<FlexBaseAnchor>) null);
    }

    private void MashupVisibilityCheckbox_Unchecked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.VisibilityCheckbox_Unchecked(sender);
    }

    private void AppearanceBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBox comboBox = sender as ComboBox;
      if (comboBox == null)
        return;
      MashupViewModel dataContext = comboBox.DataContext as MashupViewModel;
      MashupValueHelpListModel selectedItem = comboBox.SelectedItem as MashupValueHelpListModel;
      if (dataContext == null || selectedItem == null)
        return;
      string appearanceKey = selectedItem.Key;
      dataContext.AppearanceKey = appearanceKey;
      dataContext.ServiceAppearance = dataContext.AppearanceOptions.Where<MashupValueHelpListModel>((Func<MashupValueHelpListModel, bool>) (o => o.Key == appearanceKey)).FirstOrDefault<MashupValueHelpListModel>().Value;
      if (!dataContext.IsVisible || !dataContext.NotExisting)
        return;
      this.mashupViewHelper.HandleInsertMashup(dataContext, (List<FlexBaseAnchor>) null);
    }

    private void MashupFullColumn_Checked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.FullColumn_Checked(sender, (List<FlexBaseAnchor>) null);
    }

    private void MashupFullColumn_Unchecked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.FullColumn_Unchecked(sender, (List<FlexBaseAnchor>) null);
    }

    private void MashupDynamicParameterChanged(object sender, SelectionChangedEventArgs e)
    {
      this.mashupViewHelper.DynamicParameterChanged(sender, (List<FlexBaseAnchor>) null, (UXComponent) null, true, (FlexBaseAnchorType) null);
    }

    private void MashupVisibilityBindingChanged(object sender, SelectionChangedEventArgs e)
    {
      this.mashupViewHelper.VisibilityBindingChanged(sender, (List<FlexBaseAnchor>) null);
    }

    private void SubscribeEvents()
    {
      this.BaseControl.ApplyClicked += new AddMashupsBaseControl.ApplyEventHandler(this.ButtonApply_Click);
      this.BaseControl.CancelClicked += new AddMashupsBaseControl.CancelEventHandler(this.ButtonCancel_Click);
      this.BaseControl.MashupChecked += new AddMashupsBaseControl.MashupCheckedEventHandler(this.MashupVisibilityCheckbox_Checked);
      this.BaseControl.MashupUnchecked += new AddMashupsBaseControl.MashupUncheckedEventHandler(this.MashupVisibilityCheckbox_Unchecked);
      this.BaseControl.MashupAppearanceChanged += new AddMashupsBaseControl.MashupAppearanceChangedEventHandler(this.AppearanceBox_SelectionChanged);
      this.BaseControl.MashupFullColumnChecked += new AddMashupsBaseControl.MashupFullColumnSpanCheckedEventHandler(this.MashupFullColumn_Checked);
      this.BaseControl.MashupFullColumnUnchecked += new AddMashupsBaseControl.MashupFullColumnSpanUnCheckedEventHandler(this.MashupFullColumn_Unchecked);
      this.BaseControl.DynamicParameterChanged += new AddMashupsBaseControl.MashupDynamicParameterChangedEventHander(this.MashupDynamicParameterChanged);
      this.BaseControl.VisibilityBindingChanged += new AddMashupsBaseControl.MashupVisibilityBindingChangedEventHandler(this.MashupVisibilityBindingChanged);
    }

    private void UnsubscribeEvents()
    {
      this.BaseControl.ApplyClicked -= new AddMashupsBaseControl.ApplyEventHandler(this.ButtonApply_Click);
      this.BaseControl.CancelClicked -= new AddMashupsBaseControl.CancelEventHandler(this.ButtonCancel_Click);
      this.BaseControl.MashupChecked -= new AddMashupsBaseControl.MashupCheckedEventHandler(this.MashupVisibilityCheckbox_Checked);
      this.BaseControl.MashupUnchecked -= new AddMashupsBaseControl.MashupUncheckedEventHandler(this.MashupVisibilityCheckbox_Unchecked);
      this.BaseControl.MashupAppearanceChanged -= new AddMashupsBaseControl.MashupAppearanceChangedEventHandler(this.AppearanceBox_SelectionChanged);
      this.BaseControl.MashupFullColumnChecked -= new AddMashupsBaseControl.MashupFullColumnSpanCheckedEventHandler(this.MashupFullColumn_Checked);
      this.BaseControl.MashupFullColumnUnchecked -= new AddMashupsBaseControl.MashupFullColumnSpanUnCheckedEventHandler(this.MashupFullColumn_Unchecked);
      this.BaseControl.DynamicParameterChanged -= new AddMashupsBaseControl.MashupDynamicParameterChangedEventHander(this.MashupDynamicParameterChanged);
      this.BaseControl.VisibilityBindingChanged -= new AddMashupsBaseControl.MashupVisibilityBindingChangedEventHandler(this.MashupVisibilityBindingChanged);
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
      this.Terminate();
    }

    private void Terminate()
    {
      this.UnsubscribeEvents();
      if (this.semanticGroupViewModels != null)
      {
        this.semanticGroupViewModels.Clear();
        this.semanticGroupViewModels = (List<SemanticGroupViewModel>) null;
      }
      if (this.mashupViewModels != null)
      {
        this.mashupViewModels.Clear();
        this.mashupViewModels = (List<MashupViewModel>) null;
      }
      if (this.changeIDs != null)
      {
        this.changeIDs.Clear();
        this.changeIDs = (List<string>) null;
      }
      if (this.changeHistory != null)
      {
        this.changeHistory.Clear();
        this.changeHistory = (Dictionary<string, string>) null;
      }
      if (this.appearances != null)
      {
        this.appearances.Clear();
        this.appearances = (Dictionary<string, ReferencedAnchorAndAppearance>) null;
      }
      this.mashupFlexHandler = (MashupFlexibilityHandler) null;
      this.flexHandler = (FlexibilityHandler) null;
      this.loader = (LoadMashupPipesWorker) null;
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addmashupstosectiongroupmodeler.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  if (connectionId == 1)
    //    this.BaseControl = (AddMashupsBaseControl) target;
    //  else
    //    this._contentLoaded = true;
    //}
  }
}
