﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddAssignedObjectModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controller;
using SAP.BYD.LS.UIDesigner.Model.RepositoryLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddAssignedObjectModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private const string TITLE_STRING = "Add Assigned Object";
    private const string TXT_ERROR_TC_NOT_SELECTED = "Please select a Target Component";
    private const string TXT_ERROR_NOT_APPL = "Target Component is not of Authorization Classification Type Application";
    private const string TXT_ERROR_NOT_INCVIEW = "Target Component is not of type Include View";
    private const string TXT_ERROR_NOT_TASKVIEW = "Target Component is not of type Tasklist View";
    private const string TXT_ERROR_NOT_TT = "Target Component is not a ThingType";
    private const string TXT_ERROR_NOT_APPL_TT = "ThingType is not of Authorization Classification Type Application";
    private const string TXT_ERROR_WRONG_TYPE = "Not supported Type";
    private FlexibilityHandler flexHandler;
    private FlexBaseAnchorType stableAnchorType;
    private List<ScopeRule> scopeRuleList;
    private bool changesApplied;
    private UXComponent targetComponent;
    //internal System.Windows.Controls.ComboBox objectTypeList;
    //internal System.Windows.Controls.Label componentLabel;
    //internal System.Windows.Controls.TextBox targetTextBox;
    //internal System.Windows.Controls.CheckBox exposeCheckBox;
    //internal System.Windows.Controls.Button addScopeRulesButton;
    //internal System.Windows.Controls.ListView scopingListView;
    //internal System.Windows.Controls.Button okButton;
    //internal System.Windows.Controls.Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public AddAssignedObjectModeler()
    {
      this.InitializeComponent();
      this.scopeRuleList = new List<ScopeRule>();
      this.scopingListView.Visibility = Visibility.Collapsed;
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      this.stableAnchorType = this.flexHandler.GetAssignedObjectAnchor(this.UIComponent);
      if (this.stableAnchorType == null)
        return;
      this.fillTypeListBox();
      this.objectTypeList.SelectedIndex = 0;
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      this.IsChangeApplied = false;
    }

    private void fillTypeListBox()
    {
      foreach (string name in Enum.GetNames(AssignedObjectTypes.NavigationTarget.GetType()))
      {
        if (name.ToString() != AssignedObjectTypes.Report.ToString() && name.ToString() != AssignedObjectTypes.RSSFeed.ToString() && name.ToString() != AssignedObjectTypes.DataObject.ToString())
          this.objectTypeList.Items.Add((object) name);
      }
    }

    private void fillScopingRules(List<ScopeRule> scopingRuleList)
    {
      this.scopingListView.Items.Clear();
      foreach (ScopeRule scopingRule in scopingRuleList)
        this.scopingListView.Items.Add((object) scopingRule.Value);
    }

    private void clearScopingRules()
    {
      this.scopingListView.Items.Clear();
      this.scopeRuleList.Clear();
    }

    private List<FloorplanType> getSelectedFloorType(string selectedType)
    {
      List<FloorplanType> floorplanTypeList = new List<FloorplanType>();
      if (selectedType == AssignedObjectTypes.NavigationTarget.ToString())
      {
        floorplanTypeList.Add(FloorplanType.EC);
        floorplanTypeList.Add(FloorplanType.FS);
        floorplanTypeList.Add(FloorplanType.GAF);
        floorplanTypeList.Add(FloorplanType.OIF);
        floorplanTypeList.Add(FloorplanType.OWL);
        floorplanTypeList.Add(FloorplanType.QA);
        floorplanTypeList.Add(FloorplanType.QC);
        floorplanTypeList.Add(FloorplanType.QV);
        floorplanTypeList.Add(FloorplanType.TI);
        return floorplanTypeList;
      }
      if (selectedType == AssignedObjectTypes.TaskListView.ToString())
      {
        floorplanTypeList.Add(FloorplanType.TaskListView);
        return floorplanTypeList;
      }
      if (!(selectedType == AssignedObjectTypes.WocViewInclude.ToString()))
        return floorplanTypeList;
      floorplanTypeList.Add(FloorplanType.WCVIEW);
      return floorplanTypeList;
    }

    private void objectTypeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.okButton.IsEnabled = false;
      this.targetTextBox.Text = string.Empty;
      this.addScopeRulesButton.IsEnabled = false;
      this.clearScopingRules();
      this.exposeCheckBox.IsEnabled = false;
    }

    private void addScopingRuleButton_Click(object sender, RoutedEventArgs e)
    {
      UCScopeRulesEditor scopeRulesEditor = new UCScopeRulesEditor(this.scopeRuleList, this.DTExtensibleModel, false, true);
      if (scopeRulesEditor.ShowDialog() != System.Windows.Forms.DialogResult.OK)
        return;
      this.scopeRuleList = scopeRulesEditor.NewCollection;
      this.fillScopingRules(this.scopeRuleList);
    }

    private string checkDependencies()
    {
      if (this.targetComponent == null)
        return "Please select a Target Component";
      if (this.objectTypeList.SelectedItem.ToString() == AssignedObjectTypes.NavigationTarget.ToString())
      {
        if (this.targetComponent.AuthorizationClassificationCode == AuthorizationClassificationCodeType.Application)
          return (string) null;
        return "Target Component is not of Authorization Classification Type Application";
      }
      if (this.objectTypeList.SelectedItem.ToString() == AssignedObjectTypes.WocViewInclude.ToString())
      {
        if (this.targetComponent.wocViewSubType == ViewSwitchTypes.IncludeView)
          return (string) null;
        return "Target Component is not of type Include View";
      }
      if (this.objectTypeList.SelectedItem.ToString() == AssignedObjectTypes.TaskListView.ToString())
      {
        if (this.targetComponent.componentType == UXComponentType.tasklistView)
          return (string) null;
        return "Target Component is not of type Tasklist View";
      }
      if (!(this.objectTypeList.SelectedItem.ToString() == AssignedObjectTypes.ThingType.ToString()))
        return "Not supported Type";
      if (this.targetComponent.componentType != UXComponentType.thingType && string.IsNullOrEmpty(this.targetComponent.thingType))
        return "Target Component is not a ThingType";
      if (this.targetComponent.AuthorizationClassificationCode == AuthorizationClassificationCodeType.Application)
        return (string) null;
      return "ThingType is not of Authorization Classification Type Application";
    }

    private void targetCompButton_Click(object sender, RoutedEventArgs e)
    {
      RepositoryFilterBrowser repositoryFilterBrowser = new RepositoryFilterBrowser(this.getSelectedFloorType(this.objectTypeList.SelectedItem.ToString()), (FloorplanInfo) null);
      if (repositoryFilterBrowser.ShowDialog() != System.Windows.Forms.DialogResult.OK)
        return;
      FloorplanInfo selectedComponent = repositoryFilterBrowser.SelectedComponent;
      this.targetComponent = ProjectWorkspaceManager.Instance.GetUXComponent(ref selectedComponent, false);
      string str = this.checkDependencies();
      if (str == null)
      {
        this.targetTextBox.Foreground = (Brush) new SolidColorBrush(Colors.Black);
        this.targetTextBox.Text = repositoryFilterBrowser.SelectedComponent.UniqueId;
        this.okButton.IsEnabled = true;
        this.addScopeRulesButton.IsEnabled = false;
        this.clearScopingRules();
        if (this.objectTypeList.SelectedItem.ToString() != AssignedObjectTypes.WocViewInclude.ToString())
          this.addScopeRulesButton.IsEnabled = true;
        this.exposeCheckBox.IsChecked = new bool?(false);
        this.exposeCheckBox.IsEnabled = false;
        if (!(this.objectTypeList.SelectedItem.ToString() == AssignedObjectTypes.NavigationTarget.ToString()) || !(selectedComponent.ComponentType == "EC") && !(selectedComponent.ComponentType == "OWL"))
          return;
        this.exposeCheckBox.IsEnabled = true;
      }
      else
      {
        this.targetTextBox.Text = str;
        this.targetTextBox.Foreground = (Brush) new SolidColorBrush(Colors.Red);
        this.okButton.IsEnabled = false;
        this.addScopeRulesButton.IsEnabled = false;
      }
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      bool expose2overview = false;
      if (this.exposeCheckBox.IsChecked.HasValue && this.exposeCheckBox.IsChecked.Value)
        expose2overview = true;
      FloorplanInfo componentDetails = ProjectWorkspaceManager.Instance.GetComponentDetails(this.targetTextBox.Text);
      FlexBaseAnchorType componentAnchor = this.getComponentAnchor(ProjectWorkspaceManager.Instance.GetUXComponent(ref componentDetails, false));
      List<string> scopeRules = new List<string>(this.scopeRuleList.Count);
      foreach (ScopeRule scopeRule in this.scopeRuleList)
        scopeRules.Add(scopeRule.Value);
      this.flexHandler.AddAssignedObj(this.stableAnchorType, this.objectTypeList.SelectedItem.ToString(), this.targetTextBox.Text, componentAnchor, expose2overview, scopeRules);
      this.changesApplied = true;
      this.IsChangeApplied = true;
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }
    
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addassignedobjectmodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.objectTypeList = (System.Windows.Controls.ComboBox) target;
    //      this.objectTypeList.SelectionChanged += new SelectionChangedEventHandler(this.objectTypeList_SelectionChanged);
    //      break;
    //    case 2:
    //      this.componentLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 3:
    //      this.targetTextBox = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 4:
    //      ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.targetCompButton_Click);
    //      break;
    //    case 5:
    //      this.exposeCheckBox = (System.Windows.Controls.CheckBox) target;
    //      break;
    //    case 6:
    //      this.addScopeRulesButton = (System.Windows.Controls.Button) target;
    //      this.addScopeRulesButton.Click += new RoutedEventHandler(this.addScopingRuleButton_Click);
    //      break;
    //    case 7:
    //      this.scopingListView = (System.Windows.Controls.ListView) target;
    //      break;
    //    case 8:
    //      this.okButton = (System.Windows.Controls.Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 9:
    //      this.cancelButton = (System.Windows.Controls.Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
