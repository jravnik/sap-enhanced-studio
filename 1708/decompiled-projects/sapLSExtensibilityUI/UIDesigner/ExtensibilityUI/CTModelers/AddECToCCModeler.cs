﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddECToCCModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Mashup.Tools.Service;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.RepositoryLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Xml.Linq;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddECToCCModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private const string TITLE_STRING = "Add Component";
    private FlexibilityHandler flexHandler;
    private FlexBaseAnchorType stableAnchorType;
    private FlexBaseAnchorType paneContainerAnchorType;
    private bool changesApplied;
    private AddECToCCModeler.ComponentTypeEnumType type;
    private FlexPositionEnumType position;
    private FlexAlignmentEnumType alignment;
    private string selectedReportUsage;
    private string selectedViewId;
    private string selectedVariantId;
    private MashupPipeInfo selectedMashup;
    private Dictionary<string, ExistingMashupsInfo> existingMashups;
    private UXOutPortType extensionFieldOutPort;
    //internal System.Windows.Controls.ComboBox typeComboBox;
    //internal System.Windows.Controls.Label componentLabel;
    //internal System.Windows.Controls.TextBox componentTextBox;
    //internal System.Windows.Controls.Button selectComponentButton;
    //internal System.Windows.Controls.Label mashupLabel;
    //internal System.Windows.Controls.ComboBox mashupComboBox;
    //internal System.Windows.Controls.Label reportLabel;
    //internal System.Windows.Controls.ComboBox reportComboBox;
    //internal System.Windows.Controls.Label viewLabel;
    //internal System.Windows.Controls.ComboBox viewComboBox;
    //internal System.Windows.Controls.Label variantLabel;
    //internal System.Windows.Controls.ComboBox variantComboBox;
    //internal System.Windows.Controls.Label titleLabel;
    //internal System.Windows.Controls.TextBox titleTextBox;
    //internal System.Windows.Controls.Label positionLabel;
    //internal System.Windows.Controls.ComboBox positionComboBox;
    //internal System.Windows.Controls.Label anchorLabel;
    //internal System.Windows.Controls.ComboBox anchorComboBox;
    //internal System.Windows.Controls.Label alignmentLabel;
    //internal System.Windows.Controls.ComboBox alignmentComboBox;
    //internal System.Windows.Controls.Label fullWidthLabel;
    //internal System.Windows.Controls.CheckBox fullWidthCheckBox;
    //internal System.Windows.Controls.Button addButton;
    //internal System.Windows.Controls.Button cancelButton;
    //internal System.Windows.Controls.Label errorMessage;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public AddECToCCModeler()
    {
      this.Title = "Add Component";
      this.ResizeMode = ResizeMode.NoResize;
      this.InitializeComponent();
      this.UpdateExtensionFieldOutPort();
    }

    private void UpdateExtensionFieldOutPort()
    {
      this.extensionFieldOutPort = (UXOutPortType) null;
      UXComponent uiComponent = this.UIComponent;
      if (uiComponent == null || uiComponent.Interface == null || uiComponent.Interface.OutPorts == null)
        return;
      List<UXOutPortType> outPort = uiComponent.Interface.OutPorts.OutPort;
      if (outPort == null)
        return;
      foreach (UXOutPortType uxOutPortType in outPort.Reverse<UXOutPortType>())
      {
        if (uxOutPortType != null && uxOutPortType.name == "ExtensionFieldsOutPort")
        {
          this.extensionFieldOutPort = uxOutPortType;
          break;
        }
      }
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      this.stableAnchorType = this.Anchor;
      if (this.stableAnchorType == null)
        return;
      this.fillTypeComboBox();
      this.fillPositionComboBox();
      this.fillAlignmentComboBox();
      this.typeComboBox.SelectedIndex = 0;
      this.positionComboBox.SelectedIndex = 0;
      this.alignmentComboBox.SelectedIndex = 0;
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      this.IsChangeApplied = false;
    }

    private string getDescription(AddECToCCModeler.ComponentTypeEnumType componentTypeEnumType)
    {
      string str = "";
      switch (componentTypeEnumType)
      {
        case AddECToCCModeler.ComponentTypeEnumType.OWL:
          str = "Object Work List (OWL)";
          break;
        case AddECToCCModeler.ComponentTypeEnumType.EC:
          str = "Embedded Component (EC)";
          break;
        case AddECToCCModeler.ComponentTypeEnumType.ICP:
          str = "Overview Report (ICP)";
          break;
        case AddECToCCModeler.ComponentTypeEnumType.MASHUP:
          str = "Mashup";
          break;
      }
      return str;
    }

    private void fillTypeComboBox()
    {
      Array values = Enum.GetValues(AddECToCCModeler.ComponentTypeEnumType.OWL.GetType());
      this.typeComboBox.Items.Clear();
      foreach (AddECToCCModeler.ComponentTypeEnumType componentTypeEnumType in values)
      {
        ComboBoxItem comboBoxItem = new ComboBoxItem();
        comboBoxItem.Content = (object) this.getDescription(componentTypeEnumType);
        comboBoxItem.Tag = (object) componentTypeEnumType;
        this.typeComboBox.Items.Add((object) comboBoxItem);
      }
    }

    private void fillReportComboBox(List<ReportItem> reports)
    {
      this.selectedReportUsage = "";
      if (reports == null)
        return;
      this.reportComboBox.Items.Clear();
      foreach (ReportItem report in reports)
      {
        ComboBoxItem comboBoxItem = new ComboBoxItem();
        comboBoxItem.Content = (object) report.Description;
        comboBoxItem.Tag = (object) report;
        this.reportComboBox.Items.Add((object) comboBoxItem);
      }
    }

    private void fillMashupComboBox(List<MashupPipeInfo> mashups)
    {
      if (mashups == null)
        return;
      this.mashupComboBox.Items.Clear();
      foreach (MashupPipeInfo mashup in mashups)
      {
        ComboBoxItem comboBoxItem = new ComboBoxItem();
        comboBoxItem.Content = (object) mashup.DisplayName;
        comboBoxItem.Tag = (object) mashup;
        this.mashupComboBox.Items.Add((object) comboBoxItem);
      }
    }

    private void fillViewComboBox(List<ReportInfoItem> views)
    {
      this.selectedViewId = "";
      if (views == null)
        return;
      this.viewComboBox.Items.Clear();
      foreach (ReportInfoItem view in views)
      {
        ComboBoxItem comboBoxItem = new ComboBoxItem();
        comboBoxItem.Content = (object) view.Description;
        comboBoxItem.Tag = (object) view.Id;
        this.viewComboBox.Items.Add((object) comboBoxItem);
      }
    }

    private void fillVariantComboBox(List<ReportInfoItem> variants)
    {
      this.selectedVariantId = "";
      if (variants == null)
        return;
      this.variantComboBox.Items.Clear();
      foreach (ReportInfoItem variant in variants)
      {
        ComboBoxItem comboBoxItem = new ComboBoxItem();
        comboBoxItem.Content = (object) variant.Description;
        comboBoxItem.Tag = (object) variant.Id;
        this.variantComboBox.Items.Add((object) comboBoxItem);
      }
    }

    private void fillPositionComboBox()
    {
      string[] names = Enum.GetNames(FlexPositionEnumType.Bottom.GetType());
      this.positionComboBox.Items.Clear();
      foreach (string str in names)
      {
        if (str.Equals(FlexPositionEnumType.BehindAnchor.ToString()))
        {
          if (HelperFunctions.getPaneContainerAnchors(this.UIComponent).Any<XElement>())
          {
            ComboBoxItem comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = (object) str;
            this.positionComboBox.Items.Add((object) comboBoxItem);
            this.anchorLabel.Visibility = Visibility.Visible;
            this.anchorComboBox.Visibility = Visibility.Visible;
          }
          else
          {
            this.anchorLabel.Visibility = Visibility.Collapsed;
            this.anchorComboBox.Visibility = Visibility.Collapsed;
          }
        }
        else if (!str.Equals(FlexPositionEnumType.BehindAnchor.ToString()))
        {
          ComboBoxItem comboBoxItem = new ComboBoxItem();
          comboBoxItem.Content = (object) str;
          this.positionComboBox.Items.Add((object) comboBoxItem);
        }
      }
    }

    private void fillAnchorComboBox()
    {
      IEnumerable<XElement> containerAnchors = HelperFunctions.getPaneContainerAnchors(this.UIComponent);
      this.anchorComboBox.Items.Clear();
      foreach (XElement xelement1 in containerAnchors)
      {
        XElement parent = xelement1.Parent;
        if (parent != null)
        {
          XElement xelement2 = parent.Element(HelperFunctions.ns_uxv + "EmbedComponentPane");
          if (xelement2 != null && xelement2.Attributes((XName) "embedName").Any<XAttribute>())
          {
            ComboBoxItem comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = (object) xelement2.Attribute((XName) "embedName").Value;
            this.paneContainerAnchorType = new FlexBaseAnchorType();
            this.paneContainerAnchorType.type = FlexAnchorEnumType.PaneContainerAnchor;
            if (xelement1.Attributes((XName) "xrepPath").Any<XAttribute>())
              this.paneContainerAnchorType.xrepPath = xelement1.Attribute((XName) "xrepPath").Value;
            if (xelement1.Attributes((XName) "release").Any<XAttribute>())
              this.paneContainerAnchorType.release = xelement1.Attribute((XName) "release").Value;
            this.paneContainerAnchorType.TargetFile = new List<FlexTargetFileType>();
            foreach (XElement element in xelement1.Elements(HelperFunctions.ns_uimc + "TargetFile"))
            {
              FlexTargetFileType flexTargetFileType = new FlexTargetFileType();
              if (element.Attributes((XName) "xrepPath").Any<XAttribute>())
                flexTargetFileType.xrepPath = element.Attribute((XName) "xrepPath").Value;
              this.paneContainerAnchorType.TargetFile.Add(flexTargetFileType);
            }
            comboBoxItem.Tag = (object) this.paneContainerAnchorType;
            this.anchorComboBox.Items.Add((object) comboBoxItem);
          }
        }
      }
    }

    private void fillAlignmentComboBox()
    {
      string[] names = Enum.GetNames(FlexAlignmentEnumType.Full.GetType());
      this.alignmentComboBox.Items.Clear();
      foreach (string str in names)
      {
        ComboBoxItem comboBoxItem = new ComboBoxItem();
        comboBoxItem.Content = (object) str;
        this.alignmentComboBox.Items.Add((object) comboBoxItem);
      }
    }

    private List<FloorplanType> getSelectedFloorType()
    {
      List<FloorplanType> floorplanTypeList = (List<FloorplanType>) null;
      switch (this.type)
      {
        case AddECToCCModeler.ComponentTypeEnumType.OWL:
          floorplanTypeList = new List<FloorplanType>();
          floorplanTypeList.Add(FloorplanType.OWL);
          break;
        case AddECToCCModeler.ComponentTypeEnumType.EC:
          floorplanTypeList = new List<FloorplanType>();
          floorplanTypeList.Add(FloorplanType.EC);
          break;
      }
      return floorplanTypeList;
    }

    private void enableOrDisableAddButton()
    {
      bool flag1 = false;
      if (this.componentTextBox.Text != null && this.componentTextBox.Text.Length > 0)
        flag1 = true;
      bool flag2 = false;
      if (this.reportComboBox.Text != null && this.reportComboBox.Text.Length > 0)
        flag2 = true;
      bool flag3 = false;
      if (this.titleTextBox.Text != null && this.titleTextBox.Text.Length > 0)
        flag3 = true;
      bool flag4 = false;
      ComboBoxItem selectedItem1 = this.anchorComboBox.SelectedItem as ComboBoxItem;
      if (selectedItem1 != null && selectedItem1.Content != null && selectedItem1.Content.ToString().Length > 0)
        flag4 = true;
      bool flag5 = false;
      ComboBoxItem selectedItem2 = this.positionComboBox.SelectedItem as ComboBoxItem;
      if (selectedItem2 != null && selectedItem2.Content != null && selectedItem2.Content.ToString().Equals(FlexPositionEnumType.BehindAnchor.ToString()))
        flag5 = true;
      if (this.type == AddECToCCModeler.ComponentTypeEnumType.ICP)
      {
        if (flag2 && flag3 && !flag5 || flag2 && flag3 && (flag5 && flag4))
          this.addButton.IsEnabled = true;
        else
          this.addButton.IsEnabled = false;
      }
      else
      {
        if (this.type == AddECToCCModeler.ComponentTypeEnumType.ICP)
          return;
        if (flag1 && flag3 && !flag5 || flag1 && flag3 && (flag5 && flag4))
        {
          if (this.type == AddECToCCModeler.ComponentTypeEnumType.MASHUP)
          {
            this.addButton.IsEnabled = true;
            if (this.selectedMashup == null)
              return;
            if (this.existingMashups == null)
            {
              this.existingMashups = new Dictionary<string, ExistingMashupsInfo>();
              Dictionary<string, string> unSaveCTChangeHistorys = new Dictionary<string, string>();
              AddMashupsBaseControl.LoadExistingChangeTransaction(this.UIComponentPath, this.UIComponent, this.AnchorPath, ref this.existingMashups, ref unSaveCTChangeHistorys);
            }
            if (this.existingMashups.Any<KeyValuePair<string, ExistingMashupsInfo>>((Func<KeyValuePair<string, ExistingMashupsInfo>, bool>) (existingMashup => this.selectedMashup.PipeID == existingMashup.Key)))
            {
              this.addButton.IsEnabled = false;
              this.fullWidthCheckBox.IsChecked = new bool?(this.existingMashups[this.selectedMashup.PipeID].FullColumnSpan);
              this.fullWidthCheckBox.IsEnabled = false;
            }
            else
            {
              this.fullWidthCheckBox.IsChecked = new bool?(false);
              this.fullWidthCheckBox.IsEnabled = true;
            }
          }
          else
            this.addButton.IsEnabled = true;
        }
        else
          this.addButton.IsEnabled = false;
      }
    }

    private void typeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBoxItem selectedItem = this.typeComboBox.SelectedItem as ComboBoxItem;
      if (selectedItem != null)
      {
        if (AddECToCCModeler.ComponentTypeEnumType.OWL.ToString() == selectedItem.Tag.ToString())
        {
          this.type = AddECToCCModeler.ComponentTypeEnumType.OWL;
          this.positionLabel.Visibility = Visibility.Visible;
          this.positionComboBox.Visibility = Visibility.Visible;
          this.alignmentLabel.Visibility = Visibility.Visible;
          this.alignmentComboBox.Visibility = Visibility.Visible;
          this.componentTextBox.Text = "";
          this.componentLabel.Visibility = Visibility.Visible;
          this.componentTextBox.Visibility = Visibility.Visible;
          this.selectComponentButton.Visibility = Visibility.Visible;
          this.mashupComboBox.Text = "";
          this.mashupLabel.Visibility = Visibility.Collapsed;
          this.mashupComboBox.Visibility = Visibility.Collapsed;
          this.fullWidthLabel.Visibility = Visibility.Collapsed;
          this.fullWidthCheckBox.Visibility = Visibility.Collapsed;
          this.reportComboBox.Text = "";
          this.reportLabel.Visibility = Visibility.Collapsed;
          this.reportComboBox.Visibility = Visibility.Collapsed;
          this.viewComboBox.Text = "";
          this.viewLabel.Visibility = Visibility.Collapsed;
          this.viewComboBox.Visibility = Visibility.Collapsed;
          this.variantComboBox.Text = "";
          this.variantLabel.Visibility = Visibility.Collapsed;
          this.variantComboBox.Visibility = Visibility.Collapsed;
          this.titleTextBox.Text = "";
          this.titleLabel.Visibility = Visibility.Visible;
          this.titleTextBox.Visibility = Visibility.Visible;
        }
        else if (AddECToCCModeler.ComponentTypeEnumType.EC.ToString() == selectedItem.Tag.ToString())
        {
          this.type = AddECToCCModeler.ComponentTypeEnumType.EC;
          this.positionLabel.Visibility = Visibility.Visible;
          this.positionComboBox.Visibility = Visibility.Visible;
          this.alignmentLabel.Visibility = Visibility.Visible;
          this.alignmentComboBox.Visibility = Visibility.Visible;
          this.componentTextBox.Text = "";
          this.componentLabel.Visibility = Visibility.Visible;
          this.componentTextBox.Visibility = Visibility.Visible;
          this.selectComponentButton.Visibility = Visibility.Visible;
          this.mashupComboBox.Text = "";
          this.mashupLabel.Visibility = Visibility.Collapsed;
          this.mashupComboBox.Visibility = Visibility.Collapsed;
          this.fullWidthLabel.Visibility = Visibility.Collapsed;
          this.fullWidthCheckBox.Visibility = Visibility.Collapsed;
          this.reportComboBox.Text = "";
          this.reportLabel.Visibility = Visibility.Collapsed;
          this.reportComboBox.Visibility = Visibility.Collapsed;
          this.viewComboBox.Text = "";
          this.viewLabel.Visibility = Visibility.Collapsed;
          this.viewComboBox.Visibility = Visibility.Collapsed;
          this.variantComboBox.Text = "";
          this.variantLabel.Visibility = Visibility.Collapsed;
          this.variantComboBox.Visibility = Visibility.Collapsed;
          this.titleTextBox.Text = "";
          this.titleLabel.Visibility = Visibility.Visible;
          this.titleTextBox.Visibility = Visibility.Visible;
        }
        else if (AddECToCCModeler.ComponentTypeEnumType.ICP.ToString() == selectedItem.Tag.ToString())
        {
          this.type = AddECToCCModeler.ComponentTypeEnumType.ICP;
          this.positionLabel.Visibility = Visibility.Visible;
          this.positionComboBox.Visibility = Visibility.Visible;
          this.alignmentLabel.Visibility = Visibility.Visible;
          this.alignmentComboBox.Visibility = Visibility.Visible;
          this.componentTextBox.Text = "/SAP_BYD_TF/Analytics/AnalysisPattern/ANA_ICP.EC.uicomponent";
          this.componentLabel.Visibility = Visibility.Collapsed;
          this.componentTextBox.Visibility = Visibility.Collapsed;
          this.selectComponentButton.Visibility = Visibility.Collapsed;
          this.mashupComboBox.Text = "";
          this.mashupLabel.Visibility = Visibility.Collapsed;
          this.mashupComboBox.Visibility = Visibility.Collapsed;
          this.fullWidthLabel.Visibility = Visibility.Collapsed;
          this.fullWidthCheckBox.Visibility = Visibility.Collapsed;
          this.reportComboBox.Text = "";
          this.reportLabel.Visibility = Visibility.Visible;
          this.reportComboBox.Visibility = Visibility.Visible;
          this.viewComboBox.Text = "";
          this.viewLabel.Visibility = Visibility.Collapsed;
          this.viewComboBox.Visibility = Visibility.Collapsed;
          this.variantComboBox.Text = "";
          this.variantLabel.Visibility = Visibility.Collapsed;
          this.variantComboBox.Visibility = Visibility.Collapsed;
          this.titleTextBox.Text = "";
          this.titleLabel.Visibility = Visibility.Visible;
          this.titleTextBox.Visibility = Visibility.Visible;
          WaitForm.Start();
          List<ReportItem> reports = XRepositoryProxy.Instance.GetReports(this.UIComponentPath);
          WaitForm.Stop();
          this.fillReportComboBox(reports);
        }
        else if (AddECToCCModeler.ComponentTypeEnumType.MASHUP.ToString() == selectedItem.Tag.ToString())
        {
          this.type = AddECToCCModeler.ComponentTypeEnumType.MASHUP;
          this.positionLabel.Visibility = Visibility.Collapsed;
          this.positionComboBox.Visibility = Visibility.Collapsed;
          this.alignmentLabel.Visibility = Visibility.Collapsed;
          this.alignmentComboBox.Visibility = Visibility.Collapsed;
          this.componentTextBox.Text = "";
          this.componentLabel.Visibility = Visibility.Collapsed;
          this.componentTextBox.Visibility = Visibility.Collapsed;
          this.selectComponentButton.Visibility = Visibility.Collapsed;
          this.mashupComboBox.Text = "";
          this.mashupLabel.Visibility = Visibility.Visible;
          this.mashupComboBox.Visibility = Visibility.Visible;
          this.fullWidthLabel.Visibility = Visibility.Collapsed;
          this.fullWidthCheckBox.Visibility = Visibility.Collapsed;
          this.reportComboBox.Text = "";
          this.reportLabel.Visibility = Visibility.Collapsed;
          this.reportComboBox.Visibility = Visibility.Collapsed;
          this.viewComboBox.Text = "";
          this.viewLabel.Visibility = Visibility.Collapsed;
          this.viewComboBox.Visibility = Visibility.Collapsed;
          this.variantComboBox.Text = "";
          this.variantLabel.Visibility = Visibility.Collapsed;
          this.variantComboBox.Visibility = Visibility.Collapsed;
          this.titleTextBox.Text = "";
          this.titleLabel.Visibility = Visibility.Collapsed;
          this.titleTextBox.Visibility = Visibility.Collapsed;
          WaitForm.Start();
          List<MashupPipeInfo> source = LoadMashupPipesWorker.Instance.LoadWebWidgetMashups();
          WaitForm.Stop();
          this.fillMashupComboBox(source.Where<MashupPipeInfo>((Func<MashupPipeInfo, bool>) (mashup => string.IsNullOrEmpty(mashup.DynParams))).ToList<MashupPipeInfo>());
        }
      }
      this.errorMessage.Content = (object) "";
      this.enableOrDisableAddButton();
    }

    private void titleTextBox_TextChanged(object sender, TextChangedEventArgs e)
    {
      this.enableOrDisableAddButton();
    }

    private void positionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBoxItem selectedItem = this.positionComboBox.SelectedItem as ComboBoxItem;
      if (selectedItem != null)
      {
        if (FlexPositionEnumType.Bottom.ToString() == selectedItem.Content.ToString())
        {
          this.position = FlexPositionEnumType.Bottom;
          this.anchorComboBox.IsEnabled = false;
          this.anchorComboBox.Items.Clear();
        }
        else if (FlexPositionEnumType.Top.ToString() == selectedItem.Content.ToString())
        {
          this.position = FlexPositionEnumType.Top;
          this.anchorComboBox.IsEnabled = false;
          this.anchorComboBox.Items.Clear();
        }
        else if (FlexPositionEnumType.BehindAnchor.ToString() == selectedItem.Content.ToString())
        {
          this.position = FlexPositionEnumType.BehindAnchor;
          this.anchorComboBox.IsEnabled = true;
          this.fillAnchorComboBox();
        }
      }
      this.enableOrDisableAddButton();
    }

    private void anchorComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBoxItem selectedItem = this.anchorComboBox.SelectedItem as ComboBoxItem;
      if (selectedItem != null)
        this.paneContainerAnchorType = selectedItem.Tag as FlexBaseAnchorType;
      this.enableOrDisableAddButton();
    }

    private void alignmentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBoxItem selectedItem = this.alignmentComboBox.SelectedItem as ComboBoxItem;
      if (selectedItem != null)
      {
        if (FlexAlignmentEnumType.Full.ToString() == selectedItem.Content.ToString())
          this.alignment = FlexAlignmentEnumType.Full;
        else if (FlexAlignmentEnumType.Left.ToString() == selectedItem.Content.ToString())
          this.alignment = FlexAlignmentEnumType.Left;
        else if (FlexAlignmentEnumType.Right.ToString() == selectedItem.Content.ToString())
          this.alignment = FlexAlignmentEnumType.Right;
      }
      this.enableOrDisableAddButton();
    }

    private void selectComponentButton_Click(object sender, RoutedEventArgs e)
    {
      this.titleTextBox.Text = "";
      this.errorMessage.Content = (object) "";
      if (!this.type.Equals((object) AddECToCCModeler.ComponentTypeEnumType.ICP) && !this.type.Equals((object) AddECToCCModeler.ComponentTypeEnumType.MASHUP))
      {
        RepositoryFilterBrowser repositoryFilterBrowser = new RepositoryFilterBrowser(this.getSelectedFloorType(), (FloorplanInfo) null);
        if (repositoryFilterBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        {
          this.componentTextBox.Text = repositoryFilterBrowser.SelectedComponent.UniqueId;
          this.componentTextBox.ToolTip = (object) null;
          if (this.type.Equals((object) AddECToCCModeler.ComponentTypeEnumType.EC))
          {
            string uniqueId = repositoryFilterBrowser.SelectedComponent.UniqueId;
            List<RepositoryAttribute> Attributes = new List<RepositoryAttribute>();
            ContentChangeability changeability;
            XElement xelement = XElement.Parse(XRepositoryProxy.Instance.ReadXRepContentAsString(uniqueId, out Attributes, out changeability));
            if (xelement.Attributes((XName) "loadVariant").Any<XAttribute>())
            {
              string str = xelement.Attribute((XName) "loadVariant").Value;
              if (str.ToLower().Equals("quicklink") || str.ToLower().Equals("cc_quicklink") || str.ToLower().Equals("worklists"))
              {
                this.componentTextBox.Text = "";
                this.errorMessage.Content = (object) "Select an embeded component, which represents no 'Quicklink' or 'Worklist'.";
              }
            }
            else if (xelement.Attributes((XName) "AuthorizationClassificationCode").Any<XAttribute>())
            {
              string str = xelement.Attribute((XName) "AuthorizationClassificationCode").Value;
              if (!str.ToLower().Equals("application") && !str.ToLower().Equals("applicationcontainer"))
              {
                this.componentTextBox.Text = "";
                this.errorMessage.Content = (object) "Select an embedded component with authorization classification code 'Application' or 'ApplicationContainer'.";
              }
            }
          }
        }
        this.selectedReportUsage = "";
        this.selectedViewId = "";
        this.selectedVariantId = "";
      }
      this.enableOrDisableAddButton();
    }

    private void mashupComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBoxItem selectedItem = this.mashupComboBox.SelectedItem as ComboBoxItem;
      if (selectedItem == null)
        return;
      this.selectedMashup = selectedItem.Tag as MashupPipeInfo;
      this.componentTextBox.Text = this.selectedMashup.FilePath;
      this.titleTextBox.Text = this.selectedMashup.DisplayName;
      this.fullWidthLabel.Visibility = this.selectedMashup.ViewStyle == "inScreen" ? Visibility.Visible : Visibility.Collapsed;
      this.fullWidthCheckBox.Visibility = this.selectedMashup.ViewStyle == "inScreen" ? Visibility.Visible : Visibility.Collapsed;
      this.enableOrDisableAddButton();
    }

    private void reportComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBoxItem selectedItem = this.reportComboBox.SelectedItem as ComboBoxItem;
      if (selectedItem == null)
        return;
      ReportItem tag = selectedItem.Tag as ReportItem;
      this.selectedReportUsage = tag.ReportUsage;
      this.selectedViewId = "";
      List<ReportInfoItem> views = tag.getViews();
      if (views != null && views.Any<ReportInfoItem>())
      {
        this.viewLabel.Visibility = Visibility.Visible;
        this.viewComboBox.Visibility = Visibility.Visible;
        this.fillViewComboBox(views);
      }
      else
      {
        this.viewLabel.Visibility = Visibility.Collapsed;
        this.viewComboBox.Visibility = Visibility.Collapsed;
      }
      this.selectedVariantId = "";
      List<ReportInfoItem> variants = tag.getVariants();
      if (variants != null && variants.Any<ReportInfoItem>())
      {
        this.variantLabel.Visibility = Visibility.Visible;
        this.variantComboBox.Visibility = Visibility.Visible;
        this.fillVariantComboBox(variants);
      }
      else
      {
        this.variantLabel.Visibility = Visibility.Collapsed;
        this.variantComboBox.Visibility = Visibility.Collapsed;
      }
      this.enableOrDisableAddButton();
    }

    private void viewComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBoxItem selectedItem = this.viewComboBox.SelectedItem as ComboBoxItem;
      if (selectedItem == null)
        return;
      this.selectedViewId = selectedItem.Tag as string;
      this.enableOrDisableAddButton();
    }

    private void variantComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBoxItem selectedItem = this.variantComboBox.SelectedItem as ComboBoxItem;
      if (selectedItem == null)
        return;
      this.selectedVariantId = selectedItem.Tag as string;
      this.enableOrDisableAddButton();
    }

    private void addButton_Click(object sender, RoutedEventArgs e)
    {
      string text = this.componentTextBox.Text;
      if (text.Contains("Mashups"))
      {
        if (this.selectedMashup != null)
        {
          string currentViewTypeId = AddMashupsToFloorplanAnchorModeler.GetCurrentViewTypeID(this.UIComponent);
          if (!string.IsNullOrEmpty(currentViewTypeId))
          {
            string str = !string.IsNullOrEmpty(this.selectedMashup.DomainUrl) ? this.selectedMashup.DomainUrl.Trim() : string.Empty;
            MashupViewModel mashupInsertionInfo = new MashupViewModel()
            {
              PipeID = this.selectedMashup.PipeID,
              DisplayName = this.selectedMashup.DisplayName,
              Description = this.selectedMashup.Description,
              ComponentPath = this.selectedMashup.FilePath,
              MashupCategory = this.selectedMashup.MashupCategory,
              IconUrl = str,
              MashupType = this.selectedMashup.MashupType,
              ViewStyle = this.selectedMashup.ViewStyle,
              MashupInPlug = this.selectedMashup.InPort,
              MashupOutPlug = this.selectedMashup.OutPort,
              SourceOutPlug = string.Empty,
              TargetInPlug = string.Empty,
              AppearanceKey = "V-" + currentViewTypeId,
              Appearance = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.NewPaneContainer),
              ContainerID = currentViewTypeId,
              ReferencedAnchorXrepPath = this.Anchor.xrepPath,
              FullColumnSpan = this.selectedMashup.FullColumnSpan
            };
            if (mashupInsertionInfo.IsDynamic)
              mashupInsertionInfo.SourceOutPlug = "ExtensionFieldsOutPort";
            if (("Web_Widget" == mashupInsertionInfo.MashupCategory || this.extensionFieldOutPort == null && mashupInsertionInfo.MashupCategory == string.Empty && mashupInsertionInfo.ViewStyle == MashupViewStyleType.inScreen.ToString()) && mashupInsertionInfo.DynamicBindings.Count == 0)
              mashupInsertionInfo.SourceOutPlug = string.Empty;
            if (mashupInsertionInfo.SourceOutPlug == "ExtensionFieldsOutPort" && string.IsNullOrEmpty(mashupInsertionInfo.MashupInPlug))
              mashupInsertionInfo.MashupInPlug = "ExtensionFieldsInPort";
            AddMashupsBaseControl.InsertWebWidgetMashup(mashupInsertionInfo, this.flexHandler, this.UIComponent, this.UIComponentPath, this.Anchor);
          }
        }
      }
      else
      {
        FloorplanInfo componentDetails = ProjectWorkspaceManager.Instance.GetComponentDetails(text);
        FlexBaseAnchorType componentAnchor = this.getComponentAnchor(ProjectWorkspaceManager.Instance.GetUXComponent(ref componentDetails, false));
        if (this.position.Equals((object) FlexPositionEnumType.BehindAnchor))
          this.flexHandler.AddEmbeddedComponentToControlCenter(this.paneContainerAnchorType, text, this.position.ToString(), this.alignment.ToString(), this.titleTextBox.Text, this.selectedReportUsage, this.selectedViewId, this.selectedVariantId, componentAnchor);
        else
          this.flexHandler.AddEmbeddedComponentToControlCenter(this.stableAnchorType, text, this.position.ToString(), this.alignment.ToString(), this.titleTextBox.Text, this.selectedReportUsage, this.selectedViewId, this.selectedVariantId, componentAnchor);
      }
      this.changesApplied = true;
      this.IsChangeApplied = true;
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void addECToCCModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }

    private void fullWidthCheckBox_Checked(object sender, RoutedEventArgs e)
    {
      this.selectedMashup.FullColumnSpan = true;
    }

    private void fullWidthCheckBox_Unchecked(object sender, RoutedEventArgs e)
    {
      this.selectedMashup.FullColumnSpan = false;
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addectoccmodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.typeComboBox = (System.Windows.Controls.ComboBox) target;
    //      this.typeComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.typeComboBox_SelectionChanged);
    //      break;
    //    case 2:
    //      this.componentLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 3:
    //      this.componentTextBox = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 4:
    //      this.selectComponentButton = (System.Windows.Controls.Button) target;
    //      this.selectComponentButton.Click += new RoutedEventHandler(this.selectComponentButton_Click);
    //      break;
    //    case 5:
    //      this.mashupLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 6:
    //      this.mashupComboBox = (System.Windows.Controls.ComboBox) target;
    //      this.mashupComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.mashupComboBox_SelectionChanged);
    //      break;
    //    case 7:
    //      this.reportLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 8:
    //      this.reportComboBox = (System.Windows.Controls.ComboBox) target;
    //      this.reportComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.reportComboBox_SelectionChanged);
    //      break;
    //    case 9:
    //      this.viewLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 10:
    //      this.viewComboBox = (System.Windows.Controls.ComboBox) target;
    //      this.viewComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.viewComboBox_SelectionChanged);
    //      break;
    //    case 11:
    //      this.variantLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 12:
    //      this.variantComboBox = (System.Windows.Controls.ComboBox) target;
    //      this.variantComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.variantComboBox_SelectionChanged);
    //      break;
    //    case 13:
    //      this.titleLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 14:
    //      this.titleTextBox = (System.Windows.Controls.TextBox) target;
    //      this.titleTextBox.TextChanged += new TextChangedEventHandler(this.titleTextBox_TextChanged);
    //      break;
    //    case 15:
    //      this.positionLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 16:
    //      this.positionComboBox = (System.Windows.Controls.ComboBox) target;
    //      this.positionComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.positionComboBox_SelectionChanged);
    //      break;
    //    case 17:
    //      this.anchorLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 18:
    //      this.anchorComboBox = (System.Windows.Controls.ComboBox) target;
    //      this.anchorComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.anchorComboBox_SelectionChanged);
    //      break;
    //    case 19:
    //      this.alignmentLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 20:
    //      this.alignmentComboBox = (System.Windows.Controls.ComboBox) target;
    //      this.alignmentComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.alignmentComboBox_SelectionChanged);
    //      break;
    //    case 21:
    //      this.fullWidthLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 22:
    //      this.fullWidthCheckBox = (System.Windows.Controls.CheckBox) target;
    //      this.fullWidthCheckBox.Checked += new RoutedEventHandler(this.fullWidthCheckBox_Checked);
    //      this.fullWidthCheckBox.Unchecked += new RoutedEventHandler(this.fullWidthCheckBox_Unchecked);
    //      break;
    //    case 23:
    //      this.addButton = (System.Windows.Controls.Button) target;
    //      this.addButton.Click += new RoutedEventHandler(this.addButton_Click);
    //      break;
    //    case 24:
    //      this.cancelButton = (System.Windows.Controls.Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    case 25:
    //      this.errorMessage = (System.Windows.Controls.Label) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    public enum ComponentTypeEnumType
    {
      OWL,
      EC,
      ICP,
      MASHUP,
    }
  }
}
