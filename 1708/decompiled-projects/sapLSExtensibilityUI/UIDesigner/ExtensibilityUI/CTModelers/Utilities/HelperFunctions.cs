﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities.HelperFunctions
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities
{
  public class HelperFunctions
  {
    public static XNamespace ns_uxv = (XNamespace) "http://www.sap.com/a1s/cd/oberon/uxview-1.0";
    public static XNamespace ns_uimc = (XNamespace) "http://www.sap.com/a1s/cd/oberon/uimodelchange-1.0";
    public static XNamespace ns_uxc = (XNamespace) "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0";

    public static string convertUXComponentToString(UXComponent uiComponent)
    {
      try
      {
        UXComponentSerializer componentSerializer = new UXComponentSerializer();
        EncodedStringWriter encodedStringWriter = new EncodedStringWriter();
        XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
        namespaces.Add("base", "http://www.sap.com/a1s/cd/oberon/base-1.0");
        namespaces.Add("uxv", HelperFunctions.ns_uxv.NamespaceName);
        namespaces.Add("uxc", HelperFunctions.ns_uxc.NamespaceName);
        namespaces.Add("", "http://www.sap.com/a1s/cd/oberon/uxcomponent-1.0");
        componentSerializer.Serialize((TextWriter) encodedStringWriter, (object) uiComponent, namespaces);
        return encodedStringWriter.ToString();
      }
      catch (Exception ex)
      {
        throw new Exception("Unexpected exception occurred during UXComponent serialization.");
      }
    }

    public static IEnumerable<XElement> getPaneContainerAnchors(UXComponent UIComponent)
    {
      return XElement.Parse(HelperFunctions.convertUXComponentToString(UIComponent)).Descendants().Where<XElement>((Func<XElement, bool>) (el =>
      {
        if (el.Name.Equals((object) (HelperFunctions.ns_uxv + "StableAnchor")) && el.Attributes((XName) "type").Any<XAttribute>())
          return el.Attribute((XName) "type").Value.Equals("PaneContainerAnchor");
        return false;
      }));
    }

    public static IEnumerable<XElement> getTabStripPanes(UXComponent UIComponent)
    {
      return XElement.Parse(HelperFunctions.convertUXComponentToString(UIComponent)).Descendants().Where<XElement>((Func<XElement, bool>) (el => el.Name.Equals((object) (HelperFunctions.ns_uxv + "TabStripPane"))));
    }

    public static IEnumerable<XElement> getSectionGroupAnchors(UXComponent UIComponent)
    {
      return XElement.Parse(HelperFunctions.convertUXComponentToString(UIComponent)).Descendants().Where<XElement>((Func<XElement, bool>) (el =>
      {
        if (el.Name.Equals((object) (HelperFunctions.ns_uxv + "StableAnchor")) && el.Attributes((XName) "type").Any<XAttribute>())
          return el.Attribute((XName) "type").Value.Equals("SectionGroupAnchor");
        return false;
      }));
    }

    public static IEnumerable<XElement> getListAnchors(UXComponent UIComponent)
    {
      return XElement.Parse(HelperFunctions.convertUXComponentToString(UIComponent)).Descendants().Where<XElement>((Func<XElement, bool>) (el =>
      {
        if (el.Name.Equals((object) (HelperFunctions.ns_uxv + "StableAnchor")) && el.Attributes((XName) "type").Any<XAttribute>())
          return el.Attribute((XName) "type").Value.Equals("ListAnchor");
        return false;
      }));
    }

    public static IEnumerable<XElement> getTextEditControls(UXComponent UIComponent)
    {
      return XElement.Parse(HelperFunctions.convertUXComponentToString(UIComponent)).Descendants().Where<XElement>((Func<XElement, bool>) (el => el.Name.Equals((object) (HelperFunctions.ns_uxv + "TextEdit"))));
    }

    public static IEnumerable<XElement> getOVSControls(XElement uiComponent)
    {
      return uiComponent.Descendants().Where<XElement>((Func<XElement, bool>) (el => el.Name.Equals((object) (HelperFunctions.ns_uxv + "ObjectValueSelector"))));
    }

    public static IEnumerable<XElement> getOVSControls(UXComponent UIComponent)
    {
      return HelperFunctions.getOVSControls(XElement.Parse(HelperFunctions.convertUXComponentToString(UIComponent)));
    }

    public static IEnumerable<XElement> getColumns(UXComponent UIComponent)
    {
      return XElement.Parse(HelperFunctions.convertUXComponentToString(UIComponent)).Descendants().Where<XElement>((Func<XElement, bool>) (el => el.Name.Equals((object) (HelperFunctions.ns_uxv + "Column"))));
    }

    public static IEnumerable<XElement> getSectionGroupItems(UXComponent UIComponent)
    {
      return XElement.Parse(HelperFunctions.convertUXComponentToString(UIComponent)).Descendants().Where<XElement>((Func<XElement, bool>) (el => el.Name.Equals((object) (HelperFunctions.ns_uxv + "SectionGroupItem"))));
    }

    public static IEnumerable<XElement> getOutports(UXComponent UIComponent)
    {
      return XElement.Parse(HelperFunctions.convertUXComponentToString(UIComponent)).Descendants().Where<XElement>((Func<XElement, bool>) (el => el.Name.Equals((object) (HelperFunctions.ns_uxc + "OutPort"))));
    }

    public static IEnumerable<XElement> getAllExtendedTags(UXComponent UIComponent)
    {
      return XElement.Parse(HelperFunctions.convertUXComponentToString(UIComponent)).Descendants().Where<XElement>((Func<XElement, bool>) (el =>
      {
        if (el.Name.Equals((object) (HelperFunctions.ns_uxv + "SectionGroupItem")) && el.Attributes((XName) "extendedBy").Any<XAttribute>())
          return true;
        if (el.Name.Equals((object) (HelperFunctions.ns_uxv + "Column")))
          return el.Attributes((XName) "extendedBy").Any<XAttribute>();
        return false;
      }));
    }
  }
}
