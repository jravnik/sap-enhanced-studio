﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities.OBNConfigHelper
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility.CoreAPI;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controller;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Oberon.Controller;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using SAP.BYD.LS.UIDesigner.UICore.Controllers.Interface;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities
{
  public class OBNConfigHelper
  {
    private Form m_Container;
    private IOutPortType m_outPort;

    public IFloorplan m_editableSourceComponent { get; private set; }

    public VisualInportOutport m_outportConfig { get; set; }

    public IOBN m_navigation { get; private set; }

    public string NavBONS
    {
      get
      {
        if (this.m_navigation != null)
          return this.m_navigation.NavBONS;
        return (string) null;
      }
    }

    public string NavBO
    {
      get
      {
        if (this.m_navigation != null)
          return this.m_navigation.NavBO;
        return (string) null;
      }
    }

    public string NavBONode
    {
      get
      {
        if (this.m_navigation != null)
          return this.m_navigation.NavBONode;
        return (string) null;
      }
    }

    public string NavOperation
    {
      get
      {
        if (this.m_navigation == null)
          return (string) null;
        if (this.m_navigation.NavOperation != null)
          return this.m_navigation.NavOperation.Name;
        return (string) null;
      }
    }

    public string NavContextAttribute
    {
      get
      {
        if (this.m_navigation == null)
          return (string) null;
        if (this.m_navigation.NavContextAttribute == null)
          return (string) null;
        if (!string.IsNullOrEmpty(this.m_navigation.NavContextAttribute.Constant))
          return this.m_navigation.NavContextAttribute.Constant;
        if (!string.IsNullOrEmpty(this.m_navigation.NavContextAttribute.Bind))
          return this.m_navigation.NavContextAttribute.Bind;
        return this.m_navigation.NavContextAttribute.Name;
      }
    }

    public bool isConstantNavContext
    {
      get
      {
        return this.m_navigation != null && this.m_navigation.NavContextAttribute != null && string.IsNullOrEmpty(this.m_navigation.NavContextAttribute.Constant);
      }
    }

    public string PortTypePackage
    {
      get
      {
        if (this.m_navigation == null)
          return (string) null;
        if (this.m_navigation.OutPlug == null)
          return (string) null;
        return this.m_navigation.OutPlug.PortTypePackage;
      }
    }

    public string PortTypeReference
    {
      get
      {
        if (this.m_navigation == null)
          return (string) null;
        if (this.m_navigation.OutPlug == null)
          return (string) null;
        return this.m_navigation.OutPlug.PortTypeReference;
      }
    }

    public List<OBNSimpleBindingType> SimpleParameters
    {
      get
      {
        if (this.m_navigation == null)
          return (List<OBNSimpleBindingType>) null;
        if (this.m_navigation.OutPlug == null)
          return (List<OBNSimpleBindingType>) null;
        UXOutPort outPlug = (UXOutPort) this.m_navigation.OutPlug;
        if (outPlug.Parameters == null)
          return (List<OBNSimpleBindingType>) null;
        List<OBNSimpleBindingType> simpleBindingTypeList = new List<OBNSimpleBindingType>();
        foreach (IParameter parameter in outPlug.Parameters)
        {
          OBNSimpleBindingType simpleBinding = this.createSimpleBinding(parameter);
          if (simpleBinding != null)
            simpleBindingTypeList.Add(simpleBinding);
        }
        return simpleBindingTypeList;
      }
    }

    public List<OBNListBindingType> ListParameters
    {
      get
      {
        if (this.m_navigation == null)
          return (List<OBNListBindingType>) null;
        if (this.m_navigation.OutPlug == null)
          return (List<OBNListBindingType>) null;
        UXOutPort outPlug = (UXOutPort) this.m_navigation.OutPlug;
        if (outPlug.ListParameters == null)
          return (List<OBNListBindingType>) null;
        List<OBNListBindingType> obnListBindingTypeList = new List<OBNListBindingType>();
        foreach (UXListParameter listParameter in outPlug.ListParameters)
        {
          OBNListBindingType obnListBindingType = new OBNListBindingType();
          obnListBindingType.ptpListParamName = listParameter.Name;
          obnListBindingType.ptpListParamBind = listParameter.Bind;
          obnListBindingType.ptpSelRow = listParameter.SelectedRowsOnlyField;
          List<OBNSimpleBindingType> paramList = new List<OBNSimpleBindingType>();
          foreach (IParameter parameter in listParameter.Parameters)
          {
            OBNSimpleBindingType simpleBinding = this.createSimpleBinding(parameter);
            if (simpleBinding != null)
              paramList.Add(simpleBinding);
          }
          if (paramList.Count > 0)
          {
            obnListBindingType.assSimpleList(paramList);
            if (obnListBindingTypeList == null)
              obnListBindingTypeList = new List<OBNListBindingType>();
            obnListBindingTypeList.Add(obnListBindingType);
          }
        }
        return obnListBindingTypeList;
      }
    }

    public OBNConfigHelper(IFloorplan editableSourceComponent)
    {
      this.m_editableSourceComponent = editableSourceComponent;
      this.m_navigation = (IOBN) new OberonOBN((IModelObject) editableSourceComponent.ControllerNavigation);
    }

    public OBNConfigHelper(UXComponent UIComponent, IFloorplan DTComponent)
    {
      this.m_editableSourceComponent = ModelLayer.ObjectManager.GetFloorplanModel(UIComponent, (FloorplanInfo) null, false, true, DTComponent.Version);
      this.m_navigation = (IOBN) new OberonOBN((IModelObject) this.m_editableSourceComponent.ControllerNavigation);
      new PublicDataModel(UIComponent, this.m_editableSourceComponent).collectAllowedDataModel();
    }

    private OBNSimpleBindingType createSimpleBinding(IParameter parameter)
    {
      if (parameter == null)
        return (OBNSimpleBindingType) null;
      if (!string.IsNullOrEmpty(parameter.Bind))
        return new OBNSimpleBindingType()
        {
          ptpParamName = parameter.Name,
          ptpParamBind = parameter.Bind,
          ptpParamConst = (string) null,
          ptpIsKey = parameter.Key
        };
      if (string.IsNullOrEmpty(parameter.Constant))
        return (OBNSimpleBindingType) null;
      return new OBNSimpleBindingType()
      {
        ptpParamName = parameter.Name,
        ptpParamBind = (string) null,
        ptpParamConst = parameter.Constant,
        ptpIsKey = parameter.Key
      };
    }

    public void configureOBN()
    {
      UCOBNTargetConfig ucobnTargetConfig = (UCOBNTargetConfig) null;
      try
      {
        this.m_Container = new Form();
        this.m_Container.Width = 600;
        this.m_Container.Height = 600;
        ucobnTargetConfig = new UCOBNTargetConfig();
        ucobnTargetConfig.Dock = DockStyle.Fill;
        ucobnTargetConfig.SourceFloorPlan = this.m_editableSourceComponent;
        if (this.m_navigation == null)
          this.m_navigation = (IOBN) new OberonOBN((IModelObject) this.m_editableSourceComponent.ControllerNavigation);
        string navBons = this.m_navigation.NavBONS;
        string navBo = this.m_navigation.NavBO;
        string navBoNode = this.m_navigation.NavBONode;
        string str = this.m_navigation.NavOperation == null ? (string) null : this.m_navigation.NavOperation.Name;
        ucobnTargetConfig.CurrentOBN = this.m_navigation;
        ucobnTargetConfig.Cancel_Clicked += new EventHandler(this.OnnavigationDesigner_CancelClicked);
        ucobnTargetConfig.OK_Clicked += new EventHandler(this.OnNavigationDesigner_OkClicked);
        this.m_Container.Controls.Add((Control) ucobnTargetConfig);
        if (this.m_Container.ShowDialog() == DialogResult.OK)
        {
          this.m_outPort = ucobnTargetConfig.Outport;
          this.m_outportConfig = new VisualInportOutport(this.m_editableSourceComponent.ControllerInterface);
          this.m_outportConfig.CurrentOutport = ucobnTargetConfig.Outport;
          this.m_outportConfig.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
          this.m_outportConfig.VerticalAlignment = VerticalAlignment.Stretch;
          this.m_outportConfig.ApplyDefaultSettingsForExtensionOutport();
          if (navBons != null)
          {
            if (navBo != null)
            {
              if (navBoNode != null)
              {
                if (str != null)
                {
                  if (navBons.Equals(this.m_navigation.NavBONS) && navBo.Equals(this.m_navigation.NavBO) && (navBoNode.Equals(this.m_navigation.NavBONode) && this.m_navigation.NavOperation != null))
                  {
                    if (str.Equals(this.m_navigation.NavOperation.Name))
                      goto label_13;
                  }
                  this.m_outportConfig.CurrentOutport = (IOutPortType) null;
                }
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Navigation configuration operation failed.", ex));
      }
label_13:
      if (ucobnTargetConfig == null)
        return;
      ucobnTargetConfig.Cancel_Clicked -= new EventHandler(this.OnnavigationDesigner_CancelClicked);
      ucobnTargetConfig.OK_Clicked -= new EventHandler(this.OnNavigationDesigner_OkClicked);
    }

    public void configureParams()
    {
      if (this.m_outportConfig == null || this.m_outPort == null || this.m_outportConfig.CurrentOutport != null)
        return;
      this.m_outportConfig.CurrentOutport = this.m_outPort;
    }

    private void OnNavigationDesigner_OkClicked(object sender, EventArgs e)
    {
      if (this.m_Container == null || this.m_Container.IsDisposed)
        return;
      this.m_Container.DialogResult = DialogResult.OK;
      this.m_Container.Close();
    }

    private void OnnavigationDesigner_CancelClicked(object sender, EventArgs e)
    {
      if (this.m_Container == null || this.m_Container.IsDisposed)
        return;
      this.m_Container.DialogResult = DialogResult.Cancel;
      this.m_Container.Close();
    }
  }
}
