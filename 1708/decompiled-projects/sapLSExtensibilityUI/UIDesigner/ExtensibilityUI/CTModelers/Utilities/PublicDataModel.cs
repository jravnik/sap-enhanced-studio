﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities.PublicDataModel
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities
{
  public class PublicDataModel
  {
    private List<string> parameterList;
    private UXComponent UIComponent;
    private IFloorplan editableFloorplan;

    public PublicDataModel(UXComponent UIComponent, IFloorplan editableFloorplan)
    {
      this.UIComponent = UIComponent;
      this.editableFloorplan = editableFloorplan;
    }

    public void collectAllowedDataModel()
    {
      if (LoginManager.Instance.DTMode != DTModes.Partner || this.editableFloorplan == null || this.editableFloorplan.ImplementationController == null)
        return;
      this.parameterList = new List<string>();
      this.checkForExtendedBy(this.UIComponent, this.parameterList);
      this.checkForOutportParameters(this.UIComponent, this.parameterList);
      if (this.parameterList.Count > 0)
        this.filterOutNonRelevantData(this.editableFloorplan.ImplementationController.DataModel.DataFields);
      else
        this.editableFloorplan.ImplementationController.DataModel.DataFields.Clear();
    }

    private string getBindingAttribute(XElement element)
    {
      if (element == null)
        return (string) null;
      if (element.Attributes((XName) "bindingExpression").Any<XAttribute>())
        return element.Attribute((XName) "bindingExpression").Value;
      return (string) null;
    }

    private string getListBinding(XElement column)
    {
      if (column == null)
        return (string) null;
      XElement parent = column.Parent;
      if (parent == null)
        return (string) null;
      return this.getBindingAttribute(parent.Element(HelperFunctions.ns_uxv + "Binding"));
    }

    private List<XElement> getAllReleasedOutports(UXComponent uIComponent)
    {
      List<XElement> xelementList = new List<XElement>();
      foreach (XElement outport in HelperFunctions.getOutports(uIComponent))
      {
        if (outport.Element(HelperFunctions.ns_uxc + "StableAnchor") != null)
          xelementList.Add(outport);
      }
      return xelementList;
    }

    private void checkForParameterBindings(IEnumerable<XElement> parameterElements, List<string> allowedData)
    {
      foreach (XElement parameterElement in parameterElements)
      {
        if (parameterElement.Attributes((XName) "bind").Any<XAttribute>())
        {
          string str = parameterElement.Attribute((XName) "bind").Value;
          if (!allowedData.Contains(str))
            allowedData.Add(str);
        }
      }
    }

    private void checkForOutportParameters(UXComponent uIComponent, List<string> allowedData)
    {
      foreach (XElement allReleasedOutport in this.getAllReleasedOutports(uIComponent))
      {
        XName name = HelperFunctions.ns_uxc + "Parameter";
        allReleasedOutport.Elements(name);
        this.checkForParameterBindings(allReleasedOutport.Elements(HelperFunctions.ns_uxc + "Parameter"), allowedData);
        foreach (XElement element in allReleasedOutport.Elements(HelperFunctions.ns_uxc + "ParameterList"))
        {
          if (element.Attributes((XName) "listBinding").Any<XAttribute>())
          {
            string str1 = element.Attribute((XName) "listBinding").Value;
            if (!str1.StartsWith("/"))
              str1 = "/" + str1;
            List<string> allowedData1 = new List<string>();
            this.checkForParameterBindings(element.Elements(HelperFunctions.ns_uxc + "Parameter"), allowedData1);
            foreach (string str2 in allowedData1)
            {
              string str3 = str1 + (str2.StartsWith(".") ? str2.Substring(1) : str2);
              if (!allowedData.Contains(str3))
                allowedData.Add(str3);
            }
          }
        }
      }
    }

    private string getBindingExpressionList(XElement listElement, List<string> allowedData)
    {
      if (listElement.Name.LocalName.Equals("Column"))
      {
        string str1 = this.getListBinding(listElement);
        if (str1 != null)
        {
          string str2 = (string) null;
          if (str1.StartsWith("."))
            str1 = str1.Substring(1);
          else if (!allowedData.Contains(str1))
            allowedData.Add(str1);
          if (listElement.Parent != null && listElement.Parent.Name.LocalName.Equals("Column"))
            str2 = this.getBindingExpressionList(listElement.Parent, allowedData);
          else if (listElement.Parent != null && listElement.Parent.Name.LocalName.Equals("CompoundField"))
            str2 = this.getBindingExpressionStruc(listElement.Parent, allowedData);
          if (str2 == null)
            return str1;
          string str3 = str1 + str2;
          if (!allowedData.Contains(str3))
            allowedData.Add(str3);
          return str3;
        }
      }
      return (string) null;
    }

    private string getBindingExpressionStruc(XElement strucElement, List<string> allowedData)
    {
      if (!strucElement.Name.LocalName.Equals("CompoundField") || !strucElement.Attributes((XName) "structureBinding").Any<XAttribute>())
        return (string) null;
      string str1 = (string) null;
      string str2 = strucElement.Attribute((XName) "structureBinding").Value;
      if (str2.StartsWith("."))
        str2 = str2.Substring(1);
      else if (!allowedData.Contains(str2))
        allowedData.Add(str2);
      if (strucElement.Parent != null && strucElement.Parent.Name.LocalName.Equals("Column"))
        str1 = this.getBindingExpressionList(strucElement.Parent, allowedData);
      if (str1 == null)
        return str2;
      string str3 = str1 + str2;
      if (!allowedData.Contains(str3))
        allowedData.Add(str3);
      return str1;
    }

    private void getBindingExpression(XElement valueTag, List<string> allowedData)
    {
      if (!valueTag.Attributes((XName) "bindingExpression").Any<XAttribute>())
        return;
      string str1 = valueTag.Attribute((XName) "bindingExpression").Value;
      if (!str1.StartsWith("."))
      {
        if (!allowedData.Contains(str1))
          allowedData.Add(str1);
      }
      else
        str1 = str1.Substring(1);
      XElement parent = valueTag.Parent.Parent;
      string str2 = (string) null;
      if (parent.Name.LocalName.Equals("CompoundField"))
        str2 = this.getBindingExpressionStruc(parent, allowedData);
      else if (parent.Name.LocalName.Equals("Column"))
        str2 = this.getBindingExpressionList(parent, allowedData);
      if (str2 == null)
        return;
      string str3 = str2 + str1;
      if (allowedData.Contains(str3))
        return;
      allowedData.Add(str3);
    }

    private void checkForExtendedBy(UXComponent uIComponent, List<string> allowedData)
    {
      foreach (XElement allExtendedTag in HelperFunctions.getAllExtendedTags(uIComponent))
      {
        foreach (XElement descendant in allExtendedTag.Descendants(HelperFunctions.ns_uxv + "Value"))
          this.getBindingExpression(descendant, allowedData);
        foreach (XElement descendant in allExtendedTag.Descendants(HelperFunctions.ns_uxv + "Text"))
          this.getBindingExpression(descendant, allowedData);
      }
    }

    private void filterOutNonRelevantData(List<IBaseDataElementType> dataFields)
    {
      List<IBaseDataElementType> baseDataElementTypeList = new List<IBaseDataElementType>();
      foreach (IBaseDataElementType dataField in dataFields)
      {
        if (dataField is IDataField)
        {
          if (!this.parameterList.Contains(dataField.FullPath))
            baseDataElementTypeList.Add(dataField);
        }
        else if (dataField is IDataFieldList)
        {
          IDataFieldList dataFieldList = dataField as IDataFieldList;
          this.filterOutNonRelevantData(dataFieldList.DataFields);
          if (dataFieldList.DataFields.Count == 0)
            baseDataElementTypeList.Add(dataField);
        }
        else if (dataField is IDataStructure)
        {
          IDataStructure dataStructure = dataField as IDataStructure;
          this.filterOutNonRelevantData(dataStructure.DataFields);
          if (dataStructure.DataFields.Count == 0)
            baseDataElementTypeList.Add(dataField);
        }
      }
      foreach (IBaseDataElementType baseDataElementType in baseDataElementTypeList)
        dataFields.Remove(baseDataElementType);
    }

    public bool isPublic(string field)
    {
      if (LoginManager.Instance.DTMode != DTModes.Partner)
        return true;
      foreach (string parameter in this.parameterList)
      {
        if (parameter == field)
          return true;
      }
      return false;
    }
  }
}
