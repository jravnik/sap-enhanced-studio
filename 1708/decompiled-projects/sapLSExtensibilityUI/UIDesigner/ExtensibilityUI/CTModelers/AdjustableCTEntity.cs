﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AdjustableCTEntity
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using System.Collections.ObjectModel;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public class AdjustableCTEntity
  {
    public const string STRING_EQUAL = "Equal";
    public const string STRING_NOTEQUAL = "Not Equal";

    public string UISwitchId { get; set; }

    public string UISwitchCondition { get; set; }

    public ObservableCollection<AdjustablePropertiesEntity> AdjustablePropertEntities { get; private set; }

    public AdjustableCTEntity()
    {
      this.AdjustablePropertEntities = new ObservableCollection<AdjustablePropertiesEntity>();
      this.UISwitchCondition = "Equal";
    }
  }
}
