﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.SemanticGroupViewModel
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Mashup.Common;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar;
using System.Collections.Generic;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public class SemanticGroupViewModel : ObservableBase
  {
    private bool isExpand = true;

    public string SemanticCategory { get; set; }

    public string DisplayName { get; set; }

    public bool IsExpand
    {
      get
      {
        return this.isExpand;
      }
      set
      {
        this.isExpand = value;
        this.NotifyPropertyChanged("IsExpand");
      }
    }

    public List<MashupViewModel> Mashups { get; set; }

    public void Terminate()
    {
      if (this.Mashups == null)
        return;
      foreach (MashupViewModel mashup in this.Mashups)
        mashup.Terminate();
      this.Mashups.Clear();
      this.Mashups = (List<MashupViewModel>) null;
    }
  }
}
