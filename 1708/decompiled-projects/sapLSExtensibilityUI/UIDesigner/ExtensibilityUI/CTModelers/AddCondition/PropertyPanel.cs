﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddCondition.PropertyPanel
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UIDesigner.Connector.Services.Modeler;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddCondition
{
  public partial class PropertyPanel : UserControl, IComponentConnector
  {
    private readonly string uiSwitchesText;
    private List<PropertyConfig> selectedConfigList;
    private List<KeyValuePair<TY_UISWITCH, List<PropertyConfig>>> uiSwitchConfig;
    //internal Label equalsUISwitchName;
    //internal ComboBox equalsUISwitchList;
    //internal Label equalsUISwitchDescr;
    //private bool _contentLoaded;

    public List<KeyValuePair<TY_UISWITCH, List<PropertyConfig>>> UISwitchConfig
    {
      get
      {
        return this.uiSwitchConfig;
      }
    }

    public PropertyPanel(List<TY_UISWITCH> uiSwitches, List<PropertyConfig> configList)
    {
      this.InitializeComponent();
      this.selectedConfigList = configList;
      this.uiSwitchConfig = new List<KeyValuePair<TY_UISWITCH, List<PropertyConfig>>>();
      this.uiSwitchesText = "If user has assigned : ";
      this.equalsUISwitchName.Content = (object) this.uiSwitchesText;
      uiSwitches.Insert(0, new TY_UISWITCH()
      {
        ID = string.Empty,
        NAME = "all",
        DESCRIPTION = "effects all users"
      });
      foreach (TY_UISWITCH uiSwitch in uiSwitches)
      {
        this.uiSwitchConfig.Add(new KeyValuePair<TY_UISWITCH, List<PropertyConfig>>(uiSwitch, configList));
        Label label = new Label();
        label.Content = (object) uiSwitch.NAME;
        label.Tag = (object) uiSwitch;
        this.equalsUISwitchList.Items.Add((object) label);
        this.equalsUISwitchList.SelectionChanged += new SelectionChangedEventHandler(this.equalsUISwitchList_SelectionChanged);
      }
      this.equalsUISwitchList.SelectedIndex = 0;
      this.constructHeader();
      this.constructTable();
      this.fillTable();
    }

    private void equalsUISwitchList_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.equalsUISwitchDescr.Content = (object) this.uiSwitchConfig[this.equalsUISwitchList.SelectedIndex].Key.DESCRIPTION;
      this.selectedConfigList = this.uiSwitchConfig[this.equalsUISwitchList.SelectedIndex].Value;
    }

    private void constructHeader()
    {
    }

    private void constructTable()
    {
    }

    private void fillTable()
    {
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addcondition/propertypanel.xaml", UriKind.Relative));
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.equalsUISwitchName = (Label) target;
    //      break;
    //    case 2:
    //      this.equalsUISwitchList = (ComboBox) target;
    //      break;
    //    case 3:
    //      this.equalsUISwitchDescr = (Label) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
