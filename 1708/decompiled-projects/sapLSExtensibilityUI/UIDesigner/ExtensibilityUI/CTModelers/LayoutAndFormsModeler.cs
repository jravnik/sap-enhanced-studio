﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.LayoutAndFormsModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Connector;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class LayoutAndFormsModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private const string TITLE_STRING = "Change SectionGroup";
    private bool isInitialized;
    private FlexibilityHandler flexHandler;
    private SectionGroupType sectionGroupType;
    private FlexStableAnchorType stableAnchorType;
    private bool originalPersHidden;
    private string changeGroupLabelCTID;
    private string changeGroupVisibilityCTID;
    private string originSectionGroupText;
    private List<LayoutAndFormsModeler.SelectedItem> selectedItemList;
    private bool changesApplied;
    //internal TextBox sectionGroupText;
    //internal Label sectionGroupLabel;
    //internal TextBox sectionGroupID;
    //internal Label SectionGroupVisible;
    //internal CheckBox visibleCheckBox;
    //internal ListView itemListView;
    //internal Button okButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return false;
      }
    }

    public LayoutAndFormsModeler()
    {
      this.isInitialized = false;
      this.InitializeComponent();
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.sectionGroupType = this.ExtensibleModel as SectionGroupType;
      if (this.sectionGroupType == null)
        throw new ApplicationException("Selected item is not a SectionGroup, it is type of " + (object) this.ExtensibleModel.GetType());
      this.originalPersHidden = this.sectionGroupType.persHiddenSpecified && this.sectionGroupType.persHidden;
      this.visibleCheckBox.IsChecked = new bool?(!this.originalPersHidden);
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      foreach (FlexStableAnchorType allStableAnchor in this.flexHandler.GetAllStableAnchorList(this.sectionGroupType))
      {
        if (this.Anchor.xrepPath == allStableAnchor.xrepPath)
        {
          this.stableAnchorType = allStableAnchor;
          break;
        }
      }
      if (this.stableAnchorType == null)
        return;
      this.sectionGroupText.Text = this.getText(this.sectionGroupType.SectionGroupName);
      this.originSectionGroupText = this.getText(this.sectionGroupType.SectionGroupName);
      this.sectionGroupID.Text = this.sectionGroupType.id;
      this.fillListBox();
      this.Title = "Change SectionGroup";
      this.isInitialized = true;
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      foreach (LayoutAndFormsModeler.SelectedItem selectedItem in this.selectedItemList)
      {
        if (!string.IsNullOrEmpty(selectedItem.TextCTID))
        {
          string usedTextUuid;
          this.flexHandler.RemoveFieldLabelChange(selectedItem.TextCTID, selectedItem.Item, this.UIComponent, selectedItem.OriginText, out usedTextUuid);
        }
        if (!string.IsNullOrEmpty(selectedItem.VisibilityCTID))
          this.flexHandler.RemoveFieldVisibility(selectedItem.VisibilityCTID, selectedItem.Item, selectedItem.OriginPersHidden);
      }
      if (!string.IsNullOrEmpty(this.changeGroupVisibilityCTID))
        this.flexHandler.RemoveSectionGroupVisibility(this.changeGroupVisibilityCTID, this.sectionGroupType, this.originalPersHidden);
      this.removeGroupTextChanged();
      this.IsChangeApplied = false;
    }

    private void fillListBox()
    {
      this.selectedItemList = new List<LayoutAndFormsModeler.SelectedItem>();
      foreach (SectionGroupItemType sectionGroupItemType in this.sectionGroupType.SectionGroupItem)
      {
        Grid grid = new Grid();
        grid.ColumnDefinitions.Add(new ColumnDefinition()
        {
          Width = new GridLength(120.0, GridUnitType.Pixel)
        });
        grid.ColumnDefinitions.Add(new ColumnDefinition()
        {
          Width = new GridLength(120.0, GridUnitType.Pixel)
        });
        grid.ColumnDefinitions.Add(new ColumnDefinition()
        {
          Width = new GridLength(50.0, GridUnitType.Pixel)
        });
        TextBox textBox1 = new TextBox();
        textBox1.IsReadOnly = true;
        textBox1.Text = sectionGroupItemType.id;
        textBox1.HorizontalAlignment = HorizontalAlignment.Left;
        textBox1.Width = 120.0;
        textBox1.Tag = (object) sectionGroupItemType.id;
        textBox1.SetValue(Grid.ColumnProperty, (object) 0);
        grid.Children.Add((UIElement) textBox1);
        TextBox textBox2 = new TextBox();
        textBox2.Text = this.getText(sectionGroupItemType.Label);
        textBox2.HorizontalAlignment = HorizontalAlignment.Left;
        textBox2.Width = 120.0;
        textBox2.Tag = (object) sectionGroupItemType.id;
        textBox2.SetValue(Grid.ColumnProperty, (object) 0);
        textBox2.TextChanged += new TextChangedEventHandler(this.itemTitleTextBox_TextChanged);
        grid.Children.Add((UIElement) textBox2);
        CheckBox checkBox = new CheckBox();
        checkBox.HorizontalAlignment = HorizontalAlignment.Center;
        checkBox.IsChecked = new bool?(!sectionGroupItemType.persHidden);
        checkBox.Tag = (object) sectionGroupItemType.persHidden;
        checkBox.Width = 50.0;
        checkBox.Tag = (object) sectionGroupItemType.id;
        checkBox.SetValue(Grid.ColumnProperty, (object) 1);
        checkBox.Checked += new RoutedEventHandler(this.itemTitleTextBox_Checked);
        checkBox.Unchecked += new RoutedEventHandler(this.itemTitleTextBox_UnChecked);
        grid.Children.Add((UIElement) checkBox);
        ListViewItem listViewItem = new ListViewItem();
        listViewItem.Content = (object) grid;
        this.itemListView.Items.Add((object) listViewItem);
        this.selectedItemList.Add(new LayoutAndFormsModeler.SelectedItem()
        {
          Item = sectionGroupItemType,
          OriginPersHidden = sectionGroupItemType.persHidden,
          OriginText = textBox2.Text
        });
      }
    }

    private void changeVisibility(bool persHidden)
    {
      if (!this.isInitialized)
        return;
      if (this.originalPersHidden == persHidden && !string.IsNullOrEmpty(this.changeGroupVisibilityCTID))
        this.flexHandler.RemoveSectionGroupVisibility(this.changeGroupVisibilityCTID, this.sectionGroupType, this.originalPersHidden);
      else
        this.changeGroupVisibilityCTID = this.flexHandler.ChangeSectionGroupVisibility((FlexBaseAnchorType) this.stableAnchorType, this.sectionGroupType, persHidden);
    }

    private void changeItemVisibility(ref LayoutAndFormsModeler.SelectedItem selectedItemInfo, bool persHidden)
    {
      if (!this.isInitialized || selectedItemInfo.Item == null)
        return;
      if (selectedItemInfo.OriginPersHidden == persHidden)
      {
        this.flexHandler.RemoveFieldVisibility(selectedItemInfo.VisibilityCTID, selectedItemInfo.Item, selectedItemInfo.OriginPersHidden);
        selectedItemInfo.VisibilityCTID = (string) null;
      }
      else
        selectedItemInfo.VisibilityCTID = this.flexHandler.ChangeFieldVisibility((FlexBaseAnchorType) this.stableAnchorType, selectedItemInfo.Item, persHidden);
    }

    private void changeItemText(ref LayoutAndFormsModeler.SelectedItem selectedItemInfo, string newText)
    {
      if (!this.isInitialized || selectedItemInfo.Item == null)
        return;
      string usedTextUuid;
      this.flexHandler.RemoveFieldLabelChange(selectedItemInfo.TextCTID, selectedItemInfo.Item, this.UIComponent, selectedItemInfo.OriginText, out usedTextUuid);
      if (!(selectedItemInfo.OriginText != newText))
        return;
      selectedItemInfo.TextCTID = this.flexHandler.ChangeFieldLabel(selectedItemInfo.Item, this.UIComponent, (FlexBaseAnchorType) this.stableAnchorType, newText, LoginManager.Instance.Language, (Dictionary<string, string>) null, out usedTextUuid);
    }

    private void removeItemTextChanged(string ctID, SectionGroupItemType item, string newLabel)
    {
      if (string.IsNullOrEmpty(ctID))
        return;
      string usedTextUuid;
      this.flexHandler.RemoveFieldLabelChange(ctID, item, this.UIComponent, newLabel, out usedTextUuid);
    }

    private void removeGroupTextChanged()
    {
      if (string.IsNullOrEmpty(this.changeGroupLabelCTID))
        return;
      string usedTextUuid;
      this.flexHandler.RemoveGroupLabelChange(this.changeGroupLabelCTID, this.sectionGroupType, this.UIComponent, this.originSectionGroupText, out usedTextUuid);
    }

    private LayoutAndFormsModeler.SelectedItem selectListItem(object sender)
    {
      FrameworkElement frameworkElement = sender as FrameworkElement;
      if (frameworkElement != null)
      {
        string str = frameworkElement.Tag.ToString();
        if (!string.IsNullOrEmpty(str))
        {
          for (int index = 0; index < this.selectedItemList.Count; ++index)
          {
            LayoutAndFormsModeler.SelectedItem selectedItem = this.selectedItemList[index];
            if (selectedItem.Item.id == str)
            {
              this.itemListView.SelectedIndex = index;
              return selectedItem;
            }
          }
        }
      }
      return new LayoutAndFormsModeler.SelectedItem();
    }

    private void itemTitleTextBox_Checked(object sender, RoutedEventArgs e)
    {
      LayoutAndFormsModeler.SelectedItem selectedItemInfo = this.selectListItem(sender);
      this.changeItemVisibility(ref selectedItemInfo, false);
    }

    private void itemTitleTextBox_UnChecked(object sender, RoutedEventArgs e)
    {
      LayoutAndFormsModeler.SelectedItem selectedItemInfo = this.selectListItem(sender);
      this.changeItemVisibility(ref selectedItemInfo, true);
    }

    private void itemTitleTextBox_TextChanged(object sender, TextChangedEventArgs e)
    {
      TextBox textBox = sender as TextBox;
      if (textBox == null)
        return;
      LayoutAndFormsModeler.SelectedItem selectedItemInfo = this.selectListItem(sender);
      this.changeItemText(ref selectedItemInfo, textBox.Text);
    }

    private void visibleCheckBox_Checked(object sender, RoutedEventArgs e)
    {
      this.changeVisibility(false);
    }

    private void visibleCheckBox_Unchecked(object sender, RoutedEventArgs e)
    {
      this.changeVisibility(true);
    }

    private void sectionGroupText_TextChanged(object sender, TextChangedEventArgs e)
    {
      if (!this.isInitialized)
        return;
      if (this.originSectionGroupText == this.sectionGroupText.Text)
      {
        this.removeGroupTextChanged();
      }
      else
      {
        string usedTextUuid;
        this.flexHandler.RemoveGroupLabelChange(this.changeGroupLabelCTID, this.sectionGroupType, this.UIComponent, this.sectionGroupText.Text, out usedTextUuid);
        this.changeGroupLabelCTID = this.flexHandler.ChangeGroupLabel(this.sectionGroupType, this.UIComponent, (FlexBaseAnchorType) this.stableAnchorType, this.sectionGroupText.Text, LoginManager.Instance.Language, (Dictionary<string, string>) null, out usedTextUuid);
      }
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      this.changesApplied = true;
      this.IsChangeApplied = true;
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/layoutandformsmodeler.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.sectionGroupText = (TextBox) target;
    //      this.sectionGroupText.TextChanged += new TextChangedEventHandler(this.sectionGroupText_TextChanged);
    //      break;
    //    case 2:
    //      this.sectionGroupLabel = (Label) target;
    //      break;
    //    case 3:
    //      this.sectionGroupID = (TextBox) target;
    //      break;
    //    case 4:
    //      this.SectionGroupVisible = (Label) target;
    //      break;
    //    case 5:
    //      this.visibleCheckBox = (CheckBox) target;
    //      this.visibleCheckBox.Checked += new RoutedEventHandler(this.visibleCheckBox_Checked);
    //      this.visibleCheckBox.Unchecked += new RoutedEventHandler(this.visibleCheckBox_Unchecked);
    //      break;
    //    case 6:
    //      this.itemListView = (ListView) target;
    //      break;
    //    case 7:
    //      this.okButton = (Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 8:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    internal class SelectedItem
    {
      public SectionGroupItemType Item;
      public bool OriginPersHidden;
      public string OriginText;
      public string VisibilityCTID;
      public string TextCTID;
    }
  }
}
