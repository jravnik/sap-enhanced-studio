﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddMashupsBaseControl
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base.Exceptions;
using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Mashup.Common;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddMashupsBaseControl : UserControl, IComponentConnector, IStyleConnector
  {
    public static readonly DependencyProperty TemplateWorkaroundProperty = DependencyProperty.RegisterAttached("TemplateWorkaround", typeof (int), typeof (AddMashupsBaseControl), (PropertyMetadata) null);
    private bool firstButton = true;
    public AddMashupsBaseControl.ApplyEventHandler ApplyClicked;
    public AddMashupsBaseControl.CancelEventHandler CancelClicked;
    public AddMashupsBaseControl.MashupCheckedEventHandler MashupChecked;
    public AddMashupsBaseControl.MashupUncheckedEventHandler MashupUnchecked;
    public AddMashupsBaseControl.MashupAppearanceChangedEventHandler MashupAppearanceChanged;
    public AddMashupsBaseControl.MashupFullColumnSpanCheckedEventHandler MashupFullColumnChecked;
    public AddMashupsBaseControl.MashupFullColumnSpanUnCheckedEventHandler MashupFullColumnUnchecked;
    public AddMashupsBaseControl.CustomRowClickedEventHandler CustomRowClicked;
    public AddMashupsBaseControl.MashupDynamicParameterChangedEventHander DynamicParameterChanged;
    public AddMashupsBaseControl.MashupVisibilityBindingChangedEventHandler VisibilityBindingChanged;
    public AddMashupsBaseControl.MashupDynOutputBindingChangedEventHandler DynOutputBindingChanged;
    protected Control prevSelectedItem;
    //internal Grid Caption;
    //internal Rectangle HorizontalSeparator1;
    //internal Rectangle HorizontalSeparator2;
    //internal Rectangle BackgroundRectangle;
    //internal Rectangle BackgroundGradient;
    //internal TextBlock MashupNameHeader;
    //internal TextBlock MashupIDHeader;
    //internal Rectangle VerticalSeparator1;
    //internal Rectangle VerticalSeparator2;
    //internal Rectangle VerticalSeparator3;
    //internal Rectangle VerticalSeparator4;
    //internal Rectangle VerticalSeparator6;
    //internal TextBlock MashupVisibleHeader;
    //internal ScrollViewer MashupScrollViewer;
    //internal ItemsControl MashupGroups;
    //internal StackPanel ServiceDetailPanel;
    //internal TextBlock StaticTextProperties;
    //internal TextBlock MashupType;
    //internal TextBlock StaticTextMashupType;
    //internal TextBlock PortBinding;
    //internal TextBlock StaticTextPortBinding;
    //internal TextBlock MashupName;
    //internal TextBlock StaticTextMashupName;
    //internal TextBlock MashupDisplayName;
    //internal TextBox FiedLabelInput;
    //internal TextBlock MashupAppearance;
    //internal ComboBox Appearances;
    //internal Grid FullColumnSpanGrid;
    //internal TextBlock MashupFullColumnSpan;
    //internal CheckBox MashupFullColumnSpanChkbox;
    //internal Grid VisibilityBindingGrid;
    //internal TextBlock VisibilityBinding;
    //internal ComboBox VisibilibyBindingValueHelpList;
    //internal TextBlock ExtensionBindingHeader;
    //internal ItemsControl DynParamsControl;
    //internal TextBlock ExtensionOutputHeader;
    //internal ItemsControl DynOutputsControl;
    //internal Button ButtonApply;
    //internal Button ButtonCancel;
    //private bool _contentLoaded;

    protected MashupViewModel CurrentPipe { get; private set; }

    public AddMashupsBaseControl()
    {
      this.InitializeComponent();
    }

    private void ButtonApply_Click(object sender, RoutedEventArgs e)
    {
      if (this.ApplyClicked == null)
        return;
      this.ApplyClicked(sender, e);
    }

    private void ButtonCancel_Click(object sender, RoutedEventArgs e)
    {
      if (this.CancelClicked == null)
        return;
      this.CancelClicked(sender, e);
    }

    private void MashupVisibilityCheckbox_Checked(object sender, RoutedEventArgs e)
    {
      if (this.MashupChecked == null)
        return;
      this.MashupChecked(sender, e);
    }

    private void MashupVisibilityCheckbox_Unchecked(object sender, RoutedEventArgs e)
    {
      if (this.MashupUnchecked == null)
        return;
      this.MashupUnchecked(sender, e);
    }

    public List<UXPortType> GetMatchedPorts(UXComponent component, string anchorXrepPath)
    {
      List<UXPortType> uxPortTypeList = new List<UXPortType>();
      OutPortsType outPorts = component.Interface.OutPorts;
      if (outPorts != null && outPorts.OutPort != null)
      {
        foreach (UXOutPortType uxOutPortType in outPorts.OutPort)
        {
          if (!string.IsNullOrEmpty(uxOutPortType.portTypePackage) && uxOutPortType.portTypePackage == "/SAP_BYD_TF/Mashups/globalmashupporttypes.PTP.uicomponent" && (uxOutPortType.StableAnchor != null && uxOutPortType.StableAnchor.Count > 0) && (uxOutPortType.ReferencedAnchor != null && uxOutPortType.ReferencedAnchor.Count > 0))
          {
            foreach (FlexBaseAnchorType flexBaseAnchorType in uxOutPortType.ReferencedAnchor)
            {
              if (flexBaseAnchorType.xrepPath == anchorXrepPath)
                uxPortTypeList.Add((UXPortType) uxOutPortType);
            }
          }
        }
      }
      return uxPortTypeList;
    }

    public List<string> GetMashupCategories(List<UXPortType> ports)
    {
      MashupRegistryType instance = MashupRegistry.Instance;
      return MashupRegistry.GetMashupCategoriesByPorts(ports).Select<MashupCategoryType, string>((Func<MashupCategoryType, string>) (mashupCategory => mashupCategory.name)).ToList<string>();
    }

    public static string InsertWebWidgetMashup(MashupViewModel mashupInsertionInfo, FlexibilityHandler flexHandler, UXComponent component, string componentPath, FlexBaseAnchorType stableAnchorType)
    {
      return AddMashupsBaseControl.InsertMashup(mashupInsertionInfo, flexHandler, component, componentPath, stableAnchorType);
    }

    public static string InsertMashup(MashupViewModel mashupInsertionInfo, FlexibilityHandler flexHandler, UXComponent component, string componentPath, FlexBaseAnchorType stableAnchor)
    {
      bool isDynamic = mashupInsertionInfo.IsDynamic;
      MashupFlexibilityHandler flexibilityHandler = flexHandler.GetMashupFlexibilityHandler();
      FlexBaseAnchorType floorPlanAnchor = (flexHandler.GetFloorPlanAnchor(component) ?? flexHandler.GetOverviewAnchor(component)) ?? flexHandler.GetAssignedObjectAnchor(component);
      if (flexibilityHandler != null)
      {
        bool flag = ((MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupInsertionInfo.ViewStyle, false)).Equals((object) MashupViewStyleType.inScreen);
        string mashupPath = MashupConstants.GetMashupPath(mashupInsertionInfo.PipeID);
        bool isWritebackCase = !string.IsNullOrEmpty(mashupInsertionInfo.MashupOutPlug);
        bool isWithoutoutPort = string.IsNullOrEmpty(mashupInsertionInfo.MashupInPlug);
        OnNavigateType outOnNavigate = (OnNavigateType) null;
        OnNavigateType inOnNavigate = (OnNavigateType) null;
        if (!isWithoutoutPort && string.IsNullOrEmpty(mashupInsertionInfo.SourceOutPlug))
          isWithoutoutPort = true;
        if (isWritebackCase && string.IsNullOrEmpty(mashupInsertionInfo.TargetInPlug))
          isWritebackCase = false;
        EmbeddedComponentType embeddedComponent = InsertMashupHelper.ConstructEmbeddedComponent(mashupPath, mashupInsertionInfo.PipeID, mashupInsertionInfo.SourceOutPlug, mashupInsertionInfo.TargetInPlug, mashupInsertionInfo.DisplayName, mashupInsertionInfo.AppearanceKey, mashupInsertionInfo.FullColumnSpan, mashupInsertionInfo);
        UXOutPortType sourceOutPlug = (UXOutPortType) null;
        FlexBaseAnchorType leadingAnchor = (FlexBaseAnchorType) null;
        if (!isWithoutoutPort)
          AddMashupsBaseControl.ProcessOutPort(mashupInsertionInfo, flexHandler, component, stableAnchor, isDynamic, embeddedComponent, out outOnNavigate, out leadingAnchor, out sourceOutPlug);
        FlexBaseAnchorType inPortAnchor = (FlexBaseAnchorType) null;
        if (isWritebackCase)
          AddMashupsBaseControl.ProcessWritebackCase(mashupInsertionInfo, flexHandler, component, embeddedComponent, out inOnNavigate, ref inPortAnchor);
        List<string> extRefs = (List<string>) null;
        if (mashupInsertionInfo.DynamicBindings != null && mashupInsertionInfo.DynamicBindings.Count > 0)
        {
          extRefs = new List<string>();
          foreach (KeyValuePair<string, ExtBindingOption> dynamicBinding in mashupInsertionInfo.DynamicBindings)
            extRefs.Add(dynamicBinding.Value.ExternalReference);
        }
        string appearance = mashupInsertionInfo.Appearance;
        if (string.IsNullOrEmpty(appearance))
          throw new InvalidArgumentException("appearance is null or empty.");
        if (stableAnchor == null)
          throw new InvalidArgumentException("stableAnchorType is null.");
        if (!flag)
        {
          string str = AddMashupsBaseControl.InsertClickableMashup(mashupInsertionInfo, componentPath, stableAnchor, leadingAnchor, extRefs, flexibilityHandler, inOnNavigate, embeddedComponent, appearance, inPortAnchor, sourceOutPlug, outOnNavigate, isWritebackCase);
          if (str != null)
            return str;
        }
        else
        {
          string str = AddMashupsBaseControl.InsertMashupAsPane(mashupInsertionInfo, componentPath, stableAnchor, isDynamic, isWritebackCase, leadingAnchor, floorPlanAnchor, inPortAnchor, isWithoutoutPort, outOnNavigate, appearance, extRefs, flexibilityHandler, inOnNavigate, embeddedComponent);
          if (str != null)
            return str;
        }
      }
      return (string) null;
    }

    private static void ProcessWritebackCase(MashupViewModel mashupInsertionInfo, FlexibilityHandler flexHandler, UXComponent component, EmbeddedComponentType embeddedComponent, out OnNavigateType inOnNavigate, ref FlexBaseAnchorType inPortAnchor)
    {
      if (mashupInsertionInfo.TargetInPlug != mashupInsertionInfo.PipeIDWithoutSlashes)
      {
        UXInPortType targetInPortByName = component.FindTargetInPortByName(mashupInsertionInfo.TargetInPlug);
        if (targetInPortByName == null)
          throw new InvalidOperationException("Target InPlug is null.");
        List<FlexBaseAnchorType> inPortAnchor1 = flexHandler.GetInPortAnchor(targetInPortByName);
        if (inPortAnchor1 == null || inPortAnchor1.Count == 0)
          throw new InvalidOperationException("Mashup could not be inserted because inport anchors are missing.");
        inPortAnchor = inPortAnchor1[0];
        inOnNavigate = InsertMashupHelper.ConstructOnNavigate(embeddedComponent.targetComponentID, mashupInsertionInfo.MashupOutPlug, "this", targetInPortByName.name, (UXPortType) targetInPortByName, (MashupViewModel) null);
      }
      else
        inOnNavigate = InsertMashupHelper.ConstructOnNavigate(embeddedComponent.targetComponentID, mashupInsertionInfo.MashupOutPlug, "this", mashupInsertionInfo.PipeIDWithoutSlashes, (UXPortType) null, mashupInsertionInfo);
    }

    private static void ProcessOutPort(MashupViewModel mashupInsertionInfo, FlexibilityHandler flexHandler, UXComponent component, FlexBaseAnchorType stableAnchor, bool isDynamic, EmbeddedComponentType embeddedComponent, out OnNavigateType outOnNavigate, out FlexBaseAnchorType leadingAnchor, out UXOutPortType sourceOutPlug)
    {
      sourceOutPlug = component.FindSourceOutPortByName(mashupInsertionInfo.SourceOutPlug);
      if (sourceOutPlug == null)
        throw new InvalidOperationException("Source OutPlug is null.");
      if (isDynamic)
      {
        leadingAnchor = stableAnchor;
      }
      else
      {
        List<FlexBaseAnchorType> outPortAnchor = flexHandler.GetOutPortAnchor(sourceOutPlug);
        if (outPortAnchor == null || outPortAnchor.Count == 0)
          throw new InvalidOperationException("Mashup could not be inserted because outport anchors are missing.");
        leadingAnchor = outPortAnchor[0];
      }
      outOnNavigate = InsertMashupHelper.ConstructOnNavigate("this", sourceOutPlug.name, embeddedComponent.embedName, mashupInsertionInfo.MashupInPlug, (UXPortType) sourceOutPlug, mashupInsertionInfo);
    }

    private static string InsertMashupAsPane(MashupViewModel mashupInsertionInfo, string componentPath, FlexBaseAnchorType stableAnchor, bool isDynamic, bool isWritebackCase, FlexBaseAnchorType leadingAnchor, FlexBaseAnchorType floorPlanAnchor, FlexBaseAnchorType inPortAnchor, bool isWithoutoutPort, OnNavigateType outOnNavigate, string appearance, List<string> extRefs, MashupFlexibilityHandler mashupFlexHandler, OnNavigateType inOnNavigate, EmbeddedComponentType embeddedComponent)
    {
      if (string.IsNullOrEmpty(mashupInsertionInfo.ContainerID) && !appearance.Equals(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ViewSwitch)))
        throw new InvalidArgumentException("ContainerID to be insert is null or empty.");
      if (appearance.Equals(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.NewPaneContainer)))
        return InsertMashupHelper.AddMashupNewPaneContainer(mashupInsertionInfo, isWithoutoutPort, outOnNavigate, inOnNavigate, embeddedComponent, extRefs, mashupFlexHandler, componentPath, stableAnchor);
      if (appearance.Equals(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.NearPaneContainer)))
      {
        if (string.IsNullOrEmpty(mashupInsertionInfo.RefPaneContainerID))
          throw new InvalidArgumentException("ReferencePaneContainerID is null or empty.");
        FlexBaseAnchorType paneContainerAnchor = !isDynamic ? stableAnchor : InsertMashupHelper.ConstructLeadingAnchor(mashupInsertionInfo, floorPlanAnchor);
        return InsertMashupHelper.AddMashupNearPaneContainer(mashupInsertionInfo, isWritebackCase, outOnNavigate, inOnNavigate, embeddedComponent, leadingAnchor, inPortAnchor, extRefs, paneContainerAnchor, mashupFlexHandler, componentPath);
      }
      if (!appearance.Equals(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ViewSwitch)))
        return (string) null;
      if (leadingAnchor == null)
        leadingAnchor = stableAnchor;
      return InsertMashupHelper.AddMashupViewSwitch(componentPath, mashupFlexHandler, leadingAnchor, stableAnchor, embeddedComponent, mashupInsertionInfo, outOnNavigate, inOnNavigate);
    }

    private static string InsertClickableMashup(MashupViewModel mashupInsertionInfo, string componentPath, FlexBaseAnchorType stableAnchor, FlexBaseAnchorType leadingAnchor, List<string> extRefs, MashupFlexibilityHandler mashupFlexHandler, OnNavigateType inOnNavigate, EmbeddedComponentType embeddedComponent, string appearance, FlexBaseAnchorType inPortAnchor, UXOutPortType sourceOutPlug, OnNavigateType outOnNavigate, bool isWritebackCase)
    {
      UXEventHandlerType eventHandler = InsertMashupHelper.ConstructFireOutPlugEventHandler(sourceOutPlug.name, embeddedComponent.embedName);
      if (appearance.Equals(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.FunctionBar)))
        return InsertMashupHelper.AddMashupButtonintoFunctionBar(mashupInsertionInfo, isWritebackCase, outOnNavigate, inOnNavigate, embeddedComponent, leadingAnchor, inPortAnchor, extRefs, eventHandler, mashupFlexHandler, stableAnchor, componentPath);
      if (appearance.Equals(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ListToolbar)))
        return InsertMashupHelper.AddMashupButtonIntoListToolbar(mashupInsertionInfo, isWritebackCase, outOnNavigate, inOnNavigate, embeddedComponent, leadingAnchor, inPortAnchor, extRefs, eventHandler, stableAnchor, mashupFlexHandler, componentPath);
      if (appearance.Equals(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.SectionLink)))
        return InsertMashupHelper.AddMashupLinkIntoSectionGroup(mashupInsertionInfo, isWritebackCase, outOnNavigate, inOnNavigate, embeddedComponent, leadingAnchor, inPortAnchor, extRefs, eventHandler, stableAnchor, mashupFlexHandler, componentPath);
      return (string) null;
    }

    public void ApplyChangesDoneToModels(ComponentKey key, UXComponent model, List<ChangeTransaction> cts, string language)
    {
      foreach (ChangeTransaction ct in cts)
        ChangeTransactionHandler.Instance.ApplyChangeChangeTransaction(key, model, ct, language);
    }

    public static void LoadExistingChangeTransaction(string componentPath, UXComponent uxComponent, string anchorPath, ref Dictionary<string, ExistingMashupsInfo> existingMashups, ref Dictionary<string, string> unSaveCTChangeHistorys)
    {
      List<ModeledChangeTransactionInfo> publishedCTList;
      List<ModeledChangeTransactionInfo> savedCTList;
      XRepositoryProxy.Instance.GetChangeTransactionForAnchor(anchorPath, out publishedCTList, out savedCTList);
      if (publishedCTList != null && publishedCTList.Count > 0)
      {
        foreach (ModeledChangeTransactionInfo changeTransactionInfo in publishedCTList)
          AddMashupsBaseControl.CheckChangeTransaction(changeTransactionInfo.Content, ref existingMashups);
      }
      if (savedCTList != null && savedCTList.Count > 0)
      {
        foreach (ModeledChangeTransactionInfo changeTransactionInfo in savedCTList)
          AddMashupsBaseControl.CheckChangeTransaction(changeTransactionInfo.Content, ref existingMashups);
      }
      List<ModeledChangeTransactionInfo> changeTransactions = ChangeTransactionManager.Instance.GetUnsavedChangeTransactions(componentPath, uxComponent, anchorPath);
      if (changeTransactions == null || changeTransactions.Count <= 0)
        return;
      foreach (ModeledChangeTransactionInfo changeTransactionInfo in changeTransactions)
        AddMashupsBaseControl.CheckUnsaveChangeTransaction(changeTransactionInfo.Content, ref existingMashups, ref unSaveCTChangeHistorys);
    }

    private static void CheckChangeTransaction(string content, ref Dictionary<string, ExistingMashupsInfo> existingMashups)
    {
      using (MemoryStream memoryStream = new MemoryStream(new UTF8Encoding().GetBytes(content)))
      {
        object deserializedObject;
        if (!XmlHelper.GetInstance().DeserializeObject(typeof (ChangeTransaction), (Stream) memoryStream, out deserializedObject))
          return;
        ChangeTransaction changeTransaction = deserializedObject as ChangeTransaction;
        if (changeTransaction == null || changeTransaction.UsedAnchor == null || (changeTransaction.UsedAnchor.AddMashup == null || changeTransaction.UsedAnchor.AddMashup.Count <= 0))
          return;
        string mashupPipeId = changeTransaction.UsedAnchor.AddMashup[0].mashupPipeID;
        FlexAddEmbeddedComponentType embeddedComponent = changeTransaction.UsedAnchor.AddMashup[0].AddEmbeddedComponent;
        string constant1 = embeddedComponent.EmbeddedComponent.Parameter.Where<UXParameterType>((Func<UXParameterType, bool>) (p => p.name.Equals("MashupAppearanceKey"))).FirstOrDefault<UXParameterType>().constant;
        string str = string.Empty;
        if (embeddedComponent.EmbeddedComponent.Parameter.Where<UXParameterType>((Func<UXParameterType, bool>) (p => p.name.Equals("VisibilityBinding"))).Count<UXParameterType>() > 0)
          str = embeddedComponent.EmbeddedComponent.Parameter.Where<UXParameterType>((Func<UXParameterType, bool>) (p => p.name.Equals("VisibilityBinding"))).FirstOrDefault<UXParameterType>().constant;
        string constant2 = embeddedComponent.EmbeddedComponent.Parameter.Where<UXParameterType>((Func<UXParameterType, bool>) (p => p.name.Equals("TargetInPort"))).FirstOrDefault<UXParameterType>().constant;
        bool flag = false;
        FlexAddPaneContainerType paneContainerType = changeTransaction.UsedAnchor.AddMashup[0].Item as FlexAddPaneContainerType;
        if (paneContainerType != null)
          flag = paneContainerType.fullColumnSpan;
        Dictionary<string, ExtBindingOption> dictionary = new Dictionary<string, ExtBindingOption>();
        foreach (UXParameterType uxParameterType in changeTransaction.UsedAnchor.AddMashup[0].AddEmbeddedComponent.EmbeddedComponent.Parameter)
        {
          if (uxParameterType.bind != null)
            dictionary[uxParameterType.name] = new ExtBindingOption()
            {
              ParameterName = uxParameterType.bind
            };
        }
        existingMashups[mashupPipeId] = new ExistingMashupsInfo()
        {
          AppearanceKey = constant1,
          FullColumnSpan = flag,
          DynamicBindings = dictionary,
          VisibilityBindingExp = str,
          TargetInplug = constant2
        };
      }
    }

    private static void CheckUnsaveChangeTransaction(string content, ref Dictionary<string, ExistingMashupsInfo> existingMashups, ref Dictionary<string, string> unSaveCTChangeHistorys)
    {
      using (MemoryStream memoryStream = new MemoryStream(new UTF8Encoding().GetBytes(content)))
      {
        object deserializedObject;
        if (!XmlHelper.GetInstance().DeserializeObject(typeof (ChangeTransaction), (Stream) memoryStream, out deserializedObject))
          return;
        ChangeTransaction changeTransaction = deserializedObject as ChangeTransaction;
        if (changeTransaction == null || changeTransaction.UsedAnchor == null || (changeTransaction.UsedAnchor.AddMashup == null || changeTransaction.UsedAnchor.AddMashup.Count <= 0))
          return;
        string mashupPipeId = changeTransaction.UsedAnchor.AddMashup[0].mashupPipeID;
        FlexAddEmbeddedComponentType embeddedComponent = changeTransaction.UsedAnchor.AddMashup[0].AddEmbeddedComponent;
        string constant1 = embeddedComponent.EmbeddedComponent.Parameter.Where<UXParameterType>((Func<UXParameterType, bool>) (p => p.name.Equals("MashupAppearanceKey"))).FirstOrDefault<UXParameterType>().constant;
        string str1 = string.Empty;
        if (embeddedComponent.EmbeddedComponent.Parameter.Where<UXParameterType>((Func<UXParameterType, bool>) (p => p.name.Equals("VisibilityBinding"))).Count<UXParameterType>() > 0)
          str1 = embeddedComponent.EmbeddedComponent.Parameter.Where<UXParameterType>((Func<UXParameterType, bool>) (p => p.name.Equals("VisibilityBinding"))).FirstOrDefault<UXParameterType>().constant;
        string constant2 = embeddedComponent.EmbeddedComponent.Parameter.Where<UXParameterType>((Func<UXParameterType, bool>) (p => p.name.Equals("TargetInPort"))).FirstOrDefault<UXParameterType>().constant;
        bool flag = false;
        FlexAddPaneContainerType paneContainerType = changeTransaction.UsedAnchor.AddMashup[0].Item as FlexAddPaneContainerType;
        if (paneContainerType != null)
          flag = paneContainerType.fullColumnSpan;
        Dictionary<string, ExtBindingOption> dictionary = new Dictionary<string, ExtBindingOption>();
        foreach (UXParameterType uxParameterType in changeTransaction.UsedAnchor.AddMashup[0].AddEmbeddedComponent.EmbeddedComponent.Parameter)
        {
          if (uxParameterType.bind != null)
            dictionary[uxParameterType.name] = new ExtBindingOption()
            {
              ParameterName = uxParameterType.bind
            };
        }
        existingMashups[mashupPipeId] = new ExistingMashupsInfo()
        {
          AppearanceKey = constant1,
          FullColumnSpan = flag,
          DynamicBindings = dictionary,
          VisibilityBindingExp = str1,
          TargetInplug = constant2
        };
        OnNavigateType onNavigateType = (OnNavigateType) null;
        if (changeTransaction.UsedAnchor.AddMashup[0].AddNavigation != null && changeTransaction.UsedAnchor.AddMashup[0].AddNavigation.Count > 0)
          onNavigateType = changeTransaction.UsedAnchor.AddMashup[0].AddNavigation[0];
        string str2 = string.Empty;
        if (onNavigateType != null)
          str2 = onNavigateType.outPlug;
        string id = changeTransaction.id;
        string key = mashupPipeId + str2;
        unSaveCTChangeHistorys.Add(key, id);
      }
    }

    public string RevertMashup(MashupViewModel mashupInsertionInfo, FlexibilityHandler flexHandler, string componentPath, FlexBaseAnchorType floorplanAnchor)
    {
      MashupFlexibilityHandler flexibilityHandler = flexHandler.GetMashupFlexibilityHandler();
      if (flexibilityHandler == null)
        return (string) null;
      string embedName = mashupInsertionInfo.EmbedName;
      if (string.IsNullOrEmpty(embedName))
        throw new InvalidArgumentException("embedName is null or empty.");
      string appearanceKey = mashupInsertionInfo.AppearanceKey;
      if (string.IsNullOrEmpty(appearanceKey))
        throw new InvalidArgumentException("appearanceKey is null or empty.");
      FlexRevertMashupEnumType type;
      switch (appearanceKey.Substring(0, 1))
      {
        case "F":
          type = FlexRevertMashupEnumType.MashupButtonFromFunctionBar;
          break;
        case "T":
          type = FlexRevertMashupEnumType.MashupButtonFromListToolBar;
          break;
        case "S":
          type = FlexRevertMashupEnumType.MashupLinkFromSectionGroup;
          break;
        case "P":
          type = FlexRevertMashupEnumType.MashupPaneWithPort;
          break;
        case "V":
          type = FlexRevertMashupEnumType.MashupPaneWithoutPort;
          break;
        default:
          throw new InvalidArgumentException("appearance not valid.");
      }
      return flexibilityHandler.RevertMashup(componentPath, floorplanAnchor, embedName, type);
    }

    private void AppearanceBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.MashupAppearanceChanged == null)
        return;
      this.MashupAppearanceChanged(sender, e);
    }

    private void PipeIcon_ImageFailed(object sender, ExceptionRoutedEventArgs e)
    {
      Image image = sender as Image;
      if (image == null)
        return;
      image.Source = (ImageSource) new BitmapImage(new Uri("../Mashups/WebService.png", UriKind.Relative));
    }

    private void MashupFullColumnSpanChkbox_Checked(object sender, RoutedEventArgs e)
    {
      if (this.MashupFullColumnChecked == null)
        return;
      this.MashupFullColumnChecked(sender, e);
    }

    private void MashupFullColumnSpanChkbox_Unchecked(object sender, RoutedEventArgs e)
    {
      if (this.MashupFullColumnUnchecked == null)
        return;
      this.MashupFullColumnUnchecked(sender, e);
    }

    private void CustomRow_Click(object sender, RoutedEventArgs e)
    {
      Control control = sender as Control;
      if (control == null)
        return;
      if (this.prevSelectedItem != null)
        VisualStateManager.GoToState((FrameworkElement) this.prevSelectedItem, "UnSelected", true);
      VisualStateManager.GoToState((FrameworkElement) control, "Selected", true);
      this.prevSelectedItem = control;
      this.CurrentPipe = control.DataContext as MashupViewModel;
      if (this.CurrentPipe != null)
      {
        this.ServiceDetailPanel.Visibility = Visibility.Visible;
        this.ServiceDetailPanel.DataContext = (object) this.CurrentPipe;
        this.ServiceDetailPanel.UpdateLayout();
      }
      else
        this.ServiceDetailPanel.Visibility = Visibility.Collapsed;
    }

    public static int GetTemplateWorkaround(DependencyObject o)
    {
      return (int) o.GetValue(AddMashupsBaseControl.TemplateWorkaroundProperty);
    }

    public static void SetTemplateWorkaround(DependencyObject o, int value)
    {
      o.SetValue(AddMashupsBaseControl.TemplateWorkaroundProperty, (object) value);
    }

    private void ExtensionFieldsDropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.DynamicParameterChanged == null)
        return;
      this.DynamicParameterChanged(sender, e);
    }

    private void ToggleButton_Click(object sender, RoutedEventArgs e)
    {
      ToggleButton toggleButton = sender as ToggleButton;
      if (toggleButton == null)
        return;
      SemanticGroupViewModel dataContext = toggleButton.DataContext as SemanticGroupViewModel;
      if (dataContext == null)
        return;
      dataContext.IsExpand = !dataContext.IsExpand;
    }

    private void CustomRow_Loaded(object sender, RoutedEventArgs e)
    {
      if (!this.firstButton)
        return;
      (new ButtonAutomationPeer(sender as Button).GetPattern(PatternInterface.Invoke) as IInvokeProvider).Invoke();
      this.firstButton = false;
    }

    private void VisibilibyBindingValueHelpList_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.VisibilityBindingChanged == null)
        return;
      this.VisibilityBindingChanged(sender, e);
    }

    private void OutputBindingDropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.DynOutputBindingChanged == null)
        return;
      this.DynOutputBindingChanged(sender, e);
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addmashupsbasecontrol.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 3:
    //      this.Caption = (Grid) target;
    //      break;
    //    case 4:
    //      this.HorizontalSeparator1 = (Rectangle) target;
    //      break;
    //    case 5:
    //      this.HorizontalSeparator2 = (Rectangle) target;
    //      break;
    //    case 6:
    //      this.BackgroundRectangle = (Rectangle) target;
    //      break;
    //    case 7:
    //      this.BackgroundGradient = (Rectangle) target;
    //      break;
    //    case 8:
    //      this.MashupNameHeader = (TextBlock) target;
    //      break;
    //    case 9:
    //      this.MashupIDHeader = (TextBlock) target;
    //      break;
    //    case 10:
    //      this.VerticalSeparator1 = (Rectangle) target;
    //      break;
    //    case 11:
    //      this.VerticalSeparator2 = (Rectangle) target;
    //      break;
    //    case 12:
    //      this.VerticalSeparator3 = (Rectangle) target;
    //      break;
    //    case 13:
    //      this.VerticalSeparator4 = (Rectangle) target;
    //      break;
    //    case 14:
    //      this.VerticalSeparator6 = (Rectangle) target;
    //      break;
    //    case 15:
    //      this.MashupVisibleHeader = (TextBlock) target;
    //      break;
    //    case 16:
    //      this.MashupScrollViewer = (ScrollViewer) target;
    //      break;
    //    case 17:
    //      this.MashupGroups = (ItemsControl) target;
    //      break;
    //    case 20:
    //      this.ServiceDetailPanel = (StackPanel) target;
    //      break;
    //    case 21:
    //      this.StaticTextProperties = (TextBlock) target;
    //      break;
    //    case 22:
    //      this.MashupType = (TextBlock) target;
    //      break;
    //    case 23:
    //      this.StaticTextMashupType = (TextBlock) target;
    //      break;
    //    case 24:
    //      this.PortBinding = (TextBlock) target;
    //      break;
    //    case 25:
    //      this.StaticTextPortBinding = (TextBlock) target;
    //      break;
    //    case 26:
    //      this.MashupName = (TextBlock) target;
    //      break;
    //    case 27:
    //      this.StaticTextMashupName = (TextBlock) target;
    //      break;
    //    case 28:
    //      this.MashupDisplayName = (TextBlock) target;
    //      break;
    //    case 29:
    //      this.FiedLabelInput = (TextBox) target;
    //      break;
    //    case 30:
    //      this.MashupAppearance = (TextBlock) target;
    //      break;
    //    case 31:
    //      this.Appearances = (ComboBox) target;
    //      this.Appearances.SelectionChanged += new SelectionChangedEventHandler(this.AppearanceBox_SelectionChanged);
    //      break;
    //    case 32:
    //      this.FullColumnSpanGrid = (Grid) target;
    //      break;
    //    case 33:
    //      this.MashupFullColumnSpan = (TextBlock) target;
    //      break;
    //    case 34:
    //      this.MashupFullColumnSpanChkbox = (CheckBox) target;
    //      this.MashupFullColumnSpanChkbox.Checked += new RoutedEventHandler(this.MashupFullColumnSpanChkbox_Checked);
    //      this.MashupFullColumnSpanChkbox.Unchecked += new RoutedEventHandler(this.MashupFullColumnSpanChkbox_Unchecked);
    //      break;
    //    case 35:
    //      this.VisibilityBindingGrid = (Grid) target;
    //      break;
    //    case 36:
    //      this.VisibilityBinding = (TextBlock) target;
    //      break;
    //    case 37:
    //      this.VisibilibyBindingValueHelpList = (ComboBox) target;
    //      this.VisibilibyBindingValueHelpList.SelectionChanged += new SelectionChangedEventHandler(this.VisibilibyBindingValueHelpList_SelectionChanged);
    //      break;
    //    case 38:
    //      this.ExtensionBindingHeader = (TextBlock) target;
    //      break;
    //    case 39:
    //      this.DynParamsControl = (ItemsControl) target;
    //      break;
    //    case 41:
    //      this.ExtensionOutputHeader = (TextBlock) target;
    //      break;
    //    case 42:
    //      this.DynOutputsControl = (ItemsControl) target;
    //      break;
    //    case 44:
    //      this.ButtonApply = (Button) target;
    //      this.ButtonApply.Click += new RoutedEventHandler(this.ButtonApply_Click);
    //      break;
    //    case 45:
    //      this.ButtonCancel = (Button) target;
    //      this.ButtonCancel.Click += new RoutedEventHandler(this.ButtonCancel_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    [DebuggerNonUserCode]
    [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    void IStyleConnector.Connect(int connectionId, object target)
    {
      switch (connectionId)
      {
        case 40:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.ExtensionFieldsDropdown_SelectionChanged);
          break;
        case 43:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.ExtensionFieldsDropdown_SelectionChanged);
          break;
        case 1:
          ((Image) target).ImageFailed += new EventHandler<ExceptionRoutedEventArgs>(this.PipeIcon_ImageFailed);
          break;
        case 2:
          ((ToggleButton) target).Checked += new RoutedEventHandler(this.MashupVisibilityCheckbox_Checked);
          ((ToggleButton) target).Unchecked += new RoutedEventHandler(this.MashupVisibilityCheckbox_Unchecked);
          break;
        case 18:
          ((ButtonBase) target).Click += new RoutedEventHandler(this.ToggleButton_Click);
          break;
        case 19:
          ((ButtonBase) target).Click += new RoutedEventHandler(this.CustomRow_Click);
          ((FrameworkElement) target).Loaded += new RoutedEventHandler(this.CustomRow_Loaded);
          break;
      }
    }

    public delegate void ApplyEventHandler(object sender, RoutedEventArgs e);

    public delegate void CancelEventHandler(object sender, RoutedEventArgs e);

    public delegate void MashupCheckedEventHandler(object sender, RoutedEventArgs e);

    public delegate void MashupUncheckedEventHandler(object sender, RoutedEventArgs e);

    public delegate void MashupFullColumnSpanCheckedEventHandler(object sender, RoutedEventArgs e);

    public delegate void MashupFullColumnSpanUnCheckedEventHandler(object sender, RoutedEventArgs e);

    public delegate void MashupAppearanceChangedEventHandler(object sender, SelectionChangedEventArgs e);

    public delegate void CustomRowClickedEventHandler(object sender, RoutedEventArgs e);

    public delegate void ImageFaildedEventHandler(object sender, ExceptionRoutedEventArgs e);

    public delegate void MashupDynamicParameterChangedEventHander(object sender, SelectionChangedEventArgs e);

    public delegate void MashupVisibilityBindingChangedEventHandler(object sender, SelectionChangedEventArgs e);

    public delegate void MashupDynOutputBindingChangedEventHandler(object sender, SelectionChangedEventArgs e);
  }
}
