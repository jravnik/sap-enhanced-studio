﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddOBNonExtendedIdentifierModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Flexibility.CoreAPI;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml.Linq;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddOBNonExtendedIdentifierModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private FlexibilityHandler flexHandler;
    private List<XElement> ovsControls;
    private bool changesApplied;
    private OBNConfigHelper OBNHelper;
    private string parentContainerId;
    //internal Button btnConfigureButton;
    //internal Button btnConfigureOBN;
    //internal Button btnConfigureParams;
    //internal ScrollViewer OBNArea;
    //internal StackPanel ContentArea;
    //internal StackPanel UsageAreaOVS;
    //internal Label lblButton;
    //internal ComboBox comboButtons;
    //internal TextBlock errorMessage;
    //internal Button okButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public AddOBNonExtendedIdentifierModeler()
    {
      this.ResizeMode = ResizeMode.NoResize;
      this.InitializeComponent();
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        this.printError("Couldn't obtain Flexibility Handler. Please contact your administrator.");
      else if (this.Anchor == null)
        this.printError("Please, select an Section Anchor");
      else if (this.Anchor.type != FlexAnchorEnumType.SectionGroupAnchor && this.Anchor.type != FlexAnchorEnumType.ListAnchor)
      {
        this.printError("Please, select an SectionAnchor/ListAnchor.");
      }
      else
      {
        XElement uiComponent = this.determinParentContainer();
        if (uiComponent == null)
        {
          this.printError("Parent Container could not be determined. Please contact your administrator.");
        }
        else
        {
          IEnumerable<XElement> ovsControls = HelperFunctions.getOVSControls(uiComponent);
          if (ovsControls == null || !ovsControls.Any<XElement>())
          {
            this.printError("No ObjectValueSelector Control exists in the selected section.");
          }
          else
          {
            this.ovsControls = this.populateOVSControls(ovsControls);
            if (this.ovsControls == null || this.ovsControls.Count < 1)
            {
              this.printError("No ObjectValueSelector Control based on extended Identifier exists under the selected area.");
            }
            else
            {
              this.fillOVSList(this.ovsControls);
              this.OBNHelper = new OBNConfigHelper(this.UIComponent, this.DTComponent);
            }
          }
        }
      }
    }

    private void fillOVSList(List<XElement> ovsControls)
    {
      if (ovsControls == null || ovsControls.Count == 0)
      {
        this.lblButton.Visibility = Visibility.Collapsed;
        this.comboButtons.Visibility = Visibility.Collapsed;
      }
      else
      {
        this.lblButton.Visibility = Visibility.Visible;
        this.comboButtons.Visibility = Visibility.Visible;
        this.comboButtons.Items.Clear();
        foreach (XElement ovsControl in ovsControls)
        {
          ComboBoxItem comboBoxItem = new ComboBoxItem();
          comboBoxItem.Content = (object) this.getLabel(ovsControl);
          comboBoxItem.Tag = (object) this.getId(ovsControl);
          this.comboButtons.Items.Add((object) comboBoxItem);
        }
        this.comboButtons.SelectedIndex = 0;
      }
    }

    private string getId(XElement ovsCtrlElem)
    {
      if (ovsCtrlElem.Attributes((XName) "id").Any<XAttribute>())
        return ovsCtrlElem.Attribute((XName) "id").Value;
      return string.Empty;
    }

    private string getLabel(XElement ovsCtrlElem)
    {
      if (ovsCtrlElem == null)
        return string.Empty;
      XElement parent = ovsCtrlElem.Parent;
      if (parent == null)
        return string.Empty;
      XElement xelement = parent.Element(HelperFunctions.ns_uxv + "Label");
      if (xelement == null)
        return string.Empty;
      string str1 = xelement.Attributes((XName) "textPoolId").Any<XAttribute>() ? xelement.Attribute((XName) "textPoolId").Value : string.Empty;
      string str2 = xelement.Attributes((XName) "fallbackValue").Any<XAttribute>() ? xelement.Attribute((XName) "fallbackValue").Value : string.Empty;
      if (!string.IsNullOrEmpty(str1))
        return this.getText(new DependentPropertyType()
        {
          fallbackValue = str2,
          textPoolId = str1
        });
      if (!string.IsNullOrEmpty(str2))
        return str2;
      return this.getId(ovsCtrlElem);
    }

    private XElement determinParentContainer()
    {
      string xrepPath = this.Anchor.xrepPath;
      IEnumerable<XElement> xelements = HelperFunctions.getSectionGroupAnchors(this.UIComponent);
      if (this.Anchor.type == FlexAnchorEnumType.SectionGroupAnchor)
        xelements = HelperFunctions.getSectionGroupAnchors(this.UIComponent);
      else if (this.Anchor.type == FlexAnchorEnumType.ListAnchor)
        xelements = HelperFunctions.getListAnchors(this.UIComponent);
      foreach (XElement xelement in xelements)
      {
        if (xelement.Attributes((XName) "xrepPath").Any<XAttribute>() && xelement.Attribute((XName) "xrepPath").Value.Equals(xrepPath))
          return xelement.Parent;
      }
      return (XElement) null;
    }

    private List<XElement> populateOVSControls(IEnumerable<XElement> ovsElements)
    {
      List<XElement> xelementList = new List<XElement>();
      foreach (XElement ovsElement in ovsElements)
      {
        if (ovsElement.Parent != null && ovsElement.Parent.Attributes((XName) "extendedBy").Any<XAttribute>())
          xelementList.Add(ovsElement);
      }
      return xelementList;
    }

    private void checkForApplyEnablement()
    {
      this.okButton.IsEnabled = false;
      if (this.OBNHelper == null || this.OBNHelper.m_navigation == null || !this.btnConfigureParams.IsEnabled)
        return;
      this.okButton.IsEnabled = true;
    }

    private void printError(string errorMsg)
    {
      this.errorMessage.Visibility = Visibility.Visible;
      this.errorMessage.Text = errorMsg;
      this.okButton.IsEnabled = false;
      this.cancelButton.IsEnabled = true;
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      this.IsChangeApplied = false;
    }

    private string getOVSId()
    {
      ComboBoxItem selectedItem = this.comboButtons.SelectedItem as ComboBoxItem;
      if (selectedItem != null && selectedItem.Tag != null)
        return (string) selectedItem.Tag;
      return (string) null;
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      if (this.OBNHelper.m_navigation == null)
      {
        this.printError("Please specify the OBN informations");
      }
      else
      {
        string navBons = this.OBNHelper.NavBONS;
        string navBo = this.OBNHelper.NavBO;
        string navBoNode = this.OBNHelper.NavBONode;
        string navOperation = this.OBNHelper.NavOperation;
        string contextAttribute = this.OBNHelper.NavContextAttribute;
        string ctx_const = this.OBNHelper.isConstantNavContext ? "X" : (string) null;
        string portTypePackage = this.OBNHelper.PortTypePackage;
        string portTypeReference = this.OBNHelper.PortTypeReference;
        List<OBNSimpleBindingType> simpleParameters = this.OBNHelper.SimpleParameters;
        List<OBNListBindingType> listParameters = this.OBNHelper.ListParameters;
        if (this.OBNHelper.m_navigation.OutPlug == null)
          this.printError("Please specify the PortType Informations");
        else if ((simpleParameters == null || simpleParameters.Count == 0) && (listParameters == null || listParameters.Count == 0))
          this.printError("Please specify Parametermapping");
        else if (string.IsNullOrEmpty(navBons))
          this.printError("Please specify the OBN BO-Namespace");
        else if (string.IsNullOrEmpty(navBo))
          this.printError("Please specify the OBN Business Object");
        else if (string.IsNullOrEmpty(navBoNode))
          this.printError("Please specify the OBN BO-Node");
        else if (string.IsNullOrEmpty(navOperation))
          this.printError("Please specify the OBN BO-Operation");
        else if (string.IsNullOrEmpty(portTypePackage))
          this.printError("Please specify the PortType Package");
        else if (string.IsNullOrEmpty(portTypeReference))
        {
          this.printError("Please specify the PortType Reference");
        }
        else
        {
          string ovsId = this.getOVSId();
          if (string.IsNullOrEmpty(ovsId))
          {
            this.printError("Please specify the ObejectValueSelector on which the OBN has to be applied");
          }
          else
          {
            this.flexHandler.AddOBNonOVSControls(this.Anchor, ovsId, navBons, navBo, navBoNode, navOperation, contextAttribute, ctx_const, portTypePackage, portTypeReference, simpleParameters, listParameters);
            this.changesApplied = true;
            this.IsChangeApplied = true;
            this.Close();
          }
        }
      }
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void comboButtons_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.checkForApplyEnablement();
    }

    private void btnConfigureOBN_Click(object sender, RoutedEventArgs e)
    {
      this.OBNArea.Visibility = Visibility.Visible;
      this.errorMessage.Visibility = Visibility.Collapsed;
      this.OBNHelper.configureOBN();
      if (this.OBNHelper.m_outportConfig.CurrentOutport == null)
      {
        this.ContentArea.Children.Clear();
        this.OBNArea.Visibility = Visibility.Collapsed;
      }
      else
        this.btnConfigureParams.IsEnabled = true;
      this.checkForApplyEnablement();
    }

    private void btnConfigureParams_Click(object sender, RoutedEventArgs e)
    {
      this.OBNArea.Visibility = Visibility.Visible;
      this.errorMessage.Visibility = Visibility.Collapsed;
      if (this.OBNHelper.m_outportConfig == null)
      {
        this.btnConfigureParams.IsEnabled = false;
      }
      else
      {
        this.ContentArea.Children.Clear();
        this.ContentArea.Children.Add((UIElement) this.OBNHelper.m_outportConfig);
        this.OBNHelper.configureParams();
        this.checkForApplyEnablement();
      }
    }

    private void btnConfigureButton_Click(object sender, RoutedEventArgs e)
    {
      this.OBNArea.Visibility = Visibility.Collapsed;
      this.errorMessage.Visibility = Visibility.Collapsed;
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addobnonextendedidentifiermodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.btnConfigureButton = (Button) target;
    //      this.btnConfigureButton.Click += new RoutedEventHandler(this.btnConfigureButton_Click);
    //      break;
    //    case 2:
    //      this.btnConfigureOBN = (Button) target;
    //      this.btnConfigureOBN.Click += new RoutedEventHandler(this.btnConfigureOBN_Click);
    //      break;
    //    case 3:
    //      this.btnConfigureParams = (Button) target;
    //      this.btnConfigureParams.Click += new RoutedEventHandler(this.btnConfigureParams_Click);
    //      break;
    //    case 4:
    //      this.OBNArea = (ScrollViewer) target;
    //      break;
    //    case 5:
    //      this.ContentArea = (StackPanel) target;
    //      break;
    //    case 6:
    //      this.UsageAreaOVS = (StackPanel) target;
    //      break;
    //    case 7:
    //      this.lblButton = (Label) target;
    //      break;
    //    case 8:
    //      this.comboButtons = (ComboBox) target;
    //      this.comboButtons.SelectionChanged += new SelectionChangedEventHandler(this.comboButtons_SelectionChanged);
    //      break;
    //    case 9:
    //      this.errorMessage = (TextBlock) target;
    //      break;
    //    case 10:
    //      this.okButton = (Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 11:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
