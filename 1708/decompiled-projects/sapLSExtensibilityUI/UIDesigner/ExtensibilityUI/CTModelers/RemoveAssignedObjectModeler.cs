﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.RemoveAssignedObjectModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.RepositoryLayer;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class RemoveAssignedObjectModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private const string TITLE_STRING = "Remove Assigned Object";
    private FlexibilityHandler flexHandler;
    private FlexBaseAnchorType stableAnchorType;
    private List<string> assignedObjectIDList;
    private bool changesApplied;
    //internal ListView assigedObjectView;
    //internal Button okButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public RemoveAssignedObjectModeler()
    {
      this.assignedObjectIDList = new List<string>();
      this.Title = "Remove Assigned Object";
      this.InitializeComponent();
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      this.stableAnchorType = this.flexHandler.GetAssignedObjectAnchor(this.UIComponent);
      if (this.stableAnchorType == null)
        return;
      this.fillListBox();
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      this.IsChangeApplied = false;
    }

    private void fillListBox()
    {
      if (this.UIComponent.CenterStructure.AssignedObjects == null)
        return;
      foreach (AssignedObjectType assignedObjectType in this.UIComponent.CenterStructure.AssignedObjects.AssignedObject)
      {
        if (assignedObjectType.type != AssignedObjectTypes.Report && assignedObjectType.type != AssignedObjectTypes.RSSFeed)
        {
          Grid grid = new Grid();
          grid.ColumnDefinitions.Add(new ColumnDefinition()
          {
            Width = new GridLength(120.0, GridUnitType.Pixel)
          });
          grid.ColumnDefinitions.Add(new ColumnDefinition()
          {
            Width = new GridLength(1.0, GridUnitType.Star)
          });
          Label label1 = new Label();
          label1.Content = (object) assignedObjectType.type.ToString();
          label1.HorizontalAlignment = HorizontalAlignment.Left;
          label1.SetValue(Grid.ColumnProperty, (object) 0);
          grid.Children.Add((UIElement) label1);
          Label label2 = new Label();
          label2.Content = (object) assignedObjectType.targetComponentID;
          label2.HorizontalAlignment = HorizontalAlignment.Left;
          label2.SetValue(Grid.ColumnProperty, (object) 1);
          grid.Children.Add((UIElement) label2);
          ListViewItem listViewItem = new ListViewItem();
          listViewItem.Content = (object) grid;
          listViewItem.Tag = (object) assignedObjectType;
          this.assigedObjectView.Items.Add((object) listViewItem);
        }
      }
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      foreach (ListViewItem selectedItem in (IEnumerable) this.assigedObjectView.SelectedItems)
      {
        if (selectedItem.Tag != null)
        {
          AssignedObjectType tag = selectedItem.Tag as AssignedObjectType;
          string id = tag.id;
          FloorplanInfo componentDetails = ProjectWorkspaceManager.Instance.GetComponentDetails(tag.targetComponentID);
          this.flexHandler.RemoveAssignedObj(this.stableAnchorType, this.getComponentAnchor(ProjectWorkspaceManager.Instance.GetUXComponent(ref componentDetails, false)), id);
          this.changesApplied = true;
          this.IsChangeApplied = true;
        }
      }
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/removeassignedobject.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.assigedObjectView = (ListView) target;
    //      break;
    //    case 2:
    //      this.okButton = (Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 3:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
