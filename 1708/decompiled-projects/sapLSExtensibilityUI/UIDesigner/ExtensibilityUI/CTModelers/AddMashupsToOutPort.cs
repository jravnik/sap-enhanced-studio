﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddMashupsToOutPort
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Flexibility.StableAnchor;
using SAP.BYD.LS.UI.Mashup.Common;
using SAP.BYD.LS.UI.Mashup.Tools.Service;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controller;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml.Linq;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddMashupsToOutPort : BaseChangeTransactionModeler, IComponentConnector
  {
    private UXOutPortType outPort;
    private FlexibilityHandler flexHandler;
    private MashupFlexibilityHandler mashupFlexHandler;
    private FlexBaseAnchorType floorplanAnchor;
    private UXOutPort outPortObj;
    private List<FlexBaseAnchor> referencedAnchors;
    private bool isOverViewOrAssignedObjects;
    private List<SemanticGroupViewModel> semanticGroupViewModels;
    private List<string> mashupCategoryStrs;
    private Dictionary<string, XElement> viewAnchorElements;
    private List<MashupViewModel> mashupViewModels;
    private List<string> changeIDs;
    private Dictionary<string, string> changeHistory;
    private Dictionary<string, ReferencedAnchorAndAppearance> appearances;
    private LoadMashupPipesWorker loader;
    private bool changesApplied;
    private bool isComponentEmbedded;
    private Dictionary<string, ExistingMashupsInfo> existingMashups;
    private MashupViewHelper mashupViewHelper;
    private UXOutPortType extensionFieldOutPort;
    //internal AddMashupsBaseControl BaseControl;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return false;
      }
    }

    public AddMashupsToOutPort()
    {
      this.InitializeComponent();
      this.Title = "Mashups Management";
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.outPort = this.ExtensibleModel as UXOutPortType;
      this.outPortObj = this.DTExtensibleModel as UXOutPort;
      if (this.outPort == null || this.outPort.StableAnchor == null || (this.outPort.StableAnchor.Count <= 0 || this.outPort.ReferencedAnchor == null) || (this.outPort.ReferencedAnchor.Count <= 0 || this.outPortObj == null))
        return;
      this.referencedAnchors = this.outPortObj.ReferencedAnchors;
      this.flexHandler = this.getFlexibilityHandler();
      this.mashupFlexHandler = this.flexHandler.GetMashupFlexibilityHandler();
      this.floorplanAnchor = this.flexHandler.GetFloorPlanAnchor(this.UIComponent);
      if (this.flexHandler == null || this.mashupFlexHandler == null)
        return;
      this.isOverViewOrAssignedObjects = StableAnchorHelper.GetStableOverviewAnchor(this.UIComponent) != null || StableAnchorHelper.GetStableAssignedObjectAnchor(this.UIComponent) != null;
      this.semanticGroupViewModels = new List<SemanticGroupViewModel>();
      this.mashupViewModels = new List<MashupViewModel>();
      this.changeIDs = new List<string>();
      this.changeHistory = new Dictionary<string, string>();
      this.appearances = new Dictionary<string, ReferencedAnchorAndAppearance>();
      this.existingMashups = new Dictionary<string, ExistingMashupsInfo>();
      this.isComponentEmbedded = this.UIComponent.UXView != null && (this.UIComponent.UXView.patternType.Equals((object) PatternType.owl) || this.UIComponent.UXView.patternType.Equals((object) PatternType.embedded));
      this.UpdateExtensionFieldOutPort();
      this.mashupViewHelper = new MashupViewHelper(this.BaseControl, this.changeHistory, this.mashupFlexHandler, this.changeIDs, this.flexHandler, this.UIComponent, this.UIComponentPath, this.Anchor, this.extensionFieldOutPort);
      this.Render();
    }

    private void UpdateExtensionFieldOutPort()
    {
      this.extensionFieldOutPort = (UXOutPortType) null;
      UXComponent uiComponent = this.UIComponent;
      if (uiComponent == null || uiComponent.Interface == null || uiComponent.Interface.OutPorts == null)
        return;
      List<UXOutPortType> outPort = uiComponent.Interface.OutPorts.OutPort;
      if (outPort == null)
        return;
      foreach (UXOutPortType uxOutPortType in outPort.Reverse<UXOutPortType>())
      {
        if (uxOutPortType != null && uxOutPortType.name == "ExtensionFieldsOutPort")
        {
          this.extensionFieldOutPort = uxOutPortType;
          break;
        }
      }
    }

    private void Render()
    {
      this.SubscribeEvents();
      this.mashupCategoryStrs = this.BaseControl.GetMashupCategories(new List<UXPortType>()
      {
        (UXPortType) this.outPort
      });
      this.LoadMashups();
      this.BaseControl.MashupGroups.ItemsSource = (IEnumerable) this.semanticGroupViewModels;
    }

    private void LoadAppearances()
    {
      this.viewAnchorElements = MashupUtil.GetAllViewStableAnchors(this.UIComponent);
      Func<XElement, string> func = (Func<XElement, string>) (sectionGroupElement =>
      {
        if (sectionGroupElement == null)
          return (string) null;
        XElement xelement = sectionGroupElement.Element(MashupConstants.ViewNameSpace + "SectionGroupName");
        if (xelement == null || !xelement.Attributes((XName) "textPoolId").Any<XAttribute>())
          return "Untitled";
        if (!xelement.Attributes((XName) "id").Any<XAttribute>())
        {
          string empty = string.Empty;
        }
        else
        {
          string str1 = xelement.Attribute((XName) "id").Value;
        }
        string str2 = xelement.Attributes((XName) "fallbackValue").Any<XAttribute>() ? xelement.Attribute((XName) "fallbackValue").Value : string.Empty;
        return this.getText(new DependentPropertyType()
        {
          id = xelement.Attribute((XName) "id").Value,
          textPoolId = xelement.Attribute((XName) "textPoolId").Value,
          fallbackValue = str2
        });
      });
      if (this.viewAnchorElements != null)
      {
        foreach (FlexBaseAnchor referencedAnchor in this.referencedAnchors)
        {
          if (this.viewAnchorElements.ContainsKey(referencedAnchor.XRepPath))
          {
            XElement viewAnchorElement = this.viewAnchorElements[referencedAnchor.XRepPath];
            FlexAnchorEnumType valueOrDefault = referencedAnchor.Type.GetValueOrDefault();
            switch (valueOrDefault)
            {
            case FlexAnchorEnumType.PaneContainerAnchor:
                XElement parent1 = viewAnchorElement.Parent;
                string str1 = "Untitled";
                if (parent1.Elements(MashupConstants.ViewNameSpace + "SectionGroup").Any<XElement>())
                {
                XElement xelement = parent1.Elements(MashupConstants.ViewNameSpace + "SectionGroup").First<XElement>();
                str1 = func(xelement);
                }
                if (parent1.Attributes((XName) "id").Any<XAttribute>() && parent1.Name.LocalName.Equals("PaneContainer"))
                {
                XElement parent2 = parent1.Parent;
                if (parent2.Attributes((XName) "id").Any<XAttribute>() && parent2.Attributes((XName) "rows").Any<XAttribute>())
                {
                    this.appearances["P-" + parent1.Attribute((XName) "id").Value + ";ViewTypeID-" + parent2.Attribute((XName) "id").Value] = new ReferencedAnchorAndAppearance()
                    {
                    Appearance = "Embedded Near " + str1 + " Section",
                    XrepPath = referencedAnchor.XRepPath
                    };
                    continue;
                }
                continue;
                }
                continue;
            case FlexAnchorEnumType.SectionGroupAnchor:
                XElement parent3 = viewAnchorElement.Parent;
                string str2 = func(parent3);
                this.appearances["S-" + parent3.Attribute((XName) "id").Value] = new ReferencedAnchorAndAppearance()
                {
                Appearance = "Link in " + str2 + " Section",
                XrepPath = referencedAnchor.XRepPath
                };
                XElement parent4 = parent3.Parent.Parent;
                if (parent4.Attributes((XName) "id").Any<XAttribute>())
                {
                if (parent4.Name.LocalName.Equals("PaneContainer"))
                {
                    XElement parent2 = parent4.Parent;
                    if (parent2.Attributes((XName) "id").Any<XAttribute>() && parent2.Attributes((XName) "rows").Any<XAttribute>())
                    {
                    this.appearances["P-" + parent4.Attribute((XName) "id").Value + ";ViewTypeID-" + parent2.Attribute((XName) "id").Value] = new ReferencedAnchorAndAppearance()
                    {
                        Appearance = "Embedded Near " + str2 + " Section",
                        XrepPath = referencedAnchor.XRepPath
                    };
                    continue;
                    }
                    continue;
                }
                if (parent4.Name.LocalName.Equals("Member"))
                {
                    XElement parent2 = parent4.Parent.Parent.Parent;
                    if (parent2.Attributes((XName) "id").Any<XAttribute>() && parent2.Attributes((XName) "rows").Any<XAttribute>())
                    {
                    this.appearances["P-" + parent4.Attribute((XName) "id").Value + ";ViewTypeID-" + parent2.Attribute((XName) "id").Value] = new ReferencedAnchorAndAppearance()
                    {
                        Appearance = "Embedded Near " + str2 + " Section",
                        XrepPath = referencedAnchor.XRepPath
                    };
                    continue;
                    }
                    continue;
                }
                continue;
                }
                continue;
            case FlexAnchorEnumType.ListAnchor:
                XElement parent5 = viewAnchorElement.Parent.Parent;
                if (parent5.Elements(MashupConstants.ViewNameSpace + "Toolbar").Any<XElement>())
                {
                XElement xelement1 = parent5.Elements(MashupConstants.ViewNameSpace + "Toolbar").First<XElement>();
                if (xelement1 != null && xelement1.Attributes((XName) "id").Any<XAttribute>())
                {
                    string str3 = xelement1.Attribute((XName) "id").Value;
                    XElement parent2 = parent5.Parent;
                    string str4 = "Untitled";
                    if (parent2.Elements(MashupConstants.ViewNameSpace + "Header").Any<XElement>())
                    {
                    XElement xelement2 = parent2.Elements(MashupConstants.ViewNameSpace + "Header").First<XElement>();
                    if (xelement2 != null && xelement2.Attributes((XName) "textPoolId").Any<XAttribute>())
                    {
                        bool flag = xelement2.Attributes((XName) "fallbackValue").Any<XAttribute>();
                        DependentPropertyType textProperty = new DependentPropertyType();
                        textProperty.id = xelement2.Attribute((XName) "id").Value;
                        textProperty.fallbackValue = flag ? xelement2.Attribute((XName) "fallbackValue").Value : string.Empty;
                        textProperty.textPoolId = xelement2.Attribute((XName) "textPoolId").Value;
                        str4 = this.getText(textProperty);
                    }
                    }
                    this.appearances["T-" + str3] = new ReferencedAnchorAndAppearance()
                    {
                    Appearance = "Web Services Menu in " + str4 + " Section",
                    XrepPath = referencedAnchor.XRepPath
                    };
                    continue;
                }
                continue;
                }
                continue;
            default:
                continue;
            }
          }
        }
      }
      if (!this.isComponentEmbedded && this.referencedAnchors.Where<FlexBaseAnchor>((Func<FlexBaseAnchor, bool>) (anchor => anchor != null)).Any<FlexBaseAnchor>((Func<FlexBaseAnchor, bool>) (anchor => FlexAnchorEnumType.FloorPlanAnchor.Equals((object) anchor.Type))))
      {
        FlexBaseAnchor flexBaseAnchor = this.referencedAnchors.Where<FlexBaseAnchor>((Func<FlexBaseAnchor, bool>) (anchor => anchor != null)).First<FlexBaseAnchor>();
        this.appearances[Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.FunctionBar)] = new ReferencedAnchorAndAppearance()
        {
          Appearance = "Web Services Menu",
          XrepPath = flexBaseAnchor.XRepPath
        };
      }
      ContextualNavigationRegionType navigationRegion = this.UIComponent.UXView.ContextualNavigationRegion;
      if (navigationRegion == null || !(navigationRegion.Item is ViewSwitchNavigationType))
        return;
      ViewSwitchNavigationType switchNavigationType = navigationRegion.Item as ViewSwitchNavigationType;
      if (switchNavigationType == null || switchNavigationType.StableAnchor == null || switchNavigationType.StableAnchor.Where<FlexBaseAnchorType>((Func<FlexBaseAnchorType, bool>) (anchor => anchor.type == FlexAnchorEnumType.ViewSwitchNavigationAnchor)).Count<FlexBaseAnchorType>() <= 0)
        return;
      this.appearances[Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ViewSwitch) + "-" + switchNavigationType.id] = new ReferencedAnchorAndAppearance()
      {
        Appearance = "New Facet",
        XrepPath = switchNavigationType.StableAnchor.Where<FlexBaseAnchorType>((Func<FlexBaseAnchorType, bool>) (anchor => anchor.type == FlexAnchorEnumType.ViewSwitchNavigationAnchor)).FirstOrDefault<FlexBaseAnchorType>().xrepPath
      };
      this.referencedAnchors.Add(FlexBaseAnchor.CreateInstance((IModelObject) this.outPortObj, switchNavigationType.StableAnchor.Where<FlexBaseAnchorType>((Func<FlexBaseAnchorType, bool>) (anchor => anchor.type == FlexAnchorEnumType.ViewSwitchNavigationAnchor)).FirstOrDefault<FlexBaseAnchorType>()));
    }

    private static bool CheckParamMandatory(UXOutPortType outPort, string mandatoryParams)
    {
      bool flag = true;
      if (!string.IsNullOrEmpty(mandatoryParams) && outPort != null)
      {
        Dictionary<string, UXParameterType> dictionary1 = new Dictionary<string, UXParameterType>();
        Dictionary<string, UXParameterType> first = new Dictionary<string, UXParameterType>();
        if (outPort.Parameter != null && outPort.Parameter.Count != 0)
          dictionary1 = outPort.Parameter.ToDictionary<UXParameterType, string>((Func<UXParameterType, string>) (p => p.name));
        if (outPort.ListParameter != null && outPort.ListParameter.Count != 0)
        {
          foreach (UXListParameterType listParameterType in outPort.ListParameter)
          {
            UXListParameterType listParam = listParameterType;
            if (listParam.Parameter != null)
            {
              Dictionary<string, UXParameterType> dictionary2 = listParam.Parameter.ToDictionary<UXParameterType, string>((Func<UXParameterType, string>) (p => listParam.name + "/" + p.name));
              first = first.Union<KeyValuePair<string, UXParameterType>>((IEnumerable<KeyValuePair<string, UXParameterType>>) dictionary2).ToDictionary<KeyValuePair<string, UXParameterType>, string, UXParameterType>((Func<KeyValuePair<string, UXParameterType>, string>) (n => n.Key), (Func<KeyValuePair<string, UXParameterType>, UXParameterType>) (n => n.Value));
            }
          }
        }
        string str = mandatoryParams;
        char[] chArray = new char[1]{ ';' };
        foreach (string key in str.Split(chArray))
        {
          if (!string.IsNullOrEmpty(key))
          {
            if (key.IndexOf("/") != -1)
            {
              if (first.ContainsKey(key) && string.IsNullOrEmpty(first[key].bind))
                flag = false;
            }
            else if (dictionary1.ContainsKey(key) && string.IsNullOrEmpty(dictionary1[key].bind))
              flag = false;
          }
        }
      }
      return flag;
    }

    private void LoadMashups()
    {
      if (StableAnchorHelper.GetStableFloorPlanAnchor(this.UIComponent) == null && !this.isOverViewOrAssignedObjects)
        return;
      this.loader = new LoadMashupPipesWorker();
      List<MashupPipeInfo> mashupPipeInfoList = this.loader.Load(this.mashupCategoryStrs, (List<MashupTypeCategories>) null, true);
      if (mashupPipeInfoList != null)
      {
        Dictionary<string, UXInPortType> dictionary = new Dictionary<string, UXInPortType>();
        this.LoadAppearances();
        AddMashupsBaseControl.LoadExistingChangeTransaction(this.UIComponentPath, this.UIComponent, this.AnchorPath, ref this.existingMashups, ref this.changeHistory);
        foreach (MashupPipeInfo mashupPipeInfo in mashupPipeInfoList)
        {
          string mashupCategory = mashupPipeInfo.MashupCategory;
          MashupCategoryType mashupCategoryByName = MashupRegistry.GetMashupCategoryByName(mashupCategory);
          bool flag1 = this.existingMashups.ContainsKey(mashupPipeInfo.PipeID);
          if (mashupCategoryByName != null && !mashupCategoryByName.name.Equals("Web_Widget"))
          {
            string str1 = this.outPort == null || this.outPort.name == null ? string.Empty : this.outPort.name;
            if (AddMashupsToOutPort.CheckParamMandatory(this.outPort, mashupPipeInfo.MandParams))
            {
              if (this.UIComponent.GetSourceOutPorts(mashupCategory).Count > 1)
              {
                string str2 = this.outPort == null || this.outPort.Description == null ? str1 : this.outPort.Description.GetElementText(this.DTComponent.TextPoolController);
                mashupPipeInfo.DisplayName = mashupPipeInfo.DisplayName + ": " + str2;
              }
              string str3 = string.Empty;
              if (!dictionary.ContainsKey(mashupCategory))
              {
                UXInPortType targetInPort = this.UIComponent.FindTargetInPort(mashupCategory);
                if (targetInPort != null)
                  dictionary[mashupCategory] = targetInPort;
              }
              if (dictionary.ContainsKey(mashupCategory))
              {
                UXInPortType uxInPortType = dictionary[mashupCategory];
                str3 = uxInPortType == null || uxInPortType.name == null ? string.Empty : uxInPortType.name;
              }
              if (flag1 && this.existingMashups[mashupPipeInfo.PipeID].TargetInplug == mashupPipeInfo.PipeID)
                mashupPipeInfo.ViewStyle = MashupViewStyleType.newDialog.ToString();
              bool flag2 = !((MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupPipeInfo.ViewStyle, false)).Equals((object) MashupViewStyleType.inScreen);
              ObservableCollection<MashupValueHelpListModel> source = new ObservableCollection<MashupValueHelpListModel>();
              foreach (KeyValuePair<string, ReferencedAnchorAndAppearance> appearance in this.appearances)
              {
                KeyValuePair<string, ReferencedAnchorAndAppearance> appr = appearance;
                if (flag2)
                {
                  if (appr.Key.StartsWith("S-") || appr.Key.StartsWith("T-") || appr.Key.Equals(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.FunctionBar)))
                  {
                    if (source.FirstOrDefault<MashupValueHelpListModel>((Func<MashupValueHelpListModel, bool>) (o => o.Code == appr.Key)) == null)
                    {
                      source.Add(new MashupValueHelpListModel()
                      {
                        Code = appr.Key,
                        Text = appr.Value.Appearance
                      });
                    }
                    else
                    {
                      MashupValueHelpListModel valueHelpListModel1 = new MashupValueHelpListModel()
                      {
                        Code = appr.Key,
                        Text = appr.Value.Appearance
                      };
                    }
                  }
                }
                else if (appr.Key.StartsWith("P-") || appr.Key.StartsWith("V-") || appr.Key.StartsWith(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ViewSwitch)))
                {
                  if (source.FirstOrDefault<MashupValueHelpListModel>((Func<MashupValueHelpListModel, bool>) (o => o.Code == appr.Key)) == null)
                  {
                    source.Add(new MashupValueHelpListModel()
                    {
                      Code = appr.Key,
                      Text = appr.Value.Appearance
                    });
                  }
                  else
                  {
                    MashupValueHelpListModel valueHelpListModel2 = new MashupValueHelpListModel()
                    {
                      Code = appr.Key,
                      Text = appr.Value.Appearance
                    };
                  }
                }
              }
              if (source.Count > 0)
              {
                string appearanceKey = flag1 ? this.existingMashups[mashupPipeInfo.PipeID].AppearanceKey : source.First<MashupValueHelpListModel>().Key;
                string str2 = flag1 ? source.FirstOrDefault<MashupValueHelpListModel>((Func<MashupValueHelpListModel, bool>) (o => o.Key == appearanceKey)).Value : source.First<MashupValueHelpListModel>().Value;
                string str4 = string.Empty;
                bool flag3 = source.Count == 1;
                string str5 = string.Empty;
                string str6 = string.Empty;
                if (appearanceKey.StartsWith("S-") || appearanceKey.StartsWith("T-") || appearanceKey.StartsWith("V-"))
                {
                  str5 = appearanceKey.Substring(2);
                  if (appearanceKey.StartsWith("S-"))
                    str4 = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.SectionLink);
                  else if (appearanceKey.StartsWith("T-"))
                    str4 = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ListToolbar);
                  else if (appearanceKey.StartsWith("V-"))
                    str4 = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.NewPaneContainer);
                }
                else if (appearanceKey.StartsWith("P-"))
                {
                  Match match = Regex.Match(appearanceKey, "P-(?<PaneContainerID>.+);ViewTypeID-(?<ParentID>.*)");
                  if (match.Success)
                  {
                    string str7 = match.Groups["PaneContainerID"].ToString();
                    str5 = match.Groups["ParentID"].ToString();
                    str6 = str7;
                    str4 = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.NearPaneContainer);
                  }
                }
                else
                  str4 = !appearanceKey.StartsWith(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ViewSwitch)) ? Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.FunctionBar) : Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ViewSwitch);
                int num = 0;
                int index = 0;
                for (int count = source.Count; index < count; ++index)
                {
                  if (source.ElementAt<MashupValueHelpListModel>(index).Key == appearanceKey)
                    num = index;
                }
                string str8 = !string.IsNullOrEmpty(mashupPipeInfo.DomainUrl) ? mashupPipeInfo.DomainUrl.Trim() : string.Empty;
                bool flag4 = flag1 && this.existingMashups[mashupPipeInfo.PipeID].FullColumnSpan;
                MashupViewModel mashupViewModel = new MashupViewModel()
                {
                  DisplayName = mashupPipeInfo.DisplayName,
                  Description = mashupPipeInfo.Description,
                  ComponentPath = mashupPipeInfo.FilePath,
                  SemanticCategory = mashupPipeInfo.SemanticCategory,
                  MashupCategory = mashupCategory,
                  MashupCategoryDescription = MashupRegistry.GetMashupCategoryDisplayNameByName(mashupPipeInfo.MashupCategory),
                  PipeID = mashupPipeInfo.PipeID,
                  IconUrl = str8,
                  FullColumnSpan = flag4,
                  IsVisible = flag1,
                  MashupType = mashupPipeInfo.MashupType,
                  ViewStyle = mashupPipeInfo.ViewStyle,
                  MashupInPlug = mashupPipeInfo.InPort,
                  MashupOutPlug = mashupPipeInfo.OutPort,
                  SourceOutPlug = str1,
                  TargetInPlug = str3,
                  AppearanceOptions = source,
                  OneAppearance = flag3,
                  MultipleAppearances = !flag3,
                  AppearanceKey = appearanceKey,
                  Appearance = str4,
                  ServiceAppearance = str2,
                  AppearanceIndex = num,
                  ContainerID = str5,
                  RefPaneContainerID = str6,
                  ReferencedAnchorXrepPath = this.appearances[appearanceKey].XrepPath,
                  NotExisting = !flag1,
                  MandatoryParams = mashupPipeInfo.MandParams ?? string.Empty,
                  DynParams = mashupPipeInfo.DynParams ?? string.Empty,
                  DynOutputs = mashupPipeInfo.DynOutputs ?? string.Empty
                };
                if (mashupViewModel.IsDynamic)
                  mashupViewModel.SourceOutPlug = "ExtensionFieldsOutPort";
                mashupViewModel.SetDynamicBindingOptionandVisibleAppearance(this.UIComponent, this.existingMashups, (string) null);
                mashupViewModel.VisibilityBindingExpression = flag1 ? this.existingMashups[mashupPipeInfo.PipeID].VisibilityBindingExp : string.Empty;
                this.mashupViewModels.Add(mashupViewModel);
              }
            }
          }
        }
      }
      this.semanticGroupViewModels = this.mashupViewModels.GroupBy<MashupViewModel, string>((Func<MashupViewModel, string>) (serviceViewModel => serviceViewModel.SemanticCategory)).OrderBy<IGrouping<string, MashupViewModel>, string>((Func<IGrouping<string, MashupViewModel>, string>) (g => g.ToArray<MashupViewModel>()[0].SemanticCategory)).Select<IGrouping<string, MashupViewModel>, SemanticGroupViewModel>((Func<IGrouping<string, MashupViewModel>, SemanticGroupViewModel>) (g => new SemanticGroupViewModel()
      {
        SemanticCategory = g.Key,
        DisplayName = SemanticCategory.Instance[g.Key],
        Mashups = g.ToList<MashupViewModel>()
      })).ToList<SemanticGroupViewModel>();
    }

    private void SubscribeEvents()
    {
      this.BaseControl.ApplyClicked += new AddMashupsBaseControl.ApplyEventHandler(this.ButtonApply_Click);
      this.BaseControl.CancelClicked += new AddMashupsBaseControl.CancelEventHandler(this.ButtonCancel_Click);
      this.BaseControl.MashupChecked += new AddMashupsBaseControl.MashupCheckedEventHandler(this.MashupVisibilityCheckbox_Checked);
      this.BaseControl.MashupUnchecked += new AddMashupsBaseControl.MashupUncheckedEventHandler(this.MashupVisibilityCheckbox_Unchecked);
      this.BaseControl.MashupAppearanceChanged += new AddMashupsBaseControl.MashupAppearanceChangedEventHandler(this.AppearanceBox_SelectionChanged);
      this.BaseControl.MashupFullColumnChecked += new AddMashupsBaseControl.MashupFullColumnSpanCheckedEventHandler(this.MashupFullColumn_Checked);
      this.BaseControl.MashupFullColumnUnchecked += new AddMashupsBaseControl.MashupFullColumnSpanUnCheckedEventHandler(this.MashupFullColumn_Unchecked);
      this.BaseControl.DynamicParameterChanged += new AddMashupsBaseControl.MashupDynamicParameterChangedEventHander(this.MashupDynamicParameterChanged);
      this.BaseControl.VisibilityBindingChanged += new AddMashupsBaseControl.MashupVisibilityBindingChangedEventHandler(this.MashupVisibilityBindingChanged);
    }

    private void UnsubscribeEvents()
    {
      this.BaseControl.ApplyClicked -= new AddMashupsBaseControl.ApplyEventHandler(this.ButtonApply_Click);
      this.BaseControl.CancelClicked -= new AddMashupsBaseControl.CancelEventHandler(this.ButtonCancel_Click);
      this.BaseControl.MashupChecked -= new AddMashupsBaseControl.MashupCheckedEventHandler(this.MashupVisibilityCheckbox_Checked);
      this.BaseControl.MashupUnchecked -= new AddMashupsBaseControl.MashupUncheckedEventHandler(this.MashupVisibilityCheckbox_Unchecked);
      this.BaseControl.MashupAppearanceChanged -= new AddMashupsBaseControl.MashupAppearanceChangedEventHandler(this.AppearanceBox_SelectionChanged);
      this.BaseControl.MashupFullColumnChecked -= new AddMashupsBaseControl.MashupFullColumnSpanCheckedEventHandler(this.MashupFullColumn_Checked);
      this.BaseControl.MashupFullColumnUnchecked -= new AddMashupsBaseControl.MashupFullColumnSpanUnCheckedEventHandler(this.MashupFullColumn_Unchecked);
      this.BaseControl.DynamicParameterChanged -= new AddMashupsBaseControl.MashupDynamicParameterChangedEventHander(this.MashupDynamicParameterChanged);
      this.BaseControl.VisibilityBindingChanged -= new AddMashupsBaseControl.MashupVisibilityBindingChangedEventHandler(this.MashupVisibilityBindingChanged);
    }

    private void ButtonApply_Click(object sender, RoutedEventArgs e)
    {
      this.changesApplied = true;
      this.IsChangeApplied = true;
      this.Close();
    }

    private void ButtonCancel_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void AppearanceBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBox comboBox = sender as ComboBox;
      if (comboBox == null)
        return;
      MashupViewModel dataContext = comboBox.DataContext as MashupViewModel;
      MashupValueHelpListModel selectedItem = comboBox.SelectedItem as MashupValueHelpListModel;
      if (dataContext == null || selectedItem == null)
        return;
      string appearanceKey = selectedItem.Key;
      dataContext.AppearanceKey = appearanceKey;
      dataContext.ServiceAppearance = dataContext.AppearanceOptions.Where<MashupValueHelpListModel>((Func<MashupValueHelpListModel, bool>) (o => o.Key == appearanceKey)).FirstOrDefault<MashupValueHelpListModel>().Value;
      dataContext.ReferencedAnchorXrepPath = this.appearances[appearanceKey].XrepPath;
      if (!dataContext.IsVisible || !dataContext.NotExisting)
        return;
      this.mashupViewHelper.HandleInsertMashup(dataContext, this.referencedAnchors);
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      if (this.changeIDs != null)
      {
        foreach (string changeId in this.changeIDs)
          this.mashupFlexHandler.RevertChangeTransactionInSession(changeId);
      }
      this.IsChangeApplied = false;
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
      this.Terminate();
    }

    private void MashupVisibilityCheckbox_Checked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.VisibilityCheckbox_Checked(sender, this.referencedAnchors);
    }

    private void MashupVisibilityCheckbox_Unchecked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.VisibilityCheckbox_Unchecked(sender);
    }

    private void MashupFullColumn_Checked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.FullColumn_Checked(sender, this.referencedAnchors);
    }

    private void MashupFullColumn_Unchecked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.FullColumn_Unchecked(sender, this.referencedAnchors);
    }

    private void MashupDynamicParameterChanged(object sender, SelectionChangedEventArgs e)
    {
      this.mashupViewHelper.DynamicParameterChanged(sender, this.referencedAnchors, this.UIComponent, this.isComponentEmbedded, (FlexBaseAnchorType) null);
    }

    private void MashupVisibilityBindingChanged(object sender, SelectionChangedEventArgs e)
    {
      this.mashupViewHelper.VisibilityBindingChanged(sender, this.referencedAnchors);
    }

    private void InsertMashup(MashupViewModel mashupInsertionInfo)
    {
      if (!mashupInsertionInfo.IsDynamic)
      {
        foreach (FlexBaseAnchor referencedAnchor in this.referencedAnchors)
        {
          if (referencedAnchor.XRepPath == mashupInsertionInfo.ReferencedAnchorXrepPath)
          {
            string str = AddMashupsBaseControl.InsertMashup(mashupInsertionInfo, this.flexHandler, this.UIComponent, this.UIComponentPath, referencedAnchor.FlexBaseAnchorType);
            if (string.IsNullOrEmpty(str))
              break;
            this.changeIDs.Add(str);
            this.changeHistory.Add(mashupInsertionInfo.PipeID + mashupInsertionInfo.SourceOutPlug, str);
            break;
          }
        }
      }
      else
      {
        string str = AddMashupsBaseControl.InsertMashup(mashupInsertionInfo, this.flexHandler, this.UIComponent, this.UIComponentPath, this.Anchor);
        if (string.IsNullOrEmpty(str))
          return;
        this.changeIDs.Add(str);
        this.changeHistory.Add(mashupInsertionInfo.PipeID + mashupInsertionInfo.SourceOutPlug, str);
      }
    }

    private void Terminate()
    {
      this.UnsubscribeEvents();
      if (this.mashupCategoryStrs != null)
      {
        this.mashupCategoryStrs.Clear();
        this.mashupCategoryStrs = (List<string>) null;
      }
      if (this.semanticGroupViewModels != null)
      {
        this.semanticGroupViewModels.Clear();
        this.semanticGroupViewModels = (List<SemanticGroupViewModel>) null;
      }
      if (this.mashupViewModels != null)
      {
        this.mashupViewModels.Clear();
        this.mashupViewModels = (List<MashupViewModel>) null;
      }
      if (this.changeIDs != null)
      {
        this.changeIDs.Clear();
        this.changeIDs = (List<string>) null;
      }
      if (this.changeHistory != null)
      {
        this.changeHistory.Clear();
        this.changeHistory = (Dictionary<string, string>) null;
      }
      if (this.appearances != null)
      {
        this.appearances.Clear();
        this.appearances = (Dictionary<string, ReferencedAnchorAndAppearance>) null;
      }
      this.outPort = (UXOutPortType) null;
      this.mashupFlexHandler = (MashupFlexibilityHandler) null;
      this.flexHandler = (FlexibilityHandler) null;
      this.loader = (LoadMashupPipesWorker) null;
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addmashupstooutport.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  if (connectionId == 1)
    //    this.BaseControl = (AddMashupsBaseControl) target;
    //  else
    //    this._contentLoaded = true;
    //}
  }
}
