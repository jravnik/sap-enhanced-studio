﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddOVSonIDToTable
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using System.Collections.Generic;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  internal class AddOVSonIDToTable : AddOVSInfoModeler
  {
    protected override List<FlexStableAnchorType> getStableAnchorList()
    {
      return this.flexHandler.GetAllStableAnchorList(this.listType);
    }

    protected override void init()
    {
      this.InitializeComponent();
      this.listType = this.ExtensibleModel as ListType;
      if (this.listType == null)
        return;
      if (this.listType.ListHeader != null)
      {
        this.entityHeader.Text = this.getText(this.listType.ListHeader);
      }
      else
      {
        this.entityHeader.Text = "Untitled Table";
        for (IModelObject modelObject = this.DTExtensibleModel; !(modelObject is View); modelObject = modelObject.Parent)
        {
          if (modelObject is AdvancedListPane)
          {
            AdvancedListPane advancedListPane = modelObject as AdvancedListPane;
            if (!advancedListPane.HasPaneHeader)
              break;
            this.entityHeader.Text = this.getText(advancedListPane.PaneHeader.DependentPropertyType);
            break;
          }
        }
      }
    }
  }
}
