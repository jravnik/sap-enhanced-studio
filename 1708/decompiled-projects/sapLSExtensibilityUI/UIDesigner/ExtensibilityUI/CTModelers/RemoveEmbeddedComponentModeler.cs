﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.RemoveEmbeddedComponentModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controller;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using SAP.BYD.LS.UIDesigner.Model.RepositoryLayer;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class RemoveEmbeddedComponentModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private const string TITLE_STRING = "Remove Embedded Component";
    private const string ANA_GENERIC_COMP = "/SAP_BYD_TF/Analytics/AnalysisPattern/ANA_ICP_Embedded.EC.uicomponent";
    private FlexibilityHandler flexHandler;
    private FlexBaseAnchorType stableAnchorType;
    private bool changesApplied;
    private List<string> embeddedComponentList;
    //internal ListView componentListView;
    //internal Button removeButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public RemoveEmbeddedComponentModeler()
    {
      this.Title = "Remove Embedded Component";
      this.InitializeComponent();
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.embeddedComponentList = new List<string>();
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler != null)
      {
        this.stableAnchorType = this.Anchor;
        if (this.stableAnchorType != null)
          this.fillListBox();
      }
      this.removeButton.IsEnabled = false;
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      this.IsChangeApplied = false;
    }

    private EmbeddedComponentType getEmbeddedComponentType(string embedName)
    {
      foreach (EmbeddedComponentType embeddedComponentType in this.UIComponent.EmbeddedComponents.EmbeddedComponent)
      {
        if (embeddedComponentType.embedName.Equals(embedName))
          return embeddedComponentType;
      }
      return (EmbeddedComponentType) null;
    }

    private void fillListBox()
    {
      if (!this.stableAnchorType.type.Equals((object) FlexAnchorEnumType.OutPortAnchor))
        return;
      string portName = (this.DTExtensibleModel as UXOutPort).PortName;
      IControllerNavigation controllerNavigation = this.DTComponent.ControllerNavigation;
      if (controllerNavigation == null)
        return;
      foreach (IOnNavigate onNavigate in controllerNavigation.OnNavigate)
      {
        if (onNavigate.NavigationStyle == NavigateStyle.inscreen_dataflow && onNavigate.OutPlug == portName)
        {
          EmbeddedComponentType embeddedComponentType = this.getEmbeddedComponentType(onNavigate.targetComponentID);
          if (embeddedComponentType != null && !embeddedComponentType.Equals((object) "/SAP_BYD_TF/Analytics/AnalysisPattern/ANA_ICP_Embedded.EC.uicomponent"))
          {
            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition()
            {
              Width = new GridLength(180.0, GridUnitType.Pixel)
            });
            grid.ColumnDefinitions.Add(new ColumnDefinition()
            {
              Width = new GridLength(550.0, GridUnitType.Pixel)
            });
            Label label1 = new Label();
            label1.Content = (object) embeddedComponentType.embedName;
            label1.HorizontalAlignment = HorizontalAlignment.Left;
            label1.SetValue(Grid.ColumnProperty, (object) 0);
            grid.Children.Add((UIElement) label1);
            Label label2 = new Label();
            label2.Content = (object) embeddedComponentType.targetComponentID;
            label2.HorizontalAlignment = HorizontalAlignment.Left;
            label2.SetValue(Grid.ColumnProperty, (object) 1);
            grid.Children.Add((UIElement) label2);
            ListViewItem listViewItem = new ListViewItem();
            listViewItem.Content = (object) grid;
            this.componentListView.Items.Add((object) listViewItem);
            listViewItem.Tag = (object) embeddedComponentType;
          }
        }
      }
    }

    private void fillListBoxFloorplan()
    {
      if (!this.stableAnchorType.type.Equals((object) FlexAnchorEnumType.OutPortAnchor) || this.UIComponent.EmbeddedComponents == null)
        return;
      foreach (EmbeddedComponentType embeddedComponentType in this.UIComponent.EmbeddedComponents.EmbeddedComponent)
      {
        if (!embeddedComponentType.targetComponentID.Equals("/SAP_BYD_TF/Analytics/AnalysisPattern/ANA_ICP_Embedded.EC.uicomponent"))
        {
          Grid grid = new Grid();
          grid.ColumnDefinitions.Add(new ColumnDefinition()
          {
            Width = new GridLength(180.0, GridUnitType.Pixel)
          });
          grid.ColumnDefinitions.Add(new ColumnDefinition()
          {
            Width = new GridLength(550.0, GridUnitType.Pixel)
          });
          Label label1 = new Label();
          label1.Content = (object) embeddedComponentType.embedName;
          label1.HorizontalAlignment = HorizontalAlignment.Left;
          label1.SetValue(Grid.ColumnProperty, (object) 0);
          grid.Children.Add((UIElement) label1);
          Label label2 = new Label();
          label2.Content = (object) embeddedComponentType.targetComponentID;
          label2.HorizontalAlignment = HorizontalAlignment.Left;
          label2.SetValue(Grid.ColumnProperty, (object) 1);
          grid.Children.Add((UIElement) label2);
          ListViewItem listViewItem = new ListViewItem();
          listViewItem.Content = (object) grid;
          this.componentListView.Items.Add((object) listViewItem);
          listViewItem.Tag = (object) embeddedComponentType;
        }
      }
    }

    private string getTargetComponentIdOfEmbedName(string embedName)
    {
      foreach (EmbeddedComponentType embeddedComponentType in this.UIComponent.EmbeddedComponents.EmbeddedComponent)
      {
        if (embeddedComponentType.embedName == embedName)
          return embeddedComponentType.targetComponentID;
      }
      return (string) null;
    }

    private void removeButton_Click(object sender, RoutedEventArgs e)
    {
      foreach (object selectedItem in (IEnumerable) this.componentListView.SelectedItems)
      {
        EmbeddedComponentType tag = (selectedItem as ListViewItem).Tag as EmbeddedComponentType;
        if (tag != null)
        {
          string id = tag.id;
          string anchorRef = (string) null;
          IFloorplan floorplanModel = ModelLayer.ObjectManager.GetFloorplanModel(ProjectWorkspaceManager.Instance.GetComponentDetails(tag.targetComponentID), false);
          if (floorplanModel is IExtensible)
          {
            foreach (FlexBaseAnchor associatedAnchor in (floorplanModel as IExtensible).GetAssociatedAnchors())
            {
              if (associatedAnchor.FlexBaseAnchorType.type == FlexAnchorEnumType.FloorPlanAnchor)
              {
                anchorRef = associatedAnchor.FlexBaseAnchorType.xrepPath;
                break;
              }
            }
          }
          this.flexHandler.RemoveEmbeddedComponent(this.Anchor, anchorRef, id);
          this.IsChangeApplied = true;
        }
      }
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void wocViewListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.removeButton.IsEnabled = this.componentListView.SelectedItems.Count > 0;
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/removeembeddedcomponentmodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.componentListView = (ListView) target;
    //      this.componentListView.SelectionChanged += new SelectionChangedEventHandler(this.wocViewListView_SelectionChanged);
    //      break;
    //    case 2:
    //      this.removeButton = (Button) target;
    //      this.removeButton.Click += new RoutedEventHandler(this.removeButton_Click);
    //      break;
    //    case 3:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
