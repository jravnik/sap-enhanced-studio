﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.RemoveButtonModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class RemoveButtonModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private bool removeable = true;
    private ButtonGroupType buttonGroupType;
    private FlexibilityHandler flexHandler;
    private FlexBaseAnchorType stableAnchor;
    private Dictionary<string, List<string>> removedButton;
    private string changeHistory;
    private bool changesApplied;
    //internal TextBlock GuideMessage;
    //internal TreeView ButtonTreeView;
    //internal System.Windows.Controls.Button okButton;
    //internal System.Windows.Controls.Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return false;
      }
    }

    public RemoveButtonModeler()
    {
      this.InitializeComponent();
      this.Title = "Remove Button";
      this.GuideMessage.Text = "Please select a button or navigation item to remove, and click apply";
    }

    protected override void revertChanges()
    {
      if (string.IsNullOrEmpty(this.changeHistory) || this.changesApplied)
        return;
      this.flexHandler.RevertChangeTransaction(this.changeHistory);
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
      this.Terminate();
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.buttonGroupType = this.ExtensibleModel as ButtonGroupType;
      if (this.buttonGroupType == null)
        throw new ApplicationException("Selected item is not a ButtonGroup, it is type of " + (object) this.ExtensibleModel.GetType());
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      foreach (FlexBaseAnchorType allStableAnchor in this.flexHandler.GetAllStableAnchorList(this.buttonGroupType))
      {
        if (this.Anchor.xrepPath == allStableAnchor.xrepPath)
        {
          this.stableAnchor = allStableAnchor;
          break;
        }
      }
      if (this.stableAnchor == null)
        return;
      this.removedButton = new Dictionary<string, List<string>>();
      this.LoadExistingChangeTransaction(this.UIComponent, this.AnchorPath, ref this.removedButton);
      this.InitButtonTreeView();
    }

    public void LoadExistingChangeTransaction(UXComponent uxComponent, string anchorPath, ref Dictionary<string, List<string>> removedButton)
    {
      List<ModeledChangeTransactionInfo> publishedCTList;
      List<ModeledChangeTransactionInfo> savedCTList;
      XRepositoryProxy.Instance.GetChangeTransactionForAnchor(anchorPath, out publishedCTList, out savedCTList);
      if (publishedCTList != null && publishedCTList.Count > 0)
      {
        foreach (ModeledChangeTransactionInfo changeTransactionInfo in publishedCTList)
          RemoveButtonModeler.CheckChangeTransaction(changeTransactionInfo.Content, ref removedButton);
      }
      if (savedCTList != null && savedCTList.Count > 0)
      {
        foreach (ModeledChangeTransactionInfo changeTransactionInfo in savedCTList)
          RemoveButtonModeler.CheckChangeTransaction(changeTransactionInfo.Content, ref removedButton);
      }
      List<ModeledChangeTransactionInfo> changeTransactions = ChangeTransactionManager.Instance.GetUnsavedChangeTransactions(this.UIComponentPath, uxComponent, anchorPath);
      if (changeTransactions == null || changeTransactions.Count <= 0)
        return;
      foreach (ModeledChangeTransactionInfo changeTransactionInfo in changeTransactions)
        RemoveButtonModeler.CheckChangeTransaction(changeTransactionInfo.Content, ref removedButton);
    }

    private static void CheckChangeTransaction(string content, ref Dictionary<string, List<string>> removedButton)
    {
      using (MemoryStream memoryStream = new MemoryStream(new UTF8Encoding().GetBytes(content)))
      {
        object deserializedObject;
        if (!XmlHelper.GetInstance().DeserializeObject(typeof (ChangeTransaction), (Stream) memoryStream, out deserializedObject))
          return;
        ChangeTransaction changeTransaction = deserializedObject as ChangeTransaction;
        if (changeTransaction == null || changeTransaction.UsedAnchor == null || (changeTransaction.UsedAnchor.RemoveButton == null || changeTransaction.UsedAnchor.RemoveButton.Count <= 0))
          return;
        string buttonId = changeTransaction.UsedAnchor.RemoveButton[0].buttonID;
        string navigationItemId = changeTransaction.UsedAnchor.RemoveButton[0].navigationItemID;
        if (removedButton.ContainsKey(buttonId))
          removedButton[buttonId].Add(navigationItemId);
        else
          removedButton.Add(buttonId, new List<string>()
          {
            navigationItemId
          });
      }
    }

    private void InitButtonTreeView()
    {
      foreach (ButtonType buttonType in this.buttonGroupType.Button)
      {
        if (!this.removedButton.ContainsKey(buttonType.id) || buttonType.Menu != null && !this.removedButton[buttonType.id].Contains(string.Empty))
        {
          TreeViewItem treeViewItem1 = new TreeViewItem();
          treeViewItem1.Header = (object) this.getText(buttonType.Text);
          treeViewItem1.FontFamily = new FontFamily("Verdana");
          treeViewItem1.FontWeight = FontWeights.Normal;
          treeViewItem1.Tag = (object) buttonType.id;
          bool flag = true;
          ButtonGroup dtExtensibleModel = this.DTExtensibleModel as ButtonGroup;
          if (dtExtensibleModel != null)
          {
            foreach (SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button button in dtExtensibleModel.Buttons)
            {
              if (button.Id == buttonType.id && button.IsMandatory)
              {
                flag = false;
                treeViewItem1.ToolTip = (object) "This button or navigation item can't be removed!";
                break;
              }
            }
          }
          if (buttonType.Menu != null && buttonType.Menu.NavigationItem != null)
          {
            foreach (NavigationItemType navigationItemType in buttonType.Menu.NavigationItem)
            {
              if (!this.removedButton.ContainsKey(buttonType.id) || !this.removedButton[buttonType.id].Contains(navigationItemType.id))
              {
                TreeViewItem treeViewItem2 = new TreeViewItem();
                treeViewItem2.Header = (object) this.getText(navigationItemType.Title);
                treeViewItem2.FontFamily = new FontFamily("Verdana");
                treeViewItem2.FontWeight = FontWeights.Normal;
                treeViewItem2.Tag = (object) navigationItemType.id;
                if (!flag)
                  treeViewItem2.ToolTip = (object) "This button or navigation item can't be removed!";
                treeViewItem1.Items.Add((object) treeViewItem2);
              }
            }
            if (treeViewItem1.Items.Count == 0)
              continue;
          }
          treeViewItem1.IsExpanded = true;
          this.ButtonTreeView.Items.Add((object) treeViewItem1);
        }
      }
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      if (this.removeable)
      {
        this.changesApplied = true;
        this.IsChangeApplied = true;
      }
      else
      {
        this.changesApplied = false;
        this.IsChangeApplied = false;
      }
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void ButtonTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      this.okButton.IsEnabled = false;
      if (!string.IsNullOrEmpty(this.changeHistory))
        this.flexHandler.RevertChangeTransaction(this.changeHistory);
      TreeView treeView = sender as TreeView;
      if (treeView == null)
        return;
      TreeViewItem selectedItem = treeView.SelectedItem as TreeViewItem;
      if (selectedItem == null)
        return;
      string str = string.Empty;
      string tag;
      if (selectedItem.Parent is TreeViewItem)
      {
        tag = (selectedItem.Parent as TreeViewItem).Tag as string;
        str = selectedItem.Tag as string;
      }
      else
        tag = selectedItem.Tag as string;
      ButtonGroup dtExtensibleModel = this.DTExtensibleModel as ButtonGroup;
      if (dtExtensibleModel != null)
      {
        int num = 0;
        foreach (SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button button in dtExtensibleModel.Buttons)
        {
          if (button.Id == tag && button.IsMandatory)
          {
            this.removeable = false;
            break;
          }
          ++num;
        }
        if (num == dtExtensibleModel.Buttons.Count)
          this.removeable = true;
      }
      if (!this.removeable)
        return;
      this.changeHistory = this.flexHandler.RemoveButton(this.stableAnchor, new FlexRemoveButtonType()
      {
        buttonID = tag,
        navigationItemID = str
      }, this.UIComponentPath);
      this.okButton.IsEnabled = true;
    }

    private void Terminate()
    {
      if (this.buttonGroupType != null)
        this.buttonGroupType = (ButtonGroupType) null;
      if (this.flexHandler != null)
        this.flexHandler = (FlexibilityHandler) null;
      if (this.stableAnchor != null)
        this.stableAnchor = (FlexBaseAnchorType) null;
      if (this.removedButton == null)
        return;
      this.removedButton = (Dictionary<string, List<string>>) null;
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/removebuttonmodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.GuideMessage = (TextBlock) target;
    //      break;
    //    case 2:
    //      this.ButtonTreeView = (TreeView) target;
    //      this.ButtonTreeView.SelectedItemChanged += new RoutedPropertyChangedEventHandler<object>(this.ButtonTreeView_SelectedItemChanged);
    //      break;
    //    case 3:
    //      this.okButton = (System.Windows.Controls.Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 4:
    //      this.cancelButton = (System.Windows.Controls.Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
