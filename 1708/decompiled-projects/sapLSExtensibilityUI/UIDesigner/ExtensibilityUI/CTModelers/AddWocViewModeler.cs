﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddWocViewModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controller;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.RepositoryLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddWocViewModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private const string TITLE_STRING = "Add Application View";
    private const string TXT_ERROR_TC_NOT_SELECTED = "Please select an Application View";
    private const string TXT_ERROR_WRONG_VIEWTYPE = "Only Application Views can be assigned";
    private FlexibilityHandler flexHandler;
    private FlexBaseAnchorType stableAnchorType;
    private bool changesApplied;
    private AddWocViewModeler.TargetComponent targetComponent;
    private FlexPositionEnumType position;
    private List<ScopeRule> scopeRuleList;
    //internal System.Windows.Controls.Label componentLabel;
    //internal System.Windows.Controls.TextBox targetTextBox;
    //internal System.Windows.Controls.ComboBox positionListBox;
    //internal System.Windows.Controls.Button addScopeRulesButton;
    //internal System.Windows.Controls.Button okButton;
    //internal System.Windows.Controls.Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public AddWocViewModeler()
    {
      this.InitializeComponent();
      this.okButton.IsEnabled = false;
      this.scopeRuleList = new List<ScopeRule>();
      this.addScopeRulesButton.IsEnabled = false;
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.fillPositionListBox();
      this.positionListBox.SelectedIndex = 0;
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      this.stableAnchorType = this.Anchor;
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      this.IsChangeApplied = false;
    }

    private void fillPositionListBox()
    {
      foreach (string name in Enum.GetNames(FlexPositionEnumType.Bottom.GetType()))
      {
        ListBoxItem listBoxItem = new ListBoxItem();
        listBoxItem.Content = (object) name;
        this.positionListBox.Items.Add((object) listBoxItem);
        if (name == FlexPositionEnumType.BehindAnchor.ToString())
          listBoxItem.IsEnabled = false;
      }
    }

    private string checkDependencies()
    {
      if (this.targetComponent.Component == null)
        return "Please select an Application View";
      if (this.targetComponent.Component.wocViewSubType == ViewSwitchTypes.InvisibleApplication || this.targetComponent.Component.wocViewSubType == ViewSwitchTypes.Application)
        return (string) null;
      return "Only Application Views can be assigned";
    }

    private void targetCompButton_Click(object sender, RoutedEventArgs e)
    {
      this.scopeRuleList.Clear();
      this.addScopeRulesButton.IsEnabled = false;
      RepositoryFilterBrowser repositoryFilterBrowser = new RepositoryFilterBrowser(new List<FloorplanType>()
      {
        FloorplanType.WCVIEW
      }, (FloorplanInfo) null);
      if (repositoryFilterBrowser.ShowDialog() != System.Windows.Forms.DialogResult.OK)
        return;
      FloorplanInfo selectedComponent = repositoryFilterBrowser.SelectedComponent;
      this.targetComponent.Component = ProjectWorkspaceManager.Instance.GetUXComponent(ref selectedComponent, false);
      IFloorplan floorplanModel = ModelLayer.ObjectManager.GetFloorplanModel(selectedComponent, false);
      this.addScopeRulesButton.IsEnabled = false;
      string str = this.checkDependencies();
      if (str == null)
      {
        if (floorplanModel is WorkCenterView)
          this.targetComponent.Title = (floorplanModel as WorkCenterView).IdentificationArea.FloorplanTitle.ToString();
        this.targetTextBox.Text = repositoryFilterBrowser.SelectedComponent.UniqueId;
        this.targetTextBox.Foreground = (Brush) new SolidColorBrush(Colors.Black);
        this.targetComponent.Path = repositoryFilterBrowser.SelectedComponent.UniqueId;
        this.okButton.IsEnabled = true;
        this.addScopeRulesButton.IsEnabled = true;
      }
      else
      {
        this.targetTextBox.Text = str;
        this.targetTextBox.Foreground = (Brush) new SolidColorBrush(Colors.Red);
        this.okButton.IsEnabled = false;
      }
    }

    private void positionListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ListBoxItem selectedItem = this.positionListBox.SelectedItem as ListBoxItem;
      if (selectedItem == null)
        return;
      if (FlexPositionEnumType.Bottom.ToString() == selectedItem.Content.ToString())
        this.position = FlexPositionEnumType.Bottom;
      else if (FlexPositionEnumType.Top.ToString() == selectedItem.Content.ToString())
      {
        this.position = FlexPositionEnumType.Top;
      }
      else
      {
        if (!(FlexPositionEnumType.BehindAnchor.ToString() == selectedItem.Content.ToString()))
          return;
        this.position = FlexPositionEnumType.BehindAnchor;
      }
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      FlexBaseAnchorType componentAnchor = this.getComponentAnchor(this.targetComponent.Component);
      List<string> scopeRules = new List<string>(this.scopeRuleList.Count);
      foreach (ScopeRule scopeRule in this.scopeRuleList)
        scopeRules.Add(scopeRule.Value);
      string title = this.targetComponent.Title;
      this.flexHandler.AddWoCView(this.stableAnchorType, componentAnchor, this.position, this.targetComponent.Path, title, scopeRules);
      this.changesApplied = true;
      this.IsChangeApplied = true;
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }

    private void addScopingRuleButton_Click(object sender, RoutedEventArgs e)
    {
      UCScopeRulesEditor scopeRulesEditor = new UCScopeRulesEditor(this.scopeRuleList, this.DTExtensibleModel, false, true);
      if (scopeRulesEditor.ShowDialog() != System.Windows.Forms.DialogResult.OK)
        return;
      this.scopeRuleList = scopeRulesEditor.NewCollection;
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addwocviewmodeler.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.componentLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 2:
    //      this.targetTextBox = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 3:
    //      ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.targetCompButton_Click);
    //      break;
    //    case 4:
    //      this.positionListBox = (System.Windows.Controls.ComboBox) target;
    //      this.positionListBox.SelectionChanged += new SelectionChangedEventHandler(this.positionListBox_SelectionChanged);
    //      break;
    //    case 5:
    //      this.addScopeRulesButton = (System.Windows.Controls.Button) target;
    //      this.addScopeRulesButton.Click += new RoutedEventHandler(this.addScopingRuleButton_Click);
    //      break;
    //    case 6:
    //      this.okButton = (System.Windows.Controls.Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 7:
    //      this.cancelButton = (System.Windows.Controls.Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    internal struct TargetComponent
    {
      public UXComponent Component;
      public string Path;
      public string Title;
    }
  }
}
