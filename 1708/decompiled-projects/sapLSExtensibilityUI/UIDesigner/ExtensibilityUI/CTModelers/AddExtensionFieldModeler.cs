﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddExtensionFieldModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.API.ComponentDefinition;
using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Flexibility.CoreAPI;
using SAP.BYD.LS.UI.Flexibility.CoreAPI.Extensibility;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Connector.Services.Modeler;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.RepositoryLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public abstract partial class AddExtensionFieldModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private ObservableCollection<AddExtensionFieldModeler.SelectedItem> extFieldCollection = new ObservableCollection<AddExtensionFieldModeler.SelectedItem>();
    private const string TITLE_STRING = "Add Extension Field";
    private const string Identifier = "Identifier";
    protected FlexibilityHandler flexHandler;
    protected SectionGroupType sectionGroupType;
    protected ListType listType;
    private LinkType linkType;
    private FlexStableAnchorType stableAnchorType;
    private bool changesApplied;
    private Dictionary<string, string> sectionGrpQueryMapping;
    private Dictionary<string, string> dataFieldBindMapping;
    protected AbstractExtensionProcessor processor;
    protected bool isQueryExtension;
    private bool isSadlCase;
    private IFloorplan sourceComponent;
    //internal System.Windows.Controls.TextBox entityHeader;
    //internal System.Windows.Controls.TextBox errorTextBox;
    //internal System.Windows.Controls.ListView itemListView;
    //internal GridView listViewGridView;
    //internal Grid IdentifierArea;
    //internal Grid OVSArea;
    //internal System.Windows.Controls.TextBox ovsComponent;
    //internal System.Windows.Controls.Button selectComponentButton;
    //internal System.Windows.Controls.CheckBox preventQueryExecution;
    //internal System.Windows.Controls.CheckBox suggestEnabled;
    //internal System.Windows.Controls.CheckBox tokenEnabled;
    //internal System.Windows.Controls.TextBox explanation;
    //internal System.Windows.Controls.Button ConfigureOBNButton;
    //internal Grid OBNArea;
    //internal System.Windows.Controls.TextBox OBN_NS;
    //internal System.Windows.Controls.TextBox OBN_BO;
    //internal System.Windows.Controls.TextBox OBN_Node;
    //internal System.Windows.Controls.TextBox OBN_OP;
    //internal StackPanel ContentArea;
    //internal System.Windows.Controls.Button okButton;
    //internal System.Windows.Controls.Button cancelButton;
    //private bool _contentLoaded;

    public ObservableCollection<AddExtensionFieldModeler.SelectedItem> ExtFieldCollection
    {
      get
      {
        return this.extFieldCollection;
      }
    }

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public AddExtensionFieldModeler()
    {
      AddExtensionFieldModeler.SelectedItem.CheckBoxChanged += new EventHandler<EventArgs>(this.checkBoxChanged);
      this.Closing += new System.ComponentModel.CancelEventHandler(this.AddExtensionFieldModeler_Closing);
      this.InitializeComponent();
    }

    protected abstract void init();

    protected abstract List<FlexStableAnchorType> getStableAnchorList();

    protected abstract bool isAlreadyAdded(UIExtensionField item);

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.fillPresentationModeComboBox();
      this.sourceComponent = ModelLayer.ObjectManager.GetFloorplanModel(this.UIComponent, (FloorplanInfo) null, false, true, this.DTComponent.Version);
      this.isQueryExtension = this.DTExtensibleModel.Parent.GetOberonModel() is FindFormPaneType;
      this.Title = "Add Extension Field";
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      this.init();
      foreach (FlexStableAnchorType stableAnchor in this.getStableAnchorList())
      {
        if (this.Anchor.xrepPath == stableAnchor.xrepPath)
        {
          this.stableAnchorType = stableAnchor;
          break;
        }
      }
      if (this.stableAnchorType == null)
        return;
      bool flag = true;
      if (!string.IsNullOrEmpty(this.stableAnchorType.dataFieldPath) && !string.IsNullOrEmpty(this.stableAnchorType.containerId))
      {
        if (!string.IsNullOrEmpty(this.stableAnchorType.coreBoName) && !string.IsNullOrEmpty(this.stableAnchorType.coreBoName) && !string.IsNullOrEmpty(this.stableAnchorType.coreBoNameNamespace))
        {
          flag = false;
          this.isSadlCase = true;
        }
        else if (!string.IsNullOrEmpty(this.stableAnchorType.referenceBundleKey) && !string.IsNullOrEmpty(this.stableAnchorType.referenceFieldName))
        {
          flag = false;
          this.isSadlCase = false;
        }
      }
      if (flag)
      {
        this.printError("The selected anchor is not prepared for field extensibility");
      }
      else
      {
        this.assembleExtensionFieldInfo();
        this.optimizeControlHeigh();
      }
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      foreach (AddExtensionFieldModeler.SelectedItem extField in (Collection<AddExtensionFieldModeler.SelectedItem>) this.extFieldCollection)
      {
        if (!string.IsNullOrEmpty(extField.ChangeTransactionID))
          this.flexHandler.RemoveExtensionField(extField.ChangeTransactionID, extField.uxBaseDataType, extField.extField, this.processor);
      }
      this.IsChangeApplied = false;
    }

    private void optimizeControlHeigh()
    {
      int num = this.extFieldCollection.Count;
      if (this.extFieldCollection.Count > 5)
        num = 5;
      this.itemListView.Height = (double) (40 + num * 30);
      this.Height = this.itemListView.Height + 180.0;
    }

    private void getQueryBindingInfo(List<FlexStableAnchorType> anchorTypes)
    {
      DataModelDefinition instance = DataModelDefinition.GetInstance(this.UIComponent.Implementation.DataModel);
      this.sectionGrpQueryMapping = new Dictionary<string, string>();
      foreach (FlexStableAnchorType anchorType in anchorTypes)
      {
        string dataFieldPath = anchorType.dataFieldPath;
        string bindQueryName = instance.getBindQueryName(instance.getBindingExpression(dataFieldPath));
        if (bindQueryName != null)
        {
          using (List<UXQueryType>.Enumerator enumerator = this.UIComponent.Implementation.Queries.Query.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              UXQueryType current = enumerator.Current;
              if (current.name.Equals(bindQueryName))
              {
                string id = current.id;
                string containerId = anchorType.containerId;
                if (this.sectionGrpQueryMapping.ContainsKey(containerId))
                  break;
                this.sectionGrpQueryMapping.Add(containerId, id);
                break;
              }
            }
            break;
          }
        }
      }
    }

    private void getAssociationBindingInfo(List<FlexStableAnchorType> anchorTypes)
    {
      DataModelDefinition instance = DataModelDefinition.GetInstance(this.UIComponent.Implementation.DataModel);
      this.dataFieldBindMapping = new Dictionary<string, string>();
      foreach (FlexStableAnchorType anchorType in anchorTypes)
      {
        string str = "Root";
        string dataFieldPath = anchorType.dataFieldPath;
        string absoluteBackendBindInfo = instance.getAbsoluteBackendBindInfo(instance.getBindingExpression(dataFieldPath));
        if (absoluteBackendBindInfo != null)
          str = !absoluteBackendBindInfo.Contains("-~") ? absoluteBackendBindInfo : absoluteBackendBindInfo.Substring(0, absoluteBackendBindInfo.IndexOf("-~"));
        if (!this.dataFieldBindMapping.ContainsKey(dataFieldPath))
          this.dataFieldBindMapping.Add(dataFieldPath, str);
      }
    }

    private void assembleExtensionFieldInfo()
    {
      List<FlexStableAnchorType> anchorTypes = new List<FlexStableAnchorType>();
      anchorTypes.Add(this.stableAnchorType);
      if (this.isQueryExtension)
        this.getQueryBindingInfo(anchorTypes);
      else
        this.getAssociationBindingInfo(anchorTypes);
      if (!this.isSadlCase)
        this.getFields(new List<string>()
        {
          this.stableAnchorType.referenceBundleKey
        }, new List<string>()
        {
          this.stableAnchorType.referenceFieldName
        });
      else
        this.getSadlFields(new KeyValuePair<string, string>(this.stableAnchorType.coreBoName, this.stableAnchorType.coreBoNameNamespace), new KeyValuePair<string, string>(this.stableAnchorType.coreBoNodeName, this.stableAnchorType.coreBoName));
    }

    private void getSadlFields(KeyValuePair<string, string> coreBo, KeyValuePair<string, string> coreBoNode)
    {
      bool isTransientFieldsRquired = true;
      if (this.UIComponent.UXView != null && (this.UIComponent.UXView.patternType == PatternType.ovs || this.UIComponent.UXView.patternType == PatternType.owl))
        isTransientFieldsRquired = false;
      this.errorTextBox.Visibility = Visibility.Collapsed;
      __EXT__S_CUS_FACADE_UI_CONTEXT[] context = (__EXT__S_CUS_FACADE_UI_CONTEXT[]) null;
      __EXT__S_CUS_FACADE_UI_FIELD[] fields = (__EXT__S_CUS_FACADE_UI_FIELD[]) null;
      string empty = string.Empty;
      if (!XRepositoryProxy.Instance.GetExtensionFieldsForBO(coreBo, coreBoNode, this.Solution, out context, out fields, ref empty, isTransientFieldsRquired))
        this.printError(empty);
      this.setFields(context, fields);
    }

    private void setFields(__EXT__S_CUS_FACADE_UI_CONTEXT[] contextArray, __EXT__S_CUS_FACADE_UI_FIELD[] fieldArray)
    {
      List<BusinessContextInformation> customFieldsreferenceFields = new List<BusinessContextInformation>();
      for (int index = 0; index < contextArray.Length; ++index)
      {
        BusinessContextInformation contextInformation = new BusinessContextInformation();
        if (!this.isSadlCase)
          contextInformation.referenceField = new ReferenceField(contextArray[index].REFERENCE_FIELD.REF_BUNDLE_KEY, contextArray[index].REFERENCE_FIELD.REF_FIELD_NAME);
        contextInformation.Description = contextArray[index].DESCRIPTION;
        customFieldsreferenceFields.Add(contextInformation);
      }
      List<UIExtensionField> customFields = new List<UIExtensionField>();
      for (int index = 0; index < fieldArray.Length; ++index)
      {
        UIExtensionField uiExtensionField = new UIExtensionField();
        uiExtensionField.customField = new CustomFieldInformation();
        uiExtensionField.customField.Exists = fieldArray[index].CUSTOM_FIELD.EXISTS;
        uiExtensionField.customField.IsDeletionAllowed = fieldArray[index].CUSTOM_FIELD.IS_DELETION_ALLOWED;
        uiExtensionField.customField.IsToBeDeleted = fieldArray[index].CUSTOM_FIELD.IS_TO_BE_DELETED;
        uiExtensionField.customField.IsConsistent = fieldArray[index].CUSTOM_FIELD.IS_CONSISTENT;
        uiExtensionField.customField.Field = new ESRName(fieldArray[index].CUSTOM_FIELD.FIELD.NAME, fieldArray[index].CUSTOM_FIELD.FIELD.NAMESPACE, fieldArray[index].CUSTOM_FIELD.APPEARANCE_NAME);
        uiExtensionField.customField.FieldType = fieldArray[index].CUSTOM_FIELD.FIELD_TYPE;
        uiExtensionField.customField.Length = fieldArray[index].CUSTOM_FIELD.LENGTH;
        uiExtensionField.customField.Decimals = fieldArray[index].CUSTOM_FIELD.DECIMALS;
        uiExtensionField.customField.RepresentationTerm = fieldArray[index].CUSTOM_FIELD.REPRESENTATION_TERM;
        uiExtensionField.customField.IsTransient = fieldArray[index].TRANSIENT_INDICATOR;
        uiExtensionField.customField.UiText = new UIText(fieldArray[index].CUSTOM_FIELD.UI_TEXTS.CAPTION, fieldArray[index].CUSTOM_FIELD.UI_TEXTS.HEADING, fieldArray[index].CUSTOM_FIELD.UI_TEXTS.DESCRIPTION);
        uiExtensionField.businessContext = new BusinessContextInformation();
        if (!this.isSadlCase)
          uiExtensionField.businessContext.referenceField = new ReferenceField(fieldArray[index].REFERENCE_FIELD.REF_BUNDLE_KEY, fieldArray[index].REFERENCE_FIELD.REF_FIELD_NAME);
        uiExtensionField.businessContext.Description = fieldArray[index].DESCRIPTION;
        customFields.Add(uiExtensionField);
      }
      this.fillListBox(customFieldsreferenceFields, customFields);
    }

    private void getFields(List<string> bundleList, List<string> fieldList)
    {
      this.errorTextBox.Visibility = Visibility.Collapsed;
      __EXT__S_CUS_FACADE_UI_CONTEXT[] context = (__EXT__S_CUS_FACADE_UI_CONTEXT[]) null;
      __EXT__S_CUS_FACADE_UI_FIELD[] fields = (__EXT__S_CUS_FACADE_UI_FIELD[]) null;
      string empty = string.Empty;
      if (!XRepositoryProxy.Instance.GetExtensionFields(bundleList, fieldList, this.Solution, out context, out fields, ref empty))
        this.printError(empty);
      this.setFields(context, fields);
    }

    private string getQueryName(string queryId)
    {
      string str = "";
      foreach (UXQueryType uxQueryType in this.UIComponent.Implementation.Queries.Query)
      {
        if (uxQueryType.id.Equals(queryId))
        {
          str = uxQueryType.name;
          break;
        }
      }
      return str;
    }

    private void fillListBox(List<BusinessContextInformation> customFieldsreferenceFields, List<UIExtensionField> customFields)
    {
      this.extFieldCollection = new ObservableCollection<AddExtensionFieldModeler.SelectedItem>();
      this.itemListView.DataContext = (object) this.ExtFieldCollection;
      for (int index = 0; index < customFields.Count; ++index)
      {
        UIExtensionField customField = customFields[index];
        if (!(customField.customField.IsConsistent != "X") || !(customField.customField.IsConsistent != "x"))
        {
          customField.isAddedToGroup = this.isAlreadyAdded(customField);
          customField.businessContext.Anchor = this.stableAnchorType;
          if (this.isQueryExtension)
          {
            customField.isQueryExtension = true;
            this.sectionGrpQueryMapping.TryGetValue(this.stableAnchorType.containerId, out customField.queryId);
            customField.queryName = this.getQueryName(customField.queryId);
          }
          else
            this.dataFieldBindMapping.TryGetValue(this.stableAnchorType.dataFieldPath, out customField.associationPathAbsolute);
          if (!this.isAlreadyAdded(customField) && !this.isAlreadyHandled(customField))
          {
            AddExtensionFieldModeler.SelectedItem selectedItem = new AddExtensionFieldModeler.SelectedItem();
            selectedItem.ChangeTransactionID = null;
            selectedItem.RepType = customField.customField.FieldType;
            selectedItem.Decimals = customField.customField.Decimals;
            selectedItem.extField = customField;
            selectedItem.IsAdded = customField.isAddedToGroup;
            selectedItem.IsDisplayOnly = false;
            selectedItem.Name = customField.customField.UiText.Caption;
            selectedItem.RepType = customField.customField.RepresentationTerm;
            selectedItem.ToolTip = customField.customField.UiText.Description;
            selectedItem.EsrName = customField.customField.Field.CoreName;
            this.extFieldCollection.Add(selectedItem);
          }
        }
      }
    }

    private bool isAlreadyHandled(UIExtensionField extField)
    {
      return ExtFieldSessionHandler.Instance.Contains(this.flexHandler, extField);
    }

    private string getParentDataElementPath(string referencedDataFieldId)
    {
      string empty = string.Empty;
      DataModelDefinition instance = DataModelDefinition.GetInstance(this.UIComponent.Implementation.DataModel);
      string str = this.UIComponent.Implementation.DataModel.name;
      string bindingExpression = instance.getBindingExpression(referencedDataFieldId);
      if (!string.IsNullOrEmpty(bindingExpression))
        str = bindingExpression.Substring(0, bindingExpression.LastIndexOf('/'));
      return str;
    }

    private bool createFieldsOnEco(ref string errorMessage)
    {
      List<AppearanceInfo> infoList = new List<AppearanceInfo>();
      foreach (AddExtensionFieldModeler.SelectedItem extField in (Collection<AddExtensionFieldModeler.SelectedItem>) this.extFieldCollection)
      {
        if (extField.IsAdded && !string.IsNullOrEmpty(extField.ChangeTransactionID))
          infoList.Add(new AppearanceInfo()
          {
            FieldName = extField.extField.customField.Field.CoreName,
            FieldNameSpace = extField.extField.customField.Field.Namespace,
            ReferenceBundleKey = extField.extField.businessContext.referenceField.bundleKey,
            ReferenceFieldName = extField.extField.businessContext.referenceField.fieldName
          });
      }
      return XRepositoryProxy.Instance.CreateExtensionFieldsOnEco(this.flexHandler, this.Solution, infoList, ref errorMessage);
    }

    private void printError(string errorText)
    {
      this.errorTextBox.Text = errorText;
      this.errorTextBox.Foreground = (Brush) new SolidColorBrush(Colors.Red);
      this.errorTextBox.Visibility = Visibility.Visible;
    }

    private string isOBNConsistent(OBNConfigHelper obn)
    {
      if (obn.m_navigation == null)
        return "Please specify the OBN informations";
      if (obn.m_navigation.OutPlug == null)
        return "Please specify the PortType Informations";
      List<OBNSimpleBindingType> simpleParameters = obn.SimpleParameters;
      List<OBNListBindingType> listParameters = obn.ListParameters;
      if ((simpleParameters == null || simpleParameters.Count == 0) && (listParameters == null || listParameters.Count == 0))
        return "Please specify Parametermapping";
      if (string.IsNullOrEmpty(obn.NavBONS))
        return "Please specify the OBN BO-Namespace";
      if (string.IsNullOrEmpty(obn.NavBO))
        return "Please specify the OBN Business Object";
      if (string.IsNullOrEmpty(obn.NavBONode))
        return "Please specify the OBN BO-Node";
      if (string.IsNullOrEmpty(obn.NavOperation))
        return "Please specify the OBN BO-Operation";
      if (string.IsNullOrEmpty(obn.PortTypePackage))
        return "Please specify the PortType Package";
      if (string.IsNullOrEmpty(obn.PortTypeReference))
        return "Please specify the PortType Reference";
      return (string) null;
    }

    private void addExtensionField(AddExtensionFieldModeler.SelectedItem item)
    {
      UXBaseDataElementType dataFieldType = (UXBaseDataElementType) null;
      ModelEntity type = (ModelEntity) null;
      List<UXSearchParameterType> boQueryParams = (List<UXSearchParameterType>) null;
      string designtimeMainBoProxyName = this.UIComponent.Implementation.DataModel.designtimeMainBOProxyName;
      string mainBoEsrNamespace = this.UIComponent.Implementation.DataModel.designtimeMainBOEsrNamespace;
      string parentDataElementPath = this.getParentDataElementPath(item.extField.businessContext.Anchor.dataFieldPath);
      if (!string.IsNullOrEmpty(item.ChangeTransactionID))
        return;
      if (item.ovsInfoModel != null)
      {
        item.populateOVSInfo();
        if (item.OBNHelper != null)
        {
          string errorText = this.isOBNConsistent(item.OBNHelper);
          if (errorText != null)
          {
            this.printError(errorText);
            return;
          }
          item.populateOBNInfo();
        }
      }
      item.ChangeTransactionID = !this.isQueryExtension ? this.flexHandler.AddExtensionField(this.stableAnchorType, designtimeMainBoProxyName, mainBoEsrNamespace, item.IsDisplayOnly, item.extField, parentDataElementPath, this.processor, out dataFieldType, out type) : this.flexHandler.AddExtensionQueryParameter(this.stableAnchorType, item.extField, parentDataElementPath, this.processor, out dataFieldType, out type, out boQueryParams);
      item.uxBaseDataType = dataFieldType;
      item.extField.FieldName = dataFieldType.name;
    }

    private List<UXBaseDataElementType> getDatas(List<UXBaseDataElementType> dataModelItems, string path)
    {
      foreach (UXBaseDataElementType dataModelItem in dataModelItems)
      {
        if (dataModelItem.name.Equals(path))
        {
          if (dataModelItem is UXDataListType)
            return (dataModelItem as UXDataListType).Items;
          if (dataModelItem is UXDataStructureType)
            return (dataModelItem as UXDataStructureType).Items;
        }
      }
      return (List<UXBaseDataElementType>) null;
    }

    private bool isAdded(List<UXBaseDataElementType> dataModelItems, UXBaseDataElementType dataType)
    {
      foreach (NamedModelEntity dataModelItem in dataModelItems)
      {
        if (dataModelItem.name == dataType.name)
          return true;
      }
      return false;
    }

    private void insureAdded(UXDataModelType dataModel, UXBaseDataElementType dataType, string parentPath)
    {
      string[] strArray = parentPath.Split('/');
      List<UXBaseDataElementType> dataModelItems = dataModel.Items;
      for (int index = 2; index < strArray.Length; ++index)
      {
        dataModelItems = this.getDatas(dataModelItems, strArray[index]);
        if (dataModelItems == null)
          break;
      }
      if (dataModelItems == null || this.isAdded(dataModelItems, dataType))
        return;
      dataModelItems.Add(dataType);
    }

    private void removeExtensionField(AddExtensionFieldModeler.SelectedItem item)
    {
      if (string.IsNullOrEmpty(item.ChangeTransactionID))
        return;
      this.flexHandler.RemoveExtensionField(item.ChangeTransactionID, item.uxBaseDataType, item.extField, this.processor);
      item.ChangeTransactionID = (string) null;
    }

    private void removeAllExtensionFieldsFromModel()
    {
      foreach (AddExtensionFieldModeler.SelectedItem selectedItem in (IEnumerable) this.itemListView.Items)
        this.removeExtensionField(selectedItem);
    }

    private void fillPresentationModeComboBox()
    {
    }

    private void checkBoxChanged(object sender, EventArgs args)
    {
      this.errorTextBox.Visibility = Visibility.Collapsed;
      AddExtensionFieldModeler.SelectedItem selectedItem = sender as AddExtensionFieldModeler.SelectedItem;
      if (selectedItem == null)
        return;
      if (this.isQueryExtension)
        selectedItem.IsDisplayOnly = false;
      if (selectedItem.IsAdded)
      {
        this.addExtensionField(selectedItem);
      }
      else
      {
        this.removeExtensionField(selectedItem);
        selectedItem.ovsInfoModel = (AddExtensionFieldModeler.OVSInfoModel) null;
        selectedItem.OBNHelper = (OBNConfigHelper) null;
      }
      if (this.itemListView.SelectedItem == selectedItem)
        this.initializeOVSContent(selectedItem);
      else
        this.itemListView.SelectedItem = (object) selectedItem;
    }

    private void updateExtensionField(AddExtensionFieldModeler.SelectedItem item)
    {
      this.removeExtensionField(item);
      this.addExtensionField(item);
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      bool flag = false;
      foreach (AddExtensionFieldModeler.SelectedItem extField in (Collection<AddExtensionFieldModeler.SelectedItem>) this.extFieldCollection)
      {
        if (extField.IsAdded)
        {
          flag = true;
          if ((extField.ovsInfoModel != null || extField.OBNHelper != null) && extField.OBNHelper != null)
          {
            string str = this.isOBNConsistent(extField.OBNHelper);
            if (str != null)
            {
              this.printError("Field: '" + extField.Name + "':" + str);
              return;
            }
          }
          this.updateExtensionField(extField);
          ExtFieldSessionHandler.Instance.AddSelectedItem(extField);
        }
      }
      if (!flag)
      {
        this.Close();
      }
      else
      {
        if (!this.isSadlCase)
        {
          string empty = string.Empty;
          if (!this.createFieldsOnEco(ref empty))
          {
            this.printError(empty);
            return;
          }
        }
        this.changesApplied = true;
        this.IsChangeApplied = true;
        this.Close();
      }
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void AddExtensionFieldModeler_Closing(object sender, CancelEventArgs e)
    {
      XRepositoryProxy.Instance.CloseStatefullClient();
      this.revertChanges();
      AddExtensionFieldModeler.SelectedItem.CheckBoxChanged -= new EventHandler<EventArgs>(this.checkBoxChanged);
      AddExtensionFieldModeler.SelectedItem.CheckBoxChanged = (EventHandler<EventArgs>) null;
    }

    private List<FloorplanType> getSelectedFloorType()
    {
      return new List<FloorplanType>() { FloorplanType.OVS };
    }

    private void selectComponentButton_Click(object sender, RoutedEventArgs e)
    {
      AddExtensionFieldModeler.SelectedItem selectedItem = (AddExtensionFieldModeler.SelectedItem) this.itemListView.SelectedItem;
      if (selectedItem == null)
        return;
      AddExtensionFieldModeler.OVSInfoModel ovsInfoModel = selectedItem.ovsInfoModel;
      if (ovsInfoModel == null)
        return;
      ovsInfoModel.OVSComponent = "";
      RepositoryFilterBrowser repositoryFilterBrowser = new RepositoryFilterBrowser(this.getSelectedFloorType(), (FloorplanInfo) null);
      if (repositoryFilterBrowser.ShowDialog() != System.Windows.Forms.DialogResult.OK || repositoryFilterBrowser.SelectedComponent == null)
        return;
      FloorplanInfo selectedComponent = repositoryFilterBrowser.SelectedComponent;
      UXComponent uxComponent = ProjectWorkspaceManager.Instance.GetUXComponent(ref selectedComponent, false);
      foreach (RepositoryAttribute attribute in selectedComponent.Attributes)
      {
        if (attribute.Name.Equals("~SOLUTION") && attribute.Value.Equals(LoginManager.Instance.Solution))
        {
          ovsInfoModel.OVSComponent = repositoryFilterBrowser.SelectedComponent.UniqueId;
          this.errorTextBox.Visibility = Visibility.Collapsed;
          return;
        }
      }
      if (uxComponent.Tags != null)
      {
        foreach (TagType tagType in uxComponent.Tags.Tag)
        {
          if (tagType.attribute.Equals("ISVL") && tagType.value.Equals("X"))
          {
            ovsInfoModel.OVSComponent = repositoryFilterBrowser.SelectedComponent.UniqueId;
            this.errorTextBox.Visibility = Visibility.Collapsed;
            return;
          }
        }
      }
      this.printError("The selected OVS may not be used by ISV");
    }

    private void selectConfigureOBNButton_Click(object sender, RoutedEventArgs e)
    {
      AddExtensionFieldModeler.SelectedItem selectedItem = (AddExtensionFieldModeler.SelectedItem) this.itemListView.SelectedItem;
      if (selectedItem == null)
        return;
      if (selectedItem.OBNHelper == null)
        selectedItem.OBNHelper = new OBNConfigHelper(this.UIComponent, this.DTComponent);
      if (selectedItem.OBNHelper == null)
        return;
      selectedItem.OBNHelper.configureOBN();
      if (selectedItem.OBNHelper.m_outportConfig == null || selectedItem.OBNHelper.m_outportConfig.CurrentOutport == null)
        this.OBNArea.Visibility = Visibility.Collapsed;
      else if (selectedItem.OBNHelper.NavBONS == null && selectedItem.OBNHelper.NavBO == null)
      {
        selectedItem.OBNHelper = (OBNConfigHelper) null;
      }
      else
      {
        this.OBNArea.Visibility = Visibility.Visible;
        if (selectedItem.OBNHelper.m_outportConfig == null)
          return;
        this.OBN_NS.Text = selectedItem.OBNHelper.NavBONS;
        this.OBN_BO.Text = selectedItem.OBNHelper.NavBO;
        this.OBN_Node.Text = selectedItem.OBNHelper.NavBONode;
        this.OBN_OP.Text = selectedItem.OBNHelper.NavOperation;
        this.ContentArea.Children.Clear();
        this.ContentArea.Children.Add((UIElement) selectedItem.OBNHelper.m_outportConfig);
        selectedItem.OBNHelper.configureParams();
      }
    }

    private void initializeOVSContent(AddExtensionFieldModeler.SelectedItem selectedItem)
    {
    }

    private void itemListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      AddExtensionFieldModeler.SelectedItem selectedItem = (AddExtensionFieldModeler.SelectedItem) null;
      if (e != null && e.AddedItems != null && e.AddedItems.Count > 0)
        selectedItem = (AddExtensionFieldModeler.SelectedItem) e.AddedItems[0];
      this.initializeOVSContent(selectedItem);
    }

    private void presentationMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      AddExtensionFieldModeler.SelectedItem selectedItem = (AddExtensionFieldModeler.SelectedItem) this.itemListView.SelectedItem;
      if (selectedItem == null)
        return;
      AddExtensionFieldModeler.OVSInfoModel ovsInfoModel = selectedItem.ovsInfoModel;
      if (ovsInfoModel == null)
        return;
      ComboBoxItem comboBoxItem = (ComboBoxItem) null;
      if (e != null && e.AddedItems != null && e.AddedItems.Count > 0)
        comboBoxItem = (ComboBoxItem) e.AddedItems[0];
      ovsInfoModel.PresentationMode = (IdentifierPresentationModeType) comboBoxItem.Content;
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addextensionfieldmodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.entityHeader = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 2:
    //      this.errorTextBox = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 3:
    //      this.itemListView = (System.Windows.Controls.ListView) target;
    //      this.itemListView.SelectionChanged += new SelectionChangedEventHandler(this.itemListView_SelectionChanged);
    //      break;
    //    case 4:
    //      this.listViewGridView = (GridView) target;
    //      break;
    //    case 5:
    //      this.IdentifierArea = (Grid) target;
    //      break;
    //    case 6:
    //      this.OVSArea = (Grid) target;
    //      break;
    //    case 7:
    //      this.ovsComponent = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 8:
    //      this.selectComponentButton = (System.Windows.Controls.Button) target;
    //      this.selectComponentButton.Click += new RoutedEventHandler(this.selectComponentButton_Click);
    //      break;
    //    case 9:
    //      this.preventQueryExecution = (System.Windows.Controls.CheckBox) target;
    //      break;
    //    case 10:
    //      this.suggestEnabled = (System.Windows.Controls.CheckBox) target;
    //      break;
    //    case 11:
    //      this.tokenEnabled = (System.Windows.Controls.CheckBox) target;
    //      break;
    //    case 12:
    //      this.explanation = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 13:
    //      this.ConfigureOBNButton = (System.Windows.Controls.Button) target;
    //      this.ConfigureOBNButton.Click += new RoutedEventHandler(this.selectConfigureOBNButton_Click);
    //      break;
    //    case 14:
    //      this.OBNArea = (Grid) target;
    //      break;
    //    case 15:
    //      this.OBN_NS = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 16:
    //      this.OBN_BO = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 17:
    //      this.OBN_Node = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 18:
    //      this.OBN_OP = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 19:
    //      this.ContentArea = (StackPanel) target;
    //      break;
    //    case 20:
    //      this.okButton = (System.Windows.Controls.Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 21:
    //      this.cancelButton = (System.Windows.Controls.Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    public class OVSInfoModel : DependencyObject
    {
      public static readonly DependencyProperty WindowsTitleProperty = DependencyProperty.Register("WindowsTitle", typeof (string), typeof (AddExtensionFieldModeler.OVSInfoModel), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.OVSInfoModel.isPropertyChangeCallback)));
      public static readonly DependencyProperty OVSComponentProperty = DependencyProperty.Register("OVSComponent", typeof (string), typeof (AddExtensionFieldModeler.OVSInfoModel), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.OVSInfoModel.isPropertyChangeCallback)));
      public static readonly DependencyProperty ExplanationProperty = DependencyProperty.Register("Explanation", typeof (string), typeof (AddExtensionFieldModeler.OVSInfoModel), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.OVSInfoModel.isPropertyChangeCallback)));
      public static readonly DependencyProperty IsUuidSupportProperty = DependencyProperty.Register("IsUuidSupport", typeof (bool?), typeof (AddExtensionFieldModeler.OVSInfoModel), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.OVSInfoModel.isPropertyChangeCallback)));
      public static readonly DependencyProperty IsInterpretAsIntegerProperty = DependencyProperty.Register("IsInterpretAsInteger", typeof (bool), typeof (AddExtensionFieldModeler.OVSInfoModel), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.OVSInfoModel.isPropertyChangeCallback)));
      public static readonly DependencyProperty IsPreventQueryExecutionProperty = DependencyProperty.Register("IsPreventQueryExecution", typeof (bool), typeof (AddExtensionFieldModeler.OVSInfoModel), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.OVSInfoModel.isPropertyChangeCallback)));
      public static readonly DependencyProperty IsSuggestEnabledProperty = DependencyProperty.Register("IsSuggestEnabled", typeof (bool), typeof (AddExtensionFieldModeler.OVSInfoModel), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.OVSInfoModel.isPropertyChangeCallback)));
      public static readonly DependencyProperty IsTokenEnabledProperty = DependencyProperty.Register("IsTokenEnabled", typeof (bool), typeof (AddExtensionFieldModeler.OVSInfoModel), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.OVSInfoModel.isPropertyChangeCallback)));
      public static readonly DependencyProperty PresentationModeProperty = DependencyProperty.Register("PresentationMode", typeof (IdentifierPresentationModeType), typeof (AddExtensionFieldModeler.OVSInfoModel), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.OVSInfoModel.isPropertyChangeCallback)));

      public string WindowsTitle
      {
        get
        {
          return (string) this.GetValue(AddExtensionFieldModeler.OVSInfoModel.WindowsTitleProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.OVSInfoModel.WindowsTitleProperty, (object) value);
        }
      }

      public string OVSComponent
      {
        get
        {
          return (string) this.GetValue(AddExtensionFieldModeler.OVSInfoModel.OVSComponentProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.OVSInfoModel.OVSComponentProperty, (object) value);
        }
      }

      public string Explanation
      {
        get
        {
          return (string) this.GetValue(AddExtensionFieldModeler.OVSInfoModel.ExplanationProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.OVSInfoModel.ExplanationProperty, (object) value);
        }
      }

      public bool? IsUuidSupport
      {
        get
        {
          return (bool?) this.GetValue(AddExtensionFieldModeler.OVSInfoModel.IsUuidSupportProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.OVSInfoModel.IsUuidSupportProperty, (object) value);
        }
      }

      public bool IsInterpretAsInteger
      {
        get
        {
          return (bool) this.GetValue(AddExtensionFieldModeler.OVSInfoModel.IsInterpretAsIntegerProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.OVSInfoModel.IsInterpretAsIntegerProperty, (object) value);
        }
      }

      public bool IsPreventQueryExecution
      {
        get
        {
          return (bool) this.GetValue(AddExtensionFieldModeler.OVSInfoModel.IsPreventQueryExecutionProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.OVSInfoModel.IsPreventQueryExecutionProperty, (object) value);
        }
      }

      public bool IsSuggestEnabled
      {
        get
        {
          return (bool) this.GetValue(AddExtensionFieldModeler.OVSInfoModel.IsSuggestEnabledProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.OVSInfoModel.IsSuggestEnabledProperty, (object) value);
        }
      }

      public bool IsTokenEnabled
      {
        get
        {
          return (bool) this.GetValue(AddExtensionFieldModeler.OVSInfoModel.IsTokenEnabledProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.OVSInfoModel.IsTokenEnabledProperty, (object) value);
        }
      }

      public IdentifierPresentationModeType PresentationMode
      {
        get
        {
          return (IdentifierPresentationModeType) this.GetValue(AddExtensionFieldModeler.OVSInfoModel.PresentationModeProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.OVSInfoModel.PresentationModeProperty, (object) value);
        }
      }

      public OVSInfoModel()
      {
        this.IsSuggestEnabled = true;
        this.IsTokenEnabled = true;
        this.IsPreventQueryExecution = false;
        this.IsInterpretAsInteger = false;
        this.PresentationMode = IdentifierPresentationModeType.IDAndDescription;
      }

      private static void isPropertyChangeCallback(DependencyObject dp, DependencyPropertyChangedEventArgs args)
      {
      }
    }

    public class SelectedItem : DependencyObject
    {
      public static readonly DependencyProperty IsDisplayOnlyProperty = DependencyProperty.Register("IsDisplayOnly", typeof (bool), typeof (AddExtensionFieldModeler.SelectedItem), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.SelectedItem.isPropertyChangeCallback)));
      public static readonly DependencyProperty IsAddedProperty = DependencyProperty.Register("IsAdded", typeof (bool), typeof (AddExtensionFieldModeler.SelectedItem), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddExtensionFieldModeler.SelectedItem.isPropertyChangeCallback)));
      public static EventHandler<EventArgs> CheckBoxChanged;
      public UIExtensionField extField;
      public string ChangeTransactionID;
      public UXBaseDataElementType uxBaseDataType;
      public AddExtensionFieldModeler.OVSInfoModel ovsInfoModel;

      public string Name { get; set; }

      public string ToolTip { get; set; }

      public string Type { get; set; }

      public string Decimals { get; set; }

      public string RepType { get; set; }

      public string EsrName { get; set; }

      public OBNConfigHelper OBNHelper { get; set; }

      public bool IsDisplayOnly
      {
        get
        {
          return (bool) this.GetValue(AddExtensionFieldModeler.SelectedItem.IsDisplayOnlyProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.SelectedItem.IsDisplayOnlyProperty, (object) value);
        }
      }

      public bool IsAdded
      {
        get
        {
          return (bool) this.GetValue(AddExtensionFieldModeler.SelectedItem.IsAddedProperty);
        }
        set
        {
          this.SetValue(AddExtensionFieldModeler.SelectedItem.IsAddedProperty, (object) value);
        }
      }

      private static void isPropertyChangeCallback(DependencyObject dp, DependencyPropertyChangedEventArgs args)
      {
        if (AddExtensionFieldModeler.SelectedItem.CheckBoxChanged == null)
          return;
        AddExtensionFieldModeler.SelectedItem.CheckBoxChanged((object) dp, EventArgs.Empty);
      }

      public void populateOVSInfo()
      {
        if (this.ovsInfoModel == null || this.extField == null || this.RepType != "Identifier")
          return;
        UIExtensionOVSField extensionOvsField = new UIExtensionOVSField();
        if (!string.IsNullOrEmpty(this.ovsInfoModel.Explanation))
          extensionOvsField.explanationFallback = this.ovsInfoModel.Explanation;
        if (this.ovsInfoModel.IsUuidSupport.HasValue)
          extensionOvsField.uuidSupport = this.ovsInfoModel.IsUuidSupport;
        if (this.ovsInfoModel.IsInterpretAsInteger)
          extensionOvsField.interpretAsInteger = new bool?(this.ovsInfoModel.IsInterpretAsInteger);
        if (this.ovsInfoModel.PresentationMode != IdentifierPresentationModeType.IDAndDescription)
          extensionOvsField.presentationMode = new IdentifierPresentationModeType?(this.ovsInfoModel.PresentationMode);
        if (!this.IsDisplayOnly)
        {
          if (!string.IsNullOrEmpty(this.ovsInfoModel.WindowsTitle))
            extensionOvsField.windowsTitleFallback = this.ovsInfoModel.WindowsTitle;
          if (!string.IsNullOrEmpty(this.ovsInfoModel.OVSComponent))
            extensionOvsField.ovsComponentPath = this.ovsInfoModel.OVSComponent;
          if (this.ovsInfoModel.IsPreventQueryExecution)
            extensionOvsField.preventQueryExecution = new bool?(this.ovsInfoModel.IsPreventQueryExecution);
          if (!this.ovsInfoModel.IsSuggestEnabled)
            extensionOvsField.suggestEnabled = new bool?(this.ovsInfoModel.IsSuggestEnabled);
          if (!this.ovsInfoModel.IsTokenEnabled)
            extensionOvsField.tokenEnabled = new bool?(this.ovsInfoModel.IsTokenEnabled);
        }
        this.extField.OVSInfo = extensionOvsField;
      }

      public void populateOBNInfo()
      {
        if (this.ovsInfoModel == null || this.extField == null || (this.RepType != "Identifier" || this.OBNHelper == null))
          return;
        this.extField.OVSInfo.OBNInfo = new UIExtensionOBNField()
        {
          bo_ns = this.OBNHelper.NavBONS,
          bo_name = this.OBNHelper.NavBO,
          bo_node = this.OBNHelper.NavBONode,
          bo_op = this.OBNHelper.NavOperation,
          ctx_attr = this.OBNHelper.NavContextAttribute,
          ctx_const = this.OBNHelper.isConstantNavContext ? "X" : (string) null,
          ptp_package = this.OBNHelper.PortTypePackage,
          ptp_ref = this.OBNHelper.PortTypeReference,
          mapping = this.OBNHelper.SimpleParameters,
          listMapping = this.OBNHelper.ListParameters
        };
      }
    }
  }
}
