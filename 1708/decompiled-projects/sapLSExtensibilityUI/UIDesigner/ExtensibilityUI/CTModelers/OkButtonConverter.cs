﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.OkButtonConverter
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public class OkButtonConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if ((values[0] as ItemCollection).Count > 0 || (values[1] as ItemCollection).Count > 0 || (values[2] as ItemCollection).Count > 0)
        return (object) true;
      return (object) false;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      return (object[]) null;
    }
  }
}
