﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.ReasonEmptyRule
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using System.Globalization;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public class ReasonEmptyRule : ValidationRule
  {
    public override ValidationResult Validate(object value, CultureInfo cultureInfo)
    {
      if (string.IsNullOrEmpty((string) value))
        return new ValidationResult(false, (object) "the reason text could not be empty");
      return ValidationResult.ValidResult;
    }
  }
}
