﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddMashupsToFloorplanAnchorModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Flexibility.StableAnchor;
using SAP.BYD.LS.UI.Mashup.Common;
using SAP.BYD.LS.UI.Mashup.Tools.Service;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddMashupsToFloorplanAnchorModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private LoadMashupPipesWorker loader;
    private bool isOverViewOrAssignedObjects;
    private MashupFlexibilityHandler mashupFlexHandler;
    private List<SemanticGroupViewModel> semanticGroupViewModels;
    private List<MashupViewModel> mashupViewModels;
    private FlexibilityHandler flexHandler;
    private Dictionary<string, string> changeHistory;
    private List<string> changeIDs;
    private bool changesApplied;
    private Dictionary<string, ExistingMashupsInfo> existingMashups;
    private MashupViewHelper mashupViewHelper;
    private UXOutPortType extensionFieldOutPort;
    //internal AddMashupsBaseControl BaseControl;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return false;
      }
    }

    public AddMashupsToFloorplanAnchorModeler()
    {
      this.InitializeComponent();
      this.Title = "Widget Mashups Management";
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null || this.Anchor == null)
        return;
      this.isOverViewOrAssignedObjects = StableAnchorHelper.GetStableOverviewAnchor(this.UIComponent) != null || StableAnchorHelper.GetStableAssignedObjectAnchor(this.UIComponent) != null;
      this.mashupFlexHandler = this.flexHandler.GetMashupFlexibilityHandler();
      this.semanticGroupViewModels = new List<SemanticGroupViewModel>();
      this.mashupViewModels = new List<MashupViewModel>();
      this.changeIDs = new List<string>();
      this.changeHistory = new Dictionary<string, string>();
      this.existingMashups = new Dictionary<string, ExistingMashupsInfo>();
      this.UpdateExtensionFieldOutPort();
      this.mashupViewHelper = new MashupViewHelper(this.BaseControl, this.changeHistory, this.mashupFlexHandler, this.changeIDs, this.flexHandler, this.UIComponent, this.UIComponentPath, this.Anchor, this.extensionFieldOutPort);
      this.Render();
    }

    private void UpdateExtensionFieldOutPort()
    {
      this.extensionFieldOutPort = (UXOutPortType) null;
      UXComponent uiComponent = this.UIComponent;
      if (uiComponent == null || uiComponent.Interface == null || uiComponent.Interface.OutPorts == null)
        return;
      List<UXOutPortType> outPort = uiComponent.Interface.OutPorts.OutPort;
      if (outPort == null)
        return;
      foreach (UXOutPortType uxOutPortType in outPort.Reverse<UXOutPortType>())
      {
        if (uxOutPortType != null && uxOutPortType.name == "ExtensionFieldsOutPort")
        {
          this.extensionFieldOutPort = uxOutPortType;
          break;
        }
      }
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
      this.Terminate();
    }

    private void LoadMashups()
    {
      if (StableAnchorHelper.GetStableFloorPlanAnchor(this.UIComponent) != null || this.isOverViewOrAssignedObjects)
      {
        this.loader = new LoadMashupPipesWorker();
        List<MashupPipeInfo> mashupPipeInfoList = this.loader.Load(new List<string>()
        {
          "Web_Widget"
        }, new List<MashupTypeCategories>()
        {
          MashupTypeCategories.Data_Mashup,
          MashupTypeCategories.Html_Mashup
        }, 1 != 0);
        if (mashupPipeInfoList != null)
        {
          AddMashupsBaseControl.LoadExistingChangeTransaction(this.UIComponentPath, this.UIComponent, this.AnchorPath, ref this.existingMashups, ref this.changeHistory);
          string currentViewTypeId = AddMashupsToFloorplanAnchorModeler.GetCurrentViewTypeID(this.UIComponent);
          if (!string.IsNullOrEmpty(currentViewTypeId))
          {
            foreach (MashupPipeInfo mashupPipeInfo in mashupPipeInfoList)
            {
              MashupViewStyleType mashupViewStyleType = (MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupPipeInfo.ViewStyle, false);
              if (string.IsNullOrEmpty(mashupPipeInfo.DynParams))
              {
                bool flag1 = this.existingMashups.ContainsKey(mashupPipeInfo.PipeID);
                string str = !string.IsNullOrEmpty(mashupPipeInfo.DomainUrl) ? mashupPipeInfo.DomainUrl.Trim() : string.Empty;
                bool flag2 = flag1 && this.existingMashups[mashupPipeInfo.PipeID].FullColumnSpan;
                MashupViewModel mashupViewModel = new MashupViewModel()
                {
                  DisplayName = mashupPipeInfo.DisplayName,
                  Description = mashupPipeInfo.Description,
                  FullColumnSpan = flag2,
                  IsVisible = flag1,
                  ComponentPath = mashupPipeInfo.FilePath,
                  MashupCategory = mashupPipeInfo.MashupCategory,
                  SemanticCategory = mashupPipeInfo.SemanticCategory,
                  MashupCategoryDescription = MashupRegistry.GetMashupCategoryDisplayNameByName(mashupPipeInfo.MashupCategory),
                  PipeID = mashupPipeInfo.PipeID,
                  IconUrl = str,
                  MashupType = mashupPipeInfo.MashupType,
                  ViewStyle = mashupPipeInfo.ViewStyle,
                  MashupInPlug = mashupPipeInfo.InPort,
                  MashupOutPlug = mashupPipeInfo.OutPort,
                  SourceOutPlug = string.Empty,
                  TargetInPlug = string.Empty,
                  AppearanceOptions = new ObservableCollection<MashupValueHelpListModel>(),
                  OneAppearance = true,
                  MultipleAppearances = false,
                  Appearance = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.NewPaneContainer),
                  AppearanceIndex = 0,
                  ContainerID = currentViewTypeId,
                  ReferencedAnchorXrepPath = this.Anchor.xrepPath,
                  NotExisting = !flag1,
                  MandatoryParams = mashupPipeInfo.MandParams ?? string.Empty,
                  VisibilityBindingExpression = flag1 ? this.existingMashups[mashupPipeInfo.PipeID].VisibilityBindingExp : string.Empty
                };
                if (!mashupViewStyleType.Equals((object) MashupViewStyleType.inScreen))
                {
                  mashupViewModel.AppearanceKey = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.FunctionBar);
                  mashupViewModel.ServiceAppearance = "Web Services Menu";
                }
                else
                {
                  mashupViewModel.AppearanceKey = "V-" + currentViewTypeId;
                  mashupViewModel.ServiceAppearance = "New Screen Section";
                }
                mashupViewModel.AppearanceOptions.Add(new MashupValueHelpListModel()
                {
                  Code = mashupViewModel.AppearanceKey,
                  Text = mashupViewModel.ServiceAppearance
                });
                mashupViewModel.SetDynamicBindingOptionandVisibleAppearance(this.UIComponent, this.existingMashups, (string) null);
                mashupViewModel.IsPropertyPanelEnabled = !mashupViewModel.IsVisible;
                if (mashupViewModel.IsDynamic)
                  mashupViewModel.SourceOutPlug = "ExtensionFieldsOutPort";
                this.mashupViewModels.Add(mashupViewModel);
              }
            }
          }
        }
      }
      this.semanticGroupViewModels = this.mashupViewModels.GroupBy<MashupViewModel, string>((Func<MashupViewModel, string>) (serviceViewModel => serviceViewModel.SemanticCategory)).OrderBy<IGrouping<string, MashupViewModel>, string>((Func<IGrouping<string, MashupViewModel>, string>) (g => g.ToArray<MashupViewModel>()[0].SemanticCategory)).Select<IGrouping<string, MashupViewModel>, SemanticGroupViewModel>((Func<IGrouping<string, MashupViewModel>, SemanticGroupViewModel>) (g => new SemanticGroupViewModel()
      {
        SemanticCategory = g.Key,
        DisplayName = SemanticCategory.Instance[g.Key],
        Mashups = g.ToList<MashupViewModel>()
      })).ToList<SemanticGroupViewModel>();
    }

    public static string GetCurrentViewTypeID(UXComponent uxComponent)
    {
      string navigationItemId = ExtensibilityUIControl.CurrentSelectedNavigationItemID;
      string str = string.Empty;
      ViewContainerRegionType viewContainerRegion = uxComponent.UXView.ViewContainerRegion;
      if (viewContainerRegion != null)
      {
        List<ViewType> view = viewContainerRegion.View;
        if (view != null && view.Count > 0)
        {
          if (string.IsNullOrEmpty(navigationItemId))
          {
            str = view[0].id;
          }
          else
          {
            foreach (ViewType viewType in view)
            {
              if (viewType.navigationItemId == navigationItemId)
                str = viewType.id;
            }
          }
        }
      }
      return str;
    }

    private void Render()
    {
      this.SubscribeEvents();
      this.LoadMashups();
      this.BaseControl.MashupGroups.ItemsSource = (IEnumerable) this.semanticGroupViewModels;
    }

    private void ButtonApply_Click(object sender, RoutedEventArgs e)
    {
      this.changesApplied = true;
      this.IsChangeApplied = true;
      this.Close();
    }

    private void ButtonCancel_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      foreach (string changeId in this.changeIDs)
        this.mashupFlexHandler.RevertChangeTransactionInSession(changeId);
      this.IsChangeApplied = false;
    }

    private void MashupAppearence_Changed(object sender, SelectionChangedEventArgs e)
    {
      ComboBox comboBox = sender as ComboBox;
      if (comboBox == null || comboBox.SelectedItem == null)
        return;
      string key = (comboBox.SelectedItem as MashupValueHelpListModel).Key;
      MashupViewModel dataContext = comboBox.DataContext as MashupViewModel;
      string appearanceKey = dataContext.AppearanceKey;
      dataContext.AppearanceKey = key;
      if (!dataContext.IsVisible || !(appearanceKey != key) || !dataContext.NotExisting)
        return;
      this.mashupViewHelper.HandleInsertMashup(dataContext, (List<FlexBaseAnchor>) null);
    }

    private void MashupVisibilityBindingChanged(object sender, SelectionChangedEventArgs e)
    {
      this.mashupViewHelper.VisibilityBindingChanged(sender, (List<FlexBaseAnchor>) null);
    }

    private void MashupDynamicParameterChanged(object sender, SelectionChangedEventArgs e)
    {
      this.mashupViewHelper.DynamicParameterChanged(sender, (List<FlexBaseAnchor>) null, (UXComponent) null, true, (FlexBaseAnchorType) null);
    }

    private void MashupFullColumn_Checked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.FullColumn_Checked(sender, (List<FlexBaseAnchor>) null);
    }

    private void MashupFullColumn_Unchecked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.FullColumn_Unchecked(sender, (List<FlexBaseAnchor>) null);
    }

    private void MashupVisibilityCheckbox_Checked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.VisibilityCheckbox_Checked(sender, (List<FlexBaseAnchor>) null);
    }

    private void MashupVisibilityCheckbox_Unchecked(object sender, RoutedEventArgs e)
    {
      this.mashupViewHelper.VisibilityCheckbox_Unchecked(sender);
    }

    private void SubscribeEvents()
    {
      this.BaseControl.ApplyClicked += new AddMashupsBaseControl.ApplyEventHandler(this.ButtonApply_Click);
      this.BaseControl.CancelClicked += new AddMashupsBaseControl.CancelEventHandler(this.ButtonCancel_Click);
      this.BaseControl.MashupChecked += new AddMashupsBaseControl.MashupCheckedEventHandler(this.MashupVisibilityCheckbox_Checked);
      this.BaseControl.MashupUnchecked += new AddMashupsBaseControl.MashupUncheckedEventHandler(this.MashupVisibilityCheckbox_Unchecked);
      this.BaseControl.MashupFullColumnChecked += new AddMashupsBaseControl.MashupFullColumnSpanCheckedEventHandler(this.MashupFullColumn_Checked);
      this.BaseControl.MashupFullColumnUnchecked += new AddMashupsBaseControl.MashupFullColumnSpanUnCheckedEventHandler(this.MashupFullColumn_Unchecked);
      this.BaseControl.DynamicParameterChanged += new AddMashupsBaseControl.MashupDynamicParameterChangedEventHander(this.MashupDynamicParameterChanged);
      this.BaseControl.MashupAppearanceChanged += new AddMashupsBaseControl.MashupAppearanceChangedEventHandler(this.MashupAppearence_Changed);
      this.BaseControl.VisibilityBindingChanged += new AddMashupsBaseControl.MashupVisibilityBindingChangedEventHandler(this.MashupVisibilityBindingChanged);
    }

    private void UnsubscribeEvents()
    {
      this.BaseControl.ApplyClicked -= new AddMashupsBaseControl.ApplyEventHandler(this.ButtonApply_Click);
      this.BaseControl.CancelClicked -= new AddMashupsBaseControl.CancelEventHandler(this.ButtonCancel_Click);
      this.BaseControl.MashupChecked -= new AddMashupsBaseControl.MashupCheckedEventHandler(this.MashupVisibilityCheckbox_Checked);
      this.BaseControl.MashupUnchecked -= new AddMashupsBaseControl.MashupUncheckedEventHandler(this.MashupVisibilityCheckbox_Unchecked);
      this.BaseControl.MashupFullColumnChecked -= new AddMashupsBaseControl.MashupFullColumnSpanCheckedEventHandler(this.MashupFullColumn_Checked);
      this.BaseControl.MashupFullColumnUnchecked -= new AddMashupsBaseControl.MashupFullColumnSpanUnCheckedEventHandler(this.MashupFullColumn_Unchecked);
      this.BaseControl.DynamicParameterChanged -= new AddMashupsBaseControl.MashupDynamicParameterChangedEventHander(this.MashupDynamicParameterChanged);
      this.BaseControl.MashupAppearanceChanged -= new AddMashupsBaseControl.MashupAppearanceChangedEventHandler(this.MashupAppearence_Changed);
      this.BaseControl.VisibilityBindingChanged -= new AddMashupsBaseControl.MashupVisibilityBindingChangedEventHandler(this.MashupVisibilityBindingChanged);
    }

    private void Terminate()
    {
      this.UnsubscribeEvents();
      if (this.semanticGroupViewModels != null)
      {
        this.semanticGroupViewModels.Clear();
        this.semanticGroupViewModels = (List<SemanticGroupViewModel>) null;
      }
      if (this.mashupViewModels != null)
      {
        this.mashupViewModels.Clear();
        this.mashupViewModels = (List<MashupViewModel>) null;
      }
      if (this.changeIDs != null)
      {
        this.changeIDs.Clear();
        this.changeIDs = (List<string>) null;
      }
      if (this.changeHistory != null)
      {
        this.changeHistory.Clear();
        this.changeHistory = (Dictionary<string, string>) null;
      }
      this.flexHandler = (FlexibilityHandler) null;
      this.mashupFlexHandler = (MashupFlexibilityHandler) null;
      this.loader = (LoadMashupPipesWorker) null;
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addmashupstofloorplananchormodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  if (connectionId == 1)
    //    this.BaseControl = (AddMashupsBaseControl) target;
    //  else
    //    this._contentLoaded = true;
    //}
  }
}
