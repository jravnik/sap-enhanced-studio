﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.PropertyValueHolder
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public class PropertyValueHolder
  {
    public PropertyValues Value { get; set; }

    public string Field { get; set; }

    public string BO { get; set; }

    public string Namespace { get; set; }

    public string FieldExtensionNamespace { get; set; }

    public PropertyValueHolder()
    {
      this.BO = string.Empty;
      this.Namespace = string.Empty;
      this.FieldExtensionNamespace = string.Empty;
    }
  }
}
