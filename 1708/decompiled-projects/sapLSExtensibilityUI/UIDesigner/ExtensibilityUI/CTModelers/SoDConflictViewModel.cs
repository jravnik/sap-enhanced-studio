﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.SoDConflictViewModel
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public class SoDConflictViewModel : ConflictViewModel
  {
    private string resolution;

    public string Resolution
    {
      get
      {
        return this.resolution;
      }
      set
      {
        this.resolution = value;
        this.NotifyPropertyChanged("Resolution");
      }
    }
  }
}
