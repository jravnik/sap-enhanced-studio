﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.DependencyViewModel
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Mashup.Common;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public class DependencyViewModel : ObservableBase
  {
    public string ObjectID1 { get; set; }

    public string ObjectID2 { get; set; }

    public string FallbackValue { get; set; }

    public string FallbackValueType { get; set; }
  }
}
