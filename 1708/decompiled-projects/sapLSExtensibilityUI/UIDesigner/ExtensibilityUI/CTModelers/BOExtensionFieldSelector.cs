﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.BOExtensionFieldSelector
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Flexibility.CoreAPI.Extensibility;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Connector.Services.Modeler;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class BOExtensionFieldSelector : BaseChangeTransactionModeler, IComponentConnector
  {
    private ObservableCollection<BOExtensionFieldSelector.SelectedItem> extFieldCollection = new ObservableCollection<BOExtensionFieldSelector.SelectedItem>();
    private const string TITLE_STRING = "Add Extension Field";
    private const string Identifier = "Identifier";
    protected FlexibilityHandler flexHandler;
    protected SectionGroupType sectionGroupType;
    protected ListType listType;
    private LinkType linkType;
    private FlexStableAnchorType stableAnchorType;
    private bool changesApplied;
    private Dictionary<string, string> sectionGrpQueryMapping;
    private Dictionary<string, string> dataFieldBindMapping;
    protected AbstractExtensionProcessor processor;
    protected bool isQueryExtension;
    private bool isSadlCase;
    private __EXT__S_CUS_FACADE_UI_CONTEXT[] contextArray;
    private __EXT__S_CUS_FACADE_UI_FIELD[] fieldArray;
    private BOExtensionFieldSelector.SelectedItem selectedItem;
    private IFloorplan sourceComponent;
    //internal TextBox errorTextBox;
    //internal ListView itemListView;
    //internal GridView listViewGridView;
    //internal Button okButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public ObservableCollection<BOExtensionFieldSelector.SelectedItem> ExtFieldCollection
    {
      get
      {
        return this.extFieldCollection;
      }
    }

    public BOExtensionFieldSelector.SelectedItem Item
    {
      get
      {
        return this.selectedItem;
      }
    }

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public BOExtensionFieldSelector(__EXT__S_CUS_FACADE_UI_CONTEXT[] contextArray, __EXT__S_CUS_FACADE_UI_FIELD[] fieldArray)
    {
      this.Closing += new System.ComponentModel.CancelEventHandler(this.BOExtensionFieldSelector_Closing);
      this.InitializeComponent();
      this.fieldArray = fieldArray;
      this.contextArray = contextArray;
      this.setFields(contextArray, fieldArray);
      this.optimizeControlHeigh();
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
    }

    protected override void revertChanges()
    {
    }

    private void optimizeControlHeigh()
    {
      int num = this.extFieldCollection.Count;
      if (this.extFieldCollection.Count > 5)
        num = 5;
      this.itemListView.Height = (double) (40 + num * 30);
      this.Height = this.itemListView.Height + 150.0;
    }

    private void setFields(__EXT__S_CUS_FACADE_UI_CONTEXT[] contextArray, __EXT__S_CUS_FACADE_UI_FIELD[] fieldArray)
    {
      List<BusinessContextInformation> customFieldsreferenceFields = new List<BusinessContextInformation>();
      if (contextArray != null)
      {
        for (int index = 0; index < contextArray.Length; ++index)
        {
          BusinessContextInformation contextInformation = new BusinessContextInformation();
          if (!this.isSadlCase)
            contextInformation.referenceField = new ReferenceField(contextArray[index].REFERENCE_FIELD.REF_BUNDLE_KEY, contextArray[index].REFERENCE_FIELD.REF_FIELD_NAME);
          contextInformation.Description = contextArray[index].DESCRIPTION;
          customFieldsreferenceFields.Add(contextInformation);
        }
      }
      List<UIExtensionField> customFields = new List<UIExtensionField>();
      if (fieldArray != null)
      {
        for (int index = 0; index < fieldArray.Length; ++index)
        {
          if (fieldArray[index].CUSTOM_FIELD.REPRESENTATION_TERM == "Indicator")
          {
            UIExtensionField uiExtensionField = new UIExtensionField();
            uiExtensionField.customField = new CustomFieldInformation();
            uiExtensionField.customField.Exists = fieldArray[index].CUSTOM_FIELD.EXISTS;
            uiExtensionField.customField.IsDeletionAllowed = fieldArray[index].CUSTOM_FIELD.IS_DELETION_ALLOWED;
            uiExtensionField.customField.IsToBeDeleted = fieldArray[index].CUSTOM_FIELD.IS_TO_BE_DELETED;
            uiExtensionField.customField.IsConsistent = fieldArray[index].CUSTOM_FIELD.IS_CONSISTENT;
            uiExtensionField.customField.Field = new ESRName(fieldArray[index].CUSTOM_FIELD.FIELD.NAME, fieldArray[index].CUSTOM_FIELD.FIELD.NAMESPACE, fieldArray[index].CUSTOM_FIELD.APPEARANCE_NAME);
            uiExtensionField.customField.FieldType = fieldArray[index].CUSTOM_FIELD.FIELD_TYPE;
            uiExtensionField.customField.Length = fieldArray[index].CUSTOM_FIELD.LENGTH;
            uiExtensionField.customField.Decimals = fieldArray[index].CUSTOM_FIELD.DECIMALS;
            uiExtensionField.customField.RepresentationTerm = fieldArray[index].CUSTOM_FIELD.REPRESENTATION_TERM;
            uiExtensionField.customField.UiText = new UIText(fieldArray[index].CUSTOM_FIELD.UI_TEXTS.CAPTION, fieldArray[index].CUSTOM_FIELD.UI_TEXTS.HEADING, fieldArray[index].CUSTOM_FIELD.UI_TEXTS.DESCRIPTION);
            uiExtensionField.businessContext = new BusinessContextInformation();
            if (!this.isSadlCase)
              uiExtensionField.businessContext.referenceField = new ReferenceField(fieldArray[index].REFERENCE_FIELD.REF_BUNDLE_KEY, fieldArray[index].REFERENCE_FIELD.REF_FIELD_NAME);
            uiExtensionField.businessContext.Description = fieldArray[index].DESCRIPTION;
            customFields.Add(uiExtensionField);
          }
        }
      }
      this.fillListBox(customFieldsreferenceFields, customFields);
    }

    private void fillListBox(List<BusinessContextInformation> customFieldsreferenceFields, List<UIExtensionField> customFields)
    {
      this.itemListView.DataContext = (object) this.ExtFieldCollection;
      for (int index = 0; index < customFields.Count; ++index)
      {
        UIExtensionField customField = customFields[index];
        this.extFieldCollection.Add(new BOExtensionFieldSelector.SelectedItem()
        {
          EsrName = customField.customField.Field.CoreName,
          EsrNamespace = customField.customField.Field.Namespace,
          AppearanceName = customField.customField.Field.AppearanceName,
          ReferenceBundleKey = customField.businessContext.referenceField.bundleKey,
          ReferenceFieldName = customField.businessContext.referenceField.fieldName
        });
      }
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      this.DialogResult = new bool?(true);
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void BOExtensionFieldSelector_Closing(object sender, CancelEventArgs e)
    {
      XRepositoryProxy.Instance.CloseStatefullClient();
      this.revertChanges();
    }

    private void itemListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (e == null || e.AddedItems == null || e.AddedItems.Count <= 0)
        return;
      this.selectedItem = (BOExtensionFieldSelector.SelectedItem) e.AddedItems[0];
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/boextensionfieldselector.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.errorTextBox = (TextBox) target;
    //      break;
    //    case 2:
    //      this.itemListView = (ListView) target;
    //      this.itemListView.SelectionChanged += new SelectionChangedEventHandler(this.itemListView_SelectionChanged);
    //      break;
    //    case 3:
    //      this.listViewGridView = (GridView) target;
    //      break;
    //    case 4:
    //      this.okButton = (Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 5:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    public class SelectedItem : DependencyObject
    {
      public static EventHandler<EventArgs> CheckBoxChanged;

      public string EsrName { get; set; }

      public string EsrNamespace { get; set; }

      public string AppearanceName { get; set; }

      public string ReferenceBundleKey { get; set; }

      public string ReferenceFieldName { get; set; }
    }
  }
}
