﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.TextEditModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Xml.Linq;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class TextEditModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private FlexibilityHandler flexHandler;
    private List<XElement> textEditControls;
    private bool changesApplied;
    //internal Label SelectControlLabel;
    //internal ComboBox ComboBox;
    //internal Label SelectedTextEdit;
    //internal TextBox SelectedTextEditCount;
    //internal TextBlock ErrorMessage;
    //internal Button okButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public TextEditModeler()
    {
      this.ResizeMode = ResizeMode.NoResize;
      this.InitializeComponent();
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.SelectControlLabel.Visibility = Visibility.Collapsed;
      this.ComboBox.Visibility = Visibility.Collapsed;
      this.okButton.IsEnabled = false;
      this.setSelectedTextEdit((XElement) null);
      this.textEditControls = new List<XElement>();
      this.populateTextEditControls();
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        this.printError("Couldn't obtain Flexibility Handler. Please contact your administrator.");
      else if (this.Anchor == null)
        this.printError("Please, select an Section Anchor");
      else if (this.Anchor.type != FlexAnchorEnumType.SectionGroupAnchor)
        this.printError("Please, select an Section Anchor.");
      else if (!this.checkForTextEditExistance())
        this.printError("No TextEdit Control exist in the selected section.");
      else if (!this.checkForScriptedValue())
        this.printError("Relevant TextEdit control uses scripting. The change transaction requires a non-scripted 'Row Count' attribute");
      else
        this.visualizeTextEditEntities();
    }

    private void visualizeTextEditEntities()
    {
      this.ErrorMessage.Text = string.Empty;
      if (this.textEditControls.Count > 1)
      {
        this.SelectControlLabel.Visibility = Visibility.Visible;
        this.ComboBox.Visibility = Visibility.Visible;
        this.ComboBox.Items.Clear();
        foreach (XElement textEditControl in this.textEditControls)
        {
          ItemCollection items = this.ComboBox.Items;
          ComboBoxItem comboBoxItem1 = new ComboBoxItem();
          comboBoxItem1.Content = (object) this.getLabel(textEditControl);
          comboBoxItem1.Tag = (object) textEditControl;
          ComboBoxItem comboBoxItem2 = comboBoxItem1;
          items.Add((object) comboBoxItem2);
        }
        this.ComboBox.SelectedIndex = 0;
      }
      else
      {
        this.SelectControlLabel.Visibility = Visibility.Collapsed;
        this.ComboBox.Visibility = Visibility.Collapsed;
        this.setSelectedTextEdit(this.textEditControls[0]);
      }
    }

    private void setSelectedTextEdit(XElement textEditCtrlElem)
    {
      if (textEditCtrlElem == null)
      {
        this.SelectedTextEdit.Content = (object) string.Empty;
        this.SelectedTextEditCount.Visibility = Visibility.Collapsed;
      }
      else
      {
        this.SelectedTextEdit.Content = (object) this.getLabel(textEditCtrlElem);
        this.SelectedTextEditCount.Text = this.getCurrentRowCount(textEditCtrlElem);
        this.SelectedTextEditCount.Visibility = Visibility.Visible;
      }
    }

    private string getLabel(XElement textEditCtrlElem)
    {
      if (textEditCtrlElem == null)
        return string.Empty;
      XElement parent = textEditCtrlElem.Parent;
      if (parent == null || !parent.Name.Equals((object) (HelperFunctions.ns_uxv + "SectionGroupItem")))
        return string.Empty;
      XElement xelement = parent.Element(HelperFunctions.ns_uxv + "Label");
      if (xelement == null)
        return string.Empty;
      string str1 = xelement.Attributes((XName) "textPoolId").Any<XAttribute>() ? xelement.Attribute((XName) "textPoolId").Value : string.Empty;
      string str2 = xelement.Attributes((XName) "fallbackValue").Any<XAttribute>() ? xelement.Attribute((XName) "fallbackValue").Value : string.Empty;
      if (!string.IsNullOrEmpty(str1))
        return this.getText(new DependentPropertyType()
        {
          fallbackValue = str2,
          textPoolId = str1
        });
      if (!string.IsNullOrEmpty(str2))
        return str2;
      return this.getId(textEditCtrlElem);
    }

    private string getCurrentRowCount(XElement textEditCtrlElem)
    {
      XElement xelement = textEditCtrlElem.Element(HelperFunctions.ns_uxv + "RowsCountDynamic");
      if (xelement != null && xelement.Attributes((XName) "fallbackValue").Any<XAttribute>())
        return xelement.Attribute((XName) "fallbackValue").Value;
      return "1";
    }

    private string getId(XElement textEditCtrlElem)
    {
      if (textEditCtrlElem.Attributes((XName) "id").Any<XAttribute>())
        return textEditCtrlElem.Attribute((XName) "id").Value;
      return string.Empty;
    }

    private bool checkForScriptedValue()
    {
      List<XElement> xelementList = new List<XElement>();
      foreach (XElement textEditControl in this.textEditControls)
      {
        bool flag = false;
        XElement xelement = textEditControl.Element(HelperFunctions.ns_uxv + "RowsCountDynamic");
        if (xelement != null && xelement.Element(HelperFunctions.ns_uxc + "CalculationRule") != null)
          flag = true;
        if (!flag)
          xelementList.Add(textEditControl);
      }
      if (xelementList.Count <= 0)
        return false;
      if (xelementList.Count != this.textEditControls.Count)
        this.textEditControls = xelementList;
      return true;
    }

    private void checkAddedCount()
    {
      string text = this.SelectedTextEditCount.Text;
      if (string.IsNullOrEmpty(text))
      {
        this.printError("Please add an rows count");
      }
      else
      {
        try
        {
          int.Parse(text);
        }
        catch (FormatException ex)
        {
          this.printError("Please add only numeric value for count");
          return;
        }
        this.ErrorMessage.Visibility = Visibility.Collapsed;
        this.okButton.IsEnabled = true;
      }
    }

    private void populateTextEditControls()
    {
      string xrepPath = this.Anchor.xrepPath;
      foreach (XElement sectionGroupAnchor in HelperFunctions.getSectionGroupAnchors(this.UIComponent))
      {
        if (sectionGroupAnchor.Attributes((XName) "xrepPath").Any<XAttribute>() && sectionGroupAnchor.Attribute((XName) "xrepPath").Value.Equals(xrepPath))
        {
          XElement parent1 = sectionGroupAnchor.Parent;
          if (parent1 == null)
            break;
          if (parent1.Attributes((XName) "id").Any<XAttribute>())
          {
            string str1 = parent1.Attribute((XName) "id").Value;
            foreach (XElement textEditControl in HelperFunctions.getTextEditControls(this.UIComponent))
            {
              XElement xelement = (XElement) null;
              for (XElement parent2 = textEditControl.Parent; parent2 != null; parent2 = parent2.Parent)
              {
                if (parent2.Name.Equals((object) (HelperFunctions.ns_uxv + "SectionGroup")))
                {
                  xelement = parent2;
                  break;
                }
              }
              if (xelement != null && xelement.Attributes((XName) "id").Any<XAttribute>())
              {
                string str2 = xelement.Attribute((XName) "id").Value;
                if (str1.Equals(str2))
                  this.textEditControls.Add(textEditControl);
              }
            }
          }
        }
      }
    }

    private bool checkForTextEditExistance()
    {
      return this.textEditControls != null && this.textEditControls.Count != 0;
    }

    private void printError(string errorMsg)
    {
      this.ErrorMessage.Visibility = Visibility.Visible;
      this.ErrorMessage.Text = errorMsg;
      this.okButton.IsEnabled = false;
      this.cancelButton.IsEnabled = true;
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      this.IsChangeApplied = false;
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      XElement textEditCtrlElem = (XElement) null;
      if (this.textEditControls.Count > 1)
      {
        ComboBoxItem selectedItem = this.ComboBox.SelectedItem as ComboBoxItem;
        if (selectedItem != null)
          textEditCtrlElem = selectedItem.Tag as XElement;
      }
      else
        textEditCtrlElem = this.textEditControls[0];
      if (textEditCtrlElem == null)
        return;
      string text = this.SelectedTextEditCount.Text;
      if (string.IsNullOrEmpty(text))
      {
        this.printError("Please add an rows count");
      }
      else
      {
        int num1;
        try
        {
          num1 = int.Parse(text);
        }
        catch (FormatException ex)
        {
          this.printError("Please add only numeric value for count");
          return;
        }
        string currentRowCount = this.getCurrentRowCount(textEditCtrlElem);
        int num2;
        try
        {
          num2 = int.Parse(currentRowCount);
        }
        catch (FormatException ex)
        {
          this.printError("Conversion Exception occured. Couldn't parse '" + currentRowCount + "'");
          return;
        }
        if (num1 == 0)
          this.printError("The value should be > 0. CT will not be created.");
        else if (num2 == num1)
        {
          this.printError("New value is the same as the initial value. CT will not be created.");
        }
        else
        {
          string id = this.getId(textEditCtrlElem);
          if (string.IsNullOrEmpty(id))
          {
            this.printError("TextEdit Control Id couldn't be determined.");
          }
          else
          {
            this.flexHandler.ChangingTextEditRowCount(this.Anchor, id, Convert.ToString(num1));
            this.changesApplied = true;
            this.IsChangeApplied = true;
            this.Close();
          }
        }
      }
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.ErrorMessage.Visibility = Visibility.Collapsed;
      this.setSelectedTextEdit((XElement) null);
      if (e.AddedItems.Count <= 0)
        return;
      ComboBoxItem addedItem = e.AddedItems[0] as ComboBoxItem;
      if (addedItem == null)
        return;
      this.setSelectedTextEdit(addedItem.Tag as XElement);
    }

    private void SelectedTextEditCount_KeyDown(object sender, KeyEventArgs e)
    {
      switch (e.Key)
      {
        case Key.Return:
          this.checkAddedCount();
          break;
        case Key.D0:
        case Key.D1:
        case Key.D2:
        case Key.D3:
        case Key.D4:
        case Key.D5:
        case Key.D6:
        case Key.D7:
        case Key.D8:
        case Key.D9:
          this.ErrorMessage.Visibility = Visibility.Collapsed;
          break;
        default:
          this.checkAddedCount();
          break;
      }
    }

    private void SelectedTextEditCount_LostFocus(object sender, RoutedEventArgs e)
    {
      this.checkAddedCount();
    }

    private void SelectedTextEditCount_GotFocus(object sender, RoutedEventArgs e)
    {
      this.ErrorMessage.Visibility = Visibility.Collapsed;
    }

    private void SelectedTextEditCount_TextChanged(object sender, TextChangedEventArgs e)
    {
      this.checkAddedCount();
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/texteditmodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.SelectControlLabel = (Label) target;
    //      break;
    //    case 2:
    //      this.ComboBox = (ComboBox) target;
    //      this.ComboBox.SelectionChanged += new SelectionChangedEventHandler(this.ComboBox_SelectionChanged);
    //      break;
    //    case 3:
    //      this.SelectedTextEdit = (Label) target;
    //      break;
    //    case 4:
    //      this.SelectedTextEditCount = (TextBox) target;
    //      this.SelectedTextEditCount.TextChanged += new TextChangedEventHandler(this.SelectedTextEditCount_TextChanged);
    //      this.SelectedTextEditCount.GotFocus += new RoutedEventHandler(this.SelectedTextEditCount_GotFocus);
    //      break;
    //    case 5:
    //      this.ErrorMessage = (TextBlock) target;
    //      break;
    //    case 6:
    //      this.okButton = (Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 7:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
