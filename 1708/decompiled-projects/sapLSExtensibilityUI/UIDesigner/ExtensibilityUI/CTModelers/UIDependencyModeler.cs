﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.UIDependencyModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class UIDependencyModeler : BaseChangeTransactionModeler, INotifyPropertyChanged, IComponentConnector, IStyleConnector
  {
    private List<string> woCList = new List<string>();
    private List<string> partnerWoCList = new List<string>();
    private ObservableCollection<DependencyViewModel> prerequisitesViewModel = new ObservableCollection<DependencyViewModel>();
    private ObservableCollection<ConflictViewModel> technicalConflictsViewModel = new ObservableCollection<ConflictViewModel>();
    private ObservableCollection<SoDConflictViewModel> soDConflictsViewModel = new ObservableCollection<SoDConflictViewModel>();
    private const string OBJECTTYPE = "uiwocview";
    private FlexibilityHandler flexHandler;
    private FlexBaseAnchorType stableAnchor;
    private bool hasInvalidParameter;
    //internal Grid PrerequisiteGrid;
    //internal System.Windows.Controls.Button ButtonAddPrerequisite;
    //internal ItemsControl Prerequisites;
    //internal Grid TechnicalConflictGrid;
    //internal System.Windows.Controls.Button ButtonAddTechnicalConflict;
    //internal ItemsControl TechnicalConflicts;
    //internal Grid SoDConflictGrid;
    //internal System.Windows.Controls.Button ButtonAddSoDConflict;
    //internal ItemsControl SoDConflicts;
    //internal System.Windows.Controls.Button okButton;
    //internal System.Windows.Controls.Button cancelButton;
    //private bool _contentLoaded;

    public List<string> WoCList
    {
      get
      {
        return this.woCList;
      }
      set
      {
        if (value == this.woCList)
          return;
        this.woCList = value;
        this.NotifyPropertyChanged("WoCList");
      }
    }

    public List<string> PartnerWoCList
    {
      get
      {
        return this.partnerWoCList;
      }
      set
      {
        if (value == this.partnerWoCList)
          return;
        this.partnerWoCList = value;
        this.NotifyPropertyChanged("WoCList");
      }
    }

    public ObservableCollection<DependencyViewModel> PrerequisitesViewModel
    {
      get
      {
        return this.prerequisitesViewModel;
      }
      set
      {
        this.prerequisitesViewModel = value;
        this.NotifyPropertyChanged("PrerequisitesViewModel");
      }
    }

    public ObservableCollection<ConflictViewModel> TechnicalConflictsViewModel
    {
      get
      {
        return this.technicalConflictsViewModel;
      }
      set
      {
        this.technicalConflictsViewModel = value;
        this.NotifyPropertyChanged("TechnicalConflictsViewModel");
      }
    }

    public ObservableCollection<SoDConflictViewModel> SoDConflictsViewModel
    {
      get
      {
        return this.soDConflictsViewModel;
      }
      set
      {
        this.soDConflictsViewModel = value;
        this.NotifyPropertyChanged("SoDConflictsViewModel");
      }
    }

    public override bool RequiresReload
    {
      get
      {
        return false;
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public UIDependencyModeler()
    {
      this.InitializeComponent();
      this.Title = "UI Dependency";
      this.LoadAvailableWoCList();
    }

    protected void NotifyPropertyChanged(string propertyName)
    {
      if (this.PropertyChanged == null)
        return;
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    private void LoadAvailableWoCList()
    {
      List<string> allApplictionViews = ContentServiceProxy.Instance.GetAllApplictionViews();
      Regex regex = new Regex(".*/SRC/.*");
      foreach (string input in allApplictionViews)
      {
        if (!this.WoCList.Contains(input))
          this.WoCList.Add(input);
        if (!this.PartnerWoCList.Contains(input) && regex.IsMatch(input))
          this.PartnerWoCList.Add(input);
      }
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.Terminate();
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      if (this.ExtensibleModel == null)
        return;
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      FlexBaseAnchorType dependencyAnchor = this.flexHandler.GetUIDependencyAnchor(this.ExtensibleModel as UXComponent);
      if (this.Anchor.xrepPath == dependencyAnchor.xrepPath)
        this.stableAnchor = dependencyAnchor;
      if (this.stableAnchor == null)
        return;
      ModelEntity extensibleModel = this.ExtensibleModel;
    }

    protected override void revertChanges()
    {
    }

    private void ButtonAddPrerequisite_Click(object sender, RoutedEventArgs e)
    {
      this.PrerequisitesViewModel.Add(new DependencyViewModel());
      this.okButton.IsEnabled = true;
    }

    private void ButtonRemovePrerequisite_Click(object sender, RoutedEventArgs e)
    {
      this.PrerequisitesViewModel.Remove((sender as System.Windows.Controls.Button).Tag as DependencyViewModel);
      if (this.PrerequisitesViewModel.Count != 0 || this.TechnicalConflictsViewModel.Count != 0 || this.SoDConflictsViewModel.Count != 0)
        return;
      this.okButton.IsEnabled = false;
    }

    private void ButtonRemoveTechnicalConflict_Click(object sender, RoutedEventArgs e)
    {
      this.TechnicalConflictsViewModel.Remove((sender as System.Windows.Controls.Button).Tag as ConflictViewModel);
      if (this.PrerequisitesViewModel.Count != 0 || this.TechnicalConflictsViewModel.Count != 0 || this.SoDConflictsViewModel.Count != 0)
        return;
      this.okButton.IsEnabled = false;
    }

    private void ButtonAddTechnicalConflict_Click(object sender, RoutedEventArgs e)
    {
      this.TechnicalConflictsViewModel.Add(new ConflictViewModel());
      this.okButton.IsEnabled = true;
    }

    private void ButtonAddSoDConflict_Click(object sender, RoutedEventArgs e)
    {
      this.SoDConflictsViewModel.Add(new SoDConflictViewModel());
      this.okButton.IsEnabled = true;
    }

    private void ButtonRemoveSoDConflict_Click(object sender, RoutedEventArgs e)
    {
      this.SoDConflictsViewModel.Remove((sender as System.Windows.Controls.Button).Tag as SoDConflictViewModel);
      if (this.PrerequisitesViewModel.Count != 0 || this.TechnicalConflictsViewModel.Count != 0 || this.SoDConflictsViewModel.Count != 0)
        return;
      this.okButton.IsEnabled = false;
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      this.CreateChangeTransaction();
      if (this.hasInvalidParameter)
        return;
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void WoCList1_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      System.Windows.Controls.ComboBox comboBox = sender as System.Windows.Controls.ComboBox;
      if (comboBox == null || comboBox.SelectedItem == null)
        return;
      (comboBox.DataContext as DependencyViewModel).ObjectID1 = comboBox.SelectedItem.ToString();
    }

    private void WoCList2_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      System.Windows.Controls.ComboBox comboBox = sender as System.Windows.Controls.ComboBox;
      if (comboBox == null || comboBox.SelectedItem == null)
        return;
      (comboBox.DataContext as DependencyViewModel).ObjectID2 = comboBox.SelectedItem.ToString();
    }

    private void CreateChangeTransaction()
    {
      if (this.PrerequisitesViewModel.Count > 0)
      {
        foreach (DependencyViewModel dependencyViewModel in (Collection<DependencyViewModel>) this.PrerequisitesViewModel)
        {
          string objectId1 = dependencyViewModel.ObjectID1;
          string objectId2 = dependencyViewModel.ObjectID2;
          Guid.NewGuid().ToString("N");
          if (string.IsNullOrEmpty(objectId1) || string.IsNullOrEmpty(objectId2))
          {
            int num = (int) DisplayMessage.Show("There are some empty parameters,please check!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            this.IsChangeApplied = false;
            this.hasInvalidParameter = true;
            return;
          }
          if (objectId1.Equals(objectId2))
          {
            int num = (int) DisplayMessage.Show("objectID1 can not euqal objectID2,please check!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            this.IsChangeApplied = false;
            this.hasInvalidParameter = true;
            return;
          }
        }
      }
      if (this.TechnicalConflictsViewModel.Count > 0)
      {
        foreach (ConflictViewModel conflictViewModel in (Collection<ConflictViewModel>) this.TechnicalConflictsViewModel)
        {
          string objectId1 = conflictViewModel.ObjectID1;
          string objectId2 = conflictViewModel.ObjectID2;
          string reason = conflictViewModel.Reason;
          if (string.IsNullOrEmpty(objectId1) || string.IsNullOrEmpty(objectId2) || string.IsNullOrEmpty(reason))
          {
            int num = (int) DisplayMessage.Show("There are some empty parameters,please check!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            this.IsChangeApplied = false;
            this.hasInvalidParameter = true;
            return;
          }
          if (objectId1.Equals(objectId2))
          {
            int num = (int) DisplayMessage.Show("objectID1 can not euqal objectID2,please check!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            this.IsChangeApplied = false;
            this.hasInvalidParameter = true;
            return;
          }
        }
      }
      if (this.SoDConflictsViewModel.Count > 0)
      {
        foreach (SoDConflictViewModel dconflictViewModel in (Collection<SoDConflictViewModel>) this.SoDConflictsViewModel)
        {
          string objectId1 = dconflictViewModel.ObjectID1;
          string objectId2 = dconflictViewModel.ObjectID2;
          string reason = dconflictViewModel.Reason;
          string resolution = dconflictViewModel.Resolution;
          if (string.IsNullOrEmpty(objectId1) || string.IsNullOrEmpty(objectId2) || (string.IsNullOrEmpty(reason) || string.IsNullOrEmpty(resolution)))
          {
            int num = (int) DisplayMessage.Show("There are some empty parameters,please check!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            this.IsChangeApplied = false;
            this.hasInvalidParameter = true;
            return;
          }
          if (objectId1.Equals(objectId2))
          {
            int num = (int) DisplayMessage.Show("objectID1 can not euqal objectID2,please check!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            this.IsChangeApplied = false;
            this.hasInvalidParameter = true;
            return;
          }
        }
      }
      this.hasInvalidParameter = false;
      if (this.PrerequisitesViewModel.Count <= 0 && this.TechnicalConflictsViewModel.Count <= 0 && this.SoDConflictsViewModel.Count <= 0)
        return;
      if (this.PrerequisitesViewModel.Count > 0)
      {
        foreach (DependencyViewModel dependencyViewModel in (Collection<DependencyViewModel>) this.PrerequisitesViewModel)
        {
          string objectId1 = dependencyViewModel.ObjectID1;
          string objectId2 = dependencyViewModel.ObjectID2;
          string str = Guid.NewGuid().ToString("N");
          UXDependencyType uxDependencyType1 = new UXDependencyType();
          uxDependencyType1.ObjectID1 = objectId1;
          uxDependencyType1.ObjectID2 = objectId2;
          uxDependencyType1.ObjectType1 = "uiwocview";
          uxDependencyType1.ObjectType2 = "uiwocview";
          uxDependencyType1.id = str;
          UXDependencyType uxDependencyType2 = uxDependencyType1;
          this.flexHandler.AddUIDependency(this.stableAnchor, new FlexAddUIDependencyType()
          {
            Prerequisite = uxDependencyType2
          }, this.UIComponentPath);
        }
      }
      if (this.TechnicalConflictsViewModel.Count > 0)
      {
        foreach (ConflictViewModel conflictViewModel in (Collection<ConflictViewModel>) this.TechnicalConflictsViewModel)
        {
          string objectId1 = conflictViewModel.ObjectID1;
          string objectId2 = conflictViewModel.ObjectID2;
          string reason = conflictViewModel.Reason;
          string str1 = Guid.NewGuid().ToString("N");
          string str2 = Guid.NewGuid().ToString("N");
          UXConflictType uxConflictType1 = new UXConflictType();
          uxConflictType1.ObjectID1 = objectId1;
          uxConflictType1.ObjectID2 = objectId2;
          uxConflictType1.ObjectType1 = "uiwocview";
          uxConflictType1.ObjectType2 = "uiwocview";
          uxConflictType1.id = str1;
          UXConflictType uxConflictType2 = uxConflictType1;
          DependentPropertyType dependentPropertyType1 = new DependentPropertyType();
          dependentPropertyType1.id = str2;
          dependentPropertyType1.textPoolId = str2;
          dependentPropertyType1.fallbackValue = reason;
          dependentPropertyType1.fallbackValueType = DataFieldTypes.@string;
          dependentPropertyType1.fallbackValueTypeSpecified = true;
          DependentPropertyType dependentPropertyType2 = dependentPropertyType1;
          uxConflictType2.Reason = dependentPropertyType2;
          UXConflictType uxConflictType3 = uxConflictType1;
          this.flexHandler.AddUIDependency(this.stableAnchor, new FlexAddUIDependencyType()
          {
            TechnicalConflict = uxConflictType3
          }, this.UIComponentPath);
        }
      }
      if (this.SoDConflictsViewModel.Count > 0)
      {
        foreach (SoDConflictViewModel dconflictViewModel in (Collection<SoDConflictViewModel>) this.SoDConflictsViewModel)
        {
          string objectId1 = dconflictViewModel.ObjectID1;
          string objectId2 = dconflictViewModel.ObjectID2;
          string reason = dconflictViewModel.Reason;
          string resolution = dconflictViewModel.Resolution;
          string str1 = Guid.NewGuid().ToString("N");
          string str2 = Guid.NewGuid().ToString("N");
          string str3 = Guid.NewGuid().ToString("N");
          UXSoDConflictType uxSoDconflictType1 = new UXSoDConflictType();
          uxSoDconflictType1.ObjectID1 = objectId1;
          uxSoDconflictType1.ObjectID2 = objectId2;
          uxSoDconflictType1.ObjectType1 = "uiwocview";
          uxSoDconflictType1.ObjectType2 = "uiwocview";
          uxSoDconflictType1.id = str3;
          UXSoDConflictType uxSoDconflictType2 = uxSoDconflictType1;
          DependentPropertyType dependentPropertyType1 = new DependentPropertyType();
          dependentPropertyType1.id = str1;
          dependentPropertyType1.textPoolId = str1;
          dependentPropertyType1.fallbackValue = reason;
          dependentPropertyType1.fallbackValueType = DataFieldTypes.@string;
          dependentPropertyType1.fallbackValueTypeSpecified = true;
          DependentPropertyType dependentPropertyType2 = dependentPropertyType1;
          uxSoDconflictType2.Reason = dependentPropertyType2;
          UXSoDConflictType uxSoDconflictType3 = uxSoDconflictType1;
          DependentPropertyType dependentPropertyType3 = new DependentPropertyType();
          dependentPropertyType3.id = str2;
          dependentPropertyType3.textPoolId = str2;
          dependentPropertyType3.fallbackValue = resolution;
          dependentPropertyType3.fallbackValueType = DataFieldTypes.@string;
          dependentPropertyType3.fallbackValueTypeSpecified = true;
          DependentPropertyType dependentPropertyType4 = dependentPropertyType3;
          uxSoDconflictType3.Resolution = dependentPropertyType4;
          UXSoDConflictType uxSoDconflictType4 = uxSoDconflictType1;
          this.flexHandler.AddUIDependency(this.stableAnchor, new FlexAddUIDependencyType()
          {
            SoDConflict = uxSoDconflictType4
          }, this.UIComponentPath);
        }
      }
      this.IsChangeApplied = true;
    }

    private void Terminate()
    {
      this.PrerequisitesViewModel.Clear();
      this.PrerequisitesViewModel = (ObservableCollection<DependencyViewModel>) null;
      this.TechnicalConflictsViewModel.Clear();
      this.TechnicalConflictsViewModel = (ObservableCollection<ConflictViewModel>) null;
      this.SoDConflictsViewModel.Clear();
      this.SoDConflictsViewModel = (ObservableCollection<SoDConflictViewModel>) null;
      this.WoCList.Clear();
      this.WoCList = (List<string>) null;
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/uidependencymodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.PrerequisiteGrid = (Grid) target;
    //      break;
    //    case 2:
    //      this.ButtonAddPrerequisite = (System.Windows.Controls.Button) target;
    //      this.ButtonAddPrerequisite.Click += new RoutedEventHandler(this.ButtonAddPrerequisite_Click);
    //      break;
    //    case 3:
    //      this.Prerequisites = (ItemsControl) target;
    //      break;
    //    case 7:
    //      this.TechnicalConflictGrid = (Grid) target;
    //      break;
    //    case 8:
    //      this.ButtonAddTechnicalConflict = (System.Windows.Controls.Button) target;
    //      this.ButtonAddTechnicalConflict.Click += new RoutedEventHandler(this.ButtonAddTechnicalConflict_Click);
    //      break;
    //    case 9:
    //      this.TechnicalConflicts = (ItemsControl) target;
    //      break;
    //    case 13:
    //      this.SoDConflictGrid = (Grid) target;
    //      break;
    //    case 14:
    //      this.ButtonAddSoDConflict = (System.Windows.Controls.Button) target;
    //      this.ButtonAddSoDConflict.Click += new RoutedEventHandler(this.ButtonAddSoDConflict_Click);
    //      break;
    //    case 15:
    //      this.SoDConflicts = (ItemsControl) target;
    //      break;
    //    case 19:
    //      this.okButton = (System.Windows.Controls.Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 20:
    //      this.cancelButton = (System.Windows.Controls.Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [DebuggerNonUserCode]
    void IStyleConnector.Connect(int connectionId, object target)
    {
      switch (connectionId)
      {
        case 4:
          ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.ButtonRemovePrerequisite_Click);
          break;
        case 5:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.WoCList2_SelectionChanged);
          break;
        case 6:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.WoCList1_SelectionChanged);
          break;
        case 10:
          ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.ButtonRemoveTechnicalConflict_Click);
          break;
        case 11:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.WoCList2_SelectionChanged);
          break;
        case 12:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.WoCList1_SelectionChanged);
          break;
        case 16:
          ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.ButtonRemoveSoDConflict_Click);
          break;
        case 17:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.WoCList2_SelectionChanged);
          break;
        case 18:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.WoCList1_SelectionChanged);
          break;
      }
    }
  }
}
