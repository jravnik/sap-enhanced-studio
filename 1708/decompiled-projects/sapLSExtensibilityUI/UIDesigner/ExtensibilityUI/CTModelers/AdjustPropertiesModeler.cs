﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AdjustPropertiesModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Connector.Services.Modeler;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.Dialogs;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AdjustPropertiesModeler : BaseChangeTransactionModeler, IComponentConnector, IStyleConnector
  {
    private Brush BRUSH_VALUE_CHANGED = (Brush) Brushes.Red;
    private bool m_HandleEvents = true;
    private AdjustableCTEntity m_AdjustableCTEntity = new AdjustableCTEntity();
    private List<PropertyValues> m_TristateBooleanValueSource = new List<PropertyValues>();
    private List<AppearanceInfo> infoList = new List<AppearanceInfo>();
    private List<UISwitchInfo> uiSwitchCollection = XRepositoryProxy.Instance.GetUISwitches();
    private List<UISwitchInfo> uiSwitchCollection_NotEqual = XRepositoryProxy.Instance.GetUISwitches();
    private const string STRING_SECTIONGROUPITEM = "Section Group Item";
    private const string STRING_PANECONTAINER = "Pane Container";
    private const string STRING_VISIBLE_PROPERTY = "Visible";
    private const string STRING_ENABLED_PROPERTY = "Enabled";
    private const string STRING_READONLY_PROPERTY = "Readonly";
    private const string STRING_MANDATORY_PROPERTY = "Mandatory";
    private const string STRING_SECTIONGROUPHEADER = "Section Group Header";
    private bool IsFieldBound;
    private FlexibilityHandler m_FlexibilityHandler;
    private __EXT__S_CUS_FACADE_UI_CONTEXT[] contextArray;
    private __EXT__S_CUS_FACADE_UI_FIELD[] fieldArray;
    private string m_BONamspace;
    private string m_BOName;
    private bool isSadlCase;
    //internal Grid rootGrid;
    //internal Grid uiSwitchGrid;
    //internal TextBlock uiSwitchTextBlock;
    //internal System.Windows.Controls.ComboBox OperatorComboBox;
    //internal System.Windows.Controls.ComboBox uiSwitchComboBox;
    //internal System.Windows.Controls.DataGrid controlsDataGrid;
    //internal TextBlock textBlockHeaderControlHeader;
    //internal DataGridTemplateColumn VisibleColumn;
    //internal TextBlock textBlockHeaderVisibleColumn;
    //internal DataGridTemplateColumn EnabledColumn;
    //internal TextBlock textBlockHeadeEnabledColumn;
    //internal DataGridTemplateColumn MandatoryColumn;
    //internal TextBlock textBlockHeadeMandatoryColumn;
    //internal DataGridTemplateColumn ReadOnlyColumn;
    //internal TextBlock textBlockHeadReadOnlyColumn;
    //internal StackPanel toolbarPanel;
    //internal System.Windows.Controls.Button okButton;
    //internal System.Windows.Controls.Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public AdjustPropertiesModeler()
    {
      this.InitializeComponent();
    }

    private void OperatorComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (((System.Windows.Controls.ComboBox) sender).Text == "Equal")
      {
        this.uiSwitchComboBox.ItemsSource = (IEnumerable) this.uiSwitchCollection_NotEqual;
        this.uiSwitchComboBox.SelectedIndex = 0;
      }
      else
      {
        this.uiSwitchComboBox.ItemsSource = (IEnumerable) this.uiSwitchCollection;
        this.uiSwitchComboBox.SelectedIndex = 0;
      }
    }

    protected override void InitializeModeler()
    {
      this.m_HandleEvents = false;
      base.InitializeModeler();
      this.m_FlexibilityHandler = this.getFlexibilityHandler();
      foreach (PropertyValues propertyValues in Enum.GetValues(typeof (PropertyValues)))
        this.m_TristateBooleanValueSource.Add(propertyValues);
      if (this.DTComponent.ImplementationController.DataModel.IsVirtualBinding)
        this.m_TristateBooleanValueSource.Remove(PropertyValues.Bound);
      this.OperatorComboBox.ItemsSource = (IEnumerable) new List<UISwitchCondition>()
      {
        new UISwitchCondition() { ValueString = "Equal" },
        new UISwitchCondition() { ValueString = "Not Equal" }
      };
      this.uiSwitchCollection.Insert(0, new UISwitchInfo()
      {
        Id = (string) null,
        Name = "None",
        Description = "None"
      });
      this.uiSwitchComboBox.ItemsSource = (IEnumerable) this.uiSwitchCollection;
      this.uiSwitchComboBox.DataContext = (object) this.m_AdjustableCTEntity;
      this.OperatorComboBox.DataContext = (object) this.m_AdjustableCTEntity;
      this.OperatorComboBox.SelectedIndex = 0;
      this.OperatorComboBox.SelectionChanged += new SelectionChangedEventHandler(this.OperatorComboBox_SelectionChanged);
      List<FlexStableAnchorType> stableAnchorTypeList = (List<FlexStableAnchorType>) null;
      if (this.DTExtensibleModel is FieldGroup)
      {
        FieldGroup dtExtensibleModel = this.DTExtensibleModel as FieldGroup;
        stableAnchorTypeList = this.m_FlexibilityHandler.GetAllStableAnchorList(this.ExtensibleModel as SectionGroupType);
        this.LoadSectionGroupChangeTransaction(this.m_AdjustableCTEntity.AdjustablePropertEntities, dtExtensibleModel, string.Empty, true);
      }
      else if (this.DTExtensibleModel is List)
      {
        List dtExtensibleModel = this.DTExtensibleModel as List;
        stableAnchorTypeList = this.m_FlexibilityHandler.GetAllStableAnchorList(this.ExtensibleModel as ListType);
        this.LoadListChangeTransaction(this.m_AdjustableCTEntity.AdjustablePropertEntities, dtExtensibleModel, string.Empty, true);
      }
      else if (this.DTExtensibleModel is ViewSwitchNavigation)
        this.LoadViewSwitchChangeTransaction(this.m_AdjustableCTEntity.AdjustablePropertEntities, this.DTExtensibleModel as ViewSwitchNavigation, string.Empty, true);
      else if (this.DTExtensibleModel is ButtonGroup)
        this.LoadButtonGroupChangeTransaction(this.m_AdjustableCTEntity.AdjustablePropertEntities, this.DTExtensibleModel as ButtonGroup, string.Empty, true);
      if (stableAnchorTypeList != null)
      {
        foreach (FlexStableAnchorType stableAnchorType in stableAnchorTypeList)
        {
          if (!string.IsNullOrEmpty(stableAnchorType.dataFieldPath) && !string.IsNullOrEmpty(stableAnchorType.containerId))
          {
            if (!string.IsNullOrEmpty(stableAnchorType.coreBoName) && !string.IsNullOrEmpty(stableAnchorType.coreBoName) && !string.IsNullOrEmpty(stableAnchorType.coreBoNameNamespace))
              this.isSadlCase = true;
            else if (!string.IsNullOrEmpty(stableAnchorType.referenceBundleKey) && !string.IsNullOrEmpty(stableAnchorType.referenceFieldName))
              this.isSadlCase = false;
            if (!this.isSadlCase)
              this.getFields(new List<string>()
              {
                stableAnchorType.referenceBundleKey
              }, new List<string>()
              {
                stableAnchorType.referenceFieldName
              });
            else
              this.getSadlFields(new KeyValuePair<string, string>(stableAnchorType.coreBoName, stableAnchorType.coreBoNameNamespace), new KeyValuePair<string, string>(stableAnchorType.coreBoNodeName, stableAnchorType.coreBoName));
          }
        }
      }
      this.controlsDataGrid.ItemsSource = (IEnumerable) this.m_AdjustableCTEntity.AdjustablePropertEntities;
      this.m_HandleEvents = true;
    }

    private void getFields(List<string> bundleList, List<string> fieldList)
    {
      string empty = string.Empty;
      XRepositoryProxy.Instance.GetExtensionFields(bundleList, fieldList, this.Solution, out this.contextArray, out this.fieldArray, ref empty);
    }

    private void getSadlFields(KeyValuePair<string, string> coreBo, KeyValuePair<string, string> coreBoNode)
    {
      string empty = string.Empty;
      XRepositoryProxy.Instance.GetExtensionFieldsForBO(coreBo, coreBoNode, this.Solution, out this.contextArray, out this.fieldArray, ref empty, true);
    }

    private void LoadListChangeTransaction(ObservableCollection<AdjustablePropertiesEntity> adjustablePropertiesEntity, List list, string uiSwitchId, bool isUISwitchEqual)
    {
      adjustablePropertiesEntity.Clear();
      this.VisibleColumn.Visibility = Visibility.Visible;
      this.EnabledColumn.Visibility = Visibility.Collapsed;
      this.MandatoryColumn.Visibility = Visibility.Visible;
      this.ReadOnlyColumn.Visibility = Visibility.Visible;
      AdjustablePropertiesEntity propertiesEntity1 = new AdjustablePropertiesEntity();
      AdjustablePropertiesEntity propertiesEntity2 = new AdjustablePropertiesEntity()
      {
        FieldBackendBindingPath = (string) null,
        IsCompondFieldChildControl = false
      };
      foreach (ListColumn header in list.Headers)
      {
        if (header.Item != null)
        {
          string str = (string) null;
          string oldValue = (string) null;
          string field_BindingPath = (string) null;
          if (header.Item.BoundField != null && header.Item.BoundField.BackendBindingPath != null)
            str = header.Item.BoundField.BackendBindingPath;
          if (header.Item.BoundField != null && header.Item.BoundField.BindingExpression != null)
            oldValue = header.Item.BoundField.BindingExpression;
          if (header.Item.BoundField != null && str != null)
            field_BindingPath = !(oldValue == "") ? str.Replace(oldValue, string.Empty) : str;
          string controlHeader1 = header.Label != null ? header.Label.Text : header.Item.Name;
          ModelEntity oberonModel = header.GetOberonModel();
          if (header.Item is CompoundField)
          {
            foreach (AbstractControl control in (header.Item as CompoundField).Items)
            {
              string controlHeader2 = controlHeader1 + "_" + control.TypeName;
              if (control.BoundField != null && control.BoundField.BackendBindingPath != null)
                field_BindingPath = control.BoundField.BackendBindingPath;
              AdjustablePropertiesEntity basedOnControlUsage = this.GetAdjustablePropertiesEntityBasedOnControlUsage(control, oberonModel, controlHeader2, uiSwitchId, isUISwitchEqual, field_BindingPath);
              basedOnControlUsage.IsCompondFieldChildControl = true;
              adjustablePropertiesEntity.Add(basedOnControlUsage);
            }
            AdjustablePropertiesEntity basedOnControlUsage1 = this.GetAdjustablePropertiesEntityBasedOnControlUsage(header.Item, oberonModel, controlHeader1, uiSwitchId, isUISwitchEqual, field_BindingPath);
            adjustablePropertiesEntity.Add(basedOnControlUsage1);
          }
          else
          {
            AdjustablePropertiesEntity basedOnControlUsage = this.GetAdjustablePropertiesEntityBasedOnControlUsage(header.Item, oberonModel, controlHeader1, uiSwitchId, isUISwitchEqual, field_BindingPath);
            adjustablePropertiesEntity.Add(basedOnControlUsage);
          }
        }
      }
    }

    private void LoadSectionGroupChangeTransaction(ObservableCollection<AdjustablePropertiesEntity> adjustablePropertiesEntity, FieldGroup section, string uiSwitchId, bool isUISwitchEqual)
    {
      adjustablePropertiesEntity.Clear();
      this.VisibleColumn.Visibility = Visibility.Visible;
      this.EnabledColumn.Visibility = Visibility.Collapsed;
      this.MandatoryColumn.Visibility = Visibility.Visible;
      this.ReadOnlyColumn.Visibility = Visibility.Visible;
      AdjustablePropertiesEntity propertiesEntity1 = new AdjustablePropertiesEntity();
      AdjustablePropertiesEntity propertiesEntity2 = new AdjustablePropertiesEntity();
      propertiesEntity2.AssociatedControlId = section.Id;
      propertiesEntity2.AssociatedExtensibleModel = (ModelEntity) null;
      propertiesEntity2.ControlHeader = section.SectionGroupName == null || !(section.SectionGroupName.ToString() != "") ? "Section Group Header" : section.SectionGroupName.ToString();
      this.SetValueBasedOnSwitch(propertiesEntity2.Visible, section.Visible.DependentPropertyType, uiSwitchId, isUISwitchEqual);
      propertiesEntity2.HasReadOnlyProperty = false;
      propertiesEntity2.HasMandatoryProperty = false;
      adjustablePropertiesEntity.Add(propertiesEntity2);
      propertiesEntity2.FieldBackendBindingPath = (string) null;
      foreach (SectionGroupItem field in section.Fields)
      {
        if (field.Item != null)
        {
          string str = (string) null;
          string oldValue = (string) null;
          string field_BindingPath = (string) null;
          if (field.Item.BoundField != null && field.Item.BoundField.BackendBindingPath != null)
            str = field.Item.BoundField.BackendBindingPath;
          if (field.Item.BoundField != null && field.Item.BoundField.BindingExpression != null)
            oldValue = field.Item.BoundField.BindingExpression;
          if (field.Item.BoundField != null && str != null)
            field_BindingPath = !(oldValue == "") ? str.Replace(oldValue, string.Empty) : str;
          string controlHeader1 = field.Label != null ? field.Label.Text : field.Item.Name;
          ModelEntity oberonModel = field.GetOberonModel();
          if (field.Item is CompoundField)
          {
            foreach (AbstractControl control in (field.Item as CompoundField).Items)
            {
              string controlHeader2 = controlHeader1 + "_" + control.TypeName;
              if (control.BoundField != null && control.BoundField.BackendBindingPath != null)
                field_BindingPath = control.BoundField.BackendBindingPath;
              AdjustablePropertiesEntity basedOnControlUsage = this.GetAdjustablePropertiesEntityBasedOnControlUsage(control, oberonModel, controlHeader2, uiSwitchId, isUISwitchEqual, field_BindingPath);
              adjustablePropertiesEntity.Add(basedOnControlUsage);
            }
            AdjustablePropertiesEntity basedOnControlUsage1 = this.GetAdjustablePropertiesEntityBasedOnControlUsage(field.Item, oberonModel, controlHeader1, uiSwitchId, isUISwitchEqual, field_BindingPath);
            adjustablePropertiesEntity.Add(basedOnControlUsage1);
          }
          else
          {
            AdjustablePropertiesEntity basedOnControlUsage = this.GetAdjustablePropertiesEntityBasedOnControlUsage(field.Item, oberonModel, controlHeader1, uiSwitchId, isUISwitchEqual, field_BindingPath);
            adjustablePropertiesEntity.Add(basedOnControlUsage);
          }
        }
      }
    }

    private void LoadButtonGroupChangeTransaction(ObservableCollection<AdjustablePropertiesEntity> adjustablePropertiesEntity, ButtonGroup buttonGroup, string uiSwitchId, bool isUISwitchEqual)
    {
      adjustablePropertiesEntity.Clear();
      this.VisibleColumn.Visibility = Visibility.Visible;
      this.EnabledColumn.Visibility = Visibility.Visible;
      this.MandatoryColumn.Visibility = Visibility.Collapsed;
      this.ReadOnlyColumn.Visibility = Visibility.Collapsed;
      if (buttonGroup == null)
        return;
      foreach (SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button button in buttonGroup.Buttons)
      {
        if (!button.HasMenu || button.SubMenu.Count == 1)
        {
          AdjustablePropertiesEntity propertiesEntity = new AdjustablePropertiesEntity();
          propertiesEntity.AssociatedControlId = button.Id;
          propertiesEntity.ControlHeader = button.Text != null ? button.Text.Text : string.Empty;
          this.SetValueBasedOnSwitch(propertiesEntity.Visible, button.Visible.DependentPropertyType, uiSwitchId, isUISwitchEqual);
          this.SetValueBasedOnSwitch(propertiesEntity.Enabled, button.Enabled.DependentPropertyType, uiSwitchId, isUISwitchEqual);
          adjustablePropertiesEntity.Add(propertiesEntity);
        }
        else if (button.IsActionSubMenu && button.ActionButtonSubMenu != null)
        {
          foreach (ActionNavigationItem actionNavigationItem in button.ActionButtonSubMenu)
          {
            AdjustablePropertiesEntity propertiesEntity = new AdjustablePropertiesEntity();
            propertiesEntity.AssociatedControlId = actionNavigationItem.Id;
            propertiesEntity.ControlHeader = actionNavigationItem.Title != null ? actionNavigationItem.Title.Text : string.Empty;
            this.SetValueBasedOnSwitch(propertiesEntity.Visible, actionNavigationItem.Visible.DependentPropertyType, uiSwitchId, isUISwitchEqual);
            this.SetValueBasedOnSwitch(propertiesEntity.Enabled, actionNavigationItem.Enabled.DependentPropertyType, uiSwitchId, isUISwitchEqual);
            adjustablePropertiesEntity.Add(propertiesEntity);
          }
        }
        else if (button.IsSubMenu && button.SubMenu != null)
        {
          foreach (NavigationItem navigationItem in button.SubMenu)
          {
            AdjustablePropertiesEntity propertiesEntity = new AdjustablePropertiesEntity();
            propertiesEntity.AssociatedControlId = navigationItem.Id;
            propertiesEntity.ControlHeader = navigationItem.Title != null ? navigationItem.Title.Text : string.Empty;
            this.SetValueBasedOnSwitch(propertiesEntity.Visible, navigationItem.Visible.DependentPropertyType, uiSwitchId, isUISwitchEqual);
            this.SetValueBasedOnSwitch(propertiesEntity.Enabled, navigationItem.Enabled.DependentPropertyType, uiSwitchId, isUISwitchEqual);
            adjustablePropertiesEntity.Add(propertiesEntity);
          }
        }
      }
    }

    private void LoadViewSwitchChangeTransaction(ObservableCollection<AdjustablePropertiesEntity> adjustablePropertiesEntity, ViewSwitchNavigation viewSwitchNavigation, string uiSwitchId, bool isUISwitchEqual)
    {
      adjustablePropertiesEntity.Clear();
      this.VisibleColumn.Visibility = Visibility.Visible;
      this.EnabledColumn.Visibility = Visibility.Visible;
      this.MandatoryColumn.Visibility = Visibility.Collapsed;
      this.ReadOnlyColumn.Visibility = Visibility.Collapsed;
      if (viewSwitchNavigation == null)
        return;
      foreach (NavigationItem navigationItem in viewSwitchNavigation.NavigationItems)
      {
        AdjustablePropertiesEntity propertiesEntity = new AdjustablePropertiesEntity();
        propertiesEntity.AssociatedControlId = navigationItem.Id;
        propertiesEntity.ControlHeader = navigationItem.Title != null ? navigationItem.Title.Text : string.Empty;
        this.SetValueBasedOnSwitch(propertiesEntity.Visible, navigationItem.Visible.DependentPropertyType, uiSwitchId, isUISwitchEqual);
        this.SetValueBasedOnSwitch(propertiesEntity.Enabled, navigationItem.Enabled.DependentPropertyType, uiSwitchId, isUISwitchEqual);
        adjustablePropertiesEntity.Add(propertiesEntity);
      }
    }

    private AdjustablePropertiesEntity GetAdjustablePropertiesEntityBasedOnControlUsage(AbstractControl control, ModelEntity associatedExtensibleModel, string controlHeader, string uiSwitchId, bool isUISwitchEqual, string field_BindingPath)
    {
      AdjustablePropertiesEntity propertiesEntity = new AdjustablePropertiesEntity();
      propertiesEntity.AssociatedControlId = control.Id;
      propertiesEntity.AssociatedExtensibleModel = associatedExtensibleModel;
      propertiesEntity.ControlHeader = controlHeader;
      propertiesEntity.HasEnabledProperty = false;
      propertiesEntity.FieldBackendBindingPath = field_BindingPath;
      this.SetValueBasedOnSwitch(propertiesEntity.Visible, control.Visible.DependentPropertyType, uiSwitchId, isUISwitchEqual);
      if (control is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link)
      {
        propertiesEntity.HasReadOnlyProperty = false;
        SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link link = control as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link;
        if (link != null && link.AllowEdit != null && (link.AllowEdit.Text != null && link.AllowEdit.Text.Equals("true")))
        {
          if (link.Mandatory.Text == null)
            this.SetValueBasedOnSwitch(propertiesEntity.Mandatory, link.Mandatory.DependentPropertyType, uiSwitchId, isUISwitchEqual);
          else if (link.Mandatory.Text.Equals("false"))
            this.SetValueBasedOnSwitch(propertiesEntity.Mandatory, link.Mandatory.DependentPropertyType, uiSwitchId, isUISwitchEqual);
        }
        else
          propertiesEntity.HasMandatoryProperty = false;
      }
      else if (control is EditControl)
      {
        EditControl editControl = control as EditControl;
        this.SetValueBasedOnSwitch(propertiesEntity.Mandatory, editControl.Mandatory.DependentPropertyType, uiSwitchId, isUISwitchEqual);
        this.SetValueBasedOnSwitch(propertiesEntity.ReadOnly, editControl.ReadOnly.DependentPropertyType, uiSwitchId, isUISwitchEqual);
      }
      else
      {
        propertiesEntity.HasReadOnlyProperty = false;
        propertiesEntity.HasMandatoryProperty = false;
      }
      if (control is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button || control is Icon)
      {
        this.EnabledColumn.Visibility = Visibility.Visible;
        this.SetValueBasedOnSwitch(propertiesEntity.Enabled, control.Enabled.DependentPropertyType, uiSwitchId, isUISwitchEqual);
      }
      return propertiesEntity;
    }

    private void SetValueBasedOnSwitch(PropertyValueHolder valueHolder, DependentPropertyType dependentProperty, string uiSwitch, bool isUISwitchEqual)
    {
      valueHolder.Value = PropertyValues.None;
      if (dependentProperty == null || dependentProperty.PostCondition == null || dependentProperty.PostCondition.Count <= 0)
        return;
      string idBasedOnUiSwitch = this.GetConditionIdBasedOnUISwitch(dependentProperty, uiSwitch, isUISwitchEqual);
      this.SetValueBasedOnConditionId(valueHolder, dependentProperty, idBasedOnUiSwitch);
    }

    private string GetConditionIdBasedOnUISwitch(DependentPropertyType dependentProperty, string uiSwitch, bool isUISwitchEqual)
    {
      string str = (string) null;
      if (dependentProperty != null && dependentProperty.PostCondition != null && (dependentProperty.PostCondition.Count > 0 && !string.IsNullOrEmpty(uiSwitch)))
      {
        PostConditionType postConditionType = dependentProperty.PostCondition.Find((Predicate<PostConditionType>) (p => p.section == ConditionSectionType.depending));
        if (postConditionType != null)
        {
          ConditionPropertyType conditionPropertyType = (isUISwitchEqual ? postConditionType.Equals : postConditionType.NotEquals).Find((Predicate<ConditionPropertyType>) (switchCondition => switchCondition.depending == uiSwitch));
          if (conditionPropertyType != null)
            str = conditionPropertyType.id;
        }
      }
      return str;
    }

    private void SetValueBasedOnConditionId(PropertyValueHolder valueHolder, DependentPropertyType dependentProperty, string conditionId)
    {
      foreach (PostConditionType postConditionType in dependentProperty.PostCondition)
      {
        if (postConditionType.section == ConditionSectionType.@static)
        {
          ConditionPropertyType conditionPropertyType = postConditionType.Equals.Find((Predicate<ConditionPropertyType>) (vCondition => vCondition.depending == conditionId));
          if (conditionPropertyType != null)
          {
            valueHolder.Value = Convert.ToBoolean(conditionPropertyType.fallbackValue) ? PropertyValues.True : PropertyValues.False;
            break;
          }
        }
        else if (postConditionType.section == ConditionSectionType.binding)
        {
          ConditionPropertyType conditionPropertyType = postConditionType.Equals.Find((Predicate<ConditionPropertyType>) (vCondition => vCondition.depending == conditionId));
          if (conditionPropertyType != null)
          {
            valueHolder.Value = PropertyValues.Bound;
            if (string.IsNullOrEmpty(conditionPropertyType.bindingExpression))
              break;
            IImplementationController implementationController = Model.Utilities.GetImplementationController(this.DTExtensibleModel);
            if (implementationController == null || implementationController.DataModel == null)
              break;
            IBaseDataElementType dataFieldByFullPath = implementationController.DataModel.GetDataFieldByFullPath(conditionPropertyType.bindingExpression);
            if (dataFieldByFullPath == null || dataFieldByFullPath.Binding == null)
              break;
            valueHolder.Field = dataFieldByFullPath.BindingExpression;
            valueHolder.BO = dataFieldByFullPath.BindingObject;
            valueHolder.Namespace = dataFieldByFullPath.BindingNameSpace;
            valueHolder.FieldExtensionNamespace = dataFieldByFullPath.Binding.FieldESRNamespace;
            break;
          }
        }
      }
    }

    protected override void revertChanges()
    {
    }

    private void OnLoadVisibleColumn(object sender, RoutedEventArgs args)
    {
      this.m_HandleEvents = false;
      if (sender is System.Windows.Controls.DataGridCell)
      {
        System.Windows.Controls.DataGridCell dataGridCell = sender as System.Windows.Controls.DataGridCell;
        object dataContext = dataGridCell.DataContext;
        Grid visualChild = Model.Utilities.GetVisualChild<Grid>((Visual) (dataGridCell.Content as ContentPresenter));
        if (visualChild != null && visualChild.Children.Count > 1 && (visualChild.Children[0] is System.Windows.Controls.ComboBox && visualChild.Children[1] is System.Windows.Controls.Button))
          (visualChild.Children[0] as System.Windows.Controls.ComboBox).ItemsSource = (IEnumerable) this.m_TristateBooleanValueSource;
      }
      this.m_HandleEvents = true;
    }

    private void OnLoadEnabledColumn(object sender, RoutedEventArgs args)
    {
      this.m_HandleEvents = false;
      if (sender is System.Windows.Controls.DataGridCell)
      {
        System.Windows.Controls.DataGridCell dataGridCell = sender as System.Windows.Controls.DataGridCell;
        object dataContext = dataGridCell.DataContext;
        Grid visualChild = Model.Utilities.GetVisualChild<Grid>((Visual) (dataGridCell.Content as ContentPresenter));
        if (visualChild != null && visualChild.Children.Count > 1 && (visualChild.Children[0] is System.Windows.Controls.ComboBox && visualChild.Children[1] is System.Windows.Controls.Button))
          (visualChild.Children[0] as System.Windows.Controls.ComboBox).ItemsSource = (IEnumerable) this.m_TristateBooleanValueSource;
      }
      this.m_HandleEvents = true;
    }

    private void OnLoadReadOnlyColumn(object sender, RoutedEventArgs args)
    {
      this.m_HandleEvents = false;
      if (sender is System.Windows.Controls.DataGridCell)
      {
        System.Windows.Controls.DataGridCell dataGridCell = sender as System.Windows.Controls.DataGridCell;
        object dataContext = dataGridCell.DataContext;
        Grid visualChild = Model.Utilities.GetVisualChild<Grid>((Visual) (dataGridCell.Content as ContentPresenter));
        if (visualChild != null && visualChild.Children.Count > 1 && (visualChild.Children[0] is System.Windows.Controls.ComboBox && visualChild.Children[1] is System.Windows.Controls.Button))
          (visualChild.Children[0] as System.Windows.Controls.ComboBox).ItemsSource = (IEnumerable) this.m_TristateBooleanValueSource;
      }
      this.m_HandleEvents = true;
    }

    private void OnLoadMandatoryColumn(object sender, RoutedEventArgs args)
    {
      this.m_HandleEvents = false;
      if (sender is System.Windows.Controls.DataGridCell)
      {
        System.Windows.Controls.DataGridCell dataGridCell = sender as System.Windows.Controls.DataGridCell;
        object dataContext = dataGridCell.DataContext;
        Grid visualChild = Model.Utilities.GetVisualChild<Grid>((Visual) (dataGridCell.Content as ContentPresenter));
        if (visualChild != null && visualChild.Children.Count > 1 && (visualChild.Children[0] is System.Windows.Controls.ComboBox && visualChild.Children[1] is System.Windows.Controls.Button))
          (visualChild.Children[0] as System.Windows.Controls.ComboBox).ItemsSource = (IEnumerable) this.m_TristateBooleanValueSource;
      }
      this.m_HandleEvents = true;
    }

    private void VisibleBindingButton_Click(object sender, RoutedEventArgs e)
    {
      if (!(sender is System.Windows.Controls.Button))
        return;
      AdjustablePropertiesEntity dataContext = (sender as System.Windows.Controls.Button).DataContext as AdjustablePropertiesEntity;
      this.OpenBOBrowser(dataContext.Visible, dataContext.AssociatedControlId);
    }

    private void VisibleCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!this.m_HandleEvents || !(sender is System.Windows.Controls.ComboBox) || (e.AddedItems.Count <= 0 || e.RemovedItems.Count <= 0) || !((System.Windows.Controls.ComboBox) sender).IsDropDownOpen)
        return;
      System.Windows.Controls.ComboBox comboBox = sender as System.Windows.Controls.ComboBox;
      AdjustablePropertiesEntity dataContext = comboBox.DataContext as AdjustablePropertiesEntity;
      dataContext.Visible.Value = (PropertyValues) Enum.Parse(typeof (PropertyValues), e.AddedItems[0].ToString());
      dataContext.IsVisibleDirty = true;
      Grid parent = comboBox.Parent as Grid;
      if (parent == null || parent.Children.Count <= 1 || !(parent.Children[1] is System.Windows.Controls.Button))
        return;
      System.Windows.Controls.Button child = parent.Children[1] as System.Windows.Controls.Button;
      if (dataContext.Visible.Value == PropertyValues.Bound)
      {
        child.Visibility = Visibility.Visible;
        this.OpenBOBrowser(dataContext.Visible, dataContext.AssociatedControlId);
        if (this.IsFieldBound)
          return;
        child.Visibility = Visibility.Collapsed;
        comboBox.SelectedValue = (object) PropertyValues.None;
      }
      else
        child.Visibility = Visibility.Collapsed;
    }

    private void EnabledBindingButton_Click(object sender, RoutedEventArgs e)
    {
      if (!(sender is System.Windows.Controls.Button))
        return;
      AdjustablePropertiesEntity dataContext = (sender as System.Windows.Controls.Button).DataContext as AdjustablePropertiesEntity;
      this.OpenBOBrowser(dataContext.Enabled, dataContext.AssociatedControlId);
    }

    private void EnabledCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!this.m_HandleEvents || !(sender is System.Windows.Controls.ComboBox) || (e.AddedItems.Count <= 0 || e.RemovedItems.Count <= 0) || !((System.Windows.Controls.ComboBox) sender).IsDropDownOpen)
        return;
      System.Windows.Controls.ComboBox comboBox = sender as System.Windows.Controls.ComboBox;
      AdjustablePropertiesEntity dataContext = comboBox.DataContext as AdjustablePropertiesEntity;
      dataContext.Enabled.Value = (PropertyValues) Enum.Parse(typeof (PropertyValues), e.AddedItems[0].ToString());
      dataContext.IsEnabledDirty = true;
      Grid parent = comboBox.Parent as Grid;
      if (parent == null || parent.Children.Count <= 1 || !(parent.Children[1] is System.Windows.Controls.Button))
        return;
      System.Windows.Controls.Button child = parent.Children[1] as System.Windows.Controls.Button;
      if (dataContext.Enabled.Value == PropertyValues.Bound)
      {
        child.Visibility = Visibility.Visible;
        this.OpenBOBrowser(dataContext.Enabled, dataContext.AssociatedControlId);
        if (this.IsFieldBound)
          return;
        child.Visibility = Visibility.Collapsed;
        comboBox.SelectedValue = (object) PropertyValues.None;
      }
      else
        child.Visibility = Visibility.Collapsed;
    }

    private void MandatoryBindingButton_Click(object sender, RoutedEventArgs e)
    {
      if (!(sender is System.Windows.Controls.Button))
        return;
      AdjustablePropertiesEntity dataContext = (sender as System.Windows.Controls.Button).DataContext as AdjustablePropertiesEntity;
      this.OpenBOBrowser(dataContext.Mandatory, dataContext.AssociatedControlId);
    }

    private void MandatoryCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!this.m_HandleEvents || !(sender is System.Windows.Controls.ComboBox) || (e.AddedItems.Count <= 0 || e.RemovedItems.Count <= 0) || !((System.Windows.Controls.ComboBox) sender).IsDropDownOpen)
        return;
      System.Windows.Controls.ComboBox comboBox = sender as System.Windows.Controls.ComboBox;
      AdjustablePropertiesEntity dataContext = comboBox.DataContext as AdjustablePropertiesEntity;
      dataContext.Mandatory.Value = (PropertyValues) Enum.Parse(typeof (PropertyValues), e.AddedItems[0].ToString());
      dataContext.IsMandatoryDirty = true;
      Grid parent = comboBox.Parent as Grid;
      if (parent == null || parent.Children.Count <= 1 || !(parent.Children[1] is System.Windows.Controls.Button))
        return;
      System.Windows.Controls.Button child = parent.Children[1] as System.Windows.Controls.Button;
      if (dataContext.Mandatory.Value == PropertyValues.Bound)
      {
        child.Visibility = Visibility.Visible;
        this.OpenBOBrowser(dataContext.Mandatory, dataContext.AssociatedControlId);
        if (this.IsFieldBound)
          return;
        child.Visibility = Visibility.Collapsed;
        comboBox.SelectedValue = (object) PropertyValues.None;
      }
      else
        child.Visibility = Visibility.Collapsed;
    }

    private void ReadOnlyBindingButton_Click(object sender, RoutedEventArgs e)
    {
      if (!(sender is System.Windows.Controls.Button))
        return;
      AdjustablePropertiesEntity dataContext = (sender as System.Windows.Controls.Button).DataContext as AdjustablePropertiesEntity;
      this.OpenBOBrowser(dataContext.Mandatory, dataContext.AssociatedControlId);
    }

    private void ReadOnlyCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!this.m_HandleEvents || !(sender is System.Windows.Controls.ComboBox) || (e.AddedItems.Count <= 0 || e.RemovedItems.Count <= 0) || !((System.Windows.Controls.ComboBox) sender).IsDropDownOpen)
        return;
      System.Windows.Controls.ComboBox comboBox = sender as System.Windows.Controls.ComboBox;
      AdjustablePropertiesEntity dataContext = comboBox.DataContext as AdjustablePropertiesEntity;
      dataContext.ReadOnly.Value = (PropertyValues) Enum.Parse(typeof (PropertyValues), e.AddedItems[0].ToString());
      dataContext.IsReadOnlyDirty = true;
      Grid parent = comboBox.Parent as Grid;
      if (parent == null || parent.Children.Count <= 1 || !(parent.Children[1] is System.Windows.Controls.Button))
        return;
      System.Windows.Controls.Button child = parent.Children[1] as System.Windows.Controls.Button;
      if (dataContext.ReadOnly.Value == PropertyValues.Bound)
      {
        child.Visibility = Visibility.Visible;
        this.OpenBOBrowser(dataContext.ReadOnly, dataContext.AssociatedControlId);
        if (this.IsFieldBound)
          return;
        child.Visibility = Visibility.Collapsed;
        comboBox.SelectedValue = (object) PropertyValues.None;
      }
      else
        child.Visibility = Visibility.Collapsed;
    }

    private void OnPropertyValueChanged(object sender, SelectionChangedEventArgs args)
    {
      if (!this.m_HandleEvents || args.RemovedItems.Count <= 0 || (args.AddedItems.Count <= 0 || !(this.controlsDataGrid.CurrentItem is AdjustablePropertiesEntity)))
        return;
      AdjustablePropertiesEntity currentItem = this.controlsDataGrid.CurrentItem as AdjustablePropertiesEntity;
      if (this.controlsDataGrid.CurrentColumn == this.VisibleColumn)
        currentItem.IsVisibleDirty = true;
      else if (this.controlsDataGrid.CurrentColumn == this.EnabledColumn)
        currentItem.IsEnabledDirty = true;
      else if (this.controlsDataGrid.CurrentColumn == this.ReadOnlyColumn)
      {
        currentItem.IsReadOnlyDirty = true;
      }
      else
      {
        if (this.controlsDataGrid.CurrentColumn != this.MandatoryColumn)
          return;
        currentItem.IsMandatoryDirty = true;
      }
    }

    private void OnUISwitchSelectionChanged(object sender, SelectionChangedEventArgs args)
    {
      this.m_AdjustableCTEntity.AdjustablePropertEntities.Clear();
      if (!this.m_HandleEvents)
        return;
      if (this.DTExtensibleModel is List)
        this.LoadListChangeTransaction(this.m_AdjustableCTEntity.AdjustablePropertEntities, this.DTExtensibleModel as List, this.m_AdjustableCTEntity.UISwitchId, this.m_AdjustableCTEntity.UISwitchCondition == "Equal");
      else if (this.DTExtensibleModel is FieldGroup)
        this.LoadSectionGroupChangeTransaction(this.m_AdjustableCTEntity.AdjustablePropertEntities, this.DTExtensibleModel as FieldGroup, this.m_AdjustableCTEntity.UISwitchId, this.m_AdjustableCTEntity.UISwitchCondition == "Equal");
      else if (this.DTExtensibleModel is ViewSwitchNavigation)
      {
        this.LoadViewSwitchChangeTransaction(this.m_AdjustableCTEntity.AdjustablePropertEntities, this.DTExtensibleModel as ViewSwitchNavigation, this.m_AdjustableCTEntity.UISwitchId, this.m_AdjustableCTEntity.UISwitchCondition == "Equal");
      }
      else
      {
        if (!(this.DTExtensibleModel is ButtonGroup))
          return;
        this.LoadButtonGroupChangeTransaction(this.m_AdjustableCTEntity.AdjustablePropertEntities, this.DTExtensibleModel as ButtonGroup, this.m_AdjustableCTEntity.UISwitchId, this.m_AdjustableCTEntity.UISwitchCondition == "Equal");
      }
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      string str1 = !string.IsNullOrEmpty(this.m_AdjustableCTEntity.UISwitchId) ? this.m_AdjustableCTEntity.UISwitchId : string.Empty;
      RoleDependingType depending = new RoleDependingType()
      {
        key = str1,
        isEqual = this.m_AdjustableCTEntity.UISwitchCondition == "Equal"
      };
      foreach (AdjustablePropertiesEntity adjustablePropertEntity in (Collection<AdjustablePropertiesEntity>) this.m_AdjustableCTEntity.AdjustablePropertEntities)
      {
        if (adjustablePropertEntity.IsDirty)
        {
          string str2 = string.Empty;
          if (adjustablePropertEntity.IsVisibleDirty)
          {
            string controlId = adjustablePropertEntity.AssociatedExtensibleModel is ListColumnType ? adjustablePropertEntity.AssociatedExtensibleModel.id : adjustablePropertEntity.AssociatedControlId;
            if (adjustablePropertEntity.IsCompondFieldChildControl)
              controlId = adjustablePropertEntity.AssociatedControlId;
            if (adjustablePropertEntity.Visible.Value != PropertyValues.Bound)
            {
              bool flag = adjustablePropertEntity.Visible.Value == PropertyValues.True;
              str2 = this.m_FlexibilityHandler.AddCondition(this.Anchor, adjustablePropertEntity.AssociatedExtensibleModel, controlId, "Visible", string.Empty, string.Empty, string.Empty, string.Empty, flag.ToString(), true, depending);
            }
            else
              str2 = this.m_FlexibilityHandler.AddCondition(this.Anchor, adjustablePropertEntity.AssociatedExtensibleModel, controlId, "Visible", adjustablePropertEntity.Visible.BO, adjustablePropertEntity.Visible.Field, adjustablePropertEntity.Visible.FieldExtensionNamespace, adjustablePropertEntity.Visible.Namespace, string.Empty, true, depending);
          }
          if (adjustablePropertEntity.IsEnabledDirty)
          {
            if (adjustablePropertEntity.Enabled.Value != PropertyValues.Bound)
            {
              bool flag = adjustablePropertEntity.Enabled.Value == PropertyValues.True;
              str2 = this.m_FlexibilityHandler.AddCondition(this.Anchor, adjustablePropertEntity.AssociatedExtensibleModel, adjustablePropertEntity.AssociatedControlId, "Enabled", string.Empty, string.Empty, string.Empty, string.Empty, flag.ToString(), true, depending);
            }
            else
              str2 = this.m_FlexibilityHandler.AddCondition(this.Anchor, adjustablePropertEntity.AssociatedExtensibleModel, adjustablePropertEntity.AssociatedControlId, "Enabled", adjustablePropertEntity.Enabled.BO, adjustablePropertEntity.Enabled.Field, adjustablePropertEntity.Enabled.FieldExtensionNamespace, adjustablePropertEntity.Enabled.Namespace, string.Empty, true, depending);
          }
          if (adjustablePropertEntity.IsMandatoryDirty)
          {
            if (adjustablePropertEntity.Mandatory.Value != PropertyValues.Bound)
            {
              bool flag = adjustablePropertEntity.Mandatory.Value == PropertyValues.True;
              str2 = this.m_FlexibilityHandler.AddCondition(this.Anchor, adjustablePropertEntity.AssociatedExtensibleModel, adjustablePropertEntity.AssociatedControlId, "Mandatory", string.Empty, string.Empty, string.Empty, string.Empty, flag.ToString(), true, depending);
            }
            else
              str2 = this.m_FlexibilityHandler.AddCondition(this.Anchor, adjustablePropertEntity.AssociatedExtensibleModel, adjustablePropertEntity.AssociatedControlId, "Mandatory", adjustablePropertEntity.Mandatory.BO, adjustablePropertEntity.Mandatory.Field, adjustablePropertEntity.Mandatory.FieldExtensionNamespace, adjustablePropertEntity.Mandatory.Namespace, string.Empty, true, depending);
          }
          if (adjustablePropertEntity.IsReadOnlyDirty)
          {
            string propertyName = adjustablePropertEntity.AssociatedExtensibleModel is ListType ? "ReadOnly" : "Readonly";
            if (adjustablePropertEntity.ReadOnly.Value != PropertyValues.Bound)
            {
              bool flag = adjustablePropertEntity.ReadOnly.Value == PropertyValues.True;
              str2 = this.m_FlexibilityHandler.AddCondition(this.Anchor, adjustablePropertEntity.AssociatedExtensibleModel, adjustablePropertEntity.AssociatedControlId, propertyName, string.Empty, string.Empty, string.Empty, string.Empty, flag.ToString(), true, depending);
            }
            else
              str2 = this.m_FlexibilityHandler.AddCondition(this.Anchor, adjustablePropertEntity.AssociatedExtensibleModel, adjustablePropertEntity.AssociatedControlId, propertyName, adjustablePropertEntity.ReadOnly.BO, adjustablePropertEntity.ReadOnly.Field, adjustablePropertEntity.ReadOnly.FieldExtensionNamespace, adjustablePropertEntity.ReadOnly.Namespace, string.Empty, true, depending);
          }
          this.IsChangeApplied = !string.IsNullOrEmpty(str2);
        }
      }
      if (!this.isSadlCase && this.infoList != null && this.infoList.Count > 0)
      {
        string empty = string.Empty;
        XRepositoryProxy.Instance.CreateExtensionFieldsOnEco(this.m_FlexibilityHandler, this.Solution, this.infoList, ref empty);
      }
      if (!this.IsChangeApplied)
      {
        int num = (int) DisplayMessage.Show("No changes detected", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
      }
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void OpenBOBrowser(PropertyValueHolder propertyValue, string selectedControlId)
    {
      if (string.IsNullOrEmpty(this.m_BOName) || string.IsNullOrEmpty(this.m_BONamspace))
      {
        IImplementationController implementationController = Model.Utilities.GetImplementationController(this.DTExtensibleModel);
        if (implementationController == null || implementationController.DataModel == null)
          return;
        this.m_BOName = implementationController.DataModel.RootBOName;
        this.m_BONamspace = implementationController.DataModel.RootNameSpace;
      }
      BOFieldSelectorDialog fieldSelectorDialog = new BOFieldSelectorDialog(this.m_BOName, this.m_BONamspace, propertyValue.Field, true);
      if (!string.IsNullOrEmpty(fieldSelectorDialog.SelectedBO))
      {
        fieldSelectorDialog.ShowDialog();
        bool? dialogResult = fieldSelectorDialog.DialogResult;
        if ((!dialogResult.GetValueOrDefault() ? 0 : (dialogResult.HasValue ? 1 : 0)) != 0 && fieldSelectorDialog.SelectedPath != "")
        {
          this.IsFieldBound = true;
          propertyValue.Namespace = fieldSelectorDialog.SelectedNamespace;
          propertyValue.BO = fieldSelectorDialog.SelectedBO;
          propertyValue.Field = fieldSelectorDialog.SelectedPath;
          if (fieldSelectorDialog.SelectedFieldExtensionNamespace == null)
            return;
          propertyValue.FieldExtensionNamespace = fieldSelectorDialog.SelectedFieldExtensionNamespace;
        }
        else
          this.IsFieldBound = false;
      }
      else
      {
        BOExtensionFieldSelector extensionFieldSelector = new BOExtensionFieldSelector(this.contextArray, this.fieldArray);
        extensionFieldSelector.ShowDialog();
        bool? dialogResult = extensionFieldSelector.DialogResult;
        if ((!dialogResult.GetValueOrDefault() ? 0 : (dialogResult.HasValue ? 1 : 0)) != 0)
        {
          this.IsFieldBound = true;
          propertyValue.Namespace = this.m_BONamspace;
          propertyValue.BO = this.m_BOName;
          propertyValue.Field = "Root-~" + extensionFieldSelector.Item.AppearanceName;
          foreach (AdjustablePropertiesEntity adjustablePropertEntity in (Collection<AdjustablePropertiesEntity>) this.m_AdjustableCTEntity.AdjustablePropertEntities)
          {
            if (selectedControlId == adjustablePropertEntity.AssociatedControlId && !string.IsNullOrEmpty(adjustablePropertEntity.FieldBackendBindingPath))
            {
              if (adjustablePropertEntity.FieldBackendBindingPath.Contains<char>('~'))
              {
                propertyValue.Field = adjustablePropertEntity.FieldBackendBindingPath.Split('~')[0] + "~" + extensionFieldSelector.Item.AppearanceName;
                break;
              }
              propertyValue.Field = adjustablePropertEntity.FieldBackendBindingPath + "-~" + extensionFieldSelector.Item.AppearanceName;
              break;
            }
          }
          propertyValue.FieldExtensionNamespace = extensionFieldSelector.Item.EsrNamespace;
          this.infoList.Add(new AppearanceInfo()
          {
            FieldName = extensionFieldSelector.Item.EsrName,
            FieldNameSpace = extensionFieldSelector.Item.EsrNamespace,
            ReferenceBundleKey = extensionFieldSelector.Item.ReferenceBundleKey,
            ReferenceFieldName = extensionFieldSelector.Item.ReferenceFieldName
          });
        }
        else
          this.IsFieldBound = false;
      }
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/adjustpropertiesmodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.rootGrid = (Grid) target;
    //      break;
    //    case 2:
    //      this.uiSwitchGrid = (Grid) target;
    //      break;
    //    case 3:
    //      this.uiSwitchTextBlock = (TextBlock) target;
    //      break;
    //    case 4:
    //      this.OperatorComboBox = (System.Windows.Controls.ComboBox) target;
    //      this.OperatorComboBox.SelectionChanged += new SelectionChangedEventHandler(this.OnUISwitchSelectionChanged);
    //      break;
    //    case 5:
    //      this.uiSwitchComboBox = (System.Windows.Controls.ComboBox) target;
    //      this.uiSwitchComboBox.SelectionChanged += new SelectionChangedEventHandler(this.OnUISwitchSelectionChanged);
    //      break;
    //    case 6:
    //      this.controlsDataGrid = (System.Windows.Controls.DataGrid) target;
    //      break;
    //    case 7:
    //      this.textBlockHeaderControlHeader = (TextBlock) target;
    //      break;
    //    case 8:
    //      this.VisibleColumn = (DataGridTemplateColumn) target;
    //      break;
    //    case 9:
    //      this.textBlockHeaderVisibleColumn = (TextBlock) target;
    //      break;
    //    case 13:
    //      this.EnabledColumn = (DataGridTemplateColumn) target;
    //      break;
    //    case 14:
    //      this.textBlockHeadeEnabledColumn = (TextBlock) target;
    //      break;
    //    case 18:
    //      this.MandatoryColumn = (DataGridTemplateColumn) target;
    //      break;
    //    case 19:
    //      this.textBlockHeadeMandatoryColumn = (TextBlock) target;
    //      break;
    //    case 23:
    //      this.ReadOnlyColumn = (DataGridTemplateColumn) target;
    //      break;
    //    case 24:
    //      this.textBlockHeadReadOnlyColumn = (TextBlock) target;
    //      break;
    //    case 28:
    //      this.toolbarPanel = (StackPanel) target;
    //      break;
    //    case 29:
    //      this.okButton = (System.Windows.Controls.Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 30:
    //      this.cancelButton = (System.Windows.Controls.Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    [EditorBrowsable(EditorBrowsableState.Never)]
    [DebuggerNonUserCode]
    [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    void IStyleConnector.Connect(int connectionId, object target)
    {
      switch (connectionId)
      {
        case 10:
          ((System.Windows.Style) target).Setters.Add((SetterBase) new EventSetter()
          {
            Event = FrameworkElement.LoadedEvent,
            Handler = (Delegate) new RoutedEventHandler(this.OnLoadVisibleColumn)
          });
          break;
        case 11:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.VisibleCombobox_SelectionChanged);
          break;
        case 12:
          ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.VisibleBindingButton_Click);
          break;
        case 15:
          ((System.Windows.Style) target).Setters.Add((SetterBase) new EventSetter()
          {
            Event = FrameworkElement.LoadedEvent,
            Handler = (Delegate) new RoutedEventHandler(this.OnLoadEnabledColumn)
          });
          break;
        case 16:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.EnabledCombobox_SelectionChanged);
          break;
        case 17:
          ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.EnabledBindingButton_Click);
          break;
        case 20:
          ((System.Windows.Style) target).Setters.Add((SetterBase) new EventSetter()
          {
            Event = FrameworkElement.LoadedEvent,
            Handler = (Delegate) new RoutedEventHandler(this.OnLoadMandatoryColumn)
          });
          break;
        case 21:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.MandatoryCombobox_SelectionChanged);
          break;
        case 22:
          ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.MandatoryBindingButton_Click);
          break;
        case 25:
          ((System.Windows.Style) target).Setters.Add((SetterBase) new EventSetter()
          {
            Event = FrameworkElement.LoadedEvent,
            Handler = (Delegate) new RoutedEventHandler(this.OnLoadReadOnlyColumn)
          });
          break;
        case 26:
          ((Selector) target).SelectionChanged += new SelectionChangedEventHandler(this.ReadOnlyCombobox_SelectionChanged);
          break;
        case 27:
          ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.ReadOnlyBindingButton_Click);
          break;
      }
    }
  }
}
