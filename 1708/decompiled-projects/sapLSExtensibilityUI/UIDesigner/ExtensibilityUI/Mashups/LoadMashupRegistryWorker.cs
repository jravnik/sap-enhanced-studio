﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups.LoadMashupRegistryWorker
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base;
using SAP.BYD.LS.UI.Core.Connector.Services.Mashup;
using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.Connector;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups
{
  internal class LoadMashupRegistryWorker
  {
    public MashupRegistryType LoadMashupRegistry()
    {
      MashupRegistryQueryFunction registryQueryFunction1 = new MashupRegistryQueryFunction();
      registryQueryFunction1.Importing = new MashupRegistryQueryFunction.ImportingType();
      MashupRegistryQueryFunction registryQueryFunction2 = registryQueryFunction1;
      MashupProxy.Instance.CallFunctionModule((IRemoteFunction) registryQueryFunction2);
      if (registryQueryFunction2.Exporting == null)
        return (MashupRegistryType) null;
      MashupRegistryType mashupRegistryType = new MashupRegistryType();
      List<string> stringList = new List<string>();
      if (registryQueryFunction2.Exporting.EV_RETURN_CODE != 0)
      {
        int length = registryQueryFunction2.Exporting.ET_RETURN_MSG.Length;
        if (length == 0)
        {
          stringList.Add(string.Format("Return Code is {0}", (object) registryQueryFunction2.Exporting.EV_RETURN_CODE));
        }
        else
        {
          int index1 = 0;
          for (int index2 = length; index1 < index2; ++index1)
            stringList.Add(registryQueryFunction2.Exporting.ET_RETURN_MSG[index1].MESSAGE);
        }
        return (MashupRegistryType) null;
      }
      if (registryQueryFunction2.Exporting.ET_MASHUP_CATEGORY != null)
        mashupRegistryType.MashupCategories = new MashupCategoriesType()
        {
          MashupCategory = ((IEnumerable<MashupRegistryQueryFunction.FLX_MASH_S_MASHUP_CATEGORY>) registryQueryFunction2.Exporting.ET_MASHUP_CATEGORY).Select<MashupRegistryQueryFunction.FLX_MASH_S_MASHUP_CATEGORY, MashupCategoryType>((Func<MashupRegistryQueryFunction.FLX_MASH_S_MASHUP_CATEGORY, MashupCategoryType>) (s =>
          {
            return new MashupCategoryType()
            {
              name = s.MASHUP_CATEGORY_ID,
              inPortTypeReference = s.INPORT_TYPE_REF,
              outPortTypeReference = s.OUTPORT_TYPE_REF,
              inPortTypePackage = s.INPORT_TYPE_PKG,
              outPortTypePackage = s.OUTPORT_TYPE_PKG,
              semanticCategory = s.SEMANTIC_CATEGORY,
              DisplayName = new DependentPropertyType()
              {
                fallbackValueType = DataFieldTypes.@string,
                fallbackValue = s.DISPLAY_NAME
              },
              Description = new DependentPropertyType()
              {
                fallbackValueType = DataFieldTypes.@string,
                fallbackValue = s.DESCRIPTION
              }
            };
          })).ToList<MashupCategoryType>()
        };
      return mashupRegistryType;
    }
  }
}
