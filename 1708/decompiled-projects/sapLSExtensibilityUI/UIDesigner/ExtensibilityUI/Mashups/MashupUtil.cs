﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups.MashupUtil
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base.Exceptions;
using SAP.BYD.LS.UI.Core.Connector.Services.Mashup;
using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Mashup.Common;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups
{
  public static class MashupUtil
  {
    public static XNamespace UXV_NAMESPACE = (XNamespace) "http://www.sap.com/a1s/cd/oberon/uxview-1.0";

    public static string CreateElementID()
    {
      return Guid.NewGuid().ToString("N");
    }

    public static bool TryParseEnum<T>(this string value, bool ignoreCase, out T result)
    {
      result = default (T);
      if (string.IsNullOrEmpty(value))
        return false;
      Type enumType = typeof (T);
      if (!enumType.IsEnum || !Enum.IsDefined(enumType, (object) value))
        return false;
      result = (T) Enum.Parse(enumType, value, ignoreCase);
      return true;
    }

    public static UXInPortType FindTargetInPortByName(this UXComponent component, string inPortName)
    {
      if (!string.IsNullOrEmpty(inPortName) && component.Interface != null && (component.Interface.InPorts != null && component.Interface.InPorts.InPort != null))
        return component.Interface.InPorts.InPort.ToDictionary<UXInPortType, string>((Func<UXInPortType, string>) (i => i.name))[inPortName];
      return (UXInPortType) null;
    }

    public static UXOutPortType FindSourceOutPort(this UXComponent component, string mashupCategory)
    {
      List<UXOutPortType> sourceOutPorts = component.GetSourceOutPorts(mashupCategory);
      if (sourceOutPorts != null && sourceOutPorts.Count > 0)
        return sourceOutPorts.FirstOrDefault<UXOutPortType>();
      return (UXOutPortType) null;
    }

    public static UXOutPortType FindSourceOutPortByName(this UXComponent component, string outPortName)
    {
      if (!string.IsNullOrEmpty(outPortName) && component.Interface != null && (component.Interface.OutPorts != null && component.Interface.OutPorts.OutPort != null))
        return component.Interface.OutPorts.OutPort.ToDictionary<UXOutPortType, string>((Func<UXOutPortType, string>) (o => o.name))[outPortName];
      return (UXOutPortType) null;
    }

    public static List<UXInPortType> GetMatchedInPorts(this UXComponent component, string portTypePackage, string portTypeReference)
    {
      InterfaceType interfaceType = component.Interface;
      if (interfaceType != null && interfaceType.InPorts != null && interfaceType.InPorts.InPort != null)
        return component.Interface.InPorts.InPort.Where<UXInPortType>((Func<UXInPortType, bool>) (inPort =>
        {
          if (inPort.portTypePackage != null && inPort.portTypePackage.Equals(portTypePackage) && (inPort.portTypeReference != null && inPort.portTypeReference.Equals(portTypeReference)))
            return inPort.StableAnchor != null;
          return false;
        })).ToList<UXInPortType>();
      return new List<UXInPortType>();
    }

    public static List<UXInPortType> GetTargetInPorts(this UXComponent component, string mashupCategory)
    {
      string outPortTypePackage;
      string outPortTypeReference;
      MashupRegistry.GetOutPortInfoByMashupCategory(mashupCategory, out outPortTypePackage, out outPortTypeReference);
      if (outPortTypePackage == null)
        throw new ArgumentException("outPortTypePackage cannot be null.");
      if (outPortTypeReference == null)
        throw new ArgumentException("outPortTypeReference cannot be null.");
      return component.GetMatchedInPorts(outPortTypePackage, outPortTypeReference);
    }

    public static UXInPortType FindTargetInPort(this UXComponent component, string mashupCategory)
    {
      List<UXInPortType> targetInPorts = component.GetTargetInPorts(mashupCategory);
      if (targetInPorts != null && targetInPorts.Count > 0)
        return targetInPorts.FirstOrDefault<UXInPortType>();
      return (UXInPortType) null;
    }

    public static List<UXOutPortType> GetMatchedOutPorts(this UXComponent component, string portTypePackage, string portTypeReference)
    {
      if (!string.IsNullOrEmpty(portTypeReference) || !string.IsNullOrEmpty(portTypePackage))
      {
        InterfaceType interfaceType = component.Interface;
        if (interfaceType != null && interfaceType.OutPorts != null && interfaceType.OutPorts.OutPort != null)
          return component.Interface.OutPorts.OutPort.Where<UXOutPortType>((Func<UXOutPortType, bool>) (outPort =>
          {
            if (outPort.portTypePackage != null && outPort.portTypePackage.Equals(portTypePackage) && (outPort.portTypeReference != null && outPort.portTypeReference.Equals(portTypeReference)))
              return outPort.StableAnchor != null;
            return false;
          })).ToList<UXOutPortType>();
      }
      return new List<UXOutPortType>();
    }

    public static List<UXOutPortType> GetSourceOutPorts(this UXComponent component, string mashupCategory)
    {
      string inPortTypePackage;
      string inPortTypeReference;
      MashupRegistry.GetInPortInfoByMashupCategory(mashupCategory, out inPortTypePackage, out inPortTypeReference);
      if (inPortTypePackage == null)
        throw new ArgumentException("inPortTypePackage cannot be null.");
      if (inPortTypeReference == null)
        throw new ArgumentException("inPortTypeReference cannot be null.");
      return component.GetMatchedOutPorts(inPortTypePackage, inPortTypeReference);
    }

    public static bool IsFunctionCallSuccess(int returnCode, OSLS_XREP_RETURN_MSG[] messages, out List<string> messageList)
    {
      bool flag = true;
      messageList = new List<string>();
      if (returnCode != 0)
      {
        flag = false;
        if (messages != null)
        {
          int length = messages.Length;
          if (length == 0)
          {
            if (returnCode == 403)
              messageList.Add("403OperationFailedMsg");
            else
              messageList.Add(string.Format("Return Code is {0}", (object) returnCode));
          }
          else
          {
            int index1 = 0;
            for (int index2 = length; index1 < index2; ++index1)
              messageList.Add(messages[index1].MESSAGE);
          }
        }
      }
      return flag;
    }

    public static string GetElementText(this DependentPropertyType textType, ITextPoolController textPoolController)
    {
      if (textType == null)
        throw new ArgumentNullException("textType", "textType cannot be null.");
      if (textPoolController == null)
        throw new ArgumentNullException("textPoolController", "textPoolController cannot be null.");
      string str = string.Empty;
      string textPoolId = textType.textPoolId;
      if (!string.IsNullOrEmpty(textPoolId))
        str = textPoolController.GetTextPoolEntryValue(textPoolId);
      if (!string.IsNullOrEmpty(str))
        return str;
      return textType.fallbackValue;
    }

    public static Dictionary<string, XElement> GetAllViewStableAnchors(UXComponent component)
    {
        XElement xElement = XElement.Parse(HelperFunctions.convertUXComponentToString(component));
        var enumerable = from el in xElement.Descendants()
                            where el.Name.Equals(MashupConstants.ViewNameSpace + "StableAnchor") && el.Attributes("type").Any<XAttribute>() && el.Attributes("xrepPath").Any<XAttribute>() && (el.Attribute("type").Value.Equals("PaneContainerAnchor") || el.Attribute("type").Value.Equals("SectionGroupAnchor") || el.Attribute("type").Value.Equals("ListAnchor") || el.Attribute("type").Value.Equals("ViewSwitchNavigationAnchor"))
                            select new
                            {
                                key = el.Attribute("xrepPath").Value,
                                value = el
                            };
        if (enumerable != null && enumerable.Count() > 0)
        {
            return enumerable.ToDictionary(item => item.key, item => item.value);
        }
        return null;
    }

    internal static void ResetAppearance(this MashupViewModel mashupViewModel, DynamicParameter currentParam, bool bound, UXComponent uxComponent, bool isComponentEmbeded, List<FlexBaseAnchor> referencedAnchors)
    {
      bool outputBound1 = mashupViewModel.OutputBound;
      currentParam.Bound = bound;
      bool outputBound2 = mashupViewModel.OutputBound;
      if (!currentParam.IsOutput || outputBound1 == outputBound2)
        return;
      if (outputBound2)
      {
        mashupViewModel.MashupOutPlug = mashupViewModel.TargetInPlug = mashupViewModel.PipeIDWithoutSlashes;
        mashupViewModel.ViewStyle = MashupViewStyleType.newDialog.ToString();
        if (mashupViewModel.MashupCategory == "Web_Widget")
        {
          mashupViewModel.MashupInPlug = "ExtensionFieldsInPort";
          mashupViewModel.SourceOutPlug = "ExtensionFieldsOutPort";
        }
      }
      else
      {
        mashupViewModel.MashupOutPlug = mashupViewModel.TargetInPlug = string.Empty;
        mashupViewModel.ViewStyle = MashupViewStyleType.inScreen.ToString();
        if (mashupViewModel.MashupCategory == "Web_Widget")
          mashupViewModel.MashupInPlug = mashupViewModel.SourceOutPlug = string.Empty;
      }
      mashupViewModel.SetAppearance(uxComponent, isComponentEmbeded, referencedAnchors);
    }

    public static void SetAppearance(this MashupViewModel mashupViewModel, UXComponent uxComponent, bool isComponentEmbedded, List<FlexBaseAnchor> referencedAnchors)
    {
      if (referencedAnchors == null || referencedAnchors.Count<FlexBaseAnchor>() == 0)
        return;
      Dictionary<string, ReferencedAnchorAndAppearance> appearances;
      mashupViewModel.LoadAppearances(uxComponent, referencedAnchors, out appearances, isComponentEmbedded);
      mashupViewModel.AppearanceOptions.Clear();
      foreach (KeyValuePair<string, ReferencedAnchorAndAppearance> keyValuePair in appearances)
        mashupViewModel.AppearanceOptions.Add(new MashupValueHelpListModel()
        {
          Code = keyValuePair.Key,
          Text = keyValuePair.Value.Appearance
        });
      string key = appearances.FirstOrDefault<KeyValuePair<string, ReferencedAnchorAndAppearance>>().Key;
      mashupViewModel.AppearanceKey = key;
    }

    public static void SetDynamicBindingOptionandVisibleAppearance(this MashupViewModel mashupViewModel, UXComponent component, Dictionary<string, ExistingMashupsInfo> existMashupInfos, string xrepPath)
    {
      mashupViewModel.GetDynamicBindingOptions(component, xrepPath);
      if (existMashupInfos != null && existMashupInfos.Count > 0)
      {
        string pipeId = mashupViewModel.PipeID;
        if (existMashupInfos.ContainsKey(pipeId))
        {
          ExistingMashupsInfo existMashupInfo = existMashupInfos[pipeId];
          if (mashupViewModel.DynamicParameters != null)
          {
            if (mashupViewModel.DynamicParameters != null)
            {
              foreach (DynamicParameter dynamicParameter in (Collection<DynamicParameter>) mashupViewModel.DynamicParameters)
              {
                if (existMashupInfo.DynamicBindings.ContainsKey(dynamicParameter.Name))
                {
                  foreach (ExtBindingOption bindingOption in (Collection<ExtBindingOption>) dynamicParameter.BindingOptions)
                  {
                    if (bindingOption.ParameterName == existMashupInfo.DynamicBindings[dynamicParameter.Name].ParameterName)
                    {
                      dynamicParameter.BindingText = bindingOption.Text;
                      dynamicParameter.Binding = bindingOption.ParameterBind;
                    }
                  }
                }
              }
            }
            if (mashupViewModel.DynamicOutputs != null)
            {
              foreach (DynamicParameter dynamicOutput in (Collection<DynamicParameter>) mashupViewModel.DynamicOutputs)
              {
                if (existMashupInfo.DynamicBindings.ContainsKey(dynamicOutput.Name))
                {
                  if (dynamicOutput.IsOutput)
                  {
                    mashupViewModel.ViewStyle = MashupViewStyleType.newDialog.ToString();
                    mashupViewModel.MashupOutPlug = mashupViewModel.TargetInPlug = mashupViewModel.PipeIDWithoutSlashes;
                    dynamicOutput.Bound = true;
                  }
                  foreach (ExtBindingOption bindingOption in (Collection<ExtBindingOption>) dynamicOutput.BindingOptions)
                  {
                    if (bindingOption.ParameterName == existMashupInfo.DynamicBindings[dynamicOutput.Name].ParameterName)
                    {
                      dynamicOutput.BindingText = bindingOption.Text;
                      dynamicOutput.Binding = bindingOption.ParameterBind;
                    }
                  }
                }
              }
            }
          }
          mashupViewModel.AppearanceKey = existMashupInfo.AppearanceKey;
        }
      }
      if (mashupViewModel.IsVisible)
        mashupViewModel.IsPropertyPanelEnabled = false;
      else
        mashupViewModel.IsPropertyPanelEnabled = true;
    }

    public static void LoadAppearances(this MashupViewModel mashupViewModel, UXComponent uiComponent, List<FlexBaseAnchor> referencedAnchors, out Dictionary<string, ReferencedAnchorAndAppearance> appearances, bool isComponentEmbedded)
    {
      appearances = new Dictionary<string, ReferencedAnchorAndAppearance>();
      Dictionary<string, XElement> viewStableAnchors = MashupUtil.GetAllViewStableAnchors(uiComponent);
      Func<XElement, string> func = (Func<XElement, string>) (sectionGroupElement =>
      {
        if (sectionGroupElement == null)
          return (string) null;
        XElement xelement = sectionGroupElement.Element(MashupConstants.ViewNameSpace + "SectionGroupName");
        if (xelement == null || !xelement.Attributes((XName) "textPoolId").Any<XAttribute>())
          return "Untitled";
        if (!xelement.Attributes((XName) "id").Any<XAttribute>())
        {
          string empty = string.Empty;
        }
        else
        {
          string str1 = xelement.Attribute((XName) "id").Value;
        }
        string str2 = xelement.Attributes((XName) "fallbackValue").Any<XAttribute>() ? xelement.Attribute((XName) "fallbackValue").Value : string.Empty;
        return MashupUtil.getText(new DependentPropertyType()
        {
          id = xelement.Attribute((XName) "id").Value,
          textPoolId = xelement.Attribute((XName) "textPoolId").Value,
          fallbackValue = str2
        }, uiComponent);
      });
      if (viewStableAnchors != null)
      {
        foreach (FlexBaseAnchor referencedAnchor in referencedAnchors)
        {
          if (viewStableAnchors.ContainsKey(referencedAnchor.XRepPath))
          {
            XElement xelement1 = viewStableAnchors[referencedAnchor.XRepPath];
            FlexAnchorEnumType valueOrDefault = referencedAnchor.Type.GetValueOrDefault();
            switch (valueOrDefault)
            {
            case FlexAnchorEnumType.PaneContainerAnchor:
                if ((MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupViewModel.ViewStyle) == MashupViewStyleType.inScreen)
                {
                XElement parent1 = xelement1.Parent;
                string str = "Untitled";
                if (parent1.Elements(MashupConstants.ViewNameSpace + "SectionGroup").Any<XElement>())
                {
                    XElement xelement2 = parent1.Elements(MashupConstants.ViewNameSpace + "SectionGroup").First<XElement>();
                    str = func(xelement2);
                }
                if (parent1.Attributes((XName) "id").Any<XAttribute>() && parent1.Name.LocalName.Equals("PaneContainer"))
                {
                    XElement parent2 = parent1.Parent;
                    if (parent2.Attributes((XName) "id").Any<XAttribute>() && parent2.Attributes((XName) "rows").Any<XAttribute>())
                    {
                    appearances["P-" + parent1.Attribute((XName) "id").Value + ";ViewTypeID-" + parent2.Attribute((XName) "id").Value] = new ReferencedAnchorAndAppearance()
                    {
                        Appearance = "Embedded Near " + str + " Section",
                        XrepPath = referencedAnchor.XRepPath
                    };
                    continue;
                    }
                    continue;
                }
                continue;
                }
                continue;
            case FlexAnchorEnumType.SectionGroupAnchor:
                XElement parent3 = xelement1.Parent;
                string str1 = func(parent3);
                if ((MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupViewModel.ViewStyle) == MashupViewStyleType.newDialog || (MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupViewModel.ViewStyle) == MashupViewStyleType.newWindow)
                {
                appearances["S-" + parent3.Attribute((XName) "id").Value] = new ReferencedAnchorAndAppearance()
                {
                    Appearance = "Link in " + str1 + " Section",
                    XrepPath = referencedAnchor.XRepPath
                };
                continue;
                }
                if ((MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupViewModel.ViewStyle) == MashupViewStyleType.inScreen)
                {
                XElement parent1 = parent3.Parent.Parent;
                if (parent1.Attributes((XName) "id").Any<XAttribute>())
                {
                    if (parent1.Name.LocalName.Equals("PaneContainer"))
                    {
                    XElement parent2 = parent1.Parent;
                    if (parent2.Attributes((XName) "id").Any<XAttribute>() && parent2.Attributes((XName) "rows").Any<XAttribute>())
                    {
                        appearances["P-" + parent1.Attribute((XName) "id").Value + ";ViewTypeID-" + parent2.Attribute((XName) "id").Value] = new ReferencedAnchorAndAppearance()
                        {
                        Appearance = "Embedded Near " + str1 + " Section",
                        XrepPath = referencedAnchor.XRepPath
                        };
                        continue;
                    }
                    continue;
                    }
                    if (parent1.Name.LocalName.Equals("Member"))
                    {
                    XElement parent2 = parent1.Parent.Parent.Parent;
                    if (parent2.Attributes((XName) "id").Any<XAttribute>() && parent2.Attributes((XName) "rows").Any<XAttribute>())
                    {
                        appearances["P-" + parent1.Attribute((XName) "id").Value + ";ViewTypeID-" + parent2.Attribute((XName) "id").Value] = new ReferencedAnchorAndAppearance()
                        {
                        Appearance = "Embedded Near " + str1 + " Section",
                        XrepPath = referencedAnchor.XRepPath
                        };
                        continue;
                    }
                    continue;
                    }
                    continue;
                }
                continue;
                }
                continue;
            case FlexAnchorEnumType.ListAnchor:
                if ((MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupViewModel.ViewStyle) == MashupViewStyleType.newDialog || (MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupViewModel.ViewStyle) == MashupViewStyleType.newWindow)
                {
                XElement parent1 = xelement1.Parent.Parent;
                if (parent1.Elements(MashupConstants.ViewNameSpace + "Toolbar").Any<XElement>())
                {
                    XElement xelement2 = parent1.Elements(MashupConstants.ViewNameSpace + "Toolbar").First<XElement>();
                    if (xelement2 != null && xelement2.Attributes((XName) "id").Any<XAttribute>())
                    {
                    string str2 = xelement2.Attribute((XName) "id").Value;
                    XElement parent2 = parent1.Parent;
                    string str3 = "Untitled";
                    if (parent2.Elements(MashupConstants.ViewNameSpace + "Header").Any<XElement>())
                    {
                        XElement xelement3 = parent2.Elements(MashupConstants.ViewNameSpace + "Header").First<XElement>();
                        if (xelement3 != null && xelement3.Attributes((XName) "textPoolId").Any<XAttribute>())
                        {
                        bool flag = xelement3.Attributes((XName) "fallbackValue").Any<XAttribute>();
                        DependentPropertyType textProperty = new DependentPropertyType();
                        textProperty.id = xelement3.Attribute((XName) "id").Value;
                        textProperty.fallbackValue = flag ? xelement3.Attribute((XName) "fallbackValue").Value : string.Empty;
                        textProperty.textPoolId = xelement3.Attribute((XName) "textPoolId").Value;
                        str3 = MashupUtil.getText(textProperty, uiComponent);
                        }
                    }
                    appearances["T-" + str2] = new ReferencedAnchorAndAppearance()
                    {
                        Appearance = "Web Services Menu in " + str3 + " Section",
                        XrepPath = referencedAnchor.XRepPath
                    };
                    continue;
                    }
                    continue;
                }
                continue;
                }
                continue;
            case FlexAnchorEnumType.ViewSwitchNavigationAnchor:
                if ((MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupViewModel.ViewStyle) == MashupViewStyleType.inScreen)
                {
                XElement parent1 = xelement1.Parent;
                if (parent1 != null)
                {
                    string str2 = parent1.Attribute((XName) "id").Value;
                    appearances[Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ViewSwitch) + "-" + str2] = new ReferencedAnchorAndAppearance()
                    {
                    Appearance = "New Facet",
                    XrepPath = referencedAnchor.XRepPath
                    };
                    continue;
                }
                continue;
                }
                continue;
            default:
                continue;
            }
          }
        }
      }
      if (isComponentEmbedded || (!referencedAnchors.Where<FlexBaseAnchor>((Func<FlexBaseAnchor, bool>) (anchor => anchor != null)).Any<FlexBaseAnchor>((Func<FlexBaseAnchor, bool>) (anchor => FlexAnchorEnumType.FloorPlanAnchor.Equals((object) anchor.Type))) || (MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupViewModel.ViewStyle) != MashupViewStyleType.newDialog && (MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupViewModel.ViewStyle) != MashupViewStyleType.newWindow))
        return;
      FlexBaseAnchor flexBaseAnchor = referencedAnchors.Where<FlexBaseAnchor>((Func<FlexBaseAnchor, bool>) (anchor => anchor != null)).First<FlexBaseAnchor>();
      appearances[Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.FunctionBar)] = new ReferencedAnchorAndAppearance()
      {
        Appearance = "Web Services Menu",
        XrepPath = flexBaseAnchor.XRepPath
      };
    }

    public static string getText(DependentPropertyType textProperty, UXComponent uiComponent)
    {
      if (textProperty == null)
        return string.Empty;
      if (string.IsNullOrEmpty(textProperty.textPoolId))
        return textProperty.fallbackValue;
      foreach (TextBlockType textBlockType in uiComponent.TextPool)
      {
        if (textBlockType.language == LoginManager.Instance.Language)
        {
          foreach (TextPoolEntryType textPoolEntryType in textBlockType.TextPoolEntry)
          {
            if (textPoolEntryType.textUuid == textProperty.textPoolId)
            {
              if (!string.IsNullOrEmpty(textPoolEntryType.text))
                return textPoolEntryType.text;
              return textProperty.fallbackValue;
            }
          }
        }
      }
      return (string) null;
    }

    private static void GetDynamicBindingOptions(this MashupViewModel mashupViewModel, UXComponent component, string xrepPath)
    {
      if (component == null || component.Interface == null || component.Interface.OutPorts == null)
        return;
      List<UXOutPortType> outPort = component.Interface.OutPorts.OutPort;
      if (outPort == null)
        return;
      foreach (UXOutPortType extensionFieldOutPort in outPort.Reverse<UXOutPortType>())
      {
        if (extensionFieldOutPort != null && extensionFieldOutPort.name == "ExtensionFieldsOutPort")
        {
          if (extensionFieldOutPort.Parameter == null || extensionFieldOutPort.Parameter.Count <= 0)
            break;
          XDocument dataModelDoc = (XDocument) null;
          XDocument viewModelDoc = (XDocument) null;
          try
          {
            if (dataModelDoc == null)
            {
              StringBuilder output = new StringBuilder();
              using (XmlWriter xmlWriter = XmlWriter.Create(output))
                new UXDataModelTypeSerializer().Serialize(xmlWriter, (object) component.Implementation.DataModel);
              dataModelDoc = XDocument.Load((TextReader) new StringReader(output.ToString()));
            }
            if (viewModelDoc == null)
            {
              StringBuilder output = new StringBuilder();
              using (XmlWriter xmlWriter = XmlWriter.Create(output))
                new UXViewTypeSerializer().Serialize(xmlWriter, (object) component.UXView);
              viewModelDoc = XDocument.Load((TextReader) new StringReader(output.ToString()));
            }
          }
          catch (Exception ex)
          {
            throw new InvalidStateException("Invalid state due to XML model passing: " + ex.Message);
          }
          if (dataModelDoc.Root == null || viewModelDoc.Root == null)
            break;
          TextPool textPool = new TextPool();
          textPool.Initialize(component);
          ObservableCollection<ExtBindingOption> dynamicBindingOption = mashupViewModel.GetDynamicBindingOption(viewModelDoc, dataModelDoc, extensionFieldOutPort, textPool, xrepPath);
          if (mashupViewModel.DynamicParameters != null)
          {
            foreach (DynamicParameter dynamicParameter in (Collection<DynamicParameter>) mashupViewModel.DynamicParameters)
            {
              foreach (ExtBindingOption extBindingOption in (Collection<ExtBindingOption>) dynamicBindingOption)
                dynamicParameter.BindingOptions.Add(extBindingOption);
            }
          }
          if (mashupViewModel.DynamicOutputs == null)
            break;
          using (IEnumerator<DynamicParameter> enumerator = mashupViewModel.DynamicOutputs.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              DynamicParameter current = enumerator.Current;
              foreach (ExtBindingOption extBindingOption in (Collection<ExtBindingOption>) dynamicBindingOption)
                current.BindingOptions.Add(extBindingOption);
            }
            break;
          }
        }
      }
    }

    private static MashupViewModel ConstructMashupViewModel(SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupPipeInfo pipeRef, UXOutPortType outPort, UXInPortType inPort)
    {
      string str = outPort == null || outPort.name == null ? string.Empty : outPort.name;
      return new MashupViewModel()
      {
        MashupInPlug = pipeRef.InPort,
        MashupOutPlug = pipeRef.OutPort,
        SourceOutPlug = str,
        TargetInPlug = inPort == null || inPort.name == null ? string.Empty : inPort.name,
        PipeID = pipeRef.PipeID,
        MashupCategory = pipeRef.MashupCategory,
        ViewStyle = pipeRef.ViewStyle,
        IconUrl = pipeRef.DomainUrl,
        ComponentPath = pipeRef.FilePath,
        MashupCategoryDescription = MashupRegistry.GetMashupCategoryDisplayNameByName(pipeRef.MashupCategory),
        MashupType = pipeRef.MashupType,
        MandatoryParams = pipeRef.MandParams ?? string.Empty,
        DynParams = pipeRef.DynParams ?? string.Empty
      };
    }

    private static string GetText(List<TextBlockType> textPool, string textPoolId)
    {
      foreach (TextBlockType textBlockType in textPool)
      {
        if (textBlockType.language == LoginManager.Instance.Language)
        {
          foreach (TextPoolEntryType textPoolEntryType in textBlockType.TextPoolEntry)
          {
            if (textPoolEntryType.textUuid == textPoolId)
            {
              if (!string.IsNullOrEmpty(textPoolEntryType.text))
                return textPoolEntryType.text;
              return (string) null;
            }
          }
        }
      }
      return (string) null;
    }
  }
}
