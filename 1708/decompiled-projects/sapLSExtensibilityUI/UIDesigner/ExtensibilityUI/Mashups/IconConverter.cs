﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups.IconConverter
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups
{
  public class IconConverter : IValueConverter
  {
    private const string DEFAULT_ICON_URL = "../../Common/Assets/WebService.png";

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
        return (object) "../../Common/Assets/WebService.png";
      return (object) ImageProxy.ConvertIconUrl((string) value, "../../Common/Assets/WebService.png");
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public static void SetDefaultWebServiceIcon(object sender, ExceptionRoutedEventArgs e)
    {
      Image image = sender as Image;
      if (image == null)
        return;
      image.Source = (ImageSource) new BitmapImage(new Uri("../../Common/Assets/WebService.png", UriKind.Relative));
    }
  }
}
