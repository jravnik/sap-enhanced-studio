﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups.ImageProxy
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using System;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups
{
  public class ImageProxy
  {
    private const string GOOGLE_ICON_URL = "http://www.google.com/s2/favicons?domain={0}";

    public static string ConvertIconUrl(string originalUrl, string defaultIconUrl)
    {
      if (!string.IsNullOrEmpty(originalUrl) && !originalUrl.ToLower().EndsWith("png"))
      {
        if (originalUrl.StartsWith("http"))
          return string.Format("http://www.google.com/s2/favicons?domain={0}", (object) new Uri(originalUrl).Host);
        return string.Format("http://www.google.com/s2/favicons?domain={0}", (object) originalUrl);
      }
      if (!string.IsNullOrEmpty(originalUrl))
        return originalUrl;
      return defaultIconUrl;
    }
  }
}
