﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.ChangeTransactionManager
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Connector.Services.XRepository.Designtime;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI
{
  public class ChangeTransactionManager
  {
    private static readonly ChangeTransactionManager m_Instance = new ChangeTransactionManager();
    private AnchorInformations m_AnchorInformations;

    public static ChangeTransactionManager Instance
    {
      get
      {
        return ChangeTransactionManager.m_Instance;
      }
    }

    public event AfterSaveChangeTransactionEventHadler AfterSavedChangeTransaction;

    private ChangeTransactionManager()
    {
      try
      {
        using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(ExtensibilityResource.AnchorControllerInfo)))
        {
          this.m_AnchorInformations = new XmlSerializer(typeof (AnchorInformations)).Deserialize((Stream) memoryStream) as AnchorInformations;
          if (this.m_AnchorInformations == null)
            throw new ApplicationException("Deserialization of the controller xml failed");
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the Extensibility Manager.", ex));
      }
    }

    public List<FlexBaseAnchor> GetSupportedAnchors(List<FlexBaseAnchor> AssocAnchors)
    {
      List<FlexBaseAnchor> flexBaseAnchorList = new List<FlexBaseAnchor>();
      foreach (FlexBaseAnchor assocAnchor in AssocAnchors)
      {
        FlexBaseAnchor anchor = assocAnchor;
        AnchorInfo anchorInfo = this.m_AnchorInformations.SupportedAnchors.Find((Predicate<AnchorInfo>) (info =>
        {
          FlexAnchorEnumType anchorType = info.AnchorType;
          FlexAnchorEnumType? type = anchor.Type;
          if (anchorType == type.GetValueOrDefault())
            return type.HasValue;
          return false;
        }));
        if (anchorInfo != null && anchorInfo.AllowedChangeTransactions.Count != 0)
          flexBaseAnchorList.Add(anchor);
      }
      return flexBaseAnchorList;
    }

    public List<ChangeTransactionInfo> GetAllowedChangeTransactions(FlexAnchorEnumType anchorType, FlexBaseAnchor selectedAnchor)
    {
      List<ChangeTransactionInfo> changeTransactionInfoList = new List<ChangeTransactionInfo>();
      foreach (AnchorInfo supportedAnchor in this.m_AnchorInformations.SupportedAnchors)
      {
        if (supportedAnchor.AnchorType == anchorType)
        {
          changeTransactionInfoList = supportedAnchor.AllowedChangeTransactions;
          bool flag = false;
          if (anchorType != FlexAnchorEnumType.SectionGroupAnchor)
          {
            if (anchorType != FlexAnchorEnumType.ListAnchor)
              break;
          }
          FlexStableAnchor flexStableAnchor = selectedAnchor as FlexStableAnchor;
          if (flexStableAnchor != null)
          {
            if (!string.IsNullOrEmpty(flexStableAnchor.DataFieldPath) && !string.IsNullOrEmpty(flexStableAnchor.ContainerId))
            {
              if (!string.IsNullOrEmpty(flexStableAnchor.CoreBOName) && !string.IsNullOrEmpty(flexStableAnchor.CoreBONameSpace) && !string.IsNullOrEmpty(flexStableAnchor.CoreBONodeName))
                flag = true;
              if (!string.IsNullOrEmpty(flexStableAnchor.ReferenceBundleKey) && !string.IsNullOrEmpty(flexStableAnchor.ReferenceFieldName))
                flag = true;
            }
            if (!flag)
            {
              changeTransactionInfoList = new List<ChangeTransactionInfo>();
              using (List<ChangeTransactionInfo>.Enumerator enumerator = supportedAnchor.AllowedChangeTransactions.GetEnumerator())
              {
                while (enumerator.MoveNext())
                {
                  ChangeTransactionInfo current = enumerator.Current;
                  if (current.EditorTypeName != "SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddFieldToSectionGroup" && current.EditorTypeName != "SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddExtensionFieldToTable")
                    changeTransactionInfoList.Add(current);
                }
                break;
              }
            }
            else
              break;
          }
          else
            break;
        }
      }
      return changeTransactionInfoList;
    }

    public bool SaveChangeTransaction(IFloorplan componentToSave)
    {
      bool flag = false;
      UXComponent oberonModel = componentToSave.GetOberonModel() as UXComponent;
      string user = LoginManager.Instance.User;
      string language = LoginManager.Instance.Language;
      FlexUserEnumType userType = LoginManager.Instance.DTMode == DTModes.Partner ? FlexUserEnumType.Partner : FlexUserEnumType.SAP;
      FlexibilityHandler instance = FlexibilityHandler.GetInstance(componentToSave.UniqueId, oberonModel, user, userType, language, LoginManager.Instance.TopLevelFolder, (string) null, false);
      SAP.BYD.LS.UI.Flexibility.CoreAPI.Repository.ChangeTransaction[] changeTransactions = instance.GetAllChangeTransactions();
      List<CHANGE_TRANSACTION> changeTransactionList = new List<CHANGE_TRANSACTION>();
      if (changeTransactions != null)
      {
        for (int index = 0; index < changeTransactions.Length; ++index)
          changeTransactionList.Add(new CHANGE_TRANSACTION()
          {
            XREP_PATH = changeTransactions[index].XREP_PATH,
            CONTENT = changeTransactions[index].CONTENT
          });
      }
      if (XRepositoryProxy.Instance.SaveChangeTransactions(changeTransactionList.ToArray(), componentToSave.UniqueId))
      {
        componentToSave.HasDirtyChangeTransactions = false;
        instance.ChangeTransactionsSaved();
        this.RaiseSavedChangeTransactionEvent(componentToSave.UniqueId);
      }
      return flag;
    }

    public List<ModeledChangeTransactionInfo> GetUnsavedChangeTransactions(string componentPath, UXComponent uiComponent, string anchorUniqueId)
    {
      List<ModeledChangeTransactionInfo> changeTransactionInfoList = new List<ModeledChangeTransactionInfo>();
      string user = LoginManager.Instance.User;
      string language = LoginManager.Instance.Language;
      FlexUserEnumType userType = LoginManager.Instance.DTMode == DTModes.Partner ? FlexUserEnumType.Partner : FlexUserEnumType.SAP;
      SAP.BYD.LS.UI.Flexibility.CoreAPI.Repository.ChangeTransaction[] changeTransactions = FlexibilityHandler.GetInstance(componentPath, uiComponent, user, userType, language, LoginManager.Instance.TopLevelFolder, (string) null, false).GetAllChangeTransactions();
      if (changeTransactions != null)
      {
        for (int index = 0; index < changeTransactions.Length; ++index)
        {
          bool flag = false;
          byte[] numArray = Convert.FromBase64String(changeTransactions[index].CONTENT);
          using (MemoryStream memoryStream = new MemoryStream(numArray))
          {
            object deserializedObject;
            if (XmlHelper.GetInstance().DeserializeObject(typeof (SAP.BYD.LS.UI.Core.Model.ChangeTransaction), (Stream) memoryStream, out deserializedObject))
            {
              SAP.BYD.LS.UI.Core.Model.ChangeTransaction changeTransaction = deserializedObject as SAP.BYD.LS.UI.Core.Model.ChangeTransaction;
              if (changeTransaction.UsedAnchor != null)
              {
                if (changeTransaction.UsedAnchor.xrepPath == anchorUniqueId)
                  flag = true;
              }
            }
          }
          if (flag)
          {
            string str = Encoding.UTF8.GetString(numArray);
            changeTransactionInfoList.Add(new ModeledChangeTransactionInfo()
            {
              UniqueId = changeTransactions[index].XREP_PATH,
              Content = str
            });
          }
        }
      }
      return changeTransactionInfoList;
    }

    public void DeleteUnSavedChangeTransaction(string componentPath, UXComponent uiComponent, string chaneTransactionId)
    {
      FlexUserEnumType userType = LoginManager.Instance.DTMode == DTModes.Partner ? FlexUserEnumType.Partner : FlexUserEnumType.SAP;
      FlexibilityHandler.GetInstance(componentPath, uiComponent, LoginManager.Instance.User, userType, LoginManager.Instance.Language, LoginManager.Instance.TopLevelFolder, (string) null, false).RevertChangeTransaction(chaneTransactionId);
    }

    private void RaiseSavedChangeTransactionEvent(string uiComponentId)
    {
      if (this.AfterSavedChangeTransaction == null)
        return;
      this.AfterSavedChangeTransaction(new SavedChangeTransactionEventArgs()
      {
        UIComponentId = uiComponentId
      });
    }
  }
}
