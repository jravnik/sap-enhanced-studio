﻿using SAP.Copernicus.Core.Util;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyDescription("")]
[assembly: AssemblyCopyright("© 2011 SAP AG. All rights reserved.")]
[assembly: AssemblyTitle("CopernicusTranslation")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SAP AG")]
[assembly: AssemblyProduct("SAP® Business ByDesign™")]
[assembly: AssemblyTrademark("SAP and Business ByDesign are trademark(s) or registered trademark(s) of SAP AG in Germany and in several other countries.")]
[assembly: AssemblyCSN("AP-RC-BDS-TRANSL")]
[assembly: ComVisible(false)]
[assembly: Guid("b63c1117-a02d-48e1-8f2e-d883b884ee83")]
[assembly: AssemblyFileVersion("142.0.3211.0035")]
[assembly: AssemblyVersion("142.0.3211.35")]
