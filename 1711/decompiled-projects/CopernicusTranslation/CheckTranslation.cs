﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Translation.CheckTranslation
// Assembly: CopernicusTranslation, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: CA9B1E10-5FAB-4C52-8A64-B1F675DF4BDA
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusTranslation.dll

using SAP.Copernicus.Core;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Translation.Resources;
using SAP.CopernicusProjectView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Translation
{
  public class CheckTranslation : BaseForm
  {
    private List<string> _Title = new List<string>();
    private List<string[]> _DataArray = new List<string[]>();
    private KeyValuePair<string, string> Masterlang = new KeyValuePair<string, string>("EN", "English");
    private new IContainer components;
    private GroupBox groupBox1;
    private DataGridView dataGridView2;
    private GroupBox ComponentSummaryGrp;
    private DataGridView dataGridView1;
    private Button button1;
    private Label TotalEnglishCount;
    private Label label1;
    private Label label2;
    private Label label3;
    private RichTextBox richTextBox2;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private Button ExportUIText;
    private RichTextBox richTextBox3;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    private DataGridViewImageColumn Status;
    private DataGridViewTextBoxColumn Language;
    private DataGridViewTextBoxColumn NotTranslatedCount;
    private DataGridViewTextBoxColumn Count;
    private Button button3;
    private ZLANG_STRUC[] LanguageDesc;
    private bool isSolution;
    private List<string> XERPPathlist;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      DataGridViewCellStyle gridViewCellStyle1 = new DataGridViewCellStyle();
      DataGridViewCellStyle gridViewCellStyle2 = new DataGridViewCellStyle();
      this.groupBox1 = new GroupBox();
      this.label2 = new Label();
      this.label3 = new Label();
      this.richTextBox2 = new RichTextBox();
      this.TotalEnglishCount = new Label();
      this.label1 = new Label();
      this.dataGridView2 = new DataGridView();
      this.Status = new DataGridViewImageColumn();
      this.Language = new DataGridViewTextBoxColumn();
      this.NotTranslatedCount = new DataGridViewTextBoxColumn();
      this.Count = new DataGridViewTextBoxColumn();
      this.ExportUIText = new Button();
      this.ComponentSummaryGrp = new GroupBox();
      this.richTextBox3 = new RichTextBox();
      this.dataGridView1 = new DataGridView();
      this.button1 = new Button();
      this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn3 = new DataGridViewTextBoxColumn();
      this.button3 = new Button();
      this.groupBox1.SuspendLayout();
      ((ISupportInitialize) this.dataGridView2).BeginInit();
      this.ComponentSummaryGrp.SuspendLayout();
      ((ISupportInitialize) this.dataGridView1).BeginInit();
      this.SuspendLayout();
      this.buttonCancel.Location = new Point(474, 799);
      this.buttonCancel.Visible = false;
      this.buttonOk.Location = new Point(393, 799);
      this.buttonOk.Visible = false;
      this.groupBox1.Controls.Add((Control) this.label2);
      this.groupBox1.Controls.Add((Control) this.label3);
      this.groupBox1.Controls.Add((Control) this.richTextBox2);
      this.groupBox1.Controls.Add((Control) this.TotalEnglishCount);
      this.groupBox1.Controls.Add((Control) this.label1);
      this.groupBox1.Controls.Add((Control) this.dataGridView2);
      this.groupBox1.Location = new Point(12, 75);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(618, 349);
      this.groupBox1.TabIndex = 1;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Translation Status";
      this.label2.AutoSize = true;
      this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.label2.Location = new Point(136, 41);
      this.label2.Name = "label2";
      this.label2.Size = new Size(41, 13);
      this.label2.TabIndex = 8;
      this.label2.Text = "English";
      this.label3.AutoSize = true;
      this.label3.Location = new Point(10, 41);
      this.label3.Name = "label3";
      this.label3.Size = new Size(95, 13);
      this.label3.TabIndex = 7;
      this.label3.Text = "Source Language:";
      this.richTextBox2.BackColor = SystemColors.Control;
      this.richTextBox2.BorderStyle = BorderStyle.None;
      this.richTextBox2.Location = new Point(9, 18);
      this.richTextBox2.Name = "richTextBox2";
      this.richTextBox2.Size = new Size(520, 32);
      this.richTextBox2.TabIndex = 6;
      this.richTextBox2.Text = "To export strings that still need to be translated into a target language, select a row and click Export to XLIFF.";
      this.TotalEnglishCount.AutoSize = true;
      this.TotalEnglishCount.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.TotalEnglishCount.Location = new Point(135, 63);
      this.TotalEnglishCount.Name = "TotalEnglishCount";
      this.TotalEnglishCount.Size = new Size(53, 13);
      this.TotalEnglishCount.TabIndex = 3;
      this.TotalEnglishCount.Text = "<Value>";
      this.label1.AutoSize = true;
      this.label1.Location = new Point(10, 63);
      this.label1.Name = "label1";
      this.label1.Size = new Size(121, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Total Number of Strings:";
      this.dataGridView2.AllowUserToAddRows = false;
      this.dataGridView2.AllowUserToDeleteRows = false;
      this.dataGridView2.AllowUserToOrderColumns = true;
      gridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleLeft;
      gridViewCellStyle1.BackColor = SystemColors.Control;
      gridViewCellStyle1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      gridViewCellStyle1.ForeColor = SystemColors.WindowText;
      gridViewCellStyle1.WrapMode = DataGridViewTriState.True;
      this.dataGridView2.ColumnHeadersDefaultCellStyle = gridViewCellStyle1;
      this.dataGridView2.Columns.AddRange((DataGridViewColumn) this.Status, (DataGridViewColumn) this.Language, (DataGridViewColumn) this.NotTranslatedCount, (DataGridViewColumn) this.Count);
      this.dataGridView2.EditMode = DataGridViewEditMode.EditProgrammatically;
      this.dataGridView2.EnableHeadersVisualStyles = false;
      this.dataGridView2.Location = new Point(13, 113);
      this.dataGridView2.MaximumSize = new Size(600, 300);
      this.dataGridView2.MultiSelect = false;
      this.dataGridView2.Name = "dataGridView2";
      this.dataGridView2.RowHeadersVisible = false;
      this.dataGridView2.ScrollBars = ScrollBars.Vertical;
      this.dataGridView2.Size = new Size(599, 230);
      this.dataGridView2.TabIndex = 1;
      this.Status.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.Status.HeaderText = "Completed";
      this.Status.Name = "Status";
      this.Status.ReadOnly = true;
      this.Language.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.Language.HeaderText = "Target Language";
      this.Language.Name = "Language";
      this.Language.ReadOnly = true;
      this.NotTranslatedCount.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.NotTranslatedCount.HeaderText = "Not Translated";
      this.NotTranslatedCount.Name = "NotTranslatedCount";
      this.NotTranslatedCount.ReadOnly = true;
      this.Count.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.Count.HeaderText = "Translated";
      this.Count.Name = "Count";
      this.Count.ReadOnly = true;
      this.ExportUIText.Location = new Point(23, 158);
      this.ExportUIText.Name = "ExportUIText";
      this.ExportUIText.Size = new Size(92, 23);
      this.ExportUIText.TabIndex = 9;
      this.ExportUIText.Text = "Export to XLIFF";
      this.ExportUIText.UseVisualStyleBackColor = true;
      this.ExportUIText.Click += new EventHandler(this.ExportUIText_Click);
      this.ComponentSummaryGrp.Controls.Add((Control) this.richTextBox3);
      this.ComponentSummaryGrp.Controls.Add((Control) this.dataGridView1);
      this.ComponentSummaryGrp.Location = new Point(12, 465);
      this.ComponentSummaryGrp.Name = "ComponentSummaryGrp";
      this.ComponentSummaryGrp.Size = new Size(618, 328);
      this.ComponentSummaryGrp.TabIndex = 2;
      this.ComponentSummaryGrp.TabStop = false;
      this.ComponentSummaryGrp.Text = "Item Information";
      this.richTextBox3.BackColor = SystemColors.Control;
      this.richTextBox3.BorderStyle = BorderStyle.None;
      this.richTextBox3.Location = new Point(10, 21);
      this.richTextBox3.Name = "richTextBox3";
      this.richTextBox3.Size = new Size(594, 35);
      this.richTextBox3.TabIndex = 7;
      this.richTextBox3.Text = "This overview displays the number of translatable text strings for each item and the number of translated text strings for each language.";
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.AllowUserToOrderColumns = true;
      gridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
      gridViewCellStyle2.BackColor = SystemColors.Control;
      gridViewCellStyle2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      gridViewCellStyle2.ForeColor = SystemColors.WindowText;
      gridViewCellStyle2.WrapMode = DataGridViewTriState.True;
      this.dataGridView1.ColumnHeadersDefaultCellStyle = gridViewCellStyle2;
      this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.EditMode = DataGridViewEditMode.EditProgrammatically;
      this.dataGridView1.EnableHeadersVisualStyles = false;
      this.dataGridView1.Location = new Point(14, 60);
      this.dataGridView1.MaximumSize = new Size(600, 300);
      this.dataGridView1.MultiSelect = false;
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.RowHeadersVisible = false;
      this.dataGridView1.Size = new Size(590, 262);
      this.dataGridView1.TabIndex = 2;
      this.button1.Location = new Point(555, 799);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 3;
      this.button1.Text = "OK";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.dataGridViewTextBoxColumn1.HeaderText = "Language";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      this.dataGridViewTextBoxColumn1.Width = 78;
      this.dataGridViewTextBoxColumn2.HeaderText = "Count";
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      this.dataGridViewTextBoxColumn2.ReadOnly = true;
      this.dataGridViewTextBoxColumn2.Width = 79;
      this.dataGridViewTextBoxColumn3.HeaderText = "Currently translated Texts";
      this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
      this.dataGridViewTextBoxColumn3.ReadOnly = true;
      this.dataGridViewTextBoxColumn3.Width = 140;
      this.button3.Location = new Point(555, 430);
      this.button3.Name = "button3";
      this.button3.Size = new Size(75, 23);
      this.button3.TabIndex = 10;
      this.button3.Text = "OK";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new EventHandler(this.button3_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(644, 838);
      this.Controls.Add((Control) this.button3);
      this.Controls.Add((Control) this.ExportUIText);
      this.Controls.Add((Control) this.ComponentSummaryGrp);
      this.Controls.Add((Control) this.groupBox1);
      this.Controls.Add((Control) this.button1);
      this.MinimumSize = new Size(660, 600);
      this.Name = nameof (CheckTranslation);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Subtitle = "This overview displays the total number of translatable text strings and the number of translated text strings.";
      this.Text = "Check Translation Status";
      this.Load += new EventHandler(this.CheckTranslation_Load);
      this.Controls.SetChildIndex((Control) this.button1, 0);
      this.Controls.SetChildIndex((Control) this.groupBox1, 0);
      this.Controls.SetChildIndex((Control) this.ComponentSummaryGrp, 0);
      this.Controls.SetChildIndex((Control) this.buttonOk, 0);
      this.Controls.SetChildIndex((Control) this.ExportUIText, 0);
      this.Controls.SetChildIndex((Control) this.buttonCancel, 0);
      this.Controls.SetChildIndex((Control) this.button3, 0);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      ((ISupportInitialize) this.dataGridView2).EndInit();
      this.ComponentSummaryGrp.ResumeLayout(false);
      ((ISupportInitialize) this.dataGridView1).EndInit();
      this.ResumeLayout(false);
    }

    public CheckTranslation(List<string> xRepPathlist, bool isSolutionNode)
      : base(HELP_IDS.BDS_TRANSLATION_CHECK)
    {
      this.InitializeComponent();
      TranslationHandler translationHandler = new TranslationHandler();
      this.LanguageDesc = translationHandler.getLanguage();
      if (this.LanguageDesc == null)
      {
        int num = (int) CopernicusMessageBox.Show(TranslationResource.MsgLangError, "Languages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        this.XERPPathlist = xRepPathlist;
        ZPDI_CHECK_STR[] CheckList = translationHandler.CheckTextHandler(xRepPathlist.ToArray());
        if (CheckList == null)
          return;
        this.setSolutionNode(isSolutionNode);
        this.fillDataGridview(CheckList);
        this.dataGridView1.CellFormatting += new DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
      }
    }

    private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      int rowIndex = e.RowIndex;
      int columnIndex = e.ColumnIndex;
      if (columnIndex == this.dataGridView1.Columns["Component"].Index || columnIndex == this.dataGridView1.Columns[this.Masterlang.Value].Index || !(this.dataGridView1.Rows[rowIndex].Cells[this.Masterlang.Value].Value.ToString() != this.dataGridView1[columnIndex, rowIndex].Value.ToString()))
        return;
      e.CellStyle.SelectionForeColor = Color.Red;
      e.CellStyle.ForeColor = Color.Red;
    }

    private void setSolutionNode(bool Val)
    {
      if (Val)
      {
        this.isSolution = true;
        this.Size = new Size(660, 590);
        this.ComponentSummaryGrp.Visible = true;
        this.button3.Visible = false;
        this.button1.Visible = true;
      }
      else
      {
        this.isSolution = false;
        this.Size = new Size(660, 345);
        this.ComponentSummaryGrp.Visible = false;
        this.button3.Visible = true;
        this.button1.Visible = false;
      }
    }

    private void fillDataGridview(ZPDI_CHECK_STR[] CheckList)
    {
      if (this.LanguageDesc.Length <= 0)
        return;
      this._Title.Add(TranslationResource.CheckTableColComp.ToString());
      List<string> stringList1 = new List<string>();
      foreach (ZPDI_CHECK_STR check in CheckList)
      {
        if (!stringList1.Contains(check.UICOMPONENT))
          stringList1.Add(check.UICOMPONENT);
      }
      this._DataArray.Add(stringList1.ToArray());
      this._Title.Add(this.Masterlang.Value);
      List<string> stringList2 = new List<string>();
      foreach (ZPDI_CHECK_STR check in CheckList)
      {
        if (check.LANGUAGE == this.Masterlang.Value)
          stringList2.Add(check.TEXTCOUNT);
      }
      this._DataArray.Add(stringList2.ToArray());
      foreach (ZLANG_STRUC zlangStruc in this.LanguageDesc)
      {
        if (zlangStruc.LANGUAGE != this.Masterlang.Key)
        {
          this._Title.Add(zlangStruc.DESCRIPTION);
          List<string> stringList3 = new List<string>();
          foreach (ZPDI_CHECK_STR check in CheckList)
          {
            if (check.LANGUAGE == zlangStruc.DESCRIPTION)
              stringList3.Add(check.TEXTCOUNT);
          }
          this._DataArray.Add(stringList3.ToArray());
        }
      }
      this.dataGridView1.DataSource = (object) this.GetResultsTable();
      this.dataGridView1.Columns["Component"].Width = 175;
      this.dataGridView1.Columns["Component"].ReadOnly = true;
      this.dataGridView1.Columns["Component"].HeaderText = "Item Name";
      this.dataGridView1.Columns[this.Masterlang.Value].Width = 90;
      this.dataGridView1.Columns[this.Masterlang.Value].ReadOnly = true;
      this.dataGridView1.Columns[this.Masterlang.Value].HeaderText = "Strings in Source File";
      for (int index = 2; index < this.LanguageDesc.Length + 1; ++index)
        this.dataGridView1.Columns[index].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      List<KeyValuePair<string, int>> keyValuePairList = new List<KeyValuePair<string, int>>();
      foreach (ZLANG_STRUC zlangStruc in this.LanguageDesc)
      {
        if (zlangStruc.LANGUAGE != this.Masterlang.Key)
          keyValuePairList.Add(new KeyValuePair<string, int>(zlangStruc.DESCRIPTION, 0));
      }
      int num1 = 0;
      foreach (DataGridViewRow row in (IEnumerable) this.dataGridView1.Rows)
      {
        if (row.Cells[this.Masterlang.Value].Value != null)
          num1 += int.Parse(row.Cells[this.Masterlang.Value].Value.ToString());
      }
      this.TotalEnglishCount.Text = num1.ToString();
      int num2 = 0;
      foreach (DataGridViewRow row in (IEnumerable) this.dataGridView1.Rows)
      {
        num2 = 0;
        for (int index = 0; index < keyValuePairList.Count; ++index)
        {
          if (row.Cells[keyValuePairList[index].Key].Value != null)
          {
            int num3 = keyValuePairList[index].Value + int.Parse(row.Cells[keyValuePairList[index].Key].Value.ToString());
            keyValuePairList[index] = new KeyValuePair<string, int>(keyValuePairList[index].Key, num3);
          }
        }
        foreach (ZLANG_STRUC zlangStruc in this.LanguageDesc)
        {
          if (zlangStruc.LANGUAGE != this.Masterlang.Key && row.Cells[this.Masterlang.Value].Value.ToString() != row.Cells[zlangStruc.DESCRIPTION].Value.ToString())
            row.Cells[zlangStruc.DESCRIPTION].Style.BackColor = Color.Red;
        }
      }
      int index1 = 0;
      foreach (KeyValuePair<string, int> keyValuePair in keyValuePairList)
      {
        this.dataGridView2.Rows.Add();
        this.dataGridView2.Rows[index1].Cells["Language"].Value = (object) keyValuePair.Key;
        this.dataGridView2.Rows[index1].Cells["Count"].Value = (object) keyValuePair.Value;
        this.dataGridView2.Rows[index1].Cells["NotTranslatedCount"].Value = (object) (num1 - keyValuePair.Value);
        if (keyValuePair.Value.ToString() == this.TotalEnglishCount.Text)
        {
          this.dataGridView2.Rows[index1].Cells["Status"].Value = (object) ActionIcons.OK;
          this.dataGridView2.Rows[index1].Cells["Status"].ToolTipText = "Strings are translated";
        }
        else
        {
          this.dataGridView2.Rows[index1].Cells["Status"].Value = (object) ActionIcons.Error;
          this.dataGridView2.Rows[index1].Cells["Status"].ToolTipText = "Strings are not translated";
        }
        ++index1;
      }
    }

    private DataTable GetResultsTable()
    {
      DataTable dataTable = new DataTable();
      for (int index1 = 0; index1 < this._DataArray.Count; ++index1)
      {
        string columnName = this._Title[index1];
        dataTable.Columns.Add(columnName);
        List<object> objectList = new List<object>();
        foreach (string str in this._DataArray[index1])
          objectList.Add((object) str);
        while (dataTable.Rows.Count < objectList.Count)
          dataTable.Rows.Add();
        for (int index2 = 0; index2 < objectList.Count; ++index2)
          dataTable.Rows[index2][index1] = objectList[index2];
      }
      return dataTable;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void CheckTranslation_Load(object sender, EventArgs e)
    {
      this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;
      this.dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dataGridView1.ColumnHeadersHeight = this.dataGridView1.ColumnHeadersHeight * 2;
      this.dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
      this.dataGridView1.Paint += new PaintEventHandler(this.dataGridView1_Paint);
      if (this.dataGridView2.SelectedRows.Count > 0)
        this.ExportUIText.Enabled = true;
      else
        this.ExportUIText.Enabled = false;
      this.dataGridView2.SelectionChanged += new EventHandler(this.dataGridView2_SelectionChanged);
    }

    private void dataGridView2_SelectionChanged(object sender, EventArgs e)
    {
      if (this.dataGridView2.SelectedRows.Count > 0)
        this.ExportUIText.Enabled = true;
      else
        this.ExportUIText.Enabled = false;
    }

    private void dataGridView1_Paint(object sender, PaintEventArgs e)
    {
      string[] strArray = new string[3]
      {
        " ",
        " ",
        "Translated Strings"
      };
      Rectangle rectangle = new Rectangle();
      if (this.dataGridView1.ColumnCount < 3)
        return;
      Rectangle displayRectangle1 = this.dataGridView1.GetCellDisplayRectangle(2, -1, true);
      displayRectangle1.X += 2;
      displayRectangle1.Y = 2;
      for (int columnIndex = 2; columnIndex < this.LanguageDesc.Length; ++columnIndex)
      {
        Rectangle displayRectangle2 = this.dataGridView1.GetCellDisplayRectangle(columnIndex, -1, true);
        displayRectangle1.Width += displayRectangle2.Width - 4;
      }
      displayRectangle1.Height = displayRectangle1.Height / 2 - 2;
      e.Graphics.FillRectangle((Brush) new SolidBrush(this.dataGridView1.ColumnHeadersDefaultCellStyle.BackColor), displayRectangle1);
      e.Graphics.DrawString(strArray[2], this.dataGridView1.ColumnHeadersDefaultCellStyle.Font, (Brush) new SolidBrush(this.dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor), (RectangleF) displayRectangle1, new StringFormat()
      {
        Alignment = StringAlignment.Center,
        LineAlignment = StringAlignment.Center
      });
    }

    private void ExportUIText_Click(object sender, EventArgs e)
    {
      if (this.XERPPathlist == null)
        return;
      ExportText exportText = new ExportText(this.XERPPathlist);
      if (this.isSolution)
      {
        string selectedProjectName = CopernicusProjectSystemUtil.getSelectedProjectName();
        exportText.FileName = selectedProjectName;
      }
      else
      {
        string[] strArray = CopernicusProjectSystemUtil.getSelectedNodeName().Split('.');
        if (!string.IsNullOrEmpty(strArray[0]))
        {
          string str = strArray[0];
        }
      }
      string Language = this.dataGridView2.SelectedRows[0].Cells["Language"].Value.ToString();
      exportText.setLanguage(Language);
      this.Visible = false;
      int num = (int) exportText.ShowDialog();
      this.Close();
    }

    private void button3_Click(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}
