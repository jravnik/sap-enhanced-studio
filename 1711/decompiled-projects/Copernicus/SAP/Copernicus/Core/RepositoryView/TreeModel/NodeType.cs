﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.NodeType
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public enum NodeType
  {
    SAPContent,
    MySolutions,
    Solution,
    Namespace,
    BOFolder,
    DTFolder,
    BusinessObject,
    DataType,
    BONode,
    BOAction,
    ActionFolder,
    DeterminationFolder,
    BODetermination,
    EnhancementOption,
    EnhancementOptionFolder,
  }
}
