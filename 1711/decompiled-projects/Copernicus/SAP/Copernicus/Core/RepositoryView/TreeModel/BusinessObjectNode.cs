﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BusinessObjectNode
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BusinessObjectNode : BaseNode
  {
    private string boName;

    public BusinessObjectNode(RepositoryDataSet.BusinessObjectsRow boRow)
      : base((DataRow) boRow)
    {
      this.boName = boRow.Name;
      this.BOProxyName = boRow.ProxyName;
      this.Text = this.boName;
      this.ImageIndex = 6;
      this.SelectedImageIndex = 6;
    }

    public override string BOName
    {
      get
      {
        return this.boName;
      }
    }

    public string BOProxyName { get; set; }

    public RepositoryDataSet.BusinessObjectsRow getData()
    {
      return (RepositoryDataSet.BusinessObjectsRow) this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.BusinessObject;
    }
  }
}
