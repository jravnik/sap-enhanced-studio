﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BaseNode
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public abstract class BaseNode : TreeNode
  {
    protected DataRow content;

    protected BaseNode(DataRow dataRow)
    {
      this.content = dataRow;
      this.ContextMenuStrip = new ContextMenuStrip();
    }

    public bool isNodeType(NodeType type)
    {
      return this.getNodeType() == type;
    }

    public DataRow getData()
    {
      return this.content;
    }

    public BaseNode getParent()
    {
      return this.Parent as BaseNode;
    }

    public RepositoryViewControl getRepositoryViewControl()
    {
      return (RepositoryViewControl) this.TreeView.Parent;
    }

    public abstract NodeType getNodeType();

    public virtual void refresh()
    {
      Trace.TraceWarning("Refresh not supported for " + (object) this.getNodeType());
    }

    public virtual string Namespace
    {
      get
      {
        BaseNode parent = this.getParent();
        if (parent == null)
          return (string) null;
        return parent.Namespace;
      }
    }

    public virtual string BOName
    {
      get
      {
        BaseNode parent = this.getParent();
        if (parent == null)
          return (string) null;
        return parent.BOName;
      }
    }

    public virtual string NodeName
    {
      get
      {
        BaseNode parent = this.getParent();
        if (parent == null)
          return (string) null;
        return parent.NodeName;
      }
    }
  }
}
