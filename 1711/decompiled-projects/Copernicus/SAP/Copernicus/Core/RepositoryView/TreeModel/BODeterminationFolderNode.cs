﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BODeterminationFolderNode
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BODeterminationFolderNode : BaseNode
  {
    public string nodeProxyName;
    public string nodeName;

    public BODeterminationFolderNode(string nodeProxyName, string nodeName)
      : this((DataRow) null)
    {
      this.nodeProxyName = nodeProxyName;
      this.nodeName = nodeName;
    }

    public BODeterminationFolderNode(DataRow dataRow)
      : base(dataRow)
    {
      this.Text = CopernicusResources.TreeNodeTextDeterminationFolder;
      this.ImageIndex = 6;
      this.SelectedImageIndex = 6;
    }

    public override NodeType getNodeType()
    {
      return NodeType.DeterminationFolder;
    }
  }
}
