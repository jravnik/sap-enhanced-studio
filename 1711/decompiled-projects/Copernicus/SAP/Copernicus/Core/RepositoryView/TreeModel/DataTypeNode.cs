﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.DataTypeNode
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class DataTypeNode : BaseNode
  {
    private string dtName;

    public DataTypeNode(RepositoryDataSet.DataTypesRow dtRow)
      : base((DataRow) dtRow)
    {
      this.dtName = dtRow.Name;
      this.Text = this.dtName;
      this.ImageIndex = 3;
      this.SelectedImageIndex = 3;
    }

    public RepositoryDataSet.DataTypesRow getData()
    {
      return (RepositoryDataSet.DataTypesRow) this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.DataType;
    }
  }
}
