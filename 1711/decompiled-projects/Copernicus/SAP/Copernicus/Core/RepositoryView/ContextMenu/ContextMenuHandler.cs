﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.ContextMenu.ContextMenuHandler
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.PDI;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.Core.RepositoryView.TreeModel;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Core.Util.Forms;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.RepositoryView.ContextMenu
{
    public class ContextMenuHandler
    {
        private BaseNode GetSelectedTreeNode(object sender)
        {
            TreeView sourceControl = ((ContextMenuStrip)((ToolStripItem)sender).Owner).SourceControl as TreeView;
            return sourceControl == null ? (BaseNode)null : sourceControl.SelectedNode as BaseNode;
        }

        public void refresh_Click(object sender, EventArgs e)
        {
            BaseNode selectedTreeNode = this.GetSelectedTreeNode(sender);
            Trace.WriteLine("Context Menu Click: Refresh for - " + (object)selectedTreeNode.getNodeType() + " (" + selectedTreeNode.Text + ")");
            this.RefreshTreeNode(selectedTreeNode);
        }

        private void RefreshTreeNode(BaseNode node)
        {
            RepositoryViewControl repositoryViewControl = node.getRepositoryViewControl();
            repositoryViewControl.disableToolStripRefresh();
            repositoryViewControl.Cursor = Cursors.AppStarting;
            node.refresh();
            repositoryViewControl.enableToolStripRefresh();
            repositoryViewControl.Cursor = Cursors.Default;
        }

        private bool SolutionNameIsValid(string name)
        {
            if (Connection.getInstance().UsageMode != UsageMode.OneOff && Connection.getInstance().UsageMode != UsageMode.miltiCustomer)
                return !string.IsNullOrEmpty(name);
            return true;
        }

        public void CreateSolution_Click(object sender, EventArgs e)
        {
            if (Connection.getInstance().UsageMode != UsageMode.OneOff)
            {
                if (Connection.getInstance().UsageMode != UsageMode.miltiCustomer)
                {
                    if (Environment.GetEnvironmentVariable("_copernicus_trace_file") != "AllowScalable")
                    {
                        int num = (int)CopernicusMessageBox.Show(SAP.Copernicus.Resource.MsgScalableNotAllowed, SAP.Copernicus.Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        return;
                    }
                }
            }
            try
            {
                CreateSolutionWizard createSolutionWizard = new CreateSolutionWizard();
                int num = (int)createSolutionWizard.ShowDialog();
                if (createSolutionWizard == null || !this.SolutionNameIsValid(createSolutionWizard.SolutionName) || (string.IsNullOrWhiteSpace(createSolutionWizard.SolutionDescription) || string.IsNullOrWhiteSpace(createSolutionWizard.SolutionType)) || (string.IsNullOrWhiteSpace(createSolutionWizard.SolutionDetailedDescription) || string.IsNullOrWhiteSpace(createSolutionWizard.DeploymentUnit) || (string.IsNullOrWhiteSpace(createSolutionWizard.CompanyName) || string.IsNullOrWhiteSpace(createSolutionWizard.ContactPerson))) || !(SAP.Copernicus.Core.Util.Util.ValidateEmail(createSolutionWizard.EMail) != SAP.Copernicus.Resource.EmailValidationFailed))
                    return;
                CopernicusProjectSystemUtil.CloseCurrentSolution(true);
                XRepMapper.GetInstance().CreateSolution(createSolutionWizard.SolutionName, createSolutionWizard.SolutionDescription, createSolutionWizard.SolutionDetailedDescription, createSolutionWizard.Keywords, createSolutionWizard.SolutionType, createSolutionWizard.DeploymentUnit, createSolutionWizard.IsLocalSolution, createSolutionWizard.UseBBC, createSolutionWizard.DevPartner, createSolutionWizard.ContactPerson, createSolutionWizard.EMail);
                ExtensionDataCache.GetInstance().Reset();
                this.refresh_Click(sender, e);
            }
            catch (ProtocolException ex)
            {
                ex.GetType();
            }
            catch (Exception ex)
            {
                ErrorSink.Instance.AddTask("Severe error in solution creation: " + ex.ToString(), Origin.Unknown, Severity.Error, (EventHandler)null);
            }
        }

        public static void ShowCreateSolution()
        {
        }

        public void DeleteSolution_Click(object sender, EventArgs e)
        {
            SolutionNode selectedTreeNode = (SolutionNode)this.GetSelectedTreeNode(sender);
            if (MessageBox.Show(string.Format(SAP.Copernicus.Resource.MsgCancelSolutionDeletion, (object)selectedTreeNode.solutionName), SAP.Copernicus.Resource.MsgBYD, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) != DialogResult.Yes)
                return;
            MySolutionsNode parent = selectedTreeNode.getParent() as MySolutionsNode;
            this.DeleteSolution(selectedTreeNode);
            this.RefreshTreeNode((BaseNode)parent);
        }

        private void DeleteSolution(SolutionNode toBeDeletedSolution)
        {
            try
            {
                string name;
                string filePath;
                string version;
                CopernicusProjectSystemUtil.GetCurrentlyOpenedSolutionsData(out name, out filePath, out version);
                string solutionName = toBeDeletedSolution.solutionName;
                string solutionVersion = toBeDeletedSolution.solutionVersion;
                if (name != null && name == solutionName && !CopernicusProjectSystemUtil.CloseCurrentSolution(true))
                    return;
                XRepMapper instance = XRepMapper.GetInstance();
                CustomDialog customDialog = new CustomDialog(CopernicusResources.WaitCaption, CopernicusResources.WaitDeleteSolution);
                try
                {
                    RepositoryDataCache.GetInstance().DeleteSolution(solutionName, solutionVersion);
                    instance.DeleteSolutionFromXRep(solutionName, solutionVersion);
                    ExtensionDataCache.GetInstance().Reset();
                }
                finally
                {
                    if (customDialog != null)
                        customDialog.destroy();
                }
            }
            catch (ProtocolException ex)
            {
                ex.GetType();
            }
            catch (Exception ex)
            {
                ErrorSink.Instance.AddTask("Severe error while closing a solution: " + ex.ToString(), Origin.Unknown, Severity.Error, (EventHandler)null);
            }
        }

        public void CopySolutionName_Click(object sender, EventArgs e)
        {
            SolutionNode selectedTreeNode = (SolutionNode)this.GetSelectedTreeNode(sender);
            Clipboard.SetText(String.Format("{0} ({1})", selectedTreeNode.solutionDescription, selectedTreeNode.solutionName));
        }

        public void OpenSolution_Click(object sender, EventArgs e)
        {
            this.OpenSolution((SolutionNode)this.GetSelectedTreeNode(sender));
        }

        public void SolutionProperty_Click(object sender, EventArgs e)
        {
            string partnerName = Connection.getInstance().PartnerName;
            SolutionNode selectedTreeNode = (SolutionNode)this.GetSelectedTreeNode(sender);
            string solutionType = SolutionType.fromSolutionTypeCode(selectedTreeNode.solutionType).ToString();
            string devPartner = selectedTreeNode.devPartner;
            string contactPerson = selectedTreeNode.contactPerson;
            string email = selectedTreeNode.eMail;
            bool patchSolution = selectedTreeNode.patchSolution;
            string overallStatus = "";
            string solutionDescription = "";
            string detailedDescription = "";
            string keywords = "";
            string certificationStatus = "";
            string unitSemanticalName = DeploymentUnit.fromDUProxyName(new SolutionHandler().GetSolutionVersion(selectedTreeNode.solutionName, selectedTreeNode.solutionVersion, out overallStatus, out certificationStatus, out solutionDescription, out detailedDescription, out keywords).COMP_VERSIONS[0].COMPONENT.DEFAULT_DU).GetDeploymentUnitSemanticalName();
            DataRow[] dataRowArray = RepositoryDataCache.GetInstance().RepositoryDataSet.Solutions.Select("Name='" + selectedTreeNode.solutionName + "' AND Version='" + selectedTreeNode.solutionVersion + "'");
            int index = 0;
            if (index < dataRowArray.Length)
            {
                RepositoryDataSet.SolutionsRow solutionsRow = (RepositoryDataSet.SolutionsRow)dataRowArray[index];
                if (solutionsRow != null)
                {
                    devPartner = solutionsRow.DevPartner;
                    contactPerson = solutionsRow.ContactPerson;
                    email = solutionsRow.EMail;
                }
            }
            try
            {
                CreateSolutionWizard createSolutionWizard = new CreateSolutionWizard(selectedTreeNode.solutionName, partnerName, unitSemanticalName, solutionDescription, detailedDescription, keywords, solutionType, devPartner, contactPerson, email, patchSolution, overallStatus);
                int num = (int)createSolutionWizard.ShowDialog();
                if (createSolutionWizard.DevPartner == null || createSolutionWizard.ContactPerson == null || createSolutionWizard.EMail == null || createSolutionWizard.DevPartner.Equals(devPartner) && createSolutionWizard.ContactPerson.Equals(contactPerson) && (createSolutionWizard.EMail.Equals(email) && createSolutionWizard.SolutionDetailedDescription.Equals(detailedDescription)) && createSolutionWizard.Keywords.Equals(keywords))
                    return;
                XRepMapper.GetInstance().UpdateSolution(selectedTreeNode.solutionName, createSolutionWizard.DevPartner, createSolutionWizard.ContactPerson, createSolutionWizard.EMail, createSolutionWizard.SolutionDetailedDescription, createSolutionWizard.Keywords);
                this.refresh_Click(sender, e);
            }
            catch (ProtocolException ex)
            {
                ex.GetType();
            }
            catch (Exception ex)
            {
                ErrorSink.Instance.AddTask("Severe error in solution update: " + ex.ToString(), Origin.Unknown, Severity.Error, (EventHandler)null);
            }
        }

        private void OpenSolution(SolutionNode selectedSolutionNode)
        {
            try
            {
                CopernicusFileNodeProperties.clearBuffer();
                string currentSolutionName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
                string solutionName = selectedSolutionNode.solutionName;
                string pdiSubType = selectedSolutionNode.pdiSubType;
                string solutionType = selectedSolutionNode.solutionType;
                if (currentSolutionName != null && currentSolutionName == selectedSolutionNode.solutionName)
                {
                    int num = (int)MessageBox.Show(string.Format(Resources.MsgSolutionAlreadyOpen, (object)selectedSolutionNode.solutionName), Resources.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    if (!CopernicusProjectSystemUtil.CloseCurrentSolution(true))
                        return;
                    XRepMapper.GetInstance().GetSolutionFromXRepository(selectedSolutionNode.solutionName, selectedSolutionNode.solutionVersion, solutionType, pdiSubType);
                    ExtensionDataCache.GetInstance().Reset();
                }
            }
            catch (ProtocolException ex)
            {
                ex.GetType();
            }
            catch (Exception ex)
            {
                ErrorSink.Instance.AddTask("Severe error while opening a solution: " + ex.ToString(), Origin.Unknown, Severity.Error, (EventHandler)null);
            }
        }

        public void treeView_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar != 13)
                return;
            TreeView treeView = sender as TreeView;
            if (treeView == null || treeView.SelectedNode == null || !(treeView.SelectedNode is SolutionNode))
                return;
            this.OpenSolution((SolutionNode)treeView.SelectedNode);
        }

        public void treeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (!(e.Node is SolutionNode))
                return;
            SolutionNode node = (SolutionNode)e.Node;
            if (node.ImageIndex == 14)
                return;
            this.OpenSolution(node);
        }

        public void treeNodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;
            TreeNode node = e.Node;
            TreeView treeView = sender as TreeView;
            if (treeView == null || treeView.SelectedNode == node)
                return;
            treeView.SelectedNode = node;
            treeView.Select();
        }
    }
}
