﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ErrorList.CopernicusErrorTask
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;

namespace SAP.Copernicus.Core.ErrorList
{
  internal class CopernicusErrorTask : ErrorTask
  {
    private readonly int endLine;
    private readonly int endIndex;
    private readonly Origin origin;

    public CopernicusErrorTask(TextSpan span, string fileName, string message, TaskErrorCategory errorCategory, Origin origin = Origin.Unknown)
    {
      if (!string.IsNullOrEmpty(fileName) && fileName.StartsWith("/"))
        this.Document = XRepMapper.GetInstance().GetLocalPathforXRepProjectEntities(fileName);
      else
        this.Document = fileName;
      this.Text = message;
      this.Priority = TaskPriority.Normal;
      this.Category = TaskCategory.BuildCompile;
      this.ErrorCategory = errorCategory;
      this.Line = Math.Max(span.iStartLine, 0);
      this.Column = Math.Max(span.iStartIndex, 0);
      this.endLine = span.iEndLine < this.Line ? this.Line : Math.Max(span.iEndLine, 0);
      this.endIndex = this.Line != this.EndLine || span.iEndIndex >= this.Column ? Math.Max(span.iEndIndex, 0) : this.Column;
      this.origin = origin;
    }

    public TextSpan TextSpan
    {
      get
      {
        return new TextSpan()
        {
          iStartLine = this.Line,
          iStartIndex = this.Column,
          iEndLine = this.EndLine,
          iEndIndex = this.EndIndex
        };
      }
    }

    public int EndLine
    {
      get
      {
        return this.endLine;
      }
    }

    public int EndIndex
    {
      get
      {
        return this.endIndex;
      }
    }

    public Origin Origin
    {
      get
      {
        return this.origin;
      }
    }
  }
}
