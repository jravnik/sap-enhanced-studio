﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ProductionBugFix.Disclaimer
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.GUI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.ProductionBugFix
{
  public class Disclaimer : Form
  {
    private IContainer components;
    private Button btnCancel;
    private Button btnOK;
    private GroupBox gb_securityTerms;
    private WebBrowser webBrowser;
    private CheckBox chkb_accept;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.btnCancel = new Button();
      this.btnOK = new Button();
      this.gb_securityTerms = new GroupBox();
      this.webBrowser = new WebBrowser();
      this.chkb_accept = new CheckBox();
      this.gb_securityTerms.SuspendLayout();
      this.SuspendLayout();
      this.btnCancel.Location = new Point(547, 644);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new Size(75, 23);
      this.btnCancel.TabIndex = 10;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
      this.btnOK.Enabled = false;
      this.btnOK.Location = new Point(464, 644);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new Size(75, 23);
      this.btnOK.TabIndex = 9;
      this.btnOK.Text = "Continue";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new EventHandler(this.btnOK_Click);
      this.gb_securityTerms.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.gb_securityTerms.Controls.Add((Control) this.webBrowser);
      this.gb_securityTerms.Controls.Add((Control) this.chkb_accept);
      this.gb_securityTerms.Location = new Point(13, 10);
      this.gb_securityTerms.Name = "gb_securityTerms";
      this.gb_securityTerms.Size = new Size(609, 617);
      this.gb_securityTerms.TabIndex = 11;
      this.gb_securityTerms.TabStop = false;
      this.webBrowser.Location = new Point(10, 19);
      this.webBrowser.MinimumSize = new Size(20, 20);
      this.webBrowser.Name = "webBrowser";
      this.webBrowser.Size = new Size(593, 552);
      this.webBrowser.TabIndex = 7;
      this.webBrowser.DocumentText = CopernicusResources.ProdBugFixDisclaimer;
      this.chkb_accept.AutoSize = true;
      this.chkb_accept.Location = new Point(13, 583);
      this.chkb_accept.Name = "chkb_accept";
      this.chkb_accept.Size = new Size(193, 17);
      this.chkb_accept.TabIndex = 0;
      this.chkb_accept.Text = "I agree to the aforementioned terms";
      this.chkb_accept.UseVisualStyleBackColor = true;
      this.chkb_accept.Click += new EventHandler(this.chkb_accept_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.ClientSize = new Size(634, 691);
      this.ControlBox = false;
      this.Controls.Add((Control) this.btnCancel);
      this.Controls.Add((Control) this.btnOK);
      this.Controls.Add((Control) this.gb_securityTerms);
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.Name = nameof (Disclaimer);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = nameof (Disclaimer);
      this.TopMost = true;
      this.gb_securityTerms.ResumeLayout(false);
      this.gb_securityTerms.PerformLayout();
      this.ResumeLayout(false);
    }

    public Disclaimer()
    {
      this.InitializeComponent();
    }

    private void chkb_accept_Click(object sender, EventArgs e)
    {
      if (this.chkb_accept.Checked)
        this.btnOK.Enabled = true;
      else
        this.btnOK.Enabled = false;
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
      this.Close();
      CopernicusStatusBar.Instance.ShowMessage(Resource.OpenCorrectionFailed);
    }
  }
}
