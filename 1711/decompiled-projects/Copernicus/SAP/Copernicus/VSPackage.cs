﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.VSPackage
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [CompilerGenerated]
  [DebuggerNonUserCode]
  internal class VSPackage
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    internal VSPackage()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) VSPackage.resourceMan, (object) null))
          VSPackage.resourceMan = new ResourceManager("SAP.Copernicus.VSPackage", typeof (VSPackage).Assembly);
        return VSPackage.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return VSPackage.resourceCulture;
      }
      set
      {
        VSPackage.resourceCulture = value;
      }
    }

    internal static string _1
    {
      get
      {
        return VSPackage.ResourceManager.GetString("1", VSPackage.resourceCulture);
      }
    }

    internal static string _110
    {
      get
      {
        return VSPackage.ResourceManager.GetString("110", VSPackage.resourceCulture);
      }
    }

    internal static string _112
    {
      get
      {
        return VSPackage.ResourceManager.GetString("112", VSPackage.resourceCulture);
      }
    }

    internal static string _113
    {
      get
      {
        return VSPackage.ResourceManager.GetString("113", VSPackage.resourceCulture);
      }
    }

    internal static string _2
    {
      get
      {
        return VSPackage.ResourceManager.GetString("2", VSPackage.resourceCulture);
      }
    }

    internal static Bitmap _300
    {
      get
      {
        return (Bitmap) VSPackage.ResourceManager.GetObject("300", VSPackage.resourceCulture);
      }
    }

    internal static Bitmap _302
    {
      get
      {
        return (Bitmap) VSPackage.ResourceManager.GetObject("302", VSPackage.resourceCulture);
      }
    }

    internal static Icon _400
    {
      get
      {
        return (Icon) VSPackage.ResourceManager.GetObject("400", VSPackage.resourceCulture);
      }
    }
  }
}
