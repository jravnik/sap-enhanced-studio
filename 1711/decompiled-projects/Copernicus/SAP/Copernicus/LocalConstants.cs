﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.LocalConstants
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

namespace SAP.Copernicus
{
  public class LocalConstants
  {
    internal const string URL_UI_DESIGNER_PART1_XREP_QQ6 = "/ap/XREP/pub/VIRTUAL/SAP_BYD_UI_DT/UIDesigner.application?";
    internal const string URL_UI_DESIGNER_PART1_XREP = "/public/ap/ui/xrep/VIRTUAL/SAP_BYD_UI_DT/UIDesigner.application?";
    internal const string URL_UI_DESIGNER_PART2_START = "startcomponent=";
    internal const string URL_UI_DESIGNER_PART3_DISABLE_CONF_EXPL = "&disableConfigExplorer=true";
    internal const string URL_UI_RUNTIME_PART_1_REPOS = "/sap/ap/ui/repository/SAP_BYD_UI/Runtime/StartPage.html?";
    internal const string URL_UI_RUNTIME_PART_2_Client = "sap-client=";
    internal const string URL_UI_RUNTIME_PART_3_AUTH = "&sap-system-login-basic_auth=X";
    internal const string URL_UI_RUNTIME_PART_4_LANG = "&sap-language=";
    internal const string URL_UI_RUNTIME_PART_5_APP = "&app.component=";
  }
}
