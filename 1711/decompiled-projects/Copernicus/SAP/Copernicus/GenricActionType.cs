﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.GenricActionType
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

namespace SAP.Copernicus
{
  public enum GenricActionType
  {
    Activate = 8,
    Clean = 14,
    Check = 17,
  }
}
