﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ToolOptions.DRTContentExport
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using EnvDTE;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Scripting;
using SAP.CopernicusProjectView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SAP.Copernicus.ToolOptions
{
  public class DRTContentExport : Form
  {
    private IContainer components;
    private FolderBrowserDialog folderBrowserDialog;
    private Label drtExportSolution;
    private TextBox textBoxTargetDir;
    private Label label2;
    private Label label1;
    private Button buttonExport;
    private Button buttonExit;
    private Button s;
    private TextBox textBoxExportLog;
    private Label labelExportLog;
    private CopernicusProjectNode project;
    private string projectNamespace;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.drtExportSolution = new Label();
      this.textBoxTargetDir = new TextBox();
      this.label2 = new Label();
      this.label1 = new Label();
      this.buttonExport = new Button();
      this.buttonExit = new Button();
      this.folderBrowserDialog = new FolderBrowserDialog();
      this.s = new Button();
      this.textBoxExportLog = new TextBox();
      this.labelExportLog = new Label();
      this.SuspendLayout();
      this.drtExportSolution.AutoSize = true;
      this.drtExportSolution.Location = new Point(99, 9);
      this.drtExportSolution.Name = "drtExportSolution";
      this.drtExportSolution.Size = new Size(16, 13);
      this.drtExportSolution.TabIndex = 20;
      this.drtExportSolution.Text = "---";
      this.textBoxTargetDir.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.textBoxTargetDir.BackColor = SystemColors.Window;
      this.textBoxTargetDir.Location = new Point(99, 35);
      this.textBoxTargetDir.Name = "textBoxTargetDir";
      this.textBoxTargetDir.Size = new Size(208, 20);
      this.textBoxTargetDir.TabIndex = 19;
      this.textBoxTargetDir.Text = "C:\\DTDLSolutionContent";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(9, 9);
      this.label2.Name = "label2";
      this.label2.Size = new Size(81, 13);
      this.label2.TabIndex = 17;
      this.label2.Text = "Export Solution:";
      this.label1.AutoSize = true;
      this.label1.Location = new Point(9, 38);
      this.label1.Name = "label1";
      this.label1.Size = new Size(73, 13);
      this.label1.TabIndex = 18;
      this.label1.Text = "Target Folder:";
      this.buttonExport.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonExport.Location = new Point(416, 325);
      this.buttonExport.Name = "buttonExport";
      this.buttonExport.Size = new Size(75, 23);
      this.buttonExport.TabIndex = 15;
      this.buttonExport.Text = "Export";
      this.buttonExport.UseVisualStyleBackColor = true;
      this.buttonExport.Click += new EventHandler(this.buttonExport_Click);
      this.buttonExit.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonExit.Location = new Point(497, 325);
      this.buttonExit.Name = "buttonExit";
      this.buttonExit.Size = new Size(75, 23);
      this.buttonExit.TabIndex = 16;
      this.buttonExit.Text = "Exit";
      this.buttonExit.UseVisualStyleBackColor = true;
      this.buttonExit.Click += new EventHandler(this.buttonExit_Click);
      this.s.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.s.Location = new Point(317, 33);
      this.s.Name = "s";
      this.s.Size = new Size(34, 23);
      this.s.TabIndex = 14;
      this.s.Text = "...";
      this.s.UseVisualStyleBackColor = true;
      this.textBoxExportLog.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.textBoxExportLog.BackColor = SystemColors.Window;
      this.textBoxExportLog.Location = new Point(12, 93);
      this.textBoxExportLog.Multiline = true;
      this.textBoxExportLog.Name = "textBoxExportLog";
      this.textBoxExportLog.ReadOnly = true;
      this.textBoxExportLog.ScrollBars = ScrollBars.Both;
      this.textBoxExportLog.Size = new Size(560, 218);
      this.textBoxExportLog.TabIndex = 12;
      this.textBoxExportLog.WordWrap = false;
      this.labelExportLog.AutoSize = true;
      this.labelExportLog.Location = new Point(9, 77);
      this.labelExportLog.Name = "labelExportLog";
      this.labelExportLog.Size = new Size(58, 13);
      this.labelExportLog.TabIndex = 21;
      this.labelExportLog.Text = "Export Log";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(584, 362);
      this.Controls.Add((Control) this.labelExportLog);
      this.Controls.Add((Control) this.textBoxTargetDir);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.buttonExport);
      this.Controls.Add((Control) this.buttonExit);
      this.Controls.Add((Control) this.s);
      this.Controls.Add((Control) this.textBoxExportLog);
      this.Controls.Add((Control) this.drtExportSolution);
      this.Name = nameof (DRTContentExport);
      this.Text = "DTDL Solution Content Export";
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public DRTContentExport()
    {
      this.InitializeComponent();
      this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;
      this.folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;
      if (!string.IsNullOrEmpty(this.textBoxTargetDir.Text))
        this.folderBrowserDialog.SelectedPath = this.textBoxTargetDir.Text;
      this.Activated += new EventHandler(this.ActivatedEvent);
    }

    public bool IsSolutionOpen()
    {
      try
      {
        Solution solution = DTEUtil.GetDTE().Solution;
        IEnumerator enumerator = solution.Projects.GetEnumerator();
        try
        {
          if (enumerator.MoveNext())
          {
            this.project = ((Project) enumerator.Current).Object as CopernicusProjectNode;
            this.drtExportSolution.Text = Path.GetFileNameWithoutExtension(solution.FullName) + " - " + this.project.Caption;
            this.projectNamespace = this.project.RepositoryNamespace;
            return true;
          }
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            disposable.Dispose();
        }
      }
      catch (NullReferenceException ex)
      {
        int num = (int) CopernicusMessageBox.Show("Open the solution from which the content should be exported!");
      }
      return false;
    }

    private void ActivatedEvent(object sender, EventArgs e)
    {
      this.buttonExit.Focus();
    }

    private void buttonExit_Click(object sender, EventArgs e)
    {
      this.Close();
      this.Dispose();
    }

    private void buttonExport_Click(object sender, EventArgs e)
    {
      List<string> stringList = new List<string>();
      this.textBoxExportLog.Lines = new string[0];
      this.Refresh();
      string directory = this.project.BaseURI.Directory;
      string text = this.textBoxTargetDir.Text;
      if (string.IsNullOrEmpty(directory) || string.IsNullOrEmpty(text))
        return;
      if (Directory.Exists(text))
        Directory.Delete(text, true);
      Directory.CreateDirectory(text);
      Directory.CreateDirectory(Path.Combine(text, "RepDataCache"));
      new Stopwatch().Start();
      stringList.Add("Exporting RepositoryDataCache content...");
      RepositoryDataCache.GetInstance().ExportContentToFile(Path.Combine(text, "RepDataCache\\Content.xml"));
      stringList.Add("Done.");
    }

    private void ExportSFM(string currentFile, IScriptFileMetadata sfm)
    {
    }

    private void buttonSelTarget_Click(object sender, EventArgs e)
    {
    }
  }
}
