﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.isistruct
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace CopernicusBusinessObjectBrowser
{
  public struct isistruct
  {
    public string isiname;
    public string isinamespace;
    public string isidirection;
    public string isiproxyname;
    public PDI_RI_T_OP[] isioperations;
  }
}
