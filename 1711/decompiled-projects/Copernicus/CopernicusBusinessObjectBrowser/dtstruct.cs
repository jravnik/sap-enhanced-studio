﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.dtstruct
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

namespace CopernicusBusinessObjectBrowser
{
  public struct dtstruct
  {
    public string dtproxyname;
    public string dtname;
    public string dtnamespace;
    public string dtusage;
  }
}
