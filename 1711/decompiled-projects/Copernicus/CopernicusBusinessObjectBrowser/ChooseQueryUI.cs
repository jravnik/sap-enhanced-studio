﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.ChooseQueryUI
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core;
using SAP.Copernicus.Model.BusinessObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace CopernicusBusinessObjectBrowser
{
  public class ChooseQueryUI : Form
  {
    private IContainer components;
    private ComboBox comboBox1;
    private Button buttonOK;
    private Label label1;
    private SAP.Copernicus.Model.BusinessObject.BusinessObject actBO;
    private BusinessObject_Query selectedQuery;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (BaseForm));
      this.comboBox1 = new ComboBox();
      this.buttonOK = new Button();
      this.label1 = new Label();
      this.SuspendLayout();
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new Point(12, 57);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new Size(194, 21);
      this.comboBox1.TabIndex = 0;
      this.buttonOK.Location = new Point(170, 126);
      this.buttonOK.Name = "buttonOK";
      this.buttonOK.Size = new Size(75, 23);
      this.buttonOK.TabIndex = 1;
      this.buttonOK.Text = "OK";
      this.buttonOK.UseVisualStyleBackColor = true;
      this.buttonOK.Click += new EventHandler(this.buttonOK_Click);
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 23);
      this.label1.Name = "label1";
      this.label1.Size = new Size(197, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Choose the Query you want to execute: ";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(257, 161);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.buttonOK);
      this.Controls.Add((Control) this.comboBox1);
      this.Name = nameof (ChooseQueryUI);
      this.Text = "Select Query";
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public ChooseQueryUI(SAP.Copernicus.Model.BusinessObject.BusinessObject bo)
    {
      this.InitializeComponent();
      this.actBO = bo;
      this.setupComboBox();
    }

    private void setupComboBox()
    {
      List<BusinessObject_Query> allQueries = ChooseQueryUI.getAllQueries(this.actBO);
      allQueries.RemoveAll((Predicate<BusinessObject_Query>) (item => item.Name == "SelectAll"));
      this.comboBox1.Items.AddRange((object[]) allQueries.Select<BusinessObject_Query, string>((Func<BusinessObject_Query, string>) (item => item.ToParent.Name + "." + item.Name)).ToArray<string>());
      this.comboBox1.SelectedIndex = 0;
    }

    public static List<BusinessObject_Query> getAllQueries(SAP.Copernicus.Model.BusinessObject.BusinessObject bo)
    {
      List<BusinessObject_Query> businessObjectQueryList = new List<BusinessObject_Query>();
      foreach (BusinessObject_Node businessObjectNode in bo.Nodes.ToList<BusinessObject_Node>())
      {
        if (businessObjectNode.Queries.Count != 0)
          businessObjectQueryList.AddRange((IEnumerable<BusinessObject_Query>) businessObjectNode.Queries);
      }
      return businessObjectQueryList;
    }

    private void buttonOK_Click(object sender, EventArgs e)
    {
      string nodeName = this.comboBox1.SelectedItem.ToString().Split('.')[0];
      string queryName = this.comboBox1.SelectedItem.ToString().Split('.')[1];
      this.selectedQuery = ChooseQueryUI.getAllQueries(this.actBO).Find((Predicate<BusinessObject_Query>) (item =>
      {
        if (item.Name == queryName)
          return item.ToParent.Name == nodeName;
        return false;
      }));
      this.Close();
    }

    public BusinessObject_Query ShowDialog()
    {
      int num = (int) base.ShowDialog();
      return this.selectedQuery;
    }
  }
}
