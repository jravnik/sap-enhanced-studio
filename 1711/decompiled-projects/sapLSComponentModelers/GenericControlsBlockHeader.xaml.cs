﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.BlockHeader
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public partial class BlockHeader : UserControl, IComponentConnector
  {
    private readonly TextBlock titleText;
    //internal Grid LayoutRoot;
    //internal Grid ContentGrid;
    //internal TextBlock TitleText;
    //private bool _contentLoaded;

    public string Text
    {
      get
      {
        return this.titleText.Text;
      }
      set
      {
        this.titleText.Text = value;
      }
    }

    public BlockHeader(string title)
    {
      this.InitializeComponent();
      this.titleText = (TextBlock) this.LayoutRoot.FindName(nameof (TitleText));
      this.titleText.Text = title;
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/genericcontrols/blockheader.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.LayoutRoot = (Grid) target;
    //      break;
    //    case 2:
    //      this.ContentGrid = (Grid) target;
    //      break;
    //    case 3:
    //      this.TitleText = (TextBlock) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
