﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.ContextOptionsPanel
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.UICore.Controls;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public partial class ContextOptionsPanel : UserControl, IComponentConnector
  {
    public new static readonly DependencyProperty ContextMenuProperty = DependencyProperty.Register(nameof (ContextMenu), typeof (ContextMenuEx), typeof (ContextOptionsPanel), (PropertyMetadata) new UIPropertyMetadata((PropertyChangedCallback) null));
    public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.Register(nameof (IsSelected), typeof (bool), typeof (ContextOptionsPanel), (PropertyMetadata) new UIPropertyMetadata((object) false, new PropertyChangedCallback(ContextOptionsPanel.OnIsSelected_PropertyChanged)));
    //internal Border rootBorder;
    //internal WrapPanel rootPanel;
    //private bool _contentLoaded;

    public ContextMenuEx ContextMenu
    {
      get
      {
        return (ContextMenuEx) this.GetValue(ContextOptionsPanel.ContextMenuProperty);
      }
      set
      {
        this.SetValue(ContextOptionsPanel.ContextMenuProperty, (object) value);
      }
    }

    public bool IsSelected
    {
      get
      {
        return (bool) this.GetValue(ContextOptionsPanel.IsSelectedProperty);
      }
      set
      {
        this.SetValue(ContextOptionsPanel.IsSelectedProperty, (object) value);
      }
    }

    private static void OnIsSelected_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      if ((bool) e.NewValue)
        return;
      ((UIElement) sender).Opacity = 0.3;
    }

    public ContextOptionsPanel()
    {
      this.InitializeComponent();
    }

    public ContextOptionsPanel(bool isMenuRequired)
    {
      this.InitializeComponent();
      if (!isMenuRequired)
        return;
      this.ContextMenu = new ContextMenuEx();
      this.rootPanel.Children.Add((UIElement) this.ContextMenu);
    }

    public void AddSingleItem(Control element)
    {
      element.Margin = new Thickness(1.0);
      this.rootPanel.Children.Add((UIElement) element);
      this.rootPanel.HorizontalAlignment = HorizontalAlignment.Center;
      this.rootPanel.VerticalAlignment = VerticalAlignment.Center;
    }

    public ContextMenuItem AddMenuItem(string content)
    {
      ContextMenuItem contextMenuItem = new ContextMenuItem();
      contextMenuItem.ToolTip = (object) content;
      object associatedImage = this.GetAssociatedImage(content);
      if (associatedImage is BitmapImage)
      {
        contextMenuItem.Icon = (ImageSource) (associatedImage as BitmapImage);
        contextMenuItem.Action = content;
      }
      else
      {
        contextMenuItem.MinWidth = 100.0;
        contextMenuItem.Header = associatedImage;
        contextMenuItem.Action = content;
      }
      return contextMenuItem;
    }

    private object GetAssociatedImage(string content)
    {
      switch (content)
      {
        case "Add":
          return (object) new BitmapImage(new Uri("pack://application:,,,/sapLSUISkins;component/dt.images/PNG/AddIcon.png", UriKind.Absolute));
        case "Add Parameter":
          return (object) content;
        case "Add PortType":
          return (object) content;
        case "Add Tab":
          return (object) content;
        case "Delete":
          return (object) new BitmapImage(new Uri("pack://application:,,,/sapLSUISkins;component/dt.images/PNG/Delete.png", UriKind.Absolute));
        case "Delete Column":
          return (object) content;
        case "Delete Row":
          return (object) new BitmapImage(new Uri("pack://application:,,,/sapLSUISkins;component/dt.images/GIF/RemoveRow.gif", UriKind.Absolute));
        case "Insert":
          return (object) content;
        case "Insert Column":
          return (object) content;
        case "Insert Row":
          return (object) content;
        case "Cut":
          return (object) new BitmapImage(new Uri("pack://application:,,,/sapLSUISkins;component/dt.images/PNG/CutHS.png", UriKind.Absolute));
        case "Copy":
          return (object) new BitmapImage(new Uri("pack://application:,,,/sapLSUISkins;component/dt.images/PNG/CopyHS.png", UriKind.Absolute));
        case "Paste":
          return (object) new BitmapImage(new Uri("pack://application:,,,/sapLSUISkins;component/dt.images/PNG/PasteHS.png", UriKind.Absolute));
        case "Column":
          return (object) content;
        case "Layout":
          return (object) content;
        case "Refresh":
          return (object) content;
        case "Rename":
          return (object) content;
        case "Row":
          return (object) content;
        case "Select":
          return (object) content;
        case "Add Self-Defined Todo":
          return (object) content;
        case "view":
          return (object) content;
        default:
          return (object) content;
      }
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/genericcontrols/contextoptionspanel.xaml", UriKind.Relative));
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.rootBorder = (Border) target;
    //      break;
    //    case 2:
    //      this.rootPanel = (WrapPanel) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
