﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualTextEditControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualTextEditControl : UserControl
  {
    private VisualInputField inputField;
    private int rows;
    private bool hasBorder;
    private bool isEditable;

    public int Rows
    {
      get
      {
        return this.rows;
      }
      set
      {
        this.rows = value;
        this.inputField.RowSpan = this.rows;
      }
    }

    public bool HasBorder
    {
      get
      {
        return this.hasBorder;
      }
      set
      {
        this.hasBorder = value;
        this.inputField.HasBorder = value;
      }
    }

    public bool IsEditable
    {
      get
      {
        return this.isEditable;
      }
      set
      {
        this.isEditable = value;
      }
    }

    public FieldStyles FieldStyle
    {
      get
      {
        return this.inputField.FieldStyle;
      }
      set
      {
        this.inputField.FieldStyle = value;
      }
    }

    public VisualTextEditControl(int rows, bool hasBorder, bool isEditable)
    {
      this.rows = rows;
      this.hasBorder = hasBorder;
      this.isEditable = isEditable;
      this.inputField = new VisualInputField(hasBorder, true);
      this.Content = (object) this.inputField;
    }
  }
}
