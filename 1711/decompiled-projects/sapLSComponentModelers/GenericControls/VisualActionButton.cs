﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualActionButton
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualActionButton : BaseSelectableControl
  {
    private Grid contentGrid;
    private Button button;

    public VisualActionButton(IModelObject modelItem)
      : base(modelItem)
    {
      this.MinHeight = 20.0;
      this.MinWidth = 50.0;
      this.BorderBrush = (Brush) Brushes.Green;
      this.contentGrid = new Grid();
      this.button = new Button();
      this.button.Margin = new Thickness(2.0, 3.0, 2.0, 2.0);
      this.button.VerticalAlignment = VerticalAlignment.Center;
      this.button.HorizontalAlignment = HorizontalAlignment.Left;
      this.button.Background = (Brush) Brushes.LightBlue;
      this.button.Content = (object) "ACTIONS";
      this.contentGrid.Children.Add((UIElement) this.button);
      this.Content = (object) this.contentGrid;
    }

    public override string SelectionText
    {
      get
      {
        return "Action Button";
      }
    }
  }
}
