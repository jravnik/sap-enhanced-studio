﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.CCTSFormatter
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.Model.Entities.CCTS;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class CCTSFormatter
  {
    private static readonly CCTSFormatter m_Instance = new CCTSFormatter();

    private CCTSFormatter()
    {
    }

    public static CCTSFormatter Instance
    {
      get
      {
        return CCTSFormatter.m_Instance;
      }
    }

    public FieldWidth ResolveWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      FieldWidth fieldWidth = FieldWidths.TryRetrieveControlDefaultWidth(controlDefinition.GetOberonModel().GetType().Name, usage.ToString());
      if (fieldWidth == null)
      {
        switch (controlDefinition.CCTSType)
        {
          case UXCCTSTypes.amount:
            fieldWidth = this.ResolveAmountCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.code:
            fieldWidth = this.ResolveCodeCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.date:
            fieldWidth = this.ResolveDateCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.datetime:
            fieldWidth = this.ResolveDateTimeCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.description:
            fieldWidth = this.ResolveDescriptionCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.duration:
            fieldWidth = this.ResolveDurationCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.identifier:
            fieldWidth = this.ResolveIdentifierCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.indicator:
            fieldWidth = this.ResolveIndicatorCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.measure:
            fieldWidth = this.ResolveMeasureCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.name:
            fieldWidth = this.ResolveNameCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.numeric:
            fieldWidth = this.ResolveNumericCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.percent:
            fieldWidth = this.ResolvePercentCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.quantity:
            fieldWidth = this.ResolveQuantityCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.ratio:
            fieldWidth = this.ResolveRatioCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.text:
            fieldWidth = this.ResolveTextCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.time:
            fieldWidth = this.ResolveTimeCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.value:
            fieldWidth = this.ResolveValueCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
          case UXCCTSTypes.uri:
            fieldWidth = this.ResolveUriCCTSWidth(controlDefinition, enclosedCCTSType, usage);
            break;
        }
      }
      return fieldWidth;
    }

    public FieldWidth ResolveAmountCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.amount.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveCodeCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      CodeCCTS cctsItem = controlDefinition.CCTSItem as CodeCCTS;
      if (cctsItem == null)
        return (FieldWidth) null;
      switch (enclosedCCTSType)
      {
        case UXCCTSTypes.amount:
        case UXCCTSTypes.measure:
        case UXCCTSTypes.quantity:
          return FieldWidths.GetCCTSDefaultWidth(enclosedCCTSType.ToString(), usage.ToString(), "code", cctsItem.CodePresentationMode.ToString());
        default:
          return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.code.ToString(), usage.ToString(), "value", cctsItem.CodePresentationMode.ToString());
      }
    }

    public FieldWidth ResolveDateCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      BaseDateCCTS cctsItem = controlDefinition.CCTSItem as BaseDateCCTS;
      if (cctsItem != null)
        return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.date.ToString(), usage.ToString(), "value", cctsItem.DateFormat.ToString());
      return (FieldWidth) null;
    }

    public FieldWidth ResolveDateTimeCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return new FieldWidth("100%", "100%");
    }

    public FieldWidth ResolveDescriptionCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.description.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveDurationCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      BaseDurationCCTS cctsItem = controlDefinition.CCTSItem as BaseDurationCCTS;
      if (cctsItem == null)
        return (FieldWidth) null;
      switch (cctsItem.DurationUnit)
      {
        case DurationUnitType.Full:
          string str = (string) null;
          if (usage == UsageType.form)
            str = FieldWidths.Translate(FieldWidths.VERY_LONG);
          return new FieldWidth(str, str);
        case DurationUnitType.Time:
          return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.time.ToString(), usage.ToString(), "value");
        default:
          return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.measure.ToString(), usage.ToString(), "value");
      }
    }

    public FieldWidth ResolveIdentifierCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      BaseIdentifierCCTS cctsItem = controlDefinition.CCTSItem as BaseIdentifierCCTS;
      if (cctsItem != null)
        return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.identifier.ToString(), usage.ToString(), "value", cctsItem.PresentationMode.ToString());
      return (FieldWidth) null;
    }

    public FieldWidth ResolveIndicatorCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.indicator.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveMeasureCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.measure.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveNameCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.name.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveNoteCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.note.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveNumericCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.numeric.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolvePercentCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.percent.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveQuantityCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.measure.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveRatioCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.ratio.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveTextCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.text.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveTimeCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      BaseTimeCCTS cctsItem = controlDefinition.CCTSItem as BaseTimeCCTS;
      if (cctsItem == null)
        return (FieldWidth) null;
      TimeFormatType timeFormatType = cctsItem.TimeFormat == TimeFormatType.None ? TimeFormatType.HoursMinutesAndSeconds : cctsItem.TimeFormat;
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.time.ToString(), usage.ToString(), "value", timeFormatType.ToString());
    }

    public FieldWidth ResolveUriCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.uri.ToString(), usage.ToString(), "value");
    }

    public FieldWidth ResolveValueCCTSWidth(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      return FieldWidths.GetCCTSDefaultWidth(UXCCTSTypes.value.ToString(), usage.ToString(), "value");
    }
  }
}
