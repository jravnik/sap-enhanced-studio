﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.DisplayTextControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class DisplayTextControl : UserControl
  {
    private const string TEXTVIEW_DIALOGTITLE_FONT = "Arial,12,Bold";
    private const string TEXTVIEW_MSGBOXINFOTEXT_FONT = "Arial,11,Normal";
    private const string TEXTVIEW_MSGBOXMESSAGETEXT_FONT = "Arial,11,Bold";
    private const string TEXTVIEW_SECTIONGROUPTITLE_FONT = "Arial,11,Bold";
    private const string TEXTVIEW_LABEL_FONT = "Arial,11,Normal";
    private const string TEXTVIEW_ASTERISK_FONT = "Arial,12,Bold";
    private const string TEXTVIEW_TEXT_FONT = "Arial,11,Normal";
    private const string TEXTVIEW_PAGEHEADERTITLE_FONT = "Verdana,18,BoldItalic";
    private readonly TextViewStyles style;
    private TextBlock textBlock;

    public string Text
    {
      get
      {
        return this.textBlock.Text;
      }
      set
      {
        this.textBlock.Text = value;
      }
    }

    public DisplayTextControl(TextViewStyles style)
      : this(style, string.Empty)
    {
    }

    public DisplayTextControl(TextViewStyles style, string text)
    {
      this.style = style;
      this.InitializeControl();
      this.textBlock.Text = text;
      this.Content = (object) this.textBlock;
    }

    private void InitializeControl()
    {
      bool allowWrap = true;
      switch (this.style)
      {
        case TextViewStyles.DialogTitle:
          allowWrap = false;
          break;
        case TextViewStyles.SectionGroupTitle:
          allowWrap = false;
          break;
        case TextViewStyles.PageHeaderTitle:
          allowWrap = false;
          break;
      }
      this.InitializeComponent(allowWrap);
    }

    private void InitializeComponent(bool allowWrap)
    {
      this.textBlock = new TextBlock();
      this.Content = (object) this.textBlock;
      this.textBlock.TextWrapping = allowWrap ? TextWrapping.Wrap : TextWrapping.NoWrap;
      string fontInfoString;
      Brush brush;
      switch (this.style)
      {
        case TextViewStyles.DialogTitle:
          fontInfoString = "Arial,12,Bold";
          brush = (Brush) SkinConstants.BRUSH_TEXTVIEW_DIALOGTITLE;
          break;
        case TextViewStyles.MsgBoxInfoText:
          fontInfoString = "Arial,11,Normal";
          brush = (Brush) SkinConstants.BRUSH_TEXTVIEW_MESSAGEBOXINFOTEXT;
          break;
        case TextViewStyles.MsgBoxMessageText:
          fontInfoString = "Arial,11,Bold";
          brush = (Brush) SkinConstants.BRUSH_TEXTVIEW_MESSAGEBOXMESSAGETEXT;
          break;
        case TextViewStyles.SectionGroupTitle:
          fontInfoString = "Arial,11,Bold";
          brush = (Brush) SkinConstants.BRUSH_TEXTVIEW_SECTIONGROUPTITLE;
          break;
        case TextViewStyles.Label:
          fontInfoString = "Arial,11,Normal";
          brush = (Brush) SkinConstants.BRUSH_TEXTVIEW_LABEL;
          break;
        case TextViewStyles.DisplayText:
          fontInfoString = "Arial,11,Normal";
          brush = (Brush) SkinConstants.BRUSH_TEXTVIEW_TEXT;
          break;
        case TextViewStyles.PageHeaderTitle:
          fontInfoString = "Verdana,18,BoldItalic";
          brush = (Brush) SkinConstants.BRUSH_TEXTVIEW_PAGEHEADERTITLE;
          break;
        default:
          throw new NotImplementedException();
      }
      FontManager.Instance.SetFontStyle1(fontInfoString, this.textBlock);
      this.textBlock.Foreground = brush;
    }
  }
}
