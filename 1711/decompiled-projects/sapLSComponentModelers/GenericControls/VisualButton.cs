﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualButton
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualButton : Button
  {
    private static string RESOURCE_NAME_STYLE_STANDARD = "style_standard";
    private static string RESOURCE_NAME_STYLE_NEXT = "style_next";
    private static string RESOURCE_NAME_STYLE_PREVIOUS = "style_previous";
    private static string RESOURCE_NAME_STYLE_EMPHASIZED = "style_emphasized";
    private static string RESOURCE_NAME_STYLE_MENU = "style_menu";
    private static string RESOURCE_NAME_STYLE_TOGGLE = "style_toggle";
    private static string RESOURCE_NAME_STYLE_ACTION = "style_action";
    public const string DEFAULT_CONTROL_CONFIG_NAME = "com.sap.byd.canvas.buttoncontrol";
    private VisualButton.ButtonStyles buttonStyle;

    public VisualButton()
      : this(VisualButton.ButtonStyles.Standard, false)
    {
      this.Height = 20.0;
    }

    public VisualButton(VisualButton.ButtonStyles buttonStyle, bool handleFocus)
    {
      this.buttonStyle = buttonStyle;
      string resourceName;
      switch (buttonStyle)
      {
        case VisualButton.ButtonStyles.Standard:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_STANDARD;
          break;
        case VisualButton.ButtonStyles.Next:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_NEXT;
          break;
        case VisualButton.ButtonStyles.Previous:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_PREVIOUS;
          break;
        case VisualButton.ButtonStyles.Emphasized:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_EMPHASIZED;
          break;
        case VisualButton.ButtonStyles.Menu:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_MENU;
          break;
        case VisualButton.ButtonStyles.Toggle:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_TOGGLE;
          break;
        case VisualButton.ButtonStyles.Action:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_ACTION;
          break;
        default:
          throw new NotImplementedException();
      }
      if (resourceName == null)
        return;
      this.SetButtonStyle(resourceName);
    }

    public void SetStyle(VisualButton.ButtonStyles buttonStyle)
    {
      string resourceName;
      switch (buttonStyle)
      {
        case VisualButton.ButtonStyles.Standard:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_STANDARD;
          break;
        case VisualButton.ButtonStyles.Next:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_NEXT;
          break;
        case VisualButton.ButtonStyles.Previous:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_PREVIOUS;
          break;
        case VisualButton.ButtonStyles.Emphasized:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_EMPHASIZED;
          break;
        case VisualButton.ButtonStyles.Menu:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_MENU;
          break;
        case VisualButton.ButtonStyles.Toggle:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_TOGGLE;
          break;
        case VisualButton.ButtonStyles.Action:
          resourceName = VisualButton.RESOURCE_NAME_STYLE_ACTION;
          break;
        default:
          throw new NotImplementedException();
      }
      if (resourceName == null)
        return;
      this.SetButtonStyle(resourceName);
    }

    public void SetButtonStyle(string resourceName)
    {
      ResourceDictionary resourceDictionary = new ResourceDictionary();
      resourceDictionary.Source = new Uri("pack://application:,,,/sapLSUICore;component/XamlResource.xaml");
      if (resourceName == VisualButton.RESOURCE_NAME_STYLE_NEXT)
        this.Style = resourceDictionary[(object) "ButtonStyleNext"] as Style;
      else if (resourceName == VisualButton.RESOURCE_NAME_STYLE_PREVIOUS)
        this.Style = resourceDictionary[(object) "ButtonStylePrevious"] as Style;
      else if (resourceName == VisualButton.RESOURCE_NAME_STYLE_EMPHASIZED)
        this.Style = resourceDictionary[(object) "ButtonStyleEmphasised"] as Style;
      else if (resourceName == VisualButton.RESOURCE_NAME_STYLE_MENU)
        this.Style = resourceDictionary[(object) "ButtonStyleMenu"] as Style;
      else if (resourceName == VisualButton.RESOURCE_NAME_STYLE_TOGGLE)
        this.Style = resourceDictionary[(object) "ButtonStyleToggle"] as Style;
      else if (resourceName == VisualButton.RESOURCE_NAME_STYLE_ACTION)
        this.Style = resourceDictionary[(object) "ButtonStyleAction"] as Style;
      else
        this.Style = resourceDictionary[(object) "ButtonStyleStandard"] as Style;
    }

    ~VisualButton()
    {
    }

    public VisualButton.ButtonStyles ButtonStyle
    {
      get
      {
        return this.buttonStyle;
      }
    }

    protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
    {
      base.OnMouseLeftButtonDown(e);
      e.Handled = false;
    }

    public enum ButtonStyles
    {
      Standard,
      Next,
      Previous,
      Emphasized,
      Menu,
      Toggle,
      Action,
    }
  }
}
