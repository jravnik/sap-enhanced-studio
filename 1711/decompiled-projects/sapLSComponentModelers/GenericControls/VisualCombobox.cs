﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualCombobox
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualCombobox : UserControl
  {
    private VisualInputField visualControl;
    private Menu dropdownMenu;
    private List<object> items;
    private string displayMember;
    private int selectedIndex;
    private string selectedText;
    private object selectedValue;
    private Popup popupControl;
    private VisualComboBoxStyle comboboxStyle;

    public event RoutedEventHandler SelectionChanged;

    public VisualComboBoxStyle ComboboxStyle
    {
      get
      {
        return this.comboboxStyle;
      }
      set
      {
        this.comboboxStyle = value;
        this.visualControl.ReadOnly = this.comboboxStyle != VisualComboBoxStyle.ComboBox;
      }
    }

    public bool HasBorder
    {
      get
      {
        if (this.visualControl == null)
          return false;
        return this.visualControl.HasBorder;
      }
      set
      {
        if (this.visualControl == null)
          return;
        this.visualControl.HasBorder = value;
      }
    }

    public FieldStyles FieldStyle
    {
      get
      {
        return this.visualControl.FieldStyle;
      }
      set
      {
        this.visualControl.FieldStyle = value;
      }
    }

    public VisualCombobox()
      : this(true)
    {
    }

    public VisualCombobox(bool hasBorder)
    {
      this.items = new List<object>();
      this.displayMember = string.Empty;
      this.InitializeObject(hasBorder);
      this.ComboboxStyle = VisualComboBoxStyle.ComboBox;
    }

    private void InitializeObject(bool hasBorder)
    {
      this.visualControl = new VisualInputField(hasBorder);
      this.visualControl.IconStyle = IconStyles.Arrow;
      this.visualControl.ValueHelpRequested += new EventHandler<EventArgs>(this.visualControl_ValueHelpRequested);
      this.Content = (object) this.visualControl;
      this.popupControl = new Popup();
      this.popupControl.PlacementTarget = (UIElement) this.visualControl;
      this.popupControl.Placement = PlacementMode.Bottom;
      this.popupControl.VerticalOffset = 0.0;
      this.popupControl.HorizontalOffset = 0.5;
      this.AddLogicalChild((object) this.popupControl);
    }

    private void visualControl_ValueHelpRequested(object sender, EventArgs e)
    {
      if (this.items == null)
        return;
      this.dropdownMenu = new Menu();
      this.dropdownMenu.HorizontalContentAlignment = HorizontalAlignment.Center;
      this.dropdownMenu.VerticalContentAlignment = VerticalAlignment.Center;
      this.dropdownMenu.Background = (Brush) Brushes.White;
      foreach (object obj in this.items)
      {
        if (obj != null)
        {
          MenuItem menuItem = new MenuItem();
          menuItem.Header = (object) this.GetItemHeader(obj);
          menuItem.Tag = obj;
          menuItem.Width = this.ActualWidth;
          menuItem.Click += new RoutedEventHandler(this.menuitem_Click);
          this.dropdownMenu.Items.Add((object) menuItem);
        }
      }
      this.ShowPopupWindow();
    }

    private void menuitem_Click(object sender, RoutedEventArgs e)
    {
      MenuItem originalSource = e.OriginalSource as MenuItem;
      if (originalSource != null)
      {
        this.SelectedItem = originalSource.Tag;
        if (this.SelectionChanged != null)
          this.SelectionChanged((object) this, e);
      }
      this.ClosePopupWindow();
      e.Handled = true;
    }

    public int SelectedIndex
    {
      get
      {
        return this.selectedIndex;
      }
      set
      {
        if (value >= this.items.Count)
          throw new IndexOutOfRangeException();
        this.selectedIndex = value;
        this.selectedValue = this.items[this.selectedIndex];
        this.selectedText = this.GetItemHeader(this.selectedValue);
        this.visualControl.Text = this.selectedText;
      }
    }

    public object SelectedItem
    {
      get
      {
        return this.selectedValue;
      }
      set
      {
        if (!this.items.Contains(value))
          return;
        this.selectedValue = value;
        this.selectedIndex = this.items.IndexOf(this.selectedValue);
        this.selectedText = this.GetItemHeader(this.selectedValue);
        this.visualControl.Text = this.selectedText;
      }
    }

    public string SelectedText
    {
      get
      {
        return this.selectedText;
      }
      set
      {
        this.selectedText = value;
        this.visualControl.Text = this.selectedText;
      }
    }

    public List<object> Items
    {
      get
      {
        return this.items;
      }
      set
      {
        this.items = value;
      }
    }

    public string DisplayMember
    {
      get
      {
        return this.displayMember;
      }
      set
      {
        this.displayMember = value;
      }
    }

    private string GetItemHeader(object item)
    {
      try
      {
        if (this.displayMember != string.Empty)
        {
          PropertyInfo property = item.GetType().GetProperty(this.displayMember);
          if (property != (PropertyInfo) null)
          {
            object obj = property.GetValue(item, (object[]) null);
            if (obj != null)
              return obj.ToString();
          }
        }
        return item.ToString();
      }
      catch
      {
      }
      return string.Empty;
    }

    private void ShowPopupWindow()
    {
      Grid grid = new Grid();
      GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(grid, 1.0, GridUnitType.Star);
      GridUtil.PlaceElement(grid, (UIElement) this.dropdownMenu, 0, 0);
      this.popupControl.Width = this.ActualWidth - 1.0;
      this.popupControl.Child = (UIElement) grid;
      this.popupControl.IsOpen = true;
    }

    private void ClosePopupWindow()
    {
      if (this.popupControl == null)
        return;
      this.popupControl.IsOpen = false;
      foreach (MenuItem menuItem in (IEnumerable) this.dropdownMenu.Items)
        menuItem.Click -= new RoutedEventHandler(this.menuitem_Click);
      this.dropdownMenu = (Menu) null;
    }
  }
}
