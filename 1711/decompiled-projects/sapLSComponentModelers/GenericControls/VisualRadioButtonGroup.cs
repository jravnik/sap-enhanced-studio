﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualRadioButtonGroup
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualRadioButtonGroup : UserControl
  {
    private StackPanel stackPanel;
    private Orientation currentOrientation;
    private string displayMember;
    private List<object> items;
    private RadioButton selectedRadioButton;

    public List<object> Items
    {
      get
      {
        return this.items;
      }
      set
      {
        this.items = value;
        this.LoadItems();
      }
    }

    public Orientation Orientation
    {
      get
      {
        return this.currentOrientation;
      }
      set
      {
        this.currentOrientation = value;
        this.LoadItems();
      }
    }

    public string DisplayMember
    {
      get
      {
        return this.displayMember;
      }
      set
      {
      }
    }

    public VisualRadioButtonGroup()
      : this((List<object>) null, Orientation.Vertical)
    {
    }

    public VisualRadioButtonGroup(List<object> items, Orientation currentOrientation)
    {
      this.items = items != null ? items : new List<object>();
      this.currentOrientation = currentOrientation;
      this.stackPanel = new StackPanel();
      this.LoadItems();
      this.Content = (object) this.stackPanel;
    }

    private void LoadItems()
    {
      switch (this.currentOrientation)
      {
        case Orientation.Horizontal:
          this.stackPanel.Orientation = this.currentOrientation;
          break;
        case Orientation.Vertical:
          this.stackPanel.Orientation = this.currentOrientation;
          break;
      }
      this.selectedRadioButton = (RadioButton) null;
      this.stackPanel.Children.Clear();
      foreach (object obj in this.items)
      {
        RadioButton radioButton = new RadioButton();
        radioButton.Content = (object) obj.ToString();
        radioButton.Click += new RoutedEventHandler(this.radioButton_Click);
        radioButton.Margin = new Thickness(2.0);
        this.stackPanel.Children.Add((UIElement) radioButton);
        if (this.selectedRadioButton == null)
        {
          this.selectedRadioButton = radioButton;
          this.selectedRadioButton.IsChecked = new bool?(true);
        }
      }
    }

    private void radioButton_Click(object sender, RoutedEventArgs e)
    {
      if (sender != this.selectedRadioButton)
      {
        this.selectedRadioButton.IsChecked = new bool?(false);
        this.selectedRadioButton = sender as RadioButton;
        this.selectedRadioButton.IsChecked = new bool?(true);
      }
      e.Handled = true;
    }
  }
}
