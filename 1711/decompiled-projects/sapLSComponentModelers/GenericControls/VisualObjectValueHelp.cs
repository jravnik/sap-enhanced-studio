﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualObjectValueHelp
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualObjectValueHelp : UserControl
  {
    private VisualInputField visualControl;

    public FieldStyles FieldStyle
    {
      get
      {
        return this.visualControl.FieldStyle;
      }
      set
      {
        this.visualControl.FieldStyle = value;
      }
    }

    public bool HasBorder
    {
      get
      {
        if (this.visualControl == null)
          return false;
        return this.visualControl.HasBorder;
      }
      set
      {
        if (this.visualControl == null)
          return;
        this.visualControl.HasBorder = value;
      }
    }

    public VisualObjectValueHelp()
    {
      this.InitializeObject(true);
    }

    public VisualObjectValueHelp(bool hasBorder)
    {
      this.InitializeObject(hasBorder);
    }

    private void InitializeObject(bool hasBorder)
    {
      this.visualControl = new VisualInputField(hasBorder);
      this.visualControl.IconStyle = IconStyles.F4;
      this.Content = (object) this.visualControl;
    }
  }
}
