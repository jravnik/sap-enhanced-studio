﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.ControlFactory
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.Notes;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Notes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class ControlFactory
  {
    public static UIElement ConstructFormpaneControl(AbstractControl controlDefinition, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      FrameworkElement visualControl = (FrameworkElement) null;
      if (controlDefinition is InputField)
        visualControl = (FrameworkElement) new VisualInputField();
      else if (controlDefinition is FileUploadControl)
      {
        FileUploadControl fileUploadControl = controlDefinition as FileUploadControl;
        VisualButton visualButton = new VisualButton();
        visualButton.Content = (object) fileUploadControl.Text.Text;
        visualControl = (FrameworkElement) visualButton;
      }
      else if (controlDefinition is FileDownloadControl)
      {
        FileDownloadControl fileDownloadControl = controlDefinition as FileDownloadControl;
        if (fileDownloadControl.Style == FileDownloadControlStyleType.Button)
        {
          VisualButton visualButton = new VisualButton();
          visualButton.Content = (object) fileDownloadControl.Text.Text;
          visualControl = (FrameworkElement) visualButton;
        }
        else
          visualControl = (FrameworkElement) new VisualLink(VisualLink.LinkStyles.Standard)
          {
            Text = fileDownloadControl.Text.Text
          };
      }
      else if (controlDefinition is StaticText)
      {
        StaticText staticText = controlDefinition as StaticText;
        VisualStaticText visualStaticText = staticText.Format != FormatType.emphasized ? new VisualStaticText(false, false, false, false, VisualStaticText.StaticTextFormats.Standard) : new VisualStaticText(false, false, false, false, VisualStaticText.StaticTextFormats.Emphasized);
        visualStaticText.Text = staticText.Name != null ? staticText.Name : "Static Text";
        visualControl = (FrameworkElement) visualStaticText;
      }
      else if (controlDefinition is DropDownListBox)
        visualControl = (FrameworkElement) new VisualCombobox()
        {
          ComboboxStyle = VisualComboBoxStyle.ListBox
        };
      else if (controlDefinition is ObjectValueSelector)
        visualControl = controlDefinition.CCTSType == UXCCTSTypes.date || controlDefinition.CCTSType == UXCCTSTypes.datetime ? (FrameworkElement) new VisualCalendar() : (FrameworkElement) new VisualObjectValueHelp();
      else if (controlDefinition is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.CheckBox)
      {
        System.Windows.Controls.CheckBox checkBox = new System.Windows.Controls.CheckBox();
        checkBox.HorizontalAlignment = HorizontalAlignment.Left;
        checkBox.VerticalAlignment = VerticalAlignment.Center;
        visualControl = (FrameworkElement) checkBox;
      }
      else if (controlDefinition is Link)
      {
        Link link = controlDefinition as Link;
        visualControl = (FrameworkElement) new VisualStaticText(false, false, true, false, VisualStaticText.StaticTextFormats.Standard)
        {
          IsLink = true,
          IsMenuVisible = link.HasMenu,
          Text = (link.Name != null ? link.Name : "Link")
        };
      }
      else if (controlDefinition is LineSeparator)
        visualControl = (FrameworkElement) new VisualLineSeparator();
      else if (controlDefinition is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button)
      {
        VisualButton visualButton = new VisualButton();
        SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button button = controlDefinition as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button;
        visualButton.Content = button.Text != null ? (object) button.Text.Text : (object) string.Empty;
        visualControl = (FrameworkElement) visualButton;
      }
      else
      {
        if (controlDefinition is CompoundField)
          return (UIElement) new VisualCompoundField(controlDefinition as CompoundField, usage);
        if (controlDefinition is TextEditControl)
          visualControl = (FrameworkElement) new VisualTextEditControl((controlDefinition as TextEditControl).Rows, true, true);
        else if (controlDefinition is RichTextEditControl)
          visualControl = (FrameworkElement) new VisualTextEditControl(3, true, true);
        else if (controlDefinition is CustomControl)
          visualControl = (FrameworkElement) new VisualCustomControl();
        else if (controlDefinition is AvailabilityCalendarCellControl)
          visualControl = (FrameworkElement) new VisualAvailabilityCalendarCellControl(false, false, false, false, VisualStaticText.StaticTextFormats.Standard);
        else if (controlDefinition is RadioButtonGroup)
          visualControl = (FrameworkElement) new VisualRadioButtonGroup(new List<object>()
          {
            (object) "Code",
            (object) "Code1"
          }, (Orientation) Enum.Parse(typeof (Orientation), (controlDefinition as RadioButtonGroup).Orientation.ToString(), true));
        else if (controlDefinition is Icon)
        {
          System.Windows.Controls.Image image = new System.Windows.Controls.Image();
          image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.Icon);
          image.HorizontalAlignment = HorizontalAlignment.Left;
          image.VerticalAlignment = VerticalAlignment.Center;
          image.Stretch = Stretch.None;
          visualControl = (FrameworkElement) image;
        }
        else if (controlDefinition is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Image)
        {
          System.Windows.Controls.Image image = new System.Windows.Controls.Image();
          image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.Image);
          image.HorizontalAlignment = HorizontalAlignment.Left;
          image.VerticalAlignment = VerticalAlignment.Center;
          image.Stretch = Stretch.None;
          visualControl = (FrameworkElement) image;
        }
        else if (controlDefinition is ProgressBarControl)
        {
          System.Windows.Controls.Image image = new System.Windows.Controls.Image();
          image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.Progress_Bar);
          image.HorizontalAlignment = HorizontalAlignment.Left;
          image.VerticalAlignment = VerticalAlignment.Center;
          image.Stretch = Stretch.None;
          visualControl = (FrameworkElement) image;
        }
        else if (controlDefinition is MediaPlayerControl)
        {
          System.Windows.Controls.Image image = new System.Windows.Controls.Image();
          image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.MediaPlayer);
          image.HorizontalAlignment = HorizontalAlignment.Left;
          image.VerticalAlignment = VerticalAlignment.Center;
          image.Stretch = Stretch.None;
          visualControl = (FrameworkElement) image;
        }
        else if (controlDefinition is RatingControl)
        {
          System.Windows.Controls.Image image = new System.Windows.Controls.Image();
          image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.RatingControl);
          image.HorizontalAlignment = HorizontalAlignment.Left;
          image.VerticalAlignment = VerticalAlignment.Center;
          image.Stretch = Stretch.None;
          visualControl = (FrameworkElement) image;
        }
        else if (controlDefinition is LegendControl)
        {
          TextBlock textBlock = new TextBlock();
          textBlock.Text = "Legend Control";
          textBlock.VerticalAlignment = VerticalAlignment.Center;
          textBlock.HorizontalAlignment = HorizontalAlignment.Center;
          visualControl = (FrameworkElement) textBlock;
        }
        else if (controlDefinition is TagControl)
          visualControl = (FrameworkElement) new VisualTagControl(controlDefinition as TagControl);
        else if (controlDefinition is FlagControl)
          visualControl = (FrameworkElement) new VisualFlagControl(controlDefinition as FlagControl);
        else if (controlDefinition is FlagStatusControl)
          visualControl = (FrameworkElement) new VisualFlagStatusControl(controlDefinition as FlagStatusControl);
        else if (controlDefinition is FavoriteControl)
          visualControl = (FrameworkElement) new VisualFavoriteControl(controlDefinition as FavoriteControl);
        else if (controlDefinition is FavoriteStatusControl)
          visualControl = (FrameworkElement) new VisualFavoriteStatusControl(controlDefinition as FavoriteStatusControl);
        else if (controlDefinition is LayoutContainerControl)
          visualControl = ControlFactory.ConstructLayoutElementControl((controlDefinition as LayoutContainerControl).Content) as FrameworkElement;
        else if (controlDefinition is DatetimeInputField)
          visualControl = (FrameworkElement) new VisualInputField();
        else if (controlDefinition is SelectControl)
          visualControl = (FrameworkElement) new VisualCombobox()
          {
            ComboboxStyle = VisualComboBoxStyle.ListBox
          };
        else if (controlDefinition is ObjectNumber)
          visualControl = (FrameworkElement) new VisualStaticText(false, false, false, false, VisualStaticText.StaticTextFormats.Standard);
        else if (controlDefinition is CalendarControl)
          visualControl = (FrameworkElement) new VisualCalendar();
        else if (controlDefinition is CalendarControlLegend)
        {
          TextBlock textBlock = new TextBlock();
          textBlock.Text = "Legend Control";
          textBlock.VerticalAlignment = VerticalAlignment.Center;
          textBlock.HorizontalAlignment = HorizontalAlignment.Center;
          visualControl = (FrameworkElement) textBlock;
        }
        else if (controlDefinition is UnknownControl)
        {
          TextBlock textBlock = new TextBlock();
          textBlock.Text = "Unknown Control";
          textBlock.VerticalAlignment = VerticalAlignment.Center;
          textBlock.HorizontalAlignment = HorizontalAlignment.Center;
          visualControl = (FrameworkElement) textBlock;
        }
      }
      if (visualControl != null)
      {
        if (string.IsNullOrEmpty(controlDefinition.FieldWidth))
        {
          ControlFactory.SetDefaultCCTSWidth(visualControl, controlDefinition, enclosedCCTSType, usage);
        }
        else
        {
          double widthValue = ControlFactory.GetWidthValue(controlDefinition.FieldWidth);
          visualControl.Width = widthValue;
        }
        if (controlDefinition is EditControl)
          ControlFactory.SetEditableProperties((UIElement) visualControl, controlDefinition);
      }
      return (UIElement) visualControl;
    }

    public static void SetDefaultCCTSWidth(FrameworkElement visualControl, AbstractControl item, UXCCTSTypes enclosedCCTSType, UsageType usage)
    {
      if (visualControl == null)
        return;
      FieldWidth fieldWidth = CCTSFormatter.Instance.ResolveWidth(item, enclosedCCTSType, usage);
      if (fieldWidth == null)
        return;
      double widthValue = ControlFactory.GetWidthValue(fieldWidth.DefaultWidth);
      visualControl.Width = widthValue;
    }

    public static double GetWidthValue(string width)
    {
      if (!string.IsNullOrEmpty(width))
      {
        width = FieldWidths.Translate(width);
        string nonDigitPart = Utilities.GetNonDigitPart(width);
        string numericPart = Utilities.GetNumericPart(width);
        if (numericPart != string.Empty)
        {
          double num = Convert.ToDouble(numericPart);
          switch (nonDigitPart)
          {
            case "ex":
              return num * 6.0;
            case "px":
              return num;
            case "%":
              if (num < 1.0)
                num = 1.0;
              if (num > 100.0)
                num = 100.0;
              return num;
          }
        }
      }
      return 0.0;
    }

    public static void SetEditableProperties(UIElement control, AbstractControl controlDefinition)
    {
      EditControl editControl = controlDefinition as EditControl;
      if (control is VisualInputField)
      {
        VisualInputField visualInputField = control as VisualInputField;
        if (Converter.ToBoolean((DependentProperty) editControl.ReadOnly, false))
        {
          visualInputField.IsEnabled = false;
          visualInputField.FieldStyle = FieldStyles.Readonly;
        }
        else if (!Converter.ToBoolean((DependentProperty) editControl.Enabled, true))
        {
          visualInputField.IsEnabled = false;
          visualInputField.FieldStyle = FieldStyles.Disabled;
        }
        else
        {
          visualInputField.IsEnabled = true;
          visualInputField.FieldStyle = FieldStyles.Editable;
        }
      }
      else if (control is VisualCombobox)
      {
        VisualCombobox visualCombobox = control as VisualCombobox;
        if (Converter.ToBoolean((DependentProperty) editControl.ReadOnly, false))
        {
          visualCombobox.IsEnabled = false;
          visualCombobox.FieldStyle = FieldStyles.Readonly;
        }
        else if (!Converter.ToBoolean((DependentProperty) editControl.Enabled, true))
        {
          visualCombobox.IsEnabled = false;
          visualCombobox.FieldStyle = FieldStyles.Disabled;
        }
        else
        {
          visualCombobox.IsEnabled = true;
          visualCombobox.FieldStyle = FieldStyles.Editable;
        }
      }
      else if (control is VisualRadioButtonGroup)
      {
        VisualRadioButtonGroup radioButtonGroup = control as VisualRadioButtonGroup;
        if (Converter.ToBoolean((DependentProperty) editControl.ReadOnly, false) || !Converter.ToBoolean((DependentProperty) editControl.Enabled, true))
          radioButtonGroup.IsEnabled = false;
        else
          radioButtonGroup.IsEnabled = true;
      }
      else if (control is System.Windows.Controls.CheckBox)
      {
        System.Windows.Controls.CheckBox checkBox = control as System.Windows.Controls.CheckBox;
        if (Converter.ToBoolean((DependentProperty) editControl.ReadOnly, false) || !Converter.ToBoolean((DependentProperty) editControl.Enabled, true))
          checkBox.IsEnabled = false;
        else
          checkBox.IsEnabled = true;
      }
      else if (control is VisualTextEditControl)
      {
        VisualTextEditControl visualTextEditControl = control as VisualTextEditControl;
        if (Converter.ToBoolean((DependentProperty) editControl.ReadOnly, false))
        {
          visualTextEditControl.IsEnabled = false;
          visualTextEditControl.FieldStyle = FieldStyles.Readonly;
        }
        else if (!Converter.ToBoolean((DependentProperty) editControl.Enabled, true))
        {
          visualTextEditControl.IsEnabled = false;
          visualTextEditControl.FieldStyle = FieldStyles.Disabled;
        }
        else
        {
          visualTextEditControl.IsEnabled = true;
          visualTextEditControl.FieldStyle = FieldStyles.Editable;
        }
      }
      else if (control is VisualCalculator)
      {
        VisualCalculator visualCalculator = control as VisualCalculator;
        if (Converter.ToBoolean((DependentProperty) editControl.ReadOnly, false))
        {
          visualCalculator.IsEnabled = false;
          visualCalculator.FieldStyle = FieldStyles.Readonly;
        }
        else if (!Converter.ToBoolean((DependentProperty) editControl.Enabled, true))
        {
          visualCalculator.IsEnabled = false;
          visualCalculator.FieldStyle = FieldStyles.Disabled;
        }
        else
        {
          visualCalculator.IsEnabled = true;
          visualCalculator.FieldStyle = FieldStyles.Editable;
        }
      }
      else if (control is VisualObjectValueHelp)
      {
        VisualObjectValueHelp visualObjectValueHelp = control as VisualObjectValueHelp;
        if (Converter.ToBoolean((DependentProperty) editControl.ReadOnly, false))
        {
          visualObjectValueHelp.IsEnabled = false;
          visualObjectValueHelp.FieldStyle = FieldStyles.Readonly;
        }
        else if (!Converter.ToBoolean((DependentProperty) editControl.Enabled, true))
        {
          visualObjectValueHelp.IsEnabled = false;
          visualObjectValueHelp.FieldStyle = FieldStyles.Disabled;
        }
        else
        {
          visualObjectValueHelp.IsEnabled = true;
          visualObjectValueHelp.FieldStyle = FieldStyles.Editable;
        }
      }
      else
      {
        if (!(control is VisualCalendar))
          return;
        VisualCalendar visualCalendar = control as VisualCalendar;
        if (Converter.ToBoolean((DependentProperty) editControl.ReadOnly, false))
        {
          visualCalendar.IsEnabled = false;
          visualCalendar.FieldStyle = FieldStyles.Readonly;
        }
        else if (!Converter.ToBoolean((DependentProperty) editControl.Enabled, true))
        {
          visualCalendar.IsEnabled = false;
          visualCalendar.FieldStyle = FieldStyles.Disabled;
        }
        else
        {
          visualCalendar.IsEnabled = true;
          visualCalendar.FieldStyle = FieldStyles.Editable;
        }
      }
    }

    public static UIElement ConstructLayoutElementControl(LayoutElement layoutElement)
    {
      FrameworkElement frameworkElement = (FrameworkElement) null;
      try
      {
        if (layoutElement is LayoutGrid)
          frameworkElement = (FrameworkElement) new VisualLayoutGrid((IModelObject) layoutElement);
        else if (layoutElement is LayoutStackPanel)
          frameworkElement = (FrameworkElement) new VisualLayoutStackPanel((IModelObject) layoutElement);
        else if (layoutElement is LayoutControl)
          frameworkElement = (FrameworkElement) new VisualLayoutControl((IModelObject) layoutElement);
        else if (layoutElement is LayoutBorder)
          frameworkElement = (FrameworkElement) new VisualLayoutBorderControl((IModelObject) layoutElement);
        else if (layoutElement is Iterator)
          frameworkElement = (FrameworkElement) new VisualIteratorControl((IModelObject) layoutElement);
        else if (layoutElement is LayoutToolbar)
          frameworkElement = (FrameworkElement) new VisualLayoutToolbar((IModelObject) layoutElement);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
      return (UIElement) frameworkElement;
    }
  }
}
