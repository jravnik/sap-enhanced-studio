﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualStaticText
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualStaticText : BaseStyledControl
  {
    private static ImageInfo menuItemImageInfo = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Icons/icon_menu.png,10,10");
    private const string RESOURCE_BOLDTEXT_FONT = "Arial,11,Bold";
    private const string RESOURCE_NORMALTEXT_FONT = "Arial,11,Normal";
    private Grid contentGrid;
    private TextBlock textBlock;
    private VisualStaticText.StaticTextFormats format;
    private FieldStyles fieldStyle;
    private bool isLink;
    private bool isLinkEventsInitialized;
    private bool isHovering;
    private Image menuImage;

    public VisualStaticText(bool allowWrap, bool isCellEditor, bool hasMenu, bool isRightAligned, VisualStaticText.StaticTextFormats format)
      : base(isCellEditor)
    {
      this.format = format;
      this.fieldStyle = FieldStyles.Unknown;
      this.contentGrid = new Grid();
      this.textBlock = new TextBlock();
      this.textBlock.Margin = isCellEditor ? new Thickness(2.0, 3.0, 2.0, 2.0) : new Thickness(0.0, 2.0, 0.0, 0.0);
      this.textBlock.TextWrapping = allowWrap ? TextWrapping.Wrap : TextWrapping.NoWrap;
      this.textBlock.VerticalAlignment = VerticalAlignment.Center;
      this.textBlock.HorizontalAlignment = isRightAligned ? HorizontalAlignment.Right : HorizontalAlignment.Left;
      if (hasMenu)
      {
        StackPanel stackPanel = new StackPanel();
        stackPanel.Orientation = Orientation.Horizontal;
        stackPanel.Children.Add((UIElement) this.textBlock);
        this.menuImage = ResourceUtil.GetImage(VisualStaticText.menuItemImageInfo);
        this.menuImage.VerticalAlignment = VerticalAlignment.Bottom;
        this.menuImage.Margin = new Thickness(4.0, 0.0, 1.0, 2.0);
        this.menuImage.Cursor = Cursors.Hand;
        stackPanel.Children.Add((UIElement) this.menuImage);
        this.contentGrid.Children.Add((UIElement) stackPanel);
      }
      else
        this.contentGrid.Children.Add((UIElement) this.textBlock);
      if (format == VisualStaticText.StaticTextFormats.Emphasized)
        FontManager.Instance.SetFontStyle1("Arial,11,Bold", this.textBlock);
      else
        FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.textBlock);
      this.Content = (object) this.contentGrid;
    }

    public UIElement MenuIconElement
    {
      get
      {
        return (UIElement) this.menuImage;
      }
    }

    public bool IsRightAligned
    {
      get
      {
        return this.textBlock.HorizontalAlignment == HorizontalAlignment.Right;
      }
      set
      {
        this.textBlock.HorizontalAlignment = value ? HorizontalAlignment.Right : HorizontalAlignment.Left;
      }
    }

    public bool IsMenuVisible
    {
      get
      {
        if (this.menuImage == null)
          return false;
        return this.menuImage.Visibility == Visibility.Visible;
      }
      set
      {
        if (this.menuImage == null)
          return;
        this.menuImage.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
      }
    }

    public bool IsLink
    {
      get
      {
        return this.isLink;
      }
      set
      {
        this.isLink = value;
        if (this.isLink && !this.isLinkEventsInitialized)
        {
          this.isLinkEventsInitialized = true;
          this.textBlock.MouseEnter += new MouseEventHandler(this.textBlock_MouseEnter);
          this.textBlock.MouseLeave += new MouseEventHandler(this.textBlock_MouseLeave);
        }
        this.UpdateForeground();
      }
    }

    public VisualStaticText.StaticTextFormats StaticTextFormat
    {
      get
      {
        return this.format;
      }
      set
      {
        this.format = value;
        if (this.format == VisualStaticText.StaticTextFormats.Emphasized)
          FontManager.Instance.SetFontStyle1("Arial,11,Bold", this.textBlock);
        else
          FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.textBlock);
      }
    }

    private void textBlock_MouseLeave(object sender, MouseEventArgs e)
    {
      this.isHovering = false;
      this.UpdateForeground();
    }

    private void textBlock_MouseEnter(object sender, MouseEventArgs e)
    {
      this.isHovering = true;
      this.UpdateForeground();
    }

    public string Text
    {
      get
      {
        return this.textBlock.Text;
      }
      set
      {
        this.textBlock.Text = value;
      }
    }

    private void UpdateForeground()
    {
      if (this.textBlock == null)
        return;
      if (this.IsEnabled && this.isLink)
      {
        this.textBlock.Foreground = this.isHovering ? BaseStyledControl.linkHoverBrush : BaseStyledControl.linkStandardBrush;
        if (this.textBlock.Cursor == Cursors.Hand)
          return;
        this.textBlock.Cursor = Cursors.Hand;
      }
      else
      {
        this.textBlock.Foreground = BaseStyledControl.GetForegroundBrush(this.fieldStyle);
        if (this.textBlock.Cursor == Cursors.Arrow)
          return;
        this.textBlock.Cursor = Cursors.Arrow;
      }
    }

    public FieldStyles FieldStyle
    {
      get
      {
        return this.fieldStyle;
      }
      set
      {
        if (this.fieldStyle == value)
          return;
        this.fieldStyle = value;
        this.UpdateForeground();
        if (this.contentGrid == null || !this.IsCellEditor)
          return;
        this.contentGrid.Background = BaseStyledControl.GetBackgroundBrush(this.fieldStyle);
      }
    }

    public enum StaticTextFormats
    {
      Standard,
      Emphasized,
    }
  }
}
