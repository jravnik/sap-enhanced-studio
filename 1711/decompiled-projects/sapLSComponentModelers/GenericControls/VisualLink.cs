﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualLink
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualLink : UserControl
  {
    private static SolidColorBrush[] brushEnabled = new SolidColorBrush[3];
    private static SolidColorBrush[] brushHover = new SolidColorBrush[3];
    private static SolidColorBrush[] brushDisabled = new SolidColorBrush[3];
    private static string[] fontSettingDisabled = new string[3];
    private static string[] fontSettingEnabled = new string[3];
    private static string[] fontSettingHover = new string[3];
    private string currentFontSettings;
    private SolidColorBrush currentBrush;
    private VisualLink.LinkStyles style;
    private TextBlock textBlock;
    private Grid linkGrid;
    private bool isEnabled;
    private bool isHover;
    private bool isFocused;

    public event EventHandler<EventArgs> LinkClicked;

    static VisualLink()
    {
      int index1 = 0;
      VisualLink.brushEnabled[index1] = SkinConstants.BRUSH_TOOLBARLINK_STANDARD;
      VisualLink.brushHover[index1] = SkinConstants.BRUSH_TOOLBARLINK_HOVER;
      VisualLink.brushDisabled[index1] = SkinConstants.BRUSH_TOOLBARLINK_DISABLED;
      VisualLink.fontSettingHover[index1] = "Arial,11,NormalUnderline";
      VisualLink.fontSettingDisabled[index1] = "Arial,11,Normal";
      VisualLink.fontSettingEnabled[index1] = "Arial,11,Normal";
      int index2 = 1;
      VisualLink.brushEnabled[index2] = SkinConstants.BRUSH_LARGECANVASLINK_STANDRAD;
      VisualLink.brushHover[index2] = SkinConstants.BRUSH_LARGECANVASLINK_HOVER;
      VisualLink.brushDisabled[index2] = SkinConstants.BRUSH_LARGECANVASLINK_DISABLED;
      VisualLink.fontSettingHover[index2] = "Arial,14,NormalUnderline";
      VisualLink.fontSettingDisabled[index2] = "Arial,14,Normal";
      VisualLink.fontSettingEnabled[index2] = "Arial,14,Normal";
      int index3 = 2;
      VisualLink.brushEnabled[index3] = SkinConstants.BRUSH_LINK_STANDRAD;
      VisualLink.brushHover[index3] = SkinConstants.BRUSH_LINK_HOVER;
      VisualLink.brushDisabled[index3] = SkinConstants.BRUSH_LINK_DISABLED;
      VisualLink.fontSettingHover[index3] = "Arial,11,NormalUnderline";
      VisualLink.fontSettingDisabled[index3] = "Arial,11,Normal";
      VisualLink.fontSettingEnabled[index3] = "Arial,11,Normal";
    }

    public VisualLink(VisualLink.LinkStyles style)
    {
      this.style = style;
      this.isEnabled = true;
      this.isHover = false;
      this.isFocused = false;
      this.linkGrid = new Grid();
      GridUtil.AddColumnDef(this.linkGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.linkGrid, 1.0, GridUnitType.Star);
      this.textBlock = new TextBlock();
      this.textBlock.Margin = new Thickness(0.0, 1.0, 3.0, 1.0);
      this.textBlock.VerticalAlignment = VerticalAlignment.Center;
      this.textBlock.Cursor = Cursors.Hand;
      GridUtil.PlaceElement(this.linkGrid, (UIElement) this.textBlock, 0, 0);
      this.Content = (object) this.linkGrid;
      this.IsTabStop = true;
      this.textBlock.MouseEnter += new MouseEventHandler(this.textBlock_MouseEnter);
      this.textBlock.GotFocus += new RoutedEventHandler(this.textBlock_GotFocus);
      this.textBlock.MouseLeave += new MouseEventHandler(this.textBlock_MouseLeave);
      this.textBlock.LostFocus += new RoutedEventHandler(this.textBlock_LostFocus);
      this.textBlock.MouseLeftButtonDown += new MouseButtonEventHandler(this.textBlock_MouseLeftButtonDown);
      this.UpdateComponentStyle();
    }

    private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      if (!this.isEnabled)
        return;
      this.isHover = false;
      this.UpdateComponentStyle();
      if (this.LinkClicked == null)
        return;
      this.LinkClicked((object) this, EventArgs.Empty);
    }

    private void textBlock_LostFocus(object sender, RoutedEventArgs e)
    {
      this.isFocused = false;
      this.UpdateComponentStyle();
    }

    private void textBlock_MouseLeave(object sender, MouseEventArgs e)
    {
      this.isHover = false;
      this.UpdateComponentStyle();
    }

    private void textBlock_GotFocus(object sender, RoutedEventArgs e)
    {
      this.isFocused = true;
      this.UpdateComponentStyle();
    }

    private void textBlock_MouseEnter(object sender, MouseEventArgs e)
    {
      this.isHover = true;
      this.UpdateComponentStyle();
    }

    public string Text
    {
      get
      {
        return this.textBlock.Text;
      }
      set
      {
        this.textBlock.Text = value;
      }
    }

    public HorizontalAlignment TextHorizontalAlignment
    {
      get
      {
        return this.textBlock.HorizontalAlignment;
      }
      set
      {
        this.textBlock.HorizontalAlignment = value;
      }
    }

    public TextWrapping TextWrapping
    {
      get
      {
        return this.textBlock.TextWrapping;
      }
      set
      {
        this.textBlock.TextWrapping = value;
      }
    }

    public VerticalAlignment TextVerticalAlignment
    {
      get
      {
        return this.textBlock.VerticalAlignment;
      }
      set
      {
        this.textBlock.VerticalAlignment = value;
      }
    }

    public Thickness TextPadding
    {
      get
      {
        return this.textBlock.Padding;
      }
      set
      {
        this.textBlock.Padding = value;
      }
    }

    private void UpdateComponentStyle()
    {
      int style = (int) this.style;
      SolidColorBrush solidColorBrush;
      string str;
      if (!this.isEnabled)
      {
        solidColorBrush = VisualLink.brushDisabled[style];
        str = VisualLink.fontSettingDisabled[style];
      }
      else if (this.isHover || this.isFocused)
      {
        solidColorBrush = VisualLink.brushHover[style];
        str = VisualLink.fontSettingHover[style];
      }
      else
      {
        solidColorBrush = VisualLink.brushEnabled[style];
        str = VisualLink.fontSettingEnabled[style];
      }
      if (solidColorBrush != this.currentBrush)
      {
        this.currentBrush = solidColorBrush;
        this.textBlock.Foreground = (Brush) this.currentBrush;
      }
      if (!(str != this.currentFontSettings))
        return;
      this.currentFontSettings = str;
      FontManager.Instance.SetFontStyle1(this.currentFontSettings, this.textBlock);
    }

    protected override Size MeasureOverride(Size availableSize)
    {
      this.textBlock.Measure(availableSize);
      return this.textBlock.DesiredSize;
    }

    public new bool IsEnabled
    {
      get
      {
        return this.isEnabled;
      }
      set
      {
        this.isEnabled = value;
        this.IsTabStop = value;
        this.UpdateComponentStyle();
      }
    }

    public enum LinkStyles
    {
      Toolbar,
      LargeCanvas,
      Standard,
    }
  }
}
