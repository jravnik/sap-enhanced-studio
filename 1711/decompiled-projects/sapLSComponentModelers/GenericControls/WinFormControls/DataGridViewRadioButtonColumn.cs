﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.WinFormControls.DataGridViewRadioButtonColumn
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.WinFormControls
{
  public class DataGridViewRadioButtonColumn : DataGridViewColumn
  {
    private IContainer components;

    public DataGridViewRadioButtonColumn()
      : base((DataGridViewCell) new RadioButtonCell())
    {
      this.InitializeComponent();
    }

    public override DataGridViewCell CellTemplate
    {
      get
      {
        return base.CellTemplate;
      }
      set
      {
        if (value != null && !value.GetType().IsAssignableFrom(typeof (RadioButtonCell)))
          throw new InvalidCastException("Must be a RadioButtonCell");
        base.CellTemplate = value;
      }
    }

    public DataGridViewRadioButtonColumn(IContainer container)
    {
      container.Add((IComponent) this);
      this.InitializeComponent();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
    }
  }
}
