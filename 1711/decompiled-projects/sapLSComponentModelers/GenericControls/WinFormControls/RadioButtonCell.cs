﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.WinFormControls.RadioButtonCell
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.WinFormControls
{
  public class RadioButtonCell : DataGridViewTextBoxCell
  {
    private RadioButtonState rButtonState = RadioButtonState.CheckedNormal;

    protected override void OnDataGridViewChanged()
    {
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
      base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, formattedValue, (object) errorText, errorText, cellStyle, advancedBorderStyle, paintParts);
      this.ComputePaint(graphics, clipBounds, cellBounds, rowIndex, cellState, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts, true);
    }

    private void ComputePaint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts, bool paint)
    {
      Rectangle rectangle1 = this.BorderWidths(advancedBorderStyle);
      Rectangle rectangle2 = cellBounds;
      rectangle2.Offset(rectangle1.X, rectangle1.Y);
      rectangle2.Width -= rectangle1.Right;
      rectangle2.Height -= rectangle1.Bottom;
      try
      {
        Point glyphLocation = new Point(rectangle2.Left + 2, rectangle2.Top + 2);
        Rectangle textBounds = new Rectangle(rectangle2.Left + 15, rectangle2.Top + 2, rectangle2.Width - 4, cellStyle.Font.Height + 1);
        TextFormatFlags flags = TextFormatFlags.EndEllipsis | TextFormatFlags.NoPrefix | TextFormatFlags.SingleLine | TextFormatFlags.PreserveGraphicsClipping;
        this.rButtonState = !(this.EditedFormattedValue.ToString() == "True") ? RadioButtonState.UncheckedNormal : RadioButtonState.CheckedNormal;
        RadioButtonRenderer.DrawRadioButton(graphics, glyphLocation, textBounds, " ", cellStyle.Font, flags, false, this.rButtonState);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
    {
      base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
      if (this.EditedFormattedValue.ToString() == "True")
        this.rButtonState = RadioButtonState.CheckedNormal;
      else
        this.rButtonState = RadioButtonState.UncheckedNormal;
    }

    public override Type EditType
    {
      get
      {
        return typeof (RadioButtonEditingControl);
      }
    }

    public override Type ValueType
    {
      get
      {
        return typeof (bool);
      }
    }

    public override object DefaultNewRowValue
    {
      get
      {
        return (object) false;
      }
    }
  }
}
