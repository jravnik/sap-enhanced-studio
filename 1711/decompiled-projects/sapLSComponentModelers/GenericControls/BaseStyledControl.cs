﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.BaseStyledControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class BaseStyledControl : UserControl
  {
    private static Dictionary<FieldStyles, Brush> borderBrushes = new Dictionary<FieldStyles, Brush>();
    private static Dictionary<FieldStyles, Brush> backgroundBrushes = new Dictionary<FieldStyles, Brush>();
    private static Dictionary<FieldStyles, Brush> foregroundBrushes = new Dictionary<FieldStyles, Brush>();
    protected static Brush linkHoverBrush;
    protected static Brush linkStandardBrush;
    private bool isCellEditor;

    static BaseStyledControl()
    {
      ResourceDictionary resources = new VisualInputField(false).Resources;
      BaseStyledControl.linkStandardBrush = (Brush) resources[(object) "link_standard"];
      BaseStyledControl.linkHoverBrush = (Brush) resources[(object) "link_hover"];
      BaseStyledControl.borderBrushes[FieldStyles.Editable] = (Brush) resources[(object) "inputfield_not_focused_editable_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.Editable] = (Brush) resources[(object) "inputfield_not_focused_editable_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.Editable] = (Brush) resources[(object) "inputfield_not_focused_editable_text"];
      BaseStyledControl.borderBrushes[FieldStyles.Readonly] = (Brush) resources[(object) "inputfield_not_focused_read_only_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.Readonly] = (Brush) resources[(object) "inputfield_not_focused_read_only_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.Readonly] = (Brush) resources[(object) "inputfield_not_focused_editable_text"];
      BaseStyledControl.borderBrushes[FieldStyles.Disabled] = (Brush) resources[(object) "inputfield_not_focused_disabled_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.Disabled] = (Brush) resources[(object) "inputfield_not_focused_disabled_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.Disabled] = (Brush) resources[(object) "inputfield_not_focused_disabled_text"];
      BaseStyledControl.borderBrushes[FieldStyles.EditableFocused] = (Brush) resources[(object) "inputfield_focused_editable_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.EditableFocused] = (Brush) resources[(object) "inputfield_focused_editable_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.EditableFocused] = (Brush) resources[(object) "inputfield_focused_editable_text"];
      BaseStyledControl.borderBrushes[FieldStyles.Error] = (Brush) resources[(object) "inputfield_not_focused_error_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.Error] = (Brush) resources[(object) "inputfield_not_focused_error_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.Error] = (Brush) resources[(object) "inputfield_not_focused_error_text"];
      BaseStyledControl.borderBrushes[FieldStyles.ErrorReadonly] = (Brush) resources[(object) "inputfield_not_focused_error_read_only_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.ErrorReadonly] = (Brush) resources[(object) "inputfield_not_focused_error_read_only_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.ErrorReadonly] = (Brush) resources[(object) "inputfield_not_focused_error_read_only_text"];
      BaseStyledControl.borderBrushes[FieldStyles.Warning] = (Brush) resources[(object) "inputfield_not_focused_warning_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.Warning] = (Brush) resources[(object) "inputfield_not_focused_warning_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.Warning] = (Brush) resources[(object) "inputfield_not_focused_warning_text"];
      BaseStyledControl.borderBrushes[FieldStyles.WarningReadonly] = (Brush) resources[(object) "inputfield_warning_read_only_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.WarningReadonly] = (Brush) resources[(object) "inputfield_warning_read_only_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.WarningReadonly] = (Brush) resources[(object) "inputfield_warning_read_only_text"];
      BaseStyledControl.borderBrushes[FieldStyles.CellEditorLeadSel] = (Brush) resources[(object) "inputfield_celledit_leadselected_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.CellEditorLeadSel] = (Brush) resources[(object) "inputfield_celledit_leadselected_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.CellEditorLeadSel] = (Brush) resources[(object) "inputfield_celledit_leadselected_text"];
      BaseStyledControl.borderBrushes[FieldStyles.CellEditorReadOnlyLeadSel] = (Brush) resources[(object) "inputfield_celledit_ro_leadselected_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.CellEditorReadOnlyLeadSel] = (Brush) resources[(object) "inputfield_celledit_ro_leadselected_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.CellEditorReadOnlyLeadSel] = (Brush) resources[(object) "inputfield_celledit_ro_leadselected_text"];
      BaseStyledControl.borderBrushes[FieldStyles.CellEditorReadOnlySel] = (Brush) resources[(object) "inputfield_celledit_ro_selected_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.CellEditorReadOnlySel] = (Brush) resources[(object) "inputfield_celledit_ro_selected_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.CellEditorReadOnlySel] = (Brush) resources[(object) "inputfield_celledit_ro_selected_text"];
      BaseStyledControl.borderBrushes[FieldStyles.CellEditorSel] = (Brush) resources[(object) "inputfield_celledit_selected_border"];
      BaseStyledControl.backgroundBrushes[FieldStyles.CellEditorSel] = (Brush) resources[(object) "inputfield_celledit_selected_background"];
      BaseStyledControl.foregroundBrushes[FieldStyles.CellEditorSel] = (Brush) resources[(object) "inputfield_celledit_selected_text"];
    }

    public BaseStyledControl(bool isCellEditor)
    {
      this.isCellEditor = isCellEditor;
    }

    protected static Brush GetBackgroundBrush(FieldStyles style)
    {
      return BaseStyledControl.backgroundBrushes[style];
    }

    protected static Brush GetForegroundBrush(FieldStyles style)
    {
      return BaseStyledControl.foregroundBrushes[style];
    }

    public bool IsCellEditor
    {
      get
      {
        return this.isCellEditor;
      }
    }
  }
}
