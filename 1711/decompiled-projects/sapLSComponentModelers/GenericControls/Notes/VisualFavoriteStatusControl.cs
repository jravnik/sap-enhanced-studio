﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.Notes.VisualFavoriteStatusControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Notes;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.Notes
{
  public class VisualFavoriteStatusControl : UserControl
  {
    public VisualFavoriteStatusControl(FavoriteStatusControl controlModel)
    {
      TextBlock textBlock = new TextBlock();
      textBlock.Text = "Favorite Status Control";
      textBlock.HorizontalAlignment = HorizontalAlignment.Left;
      textBlock.VerticalAlignment = VerticalAlignment.Top;
      textBlock.Margin = new Thickness(2.0);
      this.Content = (object) textBlock;
    }
  }
}
