﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualRadioButton
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualRadioButton : RadioButton
  {
    public static readonly SolidColorBrush BRUSH_SUBVIEWSWITCH_RADIOBUTTON = Brushes.White;
    public static readonly SolidColorBrush BRUSH_SUBVIEWSWITCH_RADIOBUTTONBACKGROUND = new SolidColorBrush(Color.FromRgb((byte) 177, (byte) 177, (byte) 177));
    private string text;
    private VisualRadioButton.VisualRadioButtonStyles style;

    public VisualRadioButton(string text, VisualRadioButton.VisualRadioButtonStyles style)
    {
      this.Text = text;
      this.style = style;
      if (style != VisualRadioButton.VisualRadioButtonStyles.SubViewSwitchOption)
        return;
      FontManager.Instance.SetFontStyle1("Arial,11,Normal", (RadioButton) this);
      this.Foreground = (Brush) VisualRadioButton.BRUSH_SUBVIEWSWITCH_RADIOBUTTON;
      this.Background = (Brush) VisualRadioButton.BRUSH_SUBVIEWSWITCH_RADIOBUTTONBACKGROUND;
    }

    public string Text
    {
      get
      {
        return this.text;
      }
      set
      {
        if (!(this.text != value))
          return;
        this.text = value;
        this.Content = (object) value;
      }
    }

    public enum VisualRadioButtonStyles
    {
      SubViewSwitchOption,
    }
  }
}
