﻿using SAP.BYD.LS.UI.Foundation.AssemblyExtensions;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("2e755ed6-76df-42c8-9c4b-61a6777a15df")]
[assembly: ComVisible(false)]
[assembly: AssemblyFileVersion("25.0.555.1045")]
[assembly: AssemblyCompany("SAP")]
[assembly: AssemblyTimestamp("")]
[assembly: AssemblyTitle("FloorplanModelers")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("FloorplanModelers")]
[assembly: AssemblyCopyright("Copyright © SAP 2007")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCodeline("")]
[assembly: AssemblyChangelist("")]
[assembly: AssemblyVersion("25.0.555.1045")]
