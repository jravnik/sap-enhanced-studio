﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Resource
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers
{
  [CompilerGenerated]
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  internal class Resource
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resource()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) Resource.resourceMan, (object) null))
          Resource.resourceMan = new ResourceManager("SAP.BYD.LS.UIDesigner.ComponentModelers.Resource", typeof (Resource).Assembly);
        return Resource.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return Resource.resourceCulture;
      }
      set
      {
        Resource.resourceCulture = value;
      }
    }

    internal static Bitmap ActionFormIcon
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (ActionFormIcon), Resource.resourceCulture);
      }
    }

    internal static string ActionsText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ActionsText), Resource.resourceCulture);
      }
    }

    internal static string Add
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (Add), Resource.resourceCulture);
      }
    }

    internal static string AddAction
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddAction), Resource.resourceCulture);
      }
    }

    internal static string AddBinding
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddBinding), Resource.resourceCulture);
      }
    }

    internal static string AddConfigParam
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddConfigParam), Resource.resourceCulture);
      }
    }

    internal static string AddConfigParamList
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddConfigParamList), Resource.resourceCulture);
      }
    }

    internal static string AddConfigParamStructure
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddConfigParamStructure), Resource.resourceCulture);
      }
    }

    internal static string AddDefaultSet
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddDefaultSet), Resource.resourceCulture);
      }
    }

    internal static string AddEventHandler
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddEventHandler), Resource.resourceCulture);
      }
    }

    internal static Bitmap AddIcon
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (AddIcon), Resource.resourceCulture);
      }
    }

    internal static string AddInport
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddInport), Resource.resourceCulture);
      }
    }

    internal static string AddMenuItem
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddMenuItem), Resource.resourceCulture);
      }
    }

    internal static string AddModalDialog
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddModalDialog), Resource.resourceCulture);
      }
    }

    internal static string AddNavigation
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddNavigation), Resource.resourceCulture);
      }
    }

    internal static string AddNodeReferenceBinding
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddNodeReferenceBinding), Resource.resourceCulture);
      }
    }

    internal static string AddOBN
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddOBN), Resource.resourceCulture);
      }
    }

    internal static string AddOutport
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddOutport), Resource.resourceCulture);
      }
    }

    internal static string AddParameter
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddParameter), Resource.resourceCulture);
      }
    }

    internal static string AddQuery
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AddQuery), Resource.resourceCulture);
      }
    }

    internal static Bitmap AddRow
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (AddRow), Resource.resourceCulture);
      }
    }

    internal static Bitmap assign
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (assign), Resource.resourceCulture);
      }
    }

    internal static string AssociateQueryToFindForm
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (AssociateQueryToFindForm), Resource.resourceCulture);
      }
    }

    internal static Bitmap back
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (back), Resource.resourceCulture);
      }
    }

    internal static Bitmap BindImage
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (BindImage), Resource.resourceCulture);
      }
    }

    internal static string BindingsText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (BindingsText), Resource.resourceCulture);
      }
    }

    internal static string ConfigParamDefaultMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ConfigParamDefaultMessage), Resource.resourceCulture);
      }
    }

    internal static string ConfigParametersText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ConfigParametersText), Resource.resourceCulture);
      }
    }

    internal static string ConfigParameterStructureText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ConfigParameterStructureText), Resource.resourceCulture);
      }
    }

    internal static string ConfigParamlistSelected
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ConfigParamlistSelected), Resource.resourceCulture);
      }
    }

    internal static string ConfigParamstructureDefaultMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ConfigParamstructureDefaultMessage), Resource.resourceCulture);
      }
    }

    internal static string ConfigParamstructureSelected
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ConfigParamstructureSelected), Resource.resourceCulture);
      }
    }

    internal static string ConfigParamTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ConfigParamTitle), Resource.resourceCulture);
      }
    }

    internal static string ConfigurationLabel
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ConfigurationLabel), Resource.resourceCulture);
      }
    }

    internal static Bitmap configure
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (configure), Resource.resourceCulture);
      }
    }

    internal static string ConfirmDataFieldDelete
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ConfirmDataFieldDelete), Resource.resourceCulture);
      }
    }

    internal static string ConfirmDataListDelete
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ConfirmDataListDelete), Resource.resourceCulture);
      }
    }

    internal static string ControllerConfigDefaultMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ControllerConfigDefaultMessage), Resource.resourceCulture);
      }
    }

    internal static string ControllerConfigText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ControllerConfigText), Resource.resourceCulture);
      }
    }

    internal static string ControlTemplate
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ControlTemplate), Resource.resourceCulture);
      }
    }

    internal static string DefaultSetExists
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (DefaultSetExists), Resource.resourceCulture);
      }
    }

    internal static string DefaultSetTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (DefaultSetTitle), Resource.resourceCulture);
      }
    }

    internal static Bitmap Delete
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (Delete), Resource.resourceCulture);
      }
    }

    internal static string DeleteMenuItem
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (DeleteMenuItem), Resource.resourceCulture);
      }
    }

    internal static Bitmap DoubleDown
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (DoubleDown), Resource.resourceCulture);
      }
    }

    internal static Bitmap DoubleUp
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (DoubleUp), Resource.resourceCulture);
      }
    }

    internal static Bitmap DOWNARROW
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (DOWNARROW), Resource.resourceCulture);
      }
    }

    internal static string ElementNotDeletable
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ElementNotDeletable), Resource.resourceCulture);
      }
    }

    internal static string EmbeddedComponentToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (EmbeddedComponentToolbox), Resource.resourceCulture);
      }
    }

    internal static string EventHandlerDefaultMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (EventHandlerDefaultMessage), Resource.resourceCulture);
      }
    }

    internal static string EventHandlerExists
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (EventHandlerExists), Resource.resourceCulture);
      }
    }

    internal static string EventHandlersText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (EventHandlersText), Resource.resourceCulture);
      }
    }

    internal static string EventHandlerTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (EventHandlerTitle), Resource.resourceCulture);
      }
    }

    internal static string FactSheetToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (FactSheetToolbox), Resource.resourceCulture);
      }
    }

    internal static string FloorplanNotSupported
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (FloorplanNotSupported), Resource.resourceCulture);
      }
    }

    internal static Bitmap fwd
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (fwd), Resource.resourceCulture);
      }
    }

    internal static Bitmap GoBack
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (GoBack), Resource.resourceCulture);
      }
    }

    internal static Bitmap GridLayoutSnippet
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (GridLayoutSnippet), Resource.resourceCulture);
      }
    }

    internal static string GuidedActivityToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (GuidedActivityToolbox), Resource.resourceCulture);
      }
    }

    internal static Bitmap Icon
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (Icon), Resource.resourceCulture);
      }
    }

    internal static Bitmap Image
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (Image), Resource.resourceCulture);
      }
    }

    internal static string InportsText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (InportsText), Resource.resourceCulture);
      }
    }

    internal static string InterfaceBindingDefaultMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (InterfaceBindingDefaultMessage), Resource.resourceCulture);
      }
    }

    internal static string InterfaceBindingTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (InterfaceBindingTitle), Resource.resourceCulture);
      }
    }

    internal static string InterfaceInportDefaultMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (InterfaceInportDefaultMessage), Resource.resourceCulture);
      }
    }

    internal static string InterfaceInportTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (InterfaceInportTitle), Resource.resourceCulture);
      }
    }

    internal static string InterfaceOutportDefaultMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (InterfaceOutportDefaultMessage), Resource.resourceCulture);
      }
    }

    internal static string InterfaceOutportTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (InterfaceOutportTitle), Resource.resourceCulture);
      }
    }

    internal static string InterfaceText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (InterfaceText), Resource.resourceCulture);
      }
    }

    internal static Bitmap Key
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (Key), Resource.resourceCulture);
      }
    }

    internal static Bitmap ListParameter
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (ListParameter), Resource.resourceCulture);
      }
    }

    internal static Bitmap MediaPlayer
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (MediaPlayer), Resource.resourceCulture);
      }
    }

    internal static string MobileAppContainerToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MobileAppContainerToolbox), Resource.resourceCulture);
      }
    }

    internal static string ModalDialogsDefaultMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ModalDialogsDefaultMessage), Resource.resourceCulture);
      }
    }

    internal static string ModalDialogsText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ModalDialogsText), Resource.resourceCulture);
      }
    }

    internal static string ModelDialogToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ModelDialogToolbox), Resource.resourceCulture);
      }
    }

    internal static string Navigations
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (Navigations), Resource.resourceCulture);
      }
    }

    internal static string NavigationsText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (NavigationsText), Resource.resourceCulture);
      }
    }

    internal static string NodeReferenceText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (NodeReferenceText), Resource.resourceCulture);
      }
    }

    internal static string NodeReferenceTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (NodeReferenceTitle), Resource.resourceCulture);
      }
    }

    internal static string ObjectInstanceToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ObjectInstanceToolbox), Resource.resourceCulture);
      }
    }

    internal static string ObjectValueSelectorToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ObjectValueSelectorToolbox), Resource.resourceCulture);
      }
    }

    internal static string ObjectWorkListToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ObjectWorkListToolbox), Resource.resourceCulture);
      }
    }

    internal static string OBNNavigationsText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (OBNNavigationsText), Resource.resourceCulture);
      }
    }

    internal static string OBNSDefaultMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (OBNSDefaultMessage), Resource.resourceCulture);
      }
    }

    internal static string OnlyActionParameterAllowed
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (OnlyActionParameterAllowed), Resource.resourceCulture);
      }
    }

    internal static string OnlyQueryParameterAllowed
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (OnlyQueryParameterAllowed), Resource.resourceCulture);
      }
    }

    internal static string OutportsText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (OutportsText), Resource.resourceCulture);
      }
    }

    internal static string ParameterFromDifferentQuery
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ParameterFromDifferentQuery), Resource.resourceCulture);
      }
    }

    internal static Bitmap Port_Type
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (Port_Type), Resource.resourceCulture);
      }
    }

    internal static Bitmap Port_Type_Parameter
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (Port_Type_Parameter), Resource.resourceCulture);
      }
    }

    internal static string PortTypeParameterDeleteMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (PortTypeParameterDeleteMessage), Resource.resourceCulture);
      }
    }

    internal static string PreviewCancel
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (PreviewCancel), Resource.resourceCulture);
      }
    }

    internal static string PreviewMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (PreviewMessage), Resource.resourceCulture);
      }
    }

    internal static Bitmap Progress_Bar
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (Progress_Bar), Resource.resourceCulture);
      }
    }

    internal static string QueriesDefaultMessage
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (QueriesDefaultMessage), Resource.resourceCulture);
      }
    }

    internal static string QueriesText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (QueriesText), Resource.resourceCulture);
      }
    }

    internal static string QueryExists
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (QueryExists), Resource.resourceCulture);
      }
    }

    internal static string QueryParameterAlreadyBound
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (QueryParameterAlreadyBound), Resource.resourceCulture);
      }
    }

    internal static string QueryTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (QueryTitle), Resource.resourceCulture);
      }
    }

    internal static string QuickActivityToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (QuickActivityToolbox), Resource.resourceCulture);
      }
    }

    internal static string QuickCreateToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (QuickCreateToolbox), Resource.resourceCulture);
      }
    }

    internal static string QuickViewToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (QuickViewToolbox), Resource.resourceCulture);
      }
    }

    internal static Bitmap RatingControl
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (RatingControl), Resource.resourceCulture);
      }
    }

    internal static Bitmap RemoveRow
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (RemoveRow), Resource.resourceCulture);
      }
    }

    internal static string RequiredAssemblies
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (RequiredAssemblies), Resource.resourceCulture);
      }
    }

    internal static string SortMenuItem
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (SortMenuItem), Resource.resourceCulture);
      }
    }

    internal static string SourceOutportText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (SourceOutportText), Resource.resourceCulture);
      }
    }

    internal static string TargetInportText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (TargetInportText), Resource.resourceCulture);
      }
    }

    internal static string TextPoolEditorTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (TextPoolEditorTitle), Resource.resourceCulture);
      }
    }

    internal static string TextPoolText
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (TextPoolText), Resource.resourceCulture);
      }
    }

    internal static string ThingInspectorToolbox
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ThingInspectorToolbox), Resource.resourceCulture);
      }
    }

    internal static Bitmap UI_Inherited_Form_32_32
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (UI_Inherited_Form_32_32), Resource.resourceCulture);
      }
    }

    internal static Bitmap unassign
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (unassign), Resource.resourceCulture);
      }
    }

    internal static Bitmap Up
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (Up), Resource.resourceCulture);
      }
    }

    internal static string Welcome
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (Welcome), Resource.resourceCulture);
      }
    }
  }
}
