﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls.BaseSelectableControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using SAP.BYD.LS.UIDesigner.UICore.ValueConverter;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls
{
  public abstract class BaseSelectableControl : BaseVisualControl
  {
    private bool hasContextMenu;
    private bool isSpanable;
    private Brush borderBrush;
    private Thickness borderThickness;
    protected static DispatcherOperation dispatcherOperation;
    private PopupControl _leftPopup;
    private PopupControl _rightPopup;
    private PopupControl _topPopup;
    private PopupControl _bottomPopup;
    private SpanningPanel _leftSpanCtrl;
    private SpanningPanel _rightSpanCtrl;
    private SpanningPanel _topSpanCtrl;
    private SpanningPanel _bottomSpanCtrl;
    private ContextOptionsPanel _leftContextPnl;
    private ContextOptionsPanel _rightContextPnl;
    private ContextOptionsPanel _topContextPnl;
    private ContextOptionsPanel _bottomContextPnl;

    public bool IsSpanable
    {
      get
      {
        return this.isSpanable;
      }
      set
      {
        this.isSpanable = value;
      }
    }

    public bool HasContextMenu
    {
      get
      {
        return this.hasContextMenu;
      }
      set
      {
        this.hasContextMenu = value;
        base.ContextMenu = (System.Windows.Controls.ContextMenu) null;
        if (!this.hasContextMenu)
          return;
        base.ContextMenu = new System.Windows.Controls.ContextMenu();
        base.ContextMenu.MinWidth = 100.0;
      }
    }

    public new System.Windows.Controls.ContextMenu ContextMenu
    {
      get
      {
        if (!this.hasContextMenu)
          return (System.Windows.Controls.ContextMenu) null;
        return base.ContextMenu;
      }
      set
      {
        throw new ApplicationException("ContextMenu can't be assigned");
      }
    }

    public BaseSelectableControl(IModelObject model)
      : this(model, false, false)
    {
      this.HasContextMenu = true;
      this.ToolTip = (object) this.SelectionText;
      this.AddToDesignerControlDictionary();
      this.InitializeEvents();
    }

    public BaseSelectableControl(IModelObject model, bool canScroll)
      : this(model, canScroll, false)
    {
      this.HasContextMenu = true;
      this.ToolTip = (object) this.SelectionText;
      this.AddToDesignerControlDictionary();
      this.InitializeEvents();
    }

    public BaseSelectableControl(IModelObject model, bool canScroll, bool hasBorder)
      : base(model, canScroll, hasBorder)
    {
      this.HasContextMenu = true;
      this.ToolTip = (object) this.SelectionText;
      this.AddToDesignerControlDictionary();
      this.InitializeEvents();
    }

    private void InitializeEvents()
    {
      BaseDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
      if (parentDesigner == null)
        return;
      parentDesigner.OnPreviewLostFocus += new RoutedEventHandler(this.designer_OnPreviewLostFocus);
      parentDesigner.GotFocus += new EventHandler(this.designer_GotFocus);
      parentDesigner.OnWindowStateChanged += new WindowStateChanged(this.designer_OnWindowStateChanged);
    }

    private void designer_OnWindowStateChanged(FormWindowState state)
    {
      if (state != FormWindowState.Minimized)
        return;
      if (this._bottomPopup != null)
        this._bottomPopup.IsOpen = false;
      if (this._leftPopup != null)
        this._leftPopup.IsOpen = false;
      if (this._rightPopup != null)
        this._rightPopup.IsOpen = false;
      if (this._topPopup == null)
        return;
      this._topPopup.IsOpen = false;
    }

    private void designer_GotFocus(object sender, EventArgs e)
    {
      this.HideAllSpanningPopUps();
    }

    private void designer_OnPreviewLostFocus(object sender, RoutedEventArgs e)
    {
      this.HideAllSpanningPopUps();
    }

    public override void SelectMe()
    {
      try
      {
        this.borderBrush = this.BorderBrush;
        this.BorderBrush = (Brush) ComponentModelersConstants.ACTIVE_SELECTED_BORDERBRUSH;
        this.borderThickness = this.BorderThickness;
        this.BorderThickness = new Thickness(2.0);
        if (this._leftPopup != null)
          this._leftPopup.IsOpen = true;
        if (this._rightPopup != null)
          this._rightPopup.IsOpen = true;
        if (this._topPopup != null)
          this._topPopup.IsOpen = true;
        if (this._bottomPopup == null)
          return;
        this._bottomPopup.IsOpen = true;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Indication of Control selection failed.", ex));
      }
    }

    public override void UnSelect()
    {
      try
      {
        this.BorderBrush = this.borderBrush;
        this.BorderThickness = this.borderThickness;
        if (this._leftPopup != null)
        {
          if (this._leftPopup.IsMouseOver)
            return;
          this._leftPopup.IsOpen = false;
        }
        if (this._rightPopup != null)
        {
          if (this._rightPopup.IsMouseOver)
            return;
          this._rightPopup.IsOpen = false;
        }
        if (this._topPopup != null)
        {
          if (this._topPopup.IsMouseOver)
            return;
          this._topPopup.IsOpen = false;
        }
        if (this._bottomPopup == null || this._bottomPopup.IsMouseOver)
          return;
        this._bottomPopup.IsOpen = false;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UnSelection indication failed.", ex));
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      BaseDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
      if (parentDesigner == null)
        return;
      parentDesigner.OnPreviewLostFocus -= new RoutedEventHandler(this.designer_OnPreviewLostFocus);
      parentDesigner.GotFocus -= new EventHandler(this.designer_GotFocus);
      parentDesigner.OnWindowStateChanged -= new WindowStateChanged(this.designer_OnWindowStateChanged);
    }

    protected override sealed void OnMouseLeftButtonDown(MouseButtonEventArgs e)
    {
      try
      {
        IDesigner parentDesigner = (IDesigner) ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
        if (parentDesigner != null)
          parentDesigner.RaiseSelectionChanged((IDesignerControl) this, this.Model);
        e.Handled = true;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Triggering the selection of Control failed.", ex));
      }
    }

    protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
    {
      try
      {
        if (!(e.OriginalSource is FrameworkElement) || !ValidationManager.GetInstance().IsComponentEditable(this.Model, false))
          return;
        IDesignerControl selectableControl = (IDesignerControl) ComponentModelerUtilities.Instance.GetBaseSelectableControl(e.OriginalSource as FrameworkElement);
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
        if (selectableControl == null || selectableControl != this || !(this.Model is ITextDependentPropertyContainer))
          return;
        base.OnMouseDoubleClick(e);
        ITextDependentPropertyContainer model = this.Model as ITextDependentPropertyContainer;
        DependentPropertyEditorFrame propertyEditorFrame = new DependentPropertyEditorFrame(model.GetTextDependentPropertyMetadataCollection(), this.Model);
        propertyEditorFrame.ShowInTaskbar = true;
        if (propertyEditorFrame.ShowDialog() != DialogResult.OK)
          return;
        try
        {
          List<TextDependentProperty> propertyList = new List<TextDependentProperty>();
          foreach (KeyValuePair<string, DependentPropertyEditor> dependentPropertyEditor in propertyEditorFrame.DependentPropertyEditorDictionary)
          {
            if (dependentPropertyEditor.Value != null)
            {
              TextDependentProperty dependentProperty = dependentPropertyEditor.Value.DependentProperty as TextDependentProperty;
              propertyList.Add(dependentProperty);
            }
          }
          model.UpdateTextDependentProperties(propertyList);
        }
        catch (Exception ex)
        {
          ExceptionManager.GetInstance().HandleException(new UILayerException("Updation of the Text properties for the selected Control failed", ex));
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute MouseDoubleClick handler.", ex));
      }
    }

    protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
    {
      try
      {
        base.OnMouseEnter(e);
        BaseDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
        if (parentDesigner != null)
        {
          if (parentDesigner.SelectedObject != this.Model)
          {
            this.Background = (Brush) ComponentModelersConstants.ACTIVE_HOVER_GRADIANT_BRUSH;
            FrameworkElement baseVisualControl = (FrameworkElement) ComponentModelerUtilities.Instance.GetBaseVisualControl(this.Parent as FrameworkElement);
            if (baseVisualControl != null)
            {
              System.Windows.Input.MouseEventArgs mouseEventArgs = new System.Windows.Input.MouseEventArgs(e.MouseDevice, e.Timestamp);
              mouseEventArgs.RoutedEvent = UIElement.MouseLeaveEvent;
              baseVisualControl.RaiseEvent((RoutedEventArgs) mouseEventArgs);
            }
          }
          BaseSelectableControl selectedVisualControl = parentDesigner.SelectedVisualControl as BaseSelectableControl;
          if (selectedVisualControl != null && selectedVisualControl._leftPopup != null && ComponentModelerUtilities.Instance.IsParentControl((FrameworkElement) this, (FrameworkElement) selectedVisualControl))
            selectedVisualControl._leftPopup.IsOpen = true;
          if (selectedVisualControl != null && selectedVisualControl._rightPopup != null && ComponentModelerUtilities.Instance.IsParentControl((FrameworkElement) this, (FrameworkElement) selectedVisualControl))
            selectedVisualControl._rightPopup.IsOpen = true;
          if (selectedVisualControl != null && selectedVisualControl._topPopup != null && ComponentModelerUtilities.Instance.IsParentControl((FrameworkElement) this, (FrameworkElement) selectedVisualControl))
            selectedVisualControl._topPopup.IsOpen = true;
          if (selectedVisualControl != null && selectedVisualControl._bottomPopup != null && ComponentModelerUtilities.Instance.IsParentControl((FrameworkElement) this, (FrameworkElement) selectedVisualControl))
            selectedVisualControl._bottomPopup.IsOpen = true;
        }
        e.Handled = true;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Error while indicating Selection Possibility of the control", ex));
      }
    }

    protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
    {
      try
      {
        base.OnMouseLeave(e);
        this.Background = this.ControlBackground;
        if (e.LeftButton != MouseButtonState.Pressed || !this.IsDraggable)
          return;
        BaseDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
        try
        {
          if (parentDesigner != null && parentDesigner.SelectedObject == this.Model)
          {
            this.OnMouseMove(e);
            System.Windows.DataObject dataObject = new System.Windows.DataObject(typeof (IModelObject), (object) this.Model);
            if (BaseSelectableControl.dispatcherOperation != null)
              BaseSelectableControl.dispatcherOperation.Abort();
            BaseSelectableControl.dispatcherOperation = this.Dispatcher.BeginInvoke(DispatcherPriority.Background, (Delegate) new ParameterizedThreadStart(this.DoDragDrop), (object) dataObject);
          }
          e.Handled = true;
        }
        catch
        {
          e.Handled = true;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Error while indicating Selection Possibility of the control", ex));
      }
    }

    protected override sealed void OnContextMenuOpening(ContextMenuEventArgs e)
    {
      base.OnContextMenuOpening(e);
      this.TriggerSelection(this, false);
      if (this.ContextMenu != null)
      {
        this.ContextMenu.Items.Clear();
        this.AddSelectionItems(this.AddContextMenuItem("Select"));
        this.AddContextMenuItems();
        this.AddGridLayoutOperations();
        this.AddSelfDefinedTodoToContextMenu();
      }
      this.ContextMenu.IsOpen = true;
      e.Handled = true;
    }

    protected virtual void AddContextMenuItems()
    {
    }

    protected virtual void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
    }

    protected System.Windows.Controls.MenuItem AddContextMenuItem(string menuHeader)
    {
      if (this.ContextMenu == null)
        return (System.Windows.Controls.MenuItem) null;
      System.Windows.Controls.MenuItem menuItem = new System.Windows.Controls.MenuItem();
      menuItem.MinWidth = 100.0;
      menuItem.Header = (object) menuHeader;
      menuItem.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
      this.ContextMenu.Items.Add((object) menuItem);
      return menuItem;
    }

    protected System.Windows.Controls.MenuItem AddContextMenuItem(string menuHeader, System.Windows.Controls.MenuItem parentItem)
    {
      System.Windows.Controls.MenuItem menuItem = new System.Windows.Controls.MenuItem();
      menuItem.MinWidth = 100.0;
      menuItem.Header = (object) menuHeader;
      menuItem.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
      parentItem.Items.Add((object) menuItem);
      return menuItem;
    }

    protected void AddSeperatorToContextMenu()
    {
      if (this.ContextMenu == null)
        return;
      this.ContextMenu.Items.Add((object) new Separator());
    }

    private void AddSelfDefinedTodoToContextMenu()
    {
      if (this.ContextMenu == null)
        return;
      this.AddSeperatorToContextMenu();
      System.Windows.Controls.MenuItem menuItem = new System.Windows.Controls.MenuItem();
      menuItem.MinWidth = 100.0;
      menuItem.Header = (object) "Add Self-Defined Todo";
      menuItem.Click += new RoutedEventHandler(this.OnContextMenuSelfDefinedTodo_Clicked);
      this.ContextMenu.Items.Add((object) menuItem);
    }

    private void OnContextMenuItemClicked(object sender, RoutedEventArgs e)
    {
      try
      {
        if (!(sender is System.Windows.Controls.MenuItem))
          return;
        System.Windows.Controls.MenuItem source = sender as System.Windows.Controls.MenuItem;
        BaseSelectableControl tag1 = source.Tag as BaseSelectableControl;
        if (tag1 != null)
          this.TriggerSelection(tag1, false);
        else if (source.Tag is ComponentDesigner)
        {
          ComponentDesigner tag2 = source.Tag as ComponentDesigner;
          tag2.RaiseSelectionChanged((IDesignerControl) tag2, tag2.Model);
        }
        else if (ValidationManager.GetInstance().IsComponentEditable(this.Model, true))
          this.HandleContextMenuItemClick(source);
        e.Handled = true;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("ContextMenu Operation failed.", ex));
      }
    }

    private void OnContextMenuSelfDefinedTodo_Clicked(object sender, RoutedEventArgs e)
    {
      try
      {
        if (!ValidationManager.GetInstance().IsComponentEditable(this.Model, true) || this.Model == null || string.IsNullOrEmpty(this.Model.Id))
          return;
        this.RaiseAddSelfDefinedTodo(this.Model.Id);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Self Defined Todo creation failed.", ex));
      }
    }

    private void RaiseAddSelfDefinedTodo(string modelId)
    {
      IDesigner parentDesigner = (IDesigner) ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
      if (parentDesigner == null)
        return;
      parentDesigner.RaiseAddSelfDefinedTodo(modelId);
    }

    private void AddSelectionItems(System.Windows.Controls.MenuItem selectionMenu)
    {
      for (FrameworkElement parent = this.Parent as FrameworkElement; parent != null; parent = parent.Parent as FrameworkElement)
      {
        BaseSelectableControl selectableControl = parent as BaseSelectableControl;
        if (selectableControl != null)
          this.AddContextMenuItem(selectableControl.SelectionText, selectionMenu).Tag = (object) selectableControl;
      }
      ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
      this.AddContextMenuItem(parentDesigner.SelectionText, selectionMenu).Tag = (object) parentDesigner;
    }

    internal virtual void TriggerSelection(BaseSelectableControl vControl, bool isTriggeredByControlAddOperation)
    {
      IDesigner parentDesigner = (IDesigner) ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) vControl);
      if (parentDesigner == null)
        return;
      parentDesigner.RaiseSelectionChanged((IDesignerControl) vControl, vControl.Model);
    }

    private void AddToDesignerControlDictionary()
    {
      BaseDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
      if (parentDesigner == null || parentDesigner.DesignerControlDictionary == null)
        return;
      if (parentDesigner.DesignerControlDictionary.ContainsKey(this.m_ModelObject.Id))
        parentDesigner.DesignerControlDictionary.Remove(this.m_ModelObject.Id);
      parentDesigner.DesignerControlDictionary.Add(this.m_ModelObject.Id, (IDesignerControl) this);
    }

    internal void InitializeSpanningDetails(SpanningInfo info, SpanningStyles style)
    {
      if (!this.isSpanable)
        return;
      SpanningDirection spanningDirection = this.GetSpanningDirection(info, style);
      if ((spanningDirection & SpanningDirection.Left) == SpanningDirection.Left)
      {
        this.InitializeSpanningPopUp(ref this._leftContextPnl, ref this._leftPopup);
        this._leftSpanCtrl = new SpanningPanel(SpanningDirection.Left);
        this._leftSpanCtrl.LeftArrowClicked += new EventHandler<EventArgs>(this.OnSpanLeftArrowClicked);
        this._leftContextPnl.AddSingleItem((System.Windows.Controls.Control) this._leftSpanCtrl);
        this.SetSpanningBinding(ref this._leftContextPnl, ref this._leftPopup, SpanningDirection.Left);
      }
      if ((spanningDirection & SpanningDirection.Right) == SpanningDirection.Right)
      {
        this.InitializeSpanningPopUp(ref this._rightContextPnl, ref this._rightPopup);
        this._rightSpanCtrl = new SpanningPanel(SpanningDirection.Right);
        this._rightSpanCtrl.RightArrowClicked += new EventHandler<EventArgs>(this.OnSpanRightArrowClicked);
        this._rightContextPnl.AddSingleItem((System.Windows.Controls.Control) this._rightSpanCtrl);
        this.SetSpanningBinding(ref this._rightContextPnl, ref this._rightPopup, SpanningDirection.Right);
      }
      if ((spanningDirection & SpanningDirection.Top) == SpanningDirection.Top)
      {
        this.InitializeSpanningPopUp(ref this._topContextPnl, ref this._topPopup);
        this._topSpanCtrl = new SpanningPanel(SpanningDirection.Top);
        this._topSpanCtrl.UpArrowClicked += new EventHandler<EventArgs>(this.OnSpanUpArrowClicked);
        this._topContextPnl.AddSingleItem((System.Windows.Controls.Control) this._topSpanCtrl);
        this.SetSpanningBinding(ref this._topContextPnl, ref this._topPopup, SpanningDirection.Top);
      }
      if ((spanningDirection & SpanningDirection.Bottom) != SpanningDirection.Bottom)
        return;
      this.InitializeSpanningPopUp(ref this._bottomContextPnl, ref this._bottomPopup);
      this._bottomSpanCtrl = new SpanningPanel(SpanningDirection.Bottom);
      this._bottomSpanCtrl.DownArrowClicked += new EventHandler<EventArgs>(this.OnSpanDownArrowClicked);
      this._bottomContextPnl.AddSingleItem((System.Windows.Controls.Control) this._bottomSpanCtrl);
      this.SetSpanningBinding(ref this._bottomContextPnl, ref this._bottomPopup, SpanningDirection.Bottom);
    }

    private void InitializeSpanningPopUp(ref ContextOptionsPanel panel, ref PopupControl popUp)
    {
      if (panel == null)
      {
        panel = new ContextOptionsPanel();
        panel.Style = (System.Windows.Style) panel.FindResource((object) "OpacityInOutFaderStyle");
      }
      if (popUp != null)
        return;
      popUp = new PopupControl();
      popUp.AddToEventRoute(new EventRoute(System.Windows.Controls.MenuItem.ClickEvent), new RoutedEventArgs());
      popUp.AllowsTransparency = true;
      popUp.Placement = PlacementMode.RelativePoint;
      popUp.PlacementTarget = (UIElement) this;
      this.AddLogicalChild((object) popUp);
    }

    private void SetSpanningBinding(ref ContextOptionsPanel panel, ref PopupControl popUp, SpanningDirection direction)
    {
      System.Windows.Data.Binding binding1 = new System.Windows.Data.Binding("hOffsetParentBinding");
      binding1.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof (BaseSelectableControl), 1);
      binding1.Path = new PropertyPath((object) FrameworkElement.ActualWidthProperty);
      binding1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
      binding1.NotifyOnSourceUpdated = true;
      System.Windows.Data.Binding binding2 = new System.Windows.Data.Binding("vOffsetParentBinding");
      binding2.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof (BaseSelectableControl), 1);
      binding2.Path = new PropertyPath((object) FrameworkElement.ActualHeightProperty);
      binding2.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
      binding2.NotifyOnSourceUpdated = true;
      System.Windows.Data.Binding binding3 = new System.Windows.Data.Binding("hOffsetContextBinding");
      binding3.Source = (object) panel;
      binding3.Path = new PropertyPath((object) FrameworkElement.ActualWidthProperty);
      binding3.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
      binding3.NotifyOnSourceUpdated = true;
      MultiBinding multiBinding1 = new MultiBinding();
      multiBinding1.Bindings.Add((BindingBase) binding1);
      multiBinding1.Bindings.Add((BindingBase) binding3);
      multiBinding1.Converter = (IMultiValueConverter) new OffsetConverter();
      multiBinding1.NotifyOnSourceUpdated = true;
      System.Windows.Data.Binding binding4 = new System.Windows.Data.Binding("vOffsetContextBinding");
      binding4.Source = (object) panel;
      binding4.Path = new PropertyPath((object) FrameworkElement.ActualHeightProperty);
      binding4.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
      binding4.NotifyOnSourceUpdated = true;
      MultiBinding multiBinding2 = new MultiBinding();
      multiBinding2.Bindings.Add((BindingBase) binding2);
      multiBinding2.Bindings.Add((BindingBase) binding4);
      multiBinding2.Converter = (IMultiValueConverter) new OffsetConverter();
      multiBinding2.NotifyOnSourceUpdated = true;
      multiBinding1.ConverterParameter = (object) new SpanningOrientation(direction, System.Windows.Controls.Orientation.Horizontal);
      multiBinding2.ConverterParameter = (object) new SpanningOrientation(direction, System.Windows.Controls.Orientation.Vertical);
      popUp.SetBinding(Popup.HorizontalOffsetProperty, (BindingBase) multiBinding1);
      popUp.SetBinding(Popup.VerticalOffsetProperty, (BindingBase) multiBinding2);
      popUp.Child = (UIElement) panel;
    }

    private SpanningDirection GetSpanningDirection(SpanningInfo spanningInfo, SpanningStyles spanningStyle)
    {
      SpanningDirection spanningDirection = SpanningDirection.None;
      switch (spanningStyle)
      {
        case SpanningStyles.both:
          if (spanningInfo.Row + spanningInfo.RowSpan < spanningInfo.ParentRows || spanningInfo.ParentRows == -1)
            spanningDirection |= SpanningDirection.Bottom;
          if (spanningInfo.RowSpan > 1)
            spanningDirection |= SpanningDirection.Top;
          if (spanningInfo.ColSpan > 1)
            spanningDirection |= SpanningDirection.Left;
          if (spanningInfo.ColSpan + spanningInfo.Column < spanningInfo.ParentColumns)
          {
            spanningDirection |= SpanningDirection.Right;
            break;
          }
          break;
        case SpanningStyles.ColSpan:
          if (spanningInfo.ColSpan > 1)
            spanningDirection |= SpanningDirection.Left;
          if (spanningInfo.ColSpan + spanningInfo.Column < spanningInfo.ParentColumns)
          {
            spanningDirection |= SpanningDirection.Right;
            break;
          }
          break;
        case SpanningStyles.RowSpan:
          if (spanningInfo.Row + spanningInfo.RowSpan < spanningInfo.ParentRows)
            spanningDirection |= SpanningDirection.Bottom;
          if (spanningInfo.RowSpan > 1)
          {
            spanningDirection |= SpanningDirection.Top;
            break;
          }
          break;
      }
      return spanningDirection;
    }

    private void HideAllSpanningPopUps()
    {
      if (this._leftPopup != null)
        this._leftPopup.IsOpen = false;
      if (this._rightPopup != null)
        this._rightPopup.IsOpen = false;
      if (this._topPopup != null)
        this._topPopup.IsOpen = false;
      if (this._bottomPopup == null)
        return;
      this._bottomPopup.IsOpen = false;
    }

    protected virtual void OnSpanDownArrowClicked(object sender, EventArgs e)
    {
      this.HideAllSpanningPopUps();
    }

    protected virtual void OnSpanUpArrowClicked(object sender, EventArgs e)
    {
      this.HideAllSpanningPopUps();
    }

    protected virtual void OnSpanRightArrowClicked(object sender, EventArgs e)
    {
      this.HideAllSpanningPopUps();
    }

    protected virtual void OnSpanLeftArrowClicked(object sender, EventArgs e)
    {
      this.HideAllSpanningPopUps();
    }

    protected void DoDragDrop(object dataObject)
    {
      try
      {
        int num = (int) DragDrop.DoDragDrop((DependencyObject) this, dataObject, System.Windows.DragDropEffects.All);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("DragDrop Operation failed.", ex));
      }
    }

    private void AddGridLayoutOperations()
    {
      GridLayout parentOfType = Utilities.GetParentOfType(this.Model, typeof (GridLayout)) as GridLayout;
      if (parentOfType == null)
        return;
      this.AddSeperatorToContextMenu();
      System.Windows.Controls.MenuItem menuItem1 = new System.Windows.Controls.MenuItem();
      menuItem1.MinWidth = 100.0;
      menuItem1.ToolTip = (object) "Layout";
      menuItem1.Header = (object) "Layout";
      this.ContextMenu.Items.Add((object) menuItem1);
      System.Windows.Controls.MenuItem menuItem2 = new System.Windows.Controls.MenuItem();
      menuItem2.MinWidth = 100.0;
      menuItem2.ToolTip = (object) ("Insert " + parentOfType.TypeName + " Column");
      menuItem2.Header = (object) "Insert Column";
      menuItem2.Click += new RoutedEventHandler(this.OnLayoutItemsClickEvent);
      menuItem1.Items.Add((object) menuItem2);
      System.Windows.Controls.MenuItem menuItem3 = new System.Windows.Controls.MenuItem();
      menuItem3.MinWidth = 100.0;
      menuItem3.ToolTip = (object) ("Insert " + parentOfType.TypeName + " Row");
      menuItem3.Header = (object) "Insert Row";
      menuItem3.Click += new RoutedEventHandler(this.OnLayoutItemsClickEvent);
      menuItem1.Items.Add((object) menuItem3);
      System.Windows.Controls.MenuItem menuItem4 = new System.Windows.Controls.MenuItem();
      menuItem4.MinWidth = 100.0;
      menuItem4.ToolTip = (object) ("Delete " + parentOfType.TypeName + " Column");
      menuItem4.Header = (object) "Delete Column";
      menuItem4.Click += new RoutedEventHandler(this.OnLayoutItemsClickEvent);
      menuItem1.Items.Add((object) menuItem4);
      System.Windows.Controls.MenuItem menuItem5 = new System.Windows.Controls.MenuItem();
      menuItem5.MinWidth = 100.0;
      menuItem5.ToolTip = (object) ("Delete " + parentOfType.TypeName + " Row");
      menuItem5.Header = (object) "Delete Row";
      menuItem5.Click += new RoutedEventHandler(this.OnLayoutItemsClickEvent);
      menuItem1.Items.Add((object) menuItem5);
    }

    private void OnLayoutItemsClickEvent(object sender, RoutedEventArgs args)
    {
      try
      {
        if (!ValidationManager.GetInstance().IsComponentEditable(this.Model, true))
          return;
        System.Windows.Controls.MenuItem menuItem = sender as System.Windows.Controls.MenuItem;
        IDesigner parentDesigner = (IDesigner) ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
        IContainer parentOfType = Utilities.GetParentOfType(this.Model, typeof (IContainer)) as IContainer;
        if (menuItem == null || menuItem.Header == null || parentOfType == null)
          return;
        ILayoutProvider parent = parentOfType.Parent as ILayoutProvider;
        if (parent == null)
          return;
        if (menuItem.Header == (object) "Insert Column")
          parent.InsertColumn(parentOfType.Column);
        else if (menuItem.Header == (object) "Insert Row")
          parent.InsertRow(parentOfType.Row);
        else if (menuItem.Header == (object) "Delete Column")
        {
          if (parentOfType is PaneContainer)
          {
            PaneContainer paneContainer1 = parentOfType as PaneContainer;
            bool flag = true;
            IExtensible model = parentOfType as IExtensible;
            if (ValidationManager.GetInstance().IsDeletable(model))
            {
              foreach (BasePane basePane1 in paneContainer1.Items)
              {
                flag = flag ? this.isPaneDeleteble(basePane1) : flag;
                if (flag)
                {
                  if (basePane1 is TabStrip)
                  {
                    foreach (GridLayout tab in (basePane1 as TabStrip).Tabs)
                    {
                      foreach (PaneContainer paneContainer2 in tab.PaneContainers)
                      {
                        if (ValidationManager.GetInstance().IsDeletable(model))
                        {
                          foreach (BasePane basePane2 in paneContainer2.Items)
                          {
                            flag = flag ? this.isPaneDeleteble(basePane2) : flag;
                            if (!flag)
                              break;
                          }
                        }
                      }
                    }
                  }
                  else if (basePane1 is PaneContainerVariant)
                  {
                    BasePane basePane2 = (basePane1 as PaneContainerVariant).Item;
                    flag = flag ? this.isPaneDeleteble(basePane2) : flag;
                    if (basePane2 is TabStrip)
                    {
                      foreach (GridLayout tab in (basePane1 as TabStrip).Tabs)
                      {
                        foreach (PaneContainer paneContainer2 in tab.PaneContainers)
                        {
                          if (ValidationManager.GetInstance().IsDeletable(model))
                          {
                            foreach (BasePane basePane3 in paneContainer2.Items)
                            {
                              flag = flag ? this.isPaneDeleteble(basePane3) : flag;
                              if (!flag)
                                break;
                            }
                          }
                        }
                      }
                    }
                  }
                }
                else
                  break;
              }
              if (flag)
              {
                parent.RemoveColumn(parentOfType.Column);
              }
              else
              {
                int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.Model.Resource.ElementnotDeletable, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
              }
            }
            else
            {
              int num1 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.Model.Resource.ElementnotDeletable, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
          }
          else
            parent.RemoveColumn(parentOfType.Column);
          IDesignerControl designerControl = parentDesigner != null ? parentDesigner.DesignerControlDictionary[parent.Id] : (IDesignerControl) null;
          if (designerControl == null)
            return;
          this.TriggerSelection(designerControl as BaseSelectableControl, false);
        }
        else
        {
          if (menuItem.Header != (object) "Delete Row")
            return;
          if (parentOfType is PaneContainer)
          {
            PaneContainer paneContainer1 = parentOfType as PaneContainer;
            bool flag = true;
            IExtensible model = (IExtensible) paneContainer1;
            if (ValidationManager.GetInstance().IsDeletable(model))
            {
              foreach (BasePane basePane1 in paneContainer1.Items)
              {
                flag = flag ? this.isPaneDeleteble(basePane1) : flag;
                if (basePane1 is TabStrip)
                {
                  foreach (GridLayout tab in (basePane1 as TabStrip).Tabs)
                  {
                    foreach (PaneContainer paneContainer2 in tab.PaneContainers)
                    {
                      if (ValidationManager.GetInstance().IsDeletable(model))
                      {
                        foreach (BasePane basePane2 in paneContainer2.Items)
                        {
                          flag = flag ? this.isPaneDeleteble(basePane2) : flag;
                          if (!flag)
                            break;
                        }
                      }
                    }
                  }
                }
                else if (basePane1 is PaneContainerVariant)
                {
                  BasePane basePane2 = (basePane1 as PaneContainerVariant).Item;
                  flag = flag ? this.isPaneDeleteble(basePane2) : flag;
                  if (basePane2 is TabStrip)
                  {
                    foreach (GridLayout tab in (basePane1 as TabStrip).Tabs)
                    {
                      foreach (PaneContainer paneContainer2 in tab.PaneContainers)
                      {
                        if (ValidationManager.GetInstance().IsDeletable(model))
                        {
                          foreach (BasePane basePane3 in paneContainer2.Items)
                          {
                            flag = flag ? this.isPaneDeleteble(basePane3) : flag;
                            if (!flag)
                              break;
                          }
                        }
                      }
                    }
                  }
                }
              }
              if (flag)
              {
                parent.RemoveRow(paneContainer1.Row);
              }
              else
              {
                int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.Model.Resource.ElementnotDeletable, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
              }
            }
            else
            {
              int num1 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.Model.Resource.ElementnotDeletable, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
          }
          else
            parent.RemoveRow(parentOfType.Row);
          IDesignerControl designerControl = parentDesigner != null ? parentDesigner.DesignerControlDictionary[parent.Id] : (IDesignerControl) null;
          if (designerControl == null)
            return;
          this.TriggerSelection(designerControl as BaseSelectableControl, false);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Layout Operation failed.", ex));
      }
    }

    private bool isPaneDeleteble(BasePane basePane)
    {
      bool flag = true;
      if (basePane is FormPane)
      {
        foreach (IExtensible sectionGroup in (basePane as FormPane).SectionGroups)
        {
          if (!ValidationManager.GetInstance().IsDeletable(sectionGroup))
          {
            flag = false;
            break;
          }
        }
      }
      else if (basePane is AdvancedListPane)
      {
        foreach (AdvancedListPaneVariant paneVariant in (basePane as AdvancedListPane).PaneVariants)
        {
          if (!ValidationManager.GetInstance().IsDeletable((IExtensible) paneVariant.ListDefinition))
          {
            flag = false;
            break;
          }
        }
      }
      return flag;
    }
  }
}
