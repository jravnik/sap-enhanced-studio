﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls.BaseVisualControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.EventArg;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls
{
  public abstract class BaseVisualControl : System.Windows.Controls.UserControl, IDesignerControl
  {
    private Brush backgroundBrush = (Brush) ComponentModelersConstants.CONTENT_AREA_BRUSH;
    public static readonly int DRAG_TOOLTIP_POSITION_OFFSET = 10;
    protected IModelObject m_ModelObject;
    private Grid rootGridControl;
    private ScrollViewer scrollViewerControl;
    private static ControlTemplate controlTemplate;
    private bool m_IsDraggable;
    private ContextMenuStrip tooltipMenu;

    public bool IsDraggable
    {
      get
      {
        return this.m_IsDraggable;
      }
      set
      {
        this.m_IsDraggable = value;
      }
    }

    internal Grid Grid
    {
      get
      {
        return this.rootGridControl;
      }
    }

    public CornerRadius BorderCornerRadius
    {
      get
      {
        return (CornerRadius) this.GetValue(Border.CornerRadiusProperty);
      }
      set
      {
        this.SetValue(Border.CornerRadiusProperty, (object) value);
      }
    }

    public Brush ControlBackground
    {
      get
      {
        return this.backgroundBrush;
      }
      set
      {
        this.backgroundBrush = value;
        this.Background = this.backgroundBrush;
      }
    }

    public virtual ScrollViewer ScrollViewer
    {
      get
      {
        return this.scrollViewerControl;
      }
    }

    public abstract string SelectionText { get; }

    static BaseVisualControl()
    {
      using (TextReader input = (TextReader) new StringReader(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.ControlTemplate))
      {
        using (XmlReader reader = (XmlReader) new XmlTextReader(input))
          BaseVisualControl.controlTemplate = XamlReader.Load(reader) as ControlTemplate;
      }
    }

    internal BaseVisualControl(IModelObject model)
    {
      try
      {
        this.InitializeRootControl(model, false, false);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Initializing the root control failed.", ex));
      }
    }

    internal BaseVisualControl(IModelObject model, bool canScroll)
    {
      try
      {
        this.InitializeRootControl(model, canScroll, false);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Initializing the root control failed.", ex));
      }
    }

    internal BaseVisualControl(IModelObject model, bool canScroll, bool hasBorder)
    {
      try
      {
        this.InitializeRootControl(model, canScroll, hasBorder);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Initializing the root control failed.", ex));
      }
    }

    private void InitializeRootControl(IModelObject model, bool canScroll, bool hasBorder)
    {
      this.m_ModelObject = model;
      if (BaseVisualControl.controlTemplate != null)
        this.Template = BaseVisualControl.controlTemplate;
      if (this.m_ModelObject != null)
      {
        this.AttachDisplayPropertyChange();
        this.m_ModelObject.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
        this.m_ModelObject.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.m_ModelObject.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.SetDisplayMode();
        if (canScroll)
        {
          this.scrollViewerControl = new ScrollViewer();
          this.scrollViewerControl.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
          this.scrollViewerControl.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
          this.scrollViewerControl.CanContentScroll = true;
          this.Content = (object) this.scrollViewerControl;
        }
      }
      this.rootGridControl = new Grid();
      if (canScroll)
        this.scrollViewerControl.Content = (object) this.rootGridControl;
      else
        this.Content = (object) this.rootGridControl;
      this.Background = this.backgroundBrush;
      if (this.tooltipMenu != null)
        return;
      this.tooltipMenu = new ContextMenuStrip();
      this.AddLogicalChild((object) this.tooltipMenu);
      this.tooltipMenu.Items.Add(this.SelectionText);
    }

    private void AttachDisplayPropertyChange()
    {
      if (!LoginManager.Instance.OptimizedMode)
        return;
      (Utilities.GetFloorplanObject(this.m_ModelObject) as BaseComponent).DisplayModel.PropertyChanged += new PropertyChangedEventHandler(this.OnDisplayPropertyChanged);
    }

    private void DetachDisplayPropertyChange()
    {
      if (!LoginManager.Instance.OptimizedMode)
        return;
      (Utilities.GetFloorplanObject(this.m_ModelObject) as BaseComponent).DisplayModel.PropertyChanged -= new PropertyChangedEventHandler(this.OnDisplayPropertyChanged);
    }

    private void OnDisplayPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      this.SetDisplayMode();
    }

    private void SetDisplayMode()
    {
      if (Utilities.GetFloorplanObject(this.m_ModelObject).IsDisplayOnly)
        this.Opacity = 0.9;
      else
        this.Opacity = 1.0;
    }

    public IModelObject Model
    {
      get
      {
        return this.m_ModelObject;
      }
    }

    public System.Windows.Forms.Control View
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public virtual void LoadView()
    {
    }

    public virtual void UnSelect()
    {
    }

    public virtual void SelectMe()
    {
    }

    public virtual void Terminate()
    {
      try
      {
        if (this.m_ModelObject != null)
        {
          this.DetachDisplayPropertyChange();
          this.m_ModelObject.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
          this.m_ModelObject.ModelObjectAdded -= new ModelAddedEventHandler(this.OnModelObjectAdded);
          this.m_ModelObject.ModelObjectRemoved -= new ModelRemovedEventHandler(this.OnModelObjectRemoved);
          BaseDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
          if (parentDesigner != null && parentDesigner.DesignerControlDictionary != null && parentDesigner.DesignerControlDictionary.ContainsKey(this.m_ModelObject.Id))
            parentDesigner.DesignerControlDictionary.Remove(this.m_ModelObject.Id);
        }
        ComponentModelerUtilities.Instance.TerminatePanelControls((FrameworkElement) this.rootGridControl);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Termination of control failed", ex));
      }
    }

    public virtual void ReloadView()
    {
      this.Terminate();
      this.AttachDisplayPropertyChange();
      this.m_ModelObject.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      this.m_ModelObject.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
      this.m_ModelObject.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      BaseDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
      if (parentDesigner != null && parentDesigner.DesignerControlDictionary != null)
      {
        if (parentDesigner.DesignerControlDictionary.ContainsKey(this.m_ModelObject.Id))
          parentDesigner.DesignerControlDictionary.Remove(this.m_ModelObject.Id);
        parentDesigner.DesignerControlDictionary.Add(this.m_ModelObject.Id, (IDesignerControl) this);
      }
      this.Grid.Children.Clear();
      this.Grid.RowDefinitions.Clear();
      this.Grid.ColumnDefinitions.Clear();
      this.LoadView();
    }

    private bool TooltipShown { get; set; }

    protected virtual void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
      if (parentDesigner == null || e.RemovedModel == null)
        return;
      parentDesigner.ModelRemovedCall(e.RemovedModel, e.PositionInParent);
    }

    protected virtual void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
      if (parentDesigner == null || e.AddedModel == null)
        return;
      parentDesigner.ModelAddedCall(e.AddedModel, e.PositionInParent);
      if (e.AddedModel.Id == null || !parentDesigner.DesignerControlDictionary.ContainsKey(e.AddedModel.Id))
        return;
      BaseSelectableControl designerControl = parentDesigner.DesignerControlDictionary[e.AddedModel.Id] as BaseSelectableControl;
      if (designerControl == null)
        return;
      designerControl.TriggerSelection(designerControl, true);
    }

    protected virtual void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
      if (parentDesigner == null || this.m_ModelObject == null)
        return;
      parentDesigner.PropertyChangedCall(this.m_ModelObject, e.PropertyName, e.OldValue, e.NewValue);
    }

    protected override sealed void OnDragOver(System.Windows.DragEventArgs e)
    {
      base.OnDragOver(e);
      e.Effects = System.Windows.DragDropEffects.None;
      if (!this.TooltipShown && !string.IsNullOrEmpty(this.SelectionText))
      {
        Point screen = this.PointToScreen(new Point(e.GetPosition((IInputElement) this).X, e.GetPosition((IInputElement) this).Y));
        this.tooltipMenu.Show((int) screen.X + BaseVisualControl.DRAG_TOOLTIP_POSITION_OFFSET, (int) screen.Y + BaseVisualControl.DRAG_TOOLTIP_POSITION_OFFSET);
        this.TooltipShown = true;
      }
      else
        this.TooltipShown = false;
      try
      {
        this.MoveScrollbarOnDragOver(e);
        if (!this.AllowDrop)
          return;
        e.Handled = true;
        IModelObject data1 = e.Data.GetData(typeof (IModelObject).FullName) as IModelObject;
        ToolBoxEventArgs data2 = e.Data.GetData(typeof (ToolBoxEventArgs).FullName) as ToolBoxEventArgs;
        OBNEventArgs data3 = e.Data.GetData(typeof (OBNEventArgs).FullName) as OBNEventArgs;
        BOBrowserDragEventArgs data4 = e.Data.GetData(typeof (BOBrowserDragEventArgs).FullName) as BOBrowserDragEventArgs;
        if (data1 == null && data2 == null && (data3 == null && data4 == null))
        {
          System.Windows.Forms.IDataObject dataObject = DragDropUtil.GetDataObject(e.Data);
          if (dataObject != null)
          {
            ConfigurationExplorerDragEventArgs data5 = dataObject.GetData(typeof (ConfigurationExplorerDragEventArgs).FullName) as ConfigurationExplorerDragEventArgs;
            DataModelDragEventArgs data6 = dataObject.GetData(typeof (DataModelDragEventArgs).FullName) as DataModelDragEventArgs;
            if (data6 != null)
            {
              this.OnDataModelElementDragOver(e, data6);
            }
            else
            {
              if (data5 == null)
                return;
              this.OnComponentDragOver(e, data5);
            }
          }
          else
          {
            ConfigurationExplorerDragEventArgs data5 = e.Data.GetData(typeof (ConfigurationExplorerDragEventArgs).FullName) as ConfigurationExplorerDragEventArgs;
            if (data5 == null)
              return;
            this.OnComponentDragOver(e, data5);
          }
        }
        else if (data2 != null)
          this.OnToolBoxControlDragOver(e, data2);
        else if (data3 != null)
          this.OnOBNOperationDragOver(e, data3);
        else if (data4 != null)
          this.OnBOElementDragOver(e, data4);
        else if (!Utilities.IsNestedComposition(this.Model, data1))
          this.OnBaseVisualControlDragOver(e, data1);
        else
          e.Effects = System.Windows.DragDropEffects.None;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Drag Operation failed.", ex));
      }
    }

    protected override sealed void OnDrop(System.Windows.DragEventArgs e)
    {
      this.tooltipMenu.Hide();
      this.TooltipShown = false;
      this.RemoveLogicalChild((object) this.tooltipMenu);
      try
      {
        base.OnDrop(e);
        if (ValidationManager.GetInstance().IsComponentEditable(this.Model, true))
        {
          ToolBoxEventArgs toolboxEventArg = (ToolBoxEventArgs) null;
          BOBrowserDragEventArgs boBrowserArgs = (BOBrowserDragEventArgs) null;
          ConfigurationExplorerDragEventArgs ceArgs = (ConfigurationExplorerDragEventArgs) null;
          DataModelDragEventArgs dmArgs = (DataModelDragEventArgs) null;
          OBNEventArgs obnEventArgs = (OBNEventArgs) null;
          IModelObject data = e.Data.GetData(typeof (IModelObject).FullName) as IModelObject;
          if (data == null)
          {
            System.Windows.Forms.IDataObject dataObject = DragDropUtil.GetDataObject(e.Data);
            if (dataObject != null)
            {
              ceArgs = dataObject.GetData(typeof (ConfigurationExplorerDragEventArgs).FullName) as ConfigurationExplorerDragEventArgs;
              dmArgs = dataObject.GetData(typeof (DataModelDragEventArgs).FullName) as DataModelDragEventArgs;
            }
            else
            {
              toolboxEventArg = e.Data.GetData(typeof (ToolBoxEventArgs).FullName) as ToolBoxEventArgs;
              obnEventArgs = e.Data.GetData(typeof (OBNEventArgs).FullName) as OBNEventArgs;
              boBrowserArgs = e.Data.GetData(typeof (BOBrowserDragEventArgs).FullName) as BOBrowserDragEventArgs;
              ceArgs = e.Data.GetData(typeof (ConfigurationExplorerDragEventArgs).FullName) as ConfigurationExplorerDragEventArgs;
            }
          }
          if (toolboxEventArg != null)
          {
            if (!this.TryHandleLinkedToolBoxElement(toolboxEventArg))
              this.OnToolBoxControlDrop(toolboxEventArg);
          }
          else if (boBrowserArgs != null)
          {
            if (((BaseComponent) Utilities.GetFloorplanObject(this.Model)).UseUIController && boBrowserArgs[0].DraggedElement.ParentModel.BOType == "controller")
            {
              int num = (int) System.Windows.MessageBox.Show("Components in UIController mode cannot be bound to ECO", "UIDesigner", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
            else
              this.OnBOElementDrop(boBrowserArgs);
          }
          else if (ceArgs != null)
            this.OnComponentDrop(ceArgs);
          else if (dmArgs != null)
            this.OnDataModelElementDrop(dmArgs);
          else if (obnEventArgs != null)
            this.OnOBNOperationDrop(obnEventArgs);
          else if (data != null && !Utilities.IsNestedComposition(this.Model, data))
            this.OnBaseVisualControlDrop(data);
        }
        e.Handled = true;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Control Drop event Failed.", ex));
      }
    }

    protected override void OnDragLeave(System.Windows.DragEventArgs e)
    {
      base.OnDragLeave(e);
      this.tooltipMenu.Hide();
      this.TooltipShown = false;
      this.RemoveLogicalChild((object) this.tooltipMenu);
    }

    private void MoveScrollbarOnDragOver(System.Windows.DragEventArgs e)
    {
      for (FrameworkElement frameworkElement = (FrameworkElement) this; frameworkElement != null; frameworkElement = frameworkElement.Parent != null ? frameworkElement.Parent as FrameworkElement : frameworkElement.TemplatedParent as FrameworkElement)
      {
        if (frameworkElement is BaseVisualControl)
        {
          BaseVisualControl baseVisualControl = frameworkElement as BaseVisualControl;
          if (baseVisualControl != null && baseVisualControl.ScrollViewer != null)
          {
            Point position = e.GetPosition((IInputElement) baseVisualControl);
            if (position.Y <= 20.0)
            {
              double offset = baseVisualControl.ScrollViewer.ContentVerticalOffset - 10.0;
              baseVisualControl.ScrollViewer.ScrollToVerticalOffset(offset);
            }
            if (baseVisualControl.ActualHeight >= position.Y + 20.0)
              break;
            double offset1 = baseVisualControl.ScrollViewer.ContentVerticalOffset + 10.0;
            baseVisualControl.ScrollViewer.ScrollToVerticalOffset(offset1);
            break;
          }
        }
      }
    }

    protected virtual void OnDataModelElementDragOver(System.Windows.DragEventArgs e, DataModelDragEventArgs dataModelEventArgs)
    {
      if (!ValidationManager.GetInstance().ValidateModel(this.Model, (object) dataModelEventArgs.DataElement))
        return;
      e.Effects = System.Windows.DragDropEffects.Move;
    }

    protected virtual void OnOBNOperationDragOver(System.Windows.DragEventArgs e, OBNEventArgs obnOperationEventArgds)
    {
      if ((!(this.Model is SectionGroupItem) || !((this.Model as SectionGroupItem).Item is INavigationContainer)) && (!(this.Model is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button) && !(this.Model is ListColumn)))
        return;
      e.Effects = System.Windows.DragDropEffects.Copy;
    }

    protected virtual void OnBOElementDragOver(System.Windows.DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      if (boBrowserArgs == null || boBrowserArgs.Count <= 0 || !ValidationManager.GetInstance().ValidateModel(this.Model, boBrowserArgs[0].DraggedElement.GetType().FullName))
        return;
      e.Effects = System.Windows.DragDropEffects.Copy;
    }

    protected virtual void OnComponentDragOver(System.Windows.DragEventArgs e, ConfigurationExplorerDragEventArgs ceArgs)
    {
      if (!ValidationManager.GetInstance().ValidateModel(this.Model, ceArgs.ComponentDetails.GetType().FullName))
        return;
      e.Effects = System.Windows.DragDropEffects.Copy;
    }

    protected virtual void OnToolBoxControlDragOver(System.Windows.DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      if (this.TryValidateLinkedToolBoxElement(toolboxEventArg, e) || !ValidationManager.GetInstance().ValidateModel(this.Model, toolboxEventArg.Tag.ClassName))
        return;
      e.Effects = System.Windows.DragDropEffects.Copy;
    }

    protected virtual void OnBaseVisualControlDragOver(System.Windows.DragEventArgs e, IModelObject droppedModel)
    {
      if (!ValidationManager.GetInstance().ValidateModel(this.Model, droppedModel.GetType().FullName))
        return;
      e.Effects = System.Windows.DragDropEffects.Move;
    }

    protected virtual void OnDataModelElementDrop(DataModelDragEventArgs dmArgs)
    {
      this.Model.AddModelObject(dmArgs);
    }

    protected virtual void OnOBNOperationDrop(OBNEventArgs obnEventArgs)
    {
      OBNUtil.CreateOBN(this.Model, obnEventArgs.BONameSpace, obnEventArgs.BO, obnEventArgs.Node, obnEventArgs.OBNOperation.Operation);
    }

    protected virtual void OnBOElementDrop(BOBrowserDragEventArgs boBrowserArgs)
    {
      foreach (BOBrowserDragEventArg boBrowserArg in (List<BOBrowserDragEventArg>) boBrowserArgs)
        this.Model.AddModelObject(boBrowserArg);
    }

    protected virtual void OnComponentDrop(ConfigurationExplorerDragEventArgs ceArgs)
    {
      this.Model.AddModelObject(ceArgs);
    }

    protected virtual void OnToolBoxControlDrop(ToolBoxEventArgs toolboxEventArg)
    {
      this.Model.AddModelObject(toolboxEventArg.Tag);
    }

    protected bool TryHandleLinkedToolBoxElement(ToolBoxEventArgs toolboxEventArg)
    {
      bool flag = false;
      IFloorplan floorplanObject = Utilities.GetFloorplanObject(this.Model);
      switch (toolboxEventArg.Tag.ControlName)
      {
        case "ActionForm":
          if (floorplanObject.UXView != null && floorplanObject.UXView.ActionForms != null)
          {
            floorplanObject.UXView.ActionForms.AddActionForm();
            flag = true;
            break;
          }
          break;
        case "GridLayoutSnippet":
          if (floorplanObject.UXView != null)
          {
            floorplanObject.UXView.AddGridLayoutSnippet();
            flag = true;
            break;
          }
          break;
        case "ModalDialog":
          if (floorplanObject.UXView != null && floorplanObject.UXView.ModalDialogs != null)
          {
            floorplanObject.UXView.ModalDialogs.AddModalDialog();
            flag = true;
            break;
          }
          break;
        case "PlaceableFields":
          if (floorplanObject.UXView != null)
          {
            floorplanObject.UXView.AddPlaceableField();
            flag = true;
            break;
          }
          break;
        case "SideCarPanel":
          if (floorplanObject.UXView != null)
          {
            floorplanObject.UXView.SideCarViewPanels.AddSideCarViewPanel();
            flag = true;
            break;
          }
          break;
        case "ListModificationForm":
          if (floorplanObject.UXView != null)
          {
            floorplanObject.UXView.ListModificationForms.AddListModificationForm();
            flag = true;
            break;
          }
          break;
      }
      return flag;
    }

    protected bool TryValidateLinkedToolBoxElement(ToolBoxEventArgs toolboxEventArg, System.Windows.DragEventArgs e)
    {
      bool flag = false;
      switch (toolboxEventArg.Tag.ControlName)
      {
        case "ActionForm":
          e.Effects = System.Windows.DragDropEffects.Copy;
          flag = true;
          break;
        case "GridLayoutSnippet":
          e.Effects = System.Windows.DragDropEffects.Copy;
          flag = true;
          break;
        case "ModalDialog":
          e.Effects = System.Windows.DragDropEffects.Copy;
          flag = true;
          break;
        case "PlaceableFields":
          IFloorplan floorplanObject = Utilities.GetFloorplanObject(this.Model);
          if (floorplanObject != null && floorplanObject.UXView != null && floorplanObject.UXView.PlaceableFields == null)
          {
            e.Effects = System.Windows.DragDropEffects.Copy;
            flag = true;
            break;
          }
          break;
        case "SideCarPanel":
          e.Effects = System.Windows.DragDropEffects.Copy;
          flag = true;
          break;
        case "ListModificationForm":
          e.Effects = System.Windows.DragDropEffects.Copy;
          flag = true;
          break;
      }
      return flag;
    }

    protected virtual void OnBaseVisualControlDrop(IModelObject droppedModel)
    {
      this.Model.AddModelObject(droppedModel, true);
    }

    public virtual void BringChildControlToFocus(IModelObject childControlModel)
    {
    }
  }
}
