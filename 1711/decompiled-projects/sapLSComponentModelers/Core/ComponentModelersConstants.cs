﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.ComponentModelersConstants
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Drawing;
using System.Windows;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  internal class ComponentModelersConstants
  {
    public static readonly Font TEXT_BOLD_FONT = new Font("Arial", 12f, System.Drawing.FontStyle.Regular, GraphicsUnit.Pixel);
    public static readonly System.Drawing.Color WF_CONTENT_AREA_COLOR = System.Drawing.Color.FromArgb(233, 240, 245);
    public static readonly System.Windows.Media.Color ACTIVE_SELECTED_BEGIN_COLOR = Colors.White;
    public static readonly System.Windows.Media.Color ACTIVE_SELECTED_END_COLOR = System.Windows.Media.Color.FromRgb(byte.MaxValue, (byte) 153, (byte) 0);
    public static readonly System.Windows.Media.Color ACTIVE_HOVER_BEGIN_COLOR = Colors.White;
    public static readonly System.Windows.Media.Color ACTIVE_HOVER_END_COLOR = System.Windows.Media.Color.FromRgb(byte.MaxValue, (byte) 204, (byte) 102);
    public static readonly System.Windows.Media.Color FIELD_BACKGROUND_COLOR = System.Windows.Media.Color.FromRgb((byte) 143, (byte) 182, (byte) 223);
    public static readonly System.Windows.Media.Color ACTIVE_BORDER_COLOR = System.Windows.Media.Color.FromRgb((byte) 143, (byte) 182, (byte) 223);
    public static readonly System.Windows.Media.Color NODE_BACKGROUND = System.Windows.Media.Color.FromArgb(byte.MaxValue, (byte) 164, (byte) 192, (byte) 232);
    public static readonly Thickness CONTROL_BORDER_THICKNESS = new Thickness(1.0);
    public static readonly Thickness CONTROL_BORDER_MARGIN = new Thickness(2.0);
    public static readonly Thickness CALENDAR_CONTROL_BORDER_MARGIN = new Thickness(4.0);
    public static readonly SolidColorBrush ACTIVE_BORDER_BRUSH = new SolidColorBrush(ComponentModelersConstants.ACTIVE_BORDER_COLOR);
    public static readonly SolidColorBrush FIELD_BACKGROUND_BRUSH = new SolidColorBrush(ComponentModelersConstants.FIELD_BACKGROUND_COLOR);
    public static readonly LinearGradientBrush ACTIVE_HOVER_GRADIANT_BRUSH = new LinearGradientBrush(ComponentModelersConstants.ACTIVE_SELECTED_BEGIN_COLOR, ComponentModelersConstants.ACTIVE_HOVER_END_COLOR, 90.0);
    public static readonly LinearGradientBrush ACTIVE_SELECTED_GRADIANT_BRUSH = new LinearGradientBrush(ComponentModelersConstants.ACTIVE_SELECTED_BEGIN_COLOR, ComponentModelersConstants.ACTIVE_SELECTED_END_COLOR, 90.0);
    public static readonly LinearGradientBrush ACTIVE_SELECTED_BORDERBRUSH = new LinearGradientBrush(Colors.Red, Colors.Transparent, 180.0);
    public static readonly SolidColorBrush CONTAINER_BORDER_BRUSH = new SolidColorBrush(System.Windows.Media.Color.FromRgb((byte) 103, (byte) 142, (byte) 183));
    public static readonly SolidColorBrush CONTENT_AREA_BRUSH = new SolidColorBrush(System.Windows.Media.Color.FromRgb((byte) 233, (byte) 240, (byte) 245));
    public static readonly SolidColorBrush FIELD_AREA_BRUSH = new SolidColorBrush(System.Windows.Media.Color.FromRgb((byte) 214, (byte) 223, (byte) 233));
    public static readonly Thickness FIELD_CONTROLS_MARGIN = new Thickness(1.0);
    public static readonly Thickness GENERICCONTROL_BORDER_THICKNESS = new Thickness(0.0);
    public static readonly string SKINNAME = "nova";
    public static readonly string SKIN_CONFIG_REF = "sapLSUISkins;component/skin.nova/skinconfig.vcfg";
    public const string CONTEXTMENU_ADD = "Add";
    public const string CONTEXTMENU_SELECTION = "Select";
    public const string CONTEXTMENU_DELETE = "Delete";
    public const string CONTEXTMENU_LAYOUT = "Layout";
    public const string CONTEXTMENU_INSERT = "Insert";
    public const string CONTEXTMENU_COPY = "Copy";
    public const string CONTEXTMENU_PASTE = "Paste";
    public const string CONTEXTMENU_CUT = "Cut";
    public const string CONTEXTMENU_View = "view";
    public const string CONTEXTMENU_AddTab = "Add Tab";
    public const string CONTEXTMENU_ROW = "Row";
    public const string CONTEXTMENU_COLUMN = "Column";
    public const string CONTEXTMENU_INSERTROW = "Insert Row";
    public const string CONTEXTMENU_INSERTCOLUMN = "Insert Column";
    public const string CONTEXTMENU_DELETEROW = "Delete Row";
    public const string CONTEXTMENU_DELETECOLUMN = "Delete Column";
    public const int CONTEXTMENU_WIDTH = 100;
    public const string CONTEXTMENU_REFRESH = "Refresh";
    public const string CONTEXTMENU_RENAME = "Rename";
    public const string ADD_MENU_ITEM = "AddMenuItem";
    public const string DELETE_MENU_ITEM = "DeleteMenuItem";
    public const string SORT_MENU_ITEM = "SortMenuItem";
    public const string RENAME_MENU_ITEM = "RenameMenuItem";
    public const string VALUE_HELP_VALUE = "ValueHelp";
    public const string ENTER_VALUE = "Enter the value";
    public const string BTN_SET_DEFAULT = "btnSetDefault";
    public const string CONTEXTMENU_SELFDEFTODO = "Add Self-Defined Todo";
    public const string CONTEXTMENU_OBN_WIZARD = "Add OBN using Wizard";
    public const string CONTEXTMENU_EDIT_OBN_WIZARD = "Edit OBN using Wizard";
    public const string MODEL_LAYOUTGRID = "Model Layout Grid";
    public const string MODEL_LAYOUTSTACKPANEL = "Model Layout Stack Panel";
    public const string MODEL_LAYOUTBORDERCONTROL = "Model Layout Border Control";
    public const string VIEW_EMBEDDEDCOMPONENT = "View EmbeddedComponent";
    public const string REFRESH_EMBEDDEDCOMPONENT = "Refresh EmbeddedComponent";
    public const string CONTEXTMENU_ADDPORTTYPE = "Add PortType";
    public const string CONTEXTMENU_ADDPARAM = "Add Parameter";
    public const string PORTTYPE_PACKAGE = "PortTypePackage";
    public const string PORTTYPEREFERENCE = "PortTypeReference";
    public const string PORTNAME = "PortName";
    public const string LAX = "lax";
    public const string STRICT = "strict";
    public const string ACTION = "action";
    public const string FIELD_UNIT_PERCENTAGE = "%";
    public const string FIELD_UNIT_PIXEL = "px";
    public const string FIELD_UNIT_EX = "ex";
    public const double CONTENTVIEW_ROWHEIGHT = 25.0;
  }
}
