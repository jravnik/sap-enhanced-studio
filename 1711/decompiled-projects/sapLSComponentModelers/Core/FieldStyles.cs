﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.FieldStyles
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public enum FieldStyles
  {
    Unknown = -1,
    Editable = 0,
    Readonly = 1,
    Disabled = 2,
    EditableFocused = 3,
    Error = 4,
    ErrorReadonly = 5,
    Warning = 6,
    WarningReadonly = 7,
    CellEditorLeadSel = 8,
    CellEditorSel = 9,
    CellEditorReadOnlyLeadSel = 10,
    CellEditorReadOnlySel = 11,
  }
}
