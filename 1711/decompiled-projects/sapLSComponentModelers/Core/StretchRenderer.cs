﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.StretchRenderer
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public class StretchRenderer
  {
    private Canvas canvas;
    private Image imgLeft;
    private Image imgRight;
    private double leftImageWidth;

    public StretchRenderer(Canvas canvas, ImageInfo leftImageInfo, ImageInfo rightImageInfo)
    {
      this.canvas = canvas;
      this.leftImageWidth = (double) leftImageInfo.Width;
      canvas.Height = (double) leftImageInfo.Height;
      this.imgLeft = ResourceUtil.GetImage(leftImageInfo);
      this.imgLeft.Stretch = Stretch.Fill;
      canvas.Children.Add((UIElement) this.imgLeft);
      this.imgRight = ResourceUtil.GetImage(rightImageInfo);
      this.imgRight.Stretch = Stretch.Fill;
      this.imgRight.StretchDirection = StretchDirection.Both;
      this.imgRight.SetValue(Canvas.LeftProperty, (object) (double) leftImageInfo.Width);
      canvas.Children.Add((UIElement) this.imgRight);
      this.LayoutSlicing();
      canvas.SizeChanged += new SizeChangedEventHandler(this.canvas_SizeChanged);
    }

    private void canvas_SizeChanged(object sender, SizeChangedEventArgs e)
    {
      this.LayoutSlicing();
    }

    private void LayoutSlicing()
    {
      double actualWidth = this.canvas.ActualWidth;
      if (double.IsInfinity(actualWidth) || double.IsNaN(actualWidth) || this.canvas.ActualWidth < this.leftImageWidth)
        return;
      this.imgLeft.Height = this.canvas.ActualHeight;
      this.imgRight.Height = this.canvas.ActualHeight;
      this.imgRight.Width = this.canvas.ActualWidth - this.leftImageWidth;
    }

    public bool Visible
    {
      get
      {
        return this.imgLeft.Visibility == Visibility.Visible;
      }
      set
      {
        Visibility visibility = value ? Visibility.Visible : Visibility.Collapsed;
        if (this.imgLeft.Visibility == visibility)
          return;
        this.imgLeft.Visibility = visibility;
        this.imgRight.Visibility = visibility;
      }
    }
  }
}
