﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.DragDropUtil
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Reflection;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public class DragDropUtil
  {
    public static System.Windows.Forms.IDataObject GetDataObject(System.Windows.IDataObject data)
    {
      System.Windows.Forms.IDataObject dataObject = (System.Windows.Forms.IDataObject) null;
      string name = "_innerData";
      object obj = data.GetType().GetField(name, BindingFlags.Instance | BindingFlags.NonPublic).GetValue((object) data);
      FieldInfo field = obj.GetType().GetField(name, BindingFlags.Instance | BindingFlags.NonPublic);
      if (field != (FieldInfo) null)
        dataObject = (System.Windows.Forms.IDataObject) (field.GetValue(obj) as System.Windows.Forms.DataObject);
      return dataObject;
    }
  }
}
