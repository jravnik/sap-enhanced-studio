﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.ComponentConstants
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public class ComponentConstants
  {
    public const string FORM_TITLE = "Save the WCF Component";
    public const string FOLDERSELECTIONDETAILS = "FolderSelectionDetails";
    public const string VIEW_SELECTION_STEP = "ViewSelectionStep";
  }
}
