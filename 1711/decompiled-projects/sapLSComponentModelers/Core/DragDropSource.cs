﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.DragDropSource
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.UICore.Core;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  internal class DragDropSource
  {
    private IDesignerControl m_SourceControl;
    private IDesignerControl m_ParentControl;

    public IDesignerControl SourceControl
    {
      get
      {
        return this.m_SourceControl;
      }
    }

    public IDesignerControl ParentControl
    {
      get
      {
        return this.m_ParentControl;
      }
    }

    public DragDropSource(IDesignerControl source, IDesignerControl parent)
    {
      this.m_SourceControl = source;
      this.m_ParentControl = parent;
    }
  }
}
