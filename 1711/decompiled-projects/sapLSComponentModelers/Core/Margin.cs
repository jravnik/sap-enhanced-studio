﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.Margin
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public class Margin
  {
    private int left;
    private int top;
    private int right;
    private int bottom;

    public Margin(int left, int top, int right, int bottom)
    {
      this.left = left;
      this.top = top;
      this.right = right;
      this.bottom = bottom;
    }

    public int Left
    {
      get
      {
        return this.left;
      }
    }

    public int Top
    {
      get
      {
        return this.top;
      }
    }

    public int Right
    {
      get
      {
        return this.right;
      }
    }

    public int Bottom
    {
      get
      {
        return this.bottom;
      }
    }
  }
}
