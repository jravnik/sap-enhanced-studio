﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.SlicingRenderingInfo
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public class SlicingRenderingInfo
  {
    private bool renderCenter = true;
    private const string OPTION_NOCENTER = "nocenter";
    private string resourcePrefix;
    private int hslicingLeft;
    private int hslicingCenter;
    private int hslicingRight;
    private int vsclicingTop;
    private int vsclicingMiddle;
    private int vsclicingBottom;
    private int topCenterHeight;
    private int bottomCenterHeight;
    private int leftMiddleWidth;
    private int rightMiddleWidth;

    public SlicingRenderingInfo(Properties skinProperties, string keyPrefix)
    {
      if (!keyPrefix.EndsWith("."))
        keyPrefix += ".";
      this.resourcePrefix = skinProperties.Get(keyPrefix + "resprefix", (string) null);
      string str1 = skinProperties.Get(keyPrefix + "hsclicing", (string) null);
      if (str1 != null)
      {
        string[] strArray = str1.Split(',');
        if (strArray.Length == 3)
        {
          this.hslicingLeft = int.Parse(strArray[0]);
          this.hslicingCenter = int.Parse(strArray[1]);
          this.hslicingRight = int.Parse(strArray[2]);
        }
      }
      string str2 = skinProperties.Get(keyPrefix + "vsclicing", (string) null);
      if (str2 == null)
        return;
      string[] strArray1 = str2.Split(',');
      if (strArray1.Length != 3)
        return;
      this.vsclicingTop = int.Parse(strArray1[0]);
      this.vsclicingMiddle = int.Parse(strArray1[1]);
      this.vsclicingBottom = int.Parse(strArray1[2]);
    }

    public int BottomCenterHeight
    {
      get
      {
        return this.bottomCenterHeight;
      }
    }

    public int TopCenterHeight
    {
      get
      {
        return this.topCenterHeight;
      }
    }

    public int LeftMiddleWidth
    {
      get
      {
        return this.leftMiddleWidth;
      }
    }

    public int RightMiddleWidth
    {
      get
      {
        return this.rightMiddleWidth;
      }
    }

    public SlicingRenderingInfo(string valueString)
    {
      if (valueString.StartsWith("tile::"))
        valueString = valueString.Substring("tile::".Length);
      else if (valueString.StartsWith("tile_3::"))
        valueString = valueString.Substring("tile_3::".Length);
      string[] strArray1 = valueString.Split(';');
      int length = strArray1.Length;
      this.resourcePrefix = strArray1[0] + ";" + strArray1[1];
      string str1 = strArray1[2];
      if (str1 != null)
      {
        string[] strArray2 = str1.Split(',');
        if (strArray2.Length == 3)
        {
          this.hslicingLeft = int.Parse(strArray2[0]);
          this.hslicingCenter = int.Parse(strArray2[1]);
          this.hslicingRight = int.Parse(strArray2[2]);
        }
      }
      string str2 = strArray1[3];
      if (str2 != null)
      {
        string[] strArray2 = str2.Split(',');
        if (strArray2.Length == 3)
        {
          this.vsclicingTop = int.Parse(strArray2[0]);
          this.vsclicingMiddle = int.Parse(strArray2[1]);
          this.vsclicingBottom = int.Parse(strArray2[2]);
        }
      }
      if (strArray1.Length > 4 && strArray1[4].Contains("nocenter"))
        this.renderCenter = false;
      if (strArray1.Length > 5)
      {
        string[] strArray2 = strArray1[5].Split(',');
        if (strArray2.Length == 4)
        {
          this.topCenterHeight = int.Parse(strArray2[0]);
          this.bottomCenterHeight = int.Parse(strArray2[1]);
          this.leftMiddleWidth = int.Parse(strArray2[2]);
          this.rightMiddleWidth = int.Parse(strArray2[2]);
        }
        else
        {
          this.topCenterHeight = -1;
          this.bottomCenterHeight = -1;
          this.leftMiddleWidth = -1;
          this.rightMiddleWidth = -1;
        }
      }
      else
      {
        this.topCenterHeight = -1;
        this.bottomCenterHeight = -1;
        this.leftMiddleWidth = -1;
        this.rightMiddleWidth = -1;
      }
    }

    public string ResourcePrefix
    {
      get
      {
        return this.resourcePrefix;
      }
    }

    public bool RenderCenter
    {
      get
      {
        return this.renderCenter;
      }
    }

    public int HSlicingLeft
    {
      get
      {
        return this.hslicingLeft;
      }
    }

    public int HSlicingCenter
    {
      get
      {
        return this.hslicingCenter;
      }
    }

    public int HSlicingRight
    {
      get
      {
        return this.hslicingRight;
      }
    }

    public int VSlicingTop
    {
      get
      {
        return this.vsclicingTop;
      }
    }

    public int VSlicingMiddle
    {
      get
      {
        return this.vsclicingMiddle;
      }
    }

    public int VSlicingBottom
    {
      get
      {
        return this.vsclicingBottom;
      }
    }
  }
}
