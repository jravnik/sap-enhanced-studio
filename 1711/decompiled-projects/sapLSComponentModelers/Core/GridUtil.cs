﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.GridUtil
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public class GridUtil
  {
    public static void AddRowDef(Grid grid, double height, GridUnitType unitType)
    {
      grid.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(height, unitType)
      });
    }

    public static void AddRowDef(Grid grid, double height, double minimumHeight, GridUnitType unitType)
    {
      grid.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(height, unitType),
        MinHeight = minimumHeight
      });
    }

    public static void AddColumnDef(Grid grid, double value, GridUnitType unitType)
    {
      grid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(value, unitType)
      });
    }

    public static void InsertColumnDef(Grid grid, double value, GridUnitType unitType, int index)
    {
      grid.ColumnDefinitions.Insert(index, new ColumnDefinition()
      {
        Width = new GridLength(value, unitType)
      });
    }

    public static void AddColumnDef(Grid grid, double value, double minimumWidth, GridUnitType unitType)
    {
      grid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(value, unitType),
        MinWidth = minimumWidth
      });
    }

    public static void PlaceElement(Grid grid, UIElement element, int row, int col)
    {
      GridUtil.PlaceElement(grid, element, row, col, 1, 1);
    }

    public static void PlaceElement(Grid grid, UIElement element, int row, int col, int rowSpan, int colSpan)
    {
      grid.Children.Add(element);
      element.SetValue(Grid.RowProperty, (object) row);
      element.SetValue(Grid.ColumnProperty, (object) col);
      if (colSpan > 1)
        element.SetValue(Grid.ColumnSpanProperty, (object) colSpan);
      if (rowSpan <= 1)
        return;
      element.SetValue(Grid.RowSpanProperty, (object) rowSpan);
    }

    public static void RemoveElement(Grid grid, UIElement element, bool isInsertColumnwise, int rowIndex, int colIndex)
    {
      grid.Children.Remove(element);
      if (!isInsertColumnwise)
        return;
      grid.ColumnDefinitions.RemoveAt(colIndex);
    }

    public static UIElement GetElementAt(Grid grid, int rowIndex, int colIndex)
    {
      foreach (UIElement child in grid.Children)
      {
        if (Grid.GetColumn(child) == colIndex && Grid.GetRow(child) == rowIndex)
          return child;
      }
      return (UIElement) null;
    }

    public static void RemoveRow(Grid grid, int rowIndex)
    {
      grid.RowDefinitions.RemoveAt(rowIndex);
    }

    public static void RemoveColumn(Grid grid, int colIndex)
    {
      grid.ColumnDefinitions.RemoveAt(colIndex);
    }

    public static void InsertElement(Grid grid, UIElement element, int row, int col, int rowSpan, int colSpan, int position)
    {
      grid.Children.Insert(position, element);
      if (row > 0)
        element.SetValue(Grid.RowProperty, (object) row);
      if (col > 0)
        element.SetValue(Grid.ColumnProperty, (object) col);
      if (colSpan > 1)
        element.SetValue(Grid.ColumnSpanProperty, (object) colSpan);
      if (rowSpan <= 1)
        return;
      element.SetValue(Grid.RowSpanProperty, (object) rowSpan);
    }

    public static void GetNextAvialableLocation(Grid contentGrid, ref int desiredRow, ref int desiredColumn, int desiredColSpan)
    {
      foreach (UIElement child in contentGrid.Children)
      {
        int column = Grid.GetColumn(child);
        int columnSpan = Grid.GetColumnSpan(child);
        if (Grid.GetRow(child) + Grid.GetRowSpan(child) > desiredRow && desiredColumn + desiredColSpan > column && desiredColumn < column + columnSpan)
        {
          desiredColumn = desiredColumn + columnSpan;
          if (desiredColumn >= contentGrid.ColumnDefinitions.Count)
          {
            desiredRow = desiredRow + 1;
            desiredColumn = 0;
          }
        }
      }
    }
  }
}
