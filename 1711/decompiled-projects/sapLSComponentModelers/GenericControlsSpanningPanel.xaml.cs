﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.SpanningPanel
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public partial class SpanningPanel : UserControl, IComponentConnector
  {
    //internal StackPanel contentGrid;
    //internal Button arrow;
    //internal Image arrowImg;
    //private bool _contentLoaded;

    public event EventHandler<EventArgs> LeftArrowClicked;

    public event EventHandler<EventArgs> RightArrowClicked;

    public event EventHandler<EventArgs> UpArrowClicked;

    public event EventHandler<EventArgs> DownArrowClicked;

    public SpanningPanel()
    {
      this.InitializeComponent();
      this.arrow.Click += new RoutedEventHandler(this.upArrow_Click);
      this.arrow.Click += new RoutedEventHandler(this.leftArrow_Click);
      this.arrow.Click += new RoutedEventHandler(this.rightArrow_Click);
      this.arrow.Click += new RoutedEventHandler(this.downArrow_Click);
      this.contentGrid.Background = (Brush) Brushes.Transparent;
      this.Background = (Brush) Brushes.Transparent;
    }

    public SpanningPanel(SpanningDirection direction)
    {
      this.InitializeComponent();
      if (direction == SpanningDirection.Left)
      {
        this.arrow.Click += new RoutedEventHandler(this.leftArrow_Click);
        this.arrow.Visibility = Visibility.Visible;
        this.arrowImg.LayoutTransform = (Transform) new RotateTransform(180.0, 0.5, 0.5);
      }
      if (direction == SpanningDirection.Right)
      {
        this.arrow.Click += new RoutedEventHandler(this.rightArrow_Click);
        this.arrow.Visibility = Visibility.Visible;
      }
      if (direction == SpanningDirection.Top)
      {
        this.arrow.Click += new RoutedEventHandler(this.upArrow_Click);
        this.arrow.Visibility = Visibility.Visible;
        this.arrowImg.LayoutTransform = (Transform) new RotateTransform(270.0, 0.5, 0.5);
      }
      if (direction == SpanningDirection.Bottom)
      {
        this.arrow.Click += new RoutedEventHandler(this.downArrow_Click);
        this.arrow.Visibility = Visibility.Visible;
        this.arrowImg.LayoutTransform = (Transform) new RotateTransform(90.0, 0.5, 0.5);
      }
      this.contentGrid.Background = (Brush) Brushes.Transparent;
      this.Background = (Brush) Brushes.Transparent;
    }

    private void downArrow_Click(object sender, RoutedEventArgs e)
    {
      if (this.DownArrowClicked == null)
        return;
      this.DownArrowClicked((object) this, EventArgs.Empty);
    }

    private void rightArrow_Click(object sender, RoutedEventArgs e)
    {
      if (this.RightArrowClicked == null)
        return;
      this.RightArrowClicked((object) this, EventArgs.Empty);
    }

    private void leftArrow_Click(object sender, RoutedEventArgs e)
    {
      if (this.LeftArrowClicked == null)
        return;
      this.LeftArrowClicked((object) this, EventArgs.Empty);
    }

    private void upArrow_Click(object sender, RoutedEventArgs e)
    {
      if (this.UpArrowClicked == null)
        return;
      this.UpArrowClicked((object) this, EventArgs.Empty);
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/genericcontrols/spanningpanel.xaml", UriKind.Relative));
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.contentGrid = (StackPanel) target;
    //      break;
    //    case 2:
    //      this.arrow = (Button) target;
    //      break;
    //    case 3:
    //      this.arrowImg = (Image) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
