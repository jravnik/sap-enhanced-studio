﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.GAFDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class GAFDesigner : ComponentDesigner
  {
    private static readonly SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox m_ComponentToolbox = ToolboxUtilities.DeserialzeToolboxComponent(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.GuidedActivityToolbox);
    private bool handleComboBoxSelectionChangedEvent = true;
    private VisualIdentificationRegion idrControl;
    private VisualRoadmapContextualNavigationRegion cnrControl;
    private VisualView viewControl;
    private NavigationItem currentSelectedNavigationItem;
    private ComboBox comboBox;

    public override SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox Toolbox
    {
      get
      {
        return GAFDesigner.m_ComponentToolbox;
      }
    }

    internal GuidedActivityFloorplan Component
    {
      get
      {
        return this.m_ModelObject as GuidedActivityFloorplan;
      }
    }

    public GAFDesigner(IModelObject rootModel)
      : base(rootModel, true, true)
    {
      this.AllowDrop = true;
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.AddIdentificationRegion();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.AddContextualNavigationRegion();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      this.comboBox = new ComboBox();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.comboBox, 3, 0);
      this.comboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboBox_SelectionChanged);
      this.AddContentRegion();
      this.LoadLinkedControlRegion();
    }

    private void OnRoadmapSelectionChanged(object sender, RoutedEventArgs e)
    {
      if (!(e.OriginalSource is VisualRoadmapItem))
        return;
      this.AddContentRegion();
    }

    private void AddContentRegion()
    {
      if (this.viewControl != null)
      {
        this.UnSuscribeStepVariantEvents();
        this.viewControl.Terminate();
        this.Grid.Children.Remove((UIElement) this.viewControl);
      }
      this.currentSelectedNavigationItem = this.Component.ContextualNavigationRegion.GuidedActivitySteps[this.cnrControl.RoadmapControl.SelectedIndex];
      View associatedView;
      if (this.currentSelectedNavigationItem.ChildNavigationItems.Count > 0)
      {
        associatedView = Utilities.GetAssociatedView(this.Component.Views, this.currentSelectedNavigationItem.ChildNavigationItems[this.cnrControl.RoadmapControl.SelectedChildIndex]);
        this.comboBox.Visibility = Visibility.Visible;
        this.LoadComboBoxItems();
      }
      else
      {
        associatedView = Utilities.GetAssociatedView(this.Component.Views, this.currentSelectedNavigationItem);
        this.comboBox.Visibility = Visibility.Collapsed;
      }
      this.viewControl = new VisualView((IModelObject) associatedView);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.viewControl, 2, 0);
    }

    private void LoadComboBoxItems()
    {
      this.comboBox.Items.Clear();
      this.comboBox.DisplayMemberPath = "Name";
      foreach (NavigationItem childNavigationItem in this.currentSelectedNavigationItem.ChildNavigationItems)
      {
        this.comboBox.Items.Add((object) childNavigationItem);
        childNavigationItem.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnStepVariantNamePropertyChanged);
      }
      if (this.comboBox.Items.Count <= 0)
        return;
      this.comboBox.SelectedIndex = 0;
    }

    private void OnStepVariantNamePropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        if (!(e.PropertyName == NavigationItem.NavigationItemIdProperty))
          return;
        this.handleComboBoxSelectionChangedEvent = false;
        int selectedIndex = this.comboBox.SelectedIndex;
        this.LoadComboBoxItems();
        this.comboBox.SelectedIndex = selectedIndex;
        this.handleComboBoxSelectionChangedEvent = true;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to reflect the step variant property change", ex));
      }
    }

    private void AddIdentificationRegion()
    {
      if (this.idrControl != null)
      {
        this.idrControl.Terminate();
        this.Grid.Children.Remove((UIElement) this.idrControl);
      }
      this.idrControl = new VisualIdentificationRegion((IModelObject) this.Component.IdentificationRegion);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.idrControl, 0, 0);
    }

    private void AddContextualNavigationRegion()
    {
      if (this.cnrControl != null)
      {
        this.cnrControl.Terminate();
        this.Grid.Children.Remove((UIElement) this.cnrControl);
      }
      this.cnrControl = new VisualRoadmapContextualNavigationRegion((IModelObject) this.Component.ContextualNavigationRegion);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.cnrControl, 1, 0);
      this.cnrControl.AddHandler(VisualRoadmapItem.SelectedEvent, (Delegate) new RoutedEventHandler(this.OnRoadmapSelectionChanged));
    }

    private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.comboBox.SelectedIndex != -1 && this.handleComboBoxSelectionChangedEvent)
      {
        this.cnrControl.RoadmapControl.SelectedChildIndex = this.comboBox.SelectedIndex;
        NavigationItem selectedItem = this.comboBox.SelectedItem as NavigationItem;
        if (selectedItem == null)
          return;
        if (this.viewControl != null)
        {
          this.viewControl.Terminate();
          this.Grid.Children.Remove((UIElement) this.viewControl);
        }
        this.viewControl = new VisualView((IModelObject) Utilities.GetAssociatedView(this.Component.Views, selectedItem));
        GridUtil.PlaceElement(this.Grid, (UIElement) this.viewControl, 2, 0);
        this.RaiseSelectionChanged((IDesignerControl) null, (IModelObject) selectedItem);
      }
      else
        this.cnrControl.RoadmapControl.SelectedChildIndex = 0;
    }

    public override void Terminate()
    {
      this.UnSuscribeStepVariantEvents();
      base.Terminate();
    }

    private void UnSuscribeStepVariantEvents()
    {
      if (this.currentSelectedNavigationItem == null || this.currentSelectedNavigationItem.ChildNavigationItems == null || this.currentSelectedNavigationItem.ChildNavigationItems.Count <= 0)
        return;
      foreach (ModelObject childNavigationItem in this.currentSelectedNavigationItem.ChildNavigationItems)
        childNavigationItem.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnStepVariantNamePropertyChanged);
    }
  }
}
