﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea
{
  public class TabArea : BaseSelectableControl
  {
    private const string HOME_ICON_BACKBRUSH_STANDARD = "#E9F0F5,0.0,#EDF1F5,0.3,#FAFAFB,1.0";
    private const string HOME_ICON_BACKBRUSH_SELECTED = "#DFE6EE,0.0,#D8E0EA,0.16,#B6C4D9,0.35,#CDD6E5,0.6,#FAFAFB,1.0";
    private const string HOME_ICON_BACKBRUSH_HOVER = "#E1E8F0,0.0,#E9EDF3,0.3,#DEE6ED,0.5,#E9EDF3,0.7,#FAFAFB,1.0";
    private const string WC_ICON_SEPERATOR = "sapLSUISkins;component/skin.nova/Images/Controls/Shell/TabArea/WorkCenterSeparator.png,1,60";
    private TabIcon tabIcon;
    private static bool initialized;
    private static Brush backBrushStandard;
    private static Brush backBrushSelected;
    private static Brush backBrushHover;
    private static ImageInfo separatorInfo;
    private List<TabIcon> tabIcons;
    private StackPanel itemPanel;
    private double iconwidth;

    private void InitializeResources()
    {
      SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea.backBrushStandard = (Brush) ResourceUtil.GetLinearGradientBrush("#E9F0F5,0.0,#EDF1F5,0.3,#FAFAFB,1.0");
      SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea.backBrushSelected = (Brush) ResourceUtil.GetLinearGradientBrush("#DFE6EE,0.0,#D8E0EA,0.16,#B6C4D9,0.35,#CDD6E5,0.6,#FAFAFB,1.0");
      SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea.backBrushHover = (Brush) ResourceUtil.GetLinearGradientBrush("#E1E8F0,0.0,#E9EDF3,0.3,#DEE6ED,0.5,#E9EDF3,0.7,#FAFAFB,1.0");
      SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea.separatorInfo = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Shell/TabArea/WorkCenterSeparator.png,1,60");
      SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea.initialized = true;
    }

    public TabArea(ICenterStructure centerStructure)
      : base((IModelObject) centerStructure)
    {
      if (!SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea.initialized)
        this.InitializeResources();
      Canvas canvas = new Canvas();
      canvas.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.Content = (object) canvas;
      this.itemPanel = new StackPanel();
      this.itemPanel.Orientation = Orientation.Horizontal;
      canvas.Children.Add((UIElement) this.itemPanel);
      this.tabIcons = new List<TabIcon>();
      this.tabIcon = new TabIcon(centerStructure);
      this.tabIcon.OnTitleChanged += new TitleChanged(this.tabIcon_OnTitleChanged);
      this.itemPanel.Children.Add((UIElement) this.tabIcon);
      this.tabIcons.Add(this.tabIcon);
      Image image = ResourceUtil.GetImage(SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea.separatorInfo);
      image.VerticalAlignment = VerticalAlignment.Center;
      this.itemPanel.Children.Add((UIElement) image);
      this.iconwidth = this.iconwidth + this.tabIcon.getTabIconWidth() + (double) SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea.separatorInfo.Width;
      this.Width = this.iconwidth;
    }

    public ImageInfo SeparatorInfo
    {
      get
      {
        return SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea.separatorInfo;
      }
    }

    public override string SelectionText
    {
      get
      {
        return "WorkCenterItem";
      }
    }

    public new void Terminate()
    {
      if (this.tabIcons != null)
      {
        foreach (TabIcon tabIcon in this.tabIcons)
          tabIcon.Terminate();
        this.tabIcons.Clear();
        this.tabIcons = (List<TabIcon>) null;
      }
      if (this.itemPanel == null)
        return;
      this.itemPanel.Children.Clear();
    }

    private void tabIcon_OnTitleChanged(ICenterStructure structure)
    {
      foreach (UIElement child in this.itemPanel.Children)
      {
        if (child is TabIcon)
        {
          this.tabIcon = new TabIcon(structure);
          this.tabIcon.OnTitleChanged += new TitleChanged(this.tabIcon_OnTitleChanged);
          break;
        }
      }
      this.itemPanel.Children.RemoveAt(0);
      this.itemPanel.Children.Insert(0, (UIElement) this.tabIcon);
    }

    public List<TabIcon> getTabIconList()
    {
      return this.tabIcons;
    }

    public override void SelectMe()
    {
    }

    public override void UnSelect()
    {
    }
  }
}
