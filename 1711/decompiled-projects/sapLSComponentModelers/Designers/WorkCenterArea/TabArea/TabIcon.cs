﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabIcon
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea
{
  public class TabIcon : BaseSelectableControl
  {
    private const string HOME_ICON_BACKBRUSH_STANDARD = "#E9F0F5,0.0,#EDF1F5,0.3,#FAFAFB,1.0";
    private const string HOME_ICON_BACKBRUSH_SELECTED = "#DFE6EE,0.0,#D8E0EA,0.16,#B6C4D9,0.35,#CDD6E5,0.6,#FAFAFB,1.0";
    private const string HOME_ICON_BACKBRUSH_HOVER = "#E1E8F0,0.0,#E9EDF3,0.3,#DEE6ED,0.5,#E9EDF3,0.7,#FAFAFB,1.0";
    private const string WC_ICON_RESOURCE = "/sapLSUISkins;component/skin.nova/Images/Controls/Shell/TabArea/Workcenter{0}.png,32,32";
    private const string WCICON_RESOURCE_STRING = "skin::wcicons.{0}";
    private static bool initialized;
    private static Brush backBrushStandard;
    private static Brush backBrushSelected;
    private static Brush backBrushHover;
    private static ImageInfo standardImageInfo;
    private static ImageInfo hoverImageInfo;
    private static ImageInfo downImageInfo;
    private ImageInfo instanceStandardImageInfo;
    private ImageInfo instanceHoverImageInfo;
    private ImageInfo instanceDownImageInfo;
    private static Thickness tabAreaMargin;
    private static int textBottomDistance;
    private Canvas highlightCanvas;
    private Image iconImage;
    private TextBlock iconText;
    private bool hover;
    private bool IsSelected;
    private ICenterStructure centerStructure;

    public event TitleChanged OnTitleChanged;

    private void InitializeControl()
    {
      TabIcon.backBrushStandard = (Brush) ResourceUtil.GetLinearGradientBrush("#E9F0F5,0.0,#EDF1F5,0.3,#FAFAFB,1.0");
      TabIcon.backBrushSelected = (Brush) ResourceUtil.GetLinearGradientBrush("#DFE6EE,0.0,#D8E0EA,0.16,#B6C4D9,0.35,#CDD6E5,0.6,#FAFAFB,1.0");
      TabIcon.backBrushHover = (Brush) ResourceUtil.GetLinearGradientBrush("#E1E8F0,0.0,#E9EDF3,0.3,#DEE6ED,0.5,#E9EDF3,0.7,#FAFAFB,1.0");
      TabIcon.standardImageInfo = ResourceUtil.GetImageInfo("/sapLSUISkins;component/skin.nova/Images/Controls/Shell/TabArea/Workcenter{0}.png,32,32", "Standard");
      TabIcon.hoverImageInfo = ResourceUtil.GetImageInfo("/sapLSUISkins;component/skin.nova/Images/Controls/Shell/TabArea/Workcenter{0}.png,32,32", "Hover");
      TabIcon.downImageInfo = ResourceUtil.GetImageInfo("/sapLSUISkins;component/skin.nova/Images/Controls/Shell/TabArea/Workcenter{0}.png,32,32", "Down");
      TabIcon.tabAreaMargin = HomeIcon.MARGIN_TAB_AREA;
      int.TryParse("23", out TabIcon.textBottomDistance);
      this.IsSelected = true;
      TabIcon.initialized = true;
    }

    public TabIcon(ICenterStructure structure)
      : base((IModelObject) structure)
    {
      this.centerStructure = structure;
      this.LoadView();
    }

    public override void LoadView()
    {
      if (!TabIcon.initialized)
        this.InitializeControl();
      Canvas canvas = new Canvas();
      canvas.MouseEnter += new MouseEventHandler(this.backCanvas_MouseEnter);
      canvas.MouseLeave += new MouseEventHandler(this.backCanvas_MouseLeave);
      canvas.MouseLeftButtonDown += new MouseButtonEventHandler(this.backCanvas_MouseLeftButtonDown);
      this.Height = TabIcon.tabAreaMargin.Bottom;
      this.Content = (object) canvas;
      this.SizeChanged += new SizeChangedEventHandler(this.TabIcon_SizeChanged);
      this.InitializeImageResources();
      this.highlightCanvas = new Canvas();
      this.highlightCanvas.Width = this.Width;
      this.highlightCanvas.Background = TabIcon.backBrushStandard;
      this.highlightCanvas.Height = TabIcon.tabAreaMargin.Bottom;
      canvas.Children.Add((UIElement) this.highlightCanvas);
      this.iconImage = new Image();
      this.iconImage.Width = (double) this.instanceStandardImageInfo.Width;
      this.iconImage.Height = (double) this.instanceStandardImageInfo.Height;
      this.iconImage.Source = ResourceUtil.RetrieveImage(TabIcon.standardImageInfo.Name);
      canvas.Children.Add((UIElement) this.iconImage);
      this.iconText = new TextBlock();
      this.iconText.TextAlignment = TextAlignment.Center;
      this.iconText.MinWidth = 70.0;
      this.iconText.TextWrapping = TextWrapping.NoWrap;
      this.iconText.IsHitTestVisible = false;
      FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.iconText);
      canvas.Children.Add((UIElement) this.iconText);
      this.UpdateText();
      this.UpdateImageIcon();
    }

    private void InitializeImageResources()
    {
      string str = (string) null;
      if (str == null)
      {
        this.instanceDownImageInfo = TabIcon.downImageInfo;
        this.instanceHoverImageInfo = TabIcon.hoverImageInfo;
        this.instanceStandardImageInfo = TabIcon.standardImageInfo;
      }
      else
      {
        string iconResourceString = VisualConfiguration.Instance.GetWorkcenterIconResourceString(string.Format("skin::wcicons.{0}", (object) str));
        if (iconResourceString != null)
        {
          this.instanceStandardImageInfo = ConfigResourceAccessor.Instance.GetImageInfo(string.Format(iconResourceString, (object) "Standard"));
          this.instanceHoverImageInfo = ConfigResourceAccessor.Instance.GetImageInfo(string.Format(iconResourceString, (object) "Hover"));
          this.instanceDownImageInfo = ConfigResourceAccessor.Instance.GetImageInfo(string.Format(iconResourceString, (object) "Down"));
        }
        if (this.instanceStandardImageInfo == null)
          this.instanceStandardImageInfo = TabIcon.standardImageInfo;
        if (this.instanceHoverImageInfo == null)
          this.instanceHoverImageInfo = TabIcon.hoverImageInfo;
        if (this.instanceDownImageInfo != null)
          return;
        this.instanceDownImageInfo = TabIcon.downImageInfo;
      }
    }

    private void viewNavigationNode_SelectionChanged(object sender, EventArgs e)
    {
      if (this.Dispatcher.CheckAccess())
        this.UpdateImageIcon();
      else
        this.Dispatcher.BeginInvoke((Delegate) new Action(this.UpdateImageIcon));
    }

    private void viewNavigationNode_TitleChanged(object sender, EventArgs e)
    {
      if (this.Dispatcher.CheckAccess())
        this.UpdateText();
      else
        this.Dispatcher.BeginInvoke((Delegate) new Action(this.UpdateText));
    }

    private void TabIcon_SizeChanged(object sender, SizeChangedEventArgs e)
    {
      double num = 30.0 + this.iconText.ActualWidth;
      if (this.Width != num)
      {
        this.Width = num;
      }
      else
      {
        this.highlightCanvas.Width = this.Width;
        this.iconImage.SetValue(Canvas.TopProperty, (object) (this.Height - (double) TabIcon.standardImageInfo.Height - 25.0));
        this.iconImage.SetValue(Canvas.LeftProperty, (object) ((this.Width - this.iconImage.Width) / 2.0));
        this.iconText.SetValue(Canvas.TopProperty, (object) (this.Height - (double) TabIcon.textBottomDistance));
        this.iconText.SetValue(Canvas.LeftProperty, (object) 10.0);
      }
    }

    private void backCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      this.IsSelected = true;
      this.UpdateImageIcon();
    }

    private void backCanvas_MouseLeave(object sender, MouseEventArgs e)
    {
      this.hover = false;
      this.UpdateImageIcon();
    }

    private void backCanvas_MouseEnter(object sender, MouseEventArgs e)
    {
      this.hover = true;
      this.UpdateImageIcon();
    }

    public override string SelectionText
    {
      get
      {
        return "WorkCenterItem";
      }
    }

    private void UpdateText()
    {
      string str = "WorkCenterView";
      if (this.centerStructure != null && this.centerStructure.Title != null)
        str = this.centerStructure.Title.Text;
      this.iconText.Text = str;
    }

    private void UpdateTextProperty()
    {
      if (this.OnTitleChanged == null)
        return;
      this.OnTitleChanged(this.centerStructure);
    }

    private void UpdateImageIcon()
    {
      if (this.centerStructure.IconId != null && this.centerStructure.IconId != string.Empty)
      {
        string particleString = VisualConfiguration.Instance.GetWorkcenterImagePath("wcicons.com.sap.byd.wcicon." + this.centerStructure.IconId) ?? "/sapLSUISkins;component/skin.nova/Images/Controls/Shell/TabArea/Workcenter{0}.png,32,32";
        ImageInfo imageInfo1 = ResourceUtil.GetImageInfo(particleString, "Standard");
        ImageInfo imageInfo2 = ResourceUtil.GetImageInfo(particleString, "Hover");
        ImageInfo imageInfo3 = ResourceUtil.GetImageInfo(particleString, "Down");
        if (this.IsSelected && imageInfo1 != null)
        {
          this.highlightCanvas.Background = TabIcon.backBrushSelected;
          this.iconImage.Source = ResourceUtil.RetrieveImage(imageInfo1.Name);
          this.iconImage.Width = (double) imageInfo3.Width;
          this.iconImage.Height = (double) imageInfo3.Height;
        }
        else if (this.hover && imageInfo2 != null)
        {
          this.highlightCanvas.Background = TabIcon.backBrushHover;
          this.iconImage.Source = ResourceUtil.RetrieveImage(imageInfo1.Name);
          this.iconImage.Width = (double) imageInfo2.Width;
          this.iconImage.Height = (double) imageInfo2.Height;
        }
        else
        {
          if (imageInfo3 == null)
            return;
          this.highlightCanvas.Background = TabIcon.backBrushStandard;
          this.iconImage.Source = ResourceUtil.RetrieveImage(imageInfo1.Name);
          this.iconImage.Width = (double) imageInfo1.Width;
          this.iconImage.Height = (double) imageInfo1.Height;
        }
      }
      else if (this.IsSelected)
      {
        this.highlightCanvas.Background = TabIcon.backBrushSelected;
        this.iconImage.Source = ResourceUtil.RetrieveImage(TabIcon.standardImageInfo.Name);
        this.iconImage.Width = (double) TabIcon.downImageInfo.Width;
        this.iconImage.Height = (double) TabIcon.downImageInfo.Height;
      }
      else if (this.hover)
      {
        this.highlightCanvas.Background = TabIcon.backBrushHover;
        this.iconImage.Source = ResourceUtil.RetrieveImage(TabIcon.standardImageInfo.Name);
        this.iconImage.Width = (double) TabIcon.hoverImageInfo.Width;
        this.iconImage.Height = (double) TabIcon.hoverImageInfo.Height;
      }
      else
      {
        this.highlightCanvas.Background = TabIcon.backBrushStandard;
        this.iconImage.Source = ResourceUtil.RetrieveImage(TabIcon.standardImageInfo.Name);
        this.iconImage.Width = (double) TabIcon.standardImageInfo.Width;
        this.iconImage.Height = (double) TabIcon.standardImageInfo.Height;
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == "Title")
      {
        this.UpdateTextProperty();
      }
      else
      {
        if (!(e.PropertyName == "IconId"))
          return;
        this.UpdateImageIcon();
      }
    }

    public double getTabIconWidth()
    {
      return this.iconText.ActualWidth + 30.0;
    }

    public new void Terminate()
    {
    }

    public override void SelectMe()
    {
    }

    public override void UnSelect()
    {
    }
  }
}
