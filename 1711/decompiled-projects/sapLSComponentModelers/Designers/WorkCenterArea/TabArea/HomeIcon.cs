﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.HomeIcon
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea
{
  public class HomeIcon : UserControl
  {
    public static readonly Thickness MARGING_HOME_ICON = new Thickness(13.0, 38.0, 112.0, 64.0);
    public static readonly Thickness MARGIN_TAB_AREA = new Thickness((double) sbyte.MaxValue, 69.0, 50.0, 64.0);
    private const string HOME_ICON_BACKBRUSH_STANDARD = "#E9F0F5,0.0,#EDF1F5,0.3,#FAFAFB,1.0";
    private const string HOME_ICON_BACKBRUSH_SELECTED = "#DFE6EE,0.0,#D8E0EA,0.16,#B6C4D9,0.35,#CDD6E5,0.6,#FAFAFB,1.0";
    private const string HOME_ICON_BACKBRUSH_HOVER = "#E1E8F0,0.0,#E9EDF3,0.3,#DEE6ED,0.5,#E9EDF3,0.7,#FAFAFB,1.0";
    private const string HOME_ICON_RESOURCE = "sapLSUISkins;component/skin.nova/Images/WorkCenterIcons/Home{0}.png,113,95";
    private static bool initialized;
    private static Brush backBrushStandard;
    private static Brush backBrushSelected;
    private static Brush backBrushHover;
    private static ImageInfo standardImageInfo;
    private static ImageInfo hoverImageInfo;
    private static ImageInfo downImageInfo;
    private static Thickness homeIconMargin;
    private static Thickness tabAreaMargin;
    private static int textBottomDistance;
    private Canvas highlightCanvas;
    private Image iconImage;
    private TextBlock iconText;
    private bool hover;
    private bool IsSelected;

    private void InitializeResources()
    {
      HomeIcon.backBrushStandard = (Brush) ResourceUtil.GetLinearGradientBrush("#E9F0F5,0.0,#EDF1F5,0.3,#FAFAFB,1.0");
      HomeIcon.backBrushSelected = (Brush) ResourceUtil.GetLinearGradientBrush("#DFE6EE,0.0,#D8E0EA,0.16,#B6C4D9,0.35,#CDD6E5,0.6,#FAFAFB,1.0");
      HomeIcon.backBrushHover = (Brush) ResourceUtil.GetLinearGradientBrush("#E1E8F0,0.0,#E9EDF3,0.3,#DEE6ED,0.5,#E9EDF3,0.7,#FAFAFB,1.0");
      HomeIcon.standardImageInfo = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/WorkCenterIcons/Home{0}.png,113,95", "Standard");
      HomeIcon.hoverImageInfo = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/WorkCenterIcons/Home{0}.png,113,95", "Hover");
      HomeIcon.downImageInfo = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/WorkCenterIcons/Home{0}.png,113,95", "Down");
      HomeIcon.homeIconMargin = HomeIcon.MARGING_HOME_ICON;
      HomeIcon.tabAreaMargin = HomeIcon.MARGIN_TAB_AREA;
      int.TryParse("23", out HomeIcon.textBottomDistance);
      HomeIcon.initialized = true;
    }

    public HomeIcon()
    {
      if (!HomeIcon.initialized)
        this.InitializeResources();
      Canvas canvas = new Canvas();
      canvas.MouseEnter += new MouseEventHandler(this.backCanvas_MouseEnter);
      canvas.MouseLeave += new MouseEventHandler(this.backCanvas_MouseLeave);
      canvas.MouseLeftButtonDown += new MouseButtonEventHandler(this.backCanvas_MouseLeftButtonDown);
      this.Content = (object) canvas;
      this.Width = HomeIcon.homeIconMargin.Right;
      this.Height = HomeIcon.homeIconMargin.Bottom;
      this.highlightCanvas = new Canvas();
      this.highlightCanvas.Width = this.Width;
      this.highlightCanvas.Background = HomeIcon.backBrushStandard;
      this.highlightCanvas.Height = HomeIcon.tabAreaMargin.Bottom;
      this.highlightCanvas.SetValue(Canvas.TopProperty, (object) (this.Height - this.highlightCanvas.Height));
      canvas.Children.Add((UIElement) this.highlightCanvas);
      this.iconImage = new Image();
      this.iconImage.Width = (double) HomeIcon.standardImageInfo.Width;
      this.iconImage.Height = (double) HomeIcon.standardImageInfo.Height;
      this.iconImage.Source = ResourceUtil.RetrieveImage(HomeIcon.standardImageInfo.Name);
      this.iconImage.SetValue(Canvas.TopProperty, (object) (this.Height - this.iconImage.Height + 3.0));
      canvas.Children.Add((UIElement) this.iconImage);
      this.iconText = new TextBlock();
      this.iconText.SetValue(Canvas.TopProperty, (object) (this.Height - (double) HomeIcon.textBottomDistance));
      this.iconText.SetValue(Canvas.LeftProperty, (object) 5.0);
      this.iconText.Width = this.Width - 10.0;
      this.iconText.TextAlignment = TextAlignment.Center;
      this.iconText.IsHitTestVisible = false;
      this.iconText.TextWrapping = TextWrapping.NoWrap;
      FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.iconText);
      canvas.Children.Add((UIElement) this.iconText);
      this.UpdateText();
      this.UpdateImageIcon();
    }

    private void Instance_PersonalizationModeChanged(object sender, EventArgs e)
    {
    }

    private void viewNavigationNode_SelectionChanged(object sender, EventArgs e)
    {
      if (this.Dispatcher.CheckAccess())
        this.UpdateImageIcon();
      else
        this.Dispatcher.BeginInvoke((Delegate) new Action(this.UpdateImageIcon));
    }

    private void viewNavigationNode_TitleChanged(object sender, EventArgs e)
    {
      if (this.Dispatcher.CheckAccess())
        this.UpdateText();
      else
        this.Dispatcher.BeginInvoke((Delegate) new Action(this.UpdateText));
    }

    private void backCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      this.IsSelected = true;
      this.UpdateImageIcon();
    }

    private void backCanvas_MouseLeave(object sender, MouseEventArgs e)
    {
      this.hover = false;
      this.UpdateImageIcon();
    }

    private void backCanvas_MouseEnter(object sender, MouseEventArgs e)
    {
      this.hover = true;
      this.UpdateImageIcon();
    }

    private void UpdateText()
    {
      this.iconText.Text = "Home";
    }

    private void UpdateImageIcon()
    {
      if (this.IsSelected)
      {
        this.highlightCanvas.Background = HomeIcon.backBrushSelected;
        this.iconImage.Source = ResourceUtil.RetrieveImage(HomeIcon.downImageInfo.Name);
      }
      else if (this.hover)
      {
        this.highlightCanvas.Background = HomeIcon.backBrushHover;
        this.iconImage.Source = ResourceUtil.RetrieveImage(HomeIcon.hoverImageInfo.Name);
      }
      else
      {
        this.highlightCanvas.Background = HomeIcon.backBrushStandard;
        this.iconImage.Source = ResourceUtil.RetrieveImage(HomeIcon.standardImageInfo.Name);
      }
    }

    public int MarginTop
    {
      get
      {
        return (int) HomeIcon.homeIconMargin.Top;
      }
    }

    public int MarginLeft
    {
      get
      {
        return (int) HomeIcon.homeIconMargin.Left;
      }
    }

    public void Terminate()
    {
    }
  }
}
