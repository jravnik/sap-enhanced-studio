﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabScroller
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea
{
  public class TabScroller : UserControl
  {
    private Canvas contentArea;
    private Image image;

    public TabScroller()
    {
      this.contentArea = new Canvas();
      this.Content = (object) this.contentArea;
      this.Width = this.contentArea.Width;
      this.Height = this.contentArea.Height;
    }

    public void AddLayoutElements(string configResource)
    {
      ImageInfo imageInfo = ConfigResourceAccessor.Instance.GetImageInfo(configResource);
      this.image = new Image();
      this.image.Width = (double) imageInfo.Width;
      this.image.Height = (double) imageInfo.Height;
      this.image.Source = ResourceUtil.RetrieveImage(imageInfo.Name);
      this.contentArea.Children.Add((UIElement) this.image);
      this.contentArea.Width = (double) imageInfo.Width;
      this.contentArea.Height = (double) imageInfo.Height;
      this.Width = this.contentArea.Width;
      this.Height = this.contentArea.Height;
    }
  }
}
