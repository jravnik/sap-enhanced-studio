﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep.OverviewInfo
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep
{
  public partial class OverviewInfo : System.Windows.Controls.UserControl
  {
    private const string WCVIEW_UICOMPONENT_EXTENSION = ".WCVIEW.uiwocview";
    private const string EMBEDDED_UICOMPONENT = ".EC.uicomponent";
    public List<string> m_WCFFolderDetails;
    public List<string> m_OverviewECDetails;
    public List<string> m_OverviewWCViewDetails;
    public bool IsExistingFile;
    //internal Grid rootGrid;
    //internal System.Windows.Controls.GroupBox groupBoxOverview;
    //internal StackPanel stackPanelOveriew;
    //internal TextBlock lblQuickLinkECPersistPath;
    //internal StackPanel quickLinkECPersistSelection;
    //internal TextBlock lblName;
    //internal System.Windows.Controls.TextBox txtboxQLECName;
    //internal System.Windows.Controls.TextBox txtboxQLECComponent;
    //internal System.Windows.Controls.Button fileSelector;
    //internal System.Windows.Controls.Button folderSelector;
    //internal TextBlock lblQuickLinkWCViewPersistPath;
    //internal StackPanel quickLinkWCViewSelection;
    //internal TextBlock lblWCName;
    //internal System.Windows.Controls.TextBox txtboxQLWCName;
    //internal System.Windows.Controls.TextBox txtboxQLWCComponent;
    //internal System.Windows.Controls.Button btnBrowse5;
    //internal StackPanel shortidstack;
    //internal TextBlock lblShortId;
    //internal System.Windows.Controls.TextBox txtboxShortId;
    //private bool _contentLoaded;

    public OverviewInfo()
    {
      this.InitializeComponent();
    }

    public OverviewInfo(ViewSelectionStep parent)
    {
      this.InitializeComponent();
      if (parent != null)
        this.InitializeMembers(parent);
      this.SetECComponentDetails();
      this.SetWCViewComponentDetails();
    }

    public void InitializeMembers(ViewSelectionStep parent)
    {
      this.m_WCFFolderDetails = parent.m_FolderDetails;
      this.m_OverviewECDetails = this.GetClone(this.m_WCFFolderDetails, this.m_OverviewECDetails);
      this.m_OverviewECDetails[1] = this.m_OverviewECDetails[1] + "_QL_EC";
      this.m_OverviewWCViewDetails = this.GetClone(this.m_WCFFolderDetails, this.m_OverviewWCViewDetails);
      this.m_OverviewWCViewDetails.Add(this.txtboxShortId.Text);
      this.m_OverviewWCViewDetails[1] = this.m_OverviewWCViewDetails[1] + "_Overview_WCView";
    }

    private List<string> GetClone(List<string> wcfDetails, List<string> clonedList)
    {
      clonedList = new List<string>();
      clonedList.Add(wcfDetails[0]);
      clonedList.Add(wcfDetails[1]);
      clonedList.Add(wcfDetails[2]);
      return clonedList;
    }

    public void SetECComponentDetails()
    {
      if (this.m_OverviewECDetails == null || this.m_OverviewECDetails.Count < 3)
        return;
      this.txtboxQLECName.Text = this.m_OverviewECDetails[1];
      this.txtboxQLECComponent.Text = this.m_OverviewECDetails[0] + "/" + this.m_OverviewECDetails[1] + ".EC.uicomponent";
    }

    public void SetWCViewComponentDetails()
    {
      if (this.m_OverviewWCViewDetails == null || this.m_OverviewWCViewDetails.Count < 3)
        return;
      this.txtboxQLWCName.Text = this.m_OverviewWCViewDetails[1];
      this.txtboxQLWCComponent.Text = this.m_OverviewWCViewDetails[0] + "/" + this.m_OverviewWCViewDetails[1] + ".WCVIEW.uiwocview";
    }

    public void ClearValues()
    {
      this.txtboxQLECName.Text = string.Empty;
      this.txtboxQLECComponent.Text = string.Empty;
      this.txtboxQLWCName.Text = string.Empty;
      this.txtboxQLWCComponent.Text = string.Empty;
    }

    private void OpenOverviewECFolderSelector(object sender, RoutedEventArgs e)
    {
      FolderSelectionDialog folderSelectionDialog = new FolderSelectionDialog(FloorplanType.EC, this.txtboxQLECName.Text);
      if (folderSelectionDialog.ShowDialog() != DialogResult.OK)
        return;
      this.m_OverviewECDetails = new List<string>();
      this.m_OverviewECDetails.Add(folderSelectionDialog.RepositoryPath);
      this.m_OverviewECDetails.Add(folderSelectionDialog.ConfigurationName);
      this.m_OverviewECDetails.Add(folderSelectionDialog.DevClass);
      this.txtboxQLECComponent.Text = folderSelectionDialog.RepositoryPath + "/" + folderSelectionDialog.ConfigurationName + ".EC.uicomponent";
      this.txtboxQLECName.Text = folderSelectionDialog.ConfigurationName;
    }

    private void OpenOverviewWCViewFolderSelector(object sender, RoutedEventArgs e)
    {
      FolderSelectionDialog folderSelectionDialog = new FolderSelectionDialog(FloorplanType.WCVIEW, this.txtboxQLWCName.Text);
      if (folderSelectionDialog.ShowDialog() != DialogResult.OK)
        return;
      this.m_OverviewWCViewDetails = new List<string>();
      this.m_OverviewWCViewDetails.Add(folderSelectionDialog.RepositoryPath);
      this.m_OverviewWCViewDetails.Add(folderSelectionDialog.ConfigurationName);
      this.m_OverviewWCViewDetails.Add(folderSelectionDialog.DevClass);
      this.m_OverviewWCViewDetails.Add(folderSelectionDialog.ShortId);
      this.txtboxQLWCComponent.Text = folderSelectionDialog.RepositoryPath + "/" + folderSelectionDialog.ConfigurationName + ".WCVIEW.uiwocview";
      this.txtboxQLWCName.Text = folderSelectionDialog.ConfigurationName;
      this.txtboxShortId.Text = folderSelectionDialog.ShortId;
    }

    private void OpenOverviewECFileSelector(object sender, RoutedEventArgs e)
    {
      RepositoryBrowser instance = RepositoryBrowser.GetInstance();
      instance.OnFloorplanSelection -= new RepositoryBrowser.OnFloorPlanSelection(this.OnECComponentSelected);
      instance.OnFloorplanSelection += new RepositoryBrowser.OnFloorPlanSelection(this.OnECComponentSelected);
      int num = (int) instance.ShowDialog();
    }

    private bool OnECComponentSelected(object sender, EventArgs e)
    {
      try
      {
        if (sender is FloorplanInfo)
        {
          FloorplanInfo floorplanInfo = sender as FloorplanInfo;
          if (floorplanInfo != null)
          {
            if (floorplanInfo.ConfigurationType.ToLower() == FloorplanType.EC.ToString().ToLower())
            {
              this.m_OverviewECDetails = new List<string>();
              this.m_OverviewECDetails.Add(floorplanInfo.RepositoryPath);
              this.m_OverviewECDetails.Add(floorplanInfo.Name);
              this.m_OverviewECDetails.Add(floorplanInfo.DevClass);
              this.txtboxQLECName.Text = floorplanInfo.Name;
              this.txtboxQLECComponent.Text = floorplanInfo.RepositoryPath + "/" + this.m_OverviewECDetails[1] + ".EC.uicomponent";
              this.IsExistingFile = true;
              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Exception selecting the EC Component", ex));
      }
      return false;
    }

    private void OnECNameChanged(object sender, RoutedEventArgs e)
    {
      if (this.m_OverviewECDetails != null)
      {
        this.txtboxQLECComponent.Text = this.m_OverviewECDetails[0] + "/" + this.txtboxQLECName.Text + ".EC.uicomponent";
        this.m_OverviewECDetails[1] = this.txtboxQLECName.Text;
      }
      else
      {
        if (this.txtboxQLECComponent.Text == null || !(this.txtboxQLECComponent.Text != ""))
          return;
        int num = this.txtboxQLECComponent.Text.LastIndexOf('/');
        this.txtboxQLECComponent.Text = this.txtboxQLECComponent.Text.Remove(num + 1, this.txtboxQLECComponent.Text.Length - (num + 1)) + this.txtboxQLECName.Text + ".EC.uicomponent";
      }
    }

    private void OnWCViewNameChanged(object sender, RoutedEventArgs e)
    {
      if (this.m_OverviewWCViewDetails != null)
      {
        this.txtboxQLWCComponent.Text = this.m_OverviewWCViewDetails[0] + "/" + this.txtboxQLWCName.Text + ".WCVIEW.uiwocview";
        this.m_OverviewWCViewDetails[1] = this.txtboxQLWCName.Text;
      }
      else
      {
        if (this.txtboxQLWCComponent.Text == null || !(this.txtboxQLWCComponent.Text != ""))
          return;
        int num = this.txtboxQLWCComponent.Text.LastIndexOf('/');
        this.txtboxQLWCComponent.Text = this.txtboxQLWCComponent.Text.Remove(num + 1, this.txtboxQLWCComponent.Text.Length - (num + 1)) + this.txtboxQLWCName.Text + ".WCVIEW.uiwocview";
      }
    }

    private void OnShortIdChanged(object sender, RoutedEventArgs e)
    {
      if (this.m_OverviewWCViewDetails == null)
        return;
      if (new Regex("^[_a-zA-Z0-9]*$").IsMatch(this.txtboxShortId.Text))
      {
        if (this.m_OverviewWCViewDetails.Count <= 3)
          this.m_OverviewWCViewDetails.Add(this.txtboxShortId.Text);
        if (this.m_OverviewWCViewDetails[2] == "" || this.m_OverviewWCViewDetails[2] == "$TMP" && !this.txtboxShortId.Text.StartsWith("Z"))
          this.m_OverviewWCViewDetails[3] = "Z" + this.txtboxShortId.Text;
        else
          this.m_OverviewWCViewDetails[3] = this.txtboxShortId.Text;
      }
      else
      {
        int num = (int) DisplayMessage.Show("Special charaters are not allowed other than Underscore", MessageBoxButtons.OK);
        this.txtboxShortId.Text = this.txtboxShortId.Text.Remove(this.txtboxShortId.Text.Length - 1, 1);
      }
    }

    public void Reload()
    {
      this.SetECComponentDetails();
      this.SetWCViewComponentDetails();
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/designers/workcenterarea/wcfwizardstep/overviewinfo.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.rootGrid = (Grid) target;
    //      break;
    //    case 2:
    //      this.groupBoxOverview = (System.Windows.Controls.GroupBox) target;
    //      break;
    //    case 3:
    //      this.stackPanelOveriew = (StackPanel) target;
    //      break;
    //    case 4:
    //      this.lblQuickLinkECPersistPath = (TextBlock) target;
    //      break;
    //    case 5:
    //      this.quickLinkECPersistSelection = (StackPanel) target;
    //      break;
    //    case 6:
    //      this.lblName = (TextBlock) target;
    //      break;
    //    case 7:
    //      this.txtboxQLECName = (System.Windows.Controls.TextBox) target;
    //      this.txtboxQLECName.TextChanged += new TextChangedEventHandler(this.OnECNameChanged);
    //      break;
    //    case 8:
    //      this.txtboxQLECComponent = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 9:
    //      this.fileSelector = (System.Windows.Controls.Button) target;
    //      this.fileSelector.Click += new RoutedEventHandler(this.OpenOverviewECFolderSelector);
    //      break;
    //    case 10:
    //      this.folderSelector = (System.Windows.Controls.Button) target;
    //      this.folderSelector.Click += new RoutedEventHandler(this.OpenOverviewECFileSelector);
    //      break;
    //    case 11:
    //      this.lblQuickLinkWCViewPersistPath = (TextBlock) target;
    //      break;
    //    case 12:
    //      this.quickLinkWCViewSelection = (StackPanel) target;
    //      break;
    //    case 13:
    //      this.lblWCName = (TextBlock) target;
    //      break;
    //    case 14:
    //      this.txtboxQLWCName = (System.Windows.Controls.TextBox) target;
    //      this.txtboxQLWCName.TextChanged += new TextChangedEventHandler(this.OnWCViewNameChanged);
    //      break;
    //    case 15:
    //      this.txtboxQLWCComponent = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 16:
    //      this.btnBrowse5 = (System.Windows.Controls.Button) target;
    //      this.btnBrowse5.Click += new RoutedEventHandler(this.OpenOverviewWCViewFolderSelector);
    //      break;
    //    case 17:
    //      this.shortidstack = (StackPanel) target;
    //      break;
    //    case 18:
    //      this.lblShortId = (TextBlock) target;
    //      break;
    //    case 19:
    //      this.txtboxShortId = (System.Windows.Controls.TextBox) target;
    //      this.txtboxShortId.TextChanged += new TextChangedEventHandler(this.OnShortIdChanged);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
