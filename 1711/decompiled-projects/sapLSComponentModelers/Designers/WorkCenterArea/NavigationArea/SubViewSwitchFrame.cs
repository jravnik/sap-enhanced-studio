﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea.SubViewSwitchFrame
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea
{
  public class SubViewSwitchFrame : BaseSelectableControl
  {
    private List<VisualRadioButton> subViewList = new List<VisualRadioButton>();
    private static Margin marginContent = new Margin(20, 15, 20, 15);
    private static Margin marginItem = new Margin(0, 0, 0, 0);
    internal const int CNR_WIDTH = 210;
    private const string RESOURCE_BACKGROUND_TILE = "skin::control.shell.navpanel.subviewswitch_background";
    private readonly Grid contentGrid;
    private readonly BackgroundTileControl backgroundTileControl;
    private readonly Grid optionPanel;

    public event ViewSelected OnSubViewSelected;

    public IViewSwitch ViewSwitch
    {
      get
      {
        return this.m_ModelObject as IViewSwitch;
      }
    }

    public SubViewSwitchFrame(IModelObject view)
      : base(view)
    {
      this.m_ModelObject = view;
      this.contentGrid = new Grid();
      this.backgroundTileControl = new BackgroundTileControl(ConfigResourceAccessor.Instance.GetSlicingRenderingInfo("tile::sapLSUISkins;component/skin.nova/Images/Controls/Shell/NavPanel/SubViewSwitch;40,1,14;14,1,14"));
      this.contentGrid.Children.Add((UIElement) this.backgroundTileControl);
      this.backgroundTileControl.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.backgroundTileControl.VerticalAlignment = VerticalAlignment.Stretch;
      this.optionPanel = new Grid();
      this.optionPanel.Margin = new Thickness((double) SubViewSwitchFrame.marginContent.Left, (double) SubViewSwitchFrame.marginContent.Top, (double) SubViewSwitchFrame.marginContent.Right, (double) SubViewSwitchFrame.marginContent.Bottom);
      this.optionPanel.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.contentGrid.Children.Add((UIElement) this.optionPanel);
      this.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.LoadView();
    }

    public override void LoadView()
    {
      this.Clear();
      VisualSubViewSwitch subViewSwitch1 = new VisualSubViewSwitch((IModelObject) this.ViewSwitch);
      subViewSwitch1.ControlBackground = (Brush) Brushes.Transparent;
      this.AddOption(subViewSwitch1, this.ViewSwitch);
      this.Width = 210.0;
      this.subViewList.Clear();
      for (int index = 0; index < this.ViewSwitch.SubViewSwitches.Count; ++index)
      {
        IViewSwitch subViewSwitch2 = (IViewSwitch) this.ViewSwitch.SubViewSwitches[index];
        VisualSubViewSwitch subViewSwitch3 = new VisualSubViewSwitch((IModelObject) subViewSwitch2);
        subViewSwitch3.OnSubViewSelected += new SubViewSelected(this.subViewSwitch_OnViewSelected);
        this.subViewList.Add(subViewSwitch3.OptionButton);
        if (index == 0)
          this.subViewSwitch_OnViewSelected((object) subViewSwitch3.OptionButton);
        subViewSwitch3.ControlBackground = (Brush) Brushes.Transparent;
        this.AddOption(subViewSwitch3, subViewSwitch2);
      }
      this.Content = (object) this.contentGrid;
    }

    public void Clear()
    {
      this.optionPanel.Children.Clear();
      this.optionPanel.RowDefinitions.Clear();
    }

    public void AddOption(VisualSubViewSwitch subViewSwitch, IViewSwitch subView)
    {
      this.optionPanel.RowDefinitions.Add(new RowDefinition());
      this.optionPanel.Children.Add((UIElement) subViewSwitch);
      subViewSwitch.SetValue(Grid.RowProperty, (object) (this.optionPanel.Children.Count - 1));
      subViewSwitch.SetValue(Grid.ColumnProperty, (object) 0);
      subViewSwitch.Margin = new Thickness((double) SubViewSwitchFrame.marginItem.Left, (double) SubViewSwitchFrame.marginItem.Top, (double) SubViewSwitchFrame.marginItem.Right, (double) SubViewSwitchFrame.marginItem.Bottom);
      this.UpdateHeight();
    }

    private void UpdateHeight()
    {
      this.Height = (double) (SubViewSwitchFrame.marginContent.Top + SubViewSwitchFrame.marginContent.Bottom + this.optionPanel.Children.Count * 20);
    }

    public override string SelectionText
    {
      get
      {
        return "WorkCenterSubView";
      }
    }

    public override void SelectMe()
    {
    }

    public override void UnSelect()
    {
    }

    private void subViewSwitch_OnViewSelected(object sender)
    {
      VisualRadioButton visualRadioButton = sender as VisualRadioButton;
      if (visualRadioButton == null)
        return;
      foreach (VisualRadioButton subView in this.subViewList)
      {
        if (subView != visualRadioButton)
          subView.IsChecked = new bool?(false);
        else
          subView.IsChecked = new bool?(true);
      }
      IViewSwitch tag = visualRadioButton.Tag as IViewSwitch;
      if (this.OnSubViewSelected == null)
        return;
      this.OnSubViewSelected(tag);
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      this.ReloadView();
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      this.ReloadView();
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      this.ReloadView();
    }
  }
}
