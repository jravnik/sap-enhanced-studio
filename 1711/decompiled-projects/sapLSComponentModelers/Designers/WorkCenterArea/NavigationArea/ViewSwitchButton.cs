﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea.ViewSwitchButton
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea
{
  public class ViewSwitchButton : Canvas
  {
    private static ImageInfo stretchResourceNormalLeft;
    private static ImageInfo stretchResourceNormalRight;
    private static ImageInfo stretchResourceHoverLeft;
    private static ImageInfo stretchResourceHoverRight;
    private static ImageInfo stretchResourceSelectedLeft;
    private static ImageInfo stretchResourceSelectedRight;
    private static Brush textBrush;
    private static Thickness textMargin;
    private TextBlock itemText;
    private StretchRenderer normalRenderer;
    private StretchRenderer hoverRenderer;
    private StretchRenderer selectedRenderer;
    private bool hover;
    private bool selected;
    private IViewSwitch viewNavigationNode;
    private int index;

    public event EventHandler<EventArgs> ButtonClicked;

    static ViewSwitchButton()
    {
      VisualConfiguration instance1 = VisualConfiguration.Instance;
      ConfigResourceAccessor instance2 = ConfigResourceAccessor.Instance;
      ViewSwitchButton.stretchResourceNormalLeft = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Shell/NavPanel/NavItemNormalLeft.png,30,25");
      ViewSwitchButton.stretchResourceNormalRight = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Shell/NavPanel/NavitemNormalRight.png,1,25");
      ViewSwitchButton.stretchResourceHoverLeft = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Shell/NavPanel/NavItemHoverLeft.png,30,25");
      ViewSwitchButton.stretchResourceHoverRight = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Shell/NavPanel/NavitemHoverRight.png,1,25");
      ViewSwitchButton.stretchResourceSelectedLeft = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Shell/NavPanel/NavitemSelectedLeft.png,30,25");
      ViewSwitchButton.stretchResourceSelectedRight = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Shell/NavPanel/NavitemSelectedRight.png,1,25");
      ViewSwitchButton.textBrush = (Brush) SkinConstants.BRUSH_CONTROLSHELLNAVPANEL_SWITCHITEMSTEXT;
      ViewSwitchButton.textMargin = SkinConstants.MARGIN_CONTROLSHELLNAVPANEL_SWITCHITEMSTEXT;
    }

    public ViewSwitchButton(IViewSwitch viewNavigationNode, int index)
    {
      if (viewNavigationNode != null)
      {
        this.viewNavigationNode = viewNavigationNode;
        this.index = index;
        this.HorizontalAlignment = HorizontalAlignment.Stretch;
        this.normalRenderer = new StretchRenderer((Canvas) this, ViewSwitchButton.stretchResourceNormalLeft, ViewSwitchButton.stretchResourceNormalRight);
        this.hoverRenderer = new StretchRenderer((Canvas) this, ViewSwitchButton.stretchResourceHoverLeft, ViewSwitchButton.stretchResourceHoverRight);
        this.selectedRenderer = new StretchRenderer((Canvas) this, ViewSwitchButton.stretchResourceSelectedLeft, ViewSwitchButton.stretchResourceSelectedRight);
        this.itemText = new TextBlock();
        this.itemText.VerticalAlignment = VerticalAlignment.Stretch;
        FontManager.Instance.SetFontStyle1("Arial,11,Bold", this.itemText);
        this.itemText.Foreground = ViewSwitchButton.textBrush;
        this.itemText.SetValue(Canvas.LeftProperty, (object) ViewSwitchButton.textMargin.Left);
        this.itemText.SetValue(Canvas.TopProperty, (object) ViewSwitchButton.textMargin.Top);
        this.Children.Add((UIElement) this.itemText);
        this.itemText.IsHitTestVisible = false;
        this.UpdateText();
        this.UpdateBackground();
        this.MouseEnter += new MouseEventHandler(this.ViewSwitchButton_MouseEnter);
        this.MouseLeave += new MouseEventHandler(this.ViewSwitchButton_MouseLeave);
        this.MouseLeftButtonDown += new MouseButtonEventHandler(this.ViewSwitchButton_MouseLeftButtonDown);
      }
      else
      {
        this.index = index;
        this.HorizontalAlignment = HorizontalAlignment.Stretch;
        this.normalRenderer = new StretchRenderer((Canvas) this, ViewSwitchButton.stretchResourceNormalLeft, ViewSwitchButton.stretchResourceNormalRight);
        this.hoverRenderer = new StretchRenderer((Canvas) this, ViewSwitchButton.stretchResourceHoverLeft, ViewSwitchButton.stretchResourceHoverRight);
        this.selectedRenderer = new StretchRenderer((Canvas) this, ViewSwitchButton.stretchResourceSelectedLeft, ViewSwitchButton.stretchResourceSelectedRight);
        this.UpdateBackground();
      }
    }

    public void Terminate()
    {
      if (this.viewNavigationNode != null)
        this.viewNavigationNode = (IViewSwitch) null;
      this.Children.Clear();
    }

    private void viewNavigationNode_TitleChanged(object sender, EventArgs e)
    {
      if (this.Dispatcher.CheckAccess())
        this.UpdateText();
      else
        this.Dispatcher.BeginInvoke((Delegate) new Action(this.UpdateText));
    }

    private void ViewSwitchButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      if (this.ButtonClicked == null)
        return;
      this.ButtonClicked((object) this, EventArgs.Empty);
    }

    private void ViewSwitchButton_MouseLeave(object sender, MouseEventArgs e)
    {
      this.hover = false;
      this.UpdateBackground();
    }

    private void ViewSwitchButton_MouseEnter(object sender, MouseEventArgs e)
    {
      this.hover = true;
      this.UpdateBackground();
    }

    public bool Selected
    {
      get
      {
        return this.selected;
      }
      set
      {
        this.selected = value;
        this.UpdateBackground();
      }
    }

    public int Index
    {
      get
      {
        return this.index;
      }
    }

    public void UpdateText()
    {
      string str = "WorkCenterView";
      if (this.viewNavigationNode != null && this.viewNavigationNode.Text != null)
        str = this.viewNavigationNode.Text.Text;
      if (!(str != this.itemText.Text))
        return;
      this.itemText.Text = str;
    }

    public void UpdateBackground()
    {
      if (this.selected)
      {
        this.normalRenderer.Visible = false;
        this.hoverRenderer.Visible = false;
        this.selectedRenderer.Visible = true;
      }
      else if (this.hover)
      {
        this.normalRenderer.Visible = false;
        this.hoverRenderer.Visible = true;
        this.selectedRenderer.Visible = false;
      }
      else
      {
        this.normalRenderer.Visible = true;
        this.hoverRenderer.Visible = false;
        this.selectedRenderer.Visible = false;
      }
    }
  }
}
