﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea.VisualSubViewSwitch
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea
{
  public class VisualSubViewSwitch : BaseSelectableControl
  {
    internal const int CNR_WIDTH = 210;
    private VisualRadioButton selectedOptionButton;
    private static Brush switchItemBrush;
    private VisualRadioButton radioButton;

    public event SubViewSelected OnSubViewSelected;

    public IViewSwitch SubView
    {
      get
      {
        return this.m_ModelObject as IViewSwitch;
      }
    }

    public VisualRadioButton OptionButton
    {
      get
      {
        return this.radioButton;
      }
    }

    public VisualSubViewSwitch(IModelObject model)
      : base(model)
    {
      this.Margin = new Thickness(0.0);
      VisualSubViewSwitch.switchItemBrush = (Brush) SkinConstants.BRUSH_CONTROLSHELLNAVPANEL_SWITCHITEMSTEXT;
      this.LoadView();
    }

    public override void LoadView()
    {
      if (this.SubView != null && this.SubView.SubViewSwitches.Count == 0)
      {
        this.radioButton = new VisualRadioButton(this.SubView.Text != null ? this.SubView.Text.Text : "", VisualRadioButton.VisualRadioButtonStyles.SubViewSwitchOption);
        this.radioButton.Click += new RoutedEventHandler(this.radioButton_Click);
        this.radioButton.Checked += new RoutedEventHandler(this.radioButton_Checked);
        this.radioButton.Tag = (object) this.SubView;
        this.radioButton.Foreground = VisualSubViewSwitch.switchItemBrush;
        this.Grid.Children.Add((UIElement) this.radioButton);
        this.radioButton.SetValue(Grid.RowProperty, (object) 0);
        this.radioButton.SetValue(Grid.ColumnProperty, (object) 0);
        this.radioButton.HorizontalAlignment = HorizontalAlignment.Stretch;
        this.radioButton.VerticalAlignment = VerticalAlignment.Stretch;
      }
      else
      {
        TextBlock textBlock = new TextBlock();
        FontManager.Instance.SetFontStyle1("Arial,11,Bold", textBlock);
        textBlock.Text = string.Format("{0} Views", this.SubView.Text != null ? (object) this.SubView.Text.Text : (object) "");
        textBlock.Tag = (object) this.SubView;
        textBlock.Foreground = VisualSubViewSwitch.switchItemBrush;
        this.Grid.Children.Add((UIElement) textBlock);
        textBlock.SetValue(Grid.RowProperty, (object) 0);
        textBlock.SetValue(Grid.ColumnProperty, (object) 0);
        textBlock.HorizontalAlignment = HorizontalAlignment.Stretch;
        textBlock.VerticalAlignment = VerticalAlignment.Stretch;
      }
    }

    public override string SelectionText
    {
      get
      {
        return "WorkCenterSubView";
      }
    }

    public override void SelectMe()
    {
    }

    public override void UnSelect()
    {
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("WorkCenterSubView", this.AddContextMenuItem("Add"));
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (Convert.ToString(source.Header).Equals("WorkCenterSubView"))
      {
        OberonViewSwitch modelObject = this.m_ModelObject as OberonViewSwitch;
        if (modelObject == null || modelObject.Parent == null)
          return;
        OberonViewSwitch parent = modelObject.Parent as OberonViewSwitch;
        if (parent != null)
        {
          parent.AddSubView();
        }
        else
        {
          if (!(modelObject.Parent is OberonCenterStructure))
            return;
          modelObject.AddSubView();
        }
      }
      else
      {
        if (!Convert.ToString(source.Header).Equals("Delete"))
          return;
        OberonViewSwitch modelObject = this.m_ModelObject as OberonViewSwitch;
        if (modelObject == null || modelObject.Parent == null)
          return;
        OberonViewSwitch parent = modelObject.Parent as OberonViewSwitch;
        if (parent == null)
          return;
        parent.RemoveSubView(modelObject);
      }
    }

    private void radioButton_Checked(object sender, RoutedEventArgs e)
    {
      this.selectedOptionButton = (VisualRadioButton) sender;
      if (this.selectedOptionButton == null || this.OnSubViewSelected == null)
        return;
      this.OnSubViewSelected(sender);
    }

    private void radioButton_Click(object sender, RoutedEventArgs e)
    {
      BaseSelectableControl selectableControl = ComponentModelerUtilities.Instance.GetBaseSelectableControl((FrameworkElement) this);
      if (selectableControl == null)
        return;
      selectableControl.TriggerSelection(selectableControl, false);
    }

    protected override void OnMouseEnter(MouseEventArgs e)
    {
    }

    protected override void OnMouseLeave(MouseEventArgs e)
    {
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (!(e.PropertyName == "Text"))
        return;
      if (this.radioButton != null)
        this.radioButton.Text = this.SubView.Text.Text;
      else
        this.ReloadView();
    }
  }
}
