﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea.BackgroundTileControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea
{
  public class BackgroundTileControl : Grid
  {
    private const string SLICING_SUFFIX_TOP_LEFT = "TopLeft.png";
    private const string SLICING_SUFFIX_TOP_CENTER = "TopCenter.png";
    private const string SLICING_SUFFIX_TOP_RIGHT = "TopRight.png";
    private const string SLICING_SUFFIX_MIDDLE_LEFT = "MiddleLeft.png";
    private const string SLICING_SUFFIX_MIDDLE_CENTER = "MiddleCenter.png";
    private const string SLICING_SUFFIX_MIDDLE_RIGHT = "MiddleRight.png";
    private const string SLICING_SUFFIX_BOTTOM_LEFT = "BottomLeft.png";
    private const string SLICING_SUFFIX_BOTTOM_CENTER = "BottomCenter.png";
    private const string SLICING_SUFFIX_BOTTOM_RIGHT = "BottomRight.png";

    public BackgroundTileControl(SlicingRenderingInfo renderingInfo)
    {
      this.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength((double) renderingInfo.HSlicingLeft, GridUnitType.Pixel)
      });
      this.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Star)
      });
      this.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength((double) renderingInfo.HSlicingRight, GridUnitType.Pixel)
      });
      this.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength((double) renderingInfo.VSlicingTop, GridUnitType.Pixel)
      });
      this.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(1.0, GridUnitType.Star)
      });
      this.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength((double) renderingInfo.VSlicingBottom, GridUnitType.Pixel)
      });
      string resourcePrefix = renderingInfo.ResourcePrefix;
      Image image1 = this.AddSlicingImage(resourcePrefix, "TopLeft.png", 0, 0, Stretch.None, HorizontalAlignment.Left, VerticalAlignment.Top);
      Image image2 = this.AddSlicingImage(resourcePrefix, "TopCenter.png", 1, 0, Stretch.Fill, HorizontalAlignment.Stretch, VerticalAlignment.Top);
      if (renderingInfo.TopCenterHeight != -1)
        image2.Height = (double) renderingInfo.TopCenterHeight;
      image1 = this.AddSlicingImage(resourcePrefix, "TopRight.png", 2, 0, Stretch.None, HorizontalAlignment.Right, VerticalAlignment.Top);
      Image image3 = this.AddSlicingImage(resourcePrefix, "MiddleLeft.png", 0, 1, Stretch.Fill, HorizontalAlignment.Left, VerticalAlignment.Stretch);
      if (renderingInfo.LeftMiddleWidth != -1)
        image3.Width = (double) renderingInfo.LeftMiddleWidth;
      if (renderingInfo.RenderCenter)
        image1 = this.AddSlicingImage(resourcePrefix, "MiddleCenter.png", 1, 1, Stretch.Fill, HorizontalAlignment.Stretch, VerticalAlignment.Stretch);
      Image image4 = this.AddSlicingImage(resourcePrefix, "MiddleRight.png", 2, 1, Stretch.Fill, HorizontalAlignment.Right, VerticalAlignment.Stretch);
      if (renderingInfo.RightMiddleWidth != -1)
        image4.Width = (double) renderingInfo.RightMiddleWidth;
      image1 = this.AddSlicingImage(resourcePrefix, "BottomLeft.png", 0, 2, Stretch.None, HorizontalAlignment.Left, VerticalAlignment.Bottom);
      Image image5 = this.AddSlicingImage(resourcePrefix, "BottomCenter.png", 1, 2, Stretch.Fill, HorizontalAlignment.Stretch, VerticalAlignment.Bottom);
      if (renderingInfo.BottomCenterHeight != -1)
        image5.Height = (double) renderingInfo.BottomCenterHeight;
      image1 = this.AddSlicingImage(resourcePrefix, "BottomRight.png", 2, 2, Stretch.None, HorizontalAlignment.Right, VerticalAlignment.Bottom);
    }

    private Image AddSlicingImage(string resourcePrefix, string resourceSuffix, int col, int row, Stretch stretch, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment)
    {
      Image image = new Image();
      image.Source = ResourceUtil.RetrieveImage(resourcePrefix + resourceSuffix);
      image.SetValue(Grid.RowProperty, (object) row);
      image.SetValue(Grid.ColumnProperty, (object) col);
      image.Stretch = stretch;
      image.HorizontalAlignment = horizontalAlignment;
      image.VerticalAlignment = verticalAlignment;
      this.Children.Add((UIElement) image);
      return image;
    }
  }
}
