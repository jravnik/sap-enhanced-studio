﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea.VisualViewSwitch
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea
{
  public class VisualViewSwitch : BaseSelectableControl
  {
    internal const int CNR_WIDTH = 210;
    private const string FORMTITLE = "Save the WCF Component";
    private ViewSwitchButton ViewButton;

    public event EventHandler SubViewRegionChanged;

    public VisualViewSwitch(IModelObject model)
      : base(model)
    {
      this.Margin = new Thickness(0.0);
      this.LoadView();
    }

    public override void LoadView()
    {
    }

    public override string SelectionText
    {
      get
      {
        return "WorkCenterView";
      }
    }

    public override void SelectMe()
    {
    }

    public override void UnSelect()
    {
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      if (LoginManager.Instance.DTMode == DTModes.Partner)
        return;
      bool flag1 = false;
      bool flag2 = false;
      bool flag3 = false;
      System.Windows.Controls.MenuItem parentItem = this.AddContextMenuItem("Add");
      OberonViewSwitch modelObject = this.m_ModelObject as OberonViewSwitch;
      if (modelObject != null && modelObject.Parent != null)
      {
        OberonCenterStructure parent = modelObject.Parent as OberonCenterStructure;
        if (parent != null)
        {
          foreach (OberonViewSwitch viewSwitch in parent.ViewSwitches)
          {
            if (viewSwitch.ViewSwitchTypes == ViewSwitchTypes.Overview)
              flag1 = true;
            else if (viewSwitch.ViewSwitchTypes == ViewSwitchTypes.MyTasks)
              flag2 = true;
            else if (viewSwitch.ViewSwitchTypes == ViewSwitchTypes.Reports)
              flag3 = true;
          }
        }
        if (!flag1)
          this.AddContextMenuItem("Overview", parentItem);
        if (!flag2)
          this.AddContextMenuItem("My Tasks", parentItem);
        this.AddContextMenuItem("WorkCenterView", parentItem);
        this.AddContextMenuItem("WorkCenterSubView", parentItem);
        if (!flag3)
          this.AddContextMenuItem("Reports", parentItem);
      }
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      OberonViewSwitch modelObject = this.m_ModelObject as OberonViewSwitch;
      if (modelObject == null || modelObject.Parent == null)
        return;
      OberonCenterStructure parent1 = modelObject.Parent as OberonCenterStructure;
      WorkCenterFloorplan parent2 = parent1.Parent as WorkCenterFloorplan;
      if (parent1 == null)
        return;
      if (source.Header.Equals((object) "Overview"))
      {
        if (DisplayMessage.Show("Do you want to Auto create the Quick links components", MessageBoxButtons.YesNo) == DialogResult.Yes)
        {
          ControlDialog controlDialog = new ControlDialog(true, (IModelObject) parent2);
          if (controlDialog.ShowDialog() != DialogResult.OK || !controlDialog.getTextdetails())
            return;
          List<string> ecDetails = new List<string>();
          int startIndex = controlDialog.m_OInfo.txtboxQLECComponent.Text.LastIndexOf('/');
          string str = controlDialog.m_OInfo.txtboxQLECComponent.Text.Remove(startIndex, controlDialog.m_OInfo.txtboxQLECComponent.Text.Length - startIndex);
          ecDetails.Add(str);
          ecDetails.Add(controlDialog.m_OInfo.txtboxQLECName.Text);
          ModelLayer.ProjectWorkspaceManager.GetComponentDetails(controlDialog.m_OInfo.txtboxQLECComponent.Text);
          ecDetails.Add(parent2.DevClass);
          List<string> wcviewDetails = new List<string>();
          wcviewDetails.Add(parent2.RepositoryPathValue);
          wcviewDetails.Add(controlDialog.m_OInfo.txtboxQLWCName.Text);
          wcviewDetails.Add(parent2.DevClass);
          if (parent2.DevClass == "" || parent2.DevClass == "$TMP" && !controlDialog.m_OInfo.txtboxShortId.Text.StartsWith("Z"))
            wcviewDetails.Add("Z" + controlDialog.m_OInfo.txtboxShortId.Text);
          else
            wcviewDetails.Add(controlDialog.m_OInfo.txtboxShortId.Text);
          parent1.AddOverview(ecDetails, wcviewDetails, controlDialog.m_OInfo.IsExistingFile);
        }
        else
          parent1.AddOverview();
      }
      else if (source.Header.Equals((object) "My Tasks"))
      {
        if (DisplayMessage.Show("Do you want to Auto create the OWL MyTasks component", MessageBoxButtons.YesNo) == DialogResult.Yes)
        {
          ControlDialog controlDialog = new ControlDialog(false, (IModelObject) parent2);
          if (controlDialog.ShowDialog() != DialogResult.OK || !controlDialog.getTextdetails())
            return;
          List<string> ecDetails = new List<string>();
          int startIndex = controlDialog.m_MyTasksInfo.txtboxOWLComponent.Text.LastIndexOf('/');
          string str = controlDialog.m_MyTasksInfo.txtboxOWLComponent.Text.Remove(startIndex, controlDialog.m_MyTasksInfo.txtboxOWLComponent.Text.Length - startIndex);
          ecDetails.Add(str);
          ecDetails.Add(controlDialog.m_MyTasksInfo.txtboxOWLName.Text);
          ecDetails.Add(parent2.DevClass);
          List<string> wcviewDetails = new List<string>();
          wcviewDetails.Add(parent2.RepositoryPathValue);
          wcviewDetails.Add(controlDialog.m_MyTasksInfo.txtboxOWLWCName.Text);
          wcviewDetails.Add(parent2.DevClass);
          if (parent2.DevClass == "" || parent2.DevClass == "$TMP" && !controlDialog.m_MyTasksInfo.txtboxShortId.Text.StartsWith("Z"))
            wcviewDetails.Add("Z" + controlDialog.m_MyTasksInfo.txtboxShortId.Text);
          else
            wcviewDetails.Add(controlDialog.m_MyTasksInfo.txtboxShortId.Text);
          parent1.AddMyTasks(ecDetails, wcviewDetails, controlDialog.m_MyTasksInfo.IsExistingFile);
        }
        else
          parent1.AddMyTasks();
      }
      else if (source.Header.Equals((object) "WorkCenterView"))
        parent1.AddWorkCenterView();
      else if (source.Header.Equals((object) "WorkCenterSubView"))
        parent1.AddWorkCenterSubView(modelObject);
      else if (source.Header.Equals((object) "Reports"))
      {
        parent1.AddReports();
      }
      else
      {
        if (!source.Header.Equals((object) "Delete"))
          return;
        parent1.RemoveView(modelObject);
      }
    }

    protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
    {
    }

    protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
    {
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == "Text")
      {
        this.ViewButton.UpdateText();
      }
      else
      {
        if (!(e.PropertyName == "SubViewSwitches") || this.SubViewRegionChanged == null)
          return;
        this.SubViewRegionChanged(sender, (EventArgs) null);
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is OberonViewSwitch)
          this.SubViewRegionChanged(sender, (EventArgs) null);
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        if (e.RemovedModel is OberonViewSwitch)
          this.SubViewRegionChanged(sender, (EventArgs) null);
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    public void AddViewButton(ViewSwitchButton viewButton)
    {
      this.Grid.Children.Add((UIElement) viewButton);
      this.ViewButton = viewButton;
      viewButton.SetValue(Grid.RowProperty, (object) 0);
      viewButton.SetValue(Grid.ColumnProperty, (object) 0);
    }
  }
}
