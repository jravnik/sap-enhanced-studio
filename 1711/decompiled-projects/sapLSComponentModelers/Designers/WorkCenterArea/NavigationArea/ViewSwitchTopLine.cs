﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea.ViewSwitchTopLine
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea
{
  public class ViewSwitchTopLine : Canvas
  {
    private static ImageInfo stretchResourceLeft = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Shell/NavPanel/TopLineLeft.png,30,2");
    private static ImageInfo stretchResourceRight = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Shell/NavPanel/TopLineRight.png,1,2");
    private Image imgLeft;
    private Image imgRight;

    public ViewSwitchTopLine()
    {
      this.imgLeft = new Image();
      this.imgLeft.Width = (double) ViewSwitchTopLine.stretchResourceLeft.Width;
      this.imgLeft.Height = (double) ViewSwitchTopLine.stretchResourceLeft.Height;
      this.imgLeft.Source = ResourceUtil.RetrieveImage(ViewSwitchTopLine.stretchResourceLeft.Name);
      this.Children.Add((UIElement) this.imgLeft);
      this.imgRight = new Image();
      this.imgRight.Width = (double) ViewSwitchTopLine.stretchResourceRight.Width;
      this.imgRight.Height = (double) ViewSwitchTopLine.stretchResourceRight.Height;
      this.imgRight.Source = ResourceUtil.RetrieveImage(ViewSwitchTopLine.stretchResourceRight.Name);
      this.imgRight.Stretch = Stretch.Fill;
      this.Children.Add((UIElement) this.imgRight);
      this.imgRight.SetValue(Canvas.LeftProperty, (object) (double) ViewSwitchTopLine.stretchResourceLeft.Width);
      this.SizeChanged += new SizeChangedEventHandler(this.ViewSwitchTopLine_SizeChanged);
    }

    private void ViewSwitchTopLine_SizeChanged(object sender, SizeChangedEventArgs e)
    {
      double width = e.NewSize.Width;
      if (double.IsNaN(width) || double.IsInfinity(width))
        return;
      double num = width - (double) ViewSwitchTopLine.stretchResourceLeft.Width;
      if (num <= 0.0)
        return;
      this.imgRight.Width = num;
    }
  }
}
