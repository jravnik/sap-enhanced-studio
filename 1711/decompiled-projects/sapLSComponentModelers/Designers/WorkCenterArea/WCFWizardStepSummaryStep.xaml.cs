﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep.SummaryStep
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep
{
  public partial class SummaryStep : BaseWizardStepControl, IComponentConnector
  {
    private FontWeight m_BoldFontWeight = FontWeights.Bold;
    private FontWeight m_SemiFontWeight = FontWeights.SemiBold;
    private FontWeight m_NormalFontWeight = FontWeights.Normal;
    private FontWeight m_RegularFontWeight = FontWeights.Regular;
    private Thickness m_Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
    private Thickness m_ValueMargin = new Thickness(10.0, 0.0, 0.0, 0.0);
    private FontFamily m_FontFamily = new FontFamily("Arial");
    private int row = -1;
    private const string WCVIEW_UICOMPONENT_EXTENSION = ".WCVIEW.uiwocview";
    private const string OWL_UICOMPONENT_EXTENSION = ".OWL.uicomponent";
    private const string EC_UICOMPONENT_EXTENSION = ".EC.uicomponent";
    private const string REPORTS_TARGET_LIST_COMPONENTPATH = "/SAP_BYD_TF/Analytics/AnalysisPattern/ana_reports_view_wc.WCVIEW.uiwocview";
    private const string REPORTS_TARGET_CAROUSEL_COMPONENTPATH = "/SAP_BYD_TF/Analytics/AnalysisPattern/ana_reports_carousel_view.WCVIEW.uiwocview";
    private object m_WizardResult;
    //internal Grid rootGrid;
    //private bool _contentLoaded;

    public SummaryStep()
    {
      this.InitializeComponent();
      this.Background = (Brush) new SolidColorBrush(Color.FromRgb(byte.MaxValue, byte.MaxValue, byte.MaxValue));
      this.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.VerticalAlignment = VerticalAlignment.Stretch;
    }

    public override object WizardResult
    {
      get
      {
        return this.m_WizardResult;
      }
    }

    public override void OnFinish(PropertyBag propertyBag, out bool finishStep)
    {
      finishStep = true;
      if (propertyBag == null)
        return;
      ViewSelectionStep viewSelectionStep = propertyBag.GetValue("ViewSelectionStep") as ViewSelectionStep;
      if (viewSelectionStep == null)
        return;
      IFloorplan centerFloorplanModel = (IFloorplan) ModelLayer.ObjectManager.CreateWorkCenterFloorplanModel();
      if (centerFloorplanModel != null && centerFloorplanModel.CenterStructure != null)
      {
        bool? isChecked1 = viewSelectionStep.chkOverview.IsChecked;
        if ((!isChecked1.GetValueOrDefault() ? 0 : (isChecked1.HasValue ? 1 : 0)) != 0 && viewSelectionStep.overviewControl.m_OverviewECDetails.Count >= 3 && viewSelectionStep.overviewControl.m_OverviewWCViewDetails.Count >= 3)
          centerFloorplanModel.CenterStructure.AddOverview(viewSelectionStep.overviewControl.m_OverviewECDetails, viewSelectionStep.overviewControl.m_OverviewWCViewDetails, viewSelectionStep.overviewControl.IsExistingFile);
        bool? isChecked2 = viewSelectionStep.chkMyTasks.IsChecked;
        if ((!isChecked2.GetValueOrDefault() ? 0 : (isChecked2.HasValue ? 1 : 0)) != 0 && viewSelectionStep.mytasksControl.m_MyTasksOWLDetails.Count >= 3 && viewSelectionStep.mytasksControl.m_MyTasksWCViewDetails.Count >= 3)
          centerFloorplanModel.CenterStructure.AddMyTasks(viewSelectionStep.mytasksControl.m_MyTasksOWLDetails, viewSelectionStep.mytasksControl.m_MyTasksWCViewDetails, viewSelectionStep.mytasksControl.IsExistingFile);
        bool? isChecked3 = viewSelectionStep.chkReports.IsChecked;
        if ((!isChecked3.GetValueOrDefault() ? 0 : (isChecked3.HasValue ? 1 : 0)) != 0)
          centerFloorplanModel.CenterStructure.AddReports();
      }
      this.m_WizardResult = (object) centerFloorplanModel;
    }

    public override bool PreValidate(PropertyBag propertyBag)
    {
      return true;
    }

    public override void OnCancel(PropertyBag propertyBag)
    {
    }

    public override void OnNext(PropertyBag propertyBag, out bool nextStep)
    {
      nextStep = true;
    }

    public override void OnPrevious(PropertyBag propertyBag, out bool previousStep)
    {
      previousStep = true;
    }

    public override void LoadStepView(PropertyBag propertyBag)
    {
      this.row = -1;
      this.rootGrid.Children.Clear();
      this.rootGrid.ColumnDefinitions.Clear();
      this.rootGrid.RowDefinitions.Clear();
      this.rootGrid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = GridLength.Auto
      });
      this.rootGrid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = GridLength.Auto
      });
      this.rootGrid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = GridLength.Auto
      });
      if (propertyBag == null)
        return;
      ViewSelectionStep viewSelectionStep = propertyBag.GetValue("ViewSelectionStep") as ViewSelectionStep;
      if (viewSelectionStep == null)
        return;
      this.AddResult("WorkCenterFloorplan Configuration", (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      this.AddResult("Path to Persist the WorkCenter Floorplan", viewSelectionStep.m_FolderDetails[0] + "/" + viewSelectionStep.m_FolderDetails[1] + ".WCF.uiwc", 12.0, this.m_NormalFontWeight, this.m_Margin);
      this.AddResult((string) null, (string) null, 12.0, this.m_BoldFontWeight, this.m_Margin);
      this.AddResult("WCF Short-ID", viewSelectionStep.m_FolderDetails[3], 12.0, this.m_NormalFontWeight, this.m_Margin);
      this.AddResult((string) null, (string) null, 12.0, this.m_BoldFontWeight, this.m_Margin);
      this.AddResult((string) null, (string) null, 12.0, this.m_BoldFontWeight, this.m_Margin);
      this.AddResult("Overview Configuration", (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      bool? isChecked1 = viewSelectionStep.chkOverview.IsChecked;
      if ((!isChecked1.GetValueOrDefault() ? 0 : (isChecked1.HasValue ? 1 : 0)) != 0)
      {
        this.AddResult("Path to Persist QuickLink EC", viewSelectionStep.overviewControl.m_OverviewECDetails[0] + "/" + viewSelectionStep.overviewControl.m_OverviewECDetails[1] + ".EC.uicomponent", 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
        this.AddResult("Path to Persist WCView Component", viewSelectionStep.overviewControl.m_OverviewWCViewDetails[0] + "/" + viewSelectionStep.overviewControl.m_OverviewWCViewDetails[1] + ".WCVIEW.uiwocview", 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
        this.AddResult("WCView ShortId", viewSelectionStep.overviewControl.m_OverviewWCViewDetails[3], 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      }
      else
      {
        this.AddResult("Overview Not Configured", (string) null, 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      }
      this.AddResult("Work Configuration", (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      bool? isChecked2 = viewSelectionStep.chkMyTasks.IsChecked;
      if ((!isChecked2.GetValueOrDefault() ? 0 : (isChecked2.HasValue ? 1 : 0)) != 0)
      {
        this.AddResult("Path to Persist Work EC", viewSelectionStep.mytasksControl.m_MyTasksOWLDetails[0] + "/" + viewSelectionStep.mytasksControl.m_MyTasksOWLDetails[1] + ".OWL.uicomponent", 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
        this.AddResult("Path to Persist Work WCView", viewSelectionStep.mytasksControl.m_MyTasksWCViewDetails[0] + "/" + viewSelectionStep.mytasksControl.m_MyTasksWCViewDetails[1] + ".WCVIEW.uiwocview", 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
        this.AddResult("WCView ShortId", viewSelectionStep.mytasksControl.m_MyTasksWCViewDetails[3], 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      }
      else
      {
        this.AddResult("Work Not Configured", (string) null, 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      }
      this.AddResult("Reports Configuration", (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      bool? isChecked3 = viewSelectionStep.chkReports.IsChecked;
      if ((!isChecked3.GetValueOrDefault() ? 0 : (isChecked3.HasValue ? 1 : 0)) != 0)
      {
        this.AddResult("Reports List View Path : ", "/SAP_BYD_TF/Analytics/AnalysisPattern/ana_reports_view_wc.WCVIEW.uiwocview", 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
        this.AddResult("Reports Gallery View Path : ", "/SAP_BYD_TF/Analytics/AnalysisPattern/ana_reports_carousel_view.WCVIEW.uiwocview", 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      }
      else
      {
        this.AddResult("Reports Not Configured", (string) null, 12.0, this.m_NormalFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
        this.AddResult((string) null, (string) null, 14.0, this.m_BoldFontWeight, this.m_Margin);
      }
    }

    private void AddResult(string key, string value, double FontSize, FontWeight FontWeight, Thickness margin)
    {
      string empty = string.Empty;
      string str1 = string.Empty;
      string str2 = string.Empty;
      this.rootGrid.RowDefinitions.Add(new RowDefinition()
      {
        Height = GridLength.Auto
      });
      ++this.row;
      if (value != null)
      {
        str1 = value;
        str2 = ":";
      }
      TextBlock textBlock1 = new TextBlock();
      textBlock1.FontSize = FontSize;
      textBlock1.Text = key;
      textBlock1.Margin = margin;
      textBlock1.FontFamily = this.m_FontFamily;
      textBlock1.FontWeight = FontWeight;
      this.rootGrid.Children.Add((UIElement) textBlock1);
      textBlock1.SetValue(Grid.RowProperty, (object) this.row);
      textBlock1.SetValue(Grid.ColumnProperty, (object) 0);
      TextBlock textBlock2 = new TextBlock();
      textBlock2.FontSize = FontSize;
      textBlock2.Text = str2;
      textBlock2.FontWeight = FontWeight;
      this.rootGrid.Children.Add((UIElement) textBlock2);
      textBlock2.SetValue(Grid.RowProperty, (object) this.row);
      textBlock2.SetValue(Grid.ColumnProperty, (object) 1);
      TextBlock textBlock3 = new TextBlock();
      textBlock3.FontSize = FontSize;
      textBlock3.Text = str1;
      textBlock3.FontWeight = FontWeight;
      textBlock3.FontFamily = this.m_FontFamily;
      textBlock3.Margin = this.m_ValueMargin;
      this.rootGrid.Children.Add((UIElement) textBlock3);
      textBlock3.SetValue(Grid.RowProperty, (object) this.row);
      textBlock3.SetValue(Grid.ColumnProperty, (object) 2);
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/designers/workcenterarea/wcfwizardstep/summarystep.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  if (connectionId == 1)
    //    this.rootGrid = (Grid) target;
    //  else
    //    this._contentLoaded = true;
    //}
  }
}
