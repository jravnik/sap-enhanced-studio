﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea.VisualViewSwitchNavItem
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea
{
  public class VisualViewSwitchNavItem : BaseSelectableControl
  {
    private VisualSubViewSwitchNavigation subordinateList;
    private VisualViewSwitchNavItemList parentItemList;
    private string text;
    private string itemId;
    private int index;

    public event EventHandler<EventArgs> TextChanged;

    public VisualViewSwitchNavigation ViewSwitchNavigationList
    {
      get
      {
        return this.parentItemList.VisualSwitchNavigation;
      }
    }

    internal NavigationItem ViewSwitchNavItem
    {
      get
      {
        return this.m_ModelObject as NavigationItem;
      }
    }

    public VisualViewSwitchNavItem(IModelObject model, int index, string itemId, List<NavigationItem> tabItemSubordinates, VisualViewSwitchNavItemList parentItemList, ViewContainerRegion vcRegion)
      : base(model)
    {
      this.index = index;
      this.itemId = itemId;
      this.parentItemList = parentItemList;
      if (tabItemSubordinates != null && tabItemSubordinates.Count > 0)
        this.subordinateList = new VisualSubViewSwitchNavigation((IModelObject) this.ViewSwitchNavItem.ChildNavigationItems[0], tabItemSubordinates, this, vcRegion);
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.UpdateText();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Fialed to create ViewSwitchNavigation Item.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == "Title")
        {
          this.UpdateText();
        }
        else
        {
          if (!(e.PropertyName == NavigationItem.ChildNavigationItemsProperty))
            return;
          this.ViewSwitchNavigationList.RefreshViewSwitchNavigation();
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update the property changes in the model.", ex));
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.subordinateList == null)
        return;
      this.subordinateList = (VisualSubViewSwitchNavigation) null;
    }

    public void RegisterTabEventHandlers(FrameworkElement elem, VisualViewSwitchNavItem item)
    {
      elem.Tag = (object) item;
      elem.MouseLeftButtonDown += new MouseButtonEventHandler(this.tabItem_MouseLeftButtonDown);
    }

    private void tabItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      VisualViewSwitchNavItem tag = (VisualViewSwitchNavItem) ((FrameworkElement) sender).Tag;
      if (tag != null && tag.ViewSwitchNavItem.ChildNavigationItems.Count > 0)
      {
        foreach (NavigationItem childNavigationItem in tag.ViewSwitchNavItem.ChildNavigationItems)
        {
          if (tag.ItemId == childNavigationItem.NavigationItemId)
          {
            this.m_ModelObject = (IModelObject) childNavigationItem;
            break;
          }
        }
      }
      if (tag.TabItemSubordinates != null)
        tag.TabItemSubordinates.initFirstSubViewTextBlocks();
      this.OnMouseLeftButtonDown(e);
      if (this.parentItemList == null)
        return;
      this.parentItemList.Select(tag);
    }

    private void UpdateText()
    {
      this.text = this.ViewSwitchNavItem.Title.Text;
      if (this.TextChanged == null)
        return;
      this.TextChanged((object) this, EventArgs.Empty);
    }

    public void Select()
    {
      if (this.parentItemList == null)
        return;
      this.parentItemList.Select(this);
    }

    public string TabText
    {
      get
      {
        if (this.text != null)
          return this.text;
        return "";
      }
    }

    public string Text
    {
      get
      {
        return this.text;
      }
      set
      {
        this.text = value;
        if (this.TextChanged == null)
          return;
        this.TextChanged((object) this, EventArgs.Empty);
      }
    }

    public override string SelectionText
    {
      get
      {
        return "NavigationItm";
      }
    }

    public int ItemIndex
    {
      get
      {
        return this.index;
      }
    }

    public string ItemId
    {
      get
      {
        return this.itemId;
      }
      set
      {
        this.itemId = value;
      }
    }

    public VisualSubViewSwitchNavigation TabItemSubordinates
    {
      get
      {
        return this.subordinateList;
      }
      set
      {
        this.subordinateList = value;
      }
    }
  }
}
