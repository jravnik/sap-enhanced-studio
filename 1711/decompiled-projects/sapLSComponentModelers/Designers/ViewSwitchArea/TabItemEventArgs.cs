﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea.TabItemEventArgs
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea
{
  public class TabItemEventArgs : EventArgs
  {
    private VisualViewSwitchNavItem item;
    private SubViewSwitchNavItem selectedChildNavItem;

    public TabItemEventArgs(VisualViewSwitchNavItem item)
    {
      this.item = item;
    }

    public VisualViewSwitchNavItem Item
    {
      get
      {
        return this.item;
      }
    }

    public SubViewSwitchNavItem SelectedChildNavItem
    {
      get
      {
        return this.selectedChildNavItem;
      }
      set
      {
        this.selectedChildNavItem = value;
      }
    }
  }
}
