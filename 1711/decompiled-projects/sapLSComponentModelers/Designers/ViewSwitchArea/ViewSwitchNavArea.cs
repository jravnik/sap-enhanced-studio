﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea.ViewSwitchNavArea
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea
{
  public class ViewSwitchNavArea : BaseVisualControl
  {
    private const string RESOURCE_NAME_HILITE_COLOR = "#EAF1F6";
    private VisualViewSwitchNavigation oifTabStrip;
    private ViewContainerRegion vcRegion;
    private Navigation navigationDef;
    private string currentNavigationID;
    private string m_SelectedNavID;

    public string SelectedNavID
    {
      get
      {
        return this.m_SelectedNavID;
      }
    }

    internal ViewSwitchContextualNavigationRegion OIFContextNavigationRegion
    {
      get
      {
        return this.m_ModelObject as ViewSwitchContextualNavigationRegion;
      }
    }

    public override string SelectionText
    {
      get
      {
        return string.Empty;
      }
    }

    public ViewSwitchNavArea(IModelObject model)
      : base(model, true)
    {
      if (this.OIFContextNavigationRegion == null)
        this.Visibility = Visibility.Collapsed;
      else if ((Navigation) this.OIFContextNavigationRegion.ViewSwitchNavigation == null)
        this.Visibility = Visibility.Collapsed;
      else
        this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        if (!(this.m_ModelObject is ViewSwitchContextualNavigationRegion))
          return;
        base.LoadView();
        this.ControlBackground = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        this.vcRegion = (this.OIFContextNavigationRegion.Parent as UXView).ViewContainerRegion;
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Pixel);
        Rectangle rectangle = new Rectangle();
        rectangle.Height = 1.0;
        rectangle.HorizontalAlignment = HorizontalAlignment.Stretch;
        rectangle.Fill = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        GridUtil.PlaceElement(this.Grid, (UIElement) rectangle, 0, 0);
        this.navigationDef = (Navigation) (this.m_ModelObject as ViewSwitchContextualNavigationRegion).ViewSwitchNavigation;
        this.oifTabStrip = new VisualViewSwitchNavigation((IModelObject) this.navigationDef, this.vcRegion, this);
        this.oifTabStrip.Selected += new RoutedEventHandler(this.OnTabStripSelectionChanged);
        this.oifTabStrip.Margin = new Thickness(0.0, 4.0, 0.0, 0.0);
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.oifTabStrip, 1, 0);
        this.initTabStrip();
        if (this.navigationDef.NavigationItems[0].NavigationItemId != null)
        {
          this.currentNavigationID = this.navigationDef.NavigationItems[0].NavigationItemId;
          this.oifTabStrip.Select(this.currentNavigationID);
        }
        else if (this.navigationDef.NavigationItems[0].ChildNavigationItems != null && this.navigationDef.NavigationItems[0].ChildNavigationItems.Count > 0)
        {
          this.currentNavigationID = this.navigationDef.NavigationItems[0].ChildNavigationItems[0].NavigationItemId;
          this.oifTabStrip.Select(this.currentNavigationID);
        }
        List<NavigationItem> navigationItems = this.navigationDef.NavigationItems;
        this.Visibility = navigationItems != null && navigationItems.Count != 0 ? Visibility.Visible : Visibility.Collapsed;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to create ViewSwitch Navigation Area.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.oifTabStrip.RefreshViewSwitchNavigation();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      this.navigationDef = (Navigation) null;
      if (this.oifTabStrip == null)
        return;
      this.oifTabStrip.Terminate();
      this.oifTabStrip = (VisualViewSwitchNavigation) null;
    }

    public void initTabStrip()
    {
      List<NavigationItem> navigationItems = this.navigationDef.NavigationItems;
      if (navigationItems == null)
        return;
      foreach (NavigationItem navigationItem in navigationItems)
      {
        if (navigationItem.NavigationItemId != null)
          this.oifTabStrip.Items.AddItem(navigationItem, navigationItem.NavigationItemId, navigationItem.ChildNavigationItems, this.vcRegion);
        else if (navigationItem.ChildNavigationItems != null && navigationItem.ChildNavigationItems.Count > 0)
          this.oifTabStrip.Items.AddItem(navigationItem, navigationItem.ChildNavigationItems[0].NavigationItemId, navigationItem.ChildNavigationItems, this.vcRegion);
      }
    }

    private void OnTabStripSelectionChanged(object sender, RoutedEventArgs e)
    {
      VisualViewSwitchNavigation switchNavigation = sender as VisualViewSwitchNavigation;
      if (switchNavigation == null)
        return;
      this.m_SelectedNavID = switchNavigation.NavigationItemID;
    }
  }
}
