﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea.VisualViewSwitchNavItemList
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea
{
  public class VisualViewSwitchNavItemList : BaseVisualControl
  {
    private int selectedItemIndex = -1;
    private VisualViewSwitchNavigation vsNavigation;
    private List<VisualViewSwitchNavItem> navItems;

    public event EventHandler<TabItemEventArgs> ItemAdded;

    public event EventHandler<TabItemEventArgs> ItemSelected;

    public event VisualViewSwitchNavigation.SubTabItemOperationDelegate SubordinateSelected;

    public VisualViewSwitchNavigation VisualSwitchNavigation
    {
      get
      {
        return this.vsNavigation;
      }
    }

    internal Navigation ViewSwitchNavigation
    {
      get
      {
        return this.m_ModelObject as Navigation;
      }
    }

    public int SelectedItemIndex
    {
      get
      {
        return this.selectedItemIndex;
      }
    }

    public string SelectedItemId
    {
      get
      {
        if (this.selectedItemIndex == -1)
          return (string) null;
        return this.navItems[this.selectedItemIndex].ItemId;
      }
    }

    public int Count
    {
      get
      {
        return this.navItems.Count;
      }
    }

    public List<VisualViewSwitchNavItem> ViewSwitchNavItemList
    {
      get
      {
        return this.navItems;
      }
    }

    public override string SelectionText
    {
      get
      {
        return string.Empty;
      }
    }

    public VisualViewSwitchNavItemList(IModelObject model, VisualViewSwitchNavigation vsNavigation)
      : base(model)
    {
      this.vsNavigation = vsNavigation;
      this.navItems = new List<VisualViewSwitchNavItem>();
    }

    internal void setSelectedItem(VisualViewSwitchNavItem tabItem)
    {
      if (this.ItemSelected == null)
        return;
      this.ItemSelected((object) this, new TabItemEventArgs(tabItem));
    }

    internal void setSelectedItemId(string value)
    {
      if (value == null)
        return;
      int index = 0;
      for (int count = this.navItems.Count; index < count; ++index)
      {
        VisualViewSwitchNavItem navItem = this.navItems[index];
        if (navItem.ItemId == value)
        {
          this.selectedItemIndex = index;
          this.setSelectedItem(navItem);
          break;
        }
        if (navItem.TabItemSubordinates != null)
        {
          foreach (SubViewSwitchNavItem tabItemSubordinate in navItem.TabItemSubordinates)
          {
            if (tabItemSubordinate.ItemId == value)
            {
              this.selectedItemIndex = index;
              tabItemSubordinate.IsSelected = true;
              this.SubordinateSelected((object) this, new TabItemEventArgs(navItem)
              {
                SelectedChildNavItem = tabItemSubordinate
              });
              return;
            }
          }
        }
      }
    }

    public int IndexOf(VisualViewSwitchNavItem item)
    {
      return this.navItems.IndexOf(item);
    }

    public void Select(VisualViewSwitchNavItem item)
    {
      if (item != null)
      {
        if (item.ItemId != null)
        {
          this.vsNavigation.Select(item.ItemId);
        }
        else
        {
          if (item.ViewSwitchNavItem.ChildNavigationItems == null || item.ViewSwitchNavItem.ChildNavigationItems.Count <= 0)
            return;
          this.vsNavigation.Select(item.ItemId);
        }
      }
      else
        this.vsNavigation.Select((string) null);
    }

    public VisualViewSwitchNavItem AddItem(NavigationItem item, string itemId, List<NavigationItem> tabItemSubordinates, ViewContainerRegion vcRegion)
    {
      VisualViewSwitchNavItem viewSwitchNavItem = new VisualViewSwitchNavItem((IModelObject) item, this.navItems.Count, itemId, tabItemSubordinates, this, vcRegion);
      this.AddItem(viewSwitchNavItem);
      return viewSwitchNavItem;
    }

    private void AddItem(VisualViewSwitchNavItem item)
    {
      this.navItems.Add(item);
      if (this.ItemAdded == null)
        return;
      this.ItemAdded((object) this, new TabItemEventArgs(item));
    }

    public void RemoveItem(VisualViewSwitchNavItem item)
    {
      this.navItems.Remove(item);
    }

    public VisualViewSwitchNavItem Get(int index)
    {
      if (index < 0)
        return (VisualViewSwitchNavItem) null;
      if (index <= this.navItems.Count - 1)
        return this.navItems[index];
      return (VisualViewSwitchNavItem) null;
    }

    public override void Terminate()
    {
      try
      {
        base.Terminate();
        if (this.navItems == null)
          return;
        foreach (BaseVisualControl navItem in this.navItems)
          navItem.Terminate();
        this.navItems.Clear();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Termination of ViewSwitchNavigation failed.", ex));
      }
    }
  }
}
