﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterDetails
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class WorkCenterDetails
  {
    private string workcentername;

    public WorkCenterDetails(string wcName)
    {
      this.workcentername = wcName;
    }

    public string WorkCenterPath
    {
      get
      {
        return this.workcentername;
      }
      set
      {
        this.workcentername = value;
      }
    }
  }
}
