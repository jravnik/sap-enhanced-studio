﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.QuickViewDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ThinkHeaderRegion;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class QuickViewDesigner : ComponentDesigner
  {
    private static readonly SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox m_ComponentToolbox = ToolboxUtilities.DeserialzeToolboxComponent(Resource.QuickViewToolbox);
    private VisualThingHeaderRegion m_ThingHeader;

    public override SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox Toolbox
    {
      get
      {
        return QuickViewDesigner.m_ComponentToolbox;
      }
    }

    internal QuickViewComponent Component
    {
      get
      {
        return this.m_ModelObject as QuickViewComponent;
      }
    }

    public QuickViewDesigner(QuickViewComponent component)
      : base((IModelObject) component, false, false)
    {
      this.AllowDrop = true;
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      this.AddThingHeaderRegion();
    }

    public void AddThingHeaderRegion()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      this.m_ThingHeader = new VisualThingHeaderRegion((IModelObject) this.Component.HeaderArea);
      this.m_ThingHeader.HorizontalAlignment = HorizontalAlignment.Left;
      this.m_ThingHeader.Width = 400.0;
      Border border = new Border();
      border.Child = (UIElement) this.m_ThingHeader;
      border.Height = double.NaN;
      border.VerticalAlignment = VerticalAlignment.Top;
      border.BorderThickness = new Thickness(1.0);
      border.BorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 142, (byte) 182, (byte) 223));
      border.CornerRadius = new CornerRadius(1.0);
      border.HorizontalAlignment = HorizontalAlignment.Left;
      border.Width = 402.0;
      GridUtil.PlaceElement(this.Grid, (UIElement) border, 0, 0);
    }
  }
}
