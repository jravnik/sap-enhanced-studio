﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.UISettingsDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class UISettingsDesigner : ComponentDesigner, ISelectionProvider
  {
    private System.Windows.Controls.DataGrid dgWorkCenter;
    private SelectionContext m_SelectionContext;
    private System.ComponentModel.IContainer components;

    internal UISettingsComponent OberonModelObject
    {
      get
      {
        return this.m_ModelObject as UISettingsComponent;
      }
    }

    public UISettingsDesigner(IModelObject rootModel)
      : base(rootModel, true, true)
    {
    }

    public override void LoadView()
    {
      base.LoadView();
      this.AddTitle();
      this.AddContent();
      this.m_TabWindow.TabPages.Remove(this.m_DataModelTab);
      this.m_TabWindow.TabPages.Remove(this.m_ControllerTab);
      this.m_TabWindow.TabPages.Remove(this.m_OberonPreviewTab);
      this.m_SelectionContext = new SelectionContext();
      this.m_SelectionContext.AddSelectionProvider((ISelectionProvider) this);
    }

    private void AddTitle()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      DisplayTextControl displayTextControl = new DisplayTextControl(TextViewStyles.PageHeaderTitle, "Work Center Order");
      displayTextControl.Margin = new Thickness(5.0, 5.0, 5.0, 5.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) displayTextControl, 0, 0);
    }

    private void AddContent()
    {
      this.dgWorkCenter = new System.Windows.Controls.DataGrid();
      this.dgWorkCenter.RowHeaderWidth = 20.0;
      this.dgWorkCenter.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
      this.dgWorkCenter.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
      this.dgWorkCenter.ColumnHeaderHeight = 22.0;
      this.dgWorkCenter.RowHeight = 20.0;
      this.dgWorkCenter.GridLinesVisibility = DataGridGridLinesVisibility.All;
      this.dgWorkCenter.SelectionUnit = DataGridSelectionUnit.FullRow;
      this.dgWorkCenter.SelectionMode = DataGridSelectionMode.Single;
      this.dgWorkCenter.CanUserAddRows = false;
      this.dgWorkCenter.CanUserReorderColumns = false;
      this.dgWorkCenter.CanUserResizeRows = false;
      this.dgWorkCenter.CanUserSortColumns = false;
      this.dgWorkCenter.HeadersVisibility = DataGridHeadersVisibility.All;
      List<WorkCenterDetails> workCenterDetailsList = new List<WorkCenterDetails>();
      foreach (NamedModelObject workCenterReference in this.OberonModelObject.WorkCenterReferences)
      {
        WorkCenterDetails workCenterDetails = new WorkCenterDetails(workCenterReference.Name);
        workCenterDetailsList.Add(workCenterDetails);
      }
      this.dgWorkCenter.ItemsSource = (IEnumerable) workCenterDetailsList;
      this.dgWorkCenter.ColumnWidth = new DataGridLength(1.0, DataGridLengthUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.dgWorkCenter, 1, 0);
    }

    private void dgWorkCenter_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.RaiseSelectionChangedEvent((IModelObject) this.OberonModelObject);
    }

    private void RaiseSelectionChangedEvent(IModelObject modelToSelect)
    {
      if (this.SelectionChanged == null)
        return;
      this.SelectionChanged((object) this, new SelectionEventArgs()
      {
        SelectedObject = modelToSelect
      });
    }

    public new event SAP.BYD.LS.UIDesigner.UICore.Core.SelectionChangedEventHandler SelectionChanged;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (System.ComponentModel.IContainer) new Container();
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Text = "WorkCenterOrderDesigner";
    }
  }
}
