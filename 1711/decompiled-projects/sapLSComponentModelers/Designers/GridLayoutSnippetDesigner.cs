﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.GridLayoutSnippetDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class GridLayoutSnippetDesigner : ComponentDesigner
  {
    private VisualLayout m_VisualLayout;
    private Grid m_SnippetGridControl;

    private GridLayout GridLayout
    {
      get
      {
        return this.m_ModelObject as GridLayout;
      }
    }

    public Grid SnippetGridControl
    {
      get
      {
        return this.m_SnippetGridControl;
      }
    }

    public event EventHandler BackToDesignerClicked;

    public GridLayoutSnippetDesigner(IModelObject associatedModel)
      : base(associatedModel, false, false)
    {
      this.AllowDrop = true;
      this.m_SnippetGridControl = new Grid();
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        GridUtil.AddRowDef(this.m_SnippetGridControl, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.m_SnippetGridControl, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.m_SnippetGridControl, 1.0, 150.0, GridUnitType.Pixel);
        GridUtil.PlaceElement(this.m_SnippetGridControl, (UIElement) new DisplayTextControl(TextViewStyles.PageHeaderTitle, "GridLayout Snippet  : " + this.GridLayout.Id), 0, 0);
        Button button = new Button();
        button.Background = (Brush) Brushes.Transparent;
        button.BorderBrush = (Brush) Brushes.Transparent;
        StackPanel stackPanel = new StackPanel();
        stackPanel.Orientation = Orientation.Horizontal;
        stackPanel.MouseDown += new MouseButtonEventHandler(this.OnButtonClick);
        button.Content = (object) stackPanel;
        stackPanel.Children.Add((UIElement) new Image()
        {
          Source = (ImageSource) this.ConvertToBitMapImage(Resource.GoBack)
        });
        TextBlock textBlock = new TextBlock();
        textBlock.Text = "Back To Designer";
        textBlock.TextAlignment = TextAlignment.Center;
        textBlock.Foreground = (Brush) Brushes.Blue;
        textBlock.TextDecorations = TextDecorations.Underline;
        textBlock.VerticalAlignment = VerticalAlignment.Center;
        stackPanel.Children.Add((UIElement) textBlock);
        GridUtil.PlaceElement(this.m_SnippetGridControl, (UIElement) button, 0, 1);
        this.m_VisualLayout = new VisualLayout((IModelObject) this.GridLayout);
        GridUtil.AddRowDef(this.m_SnippetGridControl, 1.0, GridUnitType.Star);
        GridUtil.PlaceElement(this.m_SnippetGridControl, (UIElement) this.m_VisualLayout, 1, 0, 1, 2);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading the GridLayoutSnippet designer failed.", ex));
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.m_SnippetGridControl == null)
        return;
      ComponentModelerUtilities.Instance.TerminatePanelControls((FrameworkElement) this.m_SnippetGridControl);
    }

    private void OnButtonClick(object sender, MouseButtonEventArgs e)
    {
      if (this.BackToDesignerClicked == null)
        return;
      this.BackToDesignerClicked((object) this, EventArgs.Empty);
    }
  }
}
