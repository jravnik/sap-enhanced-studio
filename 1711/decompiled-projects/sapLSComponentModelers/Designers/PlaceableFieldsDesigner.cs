﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.PlaceableFieldsDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class PlaceableFieldsDesigner : ComponentDesigner
  {
    private VisualPlaceableFields placeableFields;
    private Grid placeableFieldDesignerGrid;
    private System.ComponentModel.IContainer components;

    public event EventHandler BackToDesignerClicked;

    internal PlaceableFields PlaceableFields
    {
      get
      {
        return this.m_ModelObject as PlaceableFields;
      }
    }

    public Grid PlaceableFieldGrid
    {
      get
      {
        return this.placeableFieldDesignerGrid;
      }
    }

    public PlaceableFieldsDesigner(IModelObject rootModel)
      : base(rootModel, false, false)
    {
      this.placeableFieldDesignerGrid = new Grid();
      this.placeableFieldDesignerGrid.AllowDrop = true;
      this.LoadView();
    }

    public override void LoadView()
    {
      this.AddTitle();
      this.AddContentRegion();
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.placeableFieldDesignerGrid == null)
        return;
      ComponentModelerUtilities.Instance.TerminatePanelControls((FrameworkElement) this.placeableFieldDesignerGrid);
    }

    private void AddTitle()
    {
      GridUtil.AddRowDef(this.placeableFieldDesignerGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.placeableFieldDesignerGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.placeableFieldDesignerGrid, 1.0, 150.0, GridUnitType.Pixel);
      GridUtil.PlaceElement(this.placeableFieldDesignerGrid, (UIElement) new DisplayTextControl(TextViewStyles.PageHeaderTitle, "PlaceableFields"), 0, 0);
      System.Windows.Controls.Button button = new System.Windows.Controls.Button();
      button.Background = (Brush) Brushes.Transparent;
      button.BorderBrush = (Brush) Brushes.Transparent;
      StackPanel stackPanel = new StackPanel();
      stackPanel.Orientation = System.Windows.Controls.Orientation.Horizontal;
      stackPanel.Children.Add((UIElement) new System.Windows.Controls.Image()
      {
        Source = (ImageSource) this.ConvertToBitMapImage(Resource.GoBack)
      });
      TextBlock textBlock = new TextBlock();
      textBlock.Text = "Back To Designer";
      textBlock.TextAlignment = TextAlignment.Center;
      textBlock.Foreground = (Brush) Brushes.Blue;
      textBlock.TextDecorations = TextDecorations.Underline;
      textBlock.VerticalAlignment = VerticalAlignment.Center;
      stackPanel.Children.Add((UIElement) textBlock);
      stackPanel.MouseDown += new MouseButtonEventHandler(this.buttonPanel_MouseDown);
      button.Content = (object) stackPanel;
      GridUtil.PlaceElement(this.placeableFieldDesignerGrid, (UIElement) button, 0, 1);
    }

    private void buttonPanel_MouseDown(object sender, MouseButtonEventArgs e)
    {
      try
      {
        if (this.BackToDesignerClicked == null)
          return;
        this.BackToDesignerClicked(sender, (EventArgs) e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    private void AddContentRegion()
    {
      ScrollViewer scrollViewer = new ScrollViewer();
      scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
      scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
      scrollViewer.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      GridUtil.AddRowDef(this.placeableFieldDesignerGrid, 1.0, 1.0, GridUnitType.Star);
      this.placeableFields = new VisualPlaceableFields((IModelObject) this.PlaceableFields);
      this.placeableFields.VerticalAlignment = VerticalAlignment.Top;
      scrollViewer.Content = (object) this.placeableFields;
      GridUtil.PlaceElement(this.placeableFieldDesignerGrid, (UIElement) scrollViewer, 1, 0, 1, 2);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (System.ComponentModel.IContainer) new Container();
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Text = nameof (PlaceableFieldsDesigner);
    }
  }
}
