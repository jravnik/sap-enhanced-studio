﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.XMLViewDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Xml;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class XMLViewDesigner : ComponentDesigner, ISelectionProvider
  {
    private SelectionContext m_SelectionContext;
    private System.Windows.Forms.WebBrowser webBowser;
    private System.ComponentModel.IContainer components;

    internal XMLObject OberonModelObject
    {
      get
      {
        return this.m_ModelObject as XMLObject;
      }
    }

    public XMLViewDesigner(IModelObject rootModel)
      : base(rootModel, true, true)
    {
    }

    public override void LoadView()
    {
      base.LoadView();
      this.AddTitle();
      this.AddContent();
      this.m_TabWindow.TabPages.Remove(this.m_DataModelTab);
      this.m_TabWindow.TabPages.Remove(this.m_ControllerTab);
      this.m_TabWindow.TabPages.Remove(this.m_OberonPreviewTab);
      this.m_SelectionContext = new SelectionContext();
      this.m_SelectionContext.AddSelectionProvider((ISelectionProvider) this);
    }

    private void AddTitle()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      DisplayTextControl displayTextControl = new DisplayTextControl(TextViewStyles.PageHeaderTitle, this.OberonModelObject.Name);
      displayTextControl.Margin = new Thickness(5.0, 5.0, 5.0, 5.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) displayTextControl, 0, 0);
    }

    private void AddContent()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      try
      {
        this.webBowser = new System.Windows.Forms.WebBrowser();
        GridUtil.PlaceElement(this.Grid, (UIElement) new WindowsFormsHost()
        {
          Child = (System.Windows.Forms.Control) this.webBowser
        }, 1, 0, 1, 2);
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(this.OberonModelObject.UISettingsXML);
        string str = Path.GetTempPath() + "TemporaryXml.xml";
        using (StreamWriter streamWriter = new StreamWriter(str, false, Encoding.UTF8))
        {
          streamWriter.Write(xmlDocument.OuterXml);
          streamWriter.Close();
          streamWriter.Dispose();
        }
        this.webBowser.Navigate(str);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.webBowser == null)
        return;
      this.webBowser.Dispose();
    }

    private void dgWorkCenter_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.RaiseSelectionChangedEvent((IModelObject) this.OberonModelObject);
    }

    private void RaiseSelectionChangedEvent(IModelObject modelToSelect)
    {
      if (this.SelectionChanged == null)
        return;
      this.SelectionChanged((object) this, new SelectionEventArgs()
      {
        SelectedObject = modelToSelect
      });
    }

    public new event SAP.BYD.LS.UIDesigner.UICore.Core.SelectionChangedEventHandler SelectionChanged;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (System.ComponentModel.IContainer) new Container();
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Text = "WorkCenterOrderDesigner";
    }
  }
}
