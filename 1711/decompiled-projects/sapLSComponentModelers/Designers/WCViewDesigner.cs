﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WCViewDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class WCViewDesigner : ComponentDesigner
  {
    private VisualLayout m_LayoutControl;

    internal WorkCenterView Component
    {
      get
      {
        return this.m_ModelObject as WorkCenterView;
      }
    }

    public WCViewDesigner(IModelObject rootModel)
      : base(rootModel, true, false)
    {
      this.AllowDrop = true;
    }

    public override void LoadView()
    {
      base.LoadView();
      this.AddIdentificationRegion();
      this.AddContentRegion();
      this.LoadLinkedControlRegion();
    }

    private void AddContentRegion()
    {
      if (this.m_LayoutControl != null)
        this.m_LayoutControl.Terminate();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.PlaceElement(this.Grid, (UIElement) new VisualView((IModelObject) this.Component.View), 1, 0);
    }

    private void AddIdentificationRegion()
    {
      if (this.m_LayoutControl != null)
        this.m_LayoutControl.Terminate();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) new VisualIdentificationRegion((IModelObject) this.Component.IdentificationArea), 0, 0);
    }
  }
}
