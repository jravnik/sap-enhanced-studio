﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.PrintTemplateDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class PrintTemplateDesigner : ComponentDesigner
  {
    private static readonly SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox m_ComponentToolbox = ToolboxUtilities.DeserialzeToolboxComponent(Resource.QuickActivityToolbox);
    private VisualHeaderRegion m_HeaderViewControl;
    private VisualFooterRegion m_FooterViewControl;
    private VisualView m_ViewControl;
    private Grid m_MyInnerGrid;

    public override SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox Toolbox
    {
      get
      {
        return PrintTemplateDesigner.m_ComponentToolbox;
      }
    }

    internal PrintTemplateFloorplan Component
    {
      get
      {
        return this.m_ModelObject as PrintTemplateFloorplan;
      }
    }

    public PrintTemplateDesigner(IModelObject rootModel)
      : base(rootModel, true, true)
    {
      this.AllowDrop = true;
    }

    public override void LoadView()
    {
      base.LoadView();
      this.m_MyInnerGrid = new Grid();
      this.m_MyInnerGrid.Margin = new Thickness(1.0);
      this.m_MyInnerGrid.AllowDrop = true;
      GridUtil.AddColumnDef(this.m_MyInnerGrid, 1.0, GridUnitType.Star);
      this.AddHeaderInformationRegion();
      this.AddContentRegion();
      this.AddFooterInformationRegion();
      ScrollViewer scrollViewer = new ScrollViewer();
      scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
      scrollViewer.CanContentScroll = true;
      scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
      scrollViewer.Content = (object) this.m_MyInnerGrid;
      GridUtil.PlaceElement(this.Grid, (UIElement) scrollViewer, 1, 0);
      this.LoadLinkedControlRegion();
    }

    private void AddHeaderInformationRegion()
    {
      if (this.m_HeaderViewControl != null)
        this.m_HeaderViewControl.Terminate();
      GridUtil.AddRowDef(this.m_MyInnerGrid, 1.0, GridUnitType.Auto);
      this.m_HeaderViewControl = new VisualHeaderRegion((IModelObject) this.Component.HeaderInformationArea);
      GridUtil.PlaceElement(this.m_MyInnerGrid, (UIElement) this.m_HeaderViewControl, 0, 0);
    }

    private void AddContentRegion()
    {
      if (this.m_ViewControl != null)
        this.m_ViewControl.Terminate();
      GridUtil.AddRowDef(this.m_MyInnerGrid, 1.0, GridUnitType.Auto);
      this.m_ViewControl = new VisualView((IModelObject) this.Component.View);
      GridUtil.PlaceElement(this.m_MyInnerGrid, (UIElement) this.m_ViewControl, 1, 0);
    }

    private void AddFooterInformationRegion()
    {
      if (this.m_FooterViewControl != null)
        this.m_FooterViewControl.Terminate();
      GridUtil.AddRowDef(this.m_MyInnerGrid, 1.0, GridUnitType.Auto);
      this.m_FooterViewControl = new VisualFooterRegion((IModelObject) this.Component.FooterInformationArea);
      GridUtil.PlaceElement(this.m_MyInnerGrid, (UIElement) this.m_FooterViewControl, 2, 0);
    }
  }
}
