﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.TaskListViewDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.IdentificationRegion2;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System.Windows;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class TaskListViewDesigner : ComponentDesigner
  {
    private TitleRegion m_TitleRegion;
    private TaskListViewContent m_TaskListViewContent;

    internal TaskListView Component
    {
      get
      {
        return this.m_ModelObject as TaskListView;
      }
    }

    public TaskListViewDesigner(IModelObject model)
      : base(model, true, false)
    {
      this.Component.ControllerInterface.ModelObjectAdded -= new ModelAddedEventHandler(this.OnModelObjectAdded);
      this.Component.ControllerInterface.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
      this.Component.ControllerInterface.ModelObjectRemoved -= new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      this.Component.ControllerInterface.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      this.m_TabWindow.Controls.Remove((Control) this.m_DataModelTab);
      this.m_TabWindow.Controls.Remove((Control) this.m_ControllerTab);
      this.m_TabWindow.Controls.Remove((Control) this.m_OberonPreviewTab);
    }

    public override void LoadView()
    {
      base.LoadView();
      this.AddIdentification();
      this.AddContent();
    }

    private void AddIdentification()
    {
      if (this.m_TitleRegion != null)
        this.m_TitleRegion.Terminate();
      GridUtil.AddRowDef(this.Grid, 30.0, GridUnitType.Pixel);
      GridUtil.PlaceElement(this.Grid, (UIElement) new TitleRegion((IModelObject) (this.Component.CenterStructure as OberonCenterStructure)), 0, 0);
    }

    private void AddContent()
    {
      if (this.m_TaskListViewContent != null)
        this.m_TaskListViewContent.Terminate();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      TaskListViewContent taskListViewContent = new TaskListViewContent((IModelObject) (this.Component.CenterStructure as OberonCenterStructure));
      taskListViewContent.Margin = new Thickness(10.0, 10.0, 10.0, 10.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) taskListViewContent, 1, 0);
    }
  }
}
