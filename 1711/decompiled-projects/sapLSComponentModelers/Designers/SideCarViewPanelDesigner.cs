﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.SideCarViewPanelDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class SideCarViewPanelDesigner : ComponentDesigner
  {
    private VisualLayout m_GridLayout;
    private Grid sideCarViewPanelDesignerGrid;
    private System.ComponentModel.IContainer components;

    public event EventHandler BackToDesignerClicked;

    internal SideCarViewPanel SideCarViewPanel
    {
      get
      {
        return this.m_ModelObject as SideCarViewPanel;
      }
    }

    public Grid SideCarViewPanelGrid
    {
      get
      {
        return this.sideCarViewPanelDesignerGrid;
      }
    }

    public SideCarViewPanelDesigner(IModelObject rootModel)
      : base(rootModel, false, false)
    {
      this.AllowDrop = true;
      this.sideCarViewPanelDesignerGrid = new Grid();
      this.LoadView();
    }

    public override void LoadView()
    {
      this.AddTitle();
      this.AddContentRegion();
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.sideCarViewPanelDesignerGrid == null)
        return;
      ComponentModelerUtilities.Instance.TerminatePanelControls((FrameworkElement) this.sideCarViewPanelDesignerGrid);
    }

    private void AddTitle()
    {
      GridUtil.AddRowDef(this.sideCarViewPanelDesignerGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.sideCarViewPanelDesignerGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.sideCarViewPanelDesignerGrid, 1.0, 150.0, GridUnitType.Pixel);
      GridUtil.PlaceElement(this.sideCarViewPanelDesignerGrid, (UIElement) new DisplayTextControl(TextViewStyles.PageHeaderTitle, "SideCarPanel  : " + this.SideCarViewPanel.Name), 0, 0);
      System.Windows.Controls.Button button = new System.Windows.Controls.Button();
      button.Background = (Brush) Brushes.Transparent;
      button.BorderBrush = (Brush) Brushes.Transparent;
      StackPanel stackPanel = new StackPanel();
      stackPanel.Orientation = System.Windows.Controls.Orientation.Horizontal;
      stackPanel.Children.Add((UIElement) new System.Windows.Controls.Image()
      {
        Source = (ImageSource) this.ConvertToBitMapImage(Resource.GoBack)
      });
      TextBlock textBlock = new TextBlock();
      textBlock.Text = "Back To Designer";
      textBlock.TextAlignment = TextAlignment.Center;
      textBlock.Foreground = (Brush) Brushes.Blue;
      textBlock.TextDecorations = TextDecorations.Underline;
      textBlock.VerticalAlignment = VerticalAlignment.Center;
      stackPanel.Children.Add((UIElement) textBlock);
      stackPanel.MouseDown += new MouseButtonEventHandler(this.buttonPanel_MouseDown);
      button.Content = (object) stackPanel;
      GridUtil.PlaceElement(this.sideCarViewPanelDesignerGrid, (UIElement) button, 0, 1);
    }

    private void buttonPanel_MouseDown(object sender, MouseButtonEventArgs e)
    {
      try
      {
        if (this.BackToDesignerClicked == null)
          return;
        this.BackToDesignerClicked(sender, (EventArgs) e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    private void AddContentRegion()
    {
      if (this.m_GridLayout != null)
        this.m_GridLayout.Terminate();
      GridUtil.AddRowDef(this.sideCarViewPanelDesignerGrid, 1.0, GridUnitType.Star);
      this.m_GridLayout = new VisualLayout((IModelObject) this.SideCarViewPanel);
      this.m_GridLayout.SetValue(Grid.ColumnSpanProperty, (object) 2);
      GridUtil.PlaceElement(this.sideCarViewPanelDesignerGrid, (UIElement) this.m_GridLayout, 1, 0);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (System.ComponentModel.IContainer) new Container();
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Text = nameof (SideCarViewPanelDesigner);
    }
  }
}
