﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ThingInspectorDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ThinkHeaderRegion;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class ThingInspectorDesigner : ComponentDesigner
  {
    private static readonly SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox m_ComponentToolbox = ToolboxUtilities.DeserialzeToolboxComponent(Resource.ThingInspectorToolbox);
    private VisualThingHeaderRegion m_ThingHeader;
    private VisualViewSwitchCNR m_VisualCNR;
    private ViewSwitchNavArea viewSwitchNav;
    private VisualToolbar m_VisualFunctionBar;
    private VisualView m_ViewControl;
    private Grid viewSwitchNavGrid;
    private Grid thingHeaderGrid;

    public override SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox Toolbox
    {
      get
      {
        return ThingInspectorDesigner.m_ComponentToolbox;
      }
    }

    internal ThingInspectorComponent Component
    {
      get
      {
        return this.m_ModelObject as ThingInspectorComponent;
      }
    }

    public ThingInspectorDesigner(ThingInspectorComponent component)
      : base((IModelObject) component, true, false)
    {
      this.AllowDrop = true;
      this.viewSwitchNavGrid = new Grid();
      this.thingHeaderGrid = new Grid();
      this.Component.ContextNavigationPanel.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnContextNavigationPanelPropertyChanged);
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 0.3, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 0.7, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.thingHeaderGrid, 1.0, GridUnitType.Star);
      this.AddThingHeaderRegion();
      GridUtil.AddRowDef(this.viewSwitchNavGrid, 1.0, GridUnitType.Auto);
      this.AddFunctionBar();
      GridUtil.AddRowDef(this.viewSwitchNavGrid, 1.0, GridUnitType.Auto);
      this.AddViewSwitchNavigationRegion();
      GridUtil.AddRowDef(this.viewSwitchNavGrid, 1.0, GridUnitType.Star);
      this.AddContentRegion();
      GridUtil.PlaceElement(this.Grid, (UIElement) this.thingHeaderGrid, 0, 0);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.viewSwitchNavGrid, 0, 1);
      this.LoadLinkedControlRegion();
    }

    public override void Terminate()
    {
      this.Component.ContextNavigationPanel.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnContextNavigationPanelPropertyChanged);
      base.Terminate();
    }

    public void AddThingHeaderRegion()
    {
      this.m_ThingHeader = new VisualThingHeaderRegion((IModelObject) this.Component.HeaderArea);
      Border border = new Border();
      border.Child = (UIElement) this.m_ThingHeader;
      border.Height = double.NaN;
      border.VerticalAlignment = VerticalAlignment.Top;
      border.BorderThickness = new Thickness(1.0);
      border.BorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 142, (byte) 182, (byte) 223));
      border.CornerRadius = new CornerRadius(1.0);
      GridUtil.PlaceElement(this.thingHeaderGrid, (UIElement) border, 0, 0);
    }

    private void AddFunctionBar()
    {
      if (this.m_VisualFunctionBar != null)
      {
        this.m_VisualFunctionBar.Terminate();
        this.viewSwitchNavGrid.Children.Remove((UIElement) this.m_VisualFunctionBar);
      }
      this.m_VisualFunctionBar = new VisualToolbar((IModelObject) this.Component.ContextNavigationPanel.Functionbar);
      GridUtil.PlaceElement(this.viewSwitchNavGrid, (UIElement) this.m_VisualFunctionBar, 0, 1);
    }

    private void AddViewSwitchNavigationRegion()
    {
      if (this.m_VisualCNR != null)
      {
        this.m_VisualCNR.Terminate();
        this.viewSwitchNavGrid.Children.Remove((UIElement) this.m_VisualCNR);
      }
      this.viewSwitchNav = new ViewSwitchNavArea((IModelObject) this.Component.ContextNavigationPanel);
      this.viewSwitchNav.AddHandler(VisualViewSwitchNavigation.SelectedEvent, (Delegate) new RoutedEventHandler(this.OnOIFNavItemSelectionChanged));
      GridUtil.PlaceElement(this.viewSwitchNavGrid, (UIElement) this.viewSwitchNav, 1, 1);
    }

    private void AddContentRegion()
    {
      if (this.m_ViewControl != null)
      {
        this.m_ViewControl.Terminate();
        this.viewSwitchNavGrid.Children.Remove((UIElement) this.m_ViewControl);
      }
      string selectedNavId = this.viewSwitchNav.SelectedNavID;
      if (selectedNavId == null)
        return;
      foreach (SAP.BYD.LS.UIDesigner.Model.Entities.Controls.View view in this.Component.Views)
      {
        if (view.NavigationItemId == selectedNavId)
        {
          this.m_ViewControl = new VisualView((IModelObject) view);
          GridUtil.PlaceElement(this.viewSwitchNavGrid, (UIElement) this.m_ViewControl, 2, 1);
          break;
        }
      }
    }

    private void OnOIFNavItemSelectionChanged(object sender, RoutedEventArgs e)
    {
      Cursor current = Cursor.Current;
      Cursor.Current = Cursors.WaitCursor;
      try
      {
        if (!(e.OriginalSource is VisualViewSwitchNavigation))
          return;
        this.AddContentRegion();
      }
      finally
      {
        Cursor.Current = current;
      }
    }

    private void OnContextNavigationPanelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      if (!(e.PropertyName == ContextualNavigation.FunctionBarProperty))
        return;
      this.AddFunctionBar();
    }
  }
}
