﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.OVSDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class OVSDesigner : ComponentDesigner
  {
    private static readonly SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox m_ComponentToolbox = ToolboxUtilities.DeserialzeToolboxComponent(Resource.ObjectValueSelectorToolbox);
    private VisualIdentificationRegion m_VisualIDR;
    private VisualContextualNavigation m_VisualCNR;
    private VisualView m_ViewControl;

    public override SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox Toolbox
    {
      get
      {
        return OVSDesigner.m_ComponentToolbox;
      }
    }

    public OVSComponent ObjectValueSelector
    {
      get
      {
        return this.m_ModelObject as OVSComponent;
      }
    }

    public OVSDesigner(IModelObject rootModel)
      : base(rootModel, false, false)
    {
      this.AllowDrop = true;
    }

    public override void LoadView()
    {
      base.LoadView();
      this.AddIdentificationRegion();
      this.AddContextualNavigationRegion();
      this.AddContentRegion();
    }

    private void AddContentRegion()
    {
      if (this.m_ViewControl != null)
        this.m_ViewControl.Terminate();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      this.m_ViewControl = new VisualView((IModelObject) this.ObjectValueSelector.View);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_ViewControl, 2, 0);
    }

    private void AddIdentificationRegion()
    {
      if (this.m_VisualIDR != null)
        this.m_VisualIDR.Terminate();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.m_VisualIDR = new VisualIdentificationRegion((IModelObject) this.ObjectValueSelector.IdentificationRegion);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_VisualIDR, 0, 0);
    }

    private void AddContextualNavigationRegion()
    {
      if (this.m_VisualCNR != null)
        this.m_VisualCNR.Terminate();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.m_VisualCNR = new VisualContextualNavigation((IModelObject) this.ObjectValueSelector.ContextualNavigationRegion);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_VisualCNR, 1, 0);
    }
  }
}
