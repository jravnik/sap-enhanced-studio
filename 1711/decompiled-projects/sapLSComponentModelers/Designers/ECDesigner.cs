﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ECDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  internal class ECDesigner : ComponentDesigner
  {
    private static readonly SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox m_ComponentToolbox = ToolboxUtilities.DeserialzeToolboxComponent(Resource.EmbeddedComponentToolbox);
    private VisualView m_ViewControl;

    public override SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox Toolbox
    {
      get
      {
        return ECDesigner.m_ComponentToolbox;
      }
    }

    internal EmbeddedComponent Component
    {
      get
      {
        return this.m_ModelObject as EmbeddedComponent;
      }
    }

    public ECDesigner(IModelObject rootModel)
      : base(rootModel, true, false)
    {
      this.AllowDrop = true;
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      this.AddContentRegion();
      this.LoadLinkedControlRegion();
    }

    private void AddContentRegion()
    {
      if (this.m_ViewControl != null)
        this.m_ViewControl.Terminate();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      this.m_ViewControl = new VisualView((IModelObject) this.Component.View);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_ViewControl, 2, 0);
    }
  }
}
