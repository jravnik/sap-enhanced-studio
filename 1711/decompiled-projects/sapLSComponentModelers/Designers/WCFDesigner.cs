﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WCFDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.EventArg;
using SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class WCFDesigner : ComponentDesigner
  {
    internal static readonly Brush BRUSH_STATUSBAR_BACKGROUND = (Brush) new SolidColorBrush(Color.FromRgb((byte) 252, (byte) 252, (byte) 252));
    private VisualLink componentID = new VisualLink(VisualLink.LinkStyles.Standard);
    internal const string RESOURCE_BACKGROUND_TABAREA = "#E9F0F5,0.0,#EDF1F5,0.3,#FAFAFB,1.0";
    internal const int CNR_WIDTH = 210;
    internal const int STATUS_BAR_WIDTH = 23;
    internal const string DEFAULT_ADDIN_CONTENTINFO = "WorkCenterView Path: ";
    internal const string DEFAULT_TEXT = "No Component Associated to this View";
    private Grid m_MyInnerGrid;
    private static ImageInfo mainWindowTopLeft;
    private static ImageInfo mainWindowMiddleLeft;
    private static ImageInfo mainWindowBottomLeft;
    private static ImageInfo mainWindowTopCenter;
    private static ImageInfo mainWindowBottomCenter;
    private static ImageInfo mainWindowTopRight;
    private static ImageInfo mainWindowMiddleRight;
    private static ImageInfo mainWindowBottomRight;
    private Canvas contentCanvas;
    private SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea tabArea;
    private IViewSwitch m_SelectedView;
    private static bool initialized;
    private Grid tabGrid;

    internal IViewSwitch selectedView
    {
      get
      {
        return this.m_SelectedView;
      }
      set
      {
        this.m_SelectedView = value;
      }
    }

    internal WorkCenterFloorplan Component
    {
      get
      {
        return this.m_ModelObject as WorkCenterFloorplan;
      }
    }

    public WCFDesigner(IModelObject rootModel)
      : base(rootModel, false, false)
    {
    }

    public override void LoadView()
    {
      base.LoadView();
      this.TerminateChildControls((FrameworkElement) this.Grid);
      if (this.m_TabWindow.Controls.Contains((System.Windows.Forms.Control) this.m_DataModelTab))
        this.m_TabWindow.Controls.Remove((System.Windows.Forms.Control) this.m_DataModelTab);
      if (this.m_TabWindow.Controls.Contains((System.Windows.Forms.Control) this.m_ControllerTab))
        this.m_TabWindow.Controls.Remove((System.Windows.Forms.Control) this.m_ControllerTab);
      this.m_MyInnerGrid = new Grid();
      this.m_MyInnerGrid.Margin = new Thickness(1.0);
      this.AllowDrop = true;
      this.m_MyInnerGrid.AllowDrop = true;
      this.m_MyInnerGrid.DragEnter += new System.Windows.DragEventHandler(this.m_MyInnerGrid_DragEnter);
      this.m_MyInnerGrid.Drop += new System.Windows.DragEventHandler(this.m_MyInnerGrid_Drop);
      this.LoadWindow();
    }

    private void TerminateChildControls(FrameworkElement rootElement)
    {
      if (rootElement is System.Windows.Controls.Panel)
      {
        System.Windows.Controls.Panel panel1 = rootElement as System.Windows.Controls.Panel;
        if (panel1 == null || panel1.Children.Count <= 0)
          return;
        foreach (UIElement child in panel1.Children)
        {
          IDesignerControl designerControl = child as IDesignerControl;
          if (designerControl != null)
          {
            designerControl.Terminate();
          }
          else
          {
            System.Windows.Controls.Panel panel2 = child as System.Windows.Controls.Panel;
            if (panel2 != null)
            {
              this.TerminateChildControls((FrameworkElement) panel2);
            }
            else
            {
              ContentControl contentControl = child as ContentControl;
              if (contentControl != null)
                this.TerminateChildControls((FrameworkElement) contentControl);
            }
          }
        }
        panel1.Children.Clear();
        Grid grid = panel1 as Grid;
        if (grid == null)
          return;
        grid.RowDefinitions.Clear();
        grid.ColumnDefinitions.Clear();
      }
      else
      {
        if (!(rootElement is ContentControl))
          return;
        this.TerminateChildControls((rootElement as ContentControl).Content as FrameworkElement);
      }
    }

    private void InitializeResources()
    {
      WCFDesigner.mainWindowTopLeft = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Windows/Main/MainWindowTopLeft.png,6,64");
      WCFDesigner.mainWindowMiddleLeft = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Windows/Main/MainWindowMiddleLeft.png,6,1");
      WCFDesigner.mainWindowBottomLeft = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Windows/Main/MainWindowBottomLeft.png,6,13");
      WCFDesigner.mainWindowTopCenter = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Windows/Main/MainWindowTopCenter.png,1,64");
      WCFDesigner.mainWindowBottomCenter = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Windows/Main/MainWindowBottomCenter.png,1,13");
      WCFDesigner.mainWindowTopRight = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Windows/Main/MainWindowTopRight.png,14,64");
      WCFDesigner.mainWindowMiddleRight = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Windows/Main/MainWindowMiddleRight.png,14,1");
      WCFDesigner.mainWindowBottomRight = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Windows/Main/MainWindowBottomRight.png,14,613");
      WCFDesigner.initialized = true;
    }

    protected override void LoadWindow()
    {
      if (!WCFDesigner.initialized)
        this.InitializeResources();
      this.Grid.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(1.0, GridUnitType.Pixel),
        MinHeight = 64.0
      });
      this.Grid.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(1.0, GridUnitType.Pixel),
        MinHeight = 64.0
      });
      this.Grid.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(10.0, GridUnitType.Star)
      });
      this.Grid.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(1.0, GridUnitType.Pixel),
        MinHeight = 23.0
      });
      this.Grid.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(1.0, GridUnitType.Pixel),
        MinHeight = 13.0
      });
      this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Pixel),
        MinWidth = 6.0
      });
      this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Pixel),
        MinWidth = 210.0
      });
      this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Star)
      });
      this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Pixel),
        MinWidth = 14.0
      });
      Image image1 = new Image();
      image1.Source = ResourceUtil.RetrieveImage(WCFDesigner.mainWindowTopLeft.Name);
      this.Grid.Children.Add((UIElement) image1);
      image1.SetValue(Grid.RowProperty, (object) 0);
      image1.SetValue(Grid.ColumnProperty, (object) 0);
      Image image2 = new Image();
      image2.Stretch = Stretch.Fill;
      image2.StretchDirection = StretchDirection.Both;
      image2.Source = ResourceUtil.RetrieveImage(WCFDesigner.mainWindowMiddleLeft.Name);
      this.Grid.Children.Add((UIElement) image2);
      image2.SetValue(Grid.RowProperty, (object) 2);
      image2.SetValue(Grid.ColumnProperty, (object) 0);
      image2.SetValue(Grid.RowSpanProperty, (object) 2);
      Image image3 = new Image();
      image3.Source = ResourceUtil.RetrieveImage(WCFDesigner.mainWindowBottomLeft.Name);
      image3.Stretch = Stretch.Fill;
      image3.StretchDirection = StretchDirection.UpOnly;
      this.Grid.Children.Add((UIElement) image3);
      image3.SetValue(Grid.RowProperty, (object) 4);
      image3.SetValue(Grid.ColumnProperty, (object) 0);
      this.AddWorkCenterTabArea();
      ScrollViewer scrollViewer = new ScrollViewer();
      scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
      scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
      VisualWCFContextualNavigationRegion navigationRegion = new VisualWCFContextualNavigationRegion((IModelObject) this.Component.CenterStructure);
      navigationRegion.OnViewSelected += new ViewSelected(this.OnViewSelected);
      scrollViewer.SetValue(Grid.RowProperty, (object) 2);
      scrollViewer.SetValue(Grid.ColumnProperty, (object) 1);
      scrollViewer.Content = (object) navigationRegion;
      this.Grid.Children.Add((UIElement) scrollViewer);
      Image image4 = new Image();
      image4.Stretch = Stretch.Fill;
      image4.StretchDirection = StretchDirection.Both;
      image4.Source = ResourceUtil.RetrieveImage(WCFDesigner.mainWindowTopCenter.Name);
      this.Grid.Children.Add((UIElement) image4);
      image4.SetValue(Grid.RowProperty, (object) 0);
      image4.SetValue(Grid.ColumnProperty, (object) 1);
      image4.SetValue(Grid.ColumnSpanProperty, (object) 2);
      this.m_MyInnerGrid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Star)
      });
      this.m_MyInnerGrid.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(1.0, GridUnitType.Star)
      });
      if (this.Component.CenterStructure.ViewSwitches != null && this.Component.CenterStructure.ViewSwitches.Count > 0)
        this.SelectedViewSwitch((IViewSwitch) this.Component.CenterStructure.ViewSwitches[0]);
      this.m_MyInnerGrid.Background = (Brush) Brushes.Transparent;
      this.Grid.Children.Add((UIElement) this.m_MyInnerGrid);
      this.m_MyInnerGrid.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.m_MyInnerGrid.VerticalAlignment = VerticalAlignment.Stretch;
      this.m_MyInnerGrid.SetValue(Grid.RowProperty, (object) 2);
      this.m_MyInnerGrid.SetValue(Grid.ColumnProperty, (object) 2);
      StackPanel stackPanel = new StackPanel();
      stackPanel.Background = WCFDesigner.BRUSH_STATUSBAR_BACKGROUND;
      this.Grid.Children.Add((UIElement) stackPanel);
      stackPanel.SetValue(Grid.RowProperty, (object) 3);
      stackPanel.SetValue(Grid.ColumnProperty, (object) 1);
      stackPanel.SetValue(Grid.ColumnSpanProperty, (object) 2);
      Image image5 = new Image();
      image5.Source = (ImageSource) ApplicationWindowImages.BrandingLogo;
      image5.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      image5.VerticalAlignment = VerticalAlignment.Top;
      image5.Margin = new Thickness(0.0, 3.0, 0.0, 0.0);
      image5.Stretch = Stretch.None;
      this.Grid.Children.Add((UIElement) image5);
      image5.SetValue(Grid.RowProperty, (object) 3);
      image5.SetValue(Grid.ColumnProperty, (object) 1);
      image5.SetValue(Grid.ColumnSpanProperty, (object) 2);
      Image image6 = new Image();
      image6.Stretch = Stretch.Fill;
      image6.StretchDirection = StretchDirection.Both;
      image6.Source = ResourceUtil.RetrieveImage(WCFDesigner.mainWindowBottomCenter.Name);
      this.Grid.Children.Add((UIElement) image6);
      image6.SetValue(Grid.RowProperty, (object) 4);
      image6.SetValue(Grid.ColumnProperty, (object) 1);
      image6.SetValue(Grid.ColumnSpanProperty, (object) 2);
      Image image7 = new Image();
      image7.Source = ResourceUtil.RetrieveImage(WCFDesigner.mainWindowTopRight.Name);
      this.Grid.Children.Add((UIElement) image7);
      image7.SetValue(Grid.RowProperty, (object) 0);
      image7.SetValue(Grid.ColumnProperty, (object) 3);
      Image image8 = new Image();
      image8.Stretch = Stretch.Fill;
      image8.StretchDirection = StretchDirection.Both;
      image8.Source = ResourceUtil.RetrieveImage(WCFDesigner.mainWindowMiddleRight.Name);
      this.Grid.Children.Add((UIElement) image8);
      image8.SetValue(Grid.RowProperty, (object) 2);
      image8.SetValue(Grid.ColumnProperty, (object) 3);
      image8.SetValue(Grid.RowSpanProperty, (object) 2);
      Image image9 = new Image();
      image9.Source = ResourceUtil.RetrieveImage(WCFDesigner.mainWindowBottomRight.Name);
      image9.Stretch = Stretch.Fill;
      image9.StretchDirection = StretchDirection.UpOnly;
      this.Grid.Children.Add((UIElement) image9);
      image9.SetValue(Grid.RowProperty, (object) 4);
      image9.SetValue(Grid.ColumnProperty, (object) 3);
    }

    private void AddViewContent(string Text, object tagInfo)
    {
      this.m_MyInnerGrid.Children.Clear();
      this.componentID = new VisualLink(VisualLink.LinkStyles.Standard);
      this.componentID.LinkClicked -= new EventHandler<EventArgs>(this.componentID_LinkClicked);
      this.componentID.LinkClicked += new EventHandler<EventArgs>(this.componentID_LinkClicked);
      this.componentID.Text = Text;
      this.componentID.Tag = tagInfo;
      this.componentID.VerticalAlignment = VerticalAlignment.Center;
      this.componentID.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      this.componentID.TextHorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      this.m_MyInnerGrid.Children.Add((UIElement) this.componentID);
      this.componentID.SetValue(Grid.RowProperty, (object) 0);
      this.componentID.SetValue(Grid.ColumnProperty, (object) 0);
    }

    private void AddWorkCenterTabArea()
    {
      this.tabGrid = new Grid();
      this.tabGrid.Background = (Brush) ResourceUtil.GetLinearGradientBrush("#E9F0F5,0.0,#EDF1F5,0.3,#FAFAFB,1.0");
      this.tabGrid.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.tabGrid.VerticalAlignment = VerticalAlignment.Stretch;
      this.tabGrid.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(1.0, GridUnitType.Pixel),
        MinHeight = 64.0
      });
      this.tabGrid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Pixel),
        MinWidth = 6.0
      });
      this.tabGrid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Star)
      });
      this.tabGrid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Pixel),
        MinWidth = 14.0
      });
      Image image1 = new Image();
      image1.Source = ResourceUtil.RetrieveImage(WCFDesigner.mainWindowMiddleLeft.Name);
      image1.Stretch = Stretch.Fill;
      image1.StretchDirection = StretchDirection.Both;
      this.tabGrid.Children.Add((UIElement) image1);
      image1.SetValue(Grid.RowProperty, (object) 0);
      image1.SetValue(Grid.ColumnProperty, (object) 0);
      this.contentCanvas = new Canvas();
      this.tabGrid.Children.Add((UIElement) this.contentCanvas);
      this.contentCanvas.SetValue(Grid.RowProperty, (object) 0);
      this.contentCanvas.SetValue(Grid.ColumnProperty, (object) 1);
      this.contentCanvas.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.contentCanvas.VerticalAlignment = VerticalAlignment.Stretch;
      this.tabArea = new SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.TabArea.TabArea(this.Component.CenterStructure);
      this.contentCanvas.Children.Add((UIElement) this.tabArea);
      Image image2 = new Image();
      image2.Stretch = Stretch.Fill;
      image2.StretchDirection = StretchDirection.Both;
      image2.Source = ResourceUtil.RetrieveImage(WCFDesigner.mainWindowMiddleRight.Name);
      this.tabGrid.Children.Add((UIElement) image2);
      image2.SetValue(Grid.RowProperty, (object) 0);
      image2.SetValue(Grid.ColumnProperty, (object) 2);
      this.Grid.Children.Add((UIElement) this.tabGrid);
      this.tabGrid.SetValue(Grid.RowProperty, (object) 1);
      this.tabGrid.SetValue(Grid.ColumnProperty, (object) 0);
      this.tabGrid.SetValue(Grid.ColumnSpanProperty, (object) 4);
    }

    private void LoadBrandingImage()
    {
      Image image = new Image();
      image.Source = (ImageSource) ApplicationWindowImages.BrandingLogo;
      image.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      image.VerticalAlignment = VerticalAlignment.Top;
      image.Margin = new Thickness(0.0, 3.0, 0.0, 0.0);
      image.Stretch = Stretch.None;
      this.Grid.Children.Add((UIElement) image);
      image.SetValue(Grid.RowProperty, (object) 3);
      image.SetValue(Grid.ColumnProperty, (object) 1);
      image.SetValue(Grid.ColumnSpanProperty, (object) 3);
    }

    private void OnViewSelected(IViewSwitch selectedViewSwitch)
    {
      this.SelectedViewSwitch(selectedViewSwitch);
    }

    private void SelectedViewSwitch(IViewSwitch selectedViewSwitch)
    {
      IViewSwitch viewSwitch = selectedViewSwitch;
      this.selectedView = viewSwitch;
      this.componentID.Tag = (object) viewSwitch.TargetComponentId;
      this.componentID.Text = string.Empty;
      if (viewSwitch.SubViewSwitches != null && viewSwitch.SubViewSwitches.Count > 0)
      {
        this.selectedView = (IViewSwitch) viewSwitch.SubViewSwitches[0];
        if (viewSwitch.SubViewSwitches[0].TargetComponentId != null && viewSwitch.SubViewSwitches[0].TargetComponentId != "")
          this.AddViewContent("WorkCenterView Path: " + viewSwitch.SubViewSwitches[0].TargetComponentId, (object) viewSwitch.SubViewSwitches[0].TargetComponentId);
        else
          this.AddViewContent("No Component Associated to this View", (object) null);
      }
      else if (viewSwitch != null && viewSwitch.TargetComponentId != null && viewSwitch.TargetComponentId != "")
        this.AddViewContent("WorkCenterView Path: " + viewSwitch.TargetComponentId, (object) viewSwitch.TargetComponentId);
      else
        this.AddViewContent("No Component Associated to this View", (object) null);
    }

    private void m_MyInnerGrid_DragEnter(object sender, System.Windows.DragEventArgs e)
    {
      System.Windows.Forms.IDataObject dataObject = DragDropUtil.GetDataObject(e.Data);
      ConfigurationExplorerDragEventArgs explorerDragEventArgs = dataObject == null ? e.Data.GetData(typeof (ConfigurationExplorerDragEventArgs).FullName) as ConfigurationExplorerDragEventArgs : dataObject.GetData(typeof (ConfigurationExplorerDragEventArgs).FullName) as ConfigurationExplorerDragEventArgs;
      if (explorerDragEventArgs != null && explorerDragEventArgs.ComponentDetails != null && explorerDragEventArgs.ComponentDetails.ConfigurationType == "WCVIEW")
        e.Effects = System.Windows.DragDropEffects.Copy;
      else
        e.Effects = System.Windows.DragDropEffects.None;
    }

    private void m_MyInnerGrid_Drop(object sender, System.Windows.DragEventArgs e)
    {
      if (this.Component.IsDisplayOnly)
      {
        int num = (int) DisplayMessage.Show("Component is in DisplayOnly mode.Please open the component in edit mode before making changes.");
      }
      else
      {
        System.Windows.Forms.IDataObject dataObject = DragDropUtil.GetDataObject(e.Data);
        ConfigurationExplorerDragEventArgs explorerDragEventArgs = dataObject == null ? e.Data.GetData(typeof (ConfigurationExplorerDragEventArgs).FullName) as ConfigurationExplorerDragEventArgs : dataObject.GetData(typeof (ConfigurationExplorerDragEventArgs).FullName) as ConfigurationExplorerDragEventArgs;
        if (explorerDragEventArgs == null || explorerDragEventArgs.ComponentDetails == null || !(explorerDragEventArgs.ComponentDetails.ConfigurationType == "WCVIEW") && !(explorerDragEventArgs.ComponentDetails.ConfigurationType == "TaskListView") || DisplayMessage.Show("ViewSwitch name will be overidden by WCView's Floorplan Title.Do you want to continue?", MessageBoxButtons.YesNo) != DialogResult.Yes)
          return;
        OberonViewSwitch selectedView = this.selectedView as OberonViewSwitch;
        if (selectedView == null)
          return;
        selectedView.AddContent(explorerDragEventArgs.ComponentDetails);
        this.AddViewContent("WorkCenterView Path: " + selectedView.TargetComponentId, (object) selectedView.TargetComponentId);
      }
    }

    private void componentID_LinkClicked(object sender, EventArgs e)
    {
      try
      {
        if (this.componentID.Tag == null)
          return;
        string tag = this.componentID.Tag as string;
        if (tag == null)
          return;
        this.OpenRelatedFloorPlan(tag);
      }
      catch (Exception ex)
      {
        int num = (int) DisplayMessage.Show("Component Cannot be Opened");
      }
    }
  }
}
