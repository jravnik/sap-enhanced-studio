﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ComponentDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Controllers;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using SAP.BYD.LS.UIDesigner.UICore.DataModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public abstract class ComponentDesigner : BaseDesigner
  {
    private string m_PreviewLanguage = "EN";
    private bool m_WithPseudoTranslation;
    protected System.Windows.Forms.TabControl m_TabWindow;
    protected TabPage m_DesignerTab;
    protected TabPage m_DataModelTab;
    protected TabPage m_ControllerTab;
    protected TabPage m_OberonPreviewTab;
    protected System.Windows.Forms.WebBrowser m_OberonPreviewBrowser;
    protected DataModelDesigner m_DataModelDesigner;
    protected ElementHost m_ControllerDesigner;
    protected VisualController m_ControllerVisualControl;
    protected UndoRedoManager m_URManager;
    private bool hasMenu;
    private List<string> m_TempPreviewIds;
    private VisualLinkedRegion m_VisualLinkedRegion;
    public ComponentDesigner m_ActiveLinkedDesigner;

    public UndoRedoManager URManager
    {
      get
      {
        return this.m_URManager;
      }
      set
      {
        this.m_URManager = value;
      }
    }

    public System.Windows.Forms.WebBrowser OberonBrowserControl
    {
      get
      {
        return this.m_OberonPreviewBrowser;
      }
    }

    public virtual string SelectionText
    {
      get
      {
        return "Floorplan";
      }
    }

    public ComponentDesigner(IModelObject associatedModel, bool hasWindowFrame, bool hasMenu)
      : base(associatedModel, hasWindowFrame)
    {
      this.m_TempPreviewIds = new List<string>();
      this.InitializeComponentDesigner();
      this.m_URManager = new UndoRedoManager();
      this.hasMenu = hasMenu;
    }

    protected virtual void InitializeComponentDesigner()
    {
      this.m_TabWindow = new System.Windows.Forms.TabControl();
      this.m_DesignerTab = new TabPage();
      this.m_DataModelTab = new TabPage();
      this.m_ControllerTab = new TabPage();
      this.m_OberonPreviewTab = new TabPage();
      this.m_OberonPreviewBrowser = new System.Windows.Forms.WebBrowser();
      this.m_TabWindow.SuspendLayout();
      this.m_DesignerTab.SuspendLayout();
      this.m_DataModelTab.SuspendLayout();
      this.m_ControllerTab.SuspendLayout();
      this.m_OberonPreviewTab.SuspendLayout();
      this.SuspendLayout();
      this.m_TabWindow.Alignment = TabAlignment.Bottom;
      this.m_TabWindow.Controls.Add((System.Windows.Forms.Control) this.m_DesignerTab);
      this.m_TabWindow.Controls.Add((System.Windows.Forms.Control) this.m_DataModelTab);
      this.m_TabWindow.Controls.Add((System.Windows.Forms.Control) this.m_ControllerTab);
      //this.m_TabWindow.Controls.Add((System.Windows.Forms.Control) this.m_OberonPreviewTab);
      this.m_TabWindow.SelectedIndex = 0;
      this.m_TabWindow.Dock = DockStyle.Fill;
      this.m_TabWindow.SelectedIndexChanged += new EventHandler(this.OnTabWindowSelectedIndexChanged);
      this.m_DesignerTab.BackColor = ComponentModelersConstants.WF_CONTENT_AREA_COLOR;
      this.m_DesignerTab.Text = "Designer";
      this.m_ElementHost = new ElementHost();
      this.m_ElementHost.Dock = DockStyle.Fill;
      this.m_DesignerTab.Controls.Add((System.Windows.Forms.Control) this.m_ElementHost);
      this.m_DataModelTab.BackColor = ComponentModelersConstants.WF_CONTENT_AREA_COLOR;
      this.m_DataModelTab.Text = "DataModel";
      this.m_ControllerTab.BackColor = ComponentModelersConstants.WF_CONTENT_AREA_COLOR;
      this.m_ControllerTab.Text = "Controller";
      this.m_OberonPreviewTab.BackColor = ComponentModelersConstants.WF_CONTENT_AREA_COLOR;
      this.m_OberonPreviewTab.Text = "Preview";
      this.m_OberonPreviewBrowser.Dock = DockStyle.Fill;
      this.m_OberonPreviewTab.Controls.Add((System.Windows.Forms.Control) this.m_OberonPreviewBrowser);
      this.AllowDrop = true;
      this.BackColor = System.Drawing.Color.Transparent;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BorderStyle = BorderStyle.FixedSingle;
      this.Controls.Add((System.Windows.Forms.Control) this.m_TabWindow);
      this.Margin = new Padding(0);
      this.Size = new System.Drawing.Size(590, 340);
      this.Click += new EventHandler(this.OnFloorplanDesignerClick);
      this.m_TabWindow.ResumeLayout(false);
      this.m_DataModelTab.ResumeLayout(false);
      this.m_ControllerTab.ResumeLayout(false);
      this.m_OberonPreviewTab.ResumeLayout(false);
      this.m_DesignerTab.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public override void Terminate()
    {
      try
      {
        base.Terminate();
        ComponentModelerUtilities.Instance.TerminatePanelControls((FrameworkElement) this.Grid);
        if (this.m_VisualLinkedRegion != null)
          this.m_VisualLinkedRegion.Terminate();
        if (this.m_OberonPreviewBrowser != null && !this.m_OberonPreviewBrowser.IsDisposed)
        {
          this.m_OberonPreviewBrowser.Dispose();
          this.m_OberonPreviewTab.Controls.Clear();
          this.m_OberonPreviewBrowser = (System.Windows.Forms.WebBrowser) null;
        }
        if (this.m_ControllerDesigner != null && !this.m_ControllerDesigner.IsDisposed)
        {
          this.m_ControllerVisualControl.Terminate();
          this.m_ControllerDesigner.Dispose();
          this.m_ControllerTab.Controls.Clear();
          this.m_ControllerDesigner = (ElementHost) null;
        }
        if (this.m_DataModelDesigner != null && !this.m_DataModelDesigner.IsDisposed)
        {
          this.m_DataModelDesigner.OnTerminate();
          this.m_DataModelDesigner.Dispose();
          this.m_DataModelTab.Controls.Clear();
          this.m_DataModelDesigner.OpenToolWindowInvoked -= new OpenToolWindowEventHandler(this.m_DataModelDesigner_OpenToolWindowInvoked);
          this.m_DataModelDesigner = (DataModelDesigner) null;
        }
        for (int index = this.m_TempPreviewIds.Count - 1; index > -1; --index)
        {
          string tempPreviewId = this.m_TempPreviewIds[index];
          ModelLayer.ProjectWorkspaceManager.HardDeleteComponent(tempPreviewId, "$TMP", (string) null);
          this.m_TempPreviewIds.Remove(tempPreviewId);
        }
        ComponentModelerUtilities.Instance.ReleaseDesigner(this.Model as IFloorplan);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Terminate of the component caused an exception", ex));
      }
    }

    public override void ReloadView()
    {
      this.Terminate();
      if (this.m_ModelObject != null)
      {
        ComponentModelerUtilities.Instance.AddDesigner(this.m_ModelObject as IFloorplan, (BaseDesigner) this);
        this.m_ModelObject.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
        this.m_ModelObject.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.m_ModelObject.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      }
      if (!(this.m_ModelObject is XMLObject) && !(this.m_ModelObject is PortTypePackage) && (!(this.m_ModelObject is UISettingsComponent) && !(this.m_ModelObject is UIDependencyComponent)))
      {
        this.LoadDataModelView();
        this.LoadControllerView();
      }
      this.m_OberonPreviewBrowser = new System.Windows.Forms.WebBrowser();
      this.m_OberonPreviewBrowser.Dock = DockStyle.Fill;
      this.m_OberonPreviewTab.Controls.Add((System.Windows.Forms.Control) this.m_OberonPreviewBrowser);
      this.LoadView();
    }

    public override void RefreshEditMode(IModelObject newModel)
    {
      if (this.m_DataModelDesigner != null)
        this.m_DataModelDesigner.RefreshEditMode();
      if (this.m_ControllerVisualControl == null)
        return;
      this.m_ControllerVisualControl.RefreshEditMode();
    }

    protected override void BringSelectedModelToFront(IModelObject modelToSelect)
    {
      List<IModelObject> modelObjectList = new List<IModelObject>();
      switch (modelToSelect.GetConfigurationArea())
      {
        case "Designer":
          this.m_TabWindow.SelectedIndex = 0;
          base.BringSelectedModelToFront(modelToSelect);
          break;
        case "Controller":
          this.m_TabWindow.SelectedIndex = 2;
          this.m_ControllerVisualControl.SelectModelInControllerTab(modelToSelect, this.m_DesignerControlDictionary);
          break;
        case "DataModel":
          this.m_TabWindow.SelectedIndex = 1;
          this.m_DataModelDesigner.SelectNodeInDataModelTab(modelToSelect);
          break;
      }
    }

    public void ModelAddedCall(IModelObject m_Model, int positionInParent)
    {
      this.m_URManager.ActionPerformed(ActionType.Add, m_Model, positionInParent);
    }

    public override void SelectObject(IModelObject objectToSelect)
    {
      try
      {
        if (objectToSelect == null)
          return;
        if (this.SelectedVisualControl != null)
        {
          this.SelectedVisualControl.UnSelect();
          this.SelectedVisualControl = (IDesignerControl) null;
        }
        if (string.IsNullOrEmpty(objectToSelect.Id))
          return;
        if (!this.m_DesignerControlDictionary.ContainsKey(objectToSelect.Id))
        {
          this.BringSelectedModelToFront(objectToSelect);
        }
        else
        {
          if (this.m_TabWindow.SelectedTab.Text != objectToSelect.GetConfigurationArea())
            this.BringSelectedModelToFront(objectToSelect);
          this.SelectedVisualControl = this.m_DesignerControlDictionary[objectToSelect.Id];
          if (this.SelectedVisualControl == null)
            return;
          this.SelectedVisualControl.SelectMe();
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Select Object operation of Designer failed to execute.", ex));
      }
    }

    public virtual void AddModelObject(ToolboxSelectedControlInfo info)
    {
    }

    public void ModelRemovedCall(IModelObject m_Model, int positionInParent)
    {
      this.m_URManager.ActionPerformed(ActionType.Delete, m_Model, positionInParent);
    }

    public void PropertyChangedCall(IModelObject m_Model, string propertyName, object oldValue, object newValue)
    {
      this.m_URManager.ActionPerformed(ActionType.Change, m_Model, propertyName, oldValue, newValue);
    }

    public override void LoadView()
    {
      base.LoadView();
      this.rootGridControl.LayoutUpdated += new EventHandler(this.rootGridControl_LayoutUpdated);
      if (!this.hasMenu)
        return;
      this.LoadBrandingImage();
    }

    private void LoadBrandingImage()
    {
      if (this.Grid.Parent == null)
        return;
      System.Windows.Controls.Image image = new System.Windows.Controls.Image();
      image.Source = (ImageSource) ApplicationWindowImages.BrandingLogo;
      image.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      image.VerticalAlignment = VerticalAlignment.Top;
      image.Margin = new Thickness(0.0, 3.0, 0.0, 0.0);
      image.Stretch = Stretch.None;
      Grid parent = this.Grid.Parent as Grid;
      GridUtil.PlaceElement(parent, (UIElement) image, parent.RowDefinitions.Count - 1, 1);
    }

    protected void LoadDataModelView()
    {
      if (this.m_DataModelDesigner == null)
      {
        this.m_DataModelDesigner = new DataModelDesigner();
        this.m_DataModelDesigner.Dock = DockStyle.Fill;
        this.m_DataModelDesigner.SelectionChanged += new SAP.BYD.LS.UIDesigner.UICore.Core.SelectionChangedEventHandler(this.OnSelectionChangedEventHandler);
        this.m_DataModelDesigner.OpenToolWindowInvoked += new OpenToolWindowEventHandler(this.m_DataModelDesigner_OpenToolWindowInvoked);
        this.m_DataModelTab.Controls.Add((System.Windows.Forms.Control) this.m_DataModelDesigner);
      }
      this.m_DataModelDesigner.Init((this.Model as IFloorplan).ImplementationController);
    }

    private void m_DataModelDesigner_OpenToolWindowInvoked(object sender, OpenToolWindowEventArgs e)
    {
      this.OpenToolWindow(sender, e);
    }

    protected void LoadControllerView()
    {
      if (this.m_ControllerDesigner == null)
      {
        this.m_ControllerDesigner = new ElementHost();
        this.m_ControllerVisualControl = new VisualController(this.Model as IFloorplan);
        this.m_ControllerVisualControl.LoadController();
        this.m_ControllerVisualControl.OpenToolWindowInvoked += new OpenToolWindowEventHandler(this.m_ControllerVisualControl_OpenToolWindowInvoked);
        this.m_ControllerDesigner.Child = (UIElement) this.m_ControllerVisualControl;
        this.m_ControllerDesigner.Dock = DockStyle.Fill;
        this.m_ControllerTab.Controls.Add((System.Windows.Forms.Control) this.m_ControllerDesigner);
      }
      else
      {
        this.m_ControllerVisualControl.ReloadSimpleNavigation();
        this.m_ControllerVisualControl.ReloadMessageNode();
        this.m_ControllerVisualControl.ReloadControllerImplPointsRootNode();
      }
      this.OnDesignerControlSelectionChange((object) null, (IModelObject) null);
    }

    private void m_ControllerVisualControl_OpenToolWindowInvoked(object sender, OpenToolWindowEventArgs e)
    {
      this.OpenToolWindow(sender, e);
    }

    private UXComponentType GetComponentType()
    {
      if (this.Model == null)
        return UXComponentType.application;
      switch ((this.Model as IFloorplan).Type)
      {
        case FloorplanType.WCF:
          return UXComponentType.workCenter;
        case FloorplanType.EC:
          return UXComponentType.embedded;
        case FloorplanType.WCVIEW:
          return UXComponentType.workCenterView;
        case FloorplanType.OVS:
          return UXComponentType.ovs;
        case FloorplanType.PTP:
          return UXComponentType.portTypePackage;
        default:
          return UXComponentType.application;
      }
    }

    protected void LoadLinkedControlRegion()
    {
      GridUtil.AddRowDef(this.rootGridControl, 1.0, GridUnitType.Auto);
      IFloorplan modelObject = this.m_ModelObject as IFloorplan;
      if (modelObject == null || modelObject.UXView == null)
        return;
      this.m_VisualLinkedRegion = new VisualLinkedRegion((IModelObject) modelObject.UXView);
      this.m_VisualLinkedRegion.OnLinkedControlDoubleClicked += new EventHandler(this.OnLinkedControlDoubleClicked);
      GridUtil.PlaceElement(this.rootGridControl, (UIElement) this.m_VisualLinkedRegion, this.rootGridControl.RowDefinitions.Count - 1, 0, 1, 3);
    }

    internal void OnLinkedControlDoubleClicked(object sender, EventArgs e)
    {
      UIElement elementAt = GridUtil.GetElementAt(this.rootGridControl, 1, 1);
      if (elementAt != null)
        GridUtil.RemoveElement(this.rootGridControl, elementAt, false, 1, 1);
      if (sender is VisualActionForm)
      {
        ActionFormDesigner actionFormDesigner = new ActionFormDesigner((IModelObject) (sender as VisualActionForm).ActionFormModel);
        actionFormDesigner.Dock = DockStyle.Fill;
        actionFormDesigner.BackToDesignerClicked += new EventHandler(this.OnBackToDesignerClicked);
        this.m_ActiveLinkedDesigner = (ComponentDesigner) actionFormDesigner;
        if (actionFormDesigner == null)
          return;
        GridUtil.PlaceElement(this.rootGridControl, (UIElement) actionFormDesigner.ActionFormGrid, 1, 1);
      }
      else if (sender is VisualPlaceableLayout)
      {
        PlaceableFieldsDesigner placeableFieldsDesigner = new PlaceableFieldsDesigner((IModelObject) (sender as VisualPlaceableLayout).PlaceableFields);
        placeableFieldsDesigner.Dock = DockStyle.Fill;
        placeableFieldsDesigner.BackToDesignerClicked += new EventHandler(this.OnBackToDesignerClicked);
        this.m_ActiveLinkedDesigner = (ComponentDesigner) placeableFieldsDesigner;
        if (placeableFieldsDesigner == null)
          return;
        GridUtil.PlaceElement(this.rootGridControl, (UIElement) placeableFieldsDesigner.PlaceableFieldGrid, 1, 1);
      }
      else if (sender is VisualSideCarViewPanel)
      {
        SideCarViewPanelDesigner viewPanelDesigner = new SideCarViewPanelDesigner((IModelObject) (sender as VisualSideCarViewPanel).SideCarViewPanel);
        viewPanelDesigner.Dock = DockStyle.Fill;
        viewPanelDesigner.BackToDesignerClicked += new EventHandler(this.OnBackToDesignerClicked);
        this.m_ActiveLinkedDesigner = (ComponentDesigner) viewPanelDesigner;
        if (viewPanelDesigner == null)
          return;
        GridUtil.PlaceElement(this.rootGridControl, (UIElement) viewPanelDesigner.SideCarViewPanelGrid, 1, 1);
      }
      else if (sender is VisualInComponentModalDialog)
      {
        InCompoentModalDialogDesigner modalDialogDesigner = new InCompoentModalDialogDesigner((sender as VisualInComponentModalDialog).Model);
        modalDialogDesigner.Dock = DockStyle.Fill;
        modalDialogDesigner.BackToDesignerClicked += new EventHandler(this.OnBackToDesignerClicked);
        this.m_ActiveLinkedDesigner = (ComponentDesigner) modalDialogDesigner;
        if (modalDialogDesigner == null)
          return;
        GridUtil.PlaceElement(this.rootGridControl, (UIElement) modalDialogDesigner.InComponentModalDialogGrid, 1, 1);
      }
      else if (sender is VisualListModificationForm)
      {
        ListModificationFormDesigner modificationFormDesigner = new ListModificationFormDesigner((sender as VisualListModificationForm).Model);
        modificationFormDesigner.BackToDesignerClicked += new EventHandler(this.OnBackToDesignerClicked);
        this.m_ActiveLinkedDesigner = (ComponentDesigner) modificationFormDesigner;
        GridUtil.PlaceElement(this.rootGridControl, (UIElement) modificationFormDesigner.ListModificationDesignerGrid, 1, 1);
      }
      else if (sender is VisualGridLayoutSnippet)
      {
        GridLayoutSnippetDesigner layoutSnippetDesigner = new GridLayoutSnippetDesigner((sender as VisualGridLayoutSnippet).Model);
        layoutSnippetDesigner.BackToDesignerClicked += new EventHandler(this.OnBackToDesignerClicked);
        this.m_ActiveLinkedDesigner = (ComponentDesigner) layoutSnippetDesigner;
        GridUtil.PlaceElement(this.rootGridControl, (UIElement) layoutSnippetDesigner.SnippetGridControl, 1, 1);
      }
      else
      {
        if (!(sender is VisualListColumn))
          return;
        ListColumnDesigner listColumnDesigner = new ListColumnDesigner((sender as VisualListColumn).Model);
        listColumnDesigner.BackToDesignerClicked += new EventHandler(this.OnBackToDesignerClicked);
        this.m_ActiveLinkedDesigner = (ComponentDesigner) listColumnDesigner;
        GridUtil.PlaceElement(this.rootGridControl, (UIElement) listColumnDesigner.ListColumnGrid, 1, 1);
      }
    }

    private void OnBackToDesignerClicked(object sender, EventArgs e)
    {
      try
      {
        if (this.m_ActiveLinkedDesigner is ActionFormDesigner)
          GridUtil.RemoveElement(this.rootGridControl, (UIElement) (this.m_ActiveLinkedDesigner as ActionFormDesigner).ActionFormGrid, false, 1, 1);
        else if (this.m_ActiveLinkedDesigner is PlaceableFieldsDesigner)
          GridUtil.RemoveElement(this.rootGridControl, (UIElement) (this.m_ActiveLinkedDesigner as PlaceableFieldsDesigner).PlaceableFieldGrid, false, 1, 1);
        else if (this.m_ActiveLinkedDesigner is SideCarViewPanelDesigner)
          GridUtil.RemoveElement(this.rootGridControl, (UIElement) (this.m_ActiveLinkedDesigner as SideCarViewPanelDesigner).SideCarViewPanelGrid, false, 1, 1);
        else if (this.m_ActiveLinkedDesigner is InCompoentModalDialogDesigner)
          GridUtil.RemoveElement(this.rootGridControl, (UIElement) (this.m_ActiveLinkedDesigner as InCompoentModalDialogDesigner).InComponentModalDialogGrid, false, 1, 1);
        else if (this.m_ActiveLinkedDesigner is ListModificationFormDesigner)
          GridUtil.RemoveElement(this.rootGridControl, (UIElement) (this.m_ActiveLinkedDesigner as ListModificationFormDesigner).ListModificationDesignerGrid, false, 1, 1);
        else if (this.m_ActiveLinkedDesigner is GridLayoutSnippetDesigner)
          GridUtil.RemoveElement(this.rootGridControl, (UIElement) (this.m_ActiveLinkedDesigner as GridLayoutSnippetDesigner).SnippetGridControl, false, 1, 1);
        else if (this.m_ActiveLinkedDesigner is ListColumnDesigner)
          GridUtil.RemoveElement(this.rootGridControl, (UIElement) (this.m_ActiveLinkedDesigner as ListColumnDesigner).ListColumnGrid, false, 1, 1);
        if (!this.rootGridControl.Children.Contains((UIElement) this.Grid))
          GridUtil.PlaceElement(this.rootGridControl, (UIElement) this.Grid, 1, 1);
        this.m_ActiveLinkedDesigner = (ComponentDesigner) null;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    protected void OnTabWindowSelectedIndexChanged(object sender, EventArgs e)
    {
      TabPage tabPage = this.m_TabWindow.TabPages[this.m_TabWindow.SelectedIndex];
      if (tabPage != null && tabPage == this.m_DataModelTab)
        this.LoadDataModelView();
      else if (tabPage != null && tabPage == this.m_ControllerTab)
        this.LoadControllerView();
      else if (tabPage != null && tabPage == this.m_DesignerTab)
      {
        this.OnDesignerControlSelectionChange((object) this, this.m_ModelObject);
      }
      else
      {
        if (tabPage == null || tabPage != this.m_OberonPreviewTab)
          return;
        this.RefreshPreviewTab();
      }
    }

    private void RefreshPreviewTab()
    {
      try
      {
        Cursor current = Cursor.Current;
        Cursor.Current = Cursors.WaitCursor;
        this.OnDesignerControlSelectionChange((object) null, (IModelObject) null);
        string urlString = ModelLayer.ObjectManager.PreviewInOberonRuntime(this.Model as IFloorplan, this.m_TempPreviewIds, this.m_WithPseudoTranslation, this.m_PreviewLanguage);
        Cursor.Current = current;
        this.m_OberonPreviewBrowser.Navigate(urlString);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    public override void Preview(bool WithPseudoTranslation, string PreviewLanguage)
    {
      this.m_WithPseudoTranslation = WithPseudoTranslation;
      this.m_PreviewLanguage = PreviewLanguage;
      try
      {
        if (this.m_TabWindow.SelectedTab != this.m_OberonPreviewTab)
          this.m_TabWindow.SelectedTab = this.m_OberonPreviewTab;
        else
          this.RefreshPreviewTab();
      }
      finally
      {
        this.m_WithPseudoTranslation = false;
      }
    }

    internal void OnDesignerControlSelectionChange(object sender, IModelObject model)
    {
      this.SelectObject(model);
      this.RaiseSelectionChanged(sender as IDesignerControl, model);
    }

    protected void OnFloorplanDesignerClick(object sender, EventArgs e)
    {
      this.RaiseSelectionChanged((IDesignerControl) this, this.Model);
    }

    internal void OnSelectionChangedEventHandler(object sender, SelectionEventArgs e)
    {
      this.OnDesignerControlSelectionChange(sender, e.SelectedObject);
    }

    private void rootGridControl_LayoutUpdated(object sender, EventArgs e)
    {
      this.m_ElementHost.Invalidate(true);
    }
  }
}
