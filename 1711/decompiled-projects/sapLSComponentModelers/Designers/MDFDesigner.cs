﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.MDFDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class MDFDesigner : ComponentDesigner
  {
    private static readonly SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox m_ComponentToolbox = ToolboxUtilities.DeserialzeToolboxComponent(Resource.ModelDialogToolbox);
    private const string CONTEXTMENU_ADD_STEP = "AddStep";
    private const string CLOSEIMAGE = "sapLSUISkins;component/skin.nova/Images/Windows/Popup/PopupWinCloseStd.png,18,18";
    private const string RESOURCE_BACKGROUND = "#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0";
    private VisualIdentificationRegion m_VisualIDR;
    private VisualContextualNavigation m_VisualCNR;
    private VisualLayout m_LayoutControl;

    public override SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox Toolbox
    {
      get
      {
        return MDFDesigner.m_ComponentToolbox;
      }
    }

    internal ModalDialogFloorplan Component
    {
      get
      {
        return this.m_ModelObject as ModalDialogFloorplan;
      }
    }

    public MDFDesigner(IModelObject rootModel)
      : base(rootModel, true, false)
    {
      this.AllowDrop = true;
    }

    public override void LoadView()
    {
      base.LoadView();
      this.AddIdentificationRegion();
      this.AddContentRegion();
      this.AddContextualNavigationRegion();
      this.LoadLinkedControlRegion();
    }

    private void AddContentRegion()
    {
      if (this.m_LayoutControl != null)
        this.m_LayoutControl.Terminate();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.PlaceElement(this.Grid, (UIElement) new VisualView((IModelObject) this.Component.View), 1, 0, 1, 2);
    }

    private void AddIdentificationRegion()
    {
      if (this.m_VisualIDR != null)
        this.m_VisualIDR.Terminate();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      this.m_VisualIDR = new VisualIdentificationRegion((IModelObject) this.Component.IdentificationArea);
      this.m_VisualIDR.HorizontalAlignment = HorizontalAlignment.Left;
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_VisualIDR, 0, 0);
      DockPanel dockPanel = new DockPanel();
      dockPanel.Background = (Brush) ResourceUtil.GetLinearGradientBrush("#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0");
      Image image = new Image();
      ResourceUtil.LoadImage(image, "sapLSUISkins;component/skin.nova/Images/Windows/Popup/PopupWinCloseStd.png,18,18");
      dockPanel.Children.Add((UIElement) image);
      image.HorizontalAlignment = HorizontalAlignment.Right;
      GridUtil.PlaceElement(this.Grid, (UIElement) dockPanel, 0, 1);
    }

    private void AddContextualNavigationRegion()
    {
      if (this.m_VisualCNR != null)
        this.m_VisualCNR.Terminate();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.m_VisualCNR = new VisualContextualNavigation((IModelObject) this.Component.ContextNavigationPanel);
      this.m_VisualCNR.HorizontalAlignment = HorizontalAlignment.Right;
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_VisualCNR, 2, 0, 1, 2);
    }
  }
}
