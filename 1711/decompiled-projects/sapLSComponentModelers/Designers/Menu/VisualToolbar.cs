﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu.VisualToolbar
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Menu;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu
{
  public class VisualToolbar : BaseSelectableControl
  {
    private const string SEPARATOR_IMAGE = "sapLSUISkins;component/skin.nova/Images/Bars/ToolBar/toolbar_divider.png,2,15";
    private const string ACTIONMENU_IMAGE = "sapLSUISkins;component/dt.images/PNG/Menu.png,16,16";
    private const string QCMENU_IMAGE = "sapLSUISkins;component/dt.images/PNG/AddIcon.png,20,20";
    private const string ADDCOMMAND = "Add";
    private const string LAUNCH_TOOLBAR_EDITOR = "Launch Toolbar Editor";
    private Grid itemGrpGrid;
    private WrapPanel wrapPanel;
    private bool isIntegratedToolbar;
    private bool isPlaceToolbarOnHeader;
    private bool isComponentToolbar;
    private System.Windows.Controls.MenuItem addMenuItem;
    private System.Windows.Controls.MenuItem actionGrpMenuItem;
    private System.Windows.Controls.MenuItem standardGrpMenuItem;
    private System.Windows.Controls.MenuItem relatedActionGrpMenuItem;
    private System.Windows.Controls.MenuItem removeDeleteGrpMenuItem;
    private System.Windows.Controls.MenuItem hierarchicalMtnGrpMenuItem;
    private System.Windows.Controls.MenuItem appSpecificGrpMenuItem;

    internal Toolbar Toolbar
    {
      get
      {
        return this.m_ModelObject as Toolbar;
      }
      set
      {
        this.m_ModelObject = (IModelObject) value;
      }
    }

    public VisualToolbar(IModelObject toolbarRegion)
      : base(toolbarRegion, false, false)
    {
      this.wrapPanel = new WrapPanel();
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.Padding = new Thickness(1.0);
        this.Margin = new Thickness(1.0);
        if (this.Toolbar is ListPaneToolbar)
        {
          AdvancedListPaneVariant parent = this.Toolbar.Parent as AdvancedListPaneVariant;
          if (parent != null && parent.Parent != null && (parent.Parent as AdvancedListPane).UseIntegratedToolbar)
            this.isIntegratedToolbar = true;
        }
        else if (this.Toolbar is FormPaneToolbar)
        {
          FormPane parent = this.Toolbar.Parent as FormPane;
          if (parent != null && parent.PlaceToolbarOnHeader)
            this.isPlaceToolbarOnHeader = true;
        }
        else if (this.Toolbar is ComponentToolbar)
          this.isComponentToolbar = true;
        if (this.isIntegratedToolbar || this.isPlaceToolbarOnHeader)
          this.Grid.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
        else
          this.Grid.Background = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        this.itemGrpGrid = new Grid();
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        if (this.isIntegratedToolbar || this.isPlaceToolbarOnHeader)
        {
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        }
        else
        {
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        }
        this.wrapPanel.ClipToBounds = true;
        this.wrapPanel.Children.Clear();
        if (this.Toolbar != null && this.Toolbar.ButtonGroups != null && this.Toolbar.ButtonGroups.Count > 0)
        {
          int count = this.Toolbar.ButtonGroups.Count;
          int index = 1;
          foreach (ButtonGroup buttonGroup in this.Toolbar.ButtonGroups)
          {
            if (buttonGroup != null && buttonGroup.Buttons.Count > 0)
              this.wrapPanel.Children.Add((UIElement) new VisualButtonGroup(buttonGroup));
            if (count != index && this.Toolbar.ButtonGroups[index] != null && (this.Toolbar.ButtonGroups[index].Buttons.Count > 0 && this.wrapPanel.Children.Count > 0))
              this.LoadToolbarSeperatorImage();
            ++index;
          }
        }
        if (this.isIntegratedToolbar || this.isPlaceToolbarOnHeader)
        {
          this.wrapPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
          GridUtil.PlaceElement(this.Grid, (UIElement) this.wrapPanel, 0, 1);
        }
        else
          GridUtil.PlaceElement(this.Grid, (UIElement) this.wrapPanel, 0, 0);
        if (this.Toolbar != null && this.Toolbar.ItemGroups != null && this.Toolbar.ItemGroups.Count > 0)
        {
          int count = this.Toolbar.ItemGroups.Count;
          List<ItemGroup> leftAlignedItmGrps = new List<ItemGroup>();
          List<ItemGroup> rightAlignedItmGrps = new List<ItemGroup>();
          List<ItemGroup> centerAlignedItmGrps = new List<ItemGroup>();
          List<ItemGroup> stretchAlignedItmGrps = new List<ItemGroup>();
          if (count > 0 && !(this.Toolbar.Parent is ThingHeaderRegion))
            this.LoadToolbarSeperatorImage();
          foreach (ItemGroup itemGroup in this.Toolbar.ItemGroups)
          {
            if (itemGroup != null && itemGroup.Alignment == HorizontalAlignmentType.left)
              leftAlignedItmGrps.Add(itemGroup);
            else if (itemGroup != null && itemGroup.Alignment == HorizontalAlignmentType.right)
              rightAlignedItmGrps.Add(itemGroup);
            else if (itemGroup != null && itemGroup.Alignment == HorizontalAlignmentType.center)
              centerAlignedItmGrps.Add(itemGroup);
            else if (itemGroup != null && itemGroup.Alignment == HorizontalAlignmentType.stretch)
              stretchAlignedItmGrps.Add(itemGroup);
          }
          if (this.isIntegratedToolbar || this.isPlaceToolbarOnHeader)
          {
            foreach (IModelObject modelObject in rightAlignedItmGrps)
              this.wrapPanel.Children.Add((UIElement) new VisualItemGroup(modelObject, (BaseVisualControl) this));
            this.AddAllignedItemGroups(leftAlignedItmGrps, rightAlignedItmGrps, centerAlignedItmGrps, stretchAlignedItmGrps);
          }
          else
            this.AddAllignedItemGroups(leftAlignedItmGrps, rightAlignedItmGrps, centerAlignedItmGrps, stretchAlignedItmGrps);
        }
        if ((this.isIntegratedToolbar || this.isPlaceToolbarOnHeader) && this.Toolbar.ActionMenuButton != null)
          this.LoadActionMenuImage();
        if (!this.isComponentToolbar || this.Toolbar.QCMenuButton == null)
          return;
        this.LoadQCMenuButtonImage();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Toolbar initialization failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Toolbar";
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
    }

    public void AddContextMenuItems(IContextMenuContainer menuContainer)
    {
      this.addMenuItem = menuContainer.AddContextMenuItemWrapper("Add");
      this.AddButtonsInContextMenu(this.Toolbar, menuContainer);
      menuContainer.AddContextMenuItemWrapper("Launch Toolbar Editor");
    }

    public void HandleContextMenuItemClickWrapper(System.Windows.Controls.MenuItem source)
    {
      MenuButtonInfo menuButtonInfo = new MenuButtonInfo();
      if (source.Header.Equals((object) "Launch Toolbar Editor") && this.Toolbar != null)
      {
        this.LaunchToolbarEditor();
      }
      else
      {
        if (source.Parent is System.Windows.Controls.MenuItem)
        {
          switch ((source.Parent as System.Windows.Controls.MenuItem).Header.ToString())
          {
            case "ActionButtons":
              menuButtonInfo = this.Toolbar.GetMenuButtonInfoForActionButtons(source.Header.ToString());
              break;
            case "StandardButtons":
              menuButtonInfo = this.Toolbar.GetMenuButtonInfoForStandardButtons(source.Header.ToString());
              break;
            case "RelatedActionButtons":
              menuButtonInfo = this.Toolbar.GetMenuButtonInfoForRelatedActions(source.Header.ToString());
              break;
            case "RemoveDeleteButtons":
              menuButtonInfo = this.Toolbar.GetMenuButtonInfoForRemoveDelete(source.Header.ToString());
              break;
            case "HierarchicalMaintenanceButtons":
              menuButtonInfo = this.Toolbar.GetMenuButtonInfoForHierarchicalMaintenance(source.Header.ToString());
              break;
            case "ApplicationSpecificButtons":
              menuButtonInfo = this.Toolbar.GetMenubuttonInfoForApplicationSpecific(source.Header.ToString());
              break;
            case "ItemGroups":
            case "Item":
              menuButtonInfo = this.Toolbar.GetMenuButtonInfoForItemGroup(source.Header.ToString());
              break;
          }
        }
        if (source.Header.ToString() != "ItemGroups" && source.Header.ToString() != "Item")
          this.AddButton(menuButtonInfo);
        else
          this.AddItemGroupsInToolbar(menuButtonInfo, source.Header.ToString());
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        if (e.RemovedModel != null)
        {
          if (!(e.RemovedModel is ItemGroup) && !(e.RemovedModel is ControlUsage))
          {
            List<VisualButtonGroup> visualButtonGroupList = new List<VisualButtonGroup>();
            foreach (UIElement child in this.wrapPanel.Children)
            {
              if (child is VisualButtonGroup)
                visualButtonGroupList.Add(child as VisualButtonGroup);
            }
            foreach (BaseVisualControl baseVisualControl in visualButtonGroupList)
            {
              WrapPanel child1 = baseVisualControl.Grid.Children[0] as WrapPanel;
              foreach (UIElement child2 in child1.Children)
              {
                if (e.RemovedModel.Id == (child2 as VisualMenuButton).ToolbarButton.Id)
                {
                  child1.Children.Remove(child2);
                  break;
                }
              }
            }
          }
          else
            this.ReloadView();
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
      base.OnModelObjectRemoved(sender, e);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelAddedEvent.", ex));
      }
    }

    private void AddButtonsInContextMenu(Toolbar toolbar, IContextMenuContainer menuContainer)
    {
      ToolbarValidationManager instance = ToolbarValidationManager.GetInstance();
      ToolbarInfo toolbarInfo = new ToolbarInfo();
      ToolbarInfo detailsForContextMenu = instance.GetButtonDetailsForContextMenu(toolbar);
      Utilities.GetFloorplanObject((IModelObject) toolbar);
      if (detailsForContextMenu.ActionButtons.Count > 0)
      {
        this.actionGrpMenuItem = menuContainer.AddContextMenuItemWrapper("ActionButtons", this.addMenuItem);
        foreach (MenuButtonInfo actionButton in detailsForContextMenu.ActionButtons)
        {
          if (actionButton.HasSubMenu)
            menuContainer.AddContextMenuItemWrapper(actionButton.Name + " Menu", this.actionGrpMenuItem);
          else
            menuContainer.AddContextMenuItemWrapper(actionButton.Name, this.actionGrpMenuItem);
        }
      }
      this.standardGrpMenuItem = (System.Windows.Controls.MenuItem) null;
      if (detailsForContextMenu.StandardButtons.Count > 0)
      {
        this.standardGrpMenuItem = menuContainer.AddContextMenuItemWrapper("StandardButtons", this.addMenuItem);
        foreach (MenuButtonInfo standardButton in detailsForContextMenu.StandardButtons)
        {
          if (standardButton.HasSubMenu)
            menuContainer.AddContextMenuItemWrapper(standardButton.Name + " Menu", this.standardGrpMenuItem);
          else
            menuContainer.AddContextMenuItemWrapper(standardButton.Name, this.standardGrpMenuItem);
        }
      }
      this.relatedActionGrpMenuItem = (System.Windows.Controls.MenuItem) null;
      if (detailsForContextMenu.RelatedActionButtons.Count > 0)
      {
        this.relatedActionGrpMenuItem = menuContainer.AddContextMenuItemWrapper("RelatedActionButtons", this.addMenuItem);
        foreach (MenuButtonInfo relatedActionButton in detailsForContextMenu.RelatedActionButtons)
        {
          if (relatedActionButton.HasSubMenu)
          {
            if (!relatedActionButton.Name.EndsWith("Menu"))
              menuContainer.AddContextMenuItemWrapper(relatedActionButton.Name + " Menu", this.relatedActionGrpMenuItem);
            else
              menuContainer.AddContextMenuItemWrapper(relatedActionButton.Name, this.relatedActionGrpMenuItem);
          }
          else
            menuContainer.AddContextMenuItemWrapper(relatedActionButton.Name, this.relatedActionGrpMenuItem);
        }
      }
      this.removeDeleteGrpMenuItem = (System.Windows.Controls.MenuItem) null;
      if (detailsForContextMenu.RemoveDeleteButtons.Count > 0)
      {
        this.removeDeleteGrpMenuItem = menuContainer.AddContextMenuItemWrapper("RemoveDeleteButtons", this.addMenuItem);
        foreach (MenuButtonInfo removeDeleteButton in detailsForContextMenu.RemoveDeleteButtons)
        {
          if (removeDeleteButton.HasSubMenu)
            menuContainer.AddContextMenuItemWrapper(removeDeleteButton.Name + " Menu", this.removeDeleteGrpMenuItem);
          else
            menuContainer.AddContextMenuItemWrapper(removeDeleteButton.Name, this.removeDeleteGrpMenuItem);
        }
      }
      this.hierarchicalMtnGrpMenuItem = (System.Windows.Controls.MenuItem) null;
      if (detailsForContextMenu.HierarchicalMaintenanceButtons.Count > 0)
      {
        this.hierarchicalMtnGrpMenuItem = menuContainer.AddContextMenuItemWrapper("HierarchicalMaintenanceButtons", this.addMenuItem);
        foreach (MenuButtonInfo maintenanceButton in detailsForContextMenu.HierarchicalMaintenanceButtons)
        {
          if (maintenanceButton.HasSubMenu)
            menuContainer.AddContextMenuItemWrapper(maintenanceButton.Name + " Menu", this.hierarchicalMtnGrpMenuItem);
          else
            menuContainer.AddContextMenuItemWrapper(maintenanceButton.Name, this.hierarchicalMtnGrpMenuItem);
        }
      }
      this.appSpecificGrpMenuItem = (System.Windows.Controls.MenuItem) null;
      if (detailsForContextMenu.ApplicationSpecificButtons.Count <= 0)
        return;
      this.appSpecificGrpMenuItem = menuContainer.AddContextMenuItemWrapper("ApplicationSpecificButtons", this.addMenuItem);
      foreach (MenuButtonInfo applicationSpecificButton in detailsForContextMenu.ApplicationSpecificButtons)
      {
        if (applicationSpecificButton.HasSubMenu)
          menuContainer.AddContextMenuItemWrapper(applicationSpecificButton.Name + " Menu", this.appSpecificGrpMenuItem);
        else
          menuContainer.AddContextMenuItemWrapper(applicationSpecificButton.Name, this.appSpecificGrpMenuItem);
      }
    }

    private void AddButton(MenuButtonInfo menuBtnInfo)
    {
      if (menuBtnInfo == null)
        return;
      this.Toolbar.AddMenuButton(menuBtnInfo);
    }

    private void AddItemGroupsInToolbar(MenuButtonInfo itemInfo, string parentMenu)
    {
      if (itemInfo == null)
        return;
      if (parentMenu == "ItemGroups")
        this.Toolbar.AddItemGroup();
      else
        this.Toolbar.AddItem(this.Toolbar.ItemGroups[0]);
    }

    private void AddButtons(ButtonGroup btnGroup)
    {
      foreach (IModelObject button in btnGroup.Buttons)
        this.wrapPanel.Children.Add((UIElement) new VisualMenuButton(button));
    }

    private void AddAllignedItemGroups(List<ItemGroup> leftAlignedItmGrps, List<ItemGroup> rightAlignedItmGrps, List<ItemGroup> centerAlignedItmGrps, List<ItemGroup> stretchAlignedItmGrps)
    {
      GridUtil.AddRowDef(this.itemGrpGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.itemGrpGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.itemGrpGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.itemGrpGrid, 1.0, GridUnitType.Auto);
      WrapPanel wrapPanel1 = new WrapPanel();
      WrapPanel wrapPanel2 = new WrapPanel();
      WrapPanel wrapPanel3 = new WrapPanel();
      WrapPanel wrapPanel4 = new WrapPanel();
      foreach (IModelObject leftAlignedItmGrp in leftAlignedItmGrps)
      {
        VisualItemGroup visualItemGroup = new VisualItemGroup(leftAlignedItmGrp, (BaseVisualControl) this);
        wrapPanel1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
        wrapPanel1.Children.Add((UIElement) visualItemGroup);
      }
      GridUtil.PlaceElement(this.itemGrpGrid, (UIElement) wrapPanel1, 0, 0);
      if (!this.isIntegratedToolbar || this.isPlaceToolbarOnHeader)
      {
        foreach (IModelObject rightAlignedItmGrp in rightAlignedItmGrps)
        {
          VisualItemGroup visualItemGroup = new VisualItemGroup(rightAlignedItmGrp, (BaseVisualControl) this);
          wrapPanel2.Children.Add((UIElement) visualItemGroup);
          wrapPanel2.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
        }
        GridUtil.PlaceElement(this.itemGrpGrid, (UIElement) wrapPanel2, 0, 2);
      }
      foreach (IModelObject centerAlignedItmGrp in centerAlignedItmGrps)
      {
        VisualItemGroup visualItemGroup = new VisualItemGroup(centerAlignedItmGrp, (BaseVisualControl) this);
        wrapPanel3.Children.Add((UIElement) visualItemGroup);
        wrapPanel3.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      }
      GridUtil.PlaceElement(this.itemGrpGrid, (UIElement) wrapPanel3, 0, 1);
      foreach (IModelObject stretchAlignedItmGrp in stretchAlignedItmGrps)
      {
        VisualItemGroup visualItemGroup = new VisualItemGroup(stretchAlignedItmGrp, (BaseVisualControl) this);
        wrapPanel4.Children.Add((UIElement) visualItemGroup);
        wrapPanel4.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      }
      GridUtil.PlaceElement(this.itemGrpGrid, (UIElement) wrapPanel4, 0, 1);
      if (this.isIntegratedToolbar || this.isPlaceToolbarOnHeader)
        GridUtil.PlaceElement(this.Grid, (UIElement) this.itemGrpGrid, 0, 0);
      else
        GridUtil.PlaceElement(this.Grid, (UIElement) this.itemGrpGrid, 0, 1);
    }

    protected void LoadToolbarSeperatorImage()
    {
      System.Windows.Controls.Image image = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image, "sapLSUISkins;component/skin.nova/Images/Bars/ToolBar/toolbar_divider.png,2,15");
      image.Stretch = Stretch.Fill;
      image.Margin = new Thickness(2.0);
      image.Height = 20.0;
      this.wrapPanel.Children.Add((UIElement) image);
    }

    private void LoadActionMenuImage()
    {
      System.Windows.Controls.Image image = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image, "sapLSUISkins;component/dt.images/PNG/Menu.png,16,16");
      image.Stretch = Stretch.Fill;
      image.Margin = new Thickness(2.0);
      image.ToolTip = (object) "ActionMenuButton";
      this.wrapPanel.Children.Add((UIElement) image);
    }

    private void LoadQCMenuButtonImage()
    {
      System.Windows.Controls.Image image = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image, "sapLSUISkins;component/dt.images/PNG/AddIcon.png,20,20");
      image.Stretch = Stretch.Fill;
      image.Margin = new Thickness(2.0);
      image.ToolTip = (object) "QCMenuButton";
      this.wrapPanel.Children.Add((UIElement) image);
    }

    private void LaunchToolbarEditor()
    {
      ToolbarEditorFrame toolbarEditorFrame = new ToolbarEditorFrame(this.Toolbar, this.Toolbar.Parent);
      BaseDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
      toolbarEditorFrame.Owner = parentDesigner.ParentForm.Parent.Parent as Form;
      toolbarEditorFrame.FormBorderStyle = FormBorderStyle.Sizable;
      if (toolbarEditorFrame.ShowDialog((IWin32Window) toolbarEditorFrame.Owner) != DialogResult.OK)
        return;
      if (this.Toolbar is ListPaneToolbar)
        (this.Toolbar.Parent as AdvancedListPaneVariant).Toolbar = toolbarEditorFrame.UpdatedToolbar as ListPaneToolbar;
      else if (this.Toolbar is FormPaneToolbar)
        (this.Toolbar.Parent as FormPane).Toolbar = toolbarEditorFrame.UpdatedToolbar as FormPaneToolbar;
      else if (this.Toolbar is CalendarPaneToolbar)
      {
        if (!(this.Toolbar.Parent is CalendarPane))
          return;
        (this.Toolbar.Parent as CalendarPane).ToolBar = toolbarEditorFrame.UpdatedToolbar as CalendarPaneToolbar;
      }
      else if (this.Toolbar is AvailabilityCalendarPaneToolbar)
        (this.Toolbar.Parent as AvailabilityCalendarPane).Toolbar = toolbarEditorFrame.UpdatedToolbar as AvailabilityCalendarPaneToolbar;
      else if (this.Toolbar is HierarchicalGraphPaneToolbar)
        (this.Toolbar.Parent as HierarchicalGraphPane).HierarchicalGraphToolbar = toolbarEditorFrame.UpdatedToolbar as HierarchicalGraphPaneToolbar;
      else if (this.Toolbar is GanttChartToolbar)
        (this.Toolbar.Parent as GanttChartPane).GanttChartPaneToolbar = toolbarEditorFrame.UpdatedToolbar as GanttChartToolbar;
      else if (this.Toolbar is CustomPaneToolbar)
        (this.Toolbar.Parent as CustomPane).Toolbar = toolbarEditorFrame.UpdatedToolbar as CustomPaneToolbar;
      else if (this.Toolbar is ModalDialogToolbar)
        this.Toolbar = (Toolbar) (toolbarEditorFrame.UpdatedToolbar as ModalDialogToolbar);
      else if (this.Toolbar is ThingHeaderToolBar)
        (this.Toolbar.Parent as ThingHeaderRegion).Toolbar = toolbarEditorFrame.UpdatedToolbar as ThingHeaderToolBar;
      else
        (this.Toolbar.Parent as ContextualNavigation).Functionbar = (Toolbar) (toolbarEditorFrame.UpdatedToolbar as ComponentToolbar);
    }
  }
}
