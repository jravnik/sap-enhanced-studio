﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu.VisualMenuButton
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Oberon.Controller;
using SAP.BYD.LS.UIDesigner.UICore.Dialogs;
using SAP.BYD.LS.UIDesigner.UICore.OBNNavigationArea;
using System;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu
{
  public class VisualMenuButton : BaseSelectableControl
  {
    private const string DEFAULT_BUTTON_NAME = "My Button";
    private const string CONTEXTMENU_MOVE_AS_FACTION = "Move as FAction";
    private const string CONTEXTMENU_ADD_OBN_USING_WIZARD = "Add OBN using Wizard";
    private const string CONTEXTMENU_EDIT_OBN_USING_WIZARD = "Edit OBN using Wizard";
    private Navigation buttonMenuDefinition;
    private SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button menuBtn;
    private System.Windows.Controls.Button toolbarButton;

    internal Toolbar Toolbar
    {
      get
      {
        return this.ToolbarButton.Parent.Parent as Toolbar;
      }
    }

    internal SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button ToolbarButton
    {
      get
      {
        return this.m_ModelObject as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button;
      }
    }

    public VisualMenuButton(IModelObject menuButton)
      : base(menuButton)
    {
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.BorderCornerRadius = new CornerRadius(2.0);
        this.Margin = new Thickness(2.0, 3.0, 2.0, 3.0);
        this.Grid.Background = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        GridUtil.AddRowDef(this.Grid, 21.0, GridUnitType.Pixel);
        this.AllowDrop = true;
        this.InitializeComponentEx();
        this.toolbarButton.Content = this.ToolbarButton.Text != null ? (object) this.ToolbarButton.Text.Text : (object) "My Button";
        this.toolbarButton.IsEnabled = Converter.ToBoolean((DependentProperty) this.ToolbarButton.Enabled, true);
        this.toolbarButton.SetValue(FrameworkElement.VerticalAlignmentProperty, (object) VerticalAlignment.Center);
        this.toolbarButton.SetValue(FrameworkElement.HorizontalAlignmentProperty, (object) System.Windows.HorizontalAlignment.Center);
        this.toolbarButton.Padding = new Thickness(3.0, 0.0, 3.0, 0.0);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.toolbarButton, 0, 0);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Button UI initialization failed.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button.TextProperty)
        {
          if (this.ToolbarButton.Text != null)
            this.toolbarButton.Content = (object) this.ToolbarButton.Text.Text;
          else
            this.toolbarButton.Content = (object) string.Empty;
        }
        else if (e.PropertyName == "Enabled")
          this.toolbarButton.IsEnabled = Converter.ToBoolean((DependentProperty) this.ToolbarButton.Enabled, true);
        else if (e.PropertyName == SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button.SubMenuProperty)
        {
          if (this.ToolbarButton.Menu == null || !this.ToolbarButton.HasMenu || (this.ToolbarButton.SubMenu == null || this.ToolbarButton.SubMenu.Count <= 0))
            return;
          (this.toolbarButton as VisualButton).SetButtonStyle("style_menu");
        }
        else
        {
          if (!(e.PropertyName == "StyleProperty") || this.ToolbarButton.HasMenu)
            return;
          VisualButton.ButtonStyles buttonStyle = VisualButton.ButtonStyles.Standard;
          if (this.ToolbarButton != null)
          {
            switch (this.ToolbarButton.Style)
            {
              case SAP.BYD.LS.UI.Core.Model.ButtonStyles.Emphasized:
                buttonStyle = VisualButton.ButtonStyles.Emphasized;
                break;
              case SAP.BYD.LS.UI.Core.Model.ButtonStyles.Previous:
                buttonStyle = VisualButton.ButtonStyles.Previous;
                break;
              case SAP.BYD.LS.UI.Core.Model.ButtonStyles.Next:
                buttonStyle = VisualButton.ButtonStyles.Next;
                break;
              case SAP.BYD.LS.UI.Core.Model.ButtonStyles.Action:
                buttonStyle = VisualButton.ButtonStyles.Action;
                break;
              default:
                buttonStyle = VisualButton.ButtonStyles.Standard;
                break;
            }
          }
          (this.toolbarButton as VisualButton).SetStyle(buttonStyle);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update the property changes in the model.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
      if (this.ToolbarButton.OnClick != null)
      {
        foreach (UXEventHandlerOperation operation in this.ToolbarButton.OnClick.Operations)
        {
          if (!(operation is EventHandlerOperationTypeFireOutport))
          {
            this.AddSeperatorToContextMenu();
            this.AddContextMenuItem("Add OBN using Wizard");
          }
          else
            break;
        }
      }
      else
      {
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Add OBN using Wizard");
      }
      if (this.ToolbarButton.OnClick != null)
      {
        foreach (UXEventHandlerOperation operation in this.ToolbarButton.OnClick.Operations)
        {
          if (operation is EventHandlerOperationTypeFireOutport)
          {
            this.AddSeperatorToContextMenu();
            this.AddContextMenuItem("Edit OBN using Wizard");
          }
        }
      }
      INamedModelObject parent = this.ToolbarButton.Parent as INamedModelObject;
      string str = parent != null ? parent.Name : string.Empty;
      if (this.Toolbar == null)
        return;
      IFloorplan floorplanObject = Utilities.GetFloorplanObject((IModelObject) this.Toolbar);
      if (this.Toolbar.Parent is BasePane || floorplanObject == null || !(str == "ApplicationSpecificButtons") || floorplanObject.Type != FloorplanType.QA && floorplanObject.Type != FloorplanType.OIF && floorplanObject.Type != FloorplanType.GAF)
        return;
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Move as FAction");
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      try
      {
        INamedModelObject parent = this.ToolbarButton.Parent as INamedModelObject;
        string str = parent != null ? parent.Name : string.Empty;
        string buttonName = this.ToolbarButton.Text != null ? this.ToolbarButton.Text.Text : string.Empty;
        if (Convert.ToString(source.Header).Equals("Delete"))
        {
          if (this.ToolbarButton.IsMandatory)
          {
            int num1 = (int) DisplayMessage.Show("The Mandatory Button cannot be deleted.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
          }
          else
          {
            if (buttonName != string.Empty)
            {
              switch (str)
              {
                case "ActionButtons":
                  this.menuBtn = this.Toolbar.GetMenuButtonForActionButtons(buttonName);
                  break;
                case "StandardButtons":
                  this.menuBtn = this.Toolbar.GetMenuButtonForStandardButtons(buttonName);
                  break;
                case "RelatedActionButtons":
                  this.menuBtn = this.Toolbar.GetMenuButtonForRelatedActions(buttonName);
                  break;
                case "RemoveDeleteButtons":
                  this.menuBtn = this.Toolbar.GetMenuButtonForRemoveDelete(buttonName);
                  break;
                case "HierarchicalMaintenanceButtons":
                  this.menuBtn = this.Toolbar.GetMenuButtonForHierarchicalMaintenance(buttonName);
                  break;
                case "ApplicationSpecificButtons":
                  this.menuBtn = this.Toolbar.GetMenuButtonForApplicationSpecific(buttonName);
                  break;
                default:
                  if (this.ToolbarButton.Parent is ButtonGroup)
                  {
                    this.Toolbar.RemoveButtonFromButtonGroup(this.ToolbarButton.Parent as ButtonGroup, this.ToolbarButton);
                    break;
                  }
                  if (this.ToolbarButton.Parent is ContextualNavigation)
                  {
                    (this.ToolbarButton.Parent as ContextualNavigation).RemoveModelObject((IModelObject) this.ToolbarButton);
                    this.menuBtn = this.ToolbarButton;
                    return;
                  }
                  break;
              }
              if (this.menuBtn == null)
                this.menuBtn = this.ToolbarButton;
            }
            else
              this.menuBtn = this.ToolbarButton;
            this.RemoveButton(this.menuBtn);
          }
        }
        else if (source.Header == (object) "Move as FAction")
        {
          if (this.Toolbar.IsButtonPresentInActionBtnGrp(this.ToolbarButton))
          {
            int num2 = (int) DisplayMessage.Show("The button already exists in the Action Button Group", MessageBoxButtons.OK, MessageBoxIcon.Hand);
          }
          else
          {
            SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button movedButton = this.ToolbarButton.Clone(CloningMode.EditorMode) as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button;
            this.RemoveButton(this.ToolbarButton);
            this.Toolbar.AddMovedButtonToActionGroup(movedButton);
          }
        }
        else if (source.Header == (object) "Add OBN using Wizard")
        {
          WizardDialog wizardDialog = new WizardDialog(OBNWizardStepFactory.Instance.GetWizardSteps(Utilities.GetFloorplanObject((IModelObject) this.ToolbarButton), (IOBN) null));
          int num3 = (int) wizardDialog.ShowDialog();
          IEventHandler wizardResult = wizardDialog.WizardResult as IEventHandler;
          if (wizardResult == null)
            return;
          this.ToolbarButton.OnClick = wizardResult;
        }
        else
        {
          if (source.Header != (object) "Edit OBN using Wizard")
            return;
          IFloorplan floorplanObject = Utilities.GetFloorplanObject((IModelObject) this.ToolbarButton);
          IEventHandler onClick = this.ToolbarButton.OnClick;
          IOutPortType outPortType = (IOutPortType) null;
          foreach (UXEventHandlerOperation operation in onClick.Operations)
          {
            if (operation is EventHandlerOperationTypeFireOutport)
            {
              foreach (IOutPortType outPort in floorplanObject.ControllerInterface.OutPorts)
              {
                if (outPort.Name == (operation as EventHandlerOperationTypeFireOutport).OutPlug)
                {
                  outPortType = outPort;
                  break;
                }
              }
            }
          }
          foreach (OberonOBN oberonObn in floorplanObject.ControllerNavigation.OBN)
          {
            if (oberonObn.OutPlug.Id == outPortType.Id)
            {
              WizardDialog wizardDialog = new WizardDialog(OBNWizardStepFactory.Instance.GetWizardSteps(floorplanObject, (IOBN) oberonObn));
              int num3 = (int) wizardDialog.ShowDialog();
              if (wizardDialog.WizardResult == null)
                break;
              this.ToolbarButton.OnClick = wizardDialog.WizardResult as IEventHandler;
              break;
            }
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Button delete Operation failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Toolbar Button";
      }
    }

    private void RemoveButton(SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button button)
    {
      if (button != null)
        this.Toolbar.RemoveButton(button);
      else if (this.ToolbarButton != null && this.ToolbarButton.Text.Text == "View All")
      {
        if (!(this.ToolbarButton.Parent is QuickActivityContextualNavigationRegion))
          return;
        (this.ToolbarButton.Parent as QuickActivityContextualNavigationRegion).RemoveContent(this.ToolbarButton);
      }
      else
      {
        if (this.ToolbarButton == null || !(this.ToolbarButton.Text.Text == "Basic View") || !(this.ToolbarButton.Parent is ViewSwitchContextualNavigationRegion))
          return;
        (this.ToolbarButton.Parent as ViewSwitchContextualNavigationRegion).RemoveContent(this.ToolbarButton);
      }
    }

    private void InitializeComponentEx()
    {
      VisualButton.ButtonStyles buttonStyle = VisualButton.ButtonStyles.Standard;
      if (this.ToolbarButton != null)
      {
        if (this.ToolbarButton.Menu != null && this.ToolbarButton.HasMenu)
        {
          this.buttonMenuDefinition = this.ToolbarButton.Menu;
          buttonStyle = VisualButton.ButtonStyles.Menu;
        }
        else
        {
          switch (this.ToolbarButton.Style)
          {
            case SAP.BYD.LS.UI.Core.Model.ButtonStyles.Emphasized:
              buttonStyle = VisualButton.ButtonStyles.Emphasized;
              break;
            case SAP.BYD.LS.UI.Core.Model.ButtonStyles.Previous:
              buttonStyle = VisualButton.ButtonStyles.Previous;
              break;
            case SAP.BYD.LS.UI.Core.Model.ButtonStyles.Next:
              buttonStyle = VisualButton.ButtonStyles.Next;
              break;
            case SAP.BYD.LS.UI.Core.Model.ButtonStyles.Action:
              buttonStyle = VisualButton.ButtonStyles.Action;
              break;
            default:
              buttonStyle = VisualButton.ButtonStyles.Standard;
              break;
          }
        }
      }
      this.toolbarButton = (System.Windows.Controls.Button) new VisualButton(buttonStyle, false);
      this.toolbarButton.AllowDrop = true;
    }
  }
}
