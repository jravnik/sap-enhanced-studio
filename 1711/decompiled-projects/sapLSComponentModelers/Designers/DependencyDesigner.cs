﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.DependencyDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class DependencyDesigner : ComponentDesigner, ISelectionProvider
  {
    private System.Windows.Controls.DataGrid prerequisiteGrid;
    private System.Windows.Controls.DataGrid technicalConflictGrid;
    private System.Windows.Controls.DataGrid soDConflictGrid;
    private SelectionContext m_SelectionContext;
    private System.ComponentModel.IContainer components;

    internal UIDependencyComponent OberonModelObject
    {
      get
      {
        return this.m_ModelObject as UIDependencyComponent;
      }
    }

    public DependencyDesigner(IModelObject rootModel)
      : base(rootModel, true, true)
    {
    }

    public override void LoadView()
    {
      base.LoadView();
      this.AddTitle();
      this.AddContent();
      this.m_TabWindow.TabPages.Remove(this.m_DataModelTab);
      this.m_TabWindow.TabPages.Remove(this.m_ControllerTab);
      this.m_TabWindow.TabPages.Remove(this.m_OberonPreviewTab);
      this.m_SelectionContext = new SelectionContext();
      this.m_SelectionContext.AddSelectionProvider((ISelectionProvider) this);
    }

    private void AddTitle()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      DisplayTextControl displayTextControl = new DisplayTextControl(TextViewStyles.PageHeaderTitle, "UI Dependency");
      displayTextControl.Margin = new Thickness(5.0, 5.0, 5.0, 5.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) displayTextControl, 0, 0);
    }

    private void AddContent()
    {
      try
      {
        System.Windows.Controls.TabControl tabControl = new System.Windows.Controls.TabControl();
        this.prerequisiteGrid = new System.Windows.Controls.DataGrid();
        this.prerequisiteGrid.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
        this.prerequisiteGrid.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
        this.prerequisiteGrid.GridLinesVisibility = DataGridGridLinesVisibility.All;
        this.prerequisiteGrid.SelectionUnit = DataGridSelectionUnit.FullRow;
        this.prerequisiteGrid.SelectionMode = DataGridSelectionMode.Single;
        this.prerequisiteGrid.CanUserAddRows = false;
        this.prerequisiteGrid.CanUserReorderColumns = false;
        this.prerequisiteGrid.CanUserResizeRows = false;
        this.prerequisiteGrid.CanUserSortColumns = false;
        this.prerequisiteGrid.HeadersVisibility = DataGridHeadersVisibility.All;
        this.technicalConflictGrid = new System.Windows.Controls.DataGrid();
        this.technicalConflictGrid.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
        this.technicalConflictGrid.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
        this.technicalConflictGrid.GridLinesVisibility = DataGridGridLinesVisibility.All;
        this.technicalConflictGrid.SelectionUnit = DataGridSelectionUnit.FullRow;
        this.technicalConflictGrid.SelectionMode = DataGridSelectionMode.Single;
        this.technicalConflictGrid.CanUserAddRows = false;
        this.technicalConflictGrid.CanUserReorderColumns = false;
        this.technicalConflictGrid.CanUserResizeRows = false;
        this.technicalConflictGrid.CanUserSortColumns = false;
        this.technicalConflictGrid.HeadersVisibility = DataGridHeadersVisibility.All;
        this.soDConflictGrid = new System.Windows.Controls.DataGrid();
        this.soDConflictGrid.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
        this.soDConflictGrid.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
        this.soDConflictGrid.GridLinesVisibility = DataGridGridLinesVisibility.All;
        this.soDConflictGrid.SelectionUnit = DataGridSelectionUnit.FullRow;
        this.soDConflictGrid.SelectionMode = DataGridSelectionMode.Single;
        this.soDConflictGrid.CanUserAddRows = false;
        this.soDConflictGrid.CanUserReorderColumns = false;
        this.soDConflictGrid.CanUserResizeRows = false;
        this.soDConflictGrid.CanUserSortColumns = false;
        this.soDConflictGrid.HeadersVisibility = DataGridHeadersVisibility.All;
        string xamlText1 = "<DataGridTemplateColumn xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" Width=\"*\" Header=\"Prerequisite WoC View\"><DataGridTemplateColumn.CellTemplate><DataTemplate><Border BorderThickness=\"0\"><TextBlock Text=\"{Binding Path=ObjectID1, Mode=OneWay}\"  HorizontalAlignment=\"Left\" VerticalAlignment=\"Center\"  TextWrapping=\"Wrap\" MinHeight=\"20\"/></Border></DataTemplate></DataGridTemplateColumn.CellTemplate></DataGridTemplateColumn>";
        string xamlText2 = "<DataGridTemplateColumn xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" Width=\"*\" Header=\"Target WoC View\"><DataGridTemplateColumn.CellTemplate><DataTemplate><Border BorderThickness=\"0\"><TextBlock Text=\"{Binding Path=ObjectID2, Mode=OneWay}\"  HorizontalAlignment=\"Left\" VerticalAlignment=\"Center\"  TextWrapping=\"Wrap\" MinHeight=\"20\"/></Border></DataTemplate></DataGridTemplateColumn.CellTemplate></DataGridTemplateColumn>";
        string xamlText3 = "<DataGridTemplateColumn xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" Width=\"*\" Header=\"Technical Conflicting WoC View\"><DataGridTemplateColumn.CellTemplate><DataTemplate><Border BorderThickness=\"0\"><TextBlock Text=\"{Binding Path=ObjectID1, Mode=OneWay}\"  HorizontalAlignment=\"Left\" VerticalAlignment=\"Center\"  TextWrapping=\"Wrap\" MinHeight=\"20\"/></Border></DataTemplate></DataGridTemplateColumn.CellTemplate></DataGridTemplateColumn>";
        string xamlText4 = "<DataGridTemplateColumn xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" Width=\"*\" Header=\"SoD Conflicting WoC View\"><DataGridTemplateColumn.CellTemplate><DataTemplate><Border BorderThickness=\"0\"><TextBlock Text=\"{Binding Path=ObjectID1, Mode=OneWay}\"  HorizontalAlignment=\"Left\" VerticalAlignment=\"Center\"  TextWrapping=\"Wrap\" MinHeight=\"20\"/></Border></DataTemplate></DataGridTemplateColumn.CellTemplate></DataGridTemplateColumn>";
        List<DependencyViewModel> dependencyViewModelList = new List<DependencyViewModel>();
        foreach (UXDependencyType uxDependencyType in this.OberonModelObject.UXDependencyTypes)
        {
          DependencyViewModel dependencyViewModel = new DependencyViewModel()
          {
            ObjectID1 = uxDependencyType.ObjectID1,
            ObjectID2 = uxDependencyType.ObjectID2
          };
          dependencyViewModelList.Add(dependencyViewModel);
        }
        this.prerequisiteGrid.ItemsSource = (IEnumerable) dependencyViewModelList;
        this.prerequisiteGrid.AutoGenerateColumns = false;
        this.prerequisiteGrid.Columns.Add((DataGridColumn) (XamlReader.Parse(xamlText2) as DataGridTemplateColumn));
        this.prerequisiteGrid.Columns.Add((DataGridColumn) (XamlReader.Parse(xamlText1) as DataGridTemplateColumn));
        this.prerequisiteGrid.ColumnWidth = new DataGridLength(1.0, DataGridLengthUnitType.Star);
        TabItem tabItem1 = new TabItem();
        tabItem1.Header = (object) "Prerequisite";
        tabItem1.Content = (object) this.prerequisiteGrid;
        tabControl.Items.Add((object) tabItem1);
        string xamlText5 = "<DataGridTemplateColumn xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" Width=\"*\" Header=\"Reason\"><DataGridTemplateColumn.CellTemplate><DataTemplate><Border BorderThickness=\"0\"><TextBlock Text=\"{Binding Path=Reason, Mode=OneWay}\"  HorizontalAlignment=\"Left\" VerticalAlignment=\"Center\"  TextWrapping=\"Wrap\" MinHeight=\"20\"/></Border></DataTemplate></DataGridTemplateColumn.CellTemplate></DataGridTemplateColumn>";
        List<ConflictViewModel> conflictViewModelList = new List<ConflictViewModel>();
        foreach (UXConflictType uxConflictType in this.OberonModelObject.UXConflictTypes)
        {
          ConflictViewModel conflictViewModel = new ConflictViewModel()
          {
            ObjectID1 = uxConflictType.ObjectID1,
            ObjectID2 = uxConflictType.ObjectID2,
            Reason = uxConflictType.fallbackValue
          };
          conflictViewModelList.Add(conflictViewModel);
        }
        this.technicalConflictGrid.ItemsSource = (IEnumerable) conflictViewModelList;
        this.technicalConflictGrid.AutoGenerateColumns = false;
        this.technicalConflictGrid.Columns.Add((DataGridColumn) (XamlReader.Parse(xamlText2) as DataGridTemplateColumn));
        this.technicalConflictGrid.Columns.Add((DataGridColumn) (XamlReader.Parse(xamlText3) as DataGridTemplateColumn));
        this.technicalConflictGrid.Columns.Add((DataGridColumn) (XamlReader.Parse(xamlText5) as DataGridTemplateColumn));
        this.technicalConflictGrid.ColumnWidth = new DataGridLength(1.0, DataGridLengthUnitType.Star);
        TabItem tabItem2 = new TabItem();
        tabItem2.Header = (object) "Technical Conflict";
        tabItem2.Content = (object) this.technicalConflictGrid;
        tabControl.Items.Add((object) tabItem2);
        string xamlText6 = "<DataGridTemplateColumn xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" Width=\"*\" Header=\"Resolution\"><DataGridTemplateColumn.CellTemplate><DataTemplate><Border BorderThickness=\"0\"><TextBlock Text=\"{Binding Path=Resolution, Mode=OneWay}\"  HorizontalAlignment=\"Left\" VerticalAlignment=\"Center\"  TextWrapping=\"Wrap\" MinHeight=\"20\"/></Border></DataTemplate></DataGridTemplateColumn.CellTemplate></DataGridTemplateColumn>";
        List<SoDConflictViewModel> dconflictViewModelList = new List<SoDConflictViewModel>();
        foreach (UXSoDConflictType uxSoDconflictType in this.OberonModelObject.UXSoDConflictTypes)
        {
          string str1 = uxSoDconflictType.Reason != null ? uxSoDconflictType.Reason.fallbackValue : string.Empty;
          string str2 = uxSoDconflictType.Resolution != null ? uxSoDconflictType.Resolution.fallbackValue : string.Empty;
          SoDConflictViewModel dconflictViewModel = new SoDConflictViewModel()
          {
            ObjectID1 = uxSoDconflictType.ObjectID1,
            ObjectID2 = uxSoDconflictType.ObjectID2,
            Reason = str1,
            Resolution = str2
          };
          dconflictViewModelList.Add(dconflictViewModel);
        }
        this.soDConflictGrid.ItemsSource = (IEnumerable) dconflictViewModelList;
        this.soDConflictGrid.AutoGenerateColumns = false;
        this.soDConflictGrid.Columns.Add((DataGridColumn) (XamlReader.Parse(xamlText2) as DataGridTemplateColumn));
        this.soDConflictGrid.Columns.Add((DataGridColumn) (XamlReader.Parse(xamlText4) as DataGridTemplateColumn));
        this.soDConflictGrid.Columns.Add((DataGridColumn) (XamlReader.Parse(xamlText5) as DataGridTemplateColumn));
        this.soDConflictGrid.Columns.Add((DataGridColumn) (XamlReader.Parse(xamlText6) as DataGridTemplateColumn));
        this.soDConflictGrid.ColumnWidth = new DataGridLength(1.0, DataGridLengthUnitType.Star);
        TabItem tabItem3 = new TabItem();
        tabItem3.Header = (object) "SoD Conflict";
        tabItem3.Content = (object) this.soDConflictGrid;
        tabControl.Items.Add((object) tabItem3);
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.PlaceElement(this.Grid, (UIElement) tabControl, 1, 0);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    private void dgWorkCenter_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.RaiseSelectionChangedEvent((IModelObject) this.OberonModelObject);
    }

    private void RaiseSelectionChangedEvent(IModelObject modelToSelect)
    {
      if (this.SelectionChanged == null)
        return;
      this.SelectionChanged((object) this, new SelectionEventArgs()
      {
        SelectedObject = modelToSelect
      });
    }

    public new event SAP.BYD.LS.UIDesigner.UICore.Core.SelectionChangedEventHandler SelectionChanged;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (System.ComponentModel.IContainer) new Container();
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Text = "WorkCenterOrderDesigner";
    }
  }
}
