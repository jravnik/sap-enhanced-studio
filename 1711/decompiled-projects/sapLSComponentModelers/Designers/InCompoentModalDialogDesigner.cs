﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.InCompoentModalDialogDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class InCompoentModalDialogDesigner : ComponentDesigner
  {
    private VisualLayout m_GridLayout;
    private Grid m_InComponentModalDialogGrid;
    private VisualToolbar tbControl;
    private DisplayTextControl title;
    private System.ComponentModel.IContainer components;

    public event EventHandler BackToDesignerClicked;

    internal ModalDialog Component
    {
      get
      {
        return this.m_ModelObject as ModalDialog;
      }
    }

    public Grid InComponentModalDialogGrid
    {
      get
      {
        return this.m_InComponentModalDialogGrid;
      }
    }

    public InCompoentModalDialogDesigner(IModelObject rootModel)
      : base(rootModel, false, false)
    {
      this.AllowDrop = true;
      this.m_InComponentModalDialogGrid = new Grid();
      this.LoadView();
    }

    public override void LoadView()
    {
      this.AddTitle();
      this.AddContentRegion();
      this.AddToolBar();
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.m_InComponentModalDialogGrid == null)
        return;
      ComponentModelerUtilities.Instance.TerminatePanelControls((FrameworkElement) this.m_InComponentModalDialogGrid);
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == ActionForm.TitleProperty)
      {
        if (this.title == null && this.Component.Title != null)
        {
          this.title = new DisplayTextControl(TextViewStyles.PageHeaderTitle, this.Component.Title.Text);
          GridUtil.PlaceElement(this.m_InComponentModalDialogGrid, (UIElement) this.title, 0, 0);
        }
        if (this.title == null || this.Component == null || this.Component.Title == null)
          return;
        this.title.Text = this.Component.Title.Text;
      }
      else
      {
        if (!(e.PropertyName == "FunctionBar") || this.tbControl == null)
          return;
        this.tbControl.Toolbar = (Toolbar) this.Component.FunctionBar;
        this.tbControl.ReloadView();
      }
    }

    private void AddTitle()
    {
      GridUtil.AddRowDef(this.m_InComponentModalDialogGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.m_InComponentModalDialogGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.m_InComponentModalDialogGrid, 1.0, 150.0, GridUnitType.Pixel);
      if (this.Component.Title != null)
      {
        this.title = new DisplayTextControl(TextViewStyles.PageHeaderTitle, this.Component.Title.Text);
        GridUtil.PlaceElement(this.m_InComponentModalDialogGrid, (UIElement) this.title, 0, 0);
      }
      System.Windows.Controls.Button button = new System.Windows.Controls.Button();
      button.Background = (Brush) Brushes.Transparent;
      button.BorderBrush = (Brush) Brushes.Transparent;
      StackPanel stackPanel = new StackPanel();
      stackPanel.Orientation = System.Windows.Controls.Orientation.Horizontal;
      stackPanel.Children.Add((UIElement) new System.Windows.Controls.Image()
      {
        Source = (ImageSource) this.ConvertToBitMapImage(Resource.GoBack)
      });
      TextBlock textBlock = new TextBlock();
      textBlock.Text = "Back To Designer";
      textBlock.TextAlignment = TextAlignment.Center;
      textBlock.Foreground = (Brush) Brushes.Blue;
      textBlock.TextDecorations = TextDecorations.Underline;
      textBlock.VerticalAlignment = VerticalAlignment.Center;
      stackPanel.Children.Add((UIElement) textBlock);
      stackPanel.MouseDown += new MouseButtonEventHandler(this.buttonPanel_MouseDown);
      button.Content = (object) stackPanel;
      GridUtil.PlaceElement(this.m_InComponentModalDialogGrid, (UIElement) button, 0, 1);
    }

    private void buttonPanel_MouseDown(object sender, MouseButtonEventArgs e)
    {
      if (this.BackToDesignerClicked == null)
        return;
      this.BackToDesignerClicked(sender, (EventArgs) e);
    }

    private void AddContentRegion()
    {
      if (this.m_GridLayout != null)
        this.m_GridLayout.Terminate();
      GridUtil.AddRowDef(this.m_InComponentModalDialogGrid, 1.0, GridUnitType.Star);
      this.m_GridLayout = new VisualLayout((IModelObject) this.Component);
      this.m_GridLayout.SetValue(Grid.ColumnSpanProperty, (object) 2);
      GridUtil.PlaceElement(this.m_InComponentModalDialogGrid, (UIElement) this.m_GridLayout, 1, 0);
    }

    private void AddToolBar()
    {
      this.tbControl = new VisualToolbar((IModelObject) this.Component.FunctionBar);
      this.tbControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
      GridUtil.AddRowDef(this.m_InComponentModalDialogGrid, 25.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.m_InComponentModalDialogGrid, (UIElement) this.tbControl, 2, 0, 1, 2);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (System.ComponentModel.IContainer) new Container();
      this.AutoScaleMode = AutoScaleMode.Font;
    }
  }
}
