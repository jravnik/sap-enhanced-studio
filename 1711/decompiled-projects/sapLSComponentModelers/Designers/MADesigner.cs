﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.MADesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class MADesigner : ComponentDesigner
  {
    private static readonly SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox m_ComponentToolbox = ToolboxUtilities.DeserialzeToolboxComponent(Resource.MobileAppContainerToolbox);
    private VisualView m_ViewControl;
    private VisualContextualNavigation m_VisualCNR;
    private VisualIdentificationRegion m_VisualIDR;

    public override SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox Toolbox
    {
      get
      {
        return MADesigner.m_ComponentToolbox;
      }
    }

    internal MobileAppContainer Component
    {
      get
      {
        return this.m_ModelObject as MobileAppContainer;
      }
    }

    public MADesigner(IModelObject rootModel)
      : base(rootModel, true, true)
    {
      this.AllowDrop = true;
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.AddIdentificationRegion();
      this.AddContentRegion();
      this.AddContextualNavigationRegion();
      this.LoadLinkedControlRegion();
    }

    private void AddIdentificationRegion()
    {
      if (this.m_VisualIDR != null)
        this.m_VisualIDR.Terminate();
      this.m_VisualIDR = new VisualIdentificationRegion((IModelObject) this.Component.IdentificationRegion);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_VisualIDR, 0, 0);
    }

    private void AddContextualNavigationRegion()
    {
      if (this.m_VisualCNR != null)
        this.m_VisualCNR.Terminate();
      this.m_VisualCNR = new VisualContextualNavigation((IModelObject) this.Component.ContextNavigationRegion);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_VisualCNR, 2, 0);
    }

    private void AddContentRegion()
    {
      if (this.m_ViewControl != null)
        this.m_ViewControl.Terminate();
      this.m_ViewControl = new VisualView((IModelObject) this.Component.View);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_ViewControl, 1, 0);
    }
  }
}
