﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.SoDConflictViewModel
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class SoDConflictViewModel
  {
    private string reason;
    private string resolution;

    public string ObjectID1 { get; set; }

    public string ObjectID2 { get; set; }

    public string Reason
    {
      get
      {
        return this.reason;
      }
      set
      {
        this.reason = value;
      }
    }

    public string Resolution
    {
      get
      {
        return this.resolution;
      }
      set
      {
        this.resolution = value;
      }
    }
  }
}
