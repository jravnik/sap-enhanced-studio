﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualGridLayoutSnippet
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualGridLayoutSnippet : BaseSelectableControl
  {
    private IDesignerControl m_ParentControl;

    private GridLayout GridLayout
    {
      get
      {
        return this.Model as GridLayout;
      }
    }

    public override string SelectionText
    {
      get
      {
        return "GridLayoutSnippet";
      }
    }

    public VisualGridLayoutSnippet(IModelObject model, IDesignerControl parentControl)
      : base(model)
    {
      this.IsDraggable = true;
      this.m_ParentControl = parentControl;
      this.LoadView();
    }

    public override void LoadView()
    {
      this.Padding = new Thickness(10.0, 0.0, 10.0, 0.0);
      System.Windows.Controls.Image image = new System.Windows.Controls.Image();
      image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(Resource.GridLayoutSnippet);
      image.Stretch = Stretch.None;
      TextBlock textBlock = new TextBlock();
      textBlock.TextAlignment = TextAlignment.Center;
      textBlock.Text = this.GridLayout.Id;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) image, 0, 0);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) textBlock, 1, 0);
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        base.AddContextMenuItems();
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for VisualGridLayoutSnippet.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      try
      {
        base.HandleContextMenuItemClick(source);
        if (source.Header == null || source.Header != (object) "Delete")
          return;
        if (this.m_ParentControl is VisualLinkedRegion)
        {
          (this.GridLayout.Parent as UXView).DeleteGridLayoutSnippet(this.GridLayout);
        }
        else
        {
          if (!(this.m_ParentControl is VisualLayout))
            return;
          (this.m_ParentControl as VisualLayout).DeleteGridSnippet();
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }
  }
}
