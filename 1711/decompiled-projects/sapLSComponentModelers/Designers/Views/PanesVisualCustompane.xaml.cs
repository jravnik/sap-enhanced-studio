﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualCustompane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controller;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  internal partial class VisualCustompane : VisualBasePane
  {
    private int m_RowIndex;
    private TextBox txtAssemblyName;
    private TextBox txtTypeName;
    private Label label1;
    private Label label2;
    private Label label3;
    private ToolTip tpAssemblyName;
    private ToolTip tpTypeName;
    private Grid gdAssemblyName;
    private Grid gdTypeName;
    private VisualToolbar customPaneToolbar;

    internal CustomPane Custompane
    {
      get
      {
        return this.m_ModelObject as CustomPane;
      }
    }

    public VisualCustompane(IModelObject model)
      : base(model)
    {
      this.LoadView();
      this.DisplayData();
      this.Background = (Brush) Brushes.Beige;
    }

    public override void ConstructBodyElement()
    {
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      if (this.Custompane.HasToolBar)
        this.CreateCustomPaneToolBar(this.Custompane);
      this.ConstructBody();
    }

    private void txtAssemblyName_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      this.SwitchOffReadOnly(this.txtAssemblyName);
    }

    private void txtAssemblyName_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
    {
      if (!ValidationManager.GetInstance().IsComponentEditable((IModelObject) this.Custompane, false))
        return;
      this.UpdateAssemblyName(this.txtAssemblyName.Text);
      this.SwitchOnReadonly(this.txtAssemblyName);
    }

    private void txtAssemblyName_LostFocus(object sender, RoutedEventArgs e)
    {
      if (!ValidationManager.GetInstance().IsComponentEditable((IModelObject) this.Custompane, false))
        return;
      this.UpdateAssemblyName(this.txtAssemblyName.Text);
      this.SwitchOnReadonly(this.txtAssemblyName);
    }

    private void txtTypeName_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      if (!ValidationManager.GetInstance().IsComponentEditable((IModelObject) this.Custompane, false))
        return;
      this.SwitchOffReadOnly(this.txtTypeName);
    }

    private void txtTypeName_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
    {
      if (!ValidationManager.GetInstance().IsComponentEditable((IModelObject) this.Custompane, false))
        return;
      this.UpdateTypeName(this.txtTypeName.Text);
      this.SwitchOnReadonly(this.txtTypeName);
    }

    private void txtTypeName_LostFocus(object sender, RoutedEventArgs e)
    {
      if (!ValidationManager.GetInstance().IsComponentEditable((IModelObject) this.Custompane, false))
        return;
      this.UpdateTypeName(this.txtTypeName.Text);
      this.SwitchOnReadonly(this.txtTypeName);
    }

    private void SwitchOnReadonly(TextBox textControl)
    {
      textControl.IsReadOnly = true;
      textControl.Background = (Brush) Brushes.LightGray;
    }

    private void SwitchOffReadOnly(TextBox textControl)
    {
      textControl.IsReadOnly = false;
      textControl.Background = (Brush) Brushes.White;
    }

    private void UpdateTypeName(string typeName)
    {
      (this.Model as CustomPane).Assembly.TypeName = typeName;
    }

    private void txtTypeName_MouseEnter(object sender, MouseEventArgs e)
    {
      if (this.txtTypeName.Text == null || !(this.txtTypeName.Text != ""))
        return;
      this.tpTypeName.Content = (object) this.txtTypeName.Text;
      this.txtTypeName.ToolTip = (object) this.tpTypeName;
    }

    private void txtAssemblyName_MouseEnter(object sender, MouseEventArgs e)
    {
      if (this.txtAssemblyName.Text == null || !(this.txtAssemblyName.Text != ""))
        return;
      this.tpAssemblyName.Content = (object) this.txtAssemblyName.Text;
      this.txtAssemblyName.ToolTip = (object) this.tpAssemblyName;
    }

    private void ConstructBody()
    {
      this.txtAssemblyName = new TextBox();
      this.txtTypeName = new TextBox();
      this.label1 = new Label();
      this.label2 = new Label();
      this.label3 = new Label();
      this.gdAssemblyName = new Grid();
      this.gdTypeName = new Grid();
      this.tpAssemblyName = new ToolTip();
      this.tpTypeName = new ToolTip();
      this.gdAssemblyName.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.gdAssemblyName.Margin = new Thickness(3.0);
      this.gdTypeName.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.gdTypeName.Margin = new Thickness(3.0);
      this.txtAssemblyName.MouseDoubleClick += new MouseButtonEventHandler(this.txtAssemblyName_MouseDoubleClick);
      this.txtAssemblyName.LostKeyboardFocus += new KeyboardFocusChangedEventHandler(this.txtAssemblyName_LostKeyboardFocus);
      this.txtAssemblyName.LostFocus += new RoutedEventHandler(this.txtAssemblyName_LostFocus);
      this.txtAssemblyName.IsReadOnly = true;
      this.txtAssemblyName.Background = (Brush) Brushes.LightGray;
      this.txtAssemblyName.Height = 30.0;
      this.txtAssemblyName.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.txtAssemblyName.MouseEnter += new MouseEventHandler(this.txtAssemblyName_MouseEnter);
      this.txtTypeName.MouseDoubleClick += new MouseButtonEventHandler(this.txtTypeName_MouseDoubleClick);
      this.txtTypeName.LostKeyboardFocus += new KeyboardFocusChangedEventHandler(this.txtTypeName_LostKeyboardFocus);
      this.txtTypeName.LostFocus += new RoutedEventHandler(this.txtTypeName_LostFocus);
      this.txtTypeName.IsReadOnly = true;
      this.txtTypeName.Background = (Brush) Brushes.LightGray;
      this.txtTypeName.Height = 30.0;
      this.txtTypeName.MouseEnter += new MouseEventHandler(this.txtTypeName_MouseEnter);
      this.label1.Height = 40.0;
      this.label1.VerticalAlignment = VerticalAlignment.Top;
      this.label1.FontSize = 16.0;
      this.label1.Content = (object) "Custom Pane";
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.label1, 1, 0);
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      this.label2.Height = 40.0;
      this.label2.VerticalAlignment = VerticalAlignment.Center;
      this.label2.FontSize = 12.0;
      this.label2.Content = (object) "Assembly Name";
      GridUtil.AddRowDef(this.gdAssemblyName, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.gdAssemblyName, 0.35, GridUnitType.Star);
      GridUtil.AddColumnDef(this.gdAssemblyName, 1.0, GridUnitType.Star);
      GridUtil.PlaceElement(this.gdAssemblyName, (UIElement) this.label2, 0, 0);
      GridUtil.PlaceElement(this.gdAssemblyName, (UIElement) this.txtAssemblyName, 0, 1);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.gdAssemblyName, 2, 0);
      this.label3.Height = 40.0;
      this.label3.VerticalAlignment = VerticalAlignment.Center;
      this.label3.FontSize = 12.0;
      this.label3.Content = (object) "Type Name  ";
      GridUtil.AddRowDef(this.gdTypeName, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.gdTypeName, 0.35, GridUnitType.Star);
      GridUtil.AddColumnDef(this.gdTypeName, 1.0, GridUnitType.Star);
      GridUtil.PlaceElement(this.gdTypeName, (UIElement) this.label3, 0, 0);
      GridUtil.PlaceElement(this.gdTypeName, (UIElement) this.txtTypeName, 0, 1);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.gdTypeName, 3, 0);
    }

    private void CreateCustomPaneToolBar(CustomPane customPane)
    {
      if (customPane.Toolbar == null)
        return;
      if (this.customPaneToolbar == null)
      {
        this.customPaneToolbar = new VisualToolbar((IModelObject) customPane.Toolbar);
        this.customPaneToolbar.VerticalAlignment = VerticalAlignment.Top;
        this.customPaneToolbar.HorizontalAlignment = HorizontalAlignment.Stretch;
        this.customPaneToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        this.customPaneToolbar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.customPaneToolbar, 0, 0);
      }
      else
      {
        this.customPaneToolbar.Toolbar = (Toolbar) customPane.Toolbar;
        this.customPaneToolbar.ReloadView();
      }
    }

    private void UpdateAssemblyName(string assemblyName)
    {
      (this.Model as CustomPane).Assembly.AssemblyName = assemblyName;
    }

    private void DisplayData()
    {
      if ((this.Model as CustomPane).Assembly == null)
        return;
      if ((this.Model as CustomPane).Assembly.AssemblyName != null)
        this.txtAssemblyName.Text = (this.Model as CustomPane).Assembly.AssemblyName;
      if ((this.Model as CustomPane).Assembly.TypeName == null)
        return;
      this.txtTypeName.Text = (this.Model as CustomPane).Assembly.TypeName;
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == CustomPane.HasToolBarProperty)
        {
          if (!string.IsNullOrEmpty(e.NewValue.ToString()) && Convert.ToBoolean(e.NewValue.ToString()))
            this.CreateCustomPaneToolBar(this.Model as CustomPane);
          else
            GridUtil.RemoveElement(this.BodyElementGrid, (UIElement) this.customPaneToolbar, false, 0, 0);
        }
        else if (e.PropertyName == CustomPane.ToolBarProperty)
        {
          this.CreateCustomPaneToolBar(this.Model as CustomPane);
        }
        else
        {
          if (!(e.PropertyName == CustomPane.AssemblyProperty) && !(e.PropertyName == CustomPane.TypeProperty))
            return;
          CustomPane customPane = sender as CustomPane;
          if (customPane == null)
            return;
          if (customPane.Assembly.AssemblyName != null)
            this.txtAssemblyName.Text = customPane.Assembly.AssemblyName;
          if (customPane.Assembly.TypeName == null)
            return;
          this.txtTypeName.Text = customPane.Assembly.TypeName;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("PropertyChanged handler failed to update the Custompane UI.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (source.Header != (object) "Delete" || this.Custompane == null)
        return;
      if (this.Custompane.Assembly != null && this.Custompane.Assembly.RequiredAssemblies != null)
      {
        if (this.Custompane.Assembly.AssemblyName != null && this.Custompane.Assembly.AssemblyName.Contains(".dll"))
        {
          List<Assembly> assemblyCollection = this.Custompane.Assembly.RequiredAssemblies.AssemblyCollection;
          foreach (Assembly assembly in this.Custompane.Assembly.RequiredAssemblies.AssemblyCollection)
          {
            if (assembly.Value.Contains(this.Custompane.Assembly.AssemblyName))
            {
              assemblyCollection.Remove(assembly);
              break;
            }
          }
          this.Custompane.Assembly.RequiredAssemblies.AssemblyCollection = assemblyCollection;
        }
        else if (this.Custompane.Assembly.AssemblyName != null && this.Custompane.Assembly.AssemblyName.Contains(".xap"))
        {
          List<Assembly> xapCollection = this.Custompane.Assembly.RequiredAssemblies.XAPCollection;
          foreach (Assembly xap in this.Custompane.Assembly.RequiredAssemblies.XAPCollection)
          {
            if (xap.Value.Contains(this.Custompane.Assembly.AssemblyName))
            {
              xapCollection.Remove(xap);
              break;
            }
          }
          this.Custompane.Assembly.RequiredAssemblies.XAPCollection = xapCollection;
        }
      }
      if (this.Custompane.Parent == null)
        return;
      this.Custompane.Parent.RemoveModelObject((IModelObject) this.Custompane);
    }
  }
}
