﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualPlaceableFields
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualPlaceableFields : BaseBorderControl
  {
    public PlaceableFields PlaceableFields
    {
      get
      {
        return this.m_ModelObject as PlaceableFields;
      }
    }

    public VisualPlaceableFields(IModelObject PlaceableFields)
      : base(PlaceableFields)
    {
      this.AllowDrop = true;
      this.IsSpanable = true;
      this.IsDraggable = true;
      this.MinHeight = 150.0;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "PlaceableFields";
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      base.OnModelObjectAdded(sender, e);
      this.ReloadView();
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      this.ReloadView();
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      this.ReloadView();
    }

    public override void LoadView()
    {
      int row = 0;
      foreach (ControlUsage field in this.PlaceableFields.Fields)
      {
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) new VisualPlaceableField((IModelObject) field), row, 0);
        ++row;
      }
    }
  }
}
