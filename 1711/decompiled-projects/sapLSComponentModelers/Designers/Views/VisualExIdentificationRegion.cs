﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualExIdentificationRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.IdentificationRegion2;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualExIdentificationRegion : BaseSelectableControl
  {
    private const string ADDCOMMAND = "Add";
    private const string EXTIDR = "Extended IDR Element";
    private const string IDR = "ExtIdr";
    private WrapPanel wrapPanel;

    internal ExtendedIdentificationRegion ExIdrRegion
    {
      get
      {
        return this.m_ModelObject as ExtendedIdentificationRegion;
      }
    }

    public VisualExIdentificationRegion(IModelObject idrRegion)
      : base(idrRegion, false, false)
    {
      this.wrapPanel = new WrapPanel();
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.Padding = new Thickness(1.0);
        this.Margin = new Thickness(1.0);
        this.ControlBackground = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        this.wrapPanel.ItemHeight = 20.0;
        this.wrapPanel.ClipToBounds = true;
        this.wrapPanel.Children.Clear();
        foreach (IModelObject element in this.ExIdrRegion.Elements)
          this.wrapPanel.Children.Add((UIElement) new IDRElement(element));
        this.Grid.Children.Add((UIElement) this.wrapPanel);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("ExIdentification Region UI initialization failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Extended IDR Element", this.AddContextMenuItem("Add"));
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) "Extended IDR Element")
        return;
      this.AddExtIDR();
    }

    public override string SelectionText
    {
      get
      {
        return "Extended IDR";
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      IModelObject addedModel = e.AddedModel;
      if (addedModel is ControlUsage)
      {
        IDRElement idrElement = new IDRElement((IModelObject) (addedModel as ControlUsage));
        this.wrapPanel.ClipToBounds = true;
        this.wrapPanel.Children.Insert(e.PositionInParent, (UIElement) idrElement);
      }
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      if (e.RemovedModel == null)
        return;
      foreach (UIElement child in this.wrapPanel.Children)
      {
        IDRElement idrElement = child as IDRElement;
        if (idrElement != null && idrElement.IDRModel == e.RemovedModel)
        {
          idrElement.Terminate();
          this.wrapPanel.Children.Remove((UIElement) idrElement);
          break;
        }
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (!(e.PropertyName == ExtendedIdentificationRegion.ElementsProperty))
        return;
      this.ReloadView();
    }

    public void AddExtIDR()
    {
      try
      {
        (this.ExIdrRegion.Parent as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.IdentificationRegion).AddExtendedIdetification();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Adding extended identification region failed", ex));
      }
    }
  }
}
