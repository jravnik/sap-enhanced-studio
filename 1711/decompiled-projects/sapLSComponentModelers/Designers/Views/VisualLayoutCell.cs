﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualLayoutCell
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.EventArg;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualLayoutCell : BaseBorderControl
  {
    private System.Windows.Controls.ComboBox combobox;
    private BaseVisualControl visualPaneControl;
    private BaseVisualControl parentVisualControl;

    internal GridLayout GridLayout
    {
      get
      {
        if (this.m_ModelObject == null)
          return (GridLayout) null;
        return this.m_ModelObject.Parent as GridLayout;
      }
    }

    internal PaneContainer PaneContainer
    {
      get
      {
        return this.m_ModelObject as PaneContainer;
      }
    }

    public VisualLayoutCell(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.parentVisualControl = parentControl;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.AllowDrop = true;
      this.IsSpanable = true;
      this.InitializeSpanningDetails();
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        GridUtil.AddRowDef(this.Grid, 150.0, 150.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        this.LoadPaneContainerView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new ModelLayerException("Loading of Visual pane container failed.", ex), true);
      }
    }

    public override string SelectionText
    {
      get
      {
        return "PaneContainer";
      }
    }

    public override void BringChildControlToFocus(IModelObject childControlModel)
    {
      try
      {
        base.BringChildControlToFocus(childControlModel);
        PaneContainerVariant containerVariant = childControlModel as PaneContainerVariant;
        if (containerVariant == null)
          return;
        int num = this.PaneContainer.Items.IndexOf((BasePane) containerVariant);
        if (num <= -1)
          return;
        this.combobox.SelectedIndex = num;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Bringing the selected child to front failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
        if (!(e.AddedModel is MapPane))
          return;
        for (IModelObject parent = e.AddedModel.Parent; parent != null; parent = parent.Parent)
        {
          if (parent is DetailVisualization)
          {
            (parent as DetailVisualization).SideBySide = true;
            break;
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new ModelLayerException("UI Updating of ModelObjectAdded event in PaneContainer failed.", ex), true);
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
        if (!(e.RemovedModel is MapPane))
          return;
        for (IModelObject parent = e.RemovedModel.Parent; parent != null; parent = parent.Parent)
        {
          if (parent is DetailVisualization)
          {
            (parent as DetailVisualization).SideBySide = false;
            break;
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new ModelLayerException("UI Updating of ModelObjectRemoved event in PaneContainer failed.", ex), true);
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == PaneContainer.ItemsProperty)
      {
        this.ReloadView();
      }
      else
      {
        if (!(e.PropertyName == PaneContainer.ColumnProperty) && !(e.PropertyName == PaneContainer.RowProperty) || this.parentVisualControl == null)
          return;
        this.parentVisualControl.ReloadView();
      }
    }

    protected override void OnSpanDownArrowClicked(object sender, EventArgs e)
    {
      try
      {
        if (Utilities.GetFloorplanObject(this.m_ModelObject).Changeability != ContentChangeability.ChangeTransaction)
        {
          base.OnSpanDownArrowClicked(sender, e);
          this.GridLayout.IncrementRowSpan((IContainer) this.PaneContainer);
        }
        else
        {
          int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.Model.Resource.ChangeTransactionMode, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnSpanLeftArrowClicked(object sender, EventArgs e)
    {
      try
      {
        if (Utilities.GetFloorplanObject(this.m_ModelObject).Changeability != ContentChangeability.ChangeTransaction)
        {
          base.OnSpanLeftArrowClicked(sender, e);
          this.GridLayout.DecrementColSpan((IContainer) this.PaneContainer);
        }
        else
        {
          int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.Model.Resource.ChangeTransactionMode, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnSpanRightArrowClicked(object sender, EventArgs e)
    {
      try
      {
        if (Utilities.GetFloorplanObject(this.m_ModelObject).Changeability != ContentChangeability.ChangeTransaction)
        {
          base.OnSpanRightArrowClicked(sender, e);
          this.GridLayout.IncrementColSpan((IContainer) this.PaneContainer);
        }
        else
        {
          int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.Model.Resource.ChangeTransactionMode, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnSpanUpArrowClicked(object sender, EventArgs e)
    {
      try
      {
        if (Utilities.GetFloorplanObject(this.m_ModelObject).Changeability != ContentChangeability.ChangeTransaction)
        {
          base.OnSpanUpArrowClicked(sender, e);
          this.GridLayout.DecrementRowSpan((IContainer) this.PaneContainer);
        }
        else
        {
          int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.Model.Resource.ChangeTransactionMode, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnBOElementDragOver(System.Windows.DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == System.Windows.DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
      {
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
        if (boElement == null)
        {
          BOElementAttribute draggedElement = boBrowserArgs[0].DraggedElement as BOElementAttribute;
          if (draggedElement != null)
            boElement = draggedElement.ParentElement;
        }
      }
      if (boElement != null && boElement.ParentQuery != null)
        e.Effects = System.Windows.DragDropEffects.None;
      if (boElement == null || boElement.ParentAction == null || this.m_ModelObject.Parent is ActionForm)
        return;
      e.Effects = System.Windows.DragDropEffects.None;
    }

    protected override void OnBOElementDrop(BOBrowserDragEventArgs boBrowserArgs)
    {
      int num1 = -1;
      FieldGroup fieldGroup = (FieldGroup) null;
      foreach (BOBrowserDragEventArg boBrowserArg in (List<BOBrowserDragEventArg>) boBrowserArgs)
      {
        ++num1;
        if (num1 == 0)
        {
          if (this.Model.Parent is ActionForm)
          {
            BOElement boElement = boBrowserArg.DraggedElement as BOElement;
            if (boElement == null)
            {
              BOElementAttribute draggedElement = boBrowserArg.DraggedElement as BOElementAttribute;
              if (draggedElement != null)
                boElement = draggedElement.ParentElement;
            }
            if (boElement.ParentAction == null)
            {
              int num2 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.OnlyActionParameterAllowed, MessageBoxButtons.OK, MessageBoxIcon.Hand);
              break;
            }
          }
          this.Model.AddModelObject(boBrowserArg);
          PaneContainer model = this.Model as PaneContainer;
          if (model != null && model.Items != null && (model.Items.Count > 0 && model.Items[0] is FormPane) && ((model.Items[0] as FormPane).SectionGroups != null && (model.Items[0] as FormPane).SectionGroups.Count > 0))
            fieldGroup = (model.Items[0] as FormPane).SectionGroups[0];
        }
        else if (fieldGroup != null)
          fieldGroup.AddModelObject(boBrowserArg);
      }
    }

    protected override void OnComponentDragOver(System.Windows.DragEventArgs e, ConfigurationExplorerDragEventArgs ceArgs)
    {
      base.OnComponentDragOver(e, ceArgs);
      if (e.Effects == System.Windows.DragDropEffects.None)
        return;
      if (ceArgs.ComponentDetails.ComponentType == "EC" || ceArgs.ComponentDetails.ComponentType == "OWL" || ceArgs.ComponentDetails.ComponentType == "MC")
        e.Effects = System.Windows.DragDropEffects.Copy;
      else
        e.Effects = System.Windows.DragDropEffects.None;
    }

    protected override void OnDataModelElementDragOver(System.Windows.DragEventArgs e, DataModelDragEventArgs dataModelEventArgs)
    {
      if (dataModelEventArgs.DataElement.IsQueryBinding)
        e.Effects = System.Windows.DragDropEffects.None;
      else
        base.OnDataModelElementDragOver(e, dataModelEventArgs);
    }

    protected override void OnBaseVisualControlDragOver(System.Windows.DragEventArgs e, IModelObject droppedModel)
    {
      if (droppedModel is FieldGroup && (droppedModel as FieldGroup).Parent.GetType() == typeof (FindFormPane))
        e.Effects = System.Windows.DragDropEffects.None;
      else
        base.OnBaseVisualControlDragOver(e, droppedModel);
    }

    private void LoadPaneContainerView()
    {
      this.Grid.Children.Clear();
      if (this.PaneContainer.Items == null || this.PaneContainer.Items.Count <= 0)
        return;
      this.visualPaneControl = PaneFactory.GetVisualPaneControl((IModelObject) this.PaneContainer.Items[0]);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.visualPaneControl, 0, 0);
      this.UpdateVariantsInformation();
    }

    private void UpdateVariantsInformation()
    {
      if (this.combobox == null)
      {
        this.combobox = new System.Windows.Controls.ComboBox();
        this.combobox.Height = 25.0;
        this.combobox.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
        this.combobox.SelectionChanged += new SelectionChangedEventHandler(this.OnComboBoxSelectionChanged);
      }
      GridUtil.PlaceElement(this.Grid, (UIElement) this.combobox, 1, 1);
      if (this.PaneContainer.Items.Count > 1)
      {
        this.combobox.Items.Clear();
        this.combobox.Visibility = Visibility.Visible;
        for (int index = 0; index < this.PaneContainer.Items.Count; ++index)
          this.combobox.Items.Add((object) this.PaneContainer.Items[index].Name);
        this.combobox.SelectedIndex = 0;
      }
      else
        this.combobox.Visibility = Visibility.Collapsed;
    }

    private void OnComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.combobox.SelectedIndex == -1 || !this.combobox.IsDropDownOpen)
        return;
      int selectedIndex = this.combobox.SelectedIndex;
      if (this.visualPaneControl != null)
      {
        this.visualPaneControl.Terminate();
        this.Grid.Children.Remove((UIElement) this.visualPaneControl);
      }
      this.visualPaneControl = PaneFactory.GetVisualPaneControl((IModelObject) this.PaneContainer.Items[selectedIndex]);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.visualPaneControl, 0, 0);
      this.TriggerSelection(this.visualPaneControl as BaseSelectableControl, false);
    }

    public void InitializeSpanningDetails()
    {
      this.InitializeSpanningDetails(new SpanningInfo(this.GridLayout.Columns, this.GridLayout.Rows, this.PaneContainer.Column, this.PaneContainer.Row, this.PaneContainer.ColSpan, this.PaneContainer.RowSpan), SpanningStyles.both);
    }
  }
}
