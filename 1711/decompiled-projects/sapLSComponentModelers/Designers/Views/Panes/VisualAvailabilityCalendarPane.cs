﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualAvailabilityCalendarPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualAvailabilityCalendarPane : VisualBasePane, IContextMenuContainer
  {
    private AvailabilityCalendarPane m_AvailabilityCalendarPane;
    private AvailabilityCalendarToolbar m_CalendarToolBar;
    private SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.VisualLegend m_VisualLegend;
    private VisualLayout m_PreviewArea;
    private int m_RowIndex;

    public VisualAvailabilityCalendarPane(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_AvailabilityCalendarPane = this.m_ModelObject as AvailabilityCalendarPane;
      this.LoadView();
    }

    protected AvailabilityCalendarPane AvailabilityCalendarPane
    {
      get
      {
        return this.m_ModelObject as AvailabilityCalendarPane;
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == AvailabilityCalendarPane.UsePreviewPaneProperty)
      {
        this.ReloadView();
      }
      else
      {
        if (!(e.PropertyName == "Toolbar"))
          return;
        this.m_CalendarToolBar.CustomToolBar = e.NewValue as AvailabilityCalendarPaneToolbar;
        this.m_CalendarToolBar.ReloadView();
      }
    }

    public override void ConstructBodyElement()
    {
    }

    public override void LoadView()
    {
      this.m_RowIndex = 0;
      base.LoadView();
      try
      {
        this.BodyElementGrid.Margin = ComponentModelersConstants.CALENDAR_CONTROL_BORDER_MARGIN;
        if (this.AvailabilityCalendarPane == null)
          return;
        if (this.AvailabilityCalendarPane.Toolbar != null)
          this.AddToolBar((Toolbar) this.AvailabilityCalendarPane.Toolbar);
        if (this.AvailabilityCalendarPane.AvailabilityCalendarList != null)
          this.AddAvailabilityCalendarList(this.AvailabilityCalendarPane.AvailabilityCalendarList);
        if (this.AvailabilityCalendarPane.LegendField != null)
          this.AddCalendarLegend(this.AvailabilityCalendarPane.LegendField);
        if (this.AvailabilityCalendarPane.PreviewArea == null)
          return;
        this.CreatePreviewArea(this.AvailabilityCalendarPane);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("AvailabilityCalendarPane failed to load", ex));
      }
    }

    private void AddToolBar(Toolbar toolBar)
    {
      if (toolBar == null)
        return;
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      this.m_CalendarToolBar = new AvailabilityCalendarToolbar((IModelObject) toolBar);
      this.m_CalendarToolBar.VerticalAlignment = VerticalAlignment.Top;
      this.m_CalendarToolBar.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.m_CalendarToolBar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      this.m_CalendarToolBar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.m_CalendarToolBar, this.m_RowIndex++, 0);
    }

    private void AddAvailabilityCalendarList(AvailabilityCalendarList list)
    {
      if (list == null)
        return;
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      VisualList visualList = new VisualList((IModelObject) list);
      visualList.VerticalAlignment = VerticalAlignment.Top;
      visualList.HorizontalAlignment = HorizontalAlignment.Stretch;
      visualList.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) visualList, this.m_RowIndex++, 0);
    }

    private void AddCalendarLegend(Legend calendarLegend)
    {
      if (calendarLegend == null)
        return;
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      this.m_VisualLegend = new SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.VisualLegend((IModelObject) calendarLegend);
      this.m_VisualLegend.VerticalAlignment = VerticalAlignment.Top;
      this.m_VisualLegend.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.m_VisualLegend.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.m_VisualLegend, this.m_RowIndex++, 0);
    }

    private void CreatePreviewArea(AvailabilityCalendarPane calendarPane)
    {
      GridUtil.AddRowDef(this.Grid, 300.0, GridUnitType.Star);
      VisualLayout visualLayout = new VisualLayout((IModelObject) calendarPane.PreviewArea);
      this.m_PreviewArea = visualLayout;
      visualLayout.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) visualLayout, this.m_RowIndex++, 0);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      try
      {
        if (source.Header != null && Convert.ToString(source.Header).Equals("Delete"))
        {
          if (this.AvailabilityCalendarPane.Parent == null)
            return;
          this.AvailabilityCalendarPane.Parent.RemoveModelObject((IModelObject) this.AvailabilityCalendarPane);
        }
        else
        {
          if (this.m_CalendarToolBar == null)
            return;
          this.m_CalendarToolBar.HandleContextMenuItemWrapper(source);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }
  }
}
