﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualRowRepeaterPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualRowRepeaterPane : VisualBasePane
  {
    private int desiredRows;

    internal RowRepeaterPane GanttChartPane
    {
      get
      {
        return this.m_ModelObject as RowRepeaterPane;
      }
    }

    public int DesiredRows
    {
      get
      {
        return this.desiredRows;
      }
    }

    public VisualRowRepeaterPane(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "RowRepeaterPane";
      }
    }

    public override void ConstructBodyElement()
    {
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.BodyElementGrid, 150.0, 150.0, GridUnitType.Auto);
    }
  }
}
