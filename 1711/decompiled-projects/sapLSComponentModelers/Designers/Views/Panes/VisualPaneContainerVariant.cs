﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualPaneContainerVariant
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.EventArg;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualPaneContainerVariant : BaseSelectableControl
  {
    private BaseVisualControl paneControl;

    private PaneContainerVariant PaneContainerVariant
    {
      get
      {
        return this.m_ModelObject as PaneContainerVariant;
      }
    }

    public VisualPaneContainerVariant(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.AllowDrop = true;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "PaneContainerVariant";
      }
    }

    public override void LoadView()
    {
      GridUtil.AddRowDef(this.Grid, 150.0, 150.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      if (this.PaneContainerVariant.Item == null)
        return;
      this.paneControl = PaneFactory.GetVisualPaneControl((IModelObject) this.PaneContainerVariant.Item);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.paneControl, 0, 0);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      if (this.paneControl != null)
      {
        this.paneControl.Terminate();
        this.Grid.Children.Remove((UIElement) this.paneControl);
      }
      if (this.PaneContainerVariant.Item != null)
      {
        this.paneControl = PaneFactory.GetVisualPaneControl((IModelObject) this.PaneContainerVariant.Item);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.paneControl, 0, 0);
        if (this.PaneContainerVariant.Item is MapPane)
        {
          for (IModelObject parent = e.AddedModel.Parent; parent != null; parent = parent.Parent)
          {
            if (parent is DetailVisualization)
            {
              (parent as DetailVisualization).SideBySide = true;
              break;
            }
          }
        }
      }
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnComponentDragOver(DragEventArgs e, ConfigurationExplorerDragEventArgs ceArgs)
    {
      base.OnComponentDragOver(e, ceArgs);
      if (e.Effects == DragDropEffects.None)
        return;
      if (ceArgs.ComponentDetails.ComponentType == "EC" || ceArgs.ComponentDetails.ComponentType == "OWL")
        e.Effects = DragDropEffects.Copy;
      else
        e.Effects = DragDropEffects.None;
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      if (this.paneControl == null)
        return;
      this.paneControl.Terminate();
      this.Grid.Children.Remove((UIElement) this.paneControl);
      if (!(e.RemovedModel is MapPane))
        return;
      for (IModelObject parent = e.RemovedModel.Parent; parent != null; parent = parent.Parent)
      {
        if (parent is DetailVisualization)
        {
          (parent as DetailVisualization).SideBySide = false;
          break;
        }
      }
    }

    protected override void OnBOElementDragOver(DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
      if (boElement == null || boElement.ParentQuery == null)
        return;
      e.Effects = DragDropEffects.None;
    }

    protected override void OnToolBoxControlDragOver(DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      base.OnToolBoxControlDragOver(e, toolboxEventArg);
      if (e.Effects == DragDropEffects.None || toolboxEventArg.Tag.ControlName == null || !toolboxEventArg.Tag.ControlName.Equals("MapPane"))
        return;
      if (this.PaneContainerVariant.Parent.Parent is DetailVisualization)
        e.Effects = DragDropEffects.Copy;
      else
        e.Effects = DragDropEffects.None;
    }

    protected override void OnToolBoxControlDrop(ToolBoxEventArgs toolboxEventArg)
    {
      if (toolboxEventArg.Tag.ControlName != null && toolboxEventArg.Tag.ControlName.Equals("MapPane"))
      {
        for (IModelObject modelObject = this.Model; modelObject != null; modelObject = modelObject.Parent)
        {
          if (modelObject is DetailVisualization)
          {
            this.Model.AddModelObject(toolboxEventArg.Tag);
            break;
          }
        }
      }
      else
        base.OnToolBoxControlDrop(toolboxEventArg);
    }
  }
}
