﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualECPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.EmbeddedComponentControl;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  internal class VisualECPane : VisualBasePane
  {
    internal EmbeddedComponentPane EmbeddedComponentPane
    {
      get
      {
        return this.m_ModelObject as EmbeddedComponentPane;
      }
    }

    public VisualECPane(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void ConstructBodyElement()
    {
      try
      {
        GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.BodyElementGrid, 150.0, 150.0, GridUnitType.Auto);
        VisualEmbeddedComponent embeddedComponent = new VisualEmbeddedComponent((IModelObject) this.EmbeddedComponentPane.AssociatedEmbeddComponent, this.EmbeddedComponentPane);
        if (this.EmbeddedComponentPane == null || this.EmbeddedComponentPane.Parent == null)
          return;
        if (this.EmbeddedComponentPane.Parent is PaneContainer)
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) embeddedComponent, 0, 0, (this.EmbeddedComponentPane.Parent as PaneContainer).RowSpan, (this.EmbeddedComponentPane.Parent as PaneContainer).ColSpan);
        else if (this.EmbeddedComponentPane.Parent is PaneContainerVariant)
        {
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) embeddedComponent, 0, 0, ((this.EmbeddedComponentPane.Parent as PaneContainerVariant).Parent as PaneContainer).RowSpan, ((this.EmbeddedComponentPane.Parent as PaneContainerVariant).Parent as PaneContainer).ColSpan);
        }
        else
        {
          if (!(this.EmbeddedComponentPane.Parent is SplitPane))
            return;
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) embeddedComponent, 0, 0, 1, 1);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of Embedded pane control failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        base.AddContextMenuItems();
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for VisualECPane.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      try
      {
        base.HandleContextMenuItemClick(source);
        if (source.Header == null || source.Header != (object) "Delete" || this.EmbeddedComponentPane.Parent == null)
          return;
        this.EmbeddedComponentPane.Parent.RemoveModelObject((IModelObject) this.EmbeddedComponentPane);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "EmbeddedComponentPane";
      }
    }
  }
}
