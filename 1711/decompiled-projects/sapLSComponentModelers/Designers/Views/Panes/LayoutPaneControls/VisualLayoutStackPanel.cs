﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls.VisualLayoutStackPanel
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls
{
  public class VisualLayoutStackPanel : BaseBorderControl
  {
    private WrapPanel sp;

    private LayoutStackPanel LayoutStackPanelModel
    {
      get
      {
        return this.m_ModelObject as LayoutStackPanel;
      }
    }

    public VisualLayoutStackPanel(IModelObject model)
      : base(model)
    {
      this.IsDraggable = true;
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.BorderBrush = (Brush) Brushes.Chocolate;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        this.Grid.MinWidth = 150.0;
        this.MinHeight = 30.0;
        double left = 0.0;
        double top = 0.0;
        double right = 0.0;
        double bottom = 0.0;
        this.sp = new WrapPanel();
        this.sp.Orientation = this.LayoutStackPanelModel.PanelOrientation != OrientationType.horizontal ? Orientation.Vertical : Orientation.Horizontal;
        LayoutStackPanel layoutStackPanelModel = this.LayoutStackPanelModel;
        if (this.LayoutStackPanelModel.Margin != null)
        {
          if (this.LayoutStackPanelModel.Margin is StandardThickness)
          {
            left = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutStackPanelModel.Margin as StandardThickness).Left);
            top = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutStackPanelModel.Margin as StandardThickness).Top);
            right = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutStackPanelModel.Margin as StandardThickness).Right);
            bottom = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutStackPanelModel.Margin as StandardThickness).Bottom);
          }
          this.Margin = new Thickness(left, top, right, bottom);
        }
        if (this.LayoutStackPanelModel.Items != null)
        {
          foreach (LayoutElement layoutElement in this.LayoutStackPanelModel.Items)
          {
            UIElement element = ControlFactory.ConstructLayoutElementControl(layoutElement);
            if (element is VisualLayoutBorderControl || element is VisualIteratorControl || element is VisualLayoutToolbar)
            {
              (element as FrameworkElement).MinHeight = 30.0;
              (element as FrameworkElement).MinWidth = 150.0;
              this.sp.Children.Add(element);
            }
            else
              this.sp.Children.Add(element);
          }
        }
        GridUtil.PlaceElement(this.Grid, (UIElement) this.sp, 0, 0);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Layout Grid Control UI initialization failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Layout Stack Panel";
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Paste");
      this.AddContextMenuItem("Cut");
      this.AddContextMenuItem("Copy");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == (object) "Delete")
        this.LayoutStackPanelModel.Parent.RemoveModelObject((IModelObject) this.LayoutStackPanelModel, false);
      else if (source.Header == (object) "Paste")
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyPasteOperation();
      else if (source.Header == (object) "Cut")
      {
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCutOperation();
      }
      else
      {
        if (source.Header != (object) "Copy")
          return;
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCopyOperation();
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        try
        {
          if (e.PropertyName == LayoutStackPanel.OrientationProperty)
          {
            if (e.NewValue.ToString() == OrientationType.horizontal.ToString())
              this.sp.Orientation = Orientation.Horizontal;
            else if (e.NewValue.ToString() == OrientationType.vertical.ToString())
              this.sp.Orientation = Orientation.Vertical;
          }
          else if (e.PropertyName == LayoutElement.ThicknessProperty)
          {
            if (this.LayoutStackPanelModel.Margin is CustomThickness)
              this.sp.Margin = new Thickness((double) (e.NewValue as CustomThickness).Left, (double) (e.NewValue as CustomThickness).Top, (double) (e.NewValue as CustomThickness).Right, (double) (e.NewValue as CustomThickness).Left);
          }
          else if (e.PropertyName == LayoutElement.MarginProperty)
          {
            if (this.LayoutStackPanelModel.MarginType == SelectionType.Standard)
              this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
          }
        }
        catch (Exception ex)
        {
          ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update the property changes in the model.", ex));
        }
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle ModelPropertyChanged event of Layout Stack Panel", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      this.ReloadView();
    }

    protected override void OnBOElementDragOver(DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
      FindFormPane parentFindFormPane = Utilities.GetParentFindFormPane((IModelObject) this.LayoutStackPanelModel);
      if (boElement != null && boElement.ParentQuery != null && parentFindFormPane == null)
        e.Effects = DragDropEffects.None;
      if (boElement == null || boElement.ParentAction == null)
        return;
      PaneContainer parentLayoutCell = Utilities.GetParentLayoutCell(this.m_ModelObject);
      if (parentLayoutCell == null || parentLayoutCell.Parent is ActionForm)
        return;
      e.Effects = DragDropEffects.None;
    }
  }
}
