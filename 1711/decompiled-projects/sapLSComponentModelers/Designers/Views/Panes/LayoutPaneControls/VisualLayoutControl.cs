﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls.VisualLayoutControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls
{
  public class VisualLayoutControl : BaseSelectableControl
  {
    private VisualLabel labelControl;
    private AbstractControl control;
    private UIElement visualControl;

    internal LayoutControl LayoutControl
    {
      get
      {
        return this.m_ModelObject as LayoutControl;
      }
    }

    public VisualLayoutControl(IModelObject model)
      : base(model)
    {
      this.IsDraggable = true;
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      (this.Model as LayoutControl).Content.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      this.Grid.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      double left = 0.0;
      double top = 0.0;
      double right = 0.0;
      double bottom = 0.0;
      if (this.LayoutControl.Margin != null && this.LayoutControl.Margin is StandardThickness)
      {
        left = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutControl.Margin as StandardThickness).Left);
        top = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutControl.Margin as StandardThickness).Top);
        right = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutControl.Margin as StandardThickness).Right);
        bottom = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutControl.Margin as StandardThickness).Bottom);
      }
      this.Margin = new Thickness(left, top, right, bottom);
      this.VerticalAlignment = VerticalAlignment.Top;
      this.control = (this.Model as LayoutControl).Content.Item;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 5.0, GridUnitType.Pixel);
      if (this.control.CCTSType != UXCCTSTypes.none)
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      else if (this.control.FieldWidth != null && this.control.FieldWidth != string.Empty)
        this.SetFieldWidthOfControls();
      else
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      bool showAsterisk = false;
      if (this.control is EditControl)
        showAsterisk = Converter.ToBoolean((DependentProperty) (this.control as EditControl).Mandatory, false);
      else if (this.control is CompoundField)
      {
        CompoundField control = this.control as CompoundField;
        if (control.Items != null)
        {
          foreach (AbstractControl abstractControl in control.Items)
          {
            if (this.control is EditControl)
            {
              EditControl editControl = abstractControl as EditControl;
              showAsterisk |= Converter.ToBoolean((DependentProperty) editControl.Mandatory, false);
            }
          }
        }
      }
      this.labelControl = new VisualLabel((this.Model as LayoutControl).Content.Label != null ? (this.Model as LayoutControl).Content.Label.Text : string.Empty, showAsterisk, false);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.labelControl, 0, 0);
      if (!this.LayoutControl.Content.LabelVisible)
        this.labelControl.Visibility = Visibility.Collapsed;
      this.visualControl = ControlFactory.ConstructFormpaneControl(this.control, UXCCTSTypes.none, UsageType.form);
      if (this.control is TextEditControl)
        this.SetHeightOfTextEditControl();
      GridUtil.PlaceElement(this.Grid, this.visualControl, 0, 2);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      this.ReloadView();
    }

    public override string SelectionText
    {
      get
      {
        return "Layout Control";
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Cut");
      this.AddContextMenuItem("Copy");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == (object) "Delete")
        this.LayoutControl.Parent.RemoveModelObject((IModelObject) this.LayoutControl, false);
      else if (source.Header == (object) "Cut")
      {
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCutOperation();
      }
      else
      {
        if (source.Header != (object) "Copy")
          return;
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCopyOperation();
      }
    }

    private void SetFieldWidthOfControls()
    {
      double widthValue = ControlFactory.GetWidthValue(this.control.FieldWidth);
      string nonDigitPart = Utilities.GetNonDigitPart(this.control.FieldWidth);
      if (nonDigitPart == "%")
      {
        GridUtil.AddColumnDef(this.Grid, widthValue, GridUnitType.Star);
        GridUtil.AddColumnDef(this.Grid, 100.0 - widthValue, GridUnitType.Star);
      }
      else
      {
        if (!(nonDigitPart == "ex") && !(nonDigitPart == "px"))
          return;
        GridUtil.AddColumnDef(this.Grid, widthValue, GridUnitType.Pixel);
      }
    }

    private void SetHeightOfTextEditControl()
    {
      TextEditControl control = this.control as TextEditControl;
      VisualTextEditControl visualControl = this.visualControl as VisualTextEditControl;
      if (control == null || visualControl == null || control.Rows == 0)
        return;
      visualControl.Rows = control.Rows;
      this.Grid.RowDefinitions[0].Height = new GridLength((double) control.Rows * 25.0, GridUnitType.Pixel);
    }
  }
}
