﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls.VisualIteratorControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls
{
  public class VisualIteratorControl : BaseBorderControl
  {
    internal Iterator Iterator
    {
      get
      {
        return this.m_ModelObject as Iterator;
      }
    }

    public VisualIteratorControl(IModelObject model)
      : base(model)
    {
      this.IsDraggable = true;
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.Grid.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      this.BorderBrush = (Brush) Brushes.CadetBlue;
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      try
      {
        this.MinHeight = 20.0;
        double left = 0.0;
        double top = 0.0;
        double right = 0.0;
        double bottom = 0.0;
        if (this.Iterator.Margin != null)
        {
          if (this.Iterator.Margin is StandardThickness)
          {
            left = VisualConfiguration.Instance.GetStandardThicknessValue((this.Iterator.Margin as StandardThickness).Left);
            top = VisualConfiguration.Instance.GetStandardThicknessValue((this.Iterator.Margin as StandardThickness).Top);
            right = VisualConfiguration.Instance.GetStandardThicknessValue((this.Iterator.Margin as StandardThickness).Right);
            bottom = VisualConfiguration.Instance.GetStandardThicknessValue((this.Iterator.Margin as StandardThickness).Bottom);
          }
          this.Margin = new Thickness(left, top, right, bottom);
        }
        if (this.Iterator.ItemTemplate == null)
          return;
        GridUtil.PlaceElement(this.Grid, ControlFactory.ConstructLayoutElementControl(this.Iterator.ItemTemplate), 0, 0);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Iterator Control UI initialization failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      this.ReloadView();
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      this.ReloadView();
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Paste");
      this.AddContextMenuItem("Cut");
      this.AddContextMenuItem("Copy");
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == (object) "Delete")
      {
        this.Iterator.Parent.RemoveModelObject((IModelObject) this.Iterator, false);
        this.Iterator.RemoveModelObject((IModelObject) this.Iterator, false);
        if (DisplayMessage.Show(Resource.ConfirmDataListDelete, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes || this.Iterator.BoundList == null)
          return;
        (this.Iterator.BoundList.Parent as IDataStructure).DeleteDataField((IBaseDataElementType) this.Iterator.BoundList, false, false, false);
      }
      else if (source.Header == (object) "Paste")
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyPasteOperation();
      else if (source.Header == (object) "Cut")
      {
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCutOperation();
      }
      else
      {
        if (source.Header != (object) "Copy")
          return;
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCopyOperation();
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      this.ReloadView();
    }

    public override string SelectionText
    {
      get
      {
        return "Iterator Control";
      }
    }
  }
}
