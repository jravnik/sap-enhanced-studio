﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls.VisualLayoutGridItem
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls
{
  public class VisualLayoutGridItem : BaseBorderControl
  {
    internal LayoutGrid ParentLayoutGrid
    {
      get
      {
        return this.LayoutGridItem.Parent as LayoutGrid;
      }
    }

    internal LayoutGridItem LayoutGridItem
    {
      get
      {
        return this.m_ModelObject as LayoutGridItem;
      }
    }

    public override string SelectionText
    {
      get
      {
        return this.LayoutGridItem.TypeName;
      }
    }

    public VisualLayoutGridItem(IModelObject layoutGridItem)
      : base(layoutGridItem)
    {
      this.IsDraggable = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.IsSpanable = true;
      this.AllowDrop = true;
      this.InitializeSpanningDetails();
      this.BorderBrush = (Brush) Brushes.Purple;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.MinHeight = (double) (30 * this.LayoutGridItem.RowSpan);
        this.MinWidth = (double) (150 * this.LayoutGridItem.ColumnSpan);
        if (this.LayoutGridItem.LayoutElement == null)
          return;
        GridUtil.PlaceElement(this.Grid, ControlFactory.ConstructLayoutElementControl(this.LayoutGridItem.LayoutElement), 0, 0);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Layout Grid Control UI initialization failed.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == (object) "Delete")
      {
        this.LayoutGridItem.Parent.RemoveModelObject((IModelObject) this.LayoutGridItem);
      }
      else
      {
        if (source.Header != (object) "Paste")
          return;
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyPasteOperation();
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Delete");
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Paste");
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the newly added Pane Containers.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the removed Pane Containers.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to change the Property change.", ex));
      }
    }

    protected override void OnSpanDownArrowClicked(object sender, EventArgs e)
    {
      try
      {
        base.OnSpanDownArrowClicked(sender, e);
        this.ParentLayoutGrid.IncrementRowSpan((IContainer) this.LayoutGridItem);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnSpanLeftArrowClicked(object sender, EventArgs e)
    {
      try
      {
        base.OnSpanLeftArrowClicked(sender, e);
        this.ParentLayoutGrid.DecrementColSpan((IContainer) this.LayoutGridItem);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnSpanRightArrowClicked(object sender, EventArgs e)
    {
      try
      {
        base.OnSpanRightArrowClicked(sender, e);
        this.ParentLayoutGrid.IncrementColSpan((IContainer) this.LayoutGridItem);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnSpanUpArrowClicked(object sender, EventArgs e)
    {
      try
      {
        base.OnSpanUpArrowClicked(sender, e);
        this.ParentLayoutGrid.DecrementRowSpan((IContainer) this.LayoutGridItem);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnBaseVisualControlDragOver(DragEventArgs e, IModelObject droppedModel)
    {
      if (this.LayoutGridItem.LayoutElement != null)
      {
        e.Effects = DragDropEffects.None;
      }
      else
      {
        if (!(droppedModel is LayoutElement))
          return;
        e.Effects = DragDropEffects.Copy;
      }
    }

    public void InitializeSpanningDetails()
    {
      this.InitializeSpanningDetails(new SpanningInfo(this.ParentLayoutGrid.ColumnDefinitions.Count, this.ParentLayoutGrid.RowDefinitions.Count, this.LayoutGridItem.Column, this.LayoutGridItem.Row, this.LayoutGridItem.ColumnSpan, this.LayoutGridItem.RowSpan), SpanningStyles.both);
    }
  }
}
