﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls.VisualLayoutGrid
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls
{
  public class VisualLayoutGrid : BaseBorderControl
  {
    internal LayoutGrid LayoutGrid
    {
      get
      {
        return this.m_ModelObject as LayoutGrid;
      }
    }

    public override string SelectionText
    {
      get
      {
        return this.LayoutGrid.TypeName;
      }
    }

    public VisualLayoutGrid(IModelObject layoutGrid)
      : base(layoutGrid)
    {
      this.IsDraggable = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.BorderBrush = (Brush) Brushes.Blue;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        Grid grid = new Grid();
        this.SetMargin();
        foreach (OberonRowDefinition rowDefinition in this.LayoutGrid.RowDefinitions)
        {
          GridUnitType unitType = (GridUnitType) Enum.Parse(typeof (GridUnitType), rowDefinition.Type.ToString(), true);
          double height = rowDefinition.Height > 0.0 ? rowDefinition.Height : 1.0;
          GridUtil.AddRowDef(grid, height, 1.0, unitType);
        }
        foreach (OberonColumnDefinition columnDefinition in this.LayoutGrid.ColumnDefinitions)
        {
          GridUnitType unitType = (GridUnitType) Enum.Parse(typeof (GridUnitType), columnDefinition.Type.ToString(), true);
          double num = columnDefinition.Width > 0.0 ? columnDefinition.Width : 1.0;
          GridUtil.AddColumnDef(grid, num, unitType);
        }
        if (this.LayoutGrid.Items != null)
        {
          foreach (LayoutGridItem layoutGridItem in this.LayoutGrid.Items)
          {
            VisualLayoutGridItem visualLayoutGridItem = new VisualLayoutGridItem((IModelObject) layoutGridItem);
            GridUtil.PlaceElement(grid, (UIElement) visualLayoutGridItem, layoutGridItem.Row, layoutGridItem.Column, layoutGridItem.RowSpan, layoutGridItem.ColumnSpan);
          }
        }
        GridUtil.PlaceElement(this.Grid, (UIElement) grid, 0, 0);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Layout Grid Control UI initialization failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Cut");
      this.AddContextMenuItem("Copy");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == (object) "Delete")
        this.LayoutGrid.Parent.RemoveModelObject((IModelObject) this.LayoutGrid, false);
      else if (source.Header == (object) "Cut")
      {
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCutOperation();
      }
      else
      {
        if (source.Header != (object) "Copy")
          return;
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCopyOperation();
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the newly added Pane Containers.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the removed Pane Containers.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        if (e.PropertyName == LayoutGrid.RowDefinitionsProperty || e.PropertyName == LayoutGrid.ColumnDefinitionsProperty || e.PropertyName == LayoutGrid.ItemsProperty)
        {
          this.ReloadView();
        }
        else
        {
          if (!(e.PropertyName == LayoutElement.MarginProperty) && !(e.PropertyName == LayoutElement.MarginTypeProperty))
            return;
          this.SetMargin();
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to change the Property change.", ex));
      }
    }

    private void SetMargin()
    {
      double left = ComponentModelersConstants.CONTROL_BORDER_MARGIN.Left;
      double top = ComponentModelersConstants.CONTROL_BORDER_MARGIN.Top;
      double right = ComponentModelersConstants.CONTROL_BORDER_MARGIN.Right;
      double bottom = ComponentModelersConstants.CONTROL_BORDER_MARGIN.Bottom;
      if (this.LayoutGrid.Margin == null)
        return;
      if (this.LayoutGrid.Margin is StandardThickness)
      {
        left = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutGrid.Margin as StandardThickness).Left);
        top = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutGrid.Margin as StandardThickness).Top);
        right = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutGrid.Margin as StandardThickness).Right);
        bottom = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutGrid.Margin as StandardThickness).Bottom);
      }
      this.Margin = new Thickness(left, top, right, bottom);
    }
  }
}
