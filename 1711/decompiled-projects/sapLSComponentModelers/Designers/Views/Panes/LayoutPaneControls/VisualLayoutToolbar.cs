﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls.VisualLayoutToolbar
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Menu;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls
{
  public class VisualLayoutToolbar : VisualToolbar
  {
    internal LayoutToolbar LayoutToolbar
    {
      get
      {
        return this.m_ModelObject as LayoutToolbar;
      }
    }

    public VisualLayoutToolbar(IModelObject layoutToolbar)
      : base(layoutToolbar)
    {
      this.m_ModelObject.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      this.LayoutToolbar.Toolbar.ModelObjectRemoved += new ModelRemovedEventHandler(this.Toolbar_ModelObjectRemoved);
      this.LayoutToolbar.Toolbar.ModelObjectAdded += new ModelAddedEventHandler(this.Toolbar_ModelObjectAdded);
    }

    public override void LoadView()
    {
      try
      {
        this.Padding = new Thickness(1.0);
        this.SetMargin();
        Grid itemGrpGrid = new Grid();
        WrapPanel wrapPanel = new WrapPanel();
        this.Grid.Background = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.PlaceElement(this.Grid, (UIElement) wrapPanel, 0, 0);
        wrapPanel.ClipToBounds = true;
        wrapPanel.Children.Clear();
        Toolbar toolbar = (Toolbar) this.LayoutToolbar.Toolbar;
        if (toolbar != null && toolbar.ButtonGroups != null && toolbar.ButtonGroups.Count > 0)
        {
          int count = toolbar.ButtonGroups.Count;
          int index = 1;
          foreach (ButtonGroup buttonGroup in toolbar.ButtonGroups)
          {
            if (buttonGroup != null && buttonGroup.Buttons.Count > 0)
            {
              VisualButtonGroup visualButtonGroup = new VisualButtonGroup(buttonGroup);
              wrapPanel.Children.Add((UIElement) visualButtonGroup);
            }
            if (count != index && toolbar.ButtonGroups[index] != null && (toolbar.ButtonGroups[index].Buttons.Count > 0 && wrapPanel.Children.Count > 0))
              this.LoadToolbarSeperatorImage();
            ++index;
          }
        }
        if (toolbar == null || toolbar.ItemGroups == null || toolbar.ItemGroups.Count <= 0)
          return;
        int count1 = toolbar.ItemGroups.Count;
        List<ItemGroup> leftAlignedItmGrps = new List<ItemGroup>();
        List<ItemGroup> rightAlignedItmGrps = new List<ItemGroup>();
        List<ItemGroup> centerAlignedItmGrps = new List<ItemGroup>();
        List<ItemGroup> stretchAlignedItmGrps = new List<ItemGroup>();
        if (count1 > 0)
          this.LoadToolbarSeperatorImage();
        foreach (ItemGroup itemGroup in toolbar.ItemGroups)
        {
          if (itemGroup != null && itemGroup.Alignment == HorizontalAlignmentType.left)
            leftAlignedItmGrps.Add(itemGroup);
          else if (itemGroup != null && itemGroup.Alignment == HorizontalAlignmentType.right)
            rightAlignedItmGrps.Add(itemGroup);
          else if (itemGroup != null && itemGroup.Alignment == HorizontalAlignmentType.center)
            centerAlignedItmGrps.Add(itemGroup);
          else if (itemGroup != null && itemGroup.Alignment == HorizontalAlignmentType.stretch)
            stretchAlignedItmGrps.Add(itemGroup);
        }
        this.AddAllignedItemGroups(itemGrpGrid, leftAlignedItmGrps, rightAlignedItmGrps, centerAlignedItmGrps, stretchAlignedItmGrps);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Toolbar initialization failed.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        if (e.PropertyName == LayoutElement.MarginProperty || e.PropertyName == LayoutElement.MarginTypeProperty || e.PropertyName == LayoutElement.ThicknessProperty)
        {
          this.SetMargin();
        }
        else
        {
          this.ReloadView();
          if (e.PropertyName == LayoutToolbar.ToolbarProperty)
            this.LayoutToolbar.Toolbar.ModelObjectRemoved += new ModelRemovedEventHandler(this.Toolbar_ModelObjectRemoved);
        }
        base.OnModelPropertyChanged(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on Model Property changed event.", ex));
      }
    }

    private void Toolbar_ModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      this.ReloadView();
    }

    private void Toolbar_ModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      this.ReloadView();
    }

    private void AddAllignedItemGroups(Grid itemGrpGrid, List<ItemGroup> leftAlignedItmGrps, List<ItemGroup> rightAlignedItmGrps, List<ItemGroup> centerAlignedItmGrps, List<ItemGroup> stretchAlignedItmGrps)
    {
      GridUtil.AddRowDef(itemGrpGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(itemGrpGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(itemGrpGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(itemGrpGrid, 1.0, GridUnitType.Auto);
      WrapPanel wrapPanel1 = new WrapPanel();
      WrapPanel wrapPanel2 = new WrapPanel();
      WrapPanel wrapPanel3 = new WrapPanel();
      WrapPanel wrapPanel4 = new WrapPanel();
      foreach (IModelObject leftAlignedItmGrp in leftAlignedItmGrps)
      {
        VisualItemGroup visualItemGroup = new VisualItemGroup(leftAlignedItmGrp, (BaseVisualControl) this);
        wrapPanel1.HorizontalAlignment = HorizontalAlignment.Left;
        wrapPanel1.Children.Add((UIElement) visualItemGroup);
      }
      GridUtil.PlaceElement(itemGrpGrid, (UIElement) wrapPanel1, 0, 0);
      foreach (IModelObject rightAlignedItmGrp in rightAlignedItmGrps)
      {
        VisualItemGroup visualItemGroup = new VisualItemGroup(rightAlignedItmGrp, (BaseVisualControl) this);
        wrapPanel2.Children.Add((UIElement) visualItemGroup);
        wrapPanel2.HorizontalAlignment = HorizontalAlignment.Right;
      }
      GridUtil.PlaceElement(itemGrpGrid, (UIElement) wrapPanel2, 0, 2);
      foreach (IModelObject centerAlignedItmGrp in centerAlignedItmGrps)
      {
        VisualItemGroup visualItemGroup = new VisualItemGroup(centerAlignedItmGrp, (BaseVisualControl) this);
        wrapPanel3.Children.Add((UIElement) visualItemGroup);
        wrapPanel3.HorizontalAlignment = HorizontalAlignment.Center;
      }
      GridUtil.PlaceElement(itemGrpGrid, (UIElement) wrapPanel3, 0, 1);
      foreach (IModelObject stretchAlignedItmGrp in stretchAlignedItmGrps)
      {
        VisualItemGroup visualItemGroup = new VisualItemGroup(stretchAlignedItmGrp, (BaseVisualControl) this);
        wrapPanel4.Children.Add((UIElement) visualItemGroup);
        wrapPanel4.HorizontalAlignment = HorizontalAlignment.Center;
      }
      GridUtil.PlaceElement(itemGrpGrid, (UIElement) wrapPanel4, 0, 1);
      GridUtil.PlaceElement(this.Grid, (UIElement) itemGrpGrid, 0, 1);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) "Delete")
        return;
      this.LayoutToolbar.Parent.RemoveModelObject((IModelObject) this.LayoutToolbar);
    }

    private void SetMargin()
    {
      double left = ComponentModelersConstants.CONTROL_BORDER_MARGIN.Left;
      double top = ComponentModelersConstants.CONTROL_BORDER_MARGIN.Top;
      double right = ComponentModelersConstants.CONTROL_BORDER_MARGIN.Right;
      double bottom = ComponentModelersConstants.CONTROL_BORDER_MARGIN.Bottom;
      if (this.LayoutToolbar.Margin == null)
        return;
      if (this.LayoutToolbar.Margin is StandardThickness)
      {
        left = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutToolbar.Margin as StandardThickness).Left);
        top = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutToolbar.Margin as StandardThickness).Top);
        right = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutToolbar.Margin as StandardThickness).Right);
        bottom = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutToolbar.Margin as StandardThickness).Bottom);
      }
      this.Margin = new Thickness(left, top, right, bottom);
    }
  }
}
