﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualMapPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualMapPane : VisualBasePane, IContextMenuContainer
  {
    public VisualMapPane(IModelObject model)
      : base(model)
    {
      this.LoadView();
    }

    protected MapPane MapPane
    {
      get
      {
        return this.m_ModelObject as MapPane;
      }
    }

    public override void ConstructBodyElement()
    {
      Image image = ResourceUtil.GetImage("sapLSUISkins;component/dt.images/PNG/MapPane.png,100,100");
      image.HorizontalAlignment = HorizontalAlignment.Stretch;
      image.VerticalAlignment = VerticalAlignment.Stretch;
      GridUtil.PlaceElement(this.Grid, (UIElement) image, 0, 0);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      try
      {
        if (source.Header == null || !Convert.ToString(source.Header).Equals("Delete") || this.MapPane.Parent == null)
          return;
        this.MapPane.Parent.RemoveModelObject((IModelObject) this.MapPane);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }
  }
}
