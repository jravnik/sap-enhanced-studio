﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.NonFocusButton
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public partial class NonFocusButton : UserControl, IComponentConnector
  {
    private static readonly Brush borderBrush = (Brush) new SolidColorBrush(ColorTranslator.FromHtml("#8A95A1"));
    private static readonly Color stdBaseTopColor = ColorTranslator.FromHtml("#EEEFF0");
    private static readonly Color stdBaseBottomColor = ColorTranslator.FromHtml("#D8DDE2");
    private static readonly Color stdButtonTopColor = ColorTranslator.FromHtml("#E1E4E8");
    private static readonly Color stdButtonMiddleColor = ColorTranslator.FromHtml("#D0D6DC");
    private static readonly Color stdButtonBottomColor = ColorTranslator.FromHtml("#D1D6DC");
    private static readonly Color hoverBaseTopColor = ColorTranslator.FromHtml("#F4F5F6");
    private static readonly Color hoverBaseBottomColor = ColorTranslator.FromHtml("#DAE0E5");
    private static readonly Color hoverButtonTopColor = ColorTranslator.FromHtml("#D7DCE1");
    private static readonly Color hoverButtonMiddleColor = ColorTranslator.FromHtml("#E1E6EA");
    private static readonly Color hoverButtonBottomColor = ColorTranslator.FromHtml("#EEEFF0");
    private static readonly Color downBaseTopColor = ColorTranslator.FromHtml("#BBC3CB");
    private static readonly Color downBaseBottomColor = ColorTranslator.FromHtml("#EBEDEF");
    private static readonly Color downButtonTopColor = ColorTranslator.FromHtml("#A3AEB8");
    private static readonly Color downButtonMiddleColor = ColorTranslator.FromHtml("#A3AEB8");
    private static readonly Color downButtonBottomColor = ColorTranslator.FromHtml("#BAC2CA");
    private const string SETTING_NAME_FONT = "Arial,11,Normal";
    private readonly Rectangle baseRectangle;
    private readonly Rectangle buttonRectangle;
    private readonly TextBlock textBlock;
    private readonly TextBlock mandatoryTextBlock;
    private GradientStop baseTopGradient;
    private GradientStop baseBottomGradient;
    private GradientStop buttonTopGradient;
    private GradientStop buttonMiddleGradient;
    private GradientStop buttonBottomGradient;
    private bool hover;
    private bool enabled;
    private bool down;
    private bool pressed;
    private string textProperty;
    //internal Grid LayoutRoot;
    //internal Rectangle BaseRectangle;
    //internal GradientStop BaseGradientTop;
    //internal GradientStop BaseGradientBottom;
    //internal Rectangle ButtonRectangle;
    //internal GradientStop ButtonGradientTop;
    //internal GradientStop ButtonGradientBottom;
    //internal GradientStop ButtonGradientMiddle;
    //internal TextBlock Content;
    //internal TextBlock Mandatory;
    //private bool _contentLoaded;

    public event EventHandler<EventArgs> ButtonClick;

    public NonFocusButton(bool hasBorder, string textProperty, bool mandatory)
    {
      this.enabled = true;
      this.InitializeComponent();
      this.textProperty = textProperty;
      this.baseRectangle = (Rectangle) this.FindName(nameof (BaseRectangle));
      this.buttonRectangle = (Rectangle) this.FindName(nameof (ButtonRectangle));
      if (hasBorder)
      {
        this.baseRectangle.Stroke = NonFocusButton.borderBrush;
        this.baseRectangle.StrokeThickness = 1.0;
        this.buttonRectangle.Margin = new Thickness(2.0, 2.0, 2.0, 4.0);
      }
      else
      {
        this.baseRectangle.StrokeThickness = 0.0;
        this.buttonRectangle.Margin = new Thickness(1.0, 1.0, 1.0, 3.0);
      }
      this.textBlock = (TextBlock) this.FindName(nameof (Content));
      this.textBlock.Margin = new Thickness(3.0, 3.0, 3.0, 3.0);
      FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.textBlock);
      if (mandatory)
      {
        this.mandatoryTextBlock = (TextBlock) this.FindName(nameof (Mandatory));
        this.textBlock.Margin = new Thickness(0.0, 3.0, 3.0, 3.0);
        FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.mandatoryTextBlock);
        this.mandatoryTextBlock.Foreground = (Brush) Brushes.Red;
        this.mandatoryTextBlock.Visibility = Visibility.Visible;
      }
      this.baseTopGradient = (GradientStop) this.FindName(nameof (BaseGradientTop));
      this.baseBottomGradient = (GradientStop) this.FindName(nameof (BaseGradientBottom));
      this.buttonTopGradient = (GradientStop) this.FindName(nameof (ButtonGradientTop));
      this.buttonMiddleGradient = (GradientStop) this.FindName(nameof (ButtonGradientMiddle));
      this.buttonBottomGradient = (GradientStop) this.FindName(nameof (ButtonGradientBottom));
      this.MouseEnter += new MouseEventHandler(this.NonFocusButton_MouseEnter);
      this.MouseLeave += new MouseEventHandler(this.NonFocusButton_MouseLeave);
      this.MouseLeftButtonDown += new MouseButtonEventHandler(this.NonFocusButton_MouseLeftButtonDown);
      this.MouseLeftButtonUp += new MouseButtonEventHandler(this.NonFocusButton_MouseLeftButtonUp);
      this.UpdateText();
      this.UpdateControlStyle();
    }

    private void UpdateText()
    {
      if (this.textProperty != null && this.textProperty != string.Empty)
        this.textBlock.Text = this.textProperty;
      else
        this.textBlock.Text = "";
    }

    private void NonFocusButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
      this.down = false;
      this.UpdateControlStyle();
      if (this.ButtonClick == null)
        return;
      this.ButtonClick((object) this, EventArgs.Empty);
    }

    private void NonFocusButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      this.down = true;
      this.UpdateControlStyle();
    }

    private void NonFocusButton_MouseLeave(object sender, MouseEventArgs e)
    {
      this.hover = false;
      this.UpdateControlStyle();
    }

    private void NonFocusButton_MouseEnter(object sender, MouseEventArgs e)
    {
      this.hover = true;
      this.UpdateControlStyle();
    }

    public string Text
    {
      get
      {
        return this.textBlock.Text;
      }
      set
      {
        this.textBlock.Text = value;
      }
    }

    public new bool IsEnabled
    {
      get
      {
        return this.enabled;
      }
      set
      {
        this.enabled = value;
        this.UpdateControlStyle();
      }
    }

    public bool IsPressed
    {
      get
      {
        return this.pressed;
      }
      set
      {
        this.pressed = false;
        this.UpdateControlStyle();
      }
    }

    public void Terminate()
    {
      if (this.textProperty == null)
        return;
      this.textProperty = (string) null;
    }

    private void UpdateControlStyle()
    {
      if (this.enabled && (this.down || this.pressed))
      {
        this.baseTopGradient.Color = NonFocusButton.downBaseTopColor;
        this.baseBottomGradient.Color = NonFocusButton.downBaseBottomColor;
        this.buttonTopGradient.Color = NonFocusButton.downButtonTopColor;
        this.buttonMiddleGradient.Color = NonFocusButton.downButtonMiddleColor;
        this.buttonBottomGradient.Color = NonFocusButton.downButtonBottomColor;
      }
      else if (this.enabled && this.hover)
      {
        this.baseTopGradient.Color = NonFocusButton.hoverBaseTopColor;
        this.baseBottomGradient.Color = NonFocusButton.hoverBaseBottomColor;
        this.buttonTopGradient.Color = NonFocusButton.hoverButtonTopColor;
        this.buttonMiddleGradient.Color = NonFocusButton.hoverButtonMiddleColor;
        this.buttonBottomGradient.Color = NonFocusButton.hoverButtonBottomColor;
      }
      else
      {
        this.baseTopGradient.Color = NonFocusButton.stdBaseTopColor;
        this.baseBottomGradient.Color = NonFocusButton.stdBaseBottomColor;
        this.buttonTopGradient.Color = NonFocusButton.stdButtonTopColor;
        this.buttonMiddleGradient.Color = NonFocusButton.stdButtonMiddleColor;
        this.buttonBottomGradient.Color = NonFocusButton.stdButtonBottomColor;
      }
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/designers/views/panes/listpanecontrols/nonfocusbutton.xaml", UriKind.Relative));
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.LayoutRoot = (Grid) target;
    //      break;
    //    case 2:
    //      this.BaseRectangle = (Rectangle) target;
    //      break;
    //    case 3:
    //      this.BaseGradientTop = (GradientStop) target;
    //      break;
    //    case 4:
    //      this.BaseGradientBottom = (GradientStop) target;
    //      break;
    //    case 5:
    //      this.ButtonRectangle = (Rectangle) target;
    //      break;
    //    case 6:
    //      this.ButtonGradientTop = (GradientStop) target;
    //      break;
    //    case 7:
    //      this.ButtonGradientBottom = (GradientStop) target;
    //      break;
    //    case 8:
    //      this.ButtonGradientMiddle = (GradientStop) target;
    //      break;
    //    case 9:
    //      this.Content = (TextBlock) target;
    //      break;
    //    case 10:
    //      this.Mandatory = (TextBlock) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
