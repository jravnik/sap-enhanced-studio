﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.VisualColumnHeader
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public class VisualColumnHeader : UserControl
  {
    public static Brush BACKGROUND_COLOR = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 212, (byte) 217, (byte) 222));
    private TextDependentProperty ColumnLabel;
    private Grid contentGrid;
    private TextBlock headerText;

    public VisualColumnHeader(TextDependentProperty label)
    {
      this.contentGrid = new Grid();
      this.ColumnLabel = label;
      this.contentGrid.Background = VisualColumnHeader.BACKGROUND_COLOR;
      this.contentGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.headerText = new TextBlock();
      this.headerText.TextWrapping = TextWrapping.Wrap;
      this.headerText.VerticalAlignment = VerticalAlignment.Center;
      this.headerText.Margin = new Thickness(3.0, 3.0, 3.0, 3.0);
      this.contentGrid.Children.Add((UIElement) this.headerText);
      this.Content = (object) this.contentGrid;
      FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.headerText);
      if (label == null)
        return;
      this.UpdateText();
    }

    private void UpdateText()
    {
      string str = (string) null;
      if (this.ColumnLabel != null)
        str = this.ColumnLabel.Text;
      if (!(str != this.headerText.Text))
        return;
      this.headerText.Text = str;
    }
  }
}
