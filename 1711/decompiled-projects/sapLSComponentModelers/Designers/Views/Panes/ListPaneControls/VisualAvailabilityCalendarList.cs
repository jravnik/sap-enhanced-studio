﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.VisualAvailabilityCalendarList
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public class VisualAvailabilityCalendarList : VisualList
  {
    private AvailabilityCalendarList AvailabilityCalendarList
    {
      get
      {
        return this.m_ModelObject as AvailabilityCalendarList;
      }
    }

    public override string SelectionText
    {
      get
      {
        return "AvailabilityCalendarList";
      }
    }

    public VisualAvailabilityCalendarList(IModelObject model)
      : base(model)
    {
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
    }
  }
}
