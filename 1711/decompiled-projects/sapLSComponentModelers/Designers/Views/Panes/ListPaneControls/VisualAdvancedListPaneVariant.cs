﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.VisualAdvancedListPaneVariant
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public class VisualAdvancedListPaneVariant : BaseSelectableControl, IContextMenuContainer
  {
    private InitFormType m_InitForm = InitFormType.advanced;
    private int index;
    private AdvancedListPane m_ListPane;
    private FindFormPane m_FFModel;
    private VisualFindFormPane m_FindFormPane;
    private VisualFindFormToolbar m_ffToolbar;
    private VisualToolbar m_Toolbar;
    private VisualList m_List;
    private VisualAlternateListVisualization m_AlternateVisual;
    private VisualLayout m_AlternateDetailsVisual;
    private VisualLayout m_PreviewArea;
    private AlternativeListVisualization alVisualization;
    private VisualToolbar m_IntegratedToolbar;

    public event UseToolBarChanged OnUseToolBarChanged;

    private AdvancedListPaneVariant ListPaneVariant
    {
      get
      {
        return this.m_ModelObject as AdvancedListPaneVariant;
      }
    }

    public VisualToolbar IntegratedToolbar
    {
      get
      {
        return this.m_IntegratedToolbar;
      }
      set
      {
        this.m_IntegratedToolbar = value;
      }
    }

    public VisualAdvancedListPaneVariant(IModelObject model, FindFormPane ffp, InitFormType initForm, IModelObject parent, AlternativeListVisualization alVisual, VisualToolbar integratedToolbar)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_ModelObject = model;
      this.m_ListPane = parent as AdvancedListPane;
      this.m_FFModel = ffp;
      this.m_InitForm = initForm;
      this.alVisualization = alVisual;
      this.m_IntegratedToolbar = integratedToolbar;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.index = 0;
        base.LoadView();
        this.Content = (object) this.Grid;
        if (this.m_InitForm == InitFormType.advanced && this.m_FFModel != null)
        {
          this.CreateFindForm(this.m_FFModel);
        }
        else
        {
          GridUtil.AddRowDef(this.Grid, 200.0, GridUnitType.Star);
          GridUtil.AddRowDef(this.Grid, 200.0, GridUnitType.Auto);
          this.index = 2;
        }
        if (this.ListPaneVariant == null)
          return;
        if (this.ListPaneVariant.ListDefinition.ListHeader != null)
          this.CreateListHeader(this.ListPaneVariant.ListDefinition);
        if (this.alVisualization == null)
        {
          if (this.ListPaneVariant.UseToolbar && !this.m_ListPane.UseIntegratedToolbar)
            this.CreateToolBar(this.ListPaneVariant);
          this.CreateListDefinition(this.ListPaneVariant);
          if (this.ListPaneVariant.PreviewArea == null)
            return;
          this.CreatePreviewArea(this.ListPaneVariant);
        }
        else
        {
          if (this.alVisualization.UseStandardToolbar && this.ListPaneVariant.UseToolbar && !this.m_ListPane.UseIntegratedToolbar)
            this.CreateToolBar(this.ListPaneVariant);
          this.CreateAlternateVisualization(this.ListPaneVariant, this.alVisualization);
          if (!this.alVisualization.UseStandardDetailsArea || this.ListPaneVariant.PreviewArea == null)
            return;
          this.CreatePreviewArea(this.ListPaneVariant);
        }
      }
      catch
      {
      }
    }

    private void CreateFindForm(FindFormPane ffp)
    {
      VisualFindFormPane visualFindFormPane = new VisualFindFormPane((IModelObject) ffp);
      visualFindFormPane.MinHeight = 80.0;
      this.m_FindFormPane = visualFindFormPane;
      visualFindFormPane.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      VisualFindFormToolbar visualFindFormToolbar = new VisualFindFormToolbar((IModelObject) ffp);
      this.m_ffToolbar = visualFindFormToolbar;
      visualFindFormToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      if (this.Grid.Children.Count == 0)
      {
        GridUtil.AddRowDef(this.Grid, 200.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 200.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) visualFindFormPane, this.index++, 0);
        GridUtil.PlaceElement(this.Grid, (UIElement) visualFindFormToolbar, this.index++, 0);
      }
      else
      {
        GridUtil.InsertElement(this.Grid, (UIElement) visualFindFormPane, 0, 0, 1, 1, 0);
        GridUtil.InsertElement(this.Grid, (UIElement) visualFindFormToolbar, 1, 0, 1, 1, 1);
      }
    }

    private void CreateListHeader(List list)
    {
      if (list == null || list.ListHeader == null || string.IsNullOrEmpty(list.ListHeader.Text))
        return;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      TextBlock textBlock = new TextBlock();
      textBlock.Text = list.ListHeader.Text;
      textBlock.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) textBlock, this.index++, 0);
    }

    private void CreateToolBar(AdvancedListPaneVariant lpv)
    {
      if (lpv.Toolbar == null)
        return;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      VisualToolbar visualToolbar = new VisualToolbar((IModelObject) lpv.Toolbar);
      this.m_Toolbar = visualToolbar;
      visualToolbar.VerticalAlignment = VerticalAlignment.Top;
      visualToolbar.HorizontalAlignment = HorizontalAlignment.Stretch;
      visualToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      visualToolbar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      GridUtil.PlaceElement(this.Grid, (UIElement) visualToolbar, this.index++, 0);
    }

    private void CreatePreviewArea(AdvancedListPaneVariant lpv)
    {
      GridUtil.AddRowDef(this.Grid, 300.0, GridUnitType.Star);
      VisualLayout visualLayout = new VisualLayout((IModelObject) lpv.PreviewArea);
      this.m_PreviewArea = visualLayout;
      visualLayout.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) visualLayout, this.index++, 0);
    }

    private void CreateListDefinition(AdvancedListPaneVariant lpv)
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      UserControl userControl = (UserControl) new VisualList((IModelObject) lpv.ListDefinition);
      this.m_List = userControl as VisualList;
      userControl.VerticalAlignment = VerticalAlignment.Top;
      userControl.HorizontalAlignment = HorizontalAlignment.Stretch;
      userControl.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) userControl, this.index++, 0);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      DisplayTextControl displayTextControl = new DisplayTextControl(TextViewStyles.SectionGroupTitle, "During design time, the UI designer displays only one row in a list; during runtime, the system displays the number of rows you defined in the MaxVisibleRows property.");
      displayTextControl.Padding = new Thickness(5.0);
      displayTextControl.ToolTip = (object) "During design time, the UI designer displays only one row in a list; during runtime, the system displays the number of rows you defined in the MaxVisibleRows property.";
      GridUtil.PlaceElement(this.Grid, (UIElement) displayTextControl, this.index++, 0);
    }

    private void CreateAlternateVisualization(AdvancedListPaneVariant lpv, AlternativeListVisualization alv)
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      UserControl userControl = (UserControl) new VisualAlternateListVisualization(this.alVisualization, (IModelObject) lpv, this);
      this.m_AlternateVisual = userControl as VisualAlternateListVisualization;
      userControl.VerticalAlignment = VerticalAlignment.Top;
      userControl.HorizontalAlignment = HorizontalAlignment.Stretch;
      userControl.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) userControl, this.index++, 0);
    }

    private void UpdateFindFormPane()
    {
      if (this.Grid.Children.Count <= 0)
        return;
      if (!(this.Grid.Children[0] is VisualFindFormPane) && this.m_InitForm == InitFormType.advanced && this.m_FFModel != null)
      {
        this.CreateFindForm(this.m_FFModel);
      }
      else
      {
        if (this.m_FindFormPane == null)
          return;
        this.m_FindFormPane.Visibility = this.m_InitForm == InitFormType.advanced ? Visibility.Visible : Visibility.Collapsed;
        this.m_ffToolbar.Visibility = this.m_FindFormPane.Visibility;
      }
    }

    public VisualAdvancedListPaneVariant ReloadAdvancedListPaneVariant(IModelObject model, FindFormPane ffp, InitFormType initForm, AlternativeListVisualization alVisual, IModelObject parent)
    {
      this.m_ModelObject = model;
      this.m_FFModel = ffp;
      this.m_ListPane = parent as AdvancedListPane;
      this.alVisualization = alVisual;
      if (this.m_InitForm != initForm)
      {
        this.m_InitForm = initForm;
        this.UpdateFindFormPane();
      }
      else
      {
        this.m_InitForm = initForm;
        this.ReloadView();
      }
      IDesigner parentDesigner = (IDesigner) ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
      if (parentDesigner != null)
        parentDesigner.RaiseSelectionChanged((IDesignerControl) this, (IModelObject) this.ListPaneVariant);
      return this;
    }

    public void LoadAlternateVisual(AlternativeListVisualization alVisual)
    {
      this.ReloadAdvancedListPaneVariant((IModelObject) this.ListPaneVariant, this.m_FFModel, this.m_InitForm, alVisual, (IModelObject) this.m_ListPane);
    }

    public override string SelectionText
    {
      get
      {
        return "AdvancedListPaneVariant";
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == AdvancedListPaneVariant.UseToolBarProperty || e.PropertyName == AdvancedListPaneVariant.UsePreviewPaneProperty || (e.PropertyName == AdvancedListPaneVariant.SwitchToolBarProperty || e.PropertyName == AdvancedListPaneVariant.AlternativeListVisualizationProperty))
      {
        this.ReloadView();
        if (this.m_IntegratedToolbar == null)
          return;
        this.OnUseToolBarChanged(this.ListPaneVariant);
      }
      else if (this.m_IntegratedToolbar != null)
      {
        this.OnUseToolBarChanged(this.ListPaneVariant);
      }
      else
      {
        if (this.m_Toolbar == null)
          return;
        this.m_Toolbar.Toolbar = (Toolbar) this.ListPaneVariant.Toolbar;
        this.m_Toolbar.ReloadView();
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (this.m_Toolbar == null)
        return;
      this.m_Toolbar.HandleContextMenuItemClickWrapper(source);
    }

    protected override void AddContextMenuItems()
    {
      if (this.m_Toolbar == null)
        return;
      this.m_Toolbar.AddContextMenuItems((IContextMenuContainer) this);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
