﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.VisualFindFormToolbar
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public class VisualFindFormToolbar : BaseVisualControl
  {
    internal Button m_BtnGo;
    internal Button m_BtnSaveQueries;
    internal Button m_BtnOrganizeQueries;
    internal Button m_BtnClear;
    internal FormPane m_FormPane;

    public override string SelectionText
    {
      get
      {
        return string.Empty;
      }
    }

    public VisualFindFormToolbar(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_FormPane = model as FormPane;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.Content = (object) this.Grid;
        GridUtil.AddRowDef(this.Grid, 30.0, GridUnitType.Pixel);
        this.Grid.HorizontalAlignment = HorizontalAlignment.Stretch;
        this.Grid.VerticalAlignment = VerticalAlignment.Center;
        this.m_BtnGo = new Button();
        this.AddElement((FrameworkElement) this.m_BtnGo);
        this.m_BtnGo.Content = (object) "Go";
        this.m_BtnGo.HorizontalAlignment = HorizontalAlignment.Left;
        this.m_BtnGo.VerticalAlignment = VerticalAlignment.Center;
        this.m_BtnGo.Background = (Brush) SkinConstants.BRUSH_BUTTON_STANDARDBACKCOLOR;
        this.m_BtnGo.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
        this.m_BtnSaveQueries = new Button();
        this.AddElement((FrameworkElement) this.m_BtnSaveQueries);
        this.m_BtnSaveQueries.Content = (object) "Reset";
        this.m_BtnSaveQueries.HorizontalAlignment = HorizontalAlignment.Left;
        this.m_BtnSaveQueries.VerticalAlignment = VerticalAlignment.Center;
        this.m_BtnSaveQueries.Background = (Brush) SkinConstants.BRUSH_BUTTON_STANDARDBACKCOLOR;
        this.m_BtnSaveQueries.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
        this.m_BtnOrganizeQueries = new Button();
        this.AddElement((FrameworkElement) this.m_BtnOrganizeQueries);
        this.m_BtnOrganizeQueries.Content = (object) "Save Query";
        this.m_BtnOrganizeQueries.HorizontalAlignment = HorizontalAlignment.Left;
        this.m_BtnOrganizeQueries.VerticalAlignment = VerticalAlignment.Center;
        this.m_BtnOrganizeQueries.Background = (Brush) SkinConstants.BRUSH_BUTTON_STANDARDBACKCOLOR;
        this.m_BtnOrganizeQueries.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
        this.m_BtnClear = new Button();
        this.AddElement((FrameworkElement) this.m_BtnClear);
        this.m_BtnClear.Content = (object) "Organize Queries";
        this.m_BtnClear.HorizontalAlignment = HorizontalAlignment.Left;
        this.m_BtnClear.VerticalAlignment = VerticalAlignment.Center;
        this.m_BtnClear.Background = (Brush) SkinConstants.BRUSH_BUTTON_STANDARDBACKCOLOR;
        this.m_BtnClear.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
        this.Grid.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of FindFormToolbar control failed.", ex));
      }
    }

    private void AddElement(FrameworkElement element)
    {
      this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength()
      });
      this.Grid.Children.Add((UIElement) element);
      element.SetValue(Grid.ColumnProperty, (object) (this.Grid.ColumnDefinitions.Count - 1));
      element.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
    }
  }
}
