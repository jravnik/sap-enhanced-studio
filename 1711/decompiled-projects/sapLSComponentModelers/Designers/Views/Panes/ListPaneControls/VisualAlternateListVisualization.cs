﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.VisualAlternateListVisualization
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public class VisualAlternateListVisualization : BaseSelectableControl, IContextMenuContainer
  {
    private VisualLayout m_MasterVisualization;
    private VisualLayout m_DetailsVisualization;
    private AdvancedListPaneVariant m_ListPaneVariant;
    private AdvancedDataSetPaneVariant m_DataSetPaneVariant;
    private VisualAdvancedListPaneVariant m_VisualListParent;
    private VisualAdvancedDataSetPaneVariant m_VisualDataSetParent;
    private int index;

    private AlternativeListVisualization AlternateListVisualization
    {
      get
      {
        return this.m_ModelObject as AlternativeListVisualization;
      }
    }

    public VisualAlternateListVisualization(AlternativeListVisualization alVisual, IModelObject parent, VisualAdvancedListPaneVariant visualParent)
      : base((IModelObject) alVisual)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_ModelObject = (IModelObject) alVisual;
      this.m_VisualListParent = visualParent;
      this.m_ListPaneVariant = parent as AdvancedListPaneVariant;
      this.LoadView();
    }

    public VisualAlternateListVisualization(AlternativeListVisualization alVisual, IModelObject parent, VisualAdvancedDataSetPaneVariant visualParent)
      : base((IModelObject) alVisual)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_ModelObject = (IModelObject) alVisual;
      this.m_VisualDataSetParent = visualParent;
      this.m_DataSetPaneVariant = parent as AdvancedDataSetPaneVariant;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.index = 0;
        GridUtil.AddRowDef(this.Grid, 300.0, GridUnitType.Star);
        this.m_MasterVisualization = new VisualLayout((IModelObject) this.AlternateListVisualization.MasterVisualization);
        this.m_MasterVisualization.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.m_MasterVisualization, this.index++, 0);
        if (!this.AlternateListVisualization.UseDetailsVisualization || this.AlternateListVisualization.DetailVisualization == null)
          return;
        this.CreateAlternateDetailsVisualization(this.AlternateListVisualization);
      }
      catch
      {
      }
    }

    private void CreateAlternateDetailsVisualization(AlternativeListVisualization alv)
    {
      GridUtil.AddRowDef(this.Grid, 300.0, GridUnitType.Star);
      this.m_DetailsVisualization = new VisualLayout((IModelObject) alv.DetailVisualization);
      this.m_DetailsVisualization.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_DetailsVisualization, this.index++, 0);
    }

    public override string SelectionText
    {
      get
      {
        return "AlternativeListVisualization";
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == AlternativeListVisualization.UseStandardDetailsAreaProperty)
        this.m_VisualListParent.LoadAlternateVisual(this.AlternateListVisualization);
      else
        this.ReloadView();
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
