﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.VisualListColumn
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Oberon.Controller;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using SAP.BYD.LS.UIDesigner.UICore.Dialogs;
using SAP.BYD.LS.UIDesigner.UICore.OBNNavigationArea;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public class VisualListColumn : BaseSelectableControl
  {
    private static readonly Brush BRUSH_TABLEHEADER_SEPARATOR = (Brush) new SolidColorBrush(ColorTranslator.FromHtml("#9EA9B6"));
    private static readonly Brush BRUSH_TABLE_TOPHIGHLIGHT = (Brush) new SolidColorBrush(ColorTranslator.FromHtml("#F0F1F2"));
    private static readonly Brush BRUSH_TABLE_CELL = (Brush) Brushes.White;
    private static readonly double HEIGHT_STANDARD_ROW = 20.0;
    private static readonly double HEIGHT_STANDARD_COLUMNHEADER = 19.0;
    private static readonly double WIDTH_GRID_SEPERATOR = 1.0;
    private bool m_HasColumnHeaders = true;
    private const string CONTEXT_MENU_ADD = "Add";
    private const string CONTEXT_MENU_DELETE = "Delete";
    private const string CONTEXT_MENU_COLUMN = "Column";
    private const string CONETXT_MENU_LISTPANE = "ListPane";
    private const string CONTEXT_MENU_COMPOUNDFIELD = "CompoundField";
    private bool m_RowSelector;
    private NonFocusButton m_HeaderButton;

    private ListColumn ListColumn
    {
      get
      {
        return this.m_ModelObject as ListColumn;
      }
    }

    private List ParentList
    {
      get
      {
        if (this.ListColumn == null || !(this.ListColumn.Parent is List))
          return (List) null;
        return this.ListColumn.Parent as List;
      }
    }

    public VisualListColumn(IModelObject model, bool rowSelector)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.ControlBackground = (Brush) Brushes.White;
      this.m_RowSelector = rowSelector;
      if (this.ParentList != null)
        this.m_HasColumnHeaders = this.ParentList.ShowHeaderRow;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        if (this.ListColumn != null && this.ListColumn.Item != null)
          this.ListColumn.Item.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
        GridUtil.AddRowDef(this.Grid, VisualListColumn.HEIGHT_STANDARD_COLUMNHEADER, GridUnitType.Pixel);
        GridUtil.AddRowDef(this.Grid, VisualListColumn.WIDTH_GRID_SEPERATOR, GridUnitType.Pixel);
        GridUtil.AddRowDef(this.Grid, VisualListColumn.HEIGHT_STANDARD_ROW, GridUnitType.Pixel);
        GridUtil.AddRowDef(this.Grid, VisualListColumn.WIDTH_GRID_SEPERATOR, GridUnitType.Pixel);
        GridUtil.AddRowDef(this.Grid, VisualListColumn.WIDTH_GRID_SEPERATOR * 2.0, GridUnitType.Pixel);
        this.AddColumnHeaderButton();
        UIElement cellControl = this.GetCellControl(this.ListColumn);
        cellControl.SetValue(Grid.ColumnProperty, (object) 0);
        cellControl.SetValue(Grid.RowProperty, (object) 2);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of ListColumn Control failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "ListColumn";
      }
    }

    protected override void OnBOElementDragOver(System.Windows.DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == System.Windows.DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
      if (boElement != null && boElement.ParentQuery != null)
        e.Effects = System.Windows.DragDropEffects.None;
      if (boElement == null || boElement.ParentAction == null || this.m_ModelObject.Parent is ActionForm)
        return;
      e.Effects = System.Windows.DragDropEffects.None;
    }

    private UIElement GetCellControl(ListColumn listColumn)
    {
      if (listColumn.Item.GetType() == typeof (InputField))
      {
        System.Windows.Controls.TextBox textBox = new System.Windows.Controls.TextBox();
        textBox.IsReadOnly = true;
        textBox.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        textBox.VerticalAlignment = VerticalAlignment.Stretch;
        textBox.Margin = new Thickness(0.5, 0.5, 0.5, 0.5);
        this.Grid.Children.Add((UIElement) textBox);
        return (UIElement) textBox;
      }
      if (listColumn.Item.GetType() == typeof (DropDownListBox))
      {
        System.Windows.Controls.ComboBox comboBox = new System.Windows.Controls.ComboBox();
        comboBox.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        comboBox.VerticalAlignment = VerticalAlignment.Stretch;
        this.Grid.Children.Add((UIElement) comboBox);
        comboBox.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        return (UIElement) comboBox;
      }
      if (listColumn.Item.GetType() == typeof (SAP.BYD.LS.UIDesigner.Model.Entities.Controls.CheckBox))
      {
        System.Windows.Controls.Control control = (System.Windows.Controls.Control) new System.Windows.Controls.CheckBox();
        control.BorderThickness = new Thickness(1.0);
        this.Grid.Children.Add((UIElement) control);
        control.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        control.VerticalAlignment = VerticalAlignment.Center;
        control.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        return (UIElement) control;
      }
      if (listColumn.Item.GetType() == typeof (SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link))
      {
        VisualStaticText visualStaticText = new VisualStaticText(false, false, true, false, VisualStaticText.StaticTextFormats.Standard);
        visualStaticText.IsLink = true;
        visualStaticText.IsMenuVisible = false;
        visualStaticText.BorderThickness = new Thickness(0.0);
        this.Grid.Children.Add((UIElement) visualStaticText);
        visualStaticText.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        visualStaticText.VerticalAlignment = VerticalAlignment.Center;
        visualStaticText.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        visualStaticText.Text = "Link";
        return (UIElement) visualStaticText;
      }
      if (listColumn.Item.GetType() == typeof (CompoundField))
      {
        VisualCompoundField visualCompoundField = new VisualCompoundField(listColumn.Item as CompoundField, UsageType.cellRenderer);
        visualCompoundField.Background = (Brush) Brushes.Transparent;
        visualCompoundField.BorderThickness = new Thickness(0.0);
        this.Grid.Children.Add((UIElement) visualCompoundField);
        visualCompoundField.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        visualCompoundField.VerticalAlignment = VerticalAlignment.Stretch;
        visualCompoundField.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        return (UIElement) visualCompoundField;
      }
      if (listColumn.Item.GetType() == typeof (StaticText))
      {
        VisualStaticText visualStaticText = (listColumn.Item as StaticText).Format != FormatType.emphasized ? new VisualStaticText(false, false, false, false, VisualStaticText.StaticTextFormats.Standard) : new VisualStaticText(false, false, false, false, VisualStaticText.StaticTextFormats.Emphasized);
        visualStaticText.Text = "Static Text";
        visualStaticText.Background = (Brush) Brushes.Transparent;
        visualStaticText.BorderThickness = new Thickness(0.0);
        this.Grid.Children.Add((UIElement) visualStaticText);
        visualStaticText.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        visualStaticText.VerticalAlignment = VerticalAlignment.Center;
        visualStaticText.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        return (UIElement) visualStaticText;
      }
      if (listColumn.Item.GetType() == typeof (ObjectValueSelector))
      {
        if (listColumn.Item.CCTSType == UXCCTSTypes.date || listColumn.Item.CCTSType == UXCCTSTypes.datetime)
        {
          VisualCalendar visualCalendar = new VisualCalendar();
          visualCalendar.Background = (Brush) Brushes.Transparent;
          visualCalendar.BorderThickness = new Thickness(0.0);
          this.Grid.Children.Add((UIElement) visualCalendar);
          visualCalendar.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
          visualCalendar.VerticalAlignment = VerticalAlignment.Center;
          visualCalendar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
          return (UIElement) visualCalendar;
        }
        VisualObjectValueHelp visualObjectValueHelp = new VisualObjectValueHelp();
        visualObjectValueHelp.Background = (Brush) Brushes.Transparent;
        visualObjectValueHelp.BorderThickness = new Thickness(0.0);
        this.Grid.Children.Add((UIElement) visualObjectValueHelp);
        visualObjectValueHelp.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
        visualObjectValueHelp.VerticalAlignment = VerticalAlignment.Center;
        visualObjectValueHelp.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        return (UIElement) visualObjectValueHelp;
      }
      if (listColumn.Item.GetType() == typeof (SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button))
      {
        VisualButton visualButton = new VisualButton();
        visualButton.Content = (object) (listColumn.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Button).Text.Text;
        visualButton.Background = (Brush) Brushes.Transparent;
        visualButton.BorderThickness = new Thickness(0.0);
        this.Grid.Children.Add((UIElement) visualButton);
        visualButton.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        visualButton.VerticalAlignment = VerticalAlignment.Center;
        visualButton.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        return (UIElement) visualButton;
      }
      if (listColumn.Item.GetType() == typeof (TextEditControl))
      {
        VisualTextEditControl visualTextEditControl = new VisualTextEditControl((listColumn.Item as TextEditControl).Rows, true, true);
        visualTextEditControl.Background = (Brush) Brushes.Transparent;
        visualTextEditControl.BorderThickness = new Thickness(0.0);
        this.Grid.Children.Add((UIElement) visualTextEditControl);
        visualTextEditControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        visualTextEditControl.VerticalAlignment = VerticalAlignment.Center;
        visualTextEditControl.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        return (UIElement) visualTextEditControl;
      }
      if (listColumn.Item.GetType() == typeof (Icon))
      {
        System.Windows.Controls.Image image = new System.Windows.Controls.Image();
        image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.Icon);
        this.Grid.Children.Add((UIElement) image);
        image.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        image.VerticalAlignment = VerticalAlignment.Center;
        image.Stretch = Stretch.None;
        return (UIElement) image;
      }
      if (listColumn.Item.GetType() == typeof (SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Image))
      {
        System.Windows.Controls.Image image = new System.Windows.Controls.Image();
        image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.Image);
        this.Grid.Children.Add((UIElement) image);
        image.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        image.VerticalAlignment = VerticalAlignment.Center;
        image.Stretch = Stretch.None;
        return (UIElement) image;
      }
      if (listColumn.Item.GetType() == typeof (RadioButtonGroup))
      {
        VisualRadioButtonGroup radioButtonGroup = new VisualRadioButtonGroup(new List<object>()
        {
          (object) "Code"
        }, System.Windows.Controls.Orientation.Vertical);
        this.Grid.Children.Add((UIElement) radioButtonGroup);
        return (UIElement) radioButtonGroup;
      }
      System.Windows.Controls.TextBox textBox1 = new System.Windows.Controls.TextBox();
      textBox1.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      textBox1.VerticalAlignment = VerticalAlignment.Stretch;
      textBox1.IsReadOnly = true;
      textBox1.Margin = new Thickness(0.5, 0.5, 0.5, 0.5);
      this.Grid.Children.Add((UIElement) textBox1);
      return (UIElement) textBox1;
    }

    private void AddRectangle(int row, Brush brush)
    {
      Rectangle rectangle = new Rectangle();
      rectangle.Fill = brush;
      rectangle.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      rectangle.VerticalAlignment = VerticalAlignment.Stretch;
      rectangle.StrokeThickness = 0.0;
      rectangle.SetValue(Grid.ColumnProperty, (object) 0);
      rectangle.SetValue(Grid.RowProperty, (object) row);
      this.Grid.Children.Add((UIElement) rectangle);
    }

    private void AddColumnHeaderButton()
    {
      string textProperty = string.Empty;
      bool mandatory = false;
      if (this.ListColumn != null)
      {
        if (this.ListColumn.Label != null)
          textProperty = this.ListColumn.Label.Text;
        if (this.ListColumn.Item is EditControl)
          mandatory = Converter.ToBoolean((DependentProperty) (this.ListColumn.Item as EditControl).Mandatory, false);
      }
      this.m_HeaderButton = new NonFocusButton(false, textProperty, mandatory);
      this.Grid.Children.Add((UIElement) this.m_HeaderButton);
      this.m_HeaderButton.Tag = (object) 0;
      this.m_HeaderButton.SetValue(Grid.ColumnProperty, (object) 0);
      this.m_HeaderButton.SetValue(Grid.RowProperty, (object) 0);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Column", this.AddContextMenuItem("Add"));
      System.Windows.Controls.MenuItem parentItem = this.AddContextMenuItem("Delete");
      this.AddContextMenuItem("Column", parentItem);
      if (this.Model.Parent is List)
        this.AddContextMenuItem("ListPane", parentItem);
      if ((this.Model as ListColumn).Item is LayoutContainerControl)
      {
        if (((this.Model as ListColumn).Item as LayoutContainerControl).Content is LayoutGrid)
          this.AddContextMenuItem("Model Layout Grid");
        else if (((this.Model as ListColumn).Item as LayoutContainerControl).Content is LayoutStackPanel)
          this.AddContextMenuItem("Model Layout Stack Panel");
        else if (((this.Model as ListColumn).Item as LayoutContainerControl).Content is LayoutBorder)
          this.AddContextMenuItem("Model Layout Border Control");
      }
      if ((this.Model as ListColumn).Item.DisplayType != DisplayType.Link)
        return;
      if (((this.Model as ListColumn).Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick != null)
      {
        foreach (UXEventHandlerOperation operation in ((this.Model as ListColumn).Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick.Operations)
        {
          if (!(operation is EventHandlerOperationTypeFireOutport))
          {
            this.AddSeperatorToContextMenu();
            this.AddContextMenuItem("Add OBN using Wizard");
          }
          else
            break;
        }
      }
      else
      {
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Add OBN using Wizard");
      }
      if (!(this.Model is ListColumn) || !((this.Model as ListColumn).Item is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link) || ((this.Model as ListColumn).Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick == null)
        return;
      foreach (UXEventHandlerOperation operation in ((this.Model as ListColumn).Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick.Operations)
      {
        if (operation is EventHandlerOperationTypeFireOutport)
        {
          this.AddSeperatorToContextMenu();
          this.AddContextMenuItem("Edit OBN using Wizard");
        }
      }
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      try
      {
        if (source.Header.ToString() == "Model Layout Grid" || source.Header.ToString() == "Model Layout Stack Panel" || source.Header.ToString() == "Model Layout Border Control")
        {
          ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
          if (parentDesigner == null)
            return;
          parentDesigner.OnLinkedControlDoubleClicked((object) this, (EventArgs) null);
        }
        else if (source.Header.ToString() == "Column")
        {
          System.Windows.Controls.MenuItem parent1 = source.Parent as System.Windows.Controls.MenuItem;
          if (parent1 == null)
            return;
          if (parent1.Header.ToString() == "Add")
          {
            if (this.ParentList != null)
            {
              this.ParentList.AddModelObject("InputField", this.ParentList.Headers.IndexOf(this.ListColumn));
            }
            else
            {
              if (!(this.ListColumn.Parent is GanttChartItemListVariant))
                return;
              GanttChartItemListVariant parent2 = this.ListColumn.Parent as GanttChartItemListVariant;
              int index = parent2.Column.IndexOf(this.ListColumn);
              parent2.AddModelObject("InputField", index);
            }
          }
          else
          {
            if (!(parent1.Header.ToString() == "Delete"))
              return;
            if (this.ParentList != null)
            {
              if (this.ParentList is AvailabilityCalendarList && this.ListColumn.Item.DisplayType == DisplayType.AvailabilityCalendarCellControl)
              {
                int num = (int) DisplayMessage.Show("This is a mandatory column.It cannot be deleted", MessageBoxButtons.OK, MessageBoxIcon.Hand);
              }
              else
              {
                this.ParentList.RemoveModelObject((IModelObject) this.ListColumn);
                if (DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.ConfirmDataFieldDelete, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes || this.ListColumn.BoundField == null)
                  return;
                (this.ListColumn.BoundField.Parent as IDataStructure).DeleteDataField(this.ListColumn.BoundField, false, false, false);
                this.ListColumn.BoundField = (IBaseDataElementType) null;
              }
            }
            else
            {
              if (!(this.ListColumn.Parent is GanttChartItemListVariant))
                return;
              (this.Model.Parent as GanttChartItemListVariant).RemoveModelObject((IModelObject) this.ListColumn);
            }
          }
        }
        else if (source.Header.ToString() == "ListPane")
        {
          IExtensible parent = this.Model.Parent as IExtensible;
          if (!((source.Parent as System.Windows.Controls.MenuItem).Header.ToString() == "Delete"))
            return;
          AdvancedListPane parentOfType1 = Utilities.GetParentOfType(this.Model, typeof (AdvancedListPane)) as AdvancedListPane;
          if (parentOfType1 != null)
          {
            PaneContainerVariant parentOfType2 = Utilities.GetParentOfType(this.Model, typeof (PaneContainerVariant)) as PaneContainerVariant;
            if (parentOfType2 == null)
            {
              PaneContainer parentOfType3 = Utilities.GetParentOfType(this.Model, typeof (PaneContainer)) as PaneContainer;
              if (parentOfType3 == null || parentOfType1 == null)
                return;
              parentOfType3.RemoveModelObject((IModelObject) parentOfType1);
            }
            else
              parentOfType2.RemoveModelObject((IModelObject) parentOfType1);
          }
          else
          {
            if (!(parent.Parent is AvailabilityCalendarPane))
              return;
            AvailabilityCalendarPane parentOfType2 = Utilities.GetParentOfType(this.Model, typeof (AvailabilityCalendarPane)) as AvailabilityCalendarPane;
            if (parentOfType2 == null || parentOfType2.Parent == null)
              return;
            parentOfType2.Parent.RemoveModelObject((IModelObject) parentOfType2);
          }
        }
        else if (source.Header.ToString() == "Add OBN using Wizard")
        {
          WizardDialog wizardDialog = new WizardDialog(OBNWizardStepFactory.Instance.GetWizardSteps(Utilities.GetFloorplanObject(this.Model), (IOBN) null));
          int num = (int) wizardDialog.ShowDialog();
          IEventHandler wizardResult = wizardDialog.WizardResult as IEventHandler;
          if (wizardResult == null)
            return;
          ((this.Model as ListColumn).Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick = wizardResult;
        }
        else
        {
          if (!(source.Header.ToString() == "Edit OBN using Wizard"))
            return;
          IFloorplan floorplanObject = Utilities.GetFloorplanObject(this.Model);
          IEventHandler onClick = ((this.Model as ListColumn).Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick;
          IOutPortType outPortType = (IOutPortType) null;
          foreach (UXEventHandlerOperation operation in onClick.Operations)
          {
            if (operation is EventHandlerOperationTypeFireOutport)
            {
              foreach (IOutPortType outPort in floorplanObject.ControllerInterface.OutPorts)
              {
                if (outPort.Name == (operation as EventHandlerOperationTypeFireOutport).OutPlug)
                {
                  outPortType = outPort;
                  break;
                }
              }
            }
          }
          foreach (OberonOBN oberonObn in floorplanObject.ControllerNavigation.OBN)
          {
            if (oberonObn.OutPlug.Id == outPortType.Id)
            {
              WizardDialog wizardDialog = new WizardDialog(OBNWizardStepFactory.Instance.GetWizardSteps(floorplanObject, (IOBN) oberonObn));
              int num = (int) wizardDialog.ShowDialog();
              if (wizardDialog.WizardResult == null)
                break;
              ((this.Model as ListColumn).Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick = wizardDialog.WizardResult as IEventHandler;
              break;
            }
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      if (e.PropertyName == "Label" || e.PropertyName == "Mandatory")
      {
        string textProperty = string.Empty;
        bool mandatory = false;
        if (this.ListColumn != null)
        {
          if (this.ListColumn.Label != null)
            textProperty = this.ListColumn.Label.Text;
          if (this.ListColumn.Item is EditControl)
            mandatory = Converter.ToBoolean((DependentProperty) (this.ListColumn.Item as EditControl).Mandatory, false);
        }
        if (this.m_HeaderButton != null)
        {
          if (this.Grid.Children.Contains((UIElement) this.m_HeaderButton))
            this.Grid.Children.Remove((UIElement) this.m_HeaderButton);
          this.m_HeaderButton = new NonFocusButton(false, textProperty, mandatory);
          this.Grid.Children.Add((UIElement) this.m_HeaderButton);
          this.m_HeaderButton.Tag = (object) 0;
          this.m_HeaderButton.SetValue(Grid.ColumnProperty, (object) 0);
          this.m_HeaderButton.SetValue(Grid.RowProperty, (object) 0);
          this.m_HeaderButton.SetValue(Grid.ColumnSpanProperty, (object) 1);
        }
        this.TriggerSelection((BaseSelectableControl) this, false);
      }
      else
        this.ReloadView();
    }

    protected override void OnBOElementDrop(BOBrowserDragEventArgs boBrowserArgs)
    {
      try
      {
        if (this.ParentList == null)
          return;
        int index = this.ParentList.Headers.IndexOf(this.ListColumn);
        foreach (BOBrowserDragEventArg boBrowserArg in (List<BOBrowserDragEventArg>) boBrowserArgs)
          this.ParentList.AddListColumn(boBrowserArg, index, this.ListColumn);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Control Drop event Failed.", ex));
      }
    }

    protected override void OnDataModelElementDrop(DataModelDragEventArgs dmArgs)
    {
      try
      {
        if (this.ParentList == null)
          return;
        int index = this.ParentList.Headers.IndexOf(this.ListColumn);
        if (this.ListColumn != null && this.ListColumn.BoundField != null && !this.ListColumn.BoundField.IsBound)
          base.OnDataModelElementDrop(dmArgs);
        else
          this.ParentList.AddListColumn(dmArgs, index, this.ListColumn);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Control Drop event Failed.", ex));
      }
    }

    protected override void OnToolBoxControlDrop(ToolBoxEventArgs toolboxEventArg)
    {
      try
      {
        if (this.ParentList == null)
          return;
        int index = this.ParentList.Headers.IndexOf(this.ListColumn);
        this.ParentList.AddModelObject(toolboxEventArg.Tag, index);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Control Drop event Failed.", ex));
      }
    }

    public override void SelectMe()
    {
      if (this.m_RowSelector)
      {
        VisualList list = this.GetList(this);
        if (list == null)
          return;
        list.SelectMe();
      }
      else
        base.SelectMe();
    }

    public override void UnSelect()
    {
      if (this.m_RowSelector)
      {
        VisualList list = this.GetList(this);
        if (list == null)
          return;
        list.UnSelect();
      }
      else
        base.UnSelect();
    }

    private VisualList GetList(VisualListColumn listColumn)
    {
      for (FrameworkElement frameworkElement = (FrameworkElement) listColumn; frameworkElement != null; frameworkElement = frameworkElement.Parent as FrameworkElement)
      {
        if (frameworkElement.Parent is VisualList)
          return frameworkElement.Parent as VisualList;
      }
      return (VisualList) null;
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.ListColumn == null)
        return;
      this.ListColumn.Item.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
    }

    protected override void OnToolBoxControlDragOver(System.Windows.DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      base.OnToolBoxControlDragOver(e, toolboxEventArg);
      if (this.ParentList == null || this.ParentList.TableDesign != ListDesignType.transparent)
        return;
      if (toolboxEventArg.Tag.ControlName == "StaticText" || toolboxEventArg.Tag.ControlName == "Icon" || (toolboxEventArg.Tag.ControlName == "Image" || toolboxEventArg.Tag.ControlName == "Link") || (toolboxEventArg.Tag.ControlName == "Button" || toolboxEventArg.Tag.ControlName == "CompoundField"))
        e.Effects = System.Windows.DragDropEffects.Copy;
      else
        e.Effects = System.Windows.DragDropEffects.None;
    }
  }
}
