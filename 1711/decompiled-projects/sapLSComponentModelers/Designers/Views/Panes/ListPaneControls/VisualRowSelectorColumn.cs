﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.VisualRowSelectorColumn
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public class VisualRowSelectorColumn : UserControl
  {
    private static readonly double standardRowHeight = 20.0;
    private static readonly double standardColumnHeaderHeight = 19.0;
    private static readonly double gridSeparatorWidth = 1.0;
    internal bool hasColumnHeaders = true;
    private NonFocusButton button;
    internal List list;
    internal BaseSelectableControl vList;
    private Grid rootGridControl;

    public VisualRowSelectorColumn(IModelObject model, BaseSelectableControl visualList)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.list = model as List;
      this.vList = visualList;
      this.rootGridControl = new Grid();
      this.Content = (object) this.rootGridControl;
      if (this.list != null)
        this.hasColumnHeaders = this.list.ShowHeaderRow;
      if (model is GanttChartItemListVariant)
        this.hasColumnHeaders = true;
      this.LoadView();
    }

    public void LoadView()
    {
      try
      {
        GridUtil.AddRowDef(this.rootGridControl, VisualRowSelectorColumn.standardColumnHeaderHeight, GridUnitType.Pixel);
        GridUtil.AddRowDef(this.rootGridControl, VisualRowSelectorColumn.gridSeparatorWidth, GridUnitType.Pixel);
        GridUtil.AddRowDef(this.rootGridControl, VisualRowSelectorColumn.standardRowHeight, GridUnitType.Pixel);
        GridUtil.AddRowDef(this.rootGridControl, VisualRowSelectorColumn.gridSeparatorWidth, GridUnitType.Pixel);
        this.AddColumnHeaderButton(0);
        NonFocusButton nonFocusButton = new NonFocusButton(true, (string) null, false);
        nonFocusButton.SetValue(Grid.ColumnProperty, (object) 0);
        nonFocusButton.SetValue(Grid.RowProperty, (object) 2);
        this.rootGridControl.Children.Add((UIElement) nonFocusButton);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of ListColumn Control failed.", ex));
      }
    }

    private void AddColumnHeaderButton(int row)
    {
      this.button = new NonFocusButton(false, string.Empty, false);
      this.rootGridControl.Children.Add((UIElement) this.button);
      this.button.Tag = (object) 0;
      this.button.SetValue(Grid.ColumnProperty, (object) 0);
      this.button.SetValue(Grid.RowProperty, (object) 0);
      this.button.SetValue(Grid.ColumnSpanProperty, (object) 1);
    }

    protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
    {
      this.vList.TriggerSelection(this.vList, false);
    }
  }
}
