﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.VisualDisplayOnlyListColumn
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Controls;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public class VisualDisplayOnlyListColumn : BaseSelectableControl
  {
    private static readonly double standardRowHeight = 20.0;
    private static readonly double standardColumnHeaderHeight = 19.0;
    private static readonly double gridSeparatorWidth = 1.0;
    private const string ADD = "Add";
    private const string DELETE = "Delete";
    private const string COLUMN = "column";
    private VisualColumnHeader button;
    internal ListColumn listColumn;
    private List list;
    private int visibleRowCount;
    private bool hasColumnHeaders;
    private bool rowSelector;

    public VisualDisplayOnlyListColumn(IModelObject model, bool rSelector)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.ControlBackground = (Brush) Brushes.White;
      this.listColumn = model as ListColumn;
      this.rowSelector = rSelector;
      this.list = this.listColumn == null ? model as List : this.listColumn.Parent as List;
      if (this.list != null)
      {
        this.visibleRowCount = this.list.MaxVisibleRows;
        this.hasColumnHeaders = this.list.ShowHeaderRow;
      }
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        int num1 = 0;
        if (this.hasColumnHeaders)
        {
          GridUtil.AddRowDef(this.Grid, VisualDisplayOnlyListColumn.standardColumnHeaderHeight, GridUnitType.Pixel);
          GridUtil.AddRowDef(this.Grid, VisualDisplayOnlyListColumn.gridSeparatorWidth, GridUnitType.Pixel);
          num1 += 2;
        }
        for (int index = 0; index < this.visibleRowCount; ++index)
        {
          GridUtil.AddRowDef(this.Grid, VisualDisplayOnlyListColumn.standardRowHeight, GridUnitType.Pixel);
          GridUtil.AddRowDef(this.Grid, VisualDisplayOnlyListColumn.gridSeparatorWidth, GridUnitType.Pixel);
          num1 += 2;
        }
        GridUtil.AddRowDef(this.Grid, VisualDisplayOnlyListColumn.gridSeparatorWidth * 2.0, GridUnitType.Pixel);
        int num2 = num1 + 1;
        if (this.hasColumnHeaders)
        {
          this.AddRectangle(0, VisualList.BRUSH_TABLEHEADER_SEPARATOR);
          this.AddColumnHeaderButton(0);
        }
        if (!this.rowSelector)
        {
          for (int index = 1; index <= this.visibleRowCount; ++index)
          {
            this.AddRectangle(index * 2, this.ControlBackground);
            this.AddRectangle(index * 2 + 1, VisualList.BRUSH_TABLE_GRID);
            UserControl cellControl = this.GetCellControl(this.listColumn.Item.GetType());
            cellControl.Background = (Brush) Brushes.Transparent;
            cellControl.BorderThickness = new Thickness(0.0);
            this.Grid.Children.Add((UIElement) cellControl);
            cellControl.HorizontalAlignment = HorizontalAlignment.Stretch;
            cellControl.VerticalAlignment = VerticalAlignment.Stretch;
            cellControl.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
            cellControl.SetValue(Grid.ColumnProperty, (object) 0);
            cellControl.SetValue(Grid.RowProperty, (object) (index * 2));
            cellControl.Tag = (object) index;
          }
          this.AddRectangle(num2 - 1, VisualList.BRUSH_TABLE_GRID);
        }
        else
        {
          for (int index = 1; index <= this.visibleRowCount; ++index)
          {
            this.AddRectangle(index * 2, this.ControlBackground);
            this.AddRectangle(index * 2 + 1, VisualList.BRUSH_TABLE_GRID);
            VisualColumnHeader visualColumnHeader = new VisualColumnHeader((TextDependentProperty) null);
            visualColumnHeader.SetValue(Grid.ColumnProperty, (object) 0);
            visualColumnHeader.SetValue(Grid.RowProperty, (object) (index * 2));
            this.Grid.Children.Add((UIElement) visualColumnHeader);
            visualColumnHeader.Tag = (object) index;
          }
          this.AddRectangle(num2 - 1, VisualList.BRUSH_TABLE_GRID);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of ListColumn Control failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "ListColumn";
      }
    }

    private UserControl GetCellControl(Type fieldType)
    {
      if (fieldType == typeof (InputField))
        return (UserControl) new VisualInputField(false)
        {
          FieldStyle = FieldStyles.Disabled
        };
      if (fieldType == typeof (DropDownListBox))
        return (UserControl) new VisualCombobox()
        {
          HasBorder = false
        };
      if (fieldType == typeof (Link))
        return (UserControl) new VisualLink(VisualLink.LinkStyles.Standard);
      return (UserControl) new VisualInputField(false)
      {
        FieldStyle = FieldStyles.Disabled
      };
    }

    private void AddRectangle(int row, Brush brush)
    {
      Rectangle rectangle = new Rectangle();
      rectangle.Fill = brush;
      rectangle.HorizontalAlignment = HorizontalAlignment.Stretch;
      rectangle.VerticalAlignment = VerticalAlignment.Stretch;
      rectangle.StrokeThickness = 0.0;
      rectangle.SetValue(Grid.ColumnProperty, (object) 0);
      rectangle.SetValue(Grid.RowProperty, (object) row);
      rectangle.SetValue(Grid.ColumnSpanProperty, (object) 1);
      rectangle.SetValue(Grid.RowSpanProperty, (object) 1);
      this.Grid.Children.Add((UIElement) rectangle);
    }

    private void AddColumnHeaderButton(int row)
    {
      string empty = string.Empty;
      if (this.listColumn == null)
        return;
      this.button = new VisualColumnHeader(this.listColumn.Label);
      this.Grid.Children.Add((UIElement) this.button);
      this.button.Tag = (object) 0;
      this.button.SetValue(Grid.ColumnProperty, (object) 0);
      this.button.SetValue(Grid.RowProperty, (object) 0);
      this.button.SetValue(Grid.ColumnSpanProperty, (object) 1);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("column", this.AddContextMenuItem("Add"));
      this.AddContextMenuItem("column", this.AddContextMenuItem("Delete"));
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (source.Header != (object) "column")
        return;
      ContextMenuItem parent = source.Parent as ContextMenuItem;
      if (parent == null)
        return;
      if (parent.Header.ToString() == "Add")
      {
        this.list.AddHeaderColumn(ColumnType.InputField);
      }
      else
      {
        if (!(parent.Header.ToString() == "Delete"))
          return;
        this.list.RemoveModelObject((IModelObject) (this.Model as ListColumn));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      if (e.PropertyName == "Label")
      {
        string empty = string.Empty;
        if (this.listColumn != null && this.listColumn.Label != null)
        {
          string text = this.listColumn.Label.Text;
        }
        if (this.button == null)
          return;
        if (this.Grid.Children.Contains((UIElement) this.button))
          this.Grid.Children.Remove((UIElement) this.button);
        this.button = new VisualColumnHeader(this.listColumn.Label);
        this.Grid.Children.Add((UIElement) this.button);
        this.button.Tag = (object) 0;
        this.button.SetValue(Grid.ColumnProperty, (object) 0);
        this.button.SetValue(Grid.RowProperty, (object) 0);
        this.button.SetValue(Grid.ColumnSpanProperty, (object) 1);
      }
      else
        this.ReloadView();
    }

    protected override void OnBOElementDrop(BOBrowserDragEventArgs boBrowserArgs)
    {
      try
      {
        if (!(this.Model is ListColumn))
          return;
        List parent = this.Model.Parent as List;
        if (parent == null)
          return;
        int index = parent.Headers.IndexOf(this.listColumn);
        foreach (BOBrowserDragEventArg boBrowserArg in (List<BOBrowserDragEventArg>) boBrowserArgs)
          parent.AddListColumn(boBrowserArg, index, this.listColumn);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Control Drop event Failed.", ex));
      }
    }

    protected override void OnToolBoxControlDrop(ToolBoxEventArgs toolboxEventArg)
    {
      try
      {
        if (!(this.Model is ListColumn))
          return;
        List parent = this.Model.Parent as List;
        if (parent == null)
          return;
        int index = parent.Headers.IndexOf(this.listColumn);
        parent.AddModelObject(toolboxEventArg.Tag, index);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Control Drop event Failed.", ex));
      }
    }

    public override void SelectMe()
    {
      if (this.rowSelector)
        this.GetList(this).SelectMe();
      else
        base.SelectMe();
    }

    public override void UnSelect()
    {
      if (this.rowSelector)
        this.GetList(this).UnSelect();
      else
        base.UnSelect();
    }

    private VisualList GetList(VisualDisplayOnlyListColumn listColumn)
    {
      for (FrameworkElement frameworkElement = (FrameworkElement) listColumn; frameworkElement != null; frameworkElement = frameworkElement.Parent as FrameworkElement)
      {
        if (frameworkElement.Parent is VisualList)
          return frameworkElement.Parent as VisualList;
      }
      return (VisualList) null;
    }
  }
}
