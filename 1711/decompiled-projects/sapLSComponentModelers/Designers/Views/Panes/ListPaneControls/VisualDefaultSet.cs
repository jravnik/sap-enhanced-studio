﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.VisualDefaultSet
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Oberon.Controller;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public class VisualDefaultSet : BaseVisualControl
  {
    private const string RESOURCE_REFRESH = "sapLSComponentModelers;component/Designers/Views/Panes/ListPaneControls/icon_refresh.png,18,18";
    private const string RESOURCE_ADVANCED = "sapLSUISkins;component/dt.images/GIF/AdvancedSearch.gif,14,14";
    private const string STANDARD_TEXT = "Standard";
    private TextBox m_FindTextBox;
    private bool m_Initialize;
    private ComboBox m_ShowComboBox;
    private ComboBox m_AlternateVisualShowComboBox;
    private AdvancedListPane Listpane;
    private AdvancedDataSetPane dataSetPane;
    private System.Windows.Controls.Button m_BtnGo;
    private TextBlock showLabel;
    private TextBlock andLabel;
    private TextBlock findLabel;
    private TextBlock AvLabel;
    private InitFormType m_InitFormType;
    private VisualLink linkLabel;
    private System.Windows.Controls.Button m_Button;
    internal VisualLink m_AdvBasicLink;
    internal System.Windows.Controls.Image m_iconRefresh;
    private TextBlock m_LabelDefaultSetSelector;
    private TextBlock m_LabelSearchBox;

    public event LinkClicked OnFFLinkClicked;

    public event DefaultSetChanged OnDefaultSetChanged;

    public event AlternateListVisualChanged OnAlternateListVisualChanged;

    public event AlternateDataSetVisualChanged OnAlternateDataSetVisualChanged;

    public override string SelectionText
    {
      get
      {
        return string.Empty;
      }
    }

    public VisualDefaultSet(IModelObject model, InitFormType initFormType)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      if (model is AdvancedListPane)
        this.Listpane = model as AdvancedListPane;
      else if (model is AdvancedDataSetPane)
        this.dataSetPane = model as AdvancedDataSetPane;
      this.m_InitFormType = initFormType;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        if (this.Listpane.UseIntegratedToolbar)
        {
          this.CreatedIntegratedDefaultSet();
        }
        else
        {
          this.m_Initialize = true;
          base.LoadView();
          this.Content = (object) this.Grid;
          GridUtil.AddRowDef(this.Grid, 30.0, GridUnitType.Pixel);
          this.Grid.HorizontalAlignment = HorizontalAlignment.Stretch;
          this.Grid.VerticalAlignment = VerticalAlignment.Center;
          this.showLabel = new TextBlock();
          this.AddElement((FrameworkElement) this.showLabel);
          this.m_LabelDefaultSetSelector = this.showLabel;
          this.showLabel.HorizontalAlignment = HorizontalAlignment.Left;
          this.showLabel.VerticalAlignment = VerticalAlignment.Center;
          this.showLabel.Text = "Show";
          this.showLabel.Margin = new Thickness(10.0, 0.0, 0.0, 0.0);
          FontManager.Instance.SetFontStyle1("Arial,11,Bold", this.showLabel);
          this.showLabel.Foreground = (Brush) SkinConstants.BRUSH_TEXTVIEW_SECTIONGROUPTITLE;
          this.m_ShowComboBox = new ComboBox();
          this.LoadDefaultSets();
          this.AddElement((FrameworkElement) this.m_ShowComboBox);
          this.m_ShowComboBox.SelectionChanged += new SelectionChangedEventHandler(this.OnSelectionChanged);
          this.m_ShowComboBox.HorizontalAlignment = HorizontalAlignment.Left;
          this.m_ShowComboBox.VerticalAlignment = VerticalAlignment.Center;
          this.m_ShowComboBox.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
          this.m_ShowComboBox.MinWidth = 100.0;
          this.andLabel = new TextBlock();
          this.AddElement((FrameworkElement) this.andLabel);
          this.m_LabelSearchBox = this.andLabel;
          this.andLabel.HorizontalAlignment = HorizontalAlignment.Left;
          this.andLabel.VerticalAlignment = VerticalAlignment.Center;
          this.andLabel.Text = "and";
          this.andLabel.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
          FontManager.Instance.SetFontStyle1("Arial,11,Bold", this.andLabel);
          this.andLabel.Foreground = (Brush) SkinConstants.BRUSH_TEXTVIEW_SECTIONGROUPTITLE;
          this.findLabel = new TextBlock();
          this.AddElement((FrameworkElement) this.findLabel);
          this.m_LabelSearchBox = this.findLabel;
          this.findLabel.HorizontalAlignment = HorizontalAlignment.Left;
          this.findLabel.VerticalAlignment = VerticalAlignment.Center;
          this.findLabel.Text = "Find";
          this.findLabel.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
          FontManager.Instance.SetFontStyle1("Arial,11,Bold", this.findLabel);
          this.findLabel.Foreground = (Brush) SkinConstants.BRUSH_TEXTVIEW_SECTIONGROUPTITLE;
          this.m_FindTextBox = new TextBox();
          this.AddElement((FrameworkElement) this.m_FindTextBox);
          this.m_FindTextBox.HorizontalAlignment = HorizontalAlignment.Left;
          this.m_FindTextBox.VerticalAlignment = VerticalAlignment.Center;
          this.m_FindTextBox.MinWidth = 75.0;
          this.m_FindTextBox.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
          this.m_BtnGo = new System.Windows.Controls.Button();
          this.AddElement((FrameworkElement) this.m_BtnGo);
          this.m_BtnGo.Content = (object) "Go";
          this.m_BtnGo.HorizontalAlignment = HorizontalAlignment.Left;
          this.m_BtnGo.VerticalAlignment = VerticalAlignment.Center;
          this.m_BtnGo.Background = (Brush) SkinConstants.BRUSH_BUTTON_STANDARDBACKCOLOR;
          this.m_BtnGo.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
          System.Windows.Controls.Image image1 = new System.Windows.Controls.Image();
          image1.Stretch = Stretch.None;
          image1.Cursor = Cursors.Hand;
          this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
          {
            Width = new GridLength(1.0, GridUnitType.Star)
          });
          this.Grid.Children.Add((UIElement) image1);
          image1.SetValue(Grid.ColumnProperty, (object) (this.Grid.ColumnDefinitions.Count - 1));
          image1.HorizontalAlignment = HorizontalAlignment.Right;
          image1.VerticalAlignment = VerticalAlignment.Center;
          image1.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
          this.AvLabel = new TextBlock();
          this.AvLabel.HorizontalAlignment = HorizontalAlignment.Right;
          this.AvLabel.VerticalAlignment = VerticalAlignment.Center;
          this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
          {
            Width = new GridLength()
          });
          this.Grid.Children.Add((UIElement) this.AvLabel);
          this.AvLabel.SetValue(Grid.ColumnProperty, (object) (this.Grid.ColumnDefinitions.Count - 1));
          this.AvLabel.Text = "Alternate Visualization";
          this.AvLabel.Margin = new Thickness(10.0, 0.0, 5.0, 0.0);
          FontManager.Instance.SetFontStyle1("Arial,11,Bold", this.AvLabel);
          this.AvLabel.Foreground = (Brush) SkinConstants.BRUSH_TEXTVIEW_SECTIONGROUPTITLE;
          this.m_AlternateVisualShowComboBox = new ComboBox();
          this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
          {
            Width = new GridLength()
          });
          this.Grid.Children.Add((UIElement) this.m_AlternateVisualShowComboBox);
          this.m_AlternateVisualShowComboBox.SetValue(Grid.ColumnProperty, (object) (this.Grid.ColumnDefinitions.Count - 1));
          this.m_AlternateVisualShowComboBox.SelectionChanged += new SelectionChangedEventHandler(this.OnAlternateVisualSelectionChanged);
          this.m_AlternateVisualShowComboBox.HorizontalAlignment = HorizontalAlignment.Right;
          this.m_AlternateVisualShowComboBox.VerticalAlignment = VerticalAlignment.Center;
          this.m_AlternateVisualShowComboBox.Margin = new Thickness(5.0, 0.0, 5.0, 0.0);
          this.m_AlternateVisualShowComboBox.MinWidth = 100.0;
          System.Windows.Controls.Image image2 = new System.Windows.Controls.Image();
          ResourceUtil.LoadImage(image2, "sapLSComponentModelers;component/Designers/Views/Panes/ListPaneControls/icon_refresh.png,18,18");
          this.m_iconRefresh = image2;
          image2.Stretch = Stretch.None;
          image2.Cursor = Cursors.Hand;
          this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
          {
            Width = new GridLength(18.0, GridUnitType.Pixel)
          });
          this.Grid.Children.Add((UIElement) image2);
          image2.SetValue(Grid.ColumnProperty, (object) (this.Grid.ColumnDefinitions.Count - 1));
          image2.HorizontalAlignment = HorizontalAlignment.Right;
          image2.VerticalAlignment = VerticalAlignment.Center;
          image2.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
          this.linkLabel = new VisualLink(VisualLink.LinkStyles.Standard);
          this.AddElement((FrameworkElement) this.linkLabel);
          this.m_AdvBasicLink = this.linkLabel;
          this.m_AdvBasicLink.LinkClicked += new EventHandler<EventArgs>(this.OnLinkClicked);
          this.linkLabel.HorizontalAlignment = HorizontalAlignment.Right;
          this.linkLabel.Margin = new Thickness(5.0, 0.0, 10.0, 0.0);
          this.linkLabel.VerticalAlignment = VerticalAlignment.Center;
          this.linkLabel.Foreground = (Brush) SkinConstants.BRUSH_TOOLBARLINK_STANDARD;
          this.UpdateAdvancedText();
          this.Grid.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
          this.SetVisibleProperties();
          this.m_Initialize = false;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of DefaultSets Area failed.", ex));
      }
    }

    private void CreatedIntegratedDefaultSet()
    {
      this.m_Initialize = true;
      base.LoadView();
      this.Content = (object) this.Grid;
      GridUtil.AddRowDef(this.Grid, 30.0, GridUnitType.Pixel);
      this.Grid.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.Grid.VerticalAlignment = VerticalAlignment.Center;
      this.m_AlternateVisualShowComboBox = new ComboBox();
      this.AddElement((FrameworkElement) this.m_AlternateVisualShowComboBox);
      this.m_AlternateVisualShowComboBox.SelectionChanged += new SelectionChangedEventHandler(this.OnAlternateVisualSelectionChanged);
      this.m_AlternateVisualShowComboBox.HorizontalAlignment = HorizontalAlignment.Left;
      this.m_AlternateVisualShowComboBox.VerticalAlignment = VerticalAlignment.Center;
      this.m_AlternateVisualShowComboBox.Margin = new Thickness(5.0, 0.0, 5.0, 0.0);
      this.m_AlternateVisualShowComboBox.MinWidth = 100.0;
      this.m_ShowComboBox = new ComboBox();
      this.LoadDefaultSets();
      this.AddElement((FrameworkElement) this.m_ShowComboBox);
      this.m_ShowComboBox.SelectionChanged += new SelectionChangedEventHandler(this.OnSelectionChanged);
      this.m_ShowComboBox.HorizontalAlignment = HorizontalAlignment.Left;
      this.m_ShowComboBox.VerticalAlignment = VerticalAlignment.Center;
      this.m_ShowComboBox.Margin = new Thickness(5.0, 0.0, 5.0, 0.0);
      this.m_ShowComboBox.MinWidth = 100.0;
      this.m_Button = new System.Windows.Controls.Button();
      this.AddElement((FrameworkElement) this.m_Button);
      System.Windows.Controls.Image image = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image, "sapLSUISkins;component/dt.images/GIF/AdvancedSearch.gif,14,14");
      this.m_Button.Content = (object) image;
      this.m_Button.HorizontalAlignment = HorizontalAlignment.Left;
      this.m_Button.VerticalAlignment = VerticalAlignment.Center;
      this.m_Button.Click += new RoutedEventHandler(this.OnButtonClicked);
      this.m_Button.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
      this.m_FindTextBox = new TextBox();
      this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Star)
      });
      this.Grid.Children.Add((UIElement) this.m_FindTextBox);
      this.m_FindTextBox.SetValue(Grid.ColumnProperty, (object) (this.Grid.ColumnDefinitions.Count - 1));
      this.m_FindTextBox.HorizontalAlignment = HorizontalAlignment.Right;
      this.m_FindTextBox.VerticalAlignment = VerticalAlignment.Center;
      this.m_FindTextBox.Margin = new Thickness(0.0, 0.0, 5.0, 0.0);
      this.m_FindTextBox.Text = "Search this view";
      this.Grid.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      this.linkLabel = new VisualLink(VisualLink.LinkStyles.Standard);
      this.AddElement((FrameworkElement) this.linkLabel);
      this.m_AdvBasicLink = this.linkLabel;
      this.m_AdvBasicLink.LinkClicked += new EventHandler<EventArgs>(this.OnLinkClicked);
      this.linkLabel.HorizontalAlignment = HorizontalAlignment.Right;
      this.linkLabel.Margin = new Thickness(5.0, 0.0, 10.0, 0.0);
      this.linkLabel.VerticalAlignment = VerticalAlignment.Center;
      this.linkLabel.Foreground = (Brush) SkinConstants.BRUSH_TOOLBARLINK_STANDARD;
      this.UpdateAdvancedText();
      this.m_AdvBasicLink.Visibility = Visibility.Collapsed;
      this.SetVisiblePropertiesForIntegratedToolbar();
      this.Grid.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      this.m_Initialize = false;
    }

    private void SetVisiblePropertiesForIntegratedToolbar()
    {
      bool flag = false;
      if (this.Listpane == null)
        return;
      if (this.Listpane.DefaultSets != null && this.Listpane.DefaultSets.Count > 0)
      {
        IDefaultSetMapping defaultSet = this.Listpane.DefaultSets[0];
        if (this.m_ShowComboBox != null && this.m_ShowComboBox.Items.Count > 0 && this.m_ShowComboBox.SelectedIndex != -1)
          defaultSet = this.Listpane.DefaultSets[this.m_ShowComboBox.SelectedIndex];
        if (defaultSet != null && defaultSet.FindFormName != null && defaultSet.FindFormName != string.Empty)
        {
          foreach (FindFormPane findFormPane in this.Listpane.FindFormPanes)
          {
            if (findFormPane.FindFormName == defaultSet.FindFormName && findFormPane.SearchTextBinding != null && findFormPane.SearchTextBinding != string.Empty)
              flag = true;
          }
        }
      }
      else
      {
        if (this.Listpane.PaneVariants.Count > 0)
        {
          foreach (AdvancedListPaneVariant paneVariant in this.Listpane.PaneVariants)
            this.AddAlternateVisual(paneVariant);
        }
        this.m_AlternateVisualShowComboBox.Visibility = Visibility.Visible;
      }
      if (this.Listpane.DefaultSets.Count > 0)
      {
        this.m_ShowComboBox.Visibility = Visibility.Visible;
        this.m_Button.Visibility = Visibility.Visible;
      }
      else
      {
        this.m_ShowComboBox.Visibility = Visibility.Collapsed;
        this.m_Button.Visibility = Visibility.Collapsed;
      }
      if (flag)
        this.m_FindTextBox.Visibility = Visibility.Visible;
      else
        this.m_FindTextBox.Visibility = Visibility.Collapsed;
      if (!string.IsNullOrEmpty(this.Listpane.SelectedDefaultSet))
      {
        foreach (IDefaultSetMapping defaultSet in this.Listpane.DefaultSets)
        {
          if (this.Listpane.SelectedDefaultSet == defaultSet.DefaultSetName)
          {
            if (this.Listpane.PaneVariants.Count > 0)
            {
              foreach (AdvancedListPaneVariant paneVariant in this.Listpane.PaneVariants)
              {
                if (paneVariant.Name == defaultSet.ListPaneVariantName)
                  this.AddAlternateVisual(paneVariant);
              }
            }
            else
              this.m_AlternateVisualShowComboBox.Visibility = Visibility.Collapsed;
          }
        }
      }
      else
        this.m_AlternateVisualShowComboBox.Visibility = Visibility.Visible;
    }

    private void SetVisibleProperties()
    {
      bool flag = false;
      if (this.Listpane != null)
      {
        if (this.Listpane.DefaultSets != null && this.Listpane.DefaultSets.Count > 0)
        {
          IDefaultSetMapping defaultSet = this.Listpane.DefaultSets[0];
          if (this.m_ShowComboBox != null && this.m_ShowComboBox.Items.Count > 0 && this.m_ShowComboBox.SelectedIndex != -1)
            defaultSet = this.Listpane.DefaultSets[this.m_ShowComboBox.SelectedIndex];
          if (defaultSet != null && defaultSet.FindFormName != null && defaultSet.FindFormName != string.Empty)
          {
            foreach (FindFormPane findFormPane in this.Listpane.FindFormPanes)
            {
              if (findFormPane.FindFormName == defaultSet.FindFormName && findFormPane.SearchTextBinding != null && findFormPane.SearchTextBinding != string.Empty)
                flag = true;
            }
          }
        }
        else
        {
          if (this.Listpane.PaneVariants.Count > 0)
          {
            foreach (AdvancedListPaneVariant paneVariant in this.Listpane.PaneVariants)
              this.AddAlternateVisual(paneVariant);
          }
          this.AvLabel.Visibility = Visibility.Visible;
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Visible;
        }
        if (this.Listpane.DefaultSets.Count > 0)
        {
          this.showLabel.Visibility = Visibility.Visible;
          this.m_ShowComboBox.Visibility = Visibility.Visible;
        }
        else
        {
          this.showLabel.Visibility = Visibility.Collapsed;
          this.m_ShowComboBox.Visibility = Visibility.Collapsed;
          this.m_AdvBasicLink.Visibility = Visibility.Hidden;
          this.m_iconRefresh.Visibility = Visibility.Visible;
        }
        if (flag && this.Listpane.DefaultSets.Count > 0)
          this.andLabel.Visibility = Visibility.Visible;
        else
          this.andLabel.Visibility = Visibility.Collapsed;
        if (flag)
        {
          this.findLabel.Visibility = Visibility.Visible;
          this.m_FindTextBox.Visibility = Visibility.Visible;
          this.m_BtnGo.Visibility = Visibility.Visible;
        }
        else
        {
          this.findLabel.Visibility = Visibility.Collapsed;
          this.m_FindTextBox.Visibility = Visibility.Collapsed;
          this.m_BtnGo.Visibility = Visibility.Collapsed;
        }
        if (!string.IsNullOrEmpty(this.Listpane.SelectedDefaultSet))
        {
          foreach (IDefaultSetMapping defaultSet in this.Listpane.DefaultSets)
          {
            if (this.Listpane.SelectedDefaultSet == defaultSet.DefaultSetName)
            {
              if (this.Listpane.PaneVariants.Count > 0)
              {
                foreach (AdvancedListPaneVariant paneVariant in this.Listpane.PaneVariants)
                {
                  if (paneVariant.Name == defaultSet.ListPaneVariantName)
                    this.AddAlternateVisual(paneVariant);
                }
              }
              else
              {
                this.AvLabel.Visibility = Visibility.Hidden;
                this.m_AlternateVisualShowComboBox.Visibility = Visibility.Hidden;
              }
            }
          }
        }
        else
        {
          this.AvLabel.Visibility = Visibility.Visible;
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Visible;
        }
      }
      else
      {
        if (this.dataSetPane == null)
          return;
        if (this.dataSetPane.DefaultSets != null && this.dataSetPane.DefaultSets.Count > 0)
        {
          IDefaultSetMapping defaultSet = this.dataSetPane.DefaultSets[0];
          if (this.m_ShowComboBox != null && this.m_ShowComboBox.Items.Count > 0 && this.m_ShowComboBox.SelectedIndex != -1)
            defaultSet = this.dataSetPane.DefaultSets[this.m_ShowComboBox.SelectedIndex];
          if (defaultSet != null && defaultSet.FindFormName != null && defaultSet.FindFormName != string.Empty)
          {
            foreach (FindFormPane findFormPane in this.dataSetPane.FindFormPanes)
            {
              if (findFormPane.FindFormName == defaultSet.FindFormName && findFormPane.SearchTextBinding != null && findFormPane.SearchTextBinding != string.Empty)
                flag = true;
            }
          }
        }
        else
        {
          if (this.dataSetPane.PaneVariants.Count > 0)
          {
            foreach (AdvancedDataSetPaneVariant paneVariant in this.dataSetPane.PaneVariants)
              this.AddAlternateVisual(paneVariant);
          }
          this.AvLabel.Visibility = Visibility.Visible;
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Hidden;
        }
        if (this.dataSetPane.DefaultSets.Count > 0)
        {
          this.showLabel.Visibility = Visibility.Visible;
          this.m_ShowComboBox.Visibility = Visibility.Visible;
        }
        else
        {
          this.showLabel.Visibility = Visibility.Collapsed;
          this.m_ShowComboBox.Visibility = Visibility.Collapsed;
          this.m_AdvBasicLink.Visibility = Visibility.Hidden;
          this.m_iconRefresh.Visibility = Visibility.Visible;
        }
        if (flag && this.dataSetPane.DefaultSets.Count > 0)
          this.andLabel.Visibility = Visibility.Visible;
        else
          this.andLabel.Visibility = Visibility.Collapsed;
        if (flag)
        {
          this.findLabel.Visibility = Visibility.Visible;
          this.m_FindTextBox.Visibility = Visibility.Visible;
          this.m_BtnGo.Visibility = Visibility.Visible;
        }
        else
        {
          this.findLabel.Visibility = Visibility.Collapsed;
          this.m_FindTextBox.Visibility = Visibility.Collapsed;
          this.m_BtnGo.Visibility = Visibility.Collapsed;
        }
        if (!string.IsNullOrEmpty(this.dataSetPane.SelectedDefaultSet))
        {
          foreach (IDefaultSetMapping defaultSet in this.dataSetPane.DefaultSets)
          {
            if (this.dataSetPane.SelectedDefaultSet == defaultSet.DefaultSetName)
            {
              if (this.dataSetPane.PaneVariants.Count > 0)
              {
                foreach (AdvancedDataSetPaneVariant paneVariant in this.dataSetPane.PaneVariants)
                {
                  if (paneVariant.Name == defaultSet.ListPaneVariantName)
                    this.AddAlternateVisual(paneVariant);
                }
              }
              else
              {
                this.AvLabel.Visibility = Visibility.Hidden;
                this.m_AlternateVisualShowComboBox.Visibility = Visibility.Hidden;
              }
            }
          }
        }
        else
        {
          this.AvLabel.Visibility = Visibility.Visible;
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Visible;
        }
      }
    }

    private void AddAlternateVisual(AdvancedListPaneVariant alpVariant)
    {
      if (alpVariant != null && alpVariant.AlternateListVisualization != null)
      {
        this.m_AlternateVisualShowComboBox.Items.Clear();
        this.m_AlternateVisualShowComboBox.Items.Add((object) "Standard");
        if (alpVariant.AlternateListVisualization.Count > 0)
        {
          if (!this.Listpane.UseIntegratedToolbar)
            this.AvLabel.Visibility = Visibility.Visible;
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Visible;
          foreach (AlternativeListVisualization listVisualization in alpVariant.AlternateListVisualization)
          {
            if (listVisualization.Title != null && !string.IsNullOrEmpty(listVisualization.Title.Text))
            {
              if (!string.IsNullOrEmpty(listVisualization.Title.Text))
                this.m_AlternateVisualShowComboBox.Items.Add((object) listVisualization.Title.Text);
              else
                this.m_AlternateVisualShowComboBox.Items.Add((object) listVisualization.Title.FallbackValue);
            }
            else
              this.m_AlternateVisualShowComboBox.Items.Add((object) listVisualization.Name.ToString());
          }
          foreach (IDefaultSetMapping defaultSet in this.Listpane.DefaultSets)
          {
            if (defaultSet.DefaultSetName == this.Listpane.SelectedDefaultSet)
            {
              if (!string.IsNullOrEmpty(defaultSet.AlternateVisualName))
              {
                foreach (AlternativeListVisualization listVisualization in alpVariant.AlternateListVisualization)
                {
                  if (listVisualization.Name == defaultSet.AlternateVisualName && listVisualization.Title != null)
                  {
                    if (!string.IsNullOrEmpty(listVisualization.Title.Text))
                      this.m_AlternateVisualShowComboBox.Text = listVisualization.Title.Text;
                    else if (!string.IsNullOrEmpty(listVisualization.Title.FallbackValue))
                      this.m_AlternateVisualShowComboBox.Text = listVisualization.Title.FallbackValue;
                  }
                }
              }
              else
                this.m_AlternateVisualShowComboBox.Text = "Standard";
            }
          }
        }
        else if (!this.Listpane.UseIntegratedToolbar)
        {
          this.AvLabel.Visibility = Visibility.Hidden;
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Hidden;
        }
        else
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Collapsed;
      }
      else
      {
        this.AvLabel.Visibility = Visibility.Hidden;
        if (!this.Listpane.UseIntegratedToolbar)
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Hidden;
        else
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Collapsed;
      }
    }

    private void AddAlternateVisual(AdvancedDataSetPaneVariant dspVariant)
    {
      if (dspVariant != null && dspVariant.AlternateVisualizations != null)
      {
        this.m_AlternateVisualShowComboBox.Items.Clear();
        this.m_AlternateVisualShowComboBox.Items.Add((object) "Standard");
        if (dspVariant.AlternateVisualizations.Count > 0)
        {
          this.AvLabel.Visibility = Visibility.Visible;
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Visible;
          foreach (AlternativeListVisualization alternateVisualization in dspVariant.AlternateVisualizations)
          {
            if (alternateVisualization.Title != null && !string.IsNullOrEmpty(alternateVisualization.Title.Text))
            {
              if (!string.IsNullOrEmpty(alternateVisualization.Title.Text))
                this.m_AlternateVisualShowComboBox.Items.Add((object) alternateVisualization.Title.Text);
              else
                this.m_AlternateVisualShowComboBox.Items.Add((object) alternateVisualization.Title.FallbackValue);
            }
            else
              this.m_AlternateVisualShowComboBox.Items.Add((object) alternateVisualization.Name.ToString());
          }
          if (this.Listpane != null)
          {
            foreach (IDefaultSetMapping defaultSet in this.Listpane.DefaultSets)
            {
              if (defaultSet.DefaultSetName == this.Listpane.SelectedDefaultSet)
              {
                if (!string.IsNullOrEmpty(defaultSet.AlternateVisualName))
                {
                  foreach (AlternativeListVisualization alternateVisualization in dspVariant.AlternateVisualizations)
                  {
                    if (alternateVisualization.Name == defaultSet.AlternateVisualName && alternateVisualization.Title != null)
                    {
                      if (!string.IsNullOrEmpty(alternateVisualization.Title.Text))
                        this.m_AlternateVisualShowComboBox.Text = alternateVisualization.Title.Text;
                      else if (!string.IsNullOrEmpty(alternateVisualization.Title.FallbackValue))
                        this.m_AlternateVisualShowComboBox.Text = alternateVisualization.Title.FallbackValue;
                    }
                  }
                }
                else
                  this.m_AlternateVisualShowComboBox.Text = "Standard";
              }
            }
          }
          else
          {
            if (this.dataSetPane == null)
              return;
            foreach (IDefaultSetMapping defaultSet in this.dataSetPane.DefaultSets)
            {
              if (defaultSet.DefaultSetName == this.dataSetPane.SelectedDefaultSet)
              {
                if (!string.IsNullOrEmpty(defaultSet.AlternateVisualName))
                {
                  foreach (AlternativeListVisualization alternateVisualization in dspVariant.AlternateVisualizations)
                  {
                    if (alternateVisualization.Name == defaultSet.AlternateVisualName && alternateVisualization.Title != null)
                    {
                      if (!string.IsNullOrEmpty(alternateVisualization.Title.Text))
                        this.m_AlternateVisualShowComboBox.Text = alternateVisualization.Title.Text;
                      else if (!string.IsNullOrEmpty(alternateVisualization.Title.FallbackValue))
                        this.m_AlternateVisualShowComboBox.Text = alternateVisualization.Title.FallbackValue;
                    }
                  }
                }
                else
                  this.m_AlternateVisualShowComboBox.Text = "Standard";
              }
            }
          }
        }
        else
        {
          this.AvLabel.Visibility = Visibility.Hidden;
          this.m_AlternateVisualShowComboBox.Visibility = Visibility.Hidden;
        }
      }
      else
      {
        this.AvLabel.Visibility = Visibility.Hidden;
        this.m_AlternateVisualShowComboBox.Visibility = Visibility.Hidden;
      }
    }

    private void AddElement(FrameworkElement element)
    {
      this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(100.0, GridUnitType.Auto)
      });
      this.Grid.Children.Add((UIElement) element);
      element.SetValue(Grid.ColumnProperty, (object) (this.Grid.ColumnDefinitions.Count - 1));
      element.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
    }

    private void UpdateAdvancedText()
    {
      string str = this.m_InitFormType == InitFormType.advanced ? "Basic" : "Advanced";
      if (!(this.m_AdvBasicLink.Text != str))
        return;
      this.m_AdvBasicLink.Text = str;
    }

    private void LoadDefaultSets()
    {
      if (this.Listpane != null)
      {
        if (this.Listpane.DefaultSets == null || this.Listpane.DefaultSets.Count <= 0)
          return;
        this.m_ShowComboBox.Items.Clear();
        foreach (IDefaultSetMapping defaultSet in this.Listpane.DefaultSets)
        {
          if (defaultSet.DefaultSetName != null && defaultSet.DefaultSetName != string.Empty)
            this.m_ShowComboBox.Items.Add((object) defaultSet.DefaultSetTitle);
        }
        if (this.Listpane.SelectedDefaultSet == null || !(this.Listpane.SelectedDefaultSet != string.Empty))
          return;
        foreach (OberonDefaultSet defaultSet in Utilities.GetImplementationController((IModelObject) this.Listpane).DefaultSets)
        {
          defaultSet.TitleChanged += new EventHandler(this.ds_TitleChanged);
          if (defaultSet.Name == this.Listpane.SelectedDefaultSet && defaultSet.TitleField != null && defaultSet.TitleField.Text != null)
          {
            this.m_ShowComboBox.Text = defaultSet.TitleField.Text;
            break;
          }
        }
      }
      else
      {
        if (this.dataSetPane == null || this.dataSetPane.DefaultSets == null || this.dataSetPane.DefaultSets.Count <= 0)
          return;
        this.m_ShowComboBox.Items.Clear();
        foreach (IDefaultSetMapping defaultSet in this.dataSetPane.DefaultSets)
        {
          if (defaultSet.DefaultSetName != null && defaultSet.DefaultSetName != string.Empty)
            this.m_ShowComboBox.Items.Add((object) defaultSet.DefaultSetTitle);
        }
      }
    }

    private void ds_TitleChanged(object sender, EventArgs e)
    {
      this.LoadDefaultSets();
    }

    private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.Listpane != null)
      {
        if (this.m_ShowComboBox.SelectedIndex == -1 || this.OnDefaultSetChanged == null || this.Listpane.DefaultSets.Count <= this.m_ShowComboBox.SelectedIndex)
          return;
        int selectedIndex = this.m_ShowComboBox.SelectedIndex;
        this.Listpane.SelectedDefaultSet = this.Listpane.DefaultSets[selectedIndex].DefaultSetName;
        if (this.Listpane.UseIntegratedToolbar)
          this.SetVisiblePropertiesForIntegratedToolbar();
        else
          this.SetVisibleProperties();
        this.OnDefaultSetChanged(this.Listpane.DefaultSets[selectedIndex]);
      }
      else
      {
        if (this.dataSetPane == null || this.m_ShowComboBox.SelectedIndex == -1 || (this.OnDefaultSetChanged == null || this.dataSetPane.DefaultSets.Count <= this.m_ShowComboBox.SelectedIndex))
          return;
        int selectedIndex = this.m_ShowComboBox.SelectedIndex;
        this.dataSetPane.SelectedDefaultSet = this.dataSetPane.DefaultSets[selectedIndex].DefaultSetName;
        if (this.Listpane.UseIntegratedToolbar)
          this.SetVisiblePropertiesForIntegratedToolbar();
        else
          this.SetVisibleProperties();
        this.OnDefaultSetChanged(this.dataSetPane.DefaultSets[selectedIndex]);
      }
    }

    private void OnAlternateVisualSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.Listpane != null)
      {
        if (!this.m_Initialize && !string.IsNullOrEmpty(this.Listpane.SelectedDefaultSet))
        {
          foreach (IDefaultSetMapping defaultSet in this.Listpane.DefaultSets)
          {
            if (this.Listpane.SelectedDefaultSet == defaultSet.DefaultSetName)
            {
              foreach (AdvancedListPaneVariant paneVariant in this.Listpane.PaneVariants)
              {
                if (paneVariant.Name == defaultSet.ListPaneVariantName)
                {
                  if (this.m_AlternateVisualShowComboBox.SelectedIndex > 0 && paneVariant.AlternateListVisualization != null && paneVariant.AlternateListVisualization.Count >= this.m_AlternateVisualShowComboBox.SelectedIndex)
                    this.OnAlternateListVisualChanged(paneVariant, paneVariant.AlternateListVisualization[this.m_AlternateVisualShowComboBox.SelectedIndex - 1]);
                  else
                    this.OnAlternateListVisualChanged(paneVariant, (AlternativeListVisualization) null);
                }
              }
            }
          }
        }
        else
        {
          if (!string.IsNullOrEmpty(this.Listpane.SelectedDefaultSet))
            return;
          foreach (AdvancedListPaneVariant paneVariant in this.Listpane.PaneVariants)
          {
            if (this.m_AlternateVisualShowComboBox.SelectedIndex > 0 && paneVariant.AlternateListVisualization != null && paneVariant.AlternateListVisualization.Count >= this.m_AlternateVisualShowComboBox.SelectedIndex)
              this.OnAlternateListVisualChanged(paneVariant, paneVariant.AlternateListVisualization[this.m_AlternateVisualShowComboBox.SelectedIndex - 1]);
            else
              this.OnAlternateListVisualChanged(paneVariant, (AlternativeListVisualization) null);
          }
        }
      }
      else
      {
        if (this.dataSetPane == null)
          return;
        if (!this.m_Initialize && !string.IsNullOrEmpty(this.dataSetPane.SelectedDefaultSet))
        {
          foreach (IDefaultSetMapping defaultSet in this.dataSetPane.DefaultSets)
          {
            if (this.dataSetPane.SelectedDefaultSet == defaultSet.DefaultSetName)
            {
              foreach (AdvancedDataSetPaneVariant paneVariant in this.dataSetPane.PaneVariants)
              {
                if (paneVariant.Name == defaultSet.ListPaneVariantName)
                {
                  if (this.m_AlternateVisualShowComboBox.SelectedIndex > 0 && paneVariant.AlternateVisualizations != null && paneVariant.AlternateVisualizations.Count >= this.m_AlternateVisualShowComboBox.SelectedIndex)
                    this.OnAlternateDataSetVisualChanged(paneVariant, paneVariant.AlternateVisualizations[this.m_AlternateVisualShowComboBox.SelectedIndex - 1]);
                  else
                    this.OnAlternateDataSetVisualChanged(paneVariant, (AlternativeListVisualization) null);
                }
              }
            }
          }
        }
        else
        {
          if (!string.IsNullOrEmpty(this.dataSetPane.SelectedDefaultSet))
            return;
          foreach (AdvancedDataSetPaneVariant paneVariant in this.dataSetPane.PaneVariants)
          {
            if (this.m_AlternateVisualShowComboBox.SelectedIndex > 0 && paneVariant.AlternateVisualizations != null && paneVariant.AlternateVisualizations.Count >= this.m_AlternateVisualShowComboBox.SelectedIndex)
              this.OnAlternateDataSetVisualChanged(paneVariant, paneVariant.AlternateVisualizations[this.m_AlternateVisualShowComboBox.SelectedIndex - 1]);
            else
              this.OnAlternateDataSetVisualChanged(paneVariant, (AlternativeListVisualization) null);
          }
        }
      }
    }

    private void OnButtonClicked(object sender, RoutedEventArgs e)
    {
      this.m_InitFormType = this.m_AdvBasicLink.Text == "Basic" ? InitFormType.basic : InitFormType.advanced;
      if (this.Listpane == null)
        return;
      IDefaultSetMapping defaultSetMapping = this.Listpane.DefaultSets[0];
      foreach (IDefaultSetMapping defaultSet in this.Listpane.DefaultSets)
      {
        if (this.m_ShowComboBox != null)
        {
          if (defaultSet.DefaultSetTitle == this.m_ShowComboBox.Text)
            defaultSetMapping = defaultSet;
        }
        else
        {
          defaultSetMapping = this.Listpane.DefaultSets[0];
          break;
        }
      }
      AlternativeListVisualization alVisual = (AlternativeListVisualization) null;
      foreach (IDefaultSetMapping defaultSet in this.Listpane.DefaultSets)
      {
        if (this.Listpane.SelectedDefaultSet == defaultSet.DefaultSetName)
        {
          foreach (AdvancedListPaneVariant paneVariant in this.Listpane.PaneVariants)
          {
            if (paneVariant.Name == defaultSet.ListPaneVariantName)
              alVisual = this.m_AlternateVisualShowComboBox.SelectedIndex <= 0 || paneVariant.AlternateListVisualization == null || paneVariant.AlternateListVisualization.Count < this.m_AlternateVisualShowComboBox.SelectedIndex ? (AlternativeListVisualization) null : paneVariant.AlternateListVisualization[this.m_AlternateVisualShowComboBox.SelectedIndex - 1];
          }
        }
      }
      if (defaultSetMapping.FindFormName != null && defaultSetMapping.FindFormName != string.Empty && this.OnFFLinkClicked != null)
      {
        this.UpdateAdvancedText();
        this.OnFFLinkClicked(defaultSetMapping, this.m_InitFormType, alVisual);
      }
      else
      {
        if (MessageBox.Show("There is no Advanced Findform assigned so far. Do you want to create and assign one?", "Alert", MessageBoxButton.YesNoCancel) != MessageBoxResult.Yes)
          return;
        this.Listpane.CreateFindForm(defaultSetMapping);
        if (defaultSetMapping.FindFormName == null || !(defaultSetMapping.FindFormName != string.Empty) || this.OnFFLinkClicked == null)
          return;
        this.UpdateAdvancedText();
        this.OnFFLinkClicked(defaultSetMapping, this.m_InitFormType, alVisual);
      }
    }

    private void OnLinkClicked(object sender, EventArgs e)
    {
      this.m_InitFormType = this.m_AdvBasicLink.Text == "Basic" ? InitFormType.basic : InitFormType.advanced;
      if (this.Listpane != null)
      {
        IDefaultSetMapping defaultSetMapping = this.Listpane.DefaultSets[0];
        foreach (IDefaultSetMapping defaultSet in this.Listpane.DefaultSets)
        {
          if (this.m_ShowComboBox != null)
          {
            if (defaultSet.DefaultSetTitle == this.m_ShowComboBox.Text)
              defaultSetMapping = defaultSet;
          }
          else
          {
            defaultSetMapping = this.Listpane.DefaultSets[0];
            break;
          }
        }
        AlternativeListVisualization alVisual = (AlternativeListVisualization) null;
        foreach (IDefaultSetMapping defaultSet in this.Listpane.DefaultSets)
        {
          if (this.Listpane.SelectedDefaultSet == defaultSet.DefaultSetName)
          {
            foreach (AdvancedListPaneVariant paneVariant in this.Listpane.PaneVariants)
            {
              if (paneVariant.Name == defaultSet.ListPaneVariantName)
                alVisual = this.m_AlternateVisualShowComboBox.SelectedIndex <= 0 || paneVariant.AlternateListVisualization == null || paneVariant.AlternateListVisualization.Count < this.m_AlternateVisualShowComboBox.SelectedIndex ? (AlternativeListVisualization) null : paneVariant.AlternateListVisualization[this.m_AlternateVisualShowComboBox.SelectedIndex - 1];
            }
          }
        }
        if (defaultSetMapping.FindFormName != null && defaultSetMapping.FindFormName != string.Empty && this.OnFFLinkClicked != null)
        {
          this.UpdateAdvancedText();
          this.OnFFLinkClicked(defaultSetMapping, this.m_InitFormType, alVisual);
        }
        else
        {
          if (MessageBox.Show("There is no Advanced Findform assigned so far. Do you want to create and assign one?", "Alert", MessageBoxButton.YesNoCancel) != MessageBoxResult.Yes)
            return;
          this.Listpane.CreateFindForm(defaultSetMapping);
          if (defaultSetMapping.FindFormName == null || !(defaultSetMapping.FindFormName != string.Empty) || this.OnFFLinkClicked == null)
            return;
          this.UpdateAdvancedText();
          this.OnFFLinkClicked(defaultSetMapping, this.m_InitFormType, alVisual);
        }
      }
      else
      {
        if (this.dataSetPane == null)
          return;
        IDefaultSetMapping defaultSetMapping = this.dataSetPane.DefaultSets[0];
        foreach (IDefaultSetMapping defaultSet in this.dataSetPane.DefaultSets)
        {
          if (this.m_ShowComboBox != null)
          {
            if (defaultSet.DefaultSetTitle == this.m_ShowComboBox.Text)
              defaultSetMapping = defaultSet;
          }
          else
          {
            defaultSetMapping = this.dataSetPane.DefaultSets[0];
            break;
          }
        }
        if (defaultSetMapping.FindFormName != null && defaultSetMapping.FindFormName != string.Empty && this.OnFFLinkClicked != null)
        {
          this.UpdateAdvancedText();
          this.OnFFLinkClicked(defaultSetMapping, this.m_InitFormType, (AlternativeListVisualization) null);
        }
        else
        {
          if (MessageBox.Show("There is no Advanced Findform assigned so far. Do you want to create and assign one?", "Alert", MessageBoxButton.YesNoCancel) != MessageBoxResult.Yes)
            return;
          this.dataSetPane.CreateFindForm(defaultSetMapping);
          if (defaultSetMapping.FindFormName == null || !(defaultSetMapping.FindFormName != string.Empty) || this.OnFFLinkClicked == null)
            return;
          this.UpdateAdvancedText();
          this.OnFFLinkClicked(defaultSetMapping, this.m_InitFormType, (AlternativeListVisualization) null);
        }
      }
    }
  }
}
