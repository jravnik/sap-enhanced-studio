﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualServiceMapPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ServiceMapPaneControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualServiceMapPane : VisualBasePane
  {
    private int m_GridRowCount;
    private Grid m_ServiceMapGroupsContainer;

    internal ServiceMapPane ServiceMapPane
    {
      get
      {
        return this.m_ModelObject as ServiceMapPane;
      }
    }

    public VisualServiceMapPane(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "ServiceMapPane";
      }
    }

    public override void ConstructBodyElement()
    {
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.BodyElementGrid, 150.0, 150.0, GridUnitType.Auto);
      this.m_ServiceMapGroupsContainer = new Grid();
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.m_ServiceMapGroupsContainer, 0, 0);
      this.ConstructServiceMapGroupsGrid();
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is ServiceMapGroup)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed because of unknown reasons.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (!(e.PropertyName == ServiceMapPane.ColumnsProperty) && !(e.PropertyName == ServiceMapPane.ServiceMapGroupsProperty))
          return;
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of Formpane was not reflected in UI because of unknown reasons.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == null || !(source.Header.ToString() == "Delete") || this.ServiceMapPane.Parent == null)
        return;
      this.ServiceMapPane.Parent.RemoveModelObject((IModelObject) this.ServiceMapPane);
    }

    private void ConstructServiceMapGroupsGrid()
    {
      if (this.ServiceMapPane.ServiceMapGroups == null || this.ServiceMapPane.ServiceMapGroups.Count <= 0)
        return;
      int panelRowCount = this.GetPanelRowCount(this.ServiceMapPane.ServiceMapGroups.Count, this.ServiceMapPane.Columns);
      this.m_GridRowCount = this.GetGridRowCount();
      List<StackPanel> stackPanelList = new List<StackPanel>();
      GridUtil.AddRowDef(this.m_ServiceMapGroupsContainer, 1.0, GridUnitType.Star);
      int num1 = 0;
      int index = 0;
      for (int col = 0; col < this.ServiceMapPane.Columns; ++col)
      {
        GridUtil.AddColumnDef(this.m_ServiceMapGroupsContainer, 1.0, GridUnitType.Star);
        StackPanel stackPanel = new StackPanel();
        GridUtil.PlaceElement(this.m_ServiceMapGroupsContainer, (UIElement) stackPanel, 0, col);
        stackPanelList.Add(stackPanel);
      }
      bool flag = true;
      int num2 = 0;
      foreach (IModelObject serviceMapGroup in this.ServiceMapPane.ServiceMapGroups)
      {
        VisualServiceMapGroup visualServiceMapGroup = new VisualServiceMapGroup(serviceMapGroup, (BaseVisualControl) this);
        if (!flag && num1 == panelRowCount)
        {
          num1 = 0;
          ++index;
          panelRowCount = this.GetPanelRowCount(this.ServiceMapPane.ServiceMapGroups.Count - num2, this.ServiceMapPane.Columns - index);
        }
        stackPanelList[index].Children.Add((UIElement) visualServiceMapGroup);
        ++num2;
        ++num1;
        flag = false;
      }
    }

    private int GetGridRowCount()
    {
      return Convert.ToInt32(Math.Ceiling(Convert.ToDouble(this.ServiceMapPane.ServiceMapGroups.Count) / Convert.ToDouble(this.ServiceMapPane.Columns)));
    }

    private int GetPanelRowCount(int itemsRemaining, int ColumnCount)
    {
      return Convert.ToInt32(Math.Ceiling(Convert.ToDouble(itemsRemaining) / Convert.ToDouble(ColumnCount)));
    }
  }
}
