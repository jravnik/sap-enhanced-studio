﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualCustomPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public partial class VisualCustomPane : VisualBasePane, IComponentConnector
  {
    //internal Rectangle rectangle1;
    //internal Label label1;
    //internal Label label2;
    //internal TextBox txtAssemblyName;
    //internal Label label3;
    //internal TextBox txtTypeName;
    //private bool _contentLoaded;

    public VisualCustomPane(IModelObject model)
      : base(model)
    {
      this.InitializeComponent();
      this.DisplayData();
    }

    public override void ConstructBodyElement()
    {
    }

    private void txtAssemblyName_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      this.SwitchOffReadOnly(this.txtAssemblyName);
    }

    private void txtAssemblyName_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
    {
      this.UpdateAssemblyName(this.txtAssemblyName.Text);
      this.SwitchOnReadonly(this.txtAssemblyName);
    }

    private void txtAssemblyName_LostFocus(object sender, RoutedEventArgs e)
    {
      this.UpdateAssemblyName(this.txtAssemblyName.Text);
      this.SwitchOnReadonly(this.txtAssemblyName);
    }

    private void txtTypeName_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      this.SwitchOffReadOnly(this.txtTypeName);
    }

    private void txtTypeName_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
    {
      this.UpdateTypeName(this.txtTypeName.Text);
      this.SwitchOnReadonly(this.txtTypeName);
    }

    private void txtTypeName_LostFocus(object sender, RoutedEventArgs e)
    {
      this.UpdateTypeName(this.txtTypeName.Text);
      this.SwitchOnReadonly(this.txtTypeName);
    }

    private void SwitchOnReadonly(TextBox textControl)
    {
      textControl.IsReadOnly = true;
      textControl.Background = (Brush) Brushes.LightGray;
    }

    private void SwitchOffReadOnly(TextBox textControl)
    {
      textControl.IsReadOnly = false;
      textControl.Background = (Brush) Brushes.White;
    }

    private void UpdateTypeName(string typeName)
    {
      (this.Model as CustomPane).Assembly.TypeName = typeName;
    }

    private void UpdateAssemblyName(string assemblyName)
    {
      (this.Model as CustomPane).Assembly.AssemblyName = assemblyName;
    }

    private void DisplayData()
    {
      if ((this.Model as CustomPane).Assembly == null)
        return;
      if ((this.Model as CustomPane).Assembly.AssemblyName != null)
        this.txtAssemblyName.Text = (this.Model as CustomPane).Assembly.AssemblyName;
      if ((this.Model as CustomPane).Assembly.TypeName == null)
        return;
      this.txtTypeName.Text = (this.Model as CustomPane).Assembly.TypeName;
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/designers/views/panes/visualcustompane.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.rectangle1 = (Rectangle) target;
    //      break;
    //    case 2:
    //      this.label1 = (Label) target;
    //      break;
    //    case 3:
    //      this.label2 = (Label) target;
    //      break;
    //    case 4:
    //      this.txtAssemblyName = (TextBox) target;
    //      this.txtAssemblyName.MouseDoubleClick += new MouseButtonEventHandler(this.txtAssemblyName_MouseDoubleClick);
    //      this.txtAssemblyName.LostKeyboardFocus += new KeyboardFocusChangedEventHandler(this.txtAssemblyName_LostKeyboardFocus);
    //      this.txtAssemblyName.LostFocus += new RoutedEventHandler(this.txtAssemblyName_LostFocus);
    //      break;
    //    case 5:
    //      this.label3 = (Label) target;
    //      break;
    //    case 6:
    //      this.txtTypeName = (TextBox) target;
    //      this.txtTypeName.MouseDoubleClick += new MouseButtonEventHandler(this.txtTypeName_MouseDoubleClick);
    //      this.txtTypeName.LostKeyboardFocus += new KeyboardFocusChangedEventHandler(this.txtTypeName_LostKeyboardFocus);
    //      this.txtTypeName.LostFocus += new RoutedEventHandler(this.txtTypeName_LostFocus);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
