﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualTabStrip
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Foundation.Controls;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.TabstripControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualTabStrip : VisualBasePane
  {
    private const string RESOURCE_TAB_FOOTER_LEFT = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_footer_left.png,5,3";
    private const string RESOURCE_TAB_FOOTER_CENTER = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_footer_center.png,1,3";
    private const string RESOURCE_TAB_FOOTER_RIGHT = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_footer_right.png,3,3";
    private const string CONTEXTMENU_ADD_INFO = "Add Info Tab";
    private const string CONTEXTMENU_ADD_NOTES = "Add Notes Tab";
    private const string CONTEXTMENU_ADD_ATTACHMENTS = "Add Attachments Tab";
    private const string CONTEXTMENU_ADD_PEOPLE = "Add People Tab";
    private TabLayoutPanel m_TabLayoutPanel;
    private List<VisualTabstripItem> m_VisualTabStripItems;
    private BaseVisualControl tabContent;
    private int selectedTabIndex;
    private int visibleTabStartIndex;
    private int visibleTabEndIndex;

    internal TabStrip TabStrip
    {
      get
      {
        return this.m_ModelObject as TabStrip;
      }
    }

    private TabContainerPane TabContainer
    {
      get
      {
        return this.m_ModelObject as TabContainerPane;
      }
    }

    public override string SelectionText
    {
      get
      {
        return "TabStrip";
      }
    }

    public VisualTabStrip(IModelObject model)
      : base(model)
    {
      this.Margin = new Thickness(1.0);
      this.m_VisualTabStripItems = new List<VisualTabstripItem>();
      this.LoadView();
    }

    public override void ConstructBodyElement()
    {
      try
      {
        this.InitializeGrid();
        int index = 0;
        foreach (IModelObject tab in this.TabStrip.Tabs)
        {
          VisualTabstripItem visualTabstripItem = new VisualTabstripItem(tab, index, this);
          this.m_TabLayoutPanel.AddChild((UIElement) visualTabstripItem);
          visualTabstripItem.TabSelected += new System.EventHandler<EventArgs>(this.SelectedTabChanged);
          visualTabstripItem.TabUnselected += new System.EventHandler<EventArgs>(this.OnTabUnselected);
          this.m_VisualTabStripItems.Add(visualTabstripItem);
          ++index;
        }
        this.visibleTabStartIndex = 0;
        this.visibleTabEndIndex = this.m_VisualTabStripItems.Count - 1;
        this.selectedTabIndex = 0;
        this.m_VisualTabStripItems[this.visibleTabStartIndex].SelectMe();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to create TabStrip UI.", ex));
      }
    }

    public override void Terminate()
    {
      try
      {
        foreach (VisualTabstripItem visualTabStripItem in this.m_VisualTabStripItems)
        {
          visualTabStripItem.TabSelected -= new System.EventHandler<EventArgs>(this.SelectedTabChanged);
          visualTabStripItem.TabUnselected -= new System.EventHandler<EventArgs>(this.OnTabUnselected);
          visualTabStripItem.Terminate();
        }
        this.m_VisualTabStripItems.Clear();
        base.Terminate();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Termination of TabstripItems failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelAddedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (!(e.PropertyName == TabStrip.TabsProperty))
          return;
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of TabStrip was not reflected in UI because of unknown reasons.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        base.AddContextMenuItems();
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Add Tab");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for TabStrip.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      try
      {
        base.HandleContextMenuItemClick(source);
        if (source.Header == (object) "Delete")
        {
          bool flag = true;
          if (this.TabStrip.Parent == null)
            return;
          foreach (GridLayout tab in this.TabStrip.Tabs)
          {
            foreach (PaneContainer paneContainer in tab.PaneContainers)
            {
              if (ValidationManager.GetInstance().IsDeletable((IExtensible) paneContainer))
              {
                foreach (BasePane basePane in paneContainer.Items)
                {
                  if (basePane is FormPane)
                  {
                    foreach (IExtensible sectionGroup in (basePane as FormPane).SectionGroups)
                    {
                      if (!ValidationManager.GetInstance().IsDeletable(sectionGroup))
                      {
                        flag = false;
                        break;
                      }
                    }
                  }
                  else if (basePane is AdvancedListPane)
                  {
                    foreach (AdvancedListPaneVariant paneVariant in (basePane as AdvancedListPane).PaneVariants)
                    {
                      if (!ValidationManager.GetInstance().IsDeletable((IExtensible) paneVariant.ListDefinition))
                        flag = false;
                    }
                  }
                }
              }
              else
                flag = false;
            }
          }
          if (flag)
          {
            this.TabStrip.Parent.RemoveModelObject((IModelObject) this.TabStrip);
          }
          else
          {
            int num = (int) DisplayMessage.Show(Resource.ElementNotDeletable, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
          }
        }
        else
        {
          if (source.Header != (object) "Add Tab")
            return;
          if (this.TabContainer != null)
          {
            this.TabContainer.AddTab(TabAppearanceType.none);
          }
          else
          {
            if (this.TabStrip == null)
              return;
            this.TabStrip.AddTab();
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    private void InitializeGrid()
    {
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      System.Windows.Controls.Image image1 = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image1, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_footer_left.png,5,3");
      image1.VerticalAlignment = VerticalAlignment.Bottom;
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) image1, 0, 0);
      System.Windows.Controls.Image image2 = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image2, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_footer_center.png,1,3");
      this.m_TabLayoutPanel = new TabLayoutPanel();
      this.m_TabLayoutPanel.BackgroundImage = image2;
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.m_TabLayoutPanel, 0, 1, 1, 2);
    }

    private void SelectVisualTab(VisualTabstripItem VisualTabToSelect)
    {
      if (VisualTabToSelect == null)
        return;
      if (this.selectedTabIndex != VisualTabToSelect.Index)
        this.m_VisualTabStripItems[this.selectedTabIndex].UnselectTab();
      this.selectedTabIndex = VisualTabToSelect.Index;
      if (this.selectedTabIndex != 0)
        this.m_VisualTabStripItems[this.selectedTabIndex - 1].SelectPreviousTab();
      if (this.selectedTabIndex != this.m_VisualTabStripItems.Count - 1)
        this.m_VisualTabStripItems[this.selectedTabIndex + 1].SelectNextTab();
      this.LoadTabContent(VisualTabToSelect.Tab);
    }

    private void LoadTabContent(Tab tab)
    {
      try
      {
        if (this.tabContent != null)
        {
          this.tabContent.Terminate();
          this.BodyElementGrid.Children.Remove((UIElement) this.tabContent);
        }
        this.tabContent = (BaseVisualControl) new VisualLayout((IModelObject) tab);
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.tabContent, 1, 1, 1, 3);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading the TabContent fialed.", ex));
      }
    }

    private void OnTabUnselected(object sender, EventArgs args)
    {
      VisualTabstripItem visualTabstripItem = sender as VisualTabstripItem;
      if (visualTabstripItem.Index != 0)
        this.m_VisualTabStripItems[visualTabstripItem.Index - 1].UnSelectPreviousTab();
      if (this.selectedTabIndex == this.m_VisualTabStripItems.Count - 1)
        return;
      this.m_VisualTabStripItems[visualTabstripItem.Index + 1].UnSelectNextTab();
    }

    private void SelectedTabChanged(object sender, EventArgs args)
    {
      try
      {
        VisualTabstripItem VisualTabToSelect = sender as VisualTabstripItem;
        if (VisualTabToSelect == null)
          return;
        this.SelectVisualTab(VisualTabToSelect);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Selecting the Tab failed.", ex));
      }
    }
  }
}
