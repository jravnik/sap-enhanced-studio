﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarPaginator
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls
{
  internal class CalendarPaginator : UserControl
  {
    private StackPanel container;
    private Image Next;
    private Image Previous;
    private Image Forward;
    private Image Backward;
    private Border borderNext;
    private Border borderPrevious;
    private Border borderForward;
    private Border borderBackward;

    public CalendarPaginator()
    {
      this.container = new StackPanel();
      this.container.Orientation = Orientation.Horizontal;
      this.container.VerticalAlignment = VerticalAlignment.Center;
      this.Next = new Image();
      this.Next.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.arrow_right);
      this.Next.Width = this.Next.Height = 10.0;
      this.borderNext = new Border();
      this.borderNext.CornerRadius = new CornerRadius(3.0);
      this.borderNext.BorderThickness = new Thickness(1.0);
      this.borderNext.VerticalAlignment = VerticalAlignment.Center;
      this.borderNext.Padding = new Thickness(4.0);
      this.borderNext.Margin = new Thickness(0.0, 5.0, 0.0, 5.0);
      this.borderNext.Child = (UIElement) this.Next;
      this.Previous = new Image();
      this.Previous.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.arrow_left);
      this.Previous.Width = this.Previous.Height = 10.0;
      this.borderPrevious = new Border();
      this.borderPrevious.CornerRadius = new CornerRadius(3.0);
      this.borderPrevious.BorderThickness = new Thickness(1.0);
      this.borderPrevious.VerticalAlignment = VerticalAlignment.Center;
      this.borderPrevious.Padding = new Thickness(4.0);
      this.borderPrevious.Child = (UIElement) this.Previous;
      this.borderNext.Margin = new Thickness(0.0, 5.0, 0.0, 5.0);
      this.Forward = new Image();
      this.Forward.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.fwd);
      this.Forward.Width = this.Forward.Height = 10.0;
      this.borderForward = new Border();
      this.borderForward.CornerRadius = new CornerRadius(3.0);
      this.borderForward.BorderThickness = new Thickness(1.0);
      this.borderForward.VerticalAlignment = VerticalAlignment.Center;
      this.borderForward.Padding = new Thickness(3.0);
      this.borderForward.Margin = new Thickness(1.0, 5.0, 1.0, 5.0);
      this.borderForward.Child = (UIElement) this.Forward;
      this.Backward = new Image();
      this.Backward.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.back);
      this.Backward.Width = this.Backward.Height = 10.0;
      this.borderBackward = new Border();
      this.borderBackward.CornerRadius = new CornerRadius(3.0);
      this.borderBackward.BorderThickness = new Thickness(1.0);
      this.borderBackward.VerticalAlignment = VerticalAlignment.Center;
      this.borderBackward.Padding = new Thickness(3.0);
      this.borderBackward.Margin = new Thickness(2.0, 5.0, 1.0, 5.0);
      this.borderBackward.Child = (UIElement) this.Backward;
      this.container.Children.Add((UIElement) this.borderBackward);
      this.container.Children.Add((UIElement) this.borderPrevious);
      this.container.Children.Add((UIElement) this.borderNext);
      this.container.Children.Add((UIElement) this.borderForward);
      this.Content = (object) this.container;
    }
  }
}
