﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.VisualLegend
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarLegendControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls.CalendarViewElements.LegendElements;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls
{
  internal class VisualLegend : BaseSelectableControl
  {
    private Grid m_Container;
    private VisualMultipleLegend m_MultipleLegendControl;

    public VisualLegend(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_Container = new Grid();
      this.Content = (object) this.m_Container;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "Legend";
      }
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.m_Container.RowDefinitions.Add(new RowDefinition());
        this.AddMultipleLegend((this.Model as Legend).MultipleLegendItem);
      }
      catch (Exception ex)
      {
      }
    }

    private void AddMultipleLegend(MultipleLegendItem multipleLegend)
    {
      if (multipleLegend == null)
        return;
      GridUtil.AddRowDef(this.m_Container, 1.0, GridUnitType.Star);
      this.m_MultipleLegendControl = new VisualMultipleLegend((IModelObject) multipleLegend);
      this.m_MultipleLegendControl.VerticalAlignment = VerticalAlignment.Top;
      this.m_MultipleLegendControl.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.m_MultipleLegendControl.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.m_Container, (UIElement) this.m_MultipleLegendControl, 0, 0);
    }
  }
}
