﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarMonthView.VisualMonthViewCellControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarMonthView
{
  public class VisualMonthViewCellControl : UserControl
  {
    private static readonly Brush brushEnabledColor = (Brush) new SolidColorBrush(Colors.White);
    private static readonly Brush brushDisabledColor = (Brush) new SolidColorBrush(Colors.Gray);
    private static readonly Brush normalBorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 100, (byte) 100, (byte) 100));
    private static readonly Brush invalidDay = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 208, (byte) 214, (byte) 220));
    private static readonly Brush cellSelected = (Brush) CalendarUtils.CreateBrush(Color.FromArgb((byte) 175, (byte) 245, (byte) 173, (byte) 29), Color.FromArgb(byte.MaxValue, (byte) 245, (byte) 173, (byte) 29), Color.FromArgb((byte) 175, (byte) 245, (byte) 173, (byte) 29));
    private Border cellBorder;
    private Grid partContainer;
    private TextBlock dateText;
    private Rectangle baseRectangle;
    private int row;
    private int column;

    internal VisualMonthViewCellControl()
    {
      this.baseRectangle = new Rectangle();
      this.baseRectangle.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.baseRectangle.VerticalAlignment = VerticalAlignment.Stretch;
      this.baseRectangle.StrokeThickness = 0.0;
      this.baseRectangle.Margin = new Thickness(0.0);
      this.baseRectangle.Opacity = 100.0;
      this.baseRectangle.Fill = VisualMonthViewCellControl.brushEnabledColor;
      this.cellBorder = new Border();
      this.cellBorder.BorderThickness = new Thickness(0.1);
      this.cellBorder.BorderBrush = VisualMonthViewCellControl.normalBorderBrush;
      this.dateText = new TextBlock();
      this.dateText.HorizontalAlignment = HorizontalAlignment.Left;
      this.dateText.VerticalAlignment = VerticalAlignment.Bottom;
      this.dateText.FontSize = CalendarUtils.MONTH_VIEW_DATE_FONT_SIZE;
      this.cellBorder.Child = (UIElement) this.dateText;
      this.partContainer = new Grid();
      this.partContainer.Children.Add((UIElement) this.baseRectangle);
      this.partContainer.Children.Add((UIElement) this.cellBorder);
      this.Content = (object) this.partContainer;
    }

    public int Row
    {
      get
      {
        return this.row;
      }
      set
      {
        this.row = value;
      }
    }

    public int Column
    {
      get
      {
        return this.column;
      }
      set
      {
        this.column = value;
      }
    }
  }
}
