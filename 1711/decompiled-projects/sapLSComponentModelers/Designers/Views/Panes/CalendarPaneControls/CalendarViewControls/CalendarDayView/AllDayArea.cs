﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarDayView.AllDayArea
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarDayView
{
  public class AllDayArea : UserControl
  {
    private Grid container;
    private int numCells;
    private List<AllDayArea.AllDayRow> entryMarker;
    private AllDayAreaCell[] allDayAreaCells;

    public AllDayArea(int numCells)
    {
      this.container = new Grid();
      this.Content = (object) this.container;
      this.numCells = numCells;
      this.entryMarker = new List<AllDayArea.AllDayRow>();
      this.allDayAreaCells = new AllDayAreaCell[numCells];
      this.container.RowDefinitions.Add(new RowDefinition());
      this.entryMarker.Add(new AllDayArea.AllDayRow(numCells));
      for (int index = 0; index < numCells; ++index)
      {
        this.container.ColumnDefinitions.Add(new ColumnDefinition());
        AllDayAreaCell allDayAreaCell = new AllDayAreaCell();
        this.container.Children.Add((UIElement) allDayAreaCell);
        allDayAreaCell.SetValue(Grid.RowProperty, (object) 0);
        allDayAreaCell.SetValue(Grid.ColumnProperty, (object) index);
        this.allDayAreaCells[index] = allDayAreaCell;
      }
    }

    internal class AllDayRow
    {
      private bool[] cells;

      internal AllDayRow(int numCols)
      {
        this.cells = new bool[numCols];
      }

      public bool HasEntry(int startCol, int numCols)
      {
        bool flag = false;
        if (this.cells != null)
        {
          for (int index = startCol; index < numCols + startCol; ++index)
          {
            if (this.cells[index])
            {
              flag = true;
              break;
            }
          }
        }
        return flag;
      }

      public void MarkEntry(int startCol, int numCols)
      {
        if (this.cells == null)
          return;
        for (int index = startCol; index < numCols + startCol; ++index)
          this.cells[index] = true;
      }
    }
  }
}
