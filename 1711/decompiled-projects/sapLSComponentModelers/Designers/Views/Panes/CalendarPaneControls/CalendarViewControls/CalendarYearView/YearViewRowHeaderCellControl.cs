﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarYearView.YearViewRowHeaderCellControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarYearView
{
  public class YearViewRowHeaderCellControl : UserControl
  {
    private TextBlock text;
    private Border cellBorder;

    internal YearViewRowHeaderCellControl(string headerText)
    {
      this.text = new TextBlock();
      this.text.HorizontalAlignment = HorizontalAlignment.Left;
      this.text.VerticalAlignment = VerticalAlignment.Center;
      this.text.Margin = new Thickness(10.0, 0.0, 10.0, 0.0);
      this.text.Text = headerText;
      this.cellBorder = new Border();
      this.cellBorder.Background = (Brush) CalendarUtils.CreateBrush(Color.FromArgb((byte) 55, byte.MaxValue, byte.MaxValue, byte.MaxValue), Color.FromArgb(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue), Color.FromArgb((byte) 55, byte.MaxValue, byte.MaxValue, byte.MaxValue));
      this.cellBorder.BorderThickness = new Thickness(0.4, 0.1, 0.4, 0.1);
      this.cellBorder.Child = (UIElement) this.text;
      this.cellBorder.BorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 100, (byte) 100, (byte) 100));
      this.Content = (object) this.cellBorder;
    }

    public string RowHeader
    {
      set
      {
        this.text.Text = value;
      }
    }
  }
}
