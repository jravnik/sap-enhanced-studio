﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.VisualCalendarYearView
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarYearView;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls
{
  internal class VisualCalendarYearView : VisualAbstractCalendarView
  {
    private static double MONTH_COLUMN_WIDTH = 150.0;
    private static double COLUMN_MIN_WIDTH = 20.0;
    private static double ROW_MIN_HEIGHT = 20.0;
    private static int MAX_COLUMNS = 31;
    private Grid container;
    private List<YearViewRowHeaderCellControl> rowHeaders;
    private YearViewCellControl[,] cells;
    private int visibleMonthCount;

    public VisualCalendarYearView(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.container = new Grid();
      this.Content = (object) this.container;
      this.LoadView();
    }

    protected SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls.CalendarViewElements.CalendarYearView CalendarYearview
    {
      get
      {
        return this.m_ModelObject as SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls.CalendarViewElements.CalendarYearView;
      }
    }

    public override void LoadView()
    {
      base.LoadView();
      this.CreateCalendarLayout();
    }

    public override string SelectionText
    {
      get
      {
        return "CalendarYearView";
      }
    }

    protected void CreateCalendarLayout()
    {
      this.visibleMonthCount = 12;
      this.container.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(100.0, GridUnitType.Auto),
        MinWidth = VisualCalendarYearView.MONTH_COLUMN_WIDTH
      });
      this.container.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(VisualCalendarYearView.ROW_MIN_HEIGHT, GridUnitType.Pixel)
      });
      YearViewColumnHeaderCellControl headerCellControl1 = new YearViewColumnHeaderCellControl("");
      this.container.Children.Add((UIElement) headerCellControl1);
      headerCellControl1.SetValue(Grid.RowProperty, (object) 0);
      headerCellControl1.SetValue(Grid.ColumnProperty, (object) 0);
      for (int index = 0; index < VisualCalendarYearView.MAX_COLUMNS; ++index)
      {
        this.container.ColumnDefinitions.Add(new ColumnDefinition()
        {
          Width = new GridLength(50.0, GridUnitType.Star),
          MinWidth = VisualCalendarYearView.COLUMN_MIN_WIDTH
        });
        YearViewColumnHeaderCellControl headerCellControl2 = new YearViewColumnHeaderCellControl(string.Concat((object) (index + 1)));
        this.container.Children.Add((UIElement) headerCellControl2);
        headerCellControl2.SetValue(Grid.RowProperty, (object) 0);
        headerCellControl2.SetValue(Grid.ColumnProperty, (object) (index + 1));
      }
      this.rowHeaders = new List<YearViewRowHeaderCellControl>();
      for (int index = 0; index < this.visibleMonthCount; ++index)
      {
        this.container.RowDefinitions.Add(new RowDefinition()
        {
          Height = new GridLength(VisualCalendarYearView.ROW_MIN_HEIGHT, GridUnitType.Pixel)
        });
        YearViewRowHeaderCellControl headerCellControl2 = new YearViewRowHeaderCellControl("");
        this.container.Children.Add((UIElement) headerCellControl2);
        headerCellControl2.SetValue(Grid.RowProperty, (object) (index + 1));
        headerCellControl2.SetValue(Grid.ColumnProperty, (object) 0);
        this.rowHeaders.Add(headerCellControl2);
      }
      this.cells = new YearViewCellControl[this.visibleMonthCount, VisualCalendarYearView.MAX_COLUMNS];
      for (int index1 = 0; index1 < this.visibleMonthCount; ++index1)
      {
        for (int index2 = 0; index2 < VisualCalendarYearView.MAX_COLUMNS; ++index2)
        {
          YearViewCellControl yearViewCellControl = new YearViewCellControl();
          this.container.Children.Add((UIElement) yearViewCellControl);
          yearViewCellControl.SetValue(Grid.RowProperty, (object) (index1 + 1));
          yearViewCellControl.SetValue(Grid.ColumnProperty, (object) (index2 + 1));
          this.cells[index1, index2] = yearViewCellControl;
        }
      }
    }
  }
}
