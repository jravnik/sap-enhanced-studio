﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarYearView.YearViewCellControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarYearView
{
  public class YearViewCellControl : UserControl
  {
    private static readonly Brush normalBorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 100, (byte) 100, (byte) 100));
    private bool isToday;
    private bool isSelected;
    private bool isWorkingDay;
    private bool hasMoreIndicator;
    private bool hasEntry;
    private bool isInvalidDate;
    private Border cellBorder;
    private Grid partContainer;
    private Rectangle baseRectangle;
    private bool moreInitialized;
    private MouseButtonEventHandler cellClickHandler;

    public bool IsToday
    {
      get
      {
        return this.isToday;
      }
      set
      {
        this.isToday = value;
        this.SetCellFormat();
      }
    }

    public bool IsSelected
    {
      get
      {
        return this.isSelected;
      }
      set
      {
        this.isSelected = value;
        this.SetCellFormat();
      }
    }

    public bool IsWorkingDay
    {
      get
      {
        return this.isWorkingDay;
      }
      set
      {
        this.isWorkingDay = value;
        this.SetCellFormat();
      }
    }

    public bool HasMoreIndicator
    {
      get
      {
        return this.hasMoreIndicator;
      }
      set
      {
        this.hasMoreIndicator = value;
      }
    }

    public bool HasEntry
    {
      get
      {
        return this.hasEntry;
      }
      set
      {
        this.hasEntry = value;
      }
    }

    public bool IsInvalidDate
    {
      get
      {
        return this.isInvalidDate;
      }
      set
      {
        this.isInvalidDate = value;
        this.SetCellFormat();
      }
    }

    internal YearViewCellControl()
    {
      this.baseRectangle = new Rectangle();
      this.baseRectangle.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.baseRectangle.VerticalAlignment = VerticalAlignment.Stretch;
      this.baseRectangle.StrokeThickness = 0.0;
      this.baseRectangle.Margin = new Thickness(0.0);
      this.baseRectangle.Opacity = 100.0;
      this.cellBorder = new Border();
      this.cellBorder.BorderThickness = new Thickness(0.1);
      this.cellBorder.BorderBrush = YearViewCellControl.normalBorderBrush;
      this.partContainer = new Grid();
      this.partContainer.Children.Add((UIElement) this.baseRectangle);
      this.partContainer.Children.Add((UIElement) this.cellBorder);
      this.Content = (object) this.partContainer;
      this.moreInitialized = false;
      this.hasEntry = false;
      this.hasMoreIndicator = false;
      this.isInvalidDate = false;
      this.isSelected = false;
      this.isToday = false;
      this.isWorkingDay = false;
    }

    private void InitializeMoreIcon()
    {
    }

    internal void SetCellFormat()
    {
      this.cellBorder.BorderBrush = YearViewCellControl.normalBorderBrush;
    }

    internal void ResetCellFormat()
    {
      if (this.cellClickHandler != null)
      {
        this.MouseLeftButtonDown -= this.cellClickHandler;
        this.cellClickHandler = (MouseButtonEventHandler) null;
      }
      this.cellBorder.BorderBrush = YearViewCellControl.normalBorderBrush;
      this.cellBorder.BorderThickness = new Thickness(0.1);
    }

    public delegate void MoreActionEventHandler(object sender, EventArgs e);

    public delegate void CellSelectEventHandler(object sender, EventArgs e);
  }
}
