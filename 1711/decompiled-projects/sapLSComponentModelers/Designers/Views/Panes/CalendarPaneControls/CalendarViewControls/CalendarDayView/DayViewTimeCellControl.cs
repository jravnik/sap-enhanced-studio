﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarDayView.DayViewTimeCellControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarDayView
{
  internal class DayViewTimeCellControl : UserControl
  {
    private static readonly Brush normalBorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 100, (byte) 100, (byte) 100));
    private static readonly Brush cellSelected = (Brush) CalendarUtils.CreateBrush(Color.FromArgb((byte) 175, (byte) 245, (byte) 173, (byte) 29), Color.FromArgb(byte.MaxValue, (byte) 245, (byte) 173, (byte) 29), Color.FromArgb((byte) 175, (byte) 245, (byte) 173, (byte) 29));
    private Border cellBorder;
    private Rectangle baseRectangle;

    internal DayViewTimeCellControl()
    {
      this.baseRectangle = new Rectangle();
      this.baseRectangle.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.baseRectangle.VerticalAlignment = VerticalAlignment.Stretch;
      this.baseRectangle.StrokeThickness = 0.0;
      this.baseRectangle.Margin = new Thickness(0.1);
      this.baseRectangle.Opacity = 100.0;
      this.cellBorder = new Border();
      this.cellBorder.BorderThickness = new Thickness(0.4);
      this.cellBorder.BorderBrush = DayViewTimeCellControl.normalBorderBrush;
      this.cellBorder.Child = (UIElement) this.baseRectangle;
      this.Content = (object) this.cellBorder;
    }
  }
}
