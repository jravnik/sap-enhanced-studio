﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.VisualCalendarWeekView
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarDayView;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls.CalendarViewElements;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls
{
  internal class VisualCalendarWeekView : VisualAbstractCalendarView
  {
    private Grid container;
    private Grid scrollContainer;
    private ScrollViewer scrollViewer;
    private DayViewTimeCellControl[,] cells;
    private AllDayArea allDayArea;
    private DayGrid[] dayColumns;
    private double rowsPerHour;
    private int daysPerWeek;
    private List<DayViewColumnHeaderCellControl> colHeaders;

    public VisualCalendarWeekView(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.container = new Grid();
      this.Content = (object) this.container;
      this.LoadView();
    }

    protected CalendarWeekView CalendarWeekView
    {
      get
      {
        return this.m_ModelObject as CalendarWeekView;
      }
    }

    public override void LoadView()
    {
      base.LoadView();
      this.CreateCalendarLayout();
    }

    public override string SelectionText
    {
      get
      {
        return "CalendarWeekView";
      }
    }

    protected void CreateCalendarLayout()
    {
      this.daysPerWeek = 7;
      this.rowsPerHour = this.CalendarWeekView.TimeScale != CalendarTimeScaleType.quarterHour ? (this.CalendarWeekView.TimeScale != CalendarTimeScaleType.halfHour ? (this.CalendarWeekView.TimeScale != CalendarTimeScaleType.oneHour ? 0.5 : 1.0) : 2.0) : 4.0;
      this.CreateTopPart();
      this.CreateScrollablePart();
    }

    private void CreateScrollablePart()
    {
      this.container.RowDefinitions.Add(new RowDefinition());
      this.scrollViewer = new ScrollViewer();
      this.container.Children.Add((UIElement) this.scrollViewer);
      this.scrollViewer.SetValue(Grid.RowProperty, (object) 2);
      this.scrollViewer.SetValue(Grid.ColumnProperty, (object) 0);
      this.scrollViewer.SetValue(Grid.ColumnSpanProperty, (object) (this.daysPerWeek + 3));
      this.scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
      this.scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
      this.scrollViewer.Height = CalendarUtils.DEFAULT_CONTENT_HEIGHT;
      this.scrollViewer.Background = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
      this.scrollViewer.BorderThickness = new Thickness(0.0);
      this.scrollViewer.Margin = new Thickness(0.0);
      this.scrollViewer.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.scrollViewer.VerticalAlignment = VerticalAlignment.Stretch;
      this.scrollViewer.HorizontalContentAlignment = HorizontalAlignment.Stretch;
      this.scrollViewer.VerticalContentAlignment = VerticalAlignment.Stretch;
      this.scrollViewer.Padding = new Thickness(0.0);
      this.scrollContainer = new Grid();
      this.scrollViewer.Content = (object) this.scrollContainer;
      for (int index = 0; index < 2; ++index)
        this.scrollContainer.ColumnDefinitions.Add(new ColumnDefinition()
        {
          Width = new GridLength(40.0, GridUnitType.Pixel)
        });
      for (int index = 0; (double) index < this.rowsPerHour * (double) CalendarUtils.HOURS_PER_DAY; ++index)
      {
        this.scrollContainer.RowDefinitions.Add(new RowDefinition()
        {
          Height = new GridLength((double) CalendarUtils.CELL_HEIGHT, GridUnitType.Pixel)
        });
        if ((double) index % this.rowsPerHour == 0.0)
        {
          int num = (int) ((double) index / this.rowsPerHour);
          DayViewRowHeaderCellControl headerCellControl = new DayViewRowHeaderCellControl(num < 10 ? "0" + num.ToString() : num.ToString(), true);
          headerCellControl.SetValue(Grid.RowProperty, (object) index);
          headerCellControl.SetValue(Grid.ColumnProperty, (object) 0);
          headerCellControl.SetValue(Grid.RowSpanProperty, (object) (this.rowsPerHour <= 1.0 ? 1 : (int) this.rowsPerHour));
          this.scrollContainer.Children.Add((UIElement) headerCellControl);
        }
        int num1 = this.rowsPerHour <= 1.0 ? 0 : (int) (60.0 / this.rowsPerHour * ((double) index % this.rowsPerHour));
        DayViewRowHeaderCellControl headerCellControl1 = new DayViewRowHeaderCellControl(num1 < 10 ? "0" + num1.ToString() : num1.ToString(), false);
        headerCellControl1.SetValue(Grid.RowProperty, (object) index);
        headerCellControl1.SetValue(Grid.ColumnProperty, (object) 1);
        this.scrollContainer.Children.Add((UIElement) headerCellControl1);
      }
      this.cells = new DayViewTimeCellControl[(int) (this.rowsPerHour * (double) CalendarUtils.HOURS_PER_DAY), this.daysPerWeek];
      for (int index1 = 0; index1 < this.daysPerWeek; ++index1)
      {
        this.scrollContainer.ColumnDefinitions.Add(new ColumnDefinition()
        {
          Width = new GridLength(100.0, GridUnitType.Star)
        });
        for (int index2 = 0; (double) index2 < this.rowsPerHour * (double) CalendarUtils.HOURS_PER_DAY; ++index2)
        {
          DayViewTimeCellControl viewTimeCellControl = new DayViewTimeCellControl();
          this.scrollContainer.Children.Add((UIElement) viewTimeCellControl);
          viewTimeCellControl.SetValue(Grid.ColumnProperty, (object) (index1 + 2));
          viewTimeCellControl.SetValue(Grid.RowProperty, (object) index2);
          this.cells[index2, index1] = viewTimeCellControl;
        }
      }
      this.dayColumns = new DayGrid[this.daysPerWeek];
      for (int index = 0; index < this.daysPerWeek; ++index)
      {
        this.dayColumns[index] = new DayGrid((int) (this.rowsPerHour * (double) CalendarUtils.HOURS_PER_DAY));
        this.scrollContainer.Children.Add((UIElement) this.dayColumns[index]);
        this.dayColumns[index].SetValue(Grid.ColumnProperty, (object) (index + 2));
        this.dayColumns[index].SetValue(Grid.RowProperty, (object) 0);
        this.dayColumns[index].SetValue(Grid.RowSpanProperty, (object) (int) (this.rowsPerHour * (double) CalendarUtils.HOURS_PER_DAY));
      }
    }

    private void CreateTopPart()
    {
      for (int index = 0; index < 2; ++index)
        this.container.ColumnDefinitions.Add(new ColumnDefinition()
        {
          Width = new GridLength(40.0, GridUnitType.Pixel)
        });
      this.container.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength((double) CalendarUtils.CELL_HEIGHT, GridUnitType.Pixel)
      });
      this.colHeaders = new List<DayViewColumnHeaderCellControl>();
      for (int index = 0; index < this.daysPerWeek; ++index)
      {
        this.container.ColumnDefinitions.Add(new ColumnDefinition()
        {
          Width = new GridLength(100.0, GridUnitType.Star)
        });
        DayViewColumnHeaderCellControl headerCellControl = new DayViewColumnHeaderCellControl();
        this.container.Children.Add((UIElement) headerCellControl);
        headerCellControl.SetValue(Grid.RowProperty, (object) 0);
        headerCellControl.SetValue(Grid.ColumnProperty, (object) (index + 2));
        this.colHeaders.Add(headerCellControl);
      }
      this.container.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(0.0, GridUnitType.Auto)
      });
      DayViewColumnHeaderCellControl headerCellControl1 = new DayViewColumnHeaderCellControl();
      this.container.Children.Add((UIElement) headerCellControl1);
      headerCellControl1.SetValue(Grid.RowProperty, (object) 0);
      headerCellControl1.SetValue(Grid.ColumnProperty, (object) (this.daysPerWeek + 2));
      headerCellControl1.SetValue(Grid.RowSpanProperty, (object) 2);
      headerCellControl1.SetValue(FrameworkElement.WidthProperty, (object) 17.0);
      this.container.RowDefinitions.Add(new RowDefinition()
      {
        MinHeight = (double) CalendarUtils.CELL_HEIGHT
      });
      this.allDayArea = new AllDayArea(this.daysPerWeek);
      this.container.Children.Add((UIElement) this.allDayArea);
      this.allDayArea.SetValue(Grid.RowProperty, (object) 1);
      this.allDayArea.SetValue(Grid.ColumnProperty, (object) 2);
      this.allDayArea.SetValue(Grid.ColumnSpanProperty, (object) this.daysPerWeek);
      DayViewColumnHeaderCellControl headerCellControl2 = new DayViewColumnHeaderCellControl();
      this.container.Children.Add((UIElement) headerCellControl2);
      headerCellControl2.SetValue(Grid.RowProperty, (object) 0);
      headerCellControl2.SetValue(Grid.ColumnProperty, (object) 0);
      headerCellControl2.SetValue(Grid.ColumnSpanProperty, (object) 2);
      AllDayAreaCell allDayAreaCell = new AllDayAreaCell();
      this.container.Children.Add((UIElement) allDayAreaCell);
      allDayAreaCell.SetValue(Grid.RowProperty, (object) 1);
      allDayAreaCell.SetValue(Grid.ColumnProperty, (object) 0);
      allDayAreaCell.SetValue(Grid.ColumnSpanProperty, (object) 2);
    }
  }
}
