﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarMonthView.VisualMonthViewColumnHeaderControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarMonthView
{
  public class VisualMonthViewColumnHeaderControl : UserControl
  {
    private Border cellBorder;
    public TextBlock textBlock;

    internal VisualMonthViewColumnHeaderControl(string text)
    {
      this.textBlock = new TextBlock();
      this.textBlock.Text = text;
      this.textBlock.HorizontalAlignment = HorizontalAlignment.Center;
      this.textBlock.VerticalAlignment = VerticalAlignment.Center;
      this.textBlock.Margin = new Thickness(1.0);
      this.cellBorder = new Border();
      this.cellBorder.Background = (Brush) CalendarUtils.CreateBrush(Color.FromArgb(byte.MaxValue, (byte) 225, (byte) 228, (byte) 232), Color.FromArgb(byte.MaxValue, (byte) 208, (byte) 214, (byte) 220), Color.FromArgb(byte.MaxValue, (byte) 209, (byte) 214, (byte) 220));
      this.cellBorder.BorderThickness = new Thickness(0.4, 1.0, 0.4, 1.0);
      this.cellBorder.Child = (UIElement) this.textBlock;
      this.cellBorder.BorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 200, (byte) 200, (byte) 200));
      this.Content = (object) this.cellBorder;
    }

    public string ColHeader
    {
      set
      {
        this.textBlock.Text = value;
      }
    }
  }
}
