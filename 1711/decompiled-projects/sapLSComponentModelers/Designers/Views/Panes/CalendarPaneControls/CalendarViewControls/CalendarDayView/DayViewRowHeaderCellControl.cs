﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarDayView.DayViewRowHeaderCellControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarDayView
{
  public class DayViewRowHeaderCellControl : UserControl
  {
    private bool isHourMode;
    private TextBlock text;
    private Border cellBorder;

    internal DayViewRowHeaderCellControl(string headerText, bool isHourCell)
    {
      this.isHourMode = isHourCell;
      this.text = new TextBlock();
      this.text.HorizontalAlignment = this.isHourMode ? HorizontalAlignment.Center : HorizontalAlignment.Left;
      this.text.VerticalAlignment = VerticalAlignment.Top;
      this.text.Text = headerText;
      this.text.FontSize = this.isHourMode ? CalendarUtils.HOUR_CELL_FONT_SIZE : CalendarUtils.MINUTE_CELL_FONT_SIZE;
      this.cellBorder = new Border();
      this.cellBorder.Background = (Brush) CalendarUtils.CreateBrush(Color.FromArgb((byte) 55, byte.MaxValue, byte.MaxValue, byte.MaxValue), Color.FromArgb(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue), Color.FromArgb((byte) 55, byte.MaxValue, byte.MaxValue, byte.MaxValue));
      this.cellBorder.BorderThickness = this.isHourMode ? new Thickness(0.4, 0.1, 0.0, 0.1) : new Thickness(0.0, 0.1, 1.0, 0.1);
      this.cellBorder.Child = (UIElement) this.text;
      this.cellBorder.BorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 135, (byte) 163, (byte) 192));
      this.cellBorder.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.cellBorder.VerticalAlignment = VerticalAlignment.Stretch;
      this.Content = (object) this.cellBorder;
    }
  }
}
