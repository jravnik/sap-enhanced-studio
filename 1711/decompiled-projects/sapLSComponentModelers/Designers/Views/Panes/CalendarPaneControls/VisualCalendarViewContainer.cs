﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.VisualCalendarViewContainer
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls.CalendarViewElements;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls
{
  internal class VisualCalendarViewContainer : BaseSelectableControl
  {
    private static readonly Brush viewBorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 135, (byte) 163, (byte) 192));
    private Border viewBorder;
    private VisualCalendarDayView m_CalendarDayView;
    private VisualCalendarMonthView m_CalendarMonthView;
    private VisualCalendarYearView m_CalendarYearView;
    private VisualCalendarWeekView m_CalendarWeekView;
    private VisualCalendarEntry m_CalendarEntry;
    private VisualDayPattern m_DayPattern;
    private VisualRecurrenceDayPattern m_RecurrenceDayPattern;
    private Grid m_Container;
    private VisualAbstractCalendarView m_CurrentView;
    private CalendarViewSelectionType m_CurrentViewType;

    public VisualCalendarViewContainer(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_Container = new Grid();
      this.LoadView();
    }

    public CalendarViewSelectionType CurrentViewType
    {
      get
      {
        return this.m_CurrentViewType;
      }
      set
      {
        this.m_CurrentViewType = value;
        this.SwitchCalendarView(value);
      }
    }

    public override string SelectionText
    {
      get
      {
        return "CalendarView";
      }
    }

    protected CalendarView CalendarView
    {
      get
      {
        return this.m_ModelObject as CalendarView;
      }
    }

    public VisualCalendarYearView YearView
    {
      get
      {
        return this.m_CalendarYearView;
      }
      set
      {
        this.m_CalendarYearView = value;
      }
    }

    public VisualCalendarMonthView MonthView
    {
      get
      {
        return this.m_CalendarMonthView;
      }
      set
      {
        this.m_CalendarMonthView = value;
      }
    }

    public VisualCalendarDayView DayView
    {
      get
      {
        return this.m_CalendarDayView;
      }
      set
      {
        this.m_CalendarDayView = value;
      }
    }

    public VisualCalendarWeekView WeekView
    {
      get
      {
        return this.m_CalendarWeekView;
      }
      set
      {
        this.m_CalendarWeekView = value;
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      if (e.AddedModel is CalendarEntry)
      {
        if (this.m_CalendarEntry == null)
          this.m_CalendarEntry = new VisualCalendarEntry((IModelObject) this.CalendarView.CalendarEntry);
        GridUtil.PlaceElement(this.m_Container, (UIElement) this.m_CalendarEntry, 2, 0);
      }
      else if (e.AddedModel is DayPattern)
      {
        if (this.m_DayPattern == null)
          this.m_DayPattern = new VisualDayPattern((IModelObject) this.CalendarView.DayPattern);
        GridUtil.PlaceElement(this.m_Container, (UIElement) this.m_DayPattern, 3, 0);
      }
      else if (e.AddedModel is RecurrenceDayPattern)
      {
        if (this.m_RecurrenceDayPattern == null)
          this.m_RecurrenceDayPattern = new VisualRecurrenceDayPattern((IModelObject) this.CalendarView.RecurrenceDayPattern);
        GridUtil.PlaceElement(this.m_Container, (UIElement) this.m_RecurrenceDayPattern, 4, 0);
      }
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      if (e.RemovedModel is CalendarEntry)
      {
        if (this.m_CalendarEntry == null)
          return;
        GridUtil.RemoveElement(this.m_Container, (UIElement) this.m_CalendarEntry, false, 2, 0);
        this.m_CalendarEntry = (VisualCalendarEntry) null;
      }
      else if (e.RemovedModel is DayPattern)
      {
        if (this.m_DayPattern == null)
          return;
        GridUtil.RemoveElement(this.m_Container, (UIElement) this.m_DayPattern, false, 3, 0);
        this.m_DayPattern = (VisualDayPattern) null;
      }
      else if (e.RemovedModel is RecurrenceDayPattern)
      {
        if (this.m_RecurrenceDayPattern != null)
          return;
        GridUtil.RemoveElement(this.m_Container, (UIElement) this.m_RecurrenceDayPattern, false, 4, 0);
        this.m_RecurrenceDayPattern = (VisualRecurrenceDayPattern) null;
      }
      else
      {
        if (!(e.RemovedModel is CalendarDayView))
          return;
        this.m_CalendarDayView = (VisualCalendarDayView) null;
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == "HasEntry")
      {
        if (this.CalendarView.HasEntry)
        {
          if (this.m_CalendarEntry == null)
            this.m_CalendarEntry = new VisualCalendarEntry((IModelObject) this.CalendarView.CalendarEntry);
          GridUtil.PlaceElement(this.m_Container, (UIElement) this.m_CalendarEntry, 2, 0);
        }
        else
        {
          GridUtil.RemoveElement(this.m_Container, (UIElement) this.m_CalendarEntry, false, 2, 0);
          this.m_CalendarEntry = (VisualCalendarEntry) null;
        }
      }
      else if (e.PropertyName == "HasDayPattern")
      {
        if (this.CalendarView.HasDayPattern)
        {
          if (this.m_DayPattern == null)
            this.m_DayPattern = new VisualDayPattern((IModelObject) this.CalendarView.DayPattern);
          GridUtil.PlaceElement(this.m_Container, (UIElement) this.m_DayPattern, 3, 0);
        }
        else
        {
          GridUtil.RemoveElement(this.m_Container, (UIElement) this.m_DayPattern, false, 3, 0);
          this.m_DayPattern = (VisualDayPattern) null;
        }
      }
      else
      {
        if (!(e.PropertyName == "HasRecurrenceDayPattern"))
          return;
        if (this.CalendarView.HasRecurrenceDayPattern)
        {
          if (this.m_RecurrenceDayPattern == null)
            this.m_RecurrenceDayPattern = new VisualRecurrenceDayPattern((IModelObject) this.CalendarView.RecurrenceDayPattern);
          GridUtil.PlaceElement(this.m_Container, (UIElement) this.m_RecurrenceDayPattern, 4, 0);
        }
        else
        {
          GridUtil.RemoveElement(this.m_Container, (UIElement) this.m_RecurrenceDayPattern, false, 4, 0);
          this.m_RecurrenceDayPattern = (VisualRecurrenceDayPattern) null;
        }
      }
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.viewBorder = new Border();
        this.viewBorder.BorderBrush = VisualCalendarViewContainer.viewBorderBrush;
        this.viewBorder.BorderThickness = new Thickness(1.0);
        this.PrepareCalendarView(this.CalendarView.DefaultView);
        this.viewBorder.Child = (UIElement) this.m_CurrentView;
        this.m_Container.RowDefinitions.Add(new RowDefinition());
        this.m_Container.ColumnDefinitions.Add(new ColumnDefinition());
        this.m_Container.RowDefinitions.Add(new RowDefinition());
        GridUtil.PlaceElement(this.m_Container, (UIElement) this.viewBorder, 1, 0);
        this.m_Container.RowDefinitions.Add(new RowDefinition());
        this.m_Container.RowDefinitions.Add(new RowDefinition());
        this.m_Container.RowDefinitions.Add(new RowDefinition());
        this.InitializeViewContainer();
        this.Content = (object) this.m_Container;
      }
      catch
      {
      }
    }

    private void SwitchCalendarView(CalendarViewSelectionType viewtype)
    {
      this.PrepareCalendarView(viewtype);
      this.viewBorder.Child = (UIElement) this.m_CurrentView;
    }

    private void InitializeViewContainer()
    {
      if (this.CalendarView.HasEntry)
      {
        if (this.m_CalendarEntry == null)
          this.m_CalendarEntry = new VisualCalendarEntry((IModelObject) this.CalendarView.CalendarEntry);
        GridUtil.PlaceElement(this.m_Container, (UIElement) this.m_CalendarEntry, 2, 0);
      }
      if (this.CalendarView.HasDayPattern)
      {
        if (this.m_DayPattern == null)
          this.m_DayPattern = new VisualDayPattern((IModelObject) this.CalendarView.DayPattern);
        GridUtil.PlaceElement(this.m_Container, (UIElement) this.m_DayPattern, 3, 0);
      }
      if (!this.CalendarView.HasRecurrenceDayPattern)
        return;
      if (this.m_RecurrenceDayPattern == null)
        this.m_RecurrenceDayPattern = new VisualRecurrenceDayPattern((IModelObject) this.CalendarView.RecurrenceDayPattern);
      GridUtil.PlaceElement(this.m_Container, (UIElement) this.m_RecurrenceDayPattern, 4, 0);
    }

    private void PrepareCalendarView(CalendarViewSelectionType calendarViewSelectionType)
    {
      switch (calendarViewSelectionType)
      {
        case CalendarViewSelectionType.CalendarYearView:
          if (this.m_CalendarYearView == null)
            this.m_CalendarYearView = new VisualCalendarYearView((IModelObject) this.CalendarView.CalendarYearView);
          this.m_CurrentView = (VisualAbstractCalendarView) this.m_CalendarYearView;
          break;
        case CalendarViewSelectionType.CalendarMonthView:
          if (this.m_CalendarMonthView == null)
            this.m_CalendarMonthView = new VisualCalendarMonthView((IModelObject) this.CalendarView.CalendarMonthView);
          this.m_CurrentView = (VisualAbstractCalendarView) this.m_CalendarMonthView;
          break;
        case CalendarViewSelectionType.CalendarWeekView:
          if (this.m_CalendarWeekView == null)
            this.m_CalendarWeekView = new VisualCalendarWeekView((IModelObject) this.CalendarView.CalendarWeekView);
          this.m_CurrentView = (VisualAbstractCalendarView) this.m_CalendarWeekView;
          break;
        case CalendarViewSelectionType.CalendarDayView:
          if (this.m_CalendarDayView == null)
            this.m_CalendarDayView = new VisualCalendarDayView((IModelObject) this.CalendarView.CalendarDayView);
          this.m_CurrentView = (VisualAbstractCalendarView) this.m_CalendarDayView;
          break;
      }
      this.m_CurrentViewType = calendarViewSelectionType;
    }
  }
}
