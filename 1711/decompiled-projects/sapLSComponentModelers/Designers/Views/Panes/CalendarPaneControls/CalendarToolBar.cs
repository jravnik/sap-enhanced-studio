﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarToolBar
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls
{
  internal class CalendarToolBar : BaseSelectableControl, IContextMenuContainer
  {
    private StackPanel container;
    private Grid containerGrid;
    private CalendarPaginator paginator;
    private VisualToolbar m_VisualToolBar;
    private VisualCombobox viewSwitch;
    private ComboBox cbViewSwitch;
    private CalendarPaneToolbar customToolBar;

    public event SelectionChangedEventHandler ViewItemSelectionChanged;

    internal ItemCollection ViewSwitchItems
    {
      get
      {
        return this.cbViewSwitch.Items;
      }
    }

    internal object ViewSwitchSelectedItem
    {
      get
      {
        return this.cbViewSwitch.SelectedItem;
      }
      set
      {
        this.cbViewSwitch.SelectedItem = value;
      }
    }

    internal CalendarPaneToolbar CustomToolBar
    {
      get
      {
        return this.customToolBar;
      }
      set
      {
        this.customToolBar = value;
        if (this.customToolBar == null)
          return;
        this.AddCustomToolBar(this.customToolBar);
      }
    }

    public CalendarToolBar(IModelObject toolbar)
      : base(toolbar)
    {
      this.container = new StackPanel();
      this.containerGrid = new Grid();
      GridUtil.AddRowDef(this.containerGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.containerGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.containerGrid, 60.0, GridUnitType.Pixel);
      GridUtil.AddColumnDef(this.containerGrid, 1.0, GridUnitType.Star);
      this.container.Orientation = Orientation.Horizontal;
      this.container.VerticalAlignment = VerticalAlignment.Center;
      this.paginator = new CalendarPaginator();
      GridUtil.PlaceElement(this.containerGrid, (UIElement) this.paginator, 0, 0);
      this.viewSwitch = new VisualCombobox();
      this.cbViewSwitch = new ComboBox();
      this.cbViewSwitch.SelectionChanged += new SelectionChangedEventHandler(this.cbViewSwitch_SelectionChanged);
      GridUtil.PlaceElement(this.containerGrid, (UIElement) this.cbViewSwitch, 0, 1);
      if (toolbar != null)
        this.AddCustomToolBar(toolbar as CalendarPaneToolbar);
      this.Content = (object) this.containerGrid;
    }

    private void cbViewSwitch_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.ViewItemSelectionChanged == null)
        return;
      this.ViewItemSelectionChanged(sender, e);
    }

    public override string SelectionText
    {
      get
      {
        return "ToolBar";
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (this.m_VisualToolBar == null)
        return;
      this.m_VisualToolBar.HandleContextMenuItemClickWrapper(source);
    }

    protected override void AddContextMenuItems()
    {
      if (this.m_VisualToolBar == null)
        return;
      this.m_VisualToolBar.AddContextMenuItems((IContextMenuContainer) this);
    }

    public void AddContextMenuItemsWrapper()
    {
      this.AddContextMenuItems();
    }

    public void HandleContextMenuItemWrapper(MenuItem source)
    {
      this.HandleContextMenuItemClick(source);
    }

    private void AddCustomToolBar(CalendarPaneToolbar toolbar)
    {
      if (this.m_VisualToolBar == null)
      {
        this.m_VisualToolBar = new VisualToolbar((IModelObject) toolbar);
        GridUtil.PlaceElement(this.containerGrid, (UIElement) this.m_VisualToolBar, 0, 2);
      }
      this.m_VisualToolBar.Toolbar = (Toolbar) toolbar;
      this.m_VisualToolBar.ReloadView();
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
