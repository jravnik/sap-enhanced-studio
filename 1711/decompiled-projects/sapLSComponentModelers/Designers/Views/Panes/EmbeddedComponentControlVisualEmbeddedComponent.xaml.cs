﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.EmbeddedComponentControl.VisualEmbeddedComponent
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controller;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Controllers.Interface;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.EmbeddedComponentControl
{
  public partial class VisualEmbeddedComponent : BaseSelectableControl, IComponentConnector
  {
    private const string OWL_COMPONENT_TYPE = "OWL";
    private const string ICP_COMPONENT_TYPE = "ICP";
    //internal Rectangle rectangle1;
    //internal Button buttonBind;
    //internal Button buttonNavigate;
    //internal Label labelHeader;
    //internal Label labelName;
    //internal TextBox textBoxName;
    //internal Label labelPath;
    //internal TextBox textBoxPath;
    //private bool _contentLoaded;

    private EmbeddComponent EmbeddComponent
    {
      get
      {
        return this.Model as EmbeddComponent;
      }
    }

    public EmbeddedComponentPane ParentEmbeddedComponentPane { get; set; }

    public VisualEmbeddedComponent(IModelObject model, EmbeddedComponentPane parentEmbeddedComponentPane)
      : base(model)
    {
      this.InitializeComponent();
      this.IsDraggable = true;
      this.ParentEmbeddedComponentPane = parentEmbeddedComponentPane;
      if (this.EmbeddComponent == null)
        return;
      this.textBoxName.Text = this.EmbeddComponent.Name;
      string targetComponentId = this.EmbeddComponent.TargetComponentId;
      this.textBoxPath.Text = targetComponentId;
      if (LoginManager.Instance.OptimizedMode)
      {
        if (targetComponentId == null || !targetComponentId.EndsWith(".OWL.uicomponent"))
          return;
        this.labelHeader.Content = (object) "OWL Component";
      }
      else
      {
        if (this.EmbeddComponent.AssociatedEmbeddedComponent == null || !(this.EmbeddComponent.AssociatedEmbeddedComponent.ComponentType == "OWL"))
          return;
        this.labelHeader.Content = (object) "OWL Component";
      }
    }

    protected override void OnMouseLeave(MouseEventArgs e)
    {
      try
      {
        base.OnMouseLeave(e);
        this.Background = this.ControlBackground;
        if (e.LeftButton != MouseButtonState.Pressed || !this.IsDraggable)
          return;
        BaseDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
        try
        {
          if (parentDesigner != null && parentDesigner.SelectedObject == this.Model)
          {
            this.OnMouseMove(e);
            DataObject dataObject = new DataObject(typeof (IModelObject), (object) this.ParentEmbeddedComponentPane);
            if (BaseSelectableControl.dispatcherOperation != null)
              BaseSelectableControl.dispatcherOperation.Abort();
            BaseSelectableControl.dispatcherOperation = base.Dispatcher.BeginInvoke(DispatcherPriority.Background, new ParameterizedThreadStart(base.DoDragDrop), dataObject);
          }
          e.Handled = true;
        }
        catch
        {
          e.Handled = true;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Error while indicating Selection Possibility of the control", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "EmbeddedComponent";
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      if (e == null || !(e.PropertyName == ModelObject.NameProperty))
        return;
      this.textBoxName.Text = this.EmbeddComponent.Name;
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Refresh");
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == (object) "Delete")
      {
        this.ParentEmbeddedComponentPane.Parent.RemoveModelObject((IModelObject) this.ParentEmbeddedComponentPane);
      }
      else
      {
        if (source.Header != (object) "Refresh")
          return;
        this.EmbeddComponent.UpdateInterfaceInformation(true);
      }
    }

    private void buttonBind_Click(object sender, RoutedEventArgs e)
    {
      IFloorplan floorplanObject = Utilities.GetFloorplanObject((IModelObject) this.EmbeddComponent);
      IFloorplan embeddedComponent = this.EmbeddComponent.AssociatedEmbeddedComponent;
      IInterface controllerInterface = embeddedComponent.ControllerInterface;
      IImplementationController implementationController = floorplanObject.ImplementationController;
      if (controllerInterface == null)
        return;
      new InterfaceBindingConfigurationEditor(floorplanObject, embeddedComponent, this.EmbeddComponent).ShowDialog();
    }

    private void buttonNavigate_Click(object sender, RoutedEventArgs e)
    {
      ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
      if (string.IsNullOrEmpty(this.EmbeddComponent.TargetComponentId))
        return;
      parentDesigner.OpenRelatedFloorPlan(this.EmbeddComponent.TargetComponentId);
    }

    private void textBoxName_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      this.SwitchOffReadOnly(this.textBoxName);
    }

    private void textBoxName_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
    {
      this.UpdateEmbeddComponentName();
      this.SwitchOnReadonly(this.textBoxName);
    }

    private void textBoxName_LostFocus(object sender, RoutedEventArgs e)
    {
      this.UpdateEmbeddComponentName();
      this.SwitchOnReadonly(this.textBoxName);
    }

    private void SwitchOnReadonly(TextBox textControl)
    {
      textControl.IsReadOnly = true;
      textControl.Background = (Brush) Brushes.LightGray;
    }

    private void SwitchOffReadOnly(TextBox textControl)
    {
      textControl.IsReadOnly = false;
      textControl.Background = (Brush) Brushes.White;
    }

    private void UpdateEmbeddComponentName()
    {
      if (!(this.textBoxName.Text != this.EmbeddComponent.Name))
        return;
      this.EmbeddComponent.Name = this.textBoxName.Text;
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/designers/views/panes/embeddedcomponentcontrol/visualembeddedcomponent.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.rectangle1 = (Rectangle) target;
    //      break;
    //    case 2:
    //      this.buttonBind = (Button) target;
    //      this.buttonBind.Click += new RoutedEventHandler(this.buttonBind_Click);
    //      break;
    //    case 3:
    //      this.buttonNavigate = (Button) target;
    //      this.buttonNavigate.Click += new RoutedEventHandler(this.buttonNavigate_Click);
    //      break;
    //    case 4:
    //      this.labelHeader = (Label) target;
    //      break;
    //    case 5:
    //      this.labelName = (Label) target;
    //      break;
    //    case 6:
    //      this.textBoxName = (TextBox) target;
    //      this.textBoxName.MouseDoubleClick += new MouseButtonEventHandler(this.textBoxName_MouseDoubleClick);
    //      this.textBoxName.LostKeyboardFocus += new KeyboardFocusChangedEventHandler(this.textBoxName_LostKeyboardFocus);
    //      this.textBoxName.LostFocus += new RoutedEventHandler(this.textBoxName_LostFocus);
    //      break;
    //    case 7:
    //      this.labelPath = (Label) target;
    //      break;
    //    case 8:
    //      this.textBoxPath = (TextBox) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
