﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualBasePane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public abstract class VisualBasePane : BaseBorderControl
  {
    protected static readonly Brush bodyBorderBrush = (Brush) SkinConstants.BRUSH_CONTAINERBODY_BORDER;
    protected static readonly Brush plainBodyBrush = (Brush) SkinConstants.BRUSH_CONTAINERBODY_PLAINBACKGROUND;
    protected static readonly Brush filledBodyBrush = (Brush) SkinConstants.BRUSH_CONTAINERBODY_FILLBACKGROUND;
    protected static readonly Thickness fillPlainContentMargin = SkinConstants.MARGIN_CONTAINER_BODY;
    protected static readonly double headerHeight = SkinConstants.HEIGHT_CONTAINER_HEADER;
    private UserControl paneHeaderControl;
    private TextBlock primaryHelpExplanationControl;
    private Grid bodyElementGrid;
    private Grid headerGrid;
    private FrameworkElement elementToAdd;

    private BasePane BasePane
    {
      get
      {
        return this.m_ModelObject as BasePane;
      }
    }

    public Grid BodyElementGrid
    {
      get
      {
        return this.bodyElementGrid;
      }
    }

    public Grid HeaderGrid
    {
      get
      {
        this.headerGrid.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
        return this.headerGrid;
      }
    }

    public VisualBasePane(IModelObject model)
      : base(model)
    {
      this.IsDraggable = true;
    }

    public override void LoadView()
    {
      base.LoadView();
      try
      {
        this.bodyElementGrid = new Grid();
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        int row = 0;
        AdvancedListPane basePane1 = this.BasePane as AdvancedListPane;
        FormPane basePane2 = this.BasePane as FormPane;
        if (basePane1 != null && basePane1.UseIntegratedToolbar || basePane2 != null && basePane2.PlaceToolbarOnHeader)
        {
          GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
          this.headerGrid = new Grid();
          GridUtil.AddRowDef(this.headerGrid, 1.0, GridUnitType.Auto);
          if (this.BasePane.PaneHeader != null && !string.IsNullOrEmpty(this.BasePane.PaneHeader.Text))
          {
            GridUtil.AddColumnDef(this.headerGrid, 1.0, GridUnitType.Auto);
            GridUtil.AddColumnDef(this.headerGrid, 1.0, GridUnitType.Star);
            this.paneHeaderControl = this.ConstructPaneHeader();
            GridUtil.PlaceElement(this.headerGrid, (UIElement) this.paneHeaderControl, 0, 0, 1, 1);
            GridUtil.PlaceElement(this.Grid, (UIElement) this.headerGrid, row, 0, 1, 1);
            ++row;
          }
          else
          {
            GridUtil.AddColumnDef(this.headerGrid, 1.0, GridUnitType.Star);
            GridUtil.PlaceElement(this.Grid, (UIElement) this.headerGrid, 0, 0, 1, 1);
            ++row;
          }
        }
        else if (this.BasePane.PaneHeader != null && !string.IsNullOrEmpty(this.BasePane.PaneHeader.Text))
        {
          GridUtil.AddRowDef(this.Grid, VisualBasePane.headerHeight, GridUnitType.Pixel);
          this.paneHeaderControl = this.ConstructPaneHeader();
          GridUtil.PlaceElement(this.Grid, (UIElement) this.paneHeaderControl, row, 0, 1, 1);
          ++row;
        }
        if (this.BasePane.PrimaryHelpExplanation != null && !string.IsNullOrEmpty(this.BasePane.PrimaryHelpExplanation.Text))
        {
          this.primaryHelpExplanationControl = this.ConstructPrimaryHelpTextHolder();
          GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) this.primaryHelpExplanationControl, row, 0, 1, this.BasePane.Columns);
          this.primaryHelpExplanationControl.Text = this.BasePane.PrimaryHelpExplanation.Text.Replace("<br/>", "\r\n");
          ++row;
        }
        this.ConstructBodyElement();
        if (this.BasePane.FixedBodyHeightSpecified && this.BasePane.FixedBodyHeight > 0)
          this.bodyElementGrid.Height = (double) this.BasePane.FixedBodyHeight;
        this.elementToAdd = this.ConstructBodyElementToAdd();
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.elementToAdd, row, 0, 1, 1);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of BasePane view failed", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "PaneControl";
      }
    }

    public abstract void ConstructBodyElement();

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == BasePane.BodyStyleProperty)
        {
          int index = this.Grid.Children.IndexOf((UIElement) this.elementToAdd);
          this.Grid.Children.Remove((UIElement) this.elementToAdd);
          this.elementToAdd = this.ConstructBodyElementToAdd();
          this.Grid.Children.Insert(index, (UIElement) this.elementToAdd);
        }
        else if (e.PropertyName == BasePane.FixedBodyHeightProperty || e.PropertyName == BasePane.FixedBodyHeightSpecifiedProperty)
        {
          if (!this.BasePane.FixedBodyHeightSpecified || this.BasePane.FixedBodyHeight <= 0)
            return;
          this.bodyElementGrid.Height = (double) this.BasePane.FixedBodyHeight;
        }
        else if (e.PropertyName == BasePane.PaneHeaderProperty)
          this.InvalidateView();
        else if (e.PropertyName == BasePane.PrimaryHelpExplanationProperty)
        {
          this.InvalidateView();
        }
        else
        {
          if (!(e.PropertyName == BasePane.PaneHeaderStyleProperty) || this.paneHeaderControl == null)
            return;
          AdvancedListPane basePane1 = this.BasePane as AdvancedListPane;
          FormPane basePane2 = this.BasePane as FormPane;
          int index = basePane1 != null && basePane1.UseIntegratedToolbar || basePane2 != null && basePane2.PlaceToolbarOnHeader ? this.HeaderGrid.Children.IndexOf((UIElement) this.paneHeaderControl) : this.Grid.Children.IndexOf((UIElement) this.paneHeaderControl);
          switch (this.BasePane.PaneHeaderStyle)
          {
            case PaneHeaderStyleType.Block:
              this.paneHeaderControl = (UserControl) new BlockHeader(this.BasePane.PaneHeader.Text);
              break;
            case PaneHeaderStyleType.Underline:
              this.paneHeaderControl = (UserControl) new UnderlineHeader(this.BasePane.PaneHeader.Text);
              break;
          }
          if (basePane1 != null && basePane1.UseIntegratedToolbar || basePane2 != null && basePane2.PlaceToolbarOnHeader)
          {
            this.HeaderGrid.Children.RemoveAt(index);
            this.HeaderGrid.Children.Insert(index, (UIElement) this.paneHeaderControl);
          }
          else
          {
            this.Grid.Children.RemoveAt(index);
            this.Grid.Children.Insert(index, (UIElement) this.paneHeaderControl);
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("PropertyChanged handler failed update the Basepane UI.", ex));
      }
    }

    private FrameworkElement ConstructBodyElementToAdd()
    {
      Border parent = this.bodyElementGrid.Parent as Border;
      if (parent != null)
        parent.Child = (UIElement) null;
      switch (this.BasePane.BodyStyle)
      {
        case PaneBodyStyleType.Fill:
          Border border1 = new Border();
          border1.Background = VisualBasePane.filledBodyBrush;
          border1.BorderBrush = VisualBasePane.bodyBorderBrush;
          border1.BorderThickness = new Thickness(1.0);
          border1.Child = (UIElement) this.bodyElementGrid;
          border1.Padding = new Thickness(VisualBasePane.fillPlainContentMargin.Left);
          return (FrameworkElement) border1;
        case PaneBodyStyleType.Plain:
          Border border2 = new Border();
          border2.Background = VisualBasePane.plainBodyBrush;
          border2.BorderBrush = VisualBasePane.bodyBorderBrush;
          border2.BorderThickness = new Thickness(1.0);
          border2.Child = (UIElement) this.bodyElementGrid;
          border2.Padding = new Thickness(VisualBasePane.fillPlainContentMargin.Left);
          return (FrameworkElement) border2;
        case PaneBodyStyleType.BorderTransparent:
          Border border3 = new Border();
          border3.BorderBrush = VisualBasePane.bodyBorderBrush;
          border3.BorderThickness = new Thickness(1.0);
          border3.Child = (UIElement) this.bodyElementGrid;
          return (FrameworkElement) border3;
        default:
          this.BorderThickness = new Thickness(0.0);
          return (FrameworkElement) this.bodyElementGrid;
      }
    }

    private UserControl ConstructPaneHeader()
    {
      UserControl userControl;
      switch (this.BasePane.PaneHeader.Style)
      {
        case PaneHeaderStyleType.Block:
          userControl = (UserControl) new BlockHeader(this.BasePane.PaneHeader.Text);
          break;
        case PaneHeaderStyleType.Underline:
          userControl = (UserControl) new UnderlineHeader(this.BasePane.PaneHeader.Text);
          break;
        default:
          userControl = (UserControl) new BlockHeader(this.BasePane.PaneHeader.Text);
          break;
      }
      return userControl;
    }

    private TextBlock ConstructPrimaryHelpTextHolder()
    {
      return new TextBlock()
      {
        TextWrapping = TextWrapping.Wrap
      };
    }

    private void InvalidateView()
    {
      this.Grid.Children.Clear();
      this.Grid.RowDefinitions.Clear();
      int row = 0;
      AdvancedListPane basePane1 = this.BasePane as AdvancedListPane;
      FormPane basePane2 = this.BasePane as FormPane;
      if (basePane1 != null && basePane1.UseIntegratedToolbar || basePane2 != null && basePane2.PlaceToolbarOnHeader)
      {
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        this.headerGrid = new Grid();
        GridUtil.AddRowDef(this.headerGrid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.headerGrid, 1.0, GridUnitType.Star);
        if (this.BasePane.PaneHeader != null && !string.IsNullOrEmpty(this.BasePane.PaneHeader.Text))
        {
          GridUtil.AddColumnDef(this.headerGrid, 1.0, GridUnitType.Star);
          this.paneHeaderControl = this.ConstructPaneHeader();
          GridUtil.PlaceElement(this.headerGrid, (UIElement) this.paneHeaderControl, 0, 0, 1, 1);
          GridUtil.PlaceElement(this.Grid, (UIElement) this.headerGrid, row, 0, 1, 1);
          ++row;
        }
        else
        {
          GridUtil.PlaceElement(this.Grid, (UIElement) this.headerGrid, 0, 0, 1, 1);
          ++row;
        }
      }
      else if (this.BasePane.PaneHeader != null && !string.IsNullOrEmpty(this.BasePane.PaneHeader.Text))
      {
        if (this.paneHeaderControl == null)
          this.paneHeaderControl = this.ConstructPaneHeader();
        GridUtil.AddRowDef(this.Grid, VisualBasePane.headerHeight, GridUnitType.Pixel);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.paneHeaderControl, row, 0, 1, 1);
        ++row;
      }
      else
        this.paneHeaderControl = (UserControl) null;
      if (this.BasePane.PrimaryHelpExplanation != null && !string.IsNullOrEmpty(this.BasePane.PrimaryHelpExplanation.Text))
      {
        if (this.primaryHelpExplanationControl == null)
          this.primaryHelpExplanationControl = this.ConstructPrimaryHelpTextHolder();
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.primaryHelpExplanationControl, row, 0, 1, this.BasePane.Columns);
        this.primaryHelpExplanationControl.Text = this.BasePane.PrimaryHelpExplanation.Text.Replace("<br/>", "\r\n");
        ++row;
      }
      else
        this.primaryHelpExplanationControl = (TextBlock) null;
      if (this.BasePane.FixedBodyHeightSpecified && this.BasePane.FixedBodyHeight > 0)
        this.bodyElementGrid.Height = (double) this.BasePane.FixedBodyHeight;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.elementToAdd, row, 0, 1, 1);
    }
  }
}
