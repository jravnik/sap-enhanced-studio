﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.NetworkChartPaneControls.VisualNetworkChartNodeVariant
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.NetworkChartPaneControls
{
  public class VisualNetworkChartNodeVariant : BaseSelectableControl
  {
    private BaseVisualControl parentVisualControl;
    private List<NetworkChartCell> m_CellData;
    private Grid containerGrid;
    private StackPanel stackPanel;
    private Grid cellDataGrid;
    private Grid buttonGrid;

    internal NetworkChartNodeVariant NetworkChartNodeVariant
    {
      get
      {
        return this.m_ModelObject as NetworkChartNodeVariant;
      }
    }

    public VisualNetworkChartNodeVariant(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.AllowDrop = true;
      this.parentVisualControl = parentControl;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return nameof (VisualNetworkChartNodeVariant);
      }
    }

    public override void LoadView()
    {
      base.LoadView();
      this.m_CellData = this.NetworkChartNodeVariant.CellDefinition;
      GridUtil.AddColumnDef(this.Grid, 1.0, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.stackPanel = new StackPanel();
      this.stackPanel.Orientation = Orientation.Vertical;
      this.CreateCellDataRegion();
      this.stackPanel.Margin = new Thickness(10.0, 10.0, 10.0, 10.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.stackPanel, 0, 0);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is NetworkChartNodeVariant)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of ControlUsageItem failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    private void CreateCellDataRegion()
    {
      this.containerGrid = new Grid();
      GridUtil.AddColumnDef(this.containerGrid, 110.0, 110.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.containerGrid, 60.0, 60.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.containerGrid, 2.0, 2.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.containerGrid, 15.0, 15.0, GridUnitType.Auto);
      this.cellDataGrid = new Grid();
      this.cellDataGrid.Background = (Brush) new SolidColorBrush(ComponentModelersConstants.NODE_BACKGROUND);
      if (this.m_CellData != null && this.m_CellData.Count > 0)
      {
        int row = 0;
        int col = 0;
        GridUtil.AddColumnDef(this.cellDataGrid, 1.0, GridUnitType.Star);
        foreach (NetworkChartCell networkChartCell in this.m_CellData)
        {
          GridUtil.AddRowDef(this.cellDataGrid, 1.0, GridUnitType.Star);
          GridUtil.PlaceElement(this.cellDataGrid, (UIElement) new VisualCellDefinition((IModelObject) networkChartCell, (BaseVisualControl) this), row, col);
          ++row;
        }
      }
      else
        GridUtil.AddRowDef(this.cellDataGrid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.containerGrid, (UIElement) this.cellDataGrid, 0, 0);
      this.CreateNode();
      this.stackPanel.Children.Add((UIElement) this.containerGrid);
    }

    private void CreateNode()
    {
      Grid grid1 = new Grid();
      GridUtil.AddColumnDef(grid1, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(grid1, 2.0, 2.0, GridUnitType.Auto);
      grid1.Background = (Brush) Brushes.Gray;
      GridUtil.PlaceElement(this.containerGrid, (UIElement) grid1, 1, 0);
      Grid grid2 = new Grid();
      GridUtil.AddColumnDef(grid2, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(grid2, 15.0, 15.0, GridUnitType.Auto);
      grid2.Background = (Brush) new SolidColorBrush(ComponentModelersConstants.NODE_BACKGROUND);
      this.CreateExpandCollapseBlock();
      GridUtil.PlaceElement(grid2, (UIElement) this.buttonGrid, 0, 0);
      GridUtil.PlaceElement(this.containerGrid, (UIElement) grid2, 2, 0);
    }

    private void CreateExpandCollapseBlock()
    {
      this.buttonGrid = new Grid();
      GridUtil.AddColumnDef(this.buttonGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.buttonGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.buttonGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.buttonGrid, 1.0, GridUnitType.Auto);
      TextBlock textBlock1 = new TextBlock();
      textBlock1.Text = "+";
      textBlock1.FontWeight = FontWeights.Bold;
      textBlock1.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.buttonGrid, (UIElement) textBlock1, 0, 0);
      TextBlock textBlock2 = new TextBlock();
      textBlock2.Text = "-";
      textBlock2.FontWeight = FontWeights.Bold;
      textBlock2.Margin = new Thickness(0.0, 0.0, 5.0, 0.0);
      GridUtil.PlaceElement(this.buttonGrid, (UIElement) textBlock2, 0, 2);
    }
  }
}
