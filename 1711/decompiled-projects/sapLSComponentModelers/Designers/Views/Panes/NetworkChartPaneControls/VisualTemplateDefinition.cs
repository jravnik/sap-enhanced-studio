﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.NetworkChartPaneControls.VisualTemplateDefinition
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.NetworkChartPaneControls
{
  public class VisualTemplateDefinition : BaseSelectableControl
  {
    private BaseVisualControl parentVisualControl;
    private VisualTemplateNodeDefinition m_TemplateNodeDefinition;

    internal NetworkChartTemplate NetworkChartTemplate
    {
      get
      {
        return this.m_ModelObject as NetworkChartTemplate;
      }
    }

    public VisualTemplateDefinition(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.AllowDrop = true;
      this.parentVisualControl = parentControl;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return nameof (VisualTemplateDefinition);
      }
    }

    public override void LoadView()
    {
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      if (this.NetworkChartTemplate.TemplateNodeDefinition == null || this.NetworkChartTemplate.TemplateNodeDefinition.Count <= 0)
        return;
      for (int row = 0; row < this.NetworkChartTemplate.TemplateNodeDefinition.Count; ++row)
      {
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
        this.m_TemplateNodeDefinition = new VisualTemplateNodeDefinition((IModelObject) this.NetworkChartTemplate.TemplateNodeDefinition[row], (BaseVisualControl) this);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.m_TemplateNodeDefinition, row, 0);
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is NetworkChartTemplate)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of TemplateDefinition failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }
  }
}
