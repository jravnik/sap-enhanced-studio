﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.NetworkChartPaneControls.VisualCellDefinition
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.NetworkChartPaneControls
{
  public class VisualCellDefinition : BaseSelectableControl
  {
    private BaseVisualControl parentVisualControl;

    internal NetworkChartCell ChartCell
    {
      get
      {
        return this.m_ModelObject as NetworkChartCell;
      }
    }

    public VisualCellDefinition(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.AllowDrop = true;
      this.parentVisualControl = parentControl;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      this.Grid.Background = (Brush) new SolidColorBrush(ComponentModelersConstants.NODE_BACKGROUND);
      VisualItem visualItem = new VisualItem((IModelObject) this.ChartCell.Item, (BaseVisualControl) this);
      visualItem.HorizontalAlignment = HorizontalAlignment.Stretch;
      GridUtil.PlaceElement(this.Grid, (UIElement) visualItem, 0, 0);
    }

    public override string SelectionText
    {
      get
      {
        return "NetworkChartCell";
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is NetworkChartCell)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == NetworkChartCell.AlignmentProperty)
        {
          if (this.parentVisualControl == null)
            return;
          this.parentVisualControl.ReloadView();
        }
        else
          this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of NetworkChartCell failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }
  }
}
