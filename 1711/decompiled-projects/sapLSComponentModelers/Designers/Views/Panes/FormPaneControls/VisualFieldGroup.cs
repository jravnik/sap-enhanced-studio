﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.FormPaneControls.VisualFieldGroup
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.FormPaneControls
{
  public class VisualFieldGroup : BaseSelectableControl
  {
    private const string DEFAULT_MORE_LINK_TEXT = "More";
    private DisplayTextControl displayTextControl;
    private VisualLink moreLinkControl;
    private double maximumLabelWidth;
    private BaseVisualControl parentVisualControl;

    internal FieldGroup FieldGroup
    {
      get
      {
        return this.m_ModelObject as FieldGroup;
      }
    }

    public double MaximumLabelWidth
    {
      get
      {
        return this.maximumLabelWidth;
      }
      set
      {
        this.maximumLabelWidth = value;
        this.UpdateMaximumLabelWidth();
      }
    }

    public VisualFieldGroup(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.parentVisualControl = parentControl;
      this.IsDraggable = true;
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.Grid.Margin = new Thickness(0.0, 0.0, 0.0, 15.0);
      this.IsSpanable = true;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        for (int index = 0; index < this.FieldGroup.ColSpan; ++index)
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        int row = 0;
        int num1 = 0;
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.CreateSectionHeaderAndMoreLinkGrid(), row, num1, 1, this.FieldGroup.ColSpan);
        int num2 = row + 1;
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        this.maximumLabelWidth = this.GetMaxLabelLength();
        foreach (SectionGroupItem field in this.FieldGroup.Fields)
        {
          if (num1 == this.FieldGroup.ColSpan || field.ForceNewLine || num1 + field.ColSpan > this.FieldGroup.ColSpan)
          {
            ++num2;
            GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
            num1 = 0;
          }
          VisualField visualField = new VisualField(field, this);
          GridUtil.PlaceElement(this.Grid, (UIElement) visualField, num2, num1, field.RowSpan, field.ColSpan);
          visualField.InitializeSpanningDetails(1, num1, num2);
          if (this.maximumLabelWidth < visualField.LabelControl.DesiredWidth)
            this.maximumLabelWidth = visualField.LabelControl.DesiredWidth;
          num1 += field.ColSpan;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of Field Group failed.", ex));
      }
    }

    private double GetMaxLabelLength()
    {
      string str1 = string.Empty;
      foreach (SectionGroupItem field in this.FieldGroup.Fields)
      {
        string str2 = field.Label != null ? field.Label.Text : string.Empty;
        if (str2 != null && str2.Length > str1.Length)
          str1 = str2;
      }
      return (double) (6 * (str1.Length + 1));
    }

    public override string SelectionText
    {
      get
      {
        return "FieldGroup";
      }
    }

    internal override void TriggerSelection(BaseSelectableControl vControl, bool isTriggeredByControlAddOperation)
    {
      if (isTriggeredByControlAddOperation)
      {
        if (this.FieldGroup.Fields != null && this.FieldGroup.Fields.Count > 0)
        {
          ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
          SectionGroupItem field = this.FieldGroup.Fields[0];
          if (parentDesigner == null || !parentDesigner.DesignerControlDictionary.ContainsKey(field.Id))
            return;
          BaseSelectableControl designerControl = parentDesigner.DesignerControlDictionary[field.Id] as BaseSelectableControl;
          if (designerControl == null)
            return;
          base.TriggerSelection(designerControl, isTriggeredByControlAddOperation);
        }
        else
          base.TriggerSelection((BaseSelectableControl) this, isTriggeredByControlAddOperation);
      }
      else
        base.TriggerSelection(vControl, isTriggeredByControlAddOperation);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == FieldGroup.ShowGroupNameProperty)
          this.displayTextControl.Visibility = Converter.ToBoolean((DependentProperty) this.FieldGroup.ShowGroupName, true) ? Visibility.Visible : Visibility.Collapsed;
        else if (e.PropertyName == FieldGroup.SectionGroupNameProperty && this.FieldGroup.SectionGroupName != null)
          this.displayTextControl.Text = this.FieldGroup.SectionGroupName.Text;
        else if (e.PropertyName == FieldGroup.ShowMoreLinkProperty)
        {
          this.moreLinkControl.Visibility = Converter.ToBoolean((DependentProperty) this.FieldGroup.ShowMoreLink, false) ? Visibility.Visible : Visibility.Collapsed;
          this.UpdateMoreLinkText();
        }
        else if (e.PropertyName == FieldGroup.MoreLinkLabelProperty)
          this.UpdateMoreLinkText();
        else if (e.PropertyName == FieldGroup.ColSpanProperty || e.PropertyName == FieldGroup.RowSpanProperty || e.PropertyName == FieldGroup.ForceNewLineProperty)
        {
          if (this.parentVisualControl == null)
            return;
          this.parentVisualControl.ReloadView();
        }
        else
        {
          if (!(e.PropertyName == FieldGroup.FieldsProperty))
            return;
          this.ReloadView();
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update the property changes in the model.", ex));
      }
    }

    protected override void OnSpanLeftArrowClicked(object sender, EventArgs e)
    {
      base.OnSpanLeftArrowClicked(sender, e);
      try
      {
        this.FieldGroup.ColSpan = this.FieldGroup.ColSpan - 1;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Spanning of the SectionGroup Failed.", ex));
      }
    }

    protected override void OnSpanRightArrowClicked(object sender, EventArgs e)
    {
      base.OnSpanRightArrowClicked(sender, e);
      try
      {
        this.FieldGroup.ColSpan = this.FieldGroup.ColSpan + 1;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Spanning of the SectionGroup Failed.", ex));
      }
    }

    protected override void OnSpanDownArrowClicked(object sender, EventArgs e)
    {
      base.OnSpanDownArrowClicked(sender, e);
      try
      {
        this.FieldGroup.RowSpan = this.FieldGroup.RowSpan + 1;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Spanning of the SectionGroup Failed.", ex));
      }
    }

    protected override void OnSpanUpArrowClicked(object sender, EventArgs e)
    {
      base.OnSpanUpArrowClicked(sender, e);
      try
      {
        this.FieldGroup.RowSpan = this.FieldGroup.RowSpan - 1;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Spanning of the SectionGroup Failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Paste");
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == (object) "Delete")
      {
        if (ValidationManager.GetInstance().IsDeletable((IExtensible) this.FieldGroup))
        {
          this.FieldGroup.Parent.RemoveModelObject((IModelObject) this.FieldGroup);
        }
        else
        {
          int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.ElementNotDeletable, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }
      }
      else
      {
        if (source.Header != (object) "Paste")
          return;
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyPasteOperation();
      }
    }

    protected override void OnBOElementDragOver(System.Windows.DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == System.Windows.DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
      if (boElement != null && boElement.ParentQuery != null && !(this.FieldGroup.Parent is FindFormPane))
        e.Effects = System.Windows.DragDropEffects.None;
      if (boElement == null || boElement.ParentAction == null)
        return;
      PaneContainer parentLayoutCell = Utilities.GetParentLayoutCell(this.m_ModelObject);
      if (parentLayoutCell == null || parentLayoutCell.Parent is ActionForm)
        return;
      e.Effects = System.Windows.DragDropEffects.None;
    }

    protected override void OnToolBoxControlDragOver(System.Windows.DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      base.OnToolBoxControlDragOver(e, toolboxEventArg);
      if (!ValidationManager.GetInstance().ValidateModel(this.Model, toolboxEventArg.Tag.ClassName))
        return;
      if (this.Model != null && this.Model.Parent is FindFormPane)
      {
        if (this.Model is FieldGroup && toolboxEventArg.Tag.ControlName == "RadioButtonGroup" || (toolboxEventArg.Tag.ControlName == "LayoutGridPane" || toolboxEventArg.Tag.ControlName == "LayoutStackPane") || toolboxEventArg.Tag.ControlName == "LayoutBorderPane")
          e.Effects = System.Windows.DragDropEffects.None;
        else
          e.Effects = System.Windows.DragDropEffects.Copy;
      }
      else
      {
        if (this.Model == null || !(this.Model.Parent is FormPane) || !(toolboxEventArg.Tag.ControlName == "LayoutGridPane") && !(toolboxEventArg.Tag.ControlName == "LayoutStackPane") && !(toolboxEventArg.Tag.ControlName == "LayoutBorderPane"))
          return;
        e.Effects = System.Windows.DragDropEffects.None;
      }
    }

    protected override void OnBOElementDrop(BOBrowserDragEventArgs boBrowserArgs)
    {
      try
      {
        if (this.FieldGroup.Parent is FindFormPane)
        {
          FindFormPane parent = this.FieldGroup.Parent as FindFormPane;
          BOElement boElement = (BOElement) null;
          BOElementAttribute elementAttribute = (BOElementAttribute) null;
          if (boBrowserArgs != null && boBrowserArgs.Count > 0)
          {
            boElement = boBrowserArgs[0].DraggedElement as BOElement;
            if (boElement == null)
            {
              elementAttribute = boBrowserArgs[0].DraggedElement as BOElementAttribute;
              if (elementAttribute != null)
                boElement = elementAttribute.ParentElement;
            }
          }
          if (boElement != null && boElement.ParentQuery != null || elementAttribute != null)
          {
            string queryPath = Utilities.GetQueryPath(boBrowserArgs[0].PathToElement);
            if (string.IsNullOrEmpty(parent.Query.QueryPath) || parent.Query.QueryPath == queryPath)
            {
              string queryParameterName = Utilities.GetQueryParameterName(boBrowserArgs[0].PathToElement);
              if (!Utilities.IsQueryParameterBound(parent.Query, queryParameterName))
              {
                base.OnBOElementDrop(boBrowserArgs);
              }
              else
              {
                int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.QueryParameterAlreadyBound, MessageBoxButtons.OK, MessageBoxIcon.Hand);
              }
            }
            else
            {
              int num1 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.ParameterFromDifferentQuery, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
          }
          else
          {
            int num2 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.OnlyQueryParameterAllowed, MessageBoxButtons.OK, MessageBoxIcon.Hand);
          }
        }
        else
          base.OnBOElementDrop(boBrowserArgs);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Adding QueryParameter to AdvancedFindForm failed.", ex));
      }
    }

    protected override void OnDataModelElementDragOver(System.Windows.DragEventArgs e, DataModelDragEventArgs dataModelEventArgs)
    {
      if (dataModelEventArgs.DataElement.IsQueryBinding && !(this.Model.Parent is FindFormPane))
        e.Effects = System.Windows.DragDropEffects.None;
      else
        base.OnDataModelElementDragOver(e, dataModelEventArgs);
    }

    protected override void OnDataModelElementDrop(DataModelDragEventArgs dmArgs)
    {
      try
      {
        if (this.FieldGroup.Parent is FindFormPane)
        {
          FindFormPane parent = this.FieldGroup.Parent as FindFormPane;
          if (dmArgs.DataElement.IsQueryBinding)
          {
            if (parent.Query.Name == dmArgs.DataElement.AssociatedQueryParameterBinding.QueryName)
            {
              base.OnDataModelElementDrop(dmArgs);
            }
            else
            {
              int num1 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.ParameterFromDifferentQuery, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
          }
          else
          {
            int num2 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.OnlyQueryParameterAllowed, MessageBoxButtons.OK, MessageBoxIcon.Hand);
          }
        }
        else
          base.OnDataModelElementDrop(dmArgs);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Adding QueryParameter to AdvancedFindForm failed.", ex));
      }
    }

    protected override void OnBaseVisualControlDragOver(System.Windows.DragEventArgs e, IModelObject droppedModel)
    {
      if (droppedModel is FieldGroup && this.FieldGroup.Parent.GetType() != (droppedModel as FieldGroup).Parent.GetType())
        e.Effects = System.Windows.DragDropEffects.None;
      else
        base.OnBaseVisualControlDragOver(e, droppedModel);
    }

    public void InitializeSpanningDetails(int desiredRowCount)
    {
      FormPane parent1 = this.FieldGroup.Parent as FormPane;
      if (parent1 != null)
        this.InitializeSpanningDetails(new SpanningInfo(parent1.Columns, desiredRowCount, this.FieldGroup.Column, this.FieldGroup.Row, this.FieldGroup.ColSpan, this.FieldGroup.RowSpan), SpanningStyles.both);
      FindFormPane parent2 = this.FieldGroup.Parent as FindFormPane;
      if (parent2 == null)
        return;
      this.InitializeSpanningDetails(new SpanningInfo(parent2.Columns, -1, this.FieldGroup.Column, this.FieldGroup.Row, this.FieldGroup.ColSpan, this.FieldGroup.RowSpan), SpanningStyles.both);
    }

    private Grid CreateSectionHeaderAndMoreLinkGrid()
    {
      Grid grid = new Grid();
      GridUtil.AddColumnDef(grid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
      bool boolean1 = Converter.ToBoolean((DependentProperty) this.FieldGroup.ShowMoreLink, false);
      bool boolean2 = Converter.ToBoolean((DependentProperty) this.FieldGroup.ShowGroupName, true);
      string text = string.Empty;
      if (this.FieldGroup.SectionGroupName != null)
        text = this.FieldGroup.SectionGroupName.Text;
      this.displayTextControl = new DisplayTextControl(TextViewStyles.SectionGroupTitle, text);
      GridUtil.PlaceElement(grid, (UIElement) this.displayTextControl, 0, 0);
      this.moreLinkControl = new VisualLink(VisualLink.LinkStyles.Standard);
      this.moreLinkControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
      this.UpdateMoreLinkText();
      GridUtil.PlaceElement(grid, (UIElement) this.moreLinkControl, 0, 1);
      this.displayTextControl.Visibility = boolean2 ? Visibility.Visible : Visibility.Collapsed;
      this.moreLinkControl.Visibility = boolean1 ? Visibility.Visible : Visibility.Collapsed;
      return grid;
    }

    private void UpdateMoreLinkText()
    {
      if (this.FieldGroup.MoreLinkLabel != null && !string.IsNullOrEmpty(this.FieldGroup.MoreLinkLabel.Text))
      {
        this.moreLinkControl.Text = this.FieldGroup.MoreLinkLabel.Text;
        this.moreLinkControl.InvalidateMeasure();
      }
      else
        this.moreLinkControl.Text = "More";
    }

    private void UpdateMaximumLabelWidth()
    {
      foreach (UIElement child in this.Grid.Children)
      {
        VisualField visualField = child as VisualField;
        if (visualField != null)
          visualField.LabelControl.ArrangeTextBlockWidth(this.maximumLabelWidth);
      }
    }
  }
}
