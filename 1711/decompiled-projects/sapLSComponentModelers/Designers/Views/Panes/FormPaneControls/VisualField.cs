﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.FormPaneControls.VisualField
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Oberon.Controller;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using SAP.BYD.LS.UIDesigner.UICore.Dialogs;
using SAP.BYD.LS.UIDesigner.UICore.OBNNavigationArea;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.FormPaneControls
{
  public class VisualField : BaseSelectableControl
  {
    private VisualLabel labelControl;
    private IModelObject sectionGroupModel;
    private UIElement visualControl;
    private VisualFieldGroup visualSectionGroup;

    public VisualLabel LabelControl
    {
      get
      {
        return this.labelControl;
      }
    }

    internal SectionGroupItem SectionGroupItem
    {
      get
      {
        return this.m_ModelObject as SectionGroupItem;
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Field";
      }
    }

    public VisualField(SectionGroupItem model, VisualFieldGroup sectionGroup)
      : base((IModelObject) model)
    {
      this.sectionGroupModel = (IModelObject) model;
      this.visualSectionGroup = sectionGroup;
      this.IsDraggable = true;
      this.IsSpanable = true;
      this.SectionGroupItem.Item.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      this.LoadView();
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.ControlBackground = (Brush) ComponentModelersConstants.FIELD_AREA_BRUSH;
      this.AllowDrop = true;
    }

    protected override void OnSpanLeftArrowClicked(object sender, EventArgs e)
    {
      base.OnSpanLeftArrowClicked(sender, e);
      try
      {
        this.SectionGroupItem.ColSpan = this.SectionGroupItem.ColSpan - 1;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Spanning of the Control Failed.", ex));
      }
    }

    protected override void OnSpanRightArrowClicked(object sender, EventArgs e)
    {
      base.OnSpanRightArrowClicked(sender, e);
      try
      {
        this.SectionGroupItem.ColSpan = this.SectionGroupItem.ColSpan + 1;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Spanning of the Control Failed.", ex));
      }
    }

    public override void ReloadView()
    {
      base.ReloadView();
      this.labelControl.ArrangeTextBlockWidth(this.visualSectionGroup.MaximumLabelWidth);
      this.SectionGroupItem.Item.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
    }

    public override void LoadView()
    {
      try
      {
        GridUtil.AddRowDef(this.Grid, 25.0, this.SectionGroupItem.Item is RadioButtonGroup ? GridUnitType.Auto : GridUnitType.Pixel);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 5.0, GridUnitType.Pixel);
        if (this.SectionGroupItem.Item.CCTSType != UXCCTSTypes.none)
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        else if (this.SectionGroupItem.Item.FieldWidth != null && this.SectionGroupItem.Item.FieldWidth != string.Empty)
          this.SetFieldWidthOfControls();
        else
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        bool showAsterisk = false;
        if (this.SectionGroupItem.Item is EditControl)
          showAsterisk = Converter.ToBoolean((DependentProperty) (this.SectionGroupItem.Item as EditControl).Mandatory, false);
        if (this.SectionGroupItem.Item is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link)
          showAsterisk = Converter.ToBoolean((DependentProperty) (this.SectionGroupItem.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).Mandatory, false);
        else if (this.SectionGroupItem.Item is CompoundField)
        {
          CompoundField compoundField = this.SectionGroupItem.Item as CompoundField;
          if (compoundField.Items != null)
          {
            foreach (AbstractControl abstractControl in compoundField.Items)
            {
              if (abstractControl is EditControl)
              {
                EditControl editControl = abstractControl as EditControl;
                showAsterisk |= Converter.ToBoolean((DependentProperty) editControl.Mandatory, false);
              }
            }
          }
        }
        this.labelControl = new VisualLabel(this.SectionGroupItem.Label != null ? this.SectionGroupItem.Label.Text : string.Empty, showAsterisk, false, false, this.visualSectionGroup.MaximumLabelWidth);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.labelControl, 0, 0);
        if (this.SectionGroupItem.NoLabel)
          this.labelControl.Visibility = Visibility.Collapsed;
        if (!this.SectionGroupItem.LabelVisible)
          this.labelControl.Visibility = Visibility.Hidden;
        this.visualControl = ControlFactory.ConstructFormpaneControl(this.SectionGroupItem.Item, UXCCTSTypes.none, UsageType.form);
        if (this.SectionGroupItem.Item is TextEditControl)
          this.SetHeightOfTextEditControl();
        else if (this.SectionGroupItem.Item is LayoutContainerControl)
          this.Grid.RowDefinitions[0].Height = new GridLength(1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, this.visualControl, 0, 2);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Field Control initialization failed.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        if (e.PropertyName == ControlUsage.LabelProperty)
        {
          this.labelControl.Text = this.SectionGroupItem.Label != null ? this.SectionGroupItem.Label.Text : string.Empty;
          if (this.visualSectionGroup == null || this.visualSectionGroup.MaximumLabelWidth >= this.labelControl.DesiredWidth)
            return;
          this.visualSectionGroup.MaximumLabelWidth = this.labelControl.DesiredWidth;
        }
        else if (e.PropertyName == ControlUsage.ItemProperty)
        {
          int int32_1 = Convert.ToInt32(this.visualControl.GetValue(Grid.ColumnProperty));
          int int32_2 = Convert.ToInt32(this.visualControl.GetValue(Grid.ColumnSpanProperty));
          this.Grid.Children.Remove(this.visualControl);
          this.visualControl = ControlFactory.ConstructFormpaneControl(this.SectionGroupItem.Item, UXCCTSTypes.none, UsageType.form);
          GridUnitType unitType = GridUnitType.Pixel;
          if (this.SectionGroupItem.Item is RadioButtonGroup)
            unitType = GridUnitType.Auto;
          this.Grid.RowDefinitions.Clear();
          GridUtil.AddRowDef(this.Grid, 25.0, unitType);
          GridUtil.PlaceElement(this.Grid, this.visualControl, 0, int32_1, 1, int32_2);
        }
        else if (e.PropertyName == AbstractControl.EnabledProperty || e.PropertyName == EditControl.ReadOnlyProperty)
          ControlFactory.SetEditableProperties(this.visualControl, this.SectionGroupItem.Item);
        else if (this.SectionGroupItem.Item is EditControl && e.PropertyName == EditControl.MandatoryProperty)
          this.labelControl.ShowAsterisk = Converter.ToBoolean((DependentProperty) (this.SectionGroupItem.Item as EditControl).Mandatory, false);
        else if (this.SectionGroupItem.Item is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link && e.PropertyName == SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link.MandatoryProperty)
          this.labelControl.ShowAsterisk = Converter.ToBoolean((DependentProperty) (this.SectionGroupItem.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).Mandatory, false);
        else if (e.PropertyName == SectionGroupItem.ForceNewLineProperty)
        {
          if (this.visualSectionGroup == null)
            return;
          this.visualSectionGroup.ReloadView();
        }
        else if (e.PropertyName == StaticText.FormatProperty)
        {
          StaticText staticText = this.SectionGroupItem.Item as StaticText;
          VisualStaticText visualControl = this.visualControl as VisualStaticText;
          if (visualControl == null || visualControl == null)
            return;
          if (staticText.Format == FormatType.emphasized)
            visualControl.StaticTextFormat = VisualStaticText.StaticTextFormats.Emphasized;
          else
            visualControl.StaticTextFormat = VisualStaticText.StaticTextFormats.Standard;
        }
        else if (e.PropertyName == RadioButtonGroup.OrientationProperty)
        {
          RadioButtonGroup radioButtonGroup = this.SectionGroupItem.Item as RadioButtonGroup;
          VisualRadioButtonGroup visualControl = this.visualControl as VisualRadioButtonGroup;
          if (radioButtonGroup == null || visualControl == null)
            return;
          visualControl.Orientation = (System.Windows.Controls.Orientation) Enum.Parse(typeof (System.Windows.Controls.Orientation), radioButtonGroup.Orientation.ToString(), true);
        }
        else if (e.PropertyName == SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link.HasMenuProperty)
        {
          SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link link = this.SectionGroupItem.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link;
          VisualStaticText visualControl = this.visualControl as VisualStaticText;
          if (link == null || visualControl == null)
            return;
          visualControl.IsMenuVisible = link.HasMenu;
        }
        else if (e.PropertyName == TextEditControl.RowsProperty)
          this.SetHeightOfTextEditControl();
        else if (e.PropertyName == CompoundField.ItemsProperty)
        {
          int row = Grid.GetRow(this.visualControl);
          int column = Grid.GetColumn(this.visualControl);
          int rowSpan = Grid.GetRowSpan(this.visualControl);
          int columnSpan = Grid.GetColumnSpan(this.visualControl);
          BaseVisualControl visualControl = this.visualControl as BaseVisualControl;
          if (visualControl != null)
            visualControl.Terminate();
          this.Grid.Children.Remove(this.visualControl);
          this.visualControl = ControlFactory.ConstructFormpaneControl(this.SectionGroupItem.Item, UXCCTSTypes.none, UsageType.form);
          GridUtil.PlaceElement(this.Grid, this.visualControl, row, column, rowSpan, columnSpan);
          if (!(this.SectionGroupItem.Item is CompoundField))
            return;
          bool flag = false;
          CompoundField compoundField = this.SectionGroupItem.Item as CompoundField;
          if (compoundField.Items != null)
          {
            foreach (AbstractControl abstractControl in compoundField.Items)
            {
              if (abstractControl is EditControl)
              {
                EditControl editControl = abstractControl as EditControl;
                flag |= Converter.ToBoolean((DependentProperty) editControl.Mandatory, false);
              }
            }
          }
          this.labelControl.ShowAsterisk = flag;
        }
        else if (e.PropertyName == SectionGroupItem.ColSpanProperty || e.PropertyName == SectionGroupItem.RowSpanProperty || e.PropertyName == SectionGroupItem.ForceNewLineProperty)
          this.visualSectionGroup.ReloadView();
        else
          this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update UI on Property Changed event.", ex));
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      this.SectionGroupItem.Item.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        base.AddContextMenuItems();
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Cut");
        this.AddContextMenuItem("Copy");
        if (this.SectionGroupItem != null && this.SectionGroupItem.Item is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link && (this.SectionGroupItem.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick != null)
        {
          foreach (UXEventHandlerOperation operation in (this.SectionGroupItem.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick.Operations)
          {
            if (!(operation is EventHandlerOperationTypeFireOutport))
            {
              this.AddSeperatorToContextMenu();
              this.AddContextMenuItem("Add OBN using Wizard");
            }
            else
              break;
          }
        }
        else
        {
          this.AddSeperatorToContextMenu();
          this.AddContextMenuItem("Add OBN using Wizard");
        }
        if (this.SectionGroupItem == null || !(this.SectionGroupItem.Item is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link) || (this.SectionGroupItem.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick == null)
          return;
        foreach (UXEventHandlerOperation operation in (this.SectionGroupItem.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick.Operations)
        {
          if (operation is EventHandlerOperationTypeFireOutport)
          {
            this.AddSeperatorToContextMenu();
            this.AddContextMenuItem("Edit OBN using Wizard");
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for VisualField.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      try
      {
        base.HandleContextMenuItemClick(source);
        if (source.Header == (object) "Delete")
        {
          FieldGroup parent = this.SectionGroupItem.Parent as FieldGroup;
          if (parent == null)
            return;
          parent.RemoveModelObject((IModelObject) this.SectionGroupItem);
        }
        else if (source.Header == (object) "Copy")
          ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCopyOperation();
        else if (source.Header == (object) "Cut")
          ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCutOperation();
        else if (source.Header == (object) "Add OBN using Wizard")
        {
          WizardDialog wizardDialog = new WizardDialog(OBNWizardStepFactory.Instance.GetWizardSteps(Utilities.GetFloorplanObject(this.sectionGroupModel), (IOBN) null));
          int num = (int) wizardDialog.ShowDialog();
          IEventHandler wizardResult = wizardDialog.WizardResult as IEventHandler;
          if (wizardResult == null)
            return;
          (this.SectionGroupItem.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick = wizardResult;
        }
        else
        {
          if (source.Header != (object) "Edit OBN using Wizard")
            return;
          IFloorplan floorplanObject = Utilities.GetFloorplanObject(this.sectionGroupModel);
          IEventHandler onClick = (this.SectionGroupItem.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick;
          IOutPortType outPortType = (IOutPortType) null;
          foreach (UXEventHandlerOperation operation in onClick.Operations)
          {
            if (operation is EventHandlerOperationTypeFireOutport)
            {
              foreach (IOutPortType outPort in floorplanObject.ControllerInterface.OutPorts)
              {
                if (outPort.Name == (operation as EventHandlerOperationTypeFireOutport).OutPlug)
                {
                  outPortType = outPort;
                  break;
                }
              }
            }
          }
          foreach (OberonOBN oberonObn in floorplanObject.ControllerNavigation.OBN)
          {
            if (oberonObn.OutPlug.Id == outPortType.Id)
            {
              WizardDialog wizardDialog = new WizardDialog(OBNWizardStepFactory.Instance.GetWizardSteps(floorplanObject, (IOBN) oberonObn));
              int num = (int) wizardDialog.ShowDialog();
              if (wizardDialog.WizardResult == null)
                break;
              (this.SectionGroupItem.Item as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.Link).OnClick = wizardDialog.WizardResult as IEventHandler;
              break;
            }
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    protected override void OnBaseVisualControlDrop(IModelObject droppedModel)
    {
      try
      {
        if (droppedModel == null || !(droppedModel is SectionGroupItem))
          return;
        SectionGroupItem sectionGroupItem = droppedModel as SectionGroupItem;
        FieldGroup parent1 = droppedModel.Parent as FieldGroup;
        FieldGroup parent2 = this.SectionGroupItem.Parent as FieldGroup;
        if (parent2 == null)
          return;
        if (parent2.ColSpan < sectionGroupItem.ColSpan)
          throw new InformationException(SAP.BYD.LS.UIDesigner.Model.Resource.ColSpanMessage);
        int index = parent2.Fields.IndexOf(this.SectionGroupItem);
        parent1.RemoveModelObject(droppedModel, true);
        parent2.AddFieldToGroup(droppedModel as SectionGroupItem, index);
        droppedModel.Parent = (IModelObject) parent2;
      }
      catch (InformationException ex)
      {
        ExceptionManager.GetInstance().HandleException(ex);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    protected override void OnDataModelElementDragOver(System.Windows.DragEventArgs e, DataModelDragEventArgs dataModelEventArgs)
    {
      if (this.Model != null && !(this.Model.Parent.Parent is FindFormPane))
        e.Effects = System.Windows.DragDropEffects.None;
      else
        base.OnDataModelElementDragOver(e, dataModelEventArgs);
    }

    protected override void OnToolBoxControlDragOver(System.Windows.DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      base.OnToolBoxControlDragOver(e, toolboxEventArg);
      if (!ValidationManager.GetInstance().ValidateModel(this.Model, toolboxEventArg.Tag.ClassName) || this.Model == null || !(this.Model.Parent.Parent is FindFormPane))
        return;
      if (this.Model is SectionGroupItem && toolboxEventArg.Tag.ControlName == "RadioButtonGroup")
        e.Effects = System.Windows.DragDropEffects.None;
      else
        e.Effects = System.Windows.DragDropEffects.Copy;
    }

    public void InitializeSpanningDetails(int desiredRowCount, int colIndex, int rowIndex)
    {
      FieldGroup parent = this.SectionGroupItem.Parent as FieldGroup;
      if (parent == null)
        return;
      this.InitializeSpanningDetails(new SpanningInfo(parent.ColSpan, desiredRowCount, colIndex, rowIndex, this.SectionGroupItem.ColSpan, this.SectionGroupItem.RowSpan), SpanningStyles.both);
    }

    private void SetFieldWidthOfControls()
    {
      double widthValue = ControlFactory.GetWidthValue(this.SectionGroupItem.Item.FieldWidth);
      string nonDigitPart = Utilities.GetNonDigitPart(this.SectionGroupItem.Item.FieldWidth);
      if (nonDigitPart == "%")
      {
        GridUtil.AddColumnDef(this.Grid, widthValue, GridUnitType.Star);
        GridUtil.AddColumnDef(this.Grid, 100.0 - widthValue, GridUnitType.Star);
      }
      else
      {
        if (!(nonDigitPart == "ex") && !(nonDigitPart == "px"))
          return;
        GridUtil.AddColumnDef(this.Grid, widthValue, GridUnitType.Pixel);
      }
    }

    private void SetHeightOfTextEditControl()
    {
      TextEditControl textEditControl = this.SectionGroupItem.Item as TextEditControl;
      VisualTextEditControl visualControl = this.visualControl as VisualTextEditControl;
      if (textEditControl == null || visualControl == null || textEditControl.Rows == 0)
        return;
      visualControl.Rows = textEditControl.Rows;
      this.Grid.RowDefinitions[0].Height = new GridLength((double) textEditControl.Rows * 25.0, GridUnitType.Pixel);
    }

    protected override void OnBOElementDrop(BOBrowserDragEventArgs boBrowserArgs)
    {
      BOElement boElement = (BOElement) null;
      BOElementAttribute elementAttribute = (BOElementAttribute) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
      {
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
        if (boElement == null)
        {
          elementAttribute = boBrowserArgs[0].DraggedElement as BOElementAttribute;
          if (elementAttribute != null)
            boElement = elementAttribute.ParentElement;
        }
      }
      FieldGroup parent1 = this.SectionGroupItem.Parent as FieldGroup;
      if (parent1 != null && parent1.Parent is FindFormPane)
      {
        FindFormPane parent2 = parent1.Parent as FindFormPane;
        if (boElement != null && boElement.ParentQuery != null || elementAttribute != null)
        {
          string queryPath = Utilities.GetQueryPath(boBrowserArgs[0].PathToElement);
          if (string.IsNullOrEmpty(parent2.Query.QueryPath) || parent2.Query.QueryPath == queryPath)
          {
            string queryParameterName = Utilities.GetQueryParameterName(boBrowserArgs[0].PathToElement);
            if (!Utilities.IsQueryParameterBound(parent2.Query, queryParameterName))
            {
              base.OnBOElementDrop(boBrowserArgs);
            }
            else
            {
              int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.QueryParameterAlreadyBound, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
          }
          else
          {
            int num1 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.ParameterFromDifferentQuery, MessageBoxButtons.OK, MessageBoxIcon.Hand);
          }
        }
        else
        {
          int num2 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.OnlyQueryParameterAllowed, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      else
        base.OnBOElementDrop(boBrowserArgs);
    }
  }
}
