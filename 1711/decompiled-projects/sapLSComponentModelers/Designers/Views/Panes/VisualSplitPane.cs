﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualSplitPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  internal class VisualSplitPane : VisualBasePane, IContextMenuContainer
  {
    private SplitPane m_SplitPane;
    private BaseVisualControl paneControl;

    public VisualSplitPane(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_SplitPane = this.m_ModelObject as SplitPane;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "SplitPane";
      }
    }

    protected SplitPane SplitPane
    {
      get
      {
        return this.m_ModelObject as SplitPane;
      }
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for VisualForm.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      try
      {
        if (source.Header == null || source.Header != (object) "Delete" || this.SplitPane.Parent == null)
          return;
        this.SplitPane.Parent.RemoveModelObject((IModelObject) this.SplitPane);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (!(e.PropertyName == SplitPane.DetailsHeaderProperty) && !(e.PropertyName == SplitPane.MasterHeaderProperty) && !(e.PropertyName == SplitPane.DetailsAreaProperty))
          return;
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of Formpane was not reflected in UI because of unknown reasons.", ex));
      }
    }

    public override void ConstructBodyElement()
    {
      try
      {
        this.BodyElementGrid.Margin = ComponentModelersConstants.CALENDAR_CONTROL_BORDER_MARGIN;
        GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.BodyElementGrid, 100.0, 100.0, GridUnitType.Star);
        if (this.SplitPane == null)
          return;
        if (this.SplitPane.MasterItem != null)
        {
          TextBlock textBlock = new TextBlock();
          textBlock.Height = 20.0;
          if (this.SplitPane.MasterHeader != null)
            textBlock.Text = this.SplitPane.MasterHeader.Text;
          textBlock.FontSize = 14.0;
          textBlock.TextAlignment = TextAlignment.Center;
          textBlock.Background = (Brush) Brushes.LightGray;
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) textBlock, 0, 0);
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) new VisualSplitPaneMasterItem((IModelObject) this.SplitPane.MasterItem), 1, 0, 2, 1);
        }
        int row = 0;
        if (this.SplitPane.DetailsItem != null)
        {
          TextBlock textBlock = new TextBlock();
          textBlock.Height = 20.0;
          if (this.SplitPane.DetailsHeader != null)
            textBlock.Text = this.SplitPane.DetailsHeader.Text;
          textBlock.FontSize = 14.0;
          textBlock.TextAlignment = TextAlignment.Center;
          textBlock.Background = (Brush) Brushes.LightGray;
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) textBlock, 0, 1);
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) new VisualSplitPaneDetailItem((IModelObject) this.SplitPane.DetailsItem), 1, 1);
          row = 2;
        }
        if (this.SplitPane.DetailsArea == null)
          return;
        this.paneControl = PaneFactory.GetVisualPaneControl((IModelObject) this.SplitPane.DetailsArea);
        GridUtil.AddRowDef(this.BodyElementGrid, 100.0, 100.0, GridUnitType.Star);
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.paneControl, row, 1);
      }
      catch
      {
      }
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
