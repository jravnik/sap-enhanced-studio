﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualDataSetPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualDataSetPane : VisualBasePane
  {
    private const string DELETE = "Delete";
    private int index;
    private AdvancedDataSetPane DataSetPane;
    private VisualAdvancedDataSetPaneVariant advancedDataSetPaneVariant;
    private VisualDefaultSet defaultSetMapping;
    private IDefaultSetMapping m_DefaultSetMapping;
    private InitFormType m_InitializeForm;

    public VisualDataSetPane(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.DataSetPane = model as AdvancedDataSetPane;
      this.LoadView();
    }

    public IDefaultSetMapping SelectedDefaultSetMapping
    {
      get
      {
        return this.m_DefaultSetMapping;
      }
      set
      {
        this.m_DefaultSetMapping = value;
      }
    }

    public InitFormType InitializeForm
    {
      get
      {
        return this.m_InitializeForm;
      }
      set
      {
        this.m_InitializeForm = value;
      }
    }

    public override void ConstructBodyElement()
    {
      try
      {
        this.index = 0;
        if (this.DataSetPane.DefaultSets != null && this.DataSetPane.DefaultSets.Count > 0)
          this.CreateDefaultSetArea();
        if (this.DataSetPane.PaneVariants == null || this.DataSetPane.PaneVariants.Count <= 0)
          return;
        this.CreateAdvancedDataSetPaneVariant();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of DataSetPane control failed.", ex));
      }
    }

    private void CreateDefaultSetArea()
    {
      if (this.advancedDataSetPaneVariant != null && this.BodyElementGrid.Children.Contains((UIElement) this.advancedDataSetPaneVariant))
        this.BodyElementGrid.Children.Remove((UIElement) this.advancedDataSetPaneVariant);
      else
        GridUtil.AddRowDef(this.BodyElementGrid, 40.0, GridUnitType.Star);
      this.defaultSetMapping = new VisualDefaultSet((IModelObject) this.DataSetPane, this.m_InitializeForm);
      if (this.DataSetPane.DefaultSets != null && this.DataSetPane.DefaultSets.Count > 0)
        this.m_DefaultSetMapping = this.DataSetPane.DefaultSets[0];
      this.defaultSetMapping.OnFFLinkClicked += new LinkClicked(this.OnFindFormLinkClicked);
      this.defaultSetMapping.OnDefaultSetChanged += new DefaultSetChanged(this.OnDefaultSetChanged);
      this.defaultSetMapping.OnAlternateDataSetVisualChanged += new AlternateDataSetVisualChanged(this.OnAlternateDataSetVisualChanged);
      this.defaultSetMapping.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.defaultSetMapping, this.index++, 0);
    }

    private void CreateAdvancedDataSetPaneVariant()
    {
      if (this.m_DefaultSetMapping == null && this.DataSetPane.DefaultSets != null && this.DataSetPane.DefaultSets.Count > 0)
        this.m_DefaultSetMapping = this.DataSetPane.DefaultSets[0];
      AdvancedDataSetPaneVariant dataSetPaneVariant = (AdvancedDataSetPaneVariant) null;
      FindFormPane ffp = (FindFormPane) null;
      AlternativeListVisualization alVisual = (AlternativeListVisualization) null;
      if (this.m_DefaultSetMapping != null)
      {
        foreach (AdvancedDataSetPaneVariant paneVariant in this.DataSetPane.PaneVariants)
        {
          if (paneVariant.Name == this.m_DefaultSetMapping.ListPaneVariantName)
          {
            dataSetPaneVariant = paneVariant;
            if (!string.IsNullOrEmpty(this.m_DefaultSetMapping.AlternateVisualName))
            {
              if (paneVariant.AlternateVisualizations != null)
              {
                if (paneVariant.AlternateVisualizations.Count > 0)
                {
                  using (List<AlternativeListVisualization>.Enumerator enumerator = paneVariant.AlternateVisualizations.GetEnumerator())
                  {
                    while (enumerator.MoveNext())
                    {
                      AlternativeListVisualization current = enumerator.Current;
                      if (current.Name == this.m_DefaultSetMapping.AlternateVisualName)
                        alVisual = current;
                    }
                    break;
                  }
                }
                else
                  break;
              }
              else
                break;
            }
            else
              break;
          }
        }
        if (this.DataSetPane.FindFormPanes != null)
        {
          foreach (FindFormPane findFormPane in this.DataSetPane.FindFormPanes)
          {
            if (findFormPane.Name == this.m_DefaultSetMapping.FindFormName)
            {
              ffp = findFormPane;
              break;
            }
          }
        }
      }
      else
      {
        dataSetPaneVariant = this.DataSetPane.PaneVariants[0];
        if (this.advancedDataSetPaneVariant != null && this.BodyElementGrid.Children.Contains((UIElement) this.advancedDataSetPaneVariant))
          this.BodyElementGrid.Children.Remove((UIElement) this.advancedDataSetPaneVariant);
        else
          GridUtil.AddRowDef(this.BodyElementGrid, 40.0, GridUnitType.Star);
        if (dataSetPaneVariant.AlternateVisualizations != null && dataSetPaneVariant.AlternateVisualizations.Count > 0)
        {
          this.defaultSetMapping = new VisualDefaultSet((IModelObject) this.DataSetPane, this.m_InitializeForm);
          this.defaultSetMapping.OnAlternateDataSetVisualChanged += new AlternateDataSetVisualChanged(this.OnAlternateDataSetVisualChanged);
          this.defaultSetMapping.Visibility = Visibility.Visible;
          this.defaultSetMapping.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.defaultSetMapping, this.index++, 0);
        }
      }
      if (this.advancedDataSetPaneVariant != null && this.BodyElementGrid.Children.Contains((UIElement) this.advancedDataSetPaneVariant))
      {
        this.advancedDataSetPaneVariant.ReloadAdvancedDataSetPaneVariant((IModelObject) dataSetPaneVariant, ffp, this.InitializeForm, alVisual, (IModelObject) this.DataSetPane);
      }
      else
      {
        GridUtil.AddRowDef(this.BodyElementGrid, 40.0, GridUnitType.Star);
        this.advancedDataSetPaneVariant = new VisualAdvancedDataSetPaneVariant((IModelObject) dataSetPaneVariant, ffp, this.InitializeForm, (IModelObject) this.DataSetPane, alVisual);
        this.advancedDataSetPaneVariant.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.advancedDataSetPaneVariant, this.index, 0);
      }
    }

    public override string SelectionText
    {
      get
      {
        return "AdvancedDataSetPane";
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (!(e.PropertyName == BasePane.PaneHeaderProperty) && !(e.PropertyName == AdvancedDataSetPane.DefaultSetMappingsProperty) && (!(e.PropertyName == AdvancedDataSetPane.FindFormPanesProperty) && !(e.PropertyName == AdvancedDataSetPane.AdvancedDataSetPaneVariantsProperty)))
        return;
      this.ReloadView();
    }

    private void OnFindFormLinkClicked(IDefaultSetMapping dsm, InitFormType initFormType, AlternativeListVisualization alVisual)
    {
      this.m_DefaultSetMapping = dsm;
      this.InitializeForm = initFormType;
      this.CreateAdvancedDataSetPaneVariant();
    }

    private void OnDefaultSetChanged(IDefaultSetMapping dsm)
    {
      if (this.m_DefaultSetMapping == null || dsm == null || !(this.m_DefaultSetMapping.ListPaneVariantName != dsm.ListPaneVariantName) && !(this.m_DefaultSetMapping.FindFormName != dsm.FindFormName) && !(this.m_DefaultSetMapping.AlternateVisualName != dsm.AlternateVisualName))
        return;
      this.m_DefaultSetMapping = dsm;
      this.CreateAdvancedDataSetPaneVariant();
    }

    private void OnAlternateDataSetVisualChanged(AdvancedDataSetPaneVariant variant, AlternativeListVisualization alVisual)
    {
      if (this.advancedDataSetPaneVariant == null || !this.BodyElementGrid.Children.Contains((UIElement) this.advancedDataSetPaneVariant))
        return;
      this.advancedDataSetPaneVariant.LoadAlternateVisual(alVisual);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (source.Header != (object) "Delete")
        return;
      this.DataSetPane.Parent.RemoveModelObject((IModelObject) this.DataSetPane);
    }
  }
}
