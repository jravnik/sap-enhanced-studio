﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualListPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualListPane : VisualBasePane
  {
    private const string ADD = "Add";
    private const string DELETE = "Delete";
    private const string COLUMN = "column";
    private int index;
    private int selectedDefaultSet;
    private AdvancedListPane ListPane;
    private VisualToolbar m_Toolbar;
    private VisualAdvancedListPaneVariant advancedListPaneVariant;
    private VisualDefaultSet defaultSetMapping;
    private IDefaultSetMapping m_DefaultSetMapping;
    private InitFormType m_InitializeForm;

    public VisualListPane(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.ListPane = model as AdvancedListPane;
      this.LoadView();
    }

    public IDefaultSetMapping SelectedDefaultSetMapping
    {
      get
      {
        return this.m_DefaultSetMapping;
      }
      set
      {
        this.m_DefaultSetMapping = value;
      }
    }

    public InitFormType InitializeForm
    {
      get
      {
        return this.m_InitializeForm;
      }
      set
      {
        this.m_InitializeForm = value;
      }
    }

    public override void ConstructBodyElement()
    {
      try
      {
        this.index = 0;
        if (this.ListPane.UseIntegratedToolbar)
        {
          this.m_Toolbar = this.CreateToolbarControl(this.GetSelectedAdvancedListPaneVariant());
          if (this.ListPane.PaneHeader != null && !string.IsNullOrEmpty(this.ListPane.PaneHeader.Text))
            GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 1);
          else
            GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 0);
        }
        if (this.ListPane.DefaultSets != null && this.ListPane.DefaultSets.Count > 0)
          this.CreateDefaultSetArea();
        if (this.ListPane.PaneVariants == null || this.ListPane.PaneVariants.Count <= 0)
          return;
        this.CreateAdvancedListPaneVariant((AlternativeListVisualization) null);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of ListPane control failed.", ex));
      }
    }

    private void CreateDefaultSetArea()
    {
      if (this.advancedListPaneVariant != null && this.BodyElementGrid.Children.Contains((UIElement) this.advancedListPaneVariant))
        this.BodyElementGrid.Children.Remove((UIElement) this.advancedListPaneVariant);
      else
        GridUtil.AddRowDef(this.BodyElementGrid, 40.0, GridUnitType.Star);
      this.defaultSetMapping = new VisualDefaultSet((IModelObject) this.ListPane, this.m_InitializeForm);
      if (this.ListPane.DefaultSets != null && this.ListPane.DefaultSets.Count > 0)
        this.m_DefaultSetMapping = this.ListPane.DefaultSets[0];
      this.defaultSetMapping.OnFFLinkClicked += new LinkClicked(this.OnFindFormLinkClicked);
      this.defaultSetMapping.OnDefaultSetChanged += new DefaultSetChanged(this.OnDefaultSetChanged);
      this.defaultSetMapping.OnAlternateListVisualChanged += new AlternateListVisualChanged(this.OnAlternateVisualChanged);
      this.defaultSetMapping.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.defaultSetMapping, this.index++, 0);
    }

    private AdvancedListPaneVariant GetSelectedAdvancedListPaneVariant()
    {
      if (this.m_DefaultSetMapping == null && this.ListPane.DefaultSets != null && this.ListPane.DefaultSets.Count > 0)
        this.m_DefaultSetMapping = this.ListPane.DefaultSets[0];
      AdvancedListPaneVariant advancedListPaneVariant = (AdvancedListPaneVariant) null;
      if (this.m_DefaultSetMapping != null)
      {
        foreach (AdvancedListPaneVariant paneVariant in this.ListPane.PaneVariants)
        {
          if (paneVariant.Name == this.m_DefaultSetMapping.ListPaneVariantName)
          {
            advancedListPaneVariant = paneVariant;
            if (!string.IsNullOrEmpty(this.m_DefaultSetMapping.AlternateVisualName))
            {
              if (paneVariant.AlternateListVisualization != null)
              {
                if (paneVariant.AlternateListVisualization.Count > 0)
                {
                  using (List<AlternativeListVisualization>.Enumerator enumerator = paneVariant.AlternateListVisualization.GetEnumerator())
                  {
                    while (enumerator.MoveNext())
                    {
                      int num = enumerator.Current.Name == this.m_DefaultSetMapping.AlternateVisualName ? 1 : 0;
                    }
                    break;
                  }
                }
                else
                  break;
              }
              else
                break;
            }
            else
              break;
          }
        }
        if (this.ListPane.FindFormPanes != null)
        {
          foreach (ExtensibleNamedModelObject findFormPane in this.ListPane.FindFormPanes)
          {
            if (findFormPane.Name == this.m_DefaultSetMapping.FindFormName)
              break;
          }
        }
      }
      else
        advancedListPaneVariant = this.ListPane.PaneVariants[0];
      return advancedListPaneVariant;
    }

    private void CreateAdvancedListPaneVariant(AlternativeListVisualization alVisual)
    {
      if (this.m_DefaultSetMapping == null && this.ListPane.DefaultSets != null && this.ListPane.DefaultSets.Count > 0)
        this.m_DefaultSetMapping = this.ListPane.DefaultSets[0];
      AdvancedListPaneVariant advancedListPaneVariant = (AdvancedListPaneVariant) null;
      FindFormPane ffp = (FindFormPane) null;
      AlternativeListVisualization alVisual1 = (AlternativeListVisualization) null;
      if (this.m_DefaultSetMapping != null)
      {
        foreach (AdvancedListPaneVariant paneVariant in this.ListPane.PaneVariants)
        {
          if (paneVariant.Name == this.m_DefaultSetMapping.ListPaneVariantName)
          {
            advancedListPaneVariant = paneVariant;
            if (!string.IsNullOrEmpty(this.m_DefaultSetMapping.AlternateVisualName))
            {
              if (paneVariant.AlternateListVisualization != null)
              {
                if (paneVariant.AlternateListVisualization.Count > 0)
                {
                  using (List<AlternativeListVisualization>.Enumerator enumerator = paneVariant.AlternateListVisualization.GetEnumerator())
                  {
                    while (enumerator.MoveNext())
                    {
                      AlternativeListVisualization current = enumerator.Current;
                      if (current.Name == this.m_DefaultSetMapping.AlternateVisualName)
                        alVisual1 = current;
                    }
                    break;
                  }
                }
                else
                  break;
              }
              else
                break;
            }
            else
              break;
          }
        }
        if (this.ListPane.FindFormPanes != null)
        {
          foreach (FindFormPane findFormPane in this.ListPane.FindFormPanes)
          {
            if (findFormPane.Name == this.m_DefaultSetMapping.FindFormName)
            {
              ffp = findFormPane;
              break;
            }
          }
        }
      }
      else
      {
        advancedListPaneVariant = this.ListPane.PaneVariants[0];
        if (this.advancedListPaneVariant != null && this.BodyElementGrid.Children.Contains((UIElement) this.advancedListPaneVariant))
          this.BodyElementGrid.Children.Remove((UIElement) this.advancedListPaneVariant);
        else
          GridUtil.AddRowDef(this.BodyElementGrid, 40.0, GridUnitType.Star);
        if (advancedListPaneVariant.AlternateListVisualization != null && advancedListPaneVariant.AlternateListVisualization.Count > 0)
        {
          this.defaultSetMapping = new VisualDefaultSet((IModelObject) this.ListPane, this.m_InitializeForm);
          this.defaultSetMapping.OnAlternateListVisualChanged += new AlternateListVisualChanged(this.OnAlternateVisualChanged);
          this.defaultSetMapping.Visibility = Visibility.Visible;
          this.defaultSetMapping.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.defaultSetMapping, this.index++, 0);
        }
      }
      if (alVisual1 == null)
        alVisual1 = alVisual;
      if (this.advancedListPaneVariant != null && this.BodyElementGrid.Children.Contains((UIElement) this.advancedListPaneVariant))
      {
        this.advancedListPaneVariant.ReloadAdvancedListPaneVariant((IModelObject) advancedListPaneVariant, ffp, this.InitializeForm, alVisual1, (IModelObject) this.ListPane);
      }
      else
      {
        GridUtil.AddRowDef(this.BodyElementGrid, 40.0, GridUnitType.Star);
        this.advancedListPaneVariant = new VisualAdvancedListPaneVariant((IModelObject) advancedListPaneVariant, ffp, this.InitializeForm, (IModelObject) this.ListPane, alVisual1, this.m_Toolbar);
        this.advancedListPaneVariant.OnUseToolBarChanged += new UseToolBarChanged(this.OnUseToolBarChanged);
        this.advancedListPaneVariant.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.advancedListPaneVariant, this.index, 0);
      }
    }

    private VisualToolbar CreateToolbarControl(AdvancedListPaneVariant lpv)
    {
      VisualToolbar visualToolbar = new VisualToolbar((IModelObject) lpv.Toolbar);
      this.m_Toolbar = visualToolbar;
      visualToolbar.VerticalAlignment = VerticalAlignment.Top;
      visualToolbar.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      visualToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      this.HeaderGrid.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      return visualToolbar;
    }

    public override string SelectionText
    {
      get
      {
        return "AdvancedListpane";
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == BasePane.PaneHeaderProperty || e.PropertyName == AdvancedListPane.DefaultSetsProperty || (e.PropertyName == AdvancedListPane.FindFormPanesProperty || e.PropertyName == AdvancedListPane.AdvancedListPaneVariantsProperty) || e.PropertyName == AdvancedListPane.UseIntegratedToolbarProperty)
        this.ReloadView();
      if (!this.ListPane.UseIntegratedToolbar)
        return;
      if (this.HeaderGrid.Children.Contains((UIElement) this.m_Toolbar))
        this.HeaderGrid.Children.Remove((UIElement) this.m_Toolbar);
      AdvancedListPaneVariant advancedListPaneVariant = this.GetSelectedAdvancedListPaneVariant();
      if (advancedListPaneVariant.Toolbar == null)
        return;
      this.m_Toolbar = this.CreateToolbarControl(advancedListPaneVariant);
      this.advancedListPaneVariant.IntegratedToolbar = this.m_Toolbar;
      if (this.ListPane.PaneHeader != null && !string.IsNullOrEmpty(this.ListPane.PaneHeader.Text))
        GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 1);
      else
        GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 0);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      if (!(e.RemovedModel is IDefaultSetMapping))
        return;
      this.ReloadView();
    }

    private void OnUseToolBarChanged(AdvancedListPaneVariant listPaneVariant)
    {
      if (!this.ListPane.UseIntegratedToolbar)
        return;
      if (this.HeaderGrid.Children.Contains((UIElement) this.m_Toolbar))
        this.HeaderGrid.Children.Remove((UIElement) this.m_Toolbar);
      AdvancedListPaneVariant advancedListPaneVariant = this.GetSelectedAdvancedListPaneVariant();
      if (advancedListPaneVariant.Toolbar == null)
        return;
      this.m_Toolbar = this.CreateToolbarControl(advancedListPaneVariant);
      this.advancedListPaneVariant.IntegratedToolbar = this.m_Toolbar;
      if (this.ListPane.PaneHeader != null && !string.IsNullOrEmpty(this.ListPane.PaneHeader.Text))
        GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 1);
      else
        GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 0);
    }

    private void OnFindFormLinkClicked(IDefaultSetMapping dsm, InitFormType initFormType, AlternativeListVisualization alVisual)
    {
      this.m_DefaultSetMapping = dsm;
      this.InitializeForm = initFormType;
      if (this.ListPane.UseIntegratedToolbar)
      {
        if (this.HeaderGrid.Children.Contains((UIElement) this.m_Toolbar))
          this.HeaderGrid.Children.Remove((UIElement) this.m_Toolbar);
        AdvancedListPaneVariant advancedListPaneVariant = this.GetSelectedAdvancedListPaneVariant();
        if (advancedListPaneVariant.Toolbar != null)
        {
          this.m_Toolbar = this.CreateToolbarControl(advancedListPaneVariant);
          this.advancedListPaneVariant.IntegratedToolbar = this.m_Toolbar;
          if (this.ListPane.PaneHeader != null && !string.IsNullOrEmpty(this.ListPane.PaneHeader.Text))
            GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 1);
          else
            GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 0);
        }
      }
      this.CreateAdvancedListPaneVariant(alVisual);
    }

    private void OnDefaultSetChanged(IDefaultSetMapping dsm)
    {
      if (this.m_DefaultSetMapping == null || dsm == null || !(this.m_DefaultSetMapping.ListPaneVariantName != dsm.ListPaneVariantName) && !(this.m_DefaultSetMapping.FindFormName != dsm.FindFormName) && !(this.m_DefaultSetMapping.AlternateVisualName != dsm.AlternateVisualName))
        return;
      this.m_DefaultSetMapping = dsm;
      if (this.ListPane.UseIntegratedToolbar)
      {
        if (this.HeaderGrid.Children.Contains((UIElement) this.m_Toolbar))
          this.HeaderGrid.Children.Remove((UIElement) this.m_Toolbar);
        AdvancedListPaneVariant advancedListPaneVariant = this.GetSelectedAdvancedListPaneVariant();
        if (advancedListPaneVariant.Toolbar != null)
        {
          this.m_Toolbar = this.CreateToolbarControl(advancedListPaneVariant);
          this.advancedListPaneVariant.IntegratedToolbar = this.m_Toolbar;
          if (this.ListPane.PaneHeader != null && !string.IsNullOrEmpty(this.ListPane.PaneHeader.Text))
            GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 1);
          else
            GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 0);
        }
      }
      this.CreateAdvancedListPaneVariant((AlternativeListVisualization) null);
    }

    private void OnAlternateVisualChanged(AdvancedListPaneVariant variant, AlternativeListVisualization alVisual)
    {
      if (this.advancedListPaneVariant == null || !this.BodyElementGrid.Children.Contains((UIElement) this.advancedListPaneVariant))
        return;
      if (this.ListPane.UseIntegratedToolbar)
      {
        if (this.HeaderGrid.Children.Contains((UIElement) this.m_Toolbar))
          this.HeaderGrid.Children.Remove((UIElement) this.m_Toolbar);
        AdvancedListPaneVariant advancedListPaneVariant = this.GetSelectedAdvancedListPaneVariant();
        if (advancedListPaneVariant.Toolbar != null)
        {
          this.m_Toolbar = this.CreateToolbarControl(advancedListPaneVariant);
          this.advancedListPaneVariant.IntegratedToolbar = this.m_Toolbar;
          if (this.ListPane.PaneHeader != null && !string.IsNullOrEmpty(this.ListPane.PaneHeader.Text))
            GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 1);
          else
            GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.m_Toolbar, 0, 0);
        }
      }
      this.advancedListPaneVariant.LoadAlternateVisual(alVisual);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      if (source.Header != (object) "Delete")
        return;
      foreach (AdvancedListPaneVariant paneVariant in this.ListPane.PaneVariants)
      {
        if (ValidationManager.GetInstance().IsDeletable((IExtensible) paneVariant.ListDefinition))
        {
          this.ListPane.Parent.RemoveModelObject((IModelObject) this.ListPane);
        }
        else
        {
          int num = (int) DisplayMessage.Show(Resource.ElementNotDeletable, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
          break;
        }
      }
    }
  }
}
