﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualGanttChartPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.GanttChartPaneControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualGanttChartPane : VisualBasePane, IContextMenuContainer
  {
    private VisualToolbar ganttChartToolbar;
    private VisualGanttChartItem ganttChartItem;
    private Grid selectionVarGrid;
    private TextBlock viewLabel;
    private ComboBox m_ShowComboBox;

    public event ListVariantChanged OnListVariantChanged;

    private GanttChartPane GanttChartPane
    {
      get
      {
        return this.m_ModelObject as GanttChartPane;
      }
    }

    public VisualGanttChartPane(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "GanttChartPane";
      }
    }

    public override void ConstructBodyElement()
    {
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      if (this.GanttChartPane.ItemDefinition != null && this.GanttChartPane.ItemDefinition.ListVariant != null && this.GanttChartPane.ItemDefinition.ListVariant.Count > 1)
      {
        this.CreateSelectionListVariantArea();
        if (this.GanttChartPane.GanttChartPaneToolbar != null)
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.selectionVarGrid, 0, 0);
        else
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.selectionVarGrid, 0, 0, 1, 2);
      }
      if (this.GanttChartPane.GanttChartPaneToolbar != null)
      {
        this.ganttChartToolbar = new VisualToolbar((IModelObject) this.GanttChartPane.GanttChartPaneToolbar);
        this.ganttChartToolbar.VerticalAlignment = VerticalAlignment.Top;
        this.ganttChartToolbar.HorizontalAlignment = HorizontalAlignment.Stretch;
        this.ganttChartToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        this.ganttChartToolbar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
        if (this.GanttChartPane.ItemDefinition != null && this.GanttChartPane.ItemDefinition.ListVariant != null && this.GanttChartPane.ItemDefinition.ListVariant.Count > 1)
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.ganttChartToolbar, 0, 1);
        else
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.ganttChartToolbar, 0, 0, 1, 2);
      }
      else
        this.ganttChartToolbar = (VisualToolbar) null;
      if (this.GanttChartPane.ItemDefinition == null)
        return;
      this.DisposeEvent();
      this.ganttChartItem = new VisualGanttChartItem((IModelObject) this.GanttChartPane.ItemDefinition, (BaseVisualControl) this);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.ganttChartItem, 1, 0, 1, 2);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        if (e.PropertyName == GanttChartPane.GanttChartPaneToolbarProperty)
        {
          this.ganttChartToolbar.Toolbar = (Toolbar) this.GanttChartPane.GanttChartPaneToolbar;
          this.ganttChartToolbar.ReloadView();
        }
        else
          this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of GanttChart Pane failed.", ex));
      }
      base.OnModelPropertyChanged(sender, e);
    }

    protected override void AddContextMenuItems()
    {
      if (this.ganttChartToolbar != null)
        this.ganttChartToolbar.AddContextMenuItems((IContextMenuContainer) this);
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (source.Header == (object) "Delete")
      {
        if (this.GanttChartPane == null)
          return;
        PaneContainerVariant parentOfType1 = Utilities.GetParentOfType(this.Model, typeof (PaneContainerVariant)) as PaneContainerVariant;
        if (parentOfType1 == null)
        {
          PaneContainer parentOfType2 = Utilities.GetParentOfType(this.Model, typeof (PaneContainer)) as PaneContainer;
          if (parentOfType2 == null)
            return;
          parentOfType2.RemoveModelObject((IModelObject) this.GanttChartPane);
        }
        else
          parentOfType1.RemoveModelObject((IModelObject) this.GanttChartPane);
      }
      else
      {
        if (this.ganttChartToolbar == null)
          return;
        this.ganttChartToolbar.HandleContextMenuItemClickWrapper(source);
      }
    }

    private void CreateSelectionListVariantArea()
    {
      this.selectionVarGrid = new Grid();
      this.selectionVarGrid.Background = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
      GridUtil.AddColumnDef(this.selectionVarGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.selectionVarGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.selectionVarGrid, 1.0, GridUnitType.Auto);
      this.viewLabel = new TextBlock();
      this.viewLabel.HorizontalAlignment = HorizontalAlignment.Left;
      this.viewLabel.VerticalAlignment = VerticalAlignment.Center;
      this.viewLabel.Text = this.GanttChartPane.ListVariantLabel != null ? this.GanttChartPane.ListVariantLabel.Text : "View";
      this.viewLabel.Margin = new Thickness(10.0, 5.0, 0.0, 5.0);
      FontManager.Instance.SetFontStyle1("Arial,11,Bold", this.viewLabel);
      this.viewLabel.Foreground = (Brush) SkinConstants.BRUSH_TEXTVIEW_SECTIONGROUPTITLE;
      GridUtil.PlaceElement(this.selectionVarGrid, (UIElement) this.viewLabel, 0, 0);
      this.m_ShowComboBox = new ComboBox();
      this.LoadVariantNames();
      this.m_ShowComboBox.SelectionChanged += new SelectionChangedEventHandler(this.OnSelectionChanged);
      this.m_ShowComboBox.HorizontalAlignment = HorizontalAlignment.Left;
      this.m_ShowComboBox.VerticalAlignment = VerticalAlignment.Center;
      this.m_ShowComboBox.Margin = new Thickness(5.0, 5.0, 5.0, 5.0);
      this.m_ShowComboBox.Width = 150.0;
      GridUtil.PlaceElement(this.selectionVarGrid, (UIElement) this.m_ShowComboBox, 0, 1);
    }

    private void LoadVariantNames()
    {
      this.m_ShowComboBox.Items.Clear();
      int count = this.GanttChartPane.ItemDefinition.ListVariant.Count;
      foreach (GanttChartItemListVariant chartItemListVariant in this.GanttChartPane.ItemDefinition.ListVariant)
      {
        if (!string.IsNullOrEmpty(chartItemListVariant.Name))
          this.m_ShowComboBox.Items.Add((object) chartItemListVariant.Name);
      }
      this.m_ShowComboBox.Text = this.GanttChartPane.ItemDefinition.ListVariant[count - 1].Name;
    }

    private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.OnListVariantChanged == null || this.m_ShowComboBox.SelectedIndex == -1 || this.GanttChartPane.ItemDefinition.ListVariant.Count <= this.m_ShowComboBox.SelectedIndex)
        return;
      this.OnListVariantChanged(this.GanttChartPane.ItemDefinition.ListVariant[this.m_ShowComboBox.SelectedIndex]);
    }

    public void DisposeEvent()
    {
      if (this.OnListVariantChanged == null)
        return;
      this.OnListVariantChanged = (ListVariantChanged) null;
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
