﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.HGraphPaneControls.VisualTemplateArea
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.HGraphPaneControls
{
  public class VisualTemplateArea : BaseSelectableControl
  {
    private const string ADDCOMMAND = "Add";
    private const string TEMPLATES = "TemplateType";

    internal TemplateArea TemplateArea
    {
      get
      {
        return this.m_ModelObject as TemplateArea;
      }
    }

    public VisualTemplateArea(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      TextBlock textBlock = new TextBlock();
      textBlock.Margin = new Thickness(4.0, 4.0, 30.0, 4.0);
      textBlock.Text = "Template Area";
      GridUtil.PlaceElement(this.Grid, (UIElement) textBlock, 0, 0);
      if (this.TemplateArea.TemplateBinding == null)
        return;
      VisualTemplate visualTemplate = new VisualTemplate((IModelObject) this.TemplateArea.TemplateBinding);
      visualTemplate.Margin = new Thickness(10.0, 10.0, 20.0, 10.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) visualTemplate, 1, 0);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        if (e.RemovedModel is Template)
          this.TemplateArea.TemplateBinding = (Template) null;
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of HierarchicalGraph failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("TemplateType", this.AddContextMenuItem("Add"));
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header.Equals((object) "TemplateType"))
      {
        this.AddTemplateInTemplateArea();
      }
      else
      {
        if (!source.Header.Equals((object) "Delete"))
          return;
        (this.TemplateArea.Parent as HierarchicalGraphPane).RemoveTemplateAreaSection(this.TemplateArea);
      }
    }

    public override string SelectionText
    {
      get
      {
        return "TemplateArea";
      }
    }

    public void AddTemplateInTemplateArea()
    {
      try
      {
        this.TemplateArea.AddTemplate();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Adding Template to the TemplateArea failed", ex));
      }
    }
  }
}
