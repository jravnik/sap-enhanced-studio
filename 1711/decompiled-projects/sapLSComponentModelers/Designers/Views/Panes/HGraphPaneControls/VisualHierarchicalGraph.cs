﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.HGraphPaneControls.VisualHierarchicalGraph
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.HGraphPaneControls
{
  public class VisualHierarchicalGraph : BaseSelectableControl
  {
    private const string ADDCOMMAND = "Add";
    private const string NODES = "NodeType";
    private List<NodeConfiguration> m_NodesConfig;
    private TextBlock hGraphTitle;

    internal HierarchicalGraph HierarchicalGraph
    {
      get
      {
        return this.m_ModelObject as HierarchicalGraph;
      }
    }

    public VisualHierarchicalGraph(IModelObject modelObj)
      : base(modelObj)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      this.hGraphTitle = new TextBlock();
      this.hGraphTitle.Margin = new Thickness(30.0, 4.0, 4.0, 4.0);
      this.hGraphTitle.Text = "Content Area";
      WrapPanel wrapPanel = new WrapPanel();
      wrapPanel.Orientation = Orientation.Horizontal;
      GridUtil.PlaceElement(this.Grid, (UIElement) this.hGraphTitle, 0, 0);
      this.m_NodesConfig = this.HierarchicalGraph.NodeConfiguration;
      if (this.m_NodesConfig == null || this.m_NodesConfig.Count <= 0)
        return;
      for (int index = 0; index < this.m_NodesConfig.Count; ++index)
      {
        VisualNodeConfiguration nodeConfiguration = new VisualNodeConfiguration((IModelObject) this.m_NodesConfig[index]);
        nodeConfiguration.Margin = new Thickness(20.0, 10.0, 10.0, 10.0);
        wrapPanel.Children.Add((UIElement) nodeConfiguration);
      }
      GridUtil.PlaceElement(this.Grid, (UIElement) wrapPanel, 1, 0);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is NodeConfiguration)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of HierarchicalGraph failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("NodeType", this.AddContextMenuItem("Add"));
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) "NodeType")
        return;
      this.AddNodeConfiguration();
    }

    public override string SelectionText
    {
      get
      {
        return "HierarchicalGraph";
      }
    }

    public void AddNodeConfiguration()
    {
      try
      {
        this.HierarchicalGraph.AddNodeToHierarchicalGraph();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Adding Nodes to the HierarchicalGraph failed", ex));
      }
    }
  }
}
