﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualCalendarPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  internal class VisualCalendarPane : VisualBasePane, IContextMenuContainer
  {
    private CalendarPane m_CalendarPane;
    private CalendarToolBar m_CalendarToolBar;
    private VisualCalendarViewContainer m_VisualCalendarViewContainer;
    private VisualLegend m_VisualLegend;
    private int m_RowIndex;

    public VisualCalendarPane(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_CalendarPane = this.m_ModelObject as CalendarPane;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "CalendarPane";
      }
    }

    protected CalendarPane CalendarPane
    {
      get
      {
        return this.m_ModelObject as CalendarPane;
      }
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        if (this.m_CalendarToolBar != null)
          this.m_CalendarToolBar.AddContextMenuItemsWrapper();
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for VisualForm.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      try
      {
        if (source.Header != null && source.Header == (object) "Delete")
        {
          if (this.CalendarPane.Parent == null)
            return;
          this.CalendarPane.Parent.RemoveModelObject((IModelObject) this.CalendarPane);
        }
        else
        {
          if (this.m_CalendarToolBar == null)
            return;
          this.m_CalendarToolBar.HandleContextMenuItemWrapper(source);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == "HasDayView")
      {
        if (this.CalendarPane.HasDayView)
        {
          if (this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Day"))
            return;
          this.m_CalendarToolBar.ViewSwitchItems.Add((object) "Day");
        }
        else
        {
          this.SetDefaultView();
          if (!this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Day"))
            return;
          this.m_CalendarToolBar.ViewSwitchItems.Remove((object) "Day");
        }
      }
      else if (e.PropertyName == "HasWeekView")
      {
        if (this.CalendarPane.HasWeekView)
        {
          if (this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Week"))
            return;
          this.m_CalendarToolBar.ViewSwitchItems.Add((object) "Week");
        }
        else
        {
          this.SetDefaultView();
          if (!this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Week"))
            return;
          this.m_CalendarToolBar.ViewSwitchItems.Remove((object) "Week");
        }
      }
      else if (e.PropertyName == "HasMonthView")
      {
        if (this.CalendarPane.HasMonthView)
        {
          if (this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Month"))
            return;
          this.m_CalendarToolBar.ViewSwitchItems.Add((object) "Month");
        }
        else
        {
          this.SetDefaultView();
          if (!this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Month"))
            return;
          this.m_CalendarToolBar.ViewSwitchItems.Remove((object) "Month");
        }
      }
      else if (e.PropertyName == "HasYearView")
      {
        if (this.CalendarPane.HasYearView)
        {
          if (this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Year"))
            return;
          this.m_CalendarToolBar.ViewSwitchItems.Add((object) "Year");
        }
        else
        {
          this.SetDefaultView();
          if (!this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Year"))
            return;
          this.m_CalendarToolBar.ViewSwitchItems.Remove((object) "Year");
        }
      }
      else if (e.PropertyName == "HasToolBar")
      {
        if (this.CalendarPane.HasToolBar)
        {
          this.m_CalendarToolBar.CustomToolBar = this.CalendarPane.ToolBar;
        }
        else
        {
          if (!this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Year"))
            return;
          this.m_CalendarToolBar.ViewSwitchItems.Remove((object) "Year");
        }
      }
      else
      {
        if (!(e.PropertyName == "ToolBar"))
          return;
        this.m_CalendarToolBar.CustomToolBar = e.NewValue as CalendarPaneToolbar;
      }
    }

    public override void ConstructBodyElement()
    {
      try
      {
        this.BodyElementGrid.Margin = ComponentModelersConstants.CALENDAR_CONTROL_BORDER_MARGIN;
        if (this.CalendarPane == null)
          return;
        if (this.CalendarPane.ToolBar != null)
          this.AddToolBar((Toolbar) this.CalendarPane.ToolBar);
        this.InitializeCalendarToolBar();
        if (this.CalendarPane.CalendarViewContainer != null)
          this.AddCalendarView(this.CalendarPane.CalendarViewContainer);
        if (this.CalendarPane.Calendarlegend == null)
          return;
        this.AddCalendarLegend(this.CalendarPane.Calendarlegend);
      }
      catch
      {
      }
    }

    private void InitializeCalendarToolBar()
    {
      if (this.CalendarPane.HasDayView && !this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Day"))
        this.m_CalendarToolBar.ViewSwitchItems.Add((object) "Day");
      if (this.CalendarPane.HasWeekView && !this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Week"))
        this.m_CalendarToolBar.ViewSwitchItems.Add((object) "Week");
      if (this.CalendarPane.HasYearView && !this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Year"))
        this.m_CalendarToolBar.ViewSwitchItems.Add((object) "Year");
      if (!this.CalendarPane.HasWeekView || this.m_CalendarToolBar.ViewSwitchItems.Contains((object) "Month"))
        return;
      this.m_CalendarToolBar.ViewSwitchItems.Add((object) "Month");
    }

    private void AddToolBar(Toolbar toolBar)
    {
      if (toolBar == null)
        return;
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      this.m_CalendarToolBar = new CalendarToolBar((IModelObject) toolBar);
      this.m_CalendarToolBar.ViewItemSelectionChanged += new SelectionChangedEventHandler(this.CalendarToolBar_ViewItemSelectionChanged);
      this.m_CalendarToolBar.VerticalAlignment = VerticalAlignment.Top;
      this.m_CalendarToolBar.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.m_CalendarToolBar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      this.m_CalendarToolBar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.m_CalendarToolBar, this.m_RowIndex++, 0);
    }

    private void CalendarToolBar_ViewItemSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.m_CalendarToolBar.ViewSwitchSelectedItem.ToString() == "Day")
        this.m_VisualCalendarViewContainer.CurrentViewType = CalendarViewSelectionType.CalendarDayView;
      else if (this.m_CalendarToolBar.ViewSwitchSelectedItem.ToString() == "Week")
        this.m_VisualCalendarViewContainer.CurrentViewType = CalendarViewSelectionType.CalendarWeekView;
      else if (this.m_CalendarToolBar.ViewSwitchSelectedItem.ToString() == "Month")
      {
        this.m_VisualCalendarViewContainer.CurrentViewType = CalendarViewSelectionType.CalendarMonthView;
      }
      else
      {
        if (!(this.m_CalendarToolBar.ViewSwitchSelectedItem.ToString() == "Year"))
          return;
        this.m_VisualCalendarViewContainer.CurrentViewType = CalendarViewSelectionType.CalendarYearView;
      }
    }

    private void AddCalendarView(CalendarView calendarView)
    {
      if (calendarView == null)
        return;
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      this.m_VisualCalendarViewContainer = new VisualCalendarViewContainer((IModelObject) calendarView);
      this.m_VisualCalendarViewContainer.VerticalAlignment = VerticalAlignment.Top;
      this.m_VisualCalendarViewContainer.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.m_VisualCalendarViewContainer.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.m_VisualCalendarViewContainer, this.m_RowIndex++, 0);
      if (this.m_CalendarToolBar == null || this.m_CalendarToolBar.ViewSwitchItems == null || this.m_CalendarToolBar.ViewSwitchItems.Count <= 0)
        return;
      this.m_CalendarToolBar.ViewSwitchSelectedItem = this.m_CalendarToolBar.ViewSwitchItems[0];
    }

    private void AddCalendarLegend(Legend calendarLegend)
    {
      if (calendarLegend == null)
        return;
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      this.m_VisualLegend = new VisualLegend((IModelObject) calendarLegend);
      this.m_VisualLegend.VerticalAlignment = VerticalAlignment.Top;
      this.m_VisualLegend.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.m_VisualLegend.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.m_VisualLegend, this.m_RowIndex++, 0);
    }

    private void SetDefaultView()
    {
      if (this.CalendarPane.HasDayView)
      {
        this.m_VisualCalendarViewContainer.CurrentViewType = CalendarViewSelectionType.CalendarDayView;
        this.m_CalendarToolBar.ViewSwitchSelectedItem = (object) "Day";
      }
      else if (this.CalendarPane.HasWeekView)
      {
        this.m_VisualCalendarViewContainer.CurrentViewType = CalendarViewSelectionType.CalendarWeekView;
        this.m_CalendarToolBar.ViewSwitchSelectedItem = (object) "Week";
      }
      else if (this.CalendarPane.HasYearView)
      {
        this.m_VisualCalendarViewContainer.CurrentViewType = CalendarViewSelectionType.CalendarYearView;
        this.m_CalendarToolBar.ViewSwitchSelectedItem = (object) "Year";
      }
      else
      {
        if (!this.CalendarPane.HasMonthView)
          return;
        this.m_VisualCalendarViewContainer.CurrentViewType = CalendarViewSelectionType.CalendarMonthView;
        this.m_CalendarToolBar.ViewSwitchSelectedItem = (object) "Month";
      }
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
