﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ServiceMapPaneControls.VisualServiceMapGroup
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ServiceMapPaneControls
{
  public class VisualServiceMapGroup : BaseSelectableControl
  {
    private const string DEFAULT_MORE_LINK_TEXT = "More";
    private DisplayTextControl displayTextControl;
    private TextBlock explanationText;
    private BaseVisualControl parentVisualControl;

    internal ServiceMapGroup ServiceMapgroup
    {
      get
      {
        return this.m_ModelObject as ServiceMapGroup;
      }
    }

    public VisualServiceMapGroup(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.parentVisualControl = parentControl;
      this.IsDraggable = true;
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.Grid.Margin = new Thickness(0.0, 0.0, 0.0, 15.0);
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) new System.Windows.Controls.Image()
        {
          Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(Resource.Image)
        }, 0, 0);
        int num1 = 0;
        string text = string.Empty;
        if (this.ServiceMapgroup.Title != null)
          text = this.ServiceMapgroup.Title.Text;
        this.displayTextControl = new DisplayTextControl(TextViewStyles.SectionGroupTitle, text);
        Grid grid1 = this.Grid;
        DisplayTextControl displayTextControl = this.displayTextControl;
        int row1 = num1;
        int num2 = 1;
        int num3 = row1 + num2;
        int col1 = 1;
        GridUtil.PlaceElement(grid1, (UIElement) displayTextControl, row1, col1);
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        this.explanationText = new TextBlock();
        this.explanationText.TextWrapping = TextWrapping.Wrap;
        this.explanationText.Background = (Brush) ComponentModelersConstants.CONTENT_AREA_BRUSH;
        if (this.ServiceMapgroup.Explanation != null)
          this.explanationText.Text = this.ServiceMapgroup.Explanation.Text;
        Grid grid2 = this.Grid;
        TextBlock explanationText = this.explanationText;
        int row2 = num3;
        int num4 = 1;
        int num5 = row2 + num4;
        int col2 = 1;
        GridUtil.PlaceElement(grid2, (UIElement) explanationText, row2, col2);
        foreach (Link link in this.ServiceMapgroup.Links)
        {
          GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
          int col3 = 1;
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualServiceLink(link, this), num5++, col3);
        }
        this.Grid.ColumnDefinitions[0].SetValue(Grid.ColumnSpanProperty, (object) (num5 + 1));
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of ServiceMapGroup failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "ServiceMapGroup";
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == ServiceMapGroup.ExplanationProperty)
        {
          if (this.ServiceMapgroup.Explanation == null)
            return;
          this.explanationText.Text = this.ServiceMapgroup.Explanation.Text;
        }
        else if (e.PropertyName == ServiceMapGroup.TitleProperty)
        {
          if (this.ServiceMapgroup.Title == null)
            return;
          this.displayTextControl.Text = this.ServiceMapgroup.Title.Text;
        }
        else
        {
          if (!(e.PropertyName == ServiceMapGroup.LinksProperty))
            return;
          this.ReloadView();
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update the property changes in the model.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == null || !(source.Header.ToString() == "Delete") || this.ServiceMapgroup.Parent == null)
        return;
      this.ServiceMapgroup.Parent.RemoveModelObject((IModelObject) this.ServiceMapgroup);
    }

    protected override void OnToolBoxControlDragOver(DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      base.OnToolBoxControlDragOver(e, toolboxEventArg);
      if (toolboxEventArg.Tag.ClassName == "SAP.BYD.LS.UIDesigner.Model.Entities.Controls.AbstractControl" && toolboxEventArg.Tag.ControlName == "Link")
        e.Effects = DragDropEffects.Copy;
      else
        e.Effects = DragDropEffects.None;
    }
  }
}
