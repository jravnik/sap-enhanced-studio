﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualLayoutContainerPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualLayoutContainerPane : VisualBasePane
  {
    internal LayoutContainerPane LayoutContainerPane
    {
      get
      {
        return this.m_ModelObject as LayoutContainerPane;
      }
    }

    public VisualLayoutContainerPane(IModelObject model)
      : base(model)
    {
      this.LoadView();
    }

    public override void ConstructBodyElement()
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      if (this.LayoutContainerPane.Item == null)
        return;
      if (this.LayoutContainerPane.Item is LayoutToolbar)
        SAP.BYD.LS.UI.Foundation.GridUtil.AddRowDef(this.BodyElementGrid, 30.0, 30.0, GridUnitType.Auto);
      else
        SAP.BYD.LS.UI.Foundation.GridUtil.AddRowDef(this.BodyElementGrid, 150.0, 150.0, GridUnitType.Star);
      SAP.BYD.LS.UI.Foundation.GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      SAP.BYD.LS.UI.Foundation.GridUtil.PlaceElement(this.BodyElementGrid, ControlFactory.ConstructLayoutElementControl(this.LayoutContainerPane.Item), 0, 0);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      base.OnModelObjectAdded(sender, e);
      if (this.LayoutContainerPane.Item == null)
        return;
      if (this.LayoutContainerPane.Item is LayoutToolbar)
        SAP.BYD.LS.UI.Foundation.GridUtil.AddRowDef(this.BodyElementGrid, 30.0, 30.0, GridUnitType.Auto);
      else
        SAP.BYD.LS.UI.Foundation.GridUtil.AddRowDef(this.BodyElementGrid, 150.0, 150.0, GridUnitType.Star);
      SAP.BYD.LS.UI.Foundation.GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      SAP.BYD.LS.UI.Foundation.GridUtil.PlaceElement(this.BodyElementGrid, ControlFactory.ConstructLayoutElementControl(this.LayoutContainerPane.Item), 0, 0);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      this.ReloadView();
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Paste");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for VisualLayoutContainerPane.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      try
      {
        if (source.Header != null && source.Header == (object) "Delete")
        {
          if (this.LayoutContainerPane.Parent == null)
            return;
          this.LayoutContainerPane.Parent.RemoveModelObject((IModelObject) this.LayoutContainerPane);
        }
        else
        {
          if (source.Header != (object) "Paste")
            return;
          ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyPasteOperation();
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Layout Container Pane";
      }
    }
  }
}
