﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualBACPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualBACPane : VisualBasePane
  {
    private System.Windows.Controls.Image m_ArrowRight;
    private System.Windows.Controls.Image m_ArrowDoubleRight;
    private System.Windows.Controls.Image m_ArrowLeft;
    private System.Windows.Controls.Image m_ArrowDoubleLeft;
    private System.Windows.Controls.Image m_ArrowDoubleUp;
    private System.Windows.Controls.Image m_ArrowUp;
    private System.Windows.Controls.Image m_ArrowDoubleDown;
    private System.Windows.Controls.Image m_ArrowDown;
    private Grid toolbarVerticalGrid;
    private Grid toolbarHorizontalGrid;
    private BrowseAndCollectPane bacPane;
    private VisualListPane visualBrowsePane;
    private VisualListPane visualCollectPane;
    private Grid horizontalGrid;
    private Grid verticalGrid;

    public VisualBACPane(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.bacPane = model as BrowseAndCollectPane;
      this.LoadView();
    }

    public override void ConstructBodyElement()
    {
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      this.visualBrowsePane = new VisualListPane((IModelObject) this.bacPane.BrowseList);
      this.visualBrowsePane.VerticalAlignment = VerticalAlignment.Top;
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.visualBrowsePane, 0, 0);
      this.visualCollectPane = new VisualListPane((IModelObject) this.bacPane.CollectList);
      this.visualCollectPane.VerticalAlignment = VerticalAlignment.Top;
      if (this.bacPane.BrowseAndCollectOrientation == OrientationType.horizontal)
      {
        GridUtil.AddColumnDef(this.BodyElementGrid, 40.0, GridUnitType.Pixel);
        this.toolbarVerticalGrid = new Grid();
        this.verticalGrid = new Grid();
        this.AddImagesInVerticalAllignment();
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.toolbarVerticalGrid, 0, 1);
        GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.visualCollectPane, 0, 2);
      }
      else
      {
        GridUtil.AddRowDef(this.BodyElementGrid, 40.0, GridUnitType.Pixel);
        this.toolbarHorizontalGrid = new Grid();
        this.horizontalGrid = new Grid();
        this.AddImagesInHorizontalAllignment();
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.toolbarHorizontalGrid, 1, 0);
        GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.visualCollectPane, 2, 0);
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Browse and Collect Pane";
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      if (e.PropertyName == BrowseAndCollectPane.BrowseAndCollectOrientationProperty)
      {
        this.ReloadView();
      }
      else
      {
        if (!(e.PropertyName == BrowseAndCollectPane.ShowAllSelectButtonsProperty))
          return;
        if (this.bacPane.BrowseAndCollectOrientation == OrientationType.horizontal)
          this.AddImagesInVerticalAllignment();
        else
          this.AddImagesInHorizontalAllignment();
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) "Delete")
        return;
      BrowseAndCollectPane parentOfType1 = Utilities.GetParentOfType(this.Model, typeof (BrowseAndCollectPane)) as BrowseAndCollectPane;
      if (parentOfType1 == null)
        return;
      PaneContainerVariant parentOfType2 = Utilities.GetParentOfType(this.Model, typeof (PaneContainerVariant)) as PaneContainerVariant;
      if (parentOfType2 == null)
      {
        PaneContainer parentOfType3 = Utilities.GetParentOfType(this.Model, typeof (PaneContainer)) as PaneContainer;
        if (parentOfType3 == null)
          return;
        parentOfType3.RemoveModelObject((IModelObject) parentOfType1);
      }
      else
        parentOfType2.RemoveModelObject((IModelObject) parentOfType1);
    }

    private void AddImagesInHorizontalAllignment()
    {
      this.toolbarHorizontalGrid.Children.Clear();
      this.toolbarHorizontalGrid.ColumnDefinitions.Clear();
      this.toolbarHorizontalGrid.RowDefinitions.Clear();
      this.horizontalGrid.Children.Clear();
      this.horizontalGrid.ColumnDefinitions.Clear();
      this.horizontalGrid.RowDefinitions.Clear();
      GridUtil.AddColumnDef(this.toolbarHorizontalGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.toolbarHorizontalGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.toolbarHorizontalGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.toolbarHorizontalGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.toolbarHorizontalGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.toolbarHorizontalGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.horizontalGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.horizontalGrid, 15.0, GridUnitType.Pixel);
      GridUtil.AddColumnDef(this.horizontalGrid, 15.0, GridUnitType.Pixel);
      GridUtil.AddColumnDef(this.horizontalGrid, 15.0, GridUnitType.Pixel);
      this.m_ArrowUp = new System.Windows.Controls.Image();
      this.m_ArrowUp.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.Up);
      this.m_ArrowUp.Stretch = Stretch.Fill;
      this.m_ArrowDown = new System.Windows.Controls.Image();
      this.m_ArrowDown.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.DOWNARROW);
      this.m_ArrowDown.Stretch = Stretch.Fill;
      if (this.bacPane.ShowAllSelectButtons)
      {
        GridUtil.AddColumnDef(this.horizontalGrid, 15.0, GridUnitType.Pixel);
        GridUtil.AddColumnDef(this.horizontalGrid, 15.0, GridUnitType.Pixel);
        this.m_ArrowDoubleUp = new System.Windows.Controls.Image();
        this.m_ArrowDoubleUp.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.DoubleUp);
        this.m_ArrowDoubleUp.Stretch = Stretch.Fill;
        GridUtil.PlaceElement(this.horizontalGrid, (UIElement) this.m_ArrowDoubleUp, 0, 0);
        GridUtil.PlaceElement(this.horizontalGrid, (UIElement) this.m_ArrowUp, 0, 1);
        GridUtil.PlaceElement(this.horizontalGrid, (UIElement) this.m_ArrowDown, 0, 4);
        this.m_ArrowDoubleDown = new System.Windows.Controls.Image();
        this.m_ArrowDoubleDown.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.DoubleDown);
        this.m_ArrowDoubleDown.Stretch = Stretch.Fill;
        GridUtil.PlaceElement(this.horizontalGrid, (UIElement) this.m_ArrowDoubleDown, 0, 3);
      }
      else
      {
        GridUtil.PlaceElement(this.horizontalGrid, (UIElement) this.m_ArrowUp, 0, 0);
        GridUtil.PlaceElement(this.horizontalGrid, (UIElement) this.m_ArrowDown, 0, 2);
      }
      GridUtil.PlaceElement(this.toolbarHorizontalGrid, (UIElement) this.horizontalGrid, 1, 1);
    }

    private void AddImagesInVerticalAllignment()
    {
      this.toolbarVerticalGrid.Children.Clear();
      this.toolbarVerticalGrid.ColumnDefinitions.Clear();
      this.toolbarVerticalGrid.RowDefinitions.Clear();
      this.verticalGrid.Children.Clear();
      this.verticalGrid.ColumnDefinitions.Clear();
      this.verticalGrid.RowDefinitions.Clear();
      GridUtil.AddColumnDef(this.toolbarVerticalGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.toolbarVerticalGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.toolbarVerticalGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.toolbarVerticalGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.toolbarVerticalGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.toolbarVerticalGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.verticalGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.verticalGrid, 15.0, GridUnitType.Pixel);
      GridUtil.AddRowDef(this.verticalGrid, 15.0, GridUnitType.Pixel);
      GridUtil.AddRowDef(this.verticalGrid, 15.0, GridUnitType.Pixel);
      this.m_ArrowRight = new System.Windows.Controls.Image();
      this.m_ArrowRight.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.arrow_right);
      this.m_ArrowRight.Stretch = Stretch.Fill;
      GridUtil.PlaceElement(this.verticalGrid, (UIElement) this.m_ArrowRight, 0, 0);
      this.m_ArrowLeft = new System.Windows.Controls.Image();
      this.m_ArrowLeft.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.arrow_left);
      this.m_ArrowLeft.Stretch = Stretch.Fill;
      if (this.bacPane.ShowAllSelectButtons)
      {
        GridUtil.AddRowDef(this.verticalGrid, 15.0, GridUnitType.Pixel);
        GridUtil.AddRowDef(this.verticalGrid, 15.0, GridUnitType.Pixel);
        this.m_ArrowDoubleRight = new System.Windows.Controls.Image();
        this.m_ArrowDoubleRight.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.fwd);
        this.m_ArrowDoubleRight.Stretch = Stretch.Fill;
        GridUtil.PlaceElement(this.verticalGrid, (UIElement) this.m_ArrowDoubleRight, 1, 0);
        GridUtil.PlaceElement(this.verticalGrid, (UIElement) this.m_ArrowLeft, 3, 0);
        this.m_ArrowDoubleLeft = new System.Windows.Controls.Image();
        this.m_ArrowDoubleLeft.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.back);
        this.m_ArrowDoubleLeft.Stretch = Stretch.Fill;
        GridUtil.PlaceElement(this.verticalGrid, (UIElement) this.m_ArrowDoubleLeft, 4, 0);
      }
      else
        GridUtil.PlaceElement(this.verticalGrid, (UIElement) this.m_ArrowLeft, 2, 0);
      GridUtil.PlaceElement(this.toolbarVerticalGrid, (UIElement) this.verticalGrid, 1, 1);
    }
  }
}
