﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualFindFormPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.FormPaneControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Oberon.Controller;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualFindFormPane : VisualBasePane
  {
    private int desiredRows;

    internal FindFormPane FindFormPane
    {
      get
      {
        return this.m_ModelObject as FindFormPane;
      }
    }

    public int DesiredRows
    {
      get
      {
        return this.desiredRows;
      }
    }

    public VisualFindFormPane(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void ConstructBodyElement()
    {
      try
      {
        this.desiredRows = this.GetDesiredNumberOfRows();
        for (int index = 0; index < this.FindFormPane.Columns; ++index)
          GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
        for (int index = 0; index < this.desiredRows; ++index)
          GridUtil.AddRowDef(this.BodyElementGrid, 100.0, 100.0, GridUnitType.Auto);
        int desiredColumn = 0;
        int desiredRow = 0;
        foreach (FieldGroup fieldGroup in this.FindFormPane.SectionGroup)
        {
          if (desiredColumn == this.FindFormPane.Columns || fieldGroup.ForceNewLine)
          {
            ++desiredRow;
            desiredColumn = 0;
          }
          GridUtil.GetNextAvialableLocation(this.BodyElementGrid, ref desiredRow, ref desiredColumn, fieldGroup.ColSpan);
          while (desiredColumn != 0 && fieldGroup.ForceNewLine)
          {
            ++desiredRow;
            desiredColumn = 0;
            GridUtil.GetNextAvialableLocation(this.BodyElementGrid, ref desiredRow, ref desiredColumn, fieldGroup.ColSpan);
          }
          if (this.BodyElementGrid.RowDefinitions.Count <= desiredRow)
            GridUtil.AddRowDef(this.BodyElementGrid, 100.0, 100.0, GridUnitType.Auto);
          if (fieldGroup.Row != desiredRow)
            fieldGroup.Row = desiredRow;
          if (fieldGroup.Column != desiredColumn)
            fieldGroup.Column = desiredColumn;
          VisualFieldGroup visualFieldGroup = new VisualFieldGroup((IModelObject) fieldGroup, (BaseVisualControl) this);
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) visualFieldGroup, desiredRow, desiredColumn, fieldGroup.RowSpan, fieldGroup.ColSpan);
          visualFieldGroup.InitializeSpanningDetails(this.desiredRows);
          desiredColumn += fieldGroup.ColSpan;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of Form pane control failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is FieldGroup)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed because of unknown reasons.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (!(e.PropertyName == FormPane.ColumnsProperty) && !(e.PropertyName == FormPane.ToolbarProperty) && !(e.PropertyName == FormPane.FieldGroupsProperty))
          return;
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of FindFormpane was not reflected in UI because of unknown reasons.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        base.AddContextMenuItems();
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for VisualForm.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      try
      {
        base.HandleContextMenuItemClick(source);
        if (source.Header == null || source.Header != (object) "Delete" || this.FindFormPane.Parent == null)
          return;
        this.FindFormPane.Parent.RemoveModelObject((IModelObject) this.FindFormPane);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    protected override void OnBOElementDrop(BOBrowserDragEventArgs boBrowserArgs)
    {
      try
      {
        BOElement boElement = (BOElement) null;
        if (boBrowserArgs != null && boBrowserArgs.Count > 0)
          boElement = boBrowserArgs[0].DraggedElement as BOElement;
        if (boElement != null && boElement.ParentQuery != null)
        {
          string queryPath = Utilities.GetQueryPath(boBrowserArgs[0].PathToElement);
          if (string.IsNullOrEmpty(this.FindFormPane.Query.QueryPath))
          {
            ((UXQuery) this.FindFormPane.Query).BindQueryPath(boBrowserArgs[0].NameSpace, boBrowserArgs[0].ProxyName, queryPath);
            base.OnBOElementDrop(boBrowserArgs);
          }
          else if (this.FindFormPane.Query.QueryPath == queryPath)
          {
            base.OnBOElementDrop(boBrowserArgs);
          }
          else
          {
            int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.ParameterFromDifferentQuery, MessageBoxButtons.OK, MessageBoxIcon.Hand);
          }
        }
        else
        {
          int num1 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.OnlyQueryParameterAllowed, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Adding of QueryParameter to Advanced find form failed.", ex));
      }
    }

    protected override void OnDataModelElementDrop(DataModelDragEventArgs dmArgs)
    {
      try
      {
        if (dmArgs.DataElement.IsQueryBinding)
        {
          if (this.FindFormPane.Query == null)
          {
            int num1 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.AssociateQueryToFindForm, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
          }
          else if (dmArgs.DataElement.AssociatedQueryParameterBinding.QueryName == this.FindFormPane.Query.Name)
          {
            base.OnDataModelElementDrop(dmArgs);
          }
          else
          {
            int num2 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.ParameterFromDifferentQuery, MessageBoxButtons.OK, MessageBoxIcon.Hand);
          }
        }
        else
        {
          int num3 = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.OnlyQueryParameterAllowed, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Adding of QueryParameter to Advanced find form failed.", ex));
      }
    }

    protected override void OnToolBoxControlDragOver(System.Windows.DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      base.OnToolBoxControlDragOver(e, toolboxEventArg);
      if (!ValidationManager.GetInstance().ValidateModel(this.Model, toolboxEventArg.Tag.ClassName))
        return;
      if (this.Model is FindFormPane && toolboxEventArg.Tag.ControlName == "RadioButtonGroup" || (toolboxEventArg.Tag.ControlName == "LayoutGridPane" || toolboxEventArg.Tag.ControlName == "LayoutStackPane") || toolboxEventArg.Tag.ControlName == "LayoutBorderPane")
        e.Effects = System.Windows.DragDropEffects.None;
      else
        e.Effects = System.Windows.DragDropEffects.Copy;
    }

    protected override void OnBaseVisualControlDragOver(System.Windows.DragEventArgs e, IModelObject droppedModel)
    {
      if (droppedModel is FieldGroup && (droppedModel as FieldGroup).Parent.GetType() != typeof (FindFormPane))
        e.Effects = System.Windows.DragDropEffects.None;
      else
        base.OnBaseVisualControlDragOver(e, droppedModel);
    }

    private int GetDesiredNumberOfRows()
    {
      int num = 0;
      foreach (FieldGroup fieldGroup in this.FindFormPane.SectionGroup)
      {
        if (num == fieldGroup.Row || fieldGroup.ForceNewLine)
          ++num;
      }
      return num;
    }

    public override string SelectionText
    {
      get
      {
        return "FindFormpane";
      }
    }
  }
}
