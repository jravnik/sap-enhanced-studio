﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls.VisualAdvancedDataSetPaneVariant
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls
{
  public class VisualAdvancedDataSetPaneVariant : BaseSelectableControl, IContextMenuContainer
  {
    private InitFormType m_InitForm = InitFormType.advanced;
    private int index;
    private AdvancedDataSetPane m_DataSetPane;
    private FindFormPane m_FFModel;
    private VisualFindFormPane m_FindFormPane;
    private VisualFindFormToolbar m_ffToolbar;
    private VisualToolbar m_Toolbar;
    private VisualDataSetControl m_DataSetControl;
    private VisualAlternateListVisualization m_AlternateVisual;
    private VisualLayout m_PreviewArea;
    private AlternativeListVisualization alVisualization;

    private AdvancedDataSetPaneVariant DataSetPaneVariant
    {
      get
      {
        return this.m_ModelObject as AdvancedDataSetPaneVariant;
      }
    }

    public VisualAdvancedDataSetPaneVariant(IModelObject model, FindFormPane ffp, InitFormType initForm, IModelObject parent, AlternativeListVisualization alVisual)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_ModelObject = model;
      this.m_DataSetPane = parent as AdvancedDataSetPane;
      this.m_FFModel = ffp;
      this.m_InitForm = initForm;
      this.alVisualization = alVisual;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.index = 0;
        base.LoadView();
        this.Content = (object) this.Grid;
        if (this.m_InitForm == InitFormType.advanced && this.m_FFModel != null)
          this.CreateFindForm(this.m_FFModel);
        if (this.DataSetPaneVariant == null)
          return;
        if (this.DataSetPaneVariant.DataSetPaneHeader != null)
          this.CreateDataSetHeader(this.DataSetPaneVariant);
        if (this.alVisualization == null)
        {
          if (this.DataSetPaneVariant.UseToolbar)
            this.CreateToolBar(this.DataSetPaneVariant);
          this.CreateDataSetDefinition(this.DataSetPaneVariant);
          if (this.DataSetPaneVariant.PreviewArea == null)
            return;
          this.CreatePreviewArea(this.DataSetPaneVariant);
        }
        else
        {
          if (this.alVisualization.UseStandardToolbar && this.DataSetPaneVariant.Toolbar != null)
            this.CreateToolBar(this.DataSetPaneVariant);
          this.CreateAlternateVisualization(this.DataSetPaneVariant, this.alVisualization);
          if (!this.alVisualization.UseStandardDetailsArea || this.DataSetPaneVariant.PreviewArea == null)
            return;
          this.CreatePreviewArea(this.DataSetPaneVariant);
        }
      }
      catch
      {
      }
    }

    private void CreateFindForm(FindFormPane ffp)
    {
      GridUtil.AddRowDef(this.Grid, 200.0, GridUnitType.Star);
      VisualFindFormPane visualFindFormPane = new VisualFindFormPane((IModelObject) ffp);
      visualFindFormPane.MinHeight = 80.0;
      this.m_FindFormPane = visualFindFormPane;
      visualFindFormPane.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) visualFindFormPane, this.index++, 0);
      GridUtil.AddRowDef(this.Grid, 200.0, GridUnitType.Auto);
      VisualFindFormToolbar visualFindFormToolbar = new VisualFindFormToolbar((IModelObject) ffp);
      this.m_ffToolbar = visualFindFormToolbar;
      visualFindFormToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) visualFindFormToolbar, this.index++, 0);
    }

    private void CreateDataSetHeader(AdvancedDataSetPaneVariant dataSetVariant)
    {
      if (dataSetVariant == null || dataSetVariant.DataSetPaneHeader == null || string.IsNullOrEmpty(dataSetVariant.DataSetPaneHeader.Text))
        return;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      TextBlock textBlock = new TextBlock();
      textBlock.Text = dataSetVariant.DataSetPaneHeader.Text;
      textBlock.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) textBlock, this.index++, 0);
    }

    private void CreateToolBar(AdvancedDataSetPaneVariant dataSetPaneVariant)
    {
      if (dataSetPaneVariant.Toolbar == null)
        return;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      VisualToolbar visualToolbar = new VisualToolbar((IModelObject) dataSetPaneVariant.Toolbar);
      this.m_Toolbar = visualToolbar;
      visualToolbar.VerticalAlignment = VerticalAlignment.Top;
      visualToolbar.HorizontalAlignment = HorizontalAlignment.Stretch;
      visualToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      visualToolbar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      GridUtil.PlaceElement(this.Grid, (UIElement) visualToolbar, this.index++, 0);
    }

    private void CreatePreviewArea(AdvancedDataSetPaneVariant dataSetPaneVariant)
    {
      GridUtil.AddRowDef(this.Grid, 300.0, GridUnitType.Star);
      VisualLayout visualLayout = new VisualLayout((IModelObject) dataSetPaneVariant.PreviewArea);
      this.m_PreviewArea = visualLayout;
      visualLayout.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) visualLayout, this.index++, 0);
    }

    private void CreateDataSetDefinition(AdvancedDataSetPaneVariant dataSetPaneVariant)
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      UserControl userControl = (UserControl) new VisualDataSetControl((IModelObject) dataSetPaneVariant.DataSetDefintion);
      this.m_DataSetControl = userControl as VisualDataSetControl;
      userControl.VerticalAlignment = VerticalAlignment.Top;
      userControl.HorizontalAlignment = HorizontalAlignment.Stretch;
      userControl.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) userControl, this.index++, 0);
    }

    private void CreateAlternateVisualization(AdvancedDataSetPaneVariant lpv, AlternativeListVisualization alv)
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      UserControl userControl = (UserControl) new VisualAlternateListVisualization(this.alVisualization, (IModelObject) lpv, this);
      this.m_AlternateVisual = userControl as VisualAlternateListVisualization;
      userControl.VerticalAlignment = VerticalAlignment.Top;
      userControl.HorizontalAlignment = HorizontalAlignment.Stretch;
      userControl.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) userControl, this.index++, 0);
    }

    public VisualAdvancedDataSetPaneVariant ReloadAdvancedDataSetPaneVariant(IModelObject model, FindFormPane ffp, InitFormType initForm, AlternativeListVisualization alVisual, IModelObject parent)
    {
      this.m_ModelObject = model;
      this.m_FFModel = ffp;
      this.m_InitForm = initForm;
      this.m_DataSetPane = parent as AdvancedDataSetPane;
      this.alVisualization = alVisual;
      this.ReloadView();
      IDesigner parentDesigner = (IDesigner) ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
      if (parentDesigner != null)
        parentDesigner.RaiseSelectionChanged((IDesignerControl) this, (IModelObject) this.DataSetPaneVariant);
      return this;
    }

    public void LoadAlternateVisual(AlternativeListVisualization alVisual)
    {
      this.ReloadAdvancedDataSetPaneVariant((IModelObject) this.DataSetPaneVariant, this.m_FFModel, this.m_InitForm, alVisual, (IModelObject) this.m_DataSetPane);
    }

    public override string SelectionText
    {
      get
      {
        return "AdvancedDataSetPaneVariant";
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == AdvancedDataSetPaneVariant.ToolbarProperty || e.PropertyName == AdvancedDataSetPaneVariant.PreviewAreaProperty || (e.PropertyName == AdvancedDataSetPaneVariant.AlternateVisualizationsProperty || e.PropertyName == AdvancedDataSetPaneVariant.UseToolBarProperty))
      {
        this.ReloadView();
      }
      else
      {
        if (this.m_Toolbar == null)
          return;
        this.m_Toolbar.Toolbar = (Toolbar) this.DataSetPaneVariant.Toolbar;
        this.m_Toolbar.ReloadView();
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (this.m_Toolbar == null)
        return;
      this.m_Toolbar.HandleContextMenuItemClickWrapper(source);
    }

    protected override void AddContextMenuItems()
    {
      if (this.m_Toolbar == null)
        return;
      this.m_Toolbar.AddContextMenuItems((IContextMenuContainer) this);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
