﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls.VisualDataSetContainerItem
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls
{
  public class VisualDataSetContainerItem : BaseSelectableControl
  {
    private const string CONTEXT_MENU_DELETE = "Delete";
    private VisualLabel labelControl;

    private DataSetControlContainerItem DataSetControlContainerItem
    {
      get
      {
        return this.m_ModelObject as DataSetControlContainerItem;
      }
    }

    public VisualDataSetContainerItem(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.ControlBackground = (Brush) ComponentModelersConstants.FIELD_AREA_BRUSH;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        GridUtil.AddRowDef(this.Grid, 25.0, this.DataSetControlContainerItem.Item is RadioButtonGroup ? GridUnitType.Auto : GridUnitType.Pixel);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 5.0, GridUnitType.Star);
        this.labelControl = new VisualLabel(this.DataSetControlContainerItem.Label != null ? this.DataSetControlContainerItem.Label.Text : string.Empty, false, false);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.labelControl, 0, 0);
        GridUtil.PlaceElement(this.Grid, ControlFactory.ConstructFormpaneControl(this.DataSetControlContainerItem.Item, this.DataSetControlContainerItem.Item.CCTSType, UsageType.form), 0, 2);
        if (this.DataSetControlContainerItem == null || this.DataSetControlContainerItem.Item == null)
          return;
        this.DataSetControlContainerItem.Item.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of DataSetControlContainerItem Control failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "DataSetControlContainerItem";
      }
    }

    protected override void OnBOElementDragOver(DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
      if (boElement != null && boElement.ParentQuery != null)
        e.Effects = DragDropEffects.None;
      if (boElement == null || boElement.ParentAction == null)
        return;
      e.Effects = DragDropEffects.None;
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      try
      {
        if (!(source.Header.ToString() == "Delete"))
          return;
        this.DataSetControlContainerItem.Parent.RemoveModelObject((IModelObject) this.DataSetControlContainerItem);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      if (e.PropertyName == "Label" || e.PropertyName == "Mandatory")
      {
        string empty = string.Empty;
        if (this.DataSetControlContainerItem != null)
        {
          if (this.DataSetControlContainerItem.Label != null)
          {
            string text = this.DataSetControlContainerItem.Label.Text;
          }
          if (this.DataSetControlContainerItem.Item is EditControl)
            Converter.ToBoolean((DependentProperty) (this.DataSetControlContainerItem.Item as EditControl).Mandatory, false);
        }
        this.TriggerSelection((BaseSelectableControl) this, false);
      }
      else
        this.ReloadView();
    }

    protected override void OnBOElementDrop(BOBrowserDragEventArgs boBrowserArgs)
    {
      try
      {
        if (this.DataSetControlContainerItem.Parent == null)
          return;
        foreach (BOBrowserDragEventArg boBrowserArg in (List<BOBrowserDragEventArg>) boBrowserArgs)
          this.DataSetControlContainerItem.Parent.AddModelObject(boBrowserArg);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Control Drop event Failed.", ex));
      }
    }

    protected override void OnDataModelElementDrop(DataModelDragEventArgs dmArgs)
    {
      try
      {
        if (this.DataSetControlContainerItem.Parent == null)
          return;
        if (this.DataSetControlContainerItem != null && this.DataSetControlContainerItem.BoundField != null && !this.DataSetControlContainerItem.BoundField.IsBound)
          base.OnDataModelElementDrop(dmArgs);
        else
          this.DataSetControlContainerItem.Parent.AddModelObject(dmArgs);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Control Drop event Failed.", ex));
      }
    }

    protected override void OnToolBoxControlDrop(ToolBoxEventArgs toolboxEventArg)
    {
      try
      {
        if (this.DataSetControlContainerItem.Parent == null)
          return;
        this.DataSetControlContainerItem.AddModelObject(toolboxEventArg.Tag);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Control Drop event Failed.", ex));
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.DataSetControlContainerItem == null)
        return;
      this.DataSetControlContainerItem.Item.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
    }
  }
}
