﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.GanttChartPaneControls.VisualGanttChartItem
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.GanttChartPaneControls
{
  public class VisualGanttChartItem : BaseSelectableControl
  {
    private BaseVisualControl parentVisualControl;
    private VisualChartItemListVariant listVariant;
    private VisualGanttChartBar barDefinition;
    private VisualGanttChartPane ganttChartPane;
    private Grid splitterGrid;

    private GanttChartItem GanttChartItem
    {
      get
      {
        return this.m_ModelObject as GanttChartItem;
      }
    }

    public VisualGanttChartItem(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.AllowDrop = true;
      this.parentVisualControl = parentControl;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.ganttChartPane = this.parentVisualControl as VisualGanttChartPane;
      if (this.GanttChartItem.ListVariant != null && this.GanttChartItem.ListVariant.Count > 0)
      {
        foreach (IModelObject model in this.GanttChartItem.ListVariant)
          this.listVariant = new VisualChartItemListVariant(model, (BaseVisualControl) this);
        this.ganttChartPane.OnListVariantChanged -= new ListVariantChanged(this.GanttChartPane_OnListVariantChanged);
        this.ganttChartPane.OnListVariantChanged += new ListVariantChanged(this.GanttChartPane_OnListVariantChanged);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.listVariant, 0, 0);
      }
      this.CreateSplitter();
      GridUtil.PlaceElement(this.Grid, (UIElement) this.splitterGrid, 0, 1);
      if (this.GanttChartItem.BarDefinition == null)
        return;
      this.barDefinition = new VisualGanttChartBar((IModelObject) this.GanttChartItem.BarDefinition);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.barDefinition, 0, 2);
      if (this.GanttChartItem.BarDefinition.RelationshipDefinition == null)
        return;
      GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNetworkRelationShip((IModelObject) this.GanttChartItem.BarDefinition.RelationshipDefinition), 1, 0, 1, 3);
    }

    private void GanttChartPane_OnListVariantChanged(GanttChartItemListVariant var)
    {
      GanttChartItemListVariant chartItemListVariant = var;
      if (chartItemListVariant == null)
        return;
      this.listVariant.ReloadChartItemListVariant((IModelObject) chartItemListVariant);
    }

    public override void Terminate()
    {
      if (this.listVariant != null)
        this.listVariant = (VisualChartItemListVariant) null;
      this.ganttChartPane.DisposeEvent();
      base.Terminate();
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        if (e.PropertyName == GanttChartItem.ListVariantProperty)
        {
          if (this.parentVisualControl != null)
            this.parentVisualControl.ReloadView();
        }
        else
          this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of GanttChartItem failed.", ex));
      }
      base.OnModelPropertyChanged(sender, e);
    }

    public override string SelectionText
    {
      get
      {
        return "GanttChartItem";
      }
    }

    private void CreateSplitter()
    {
      this.splitterGrid = new Grid();
      GridUtil.AddColumnDef(this.splitterGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.splitterGrid, 1.0, GridUnitType.Star);
      Rectangle rectangle = new Rectangle();
      rectangle.Width = 5.0;
      rectangle.MinHeight = 150.0;
      GridUtil.PlaceElement(this.splitterGrid, (UIElement) rectangle, 0, 0);
    }
  }
}
