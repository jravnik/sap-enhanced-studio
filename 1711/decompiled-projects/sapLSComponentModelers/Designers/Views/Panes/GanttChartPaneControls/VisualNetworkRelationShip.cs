﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.GanttChartPaneControls.VisualNetworkRelationShip
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.GanttChartPaneControls
{
  public class VisualNetworkRelationShip : BaseSelectableControl
  {
    private NetworkRelationship NetworkRelationship
    {
      get
      {
        return this.m_ModelObject as NetworkRelationship;
      }
    }

    public VisualNetworkRelationShip(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      TextBlock textBlock = new TextBlock();
      textBlock.Height = 20.0;
      textBlock.Text = "BAR RELATIONSHIP";
      textBlock.FontSize = 14.0;
      textBlock.TextAlignment = TextAlignment.Center;
      textBlock.Background = (Brush) Brushes.LightGray;
      GridUtil.PlaceElement(this.Grid, (UIElement) textBlock, 0, 0);
    }

    public override string SelectionText
    {
      get
      {
        return "NetworkRelationship";
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of NetworkRelationship failed.", ex));
      }
      base.OnModelPropertyChanged(sender, e);
    }
  }
}
