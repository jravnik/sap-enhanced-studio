﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.TabstripControls.VisualTabstripItem
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.TabstripControls
{
  public class VisualTabstripItem : BaseSelectableControl
  {
    private const string RESOURCE_TAB_RIGHT_ON = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_end.png,12,24";
    private const string RESOURCE_TAB_RIGHT_OFF = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_end.png,12,24";
    private const string RESOURCE_TAB_RIGHT_OFFON = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_on_right.png,12,24";
    private const string RESOURCE_TAB_RIGHT_ONOFF = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_off_right.png,12,24";
    private const string RESOURCE_TAB_BACK_ON = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_back.png,1,24";
    private const string RESOURCE_TAB_BACK_OFF = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_back.png,1,24";
    private const string RESOURCE_TAB_LEFT_ONOFF = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_off_left.png,12,24";
    private const string RESOURCE_TAB_LEFT_OFFON = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_on_left.png,12,24";
    private const string RESOURCE_TAB_LEFT_ON = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on.png,23,24";
    private const string RESOURCE_TAB_LEFT_OFF = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off.png,23,24";
    private TextBlock textControl;
    private Image leftImage;
    private Image rightImage;
    private Image centerImage;
    private bool selected;
    private int index;
    private VisualTabStrip m_parentTabstrip;
    internal EventHandler<EventArgs> TabSelected;
    internal EventHandler<EventArgs> TabUnselected;

    internal int Index
    {
      get
      {
        return this.index;
      }
      set
      {
        this.index = value;
      }
    }

    internal bool Selected
    {
      get
      {
        return this.selected;
      }
    }

    internal Tab Tab
    {
      get
      {
        return this.m_ModelObject as Tab;
      }
    }

    public VisualTabstripItem(IModelObject model, int index, VisualTabStrip parent)
      : base(model)
    {
      this.index = index;
      this.m_parentTabstrip = parent;
      this.LoadView();
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (!(e.PropertyName == "TabText"))
        return;
      this.textControl.Text = this.Tab.TabText.Text;
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.textControl = new TextBlock();
        this.textControl.IsHitTestVisible = true;
        this.textControl.VerticalAlignment = VerticalAlignment.Center;
        this.textControl.HorizontalAlignment = HorizontalAlignment.Center;
        this.textControl.Text = this.Tab.TabText.Text;
        this.IsHitTestVisible = true;
        this.MouseDown += new MouseButtonEventHandler(this.VisualTabstripItem_MouseDown);
        this.InitializeGrid();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Fialed to create TabStrip Item.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        base.AddContextMenuItems();
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for TabStrip.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      try
      {
        base.HandleContextMenuItemClick(source);
        if (source.Header == null || source.Header != (object) "Delete" || this.Tab == null)
          return;
        this.Tab.Parent.RemoveModelObject((IModelObject) this.Tab);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    private void VisualTabstripItem_MouseDown(object sender, MouseButtonEventArgs e)
    {
      if (this.TabSelected == null)
        return;
      this.TabSelected((object) this, (EventArgs) e);
    }

    public override void Terminate()
    {
      base.Terminate();
      this.leftImage = (Image) null;
      this.rightImage = (Image) null;
      this.centerImage = (Image) null;
    }

    public override void SelectMe()
    {
      try
      {
        if (this.TabSelected != null)
          this.TabSelected((object) this, (EventArgs) null);
        if (this.index == 0)
          ResourceUtil.LoadImage(this.leftImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on.png,23,24");
        else
          ResourceUtil.LoadImage(this.leftImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_on_left.png,12,24");
        TabStrip parent = this.Tab.Parent as TabStrip;
        if (parent != null && parent.Tabs.Count - 1 > this.index)
          ResourceUtil.LoadImage(this.rightImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_off_right.png,12,24");
        else
          ResourceUtil.LoadImage(this.rightImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_end.png,12,24");
        ResourceUtil.LoadImage(this.centerImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_back.png,1,24");
        this.centerImage.Stretch = Stretch.Fill;
        this.centerImage.Width = double.NaN;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Selection of the TabStrip failed.", ex));
      }
    }

    public override void UnSelect()
    {
      try
      {
        base.UnSelect();
        this.selected = false;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    public void UnselectTab()
    {
      this.selected = false;
      if (this.index == 0)
        ResourceUtil.LoadImage(this.leftImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off.png,23,24");
      else
        ResourceUtil.LoadImage(this.leftImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_off_left.png,12,24");
      ResourceUtil.LoadImage(this.rightImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_end.png,12,24");
      ResourceUtil.LoadImage(this.centerImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_back.png,1,24");
      this.centerImage.Stretch = Stretch.Fill;
      this.centerImage.Width = double.NaN;
      if (this.TabUnselected == null)
        return;
      this.TabUnselected((object) this, (EventArgs) null);
    }

    public void SelectPreviousTab()
    {
      try
      {
        ResourceUtil.LoadImage(this.rightImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_on_right.png,12,24");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Selection of the TabStrip failed.", ex));
      }
    }

    private void InitializeGrid()
    {
      this.leftImage = new Image();
      this.rightImage = new Image();
      this.centerImage = new Image();
      if (this.index == 0)
        ResourceUtil.LoadImage(this.leftImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off.png,23,24");
      else
        ResourceUtil.LoadImage(this.leftImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_off_left.png,12,24");
      ResourceUtil.LoadImage(this.rightImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_end.png,12,24");
      ResourceUtil.LoadImage(this.centerImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_back.png,1,24");
      this.centerImage.Stretch = Stretch.Fill;
      this.centerImage.Width = double.NaN;
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.leftImage, 0, 0);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.centerImage, 0, 1);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.textControl, 0, 1);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.rightImage, 0, 2);
    }

    public void SelectNextTab()
    {
      try
      {
        ResourceUtil.LoadImage(this.leftImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_off_left.png,12,24");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Selection of the TabStrip failed.", ex));
      }
    }

    public void UnSelectPreviousTab()
    {
      try
      {
        ResourceUtil.LoadImage(this.rightImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_end.png,12,24");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Selection of the TabStrip failed.", ex));
      }
    }

    public void UnSelectNextTab()
    {
      try
      {
        if (this.index == 0)
          ResourceUtil.LoadImage(this.leftImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off.png,23,24");
        else
          ResourceUtil.LoadImage(this.leftImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_on_off_left.png,12,24");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Selection of the TabStrip failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Tab";
      }
    }

    public void SetDisplayStylesOfTabStripItem(int startIndex, int endIndex, int selectedIndex)
    {
      if (this.index == endIndex)
      {
        if (selectedIndex != this.index + 1)
          return;
        ResourceUtil.LoadImage(this.rightImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_end.png,12,24");
      }
      else
      {
        if (endIndex != this.index + 1 || this.index + 1 != selectedIndex)
          return;
        ResourceUtil.LoadImage(this.rightImage, "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_off_on_right.png,12,24");
      }
    }
  }
}
