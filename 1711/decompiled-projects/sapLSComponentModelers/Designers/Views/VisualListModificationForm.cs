﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualListModificationForm
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualListModificationForm : BaseSelectableControl
  {
    private readonly string DELETECOMMAND = "Delete";
    private TextBlock text;

    internal ListModificationForm ListModificationForm
    {
      get
      {
        return this.m_ModelObject as ListModificationForm;
      }
    }

    public VisualListModificationForm(ListModificationForm listModificationForm)
      : base((IModelObject) listModificationForm)
    {
      this.LoadView();
    }

    public override void LoadView()
    {
      this.Padding = new Thickness(10.0, 0.0, 10.0, 0.0);
      System.Windows.Controls.Image image = new System.Windows.Controls.Image();
      image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(Resource.ActionFormIcon);
      image.Stretch = Stretch.None;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) image, 0, 0);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.text = new TextBlock();
      this.text.TextAlignment = TextAlignment.Center;
      this.text.Text = this.ListModificationForm.Name;
      GridUtil.PlaceElement(this.Grid, (UIElement) this.text, 1, 0);
    }

    public override string SelectionText
    {
      get
      {
        return "List Modification Form";
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem(this.DELETECOMMAND);
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (!(source.Header.ToString() == this.DELETECOMMAND))
        return;
      ListModificationFormsCollection parent = this.ListModificationForm.Parent as ListModificationFormsCollection;
      if (parent == null)
        return;
      parent.RemoveListModificationForm(this.ListModificationForm);
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (!(e.PropertyName == ModelObject.NameProperty))
        return;
      this.text.Text = e.NewValue.ToString();
    }

    private void OnClick(object sender, MouseButtonEventArgs e)
    {
      if (e.ClickCount <= 1)
        return;
      object tag = (sender is System.Windows.Controls.Image || sender is TextBlock ? (FrameworkElement) (((FrameworkElement) sender).Parent as StackPanel) : (FrameworkElement) (sender as StackPanel)).Tag;
    }
  }
}
