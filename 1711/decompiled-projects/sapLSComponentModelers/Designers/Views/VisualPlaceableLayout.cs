﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualPlaceableLayout
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualPlaceableLayout : BaseBorderControl
  {
    private readonly string DELETECOMMAND = "Delete";
    private TextBlock text;

    public PlaceableFields PlaceableFields
    {
      get
      {
        return this.m_ModelObject as PlaceableFields;
      }
    }

    public VisualPlaceableLayout(IModelObject PlaceableFields)
      : base(PlaceableFields)
    {
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "PlaceableFields";
      }
    }

    public override void LoadView()
    {
      this.Padding = new Thickness(10.0, 0.0, 10.0, 0.0);
      System.Windows.Controls.Image image = new System.Windows.Controls.Image();
      image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(Resource.ActionFormIcon);
      image.Stretch = Stretch.None;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) image, 0, 0);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.text = new TextBlock();
      this.text.TextAlignment = TextAlignment.Center;
      this.text.Text = "PlaceableFields";
      GridUtil.PlaceElement(this.Grid, (UIElement) this.text, 1, 0);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem(this.DELETECOMMAND);
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) this.DELETECOMMAND)
        return;
      UXView parent = this.PlaceableFields.Parent as UXView;
      if (parent == null)
        return;
      parent.RemovePlaceableField();
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      base.OnModelObjectAdded(sender, e);
      this.ReloadView();
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      this.ReloadView();
    }
  }
}
