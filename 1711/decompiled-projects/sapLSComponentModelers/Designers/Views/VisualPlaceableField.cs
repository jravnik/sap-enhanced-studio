﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualPlaceableField
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualPlaceableField : BaseSelectableControl
  {
    private const string RESOURCE_BACKGROUND = "#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0";
    private const string DELETECOMMAND = "Delete";
    private VisualLabel labelControl;
    private UIElement visualControl;
    private StackPanel contentPanel;

    internal ControlUsage ControlUsage
    {
      get
      {
        return this.m_ModelObject as ControlUsage;
      }
    }

    public VisualPlaceableField(IModelObject placeableField)
      : base(placeableField)
    {
      this.AllowDrop = true;
      this.LoadView();
    }

    public override void LoadView()
    {
      this.IsDraggable = true;
      this.IsSpanable = true;
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.ControlBackground = (Brush) ComponentModelersConstants.FIELD_AREA_BRUSH;
      this.PlaceElements(this.ControlUsage);
    }

    public override string SelectionText
    {
      get
      {
        return "PlaceableField";
      }
    }

    protected override void OnToolBoxControlDragOver(DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      base.OnToolBoxControlDragOver(e, toolboxEventArg);
      if (!ValidationManager.GetInstance().ValidateModel(this.Model, toolboxEventArg.Tag.ClassName))
        return;
      if (this.Model is ControlUsage)
        e.Effects = DragDropEffects.Copy;
      else
        e.Effects = DragDropEffects.None;
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        base.AddContextMenuItems();
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
        this.AddSeperatorToContextMenu();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for VisualField.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      try
      {
        base.HandleContextMenuItemClick(source);
        if (source.Header != (object) "Delete")
          return;
        PlaceableFields parent = this.ControlUsage.Parent as PlaceableFields;
        if (parent == null)
          return;
        parent.RemoveModelObject((IModelObject) this.ControlUsage, false);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    internal override void TriggerSelection(BaseSelectableControl vControl, bool isTriggeredByControlAddOperation)
    {
      if (isTriggeredByControlAddOperation)
      {
        if (this.ControlUsage != null)
        {
          ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
          if (parentDesigner.m_ActiveLinkedDesigner == null || !parentDesigner.m_ActiveLinkedDesigner.DesignerControlDictionary.ContainsKey(this.ControlUsage.Item.Id))
            return;
          BaseSelectableControl designerControl = parentDesigner.m_ActiveLinkedDesigner.DesignerControlDictionary[this.ControlUsage.Item.Id] as BaseSelectableControl;
          if (designerControl == null)
            return;
          base.TriggerSelection(designerControl, isTriggeredByControlAddOperation);
        }
        else
          base.TriggerSelection((BaseSelectableControl) this, isTriggeredByControlAddOperation);
      }
      else
        base.TriggerSelection(vControl, isTriggeredByControlAddOperation);
    }

    public override void ReloadView()
    {
      base.ReloadView();
      this.ControlUsage.Item.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        if (e.PropertyName == ControlUsage.LabelProperty)
          this.labelControl.Text = this.ControlUsage.Label != null ? this.ControlUsage.Label.Text : string.Empty;
        else if (e.PropertyName == ControlUsage.ItemProperty)
        {
          int int32_1 = Convert.ToInt32(this.visualControl.GetValue(Grid.ColumnProperty));
          int int32_2 = Convert.ToInt32(this.visualControl.GetValue(Grid.ColumnSpanProperty));
          this.Grid.Children.Remove(this.visualControl);
          this.visualControl = ControlFactory.ConstructFormpaneControl(this.ControlUsage.Item, UXCCTSTypes.none, UsageType.form);
          GridUnitType unitType = GridUnitType.Pixel;
          if (this.ControlUsage.Item is RadioButtonGroup)
            unitType = GridUnitType.Auto;
          this.Grid.RowDefinitions.Clear();
          GridUtil.AddRowDef(this.Grid, 25.0, unitType);
          GridUtil.PlaceElement(this.Grid, this.visualControl, 0, int32_1, 1, int32_2);
        }
        else if (e.PropertyName == AbstractControl.EnabledProperty || e.PropertyName == EditControl.ReadOnlyProperty)
          ControlFactory.SetEditableProperties(this.visualControl, this.ControlUsage.Item);
        else if (e.PropertyName == EditControl.MandatoryProperty)
          this.labelControl.ShowAsterisk = Converter.ToBoolean((DependentProperty) (this.ControlUsage.Item as EditControl).Mandatory, false);
        else if (e.PropertyName == StaticText.FormatProperty)
        {
          StaticText staticText = this.ControlUsage.Item as StaticText;
          VisualStaticText visualControl = this.visualControl as VisualStaticText;
          if (visualControl == null || visualControl == null)
            return;
          if (staticText.Format == FormatType.emphasized)
            visualControl.StaticTextFormat = VisualStaticText.StaticTextFormats.Emphasized;
          else
            visualControl.StaticTextFormat = VisualStaticText.StaticTextFormats.Standard;
        }
        else if (e.PropertyName == RadioButtonGroup.OrientationProperty)
        {
          RadioButtonGroup radioButtonGroup = this.ControlUsage.Item as RadioButtonGroup;
          VisualRadioButtonGroup visualControl = this.visualControl as VisualRadioButtonGroup;
          if (radioButtonGroup == null || visualControl == null)
            return;
          visualControl.Orientation = (Orientation) Enum.Parse(typeof (Orientation), radioButtonGroup.Orientation.ToString(), true);
        }
        else if (e.PropertyName == Link.HasMenuProperty)
        {
          Link link = this.ControlUsage.Item as Link;
          VisualStaticText visualControl = this.visualControl as VisualStaticText;
          if (link == null || visualControl == null)
            return;
          visualControl.IsMenuVisible = link.HasMenu;
        }
        else if (e.PropertyName == TextEditControl.RowsProperty)
          this.SetHeightOfTextEditControl();
        else if (e.PropertyName == CompoundField.ItemsProperty)
        {
          int row = Grid.GetRow(this.visualControl);
          int column = Grid.GetColumn(this.visualControl);
          int rowSpan = Grid.GetRowSpan(this.visualControl);
          int columnSpan = Grid.GetColumnSpan(this.visualControl);
          BaseVisualControl visualControl = this.visualControl as BaseVisualControl;
          if (visualControl != null)
            visualControl.Terminate();
          this.Grid.Children.Remove(this.visualControl);
          this.visualControl = ControlFactory.ConstructFormpaneControl(this.ControlUsage.Item, UXCCTSTypes.none, UsageType.form);
          GridUtil.PlaceElement(this.Grid, this.visualControl, row, column, rowSpan, columnSpan);
          if (!(this.ControlUsage.Item is CompoundField))
            return;
          bool flag = false;
          CompoundField compoundField = this.ControlUsage.Item as CompoundField;
          if (compoundField.Items != null)
          {
            foreach (AbstractControl abstractControl in compoundField.Items)
            {
              if (abstractControl is EditControl)
              {
                EditControl editControl = abstractControl as EditControl;
                flag |= Converter.ToBoolean((DependentProperty) editControl.Mandatory, false);
              }
            }
          }
          this.labelControl.ShowAsterisk = flag;
        }
        else
          this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update UI on Property Changed event.", ex));
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      this.ControlUsage.Item.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
    }

    private void PlaceElements(ControlUsage cu)
    {
      GridUtil.AddRowDef(this.Grid, 25.0, cu.Item is RadioButtonGroup ? GridUnitType.Auto : GridUnitType.Pixel);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 5.0, GridUnitType.Pixel);
      if (cu.Item.CCTSType != UXCCTSTypes.none)
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      else if (cu.Item.FieldWidth != null && cu.Item.FieldWidth != string.Empty)
        this.SetFieldWidthOfControls(cu);
      else
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      string text = this.ControlUsage.Label != null ? this.ControlUsage.Label.Text : string.Empty;
      bool showAsterisk = false;
      if (cu.Item is EditControl)
        showAsterisk = Converter.ToBoolean((DependentProperty) (cu.Item as EditControl).Mandatory, false);
      else if (cu.Item is CompoundField)
      {
        CompoundField compoundField = cu.Item as CompoundField;
        if (compoundField.Items != null)
        {
          foreach (AbstractControl abstractControl in compoundField.Items)
          {
            if (abstractControl is EditControl)
            {
              EditControl editControl = abstractControl as EditControl;
              showAsterisk |= Converter.ToBoolean((DependentProperty) editControl.Mandatory, false);
            }
          }
        }
      }
      this.labelControl = new VisualLabel(text, showAsterisk, false);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.labelControl, 0, 0);
      this.visualControl = ControlFactory.ConstructFormpaneControl(cu.Item, UXCCTSTypes.none, UsageType.form);
      if (cu.Item is TextEditControl)
        this.SetHeightOfTextEditControl(cu);
      if (cu.Item is LineSeparator)
      {
        this.contentPanel = new StackPanel();
        this.contentPanel.Orientation = Orientation.Horizontal;
        Brush brush = (Brush) new SolidColorBrush(Color.FromRgb((byte) 44, (byte) 118, (byte) 118));
        Line line = new Line();
        line.Stroke = brush;
        line.X1 = 1.0;
        line.X2 = 780.0;
        line.Y1 = 1.0;
        line.Y2 = 1.0;
        line.HorizontalAlignment = HorizontalAlignment.Left;
        line.VerticalAlignment = VerticalAlignment.Center;
        line.StrokeThickness = 1.0;
        this.contentPanel.Children.Add((UIElement) line);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.contentPanel, 0, 2);
      }
      else
        GridUtil.PlaceElement(this.Grid, this.visualControl, 0, 2);
    }

    private void SetHeightOfTextEditControl(ControlUsage cu)
    {
      TextEditControl textEditControl = cu.Item as TextEditControl;
      VisualTextEditControl visualControl = this.visualControl as VisualTextEditControl;
      if (textEditControl == null || visualControl == null || textEditControl.Rows == 0)
        return;
      visualControl.Rows = textEditControl.Rows;
      this.Grid.RowDefinitions[0].Height = new GridLength((double) textEditControl.Rows * 25.0, GridUnitType.Pixel);
    }

    private void SetFieldWidthOfControls(ControlUsage cu)
    {
      double widthValue = ControlFactory.GetWidthValue(cu.Item.FieldWidth);
      string nonDigitPart = Utilities.GetNonDigitPart(cu.Item.FieldWidth);
      if (nonDigitPart == "%")
      {
        GridUtil.AddColumnDef(this.Grid, widthValue, GridUnitType.Star);
        GridUtil.AddColumnDef(this.Grid, 100.0 - widthValue, GridUnitType.Star);
      }
      else
      {
        if (!(nonDigitPart == "ex") && !(nonDigitPart == "px"))
          return;
        GridUtil.AddColumnDef(this.Grid, widthValue, GridUnitType.Pixel);
      }
    }

    private void SetHeightOfTextEditControl()
    {
      TextEditControl textEditControl = this.ControlUsage.Item as TextEditControl;
      VisualTextEditControl visualControl = this.visualControl as VisualTextEditControl;
      if (textEditControl == null || visualControl == null || textEditControl.Rows == 0)
        return;
      visualControl.Rows = textEditControl.Rows;
      this.Grid.RowDefinitions[0].Height = new GridLength((double) textEditControl.Rows * 25.0, GridUnitType.Pixel);
    }
  }
}
