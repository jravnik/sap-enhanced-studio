﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualLayout
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualLayout : BaseSelectableControl
  {
    private UserControl paneHeaderControl;
    private double headerHeight;

    internal GridLayout Layout
    {
      get
      {
        return this.m_ModelObject as GridLayout;
      }
    }

    public override string SelectionText
    {
      get
      {
        return this.Layout.TypeName;
      }
    }

    public VisualLayout(IModelObject layout)
      : base(layout, true)
    {
      this.IsDraggable = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.headerHeight = SkinConstants.HEIGHT_CONTAINER_HEADER;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.Margin = new Thickness(1.0);
        this.Padding = new Thickness(1.0);
        if (!this.Layout.UseGridLayoutSnippet)
        {
          int num = 0;
          if (this.Layout.PrimaryHelpExplanation != null)
            GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
          if (this.Layout.PaneHeader != null && !string.IsNullOrEmpty(this.Layout.PaneHeader.Text))
            GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
          for (int index = 0; index < this.Layout.Rows; ++index)
            GridUtil.AddRowDef(this.Grid, 150.0, 150.0, GridUnitType.Auto);
          for (int index = 0; index < this.Layout.Columns; ++index)
            GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
          if (this.Layout.PrimaryHelpExplanation != null)
            GridUtil.PlaceElement(this.Grid, (UIElement) new TextBlock()
            {
              TextWrapping = TextWrapping.Wrap,
              Text = this.Layout.PrimaryHelpExplanation.Text
            }, num++, 0, 1, this.Layout.Columns);
          if (this.Layout.PaneHeader != null && !string.IsNullOrEmpty(this.Layout.PaneHeader.Text))
          {
            GridUtil.AddRowDef(this.Grid, this.headerHeight, GridUnitType.Pixel);
            this.paneHeaderControl = this.ConstructPaneHeader();
            GridUtil.PlaceElement(this.Grid, (UIElement) this.paneHeaderControl, num++, 0, 1, this.Layout.Columns);
          }
          if (this.Layout.PaneContainers == null)
            return;
          foreach (PaneContainer paneContainer in this.Layout.PaneContainers)
            GridUtil.PlaceElement(this.Grid, (UIElement) new VisualLayoutCell((IModelObject) paneContainer, (BaseVisualControl) this), paneContainer.Row + num, paneContainer.Column, paneContainer.RowSpan, paneContainer.ColSpan);
        }
        else
        {
          GridUtil.AddRowDef(this.Grid, 150.0, 150.0, GridUnitType.Auto);
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
          if (this.Layout.GridLayoutSnippet != null)
          {
            VisualGridLayoutSnippet gridLayoutSnippet = new VisualGridLayoutSnippet((IModelObject) this.Layout.GridLayoutSnippet, (IDesignerControl) this);
            gridLayoutSnippet.HorizontalAlignment = HorizontalAlignment.Center;
            gridLayoutSnippet.VerticalAlignment = VerticalAlignment.Center;
            gridLayoutSnippet.MouseDoubleClick += new MouseButtonEventHandler(this.OnLinkedControlMouseDoubleClick);
            GridUtil.PlaceElement(this.Grid, (UIElement) gridLayoutSnippet, 0, 0);
          }
          else
          {
            TextBlock textBlock = new TextBlock();
            textBlock.TextAlignment = TextAlignment.Center;
            textBlock.HorizontalAlignment = HorizontalAlignment.Center;
            textBlock.VerticalAlignment = VerticalAlignment.Center;
            textBlock.Text = "Drag & Drop the GridLayoutSnippet";
            GridUtil.PlaceElement(this.Grid, (UIElement) textBlock, 0, 0);
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Layout Control UI initialization failed.", ex));
      }
    }

    private UserControl ConstructPaneHeader()
    {
      UserControl userControl;
      switch (this.Layout.PaneHeader.Style)
      {
        case PaneHeaderStyleType.Block:
          userControl = (UserControl) new BlockHeader(this.Layout.PaneHeader.Text);
          break;
        case PaneHeaderStyleType.Underline:
          userControl = (UserControl) new UnderlineHeader(this.Layout.PaneHeader.Text);
          break;
        default:
          userControl = (UserControl) new BlockHeader(this.Layout.PaneHeader.Text);
          break;
      }
      return userControl;
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the newly added Pane Containers.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the removed Pane Containers.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        if (!(e.PropertyName == GridLayout.RowsProperty) && !(e.PropertyName == GridLayout.ColumnsProperty) && (!(e.PropertyName == GridLayout.UseGridLayoutSnippetProperty) && !(e.PropertyName == GridLayout.GridLayoutSnippetProperty)) && (!(e.PropertyName == BasePane.PrimaryHelpExplanationProperty) && !(e.PropertyName == BasePane.PaneHeaderProperty)))
          return;
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to change the Property change.", ex));
      }
    }

    protected override void OnBaseVisualControlDragOver(DragEventArgs e, IModelObject droppedModel)
    {
      if (droppedModel is GridLayout && (droppedModel as GridLayout).IsSnippet)
        e.Effects = DragDropEffects.Copy;
      else
        base.OnBaseVisualControlDragOver(e, droppedModel);
    }

    protected override void OnBaseVisualControlDrop(IModelObject droppedModel)
    {
      if (!(droppedModel is GridLayout))
        return;
      GridLayout gridLayout = droppedModel as GridLayout;
      if (!gridLayout.IsSnippet)
        return;
      this.Layout.GridLayoutSnippet = gridLayout;
    }

    private VisualLayoutCell GetVisualPaneContainer(PaneContainer pContainer)
    {
      VisualLayoutCell visualLayoutCell = (VisualLayoutCell) null;
      foreach (UIElement child in this.Grid.Children)
      {
        visualLayoutCell = child as VisualLayoutCell;
        if (visualLayoutCell != null)
        {
          if (visualLayoutCell.PaneContainer == pContainer)
            break;
        }
      }
      return visualLayoutCell;
    }

    private bool IsVisualLayoutCellPresent(PaneContainer paneContainer)
    {
      foreach (VisualLayoutCell child in this.Grid.Children)
      {
        if (child.PaneContainer == paneContainer)
          return true;
      }
      return false;
    }

    private void OnLinkedControlMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
      if (parentDesigner == null)
        return;
      parentDesigner.OnLinkedControlDoubleClicked(sender, (EventArgs) e);
    }

    internal void DeleteGridSnippet()
    {
      if (!this.Layout.UseGridLayoutSnippet)
        return;
      this.Layout.DeleteGridSnippetReference();
    }

    private void OnSpanRight(object source, EventArgs e)
    {
      VisualLayoutCell visualLayoutCell1 = source as VisualLayoutCell;
      if (visualLayoutCell1 == null)
        return;
      int num1 = (int) visualLayoutCell1.GetValue(Grid.ColumnProperty);
      int num2 = (int) visualLayoutCell1.GetValue(Grid.RowProperty);
      int num3 = (int) visualLayoutCell1.GetValue(Grid.ColumnSpanProperty);
      foreach (UIElement child in this.Grid.Children)
      {
        VisualLayoutCell visualLayoutCell2 = child as VisualLayoutCell;
        if (visualLayoutCell2 != null)
        {
          int num4 = (int) visualLayoutCell2.GetValue(Grid.ColumnProperty);
          int num5 = (int) visualLayoutCell2.GetValue(Grid.RowProperty);
          if (num4 == num1 + 1 && num2 == num5)
          {
            this.Grid.Children.Remove((UIElement) visualLayoutCell2);
            visualLayoutCell1.SetValue(Grid.ColumnSpanProperty, (object) (num3 + 1));
            break;
          }
        }
      }
    }

    private void OnSpanUp(object source, EventArgs e)
    {
    }

    private void OnSpanLeft(object source, EventArgs e)
    {
    }

    private void OnSpanDown(object source, EventArgs e)
    {
      VisualLayoutCell visualLayoutCell1 = source as VisualLayoutCell;
      if (visualLayoutCell1 == null)
        return;
      int num1 = (int) visualLayoutCell1.GetValue(Grid.ColumnProperty);
      int num2 = (int) visualLayoutCell1.GetValue(Grid.RowProperty);
      int num3 = (int) visualLayoutCell1.GetValue(Grid.ColumnSpanProperty);
      foreach (UIElement child in this.Grid.Children)
      {
        VisualLayoutCell visualLayoutCell2 = child as VisualLayoutCell;
        if (visualLayoutCell2 != null)
        {
          int num4 = (int) visualLayoutCell2.GetValue(Grid.ColumnProperty);
          int num5 = (int) visualLayoutCell2.GetValue(Grid.RowProperty);
          if (num4 == num1 && num5 == num2 + 1)
          {
            this.Grid.Children.Remove((UIElement) visualLayoutCell2);
            visualLayoutCell1.SetValue(Grid.RowSpanProperty, (object) (num3 + 1));
            break;
          }
        }
      }
    }
  }
}
