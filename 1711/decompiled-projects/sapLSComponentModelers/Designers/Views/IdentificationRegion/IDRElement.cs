﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.IdentificationRegion.IDRElement
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.IdentificationRegion2
{
  public class IDRElement : BaseBorderControl
  {
    private const string RESOURCE_BACKGROUND = "#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0";
    private const string DELETECOMMAND = "Delete";
    private const string TEXTFONT = "Arial,11,Normal";
    private TextBlock label;
    private TextBlock value;

    internal ControlUsage IDRModel
    {
      get
      {
        return this.m_ModelObject as ControlUsage;
      }
    }

    public IDRElement(IModelObject ticketModel)
      : base(ticketModel)
    {
      this.AllowDrop = true;
      this.label = new TextBlock();
      this.label.Text = this.IDRModel.Label != null ? this.IDRModel.Label.Text : (string) null;
      FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.label);
      this.label.FontWeight = FontWeights.Bold;
      this.value = new TextBlock();
      FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.value);
      this.value.Text = string.Empty;
      this.LoadView();
    }

    public override void LoadView()
    {
      this.Padding = new Thickness(2.0);
      this.ControlBackground = (Brush) ResourceUtil.GetLinearGradientBrush("#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0");
      GridUtil.AddRowDef(this.Grid, 20.0, GridUnitType.Pixel);
      this.label.SetValue(FrameworkElement.VerticalAlignmentProperty, (object) VerticalAlignment.Center);
      this.label.SetValue(FrameworkElement.HorizontalAlignmentProperty, (object) HorizontalAlignment.Left);
      GridUtil.AddColumnDef(this.Grid, 70.0, 70.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.label, 0, 0);
      this.value.SetValue(FrameworkElement.VerticalAlignmentProperty, (object) VerticalAlignment.Center);
      this.value.SetValue(FrameworkElement.HorizontalAlignmentProperty, (object) HorizontalAlignment.Right);
      this.value.Padding = new Thickness(10.0, 0.0, 0.0, 0.0);
      GridUtil.AddColumnDef(this.Grid, 80.0, 80.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.value, 0, 1);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) "Delete")
        return;
      ExtendedIdentificationRegion parent = this.IDRModel.Parent as ExtendedIdentificationRegion;
      if (parent == null)
        return;
      parent.RemoveExtendedIdentification(this.IDRModel);
    }

    protected override void OnBaseVisualControlDrop(IModelObject droppedModel)
    {
      try
      {
        IDRElement idrElement = droppedModel as IDRElement;
        if (idrElement == null)
          return;
        WrapPanel parent1 = this.Parent as WrapPanel;
        int index1 = parent1.Children.IndexOf((UIElement) this);
        int index2 = parent1.Children.IndexOf((UIElement) idrElement);
        ExtendedIdentificationRegion parent2 = this.IDRModel.Parent as ExtendedIdentificationRegion;
        parent2.ChangeFieldIndex(this.IDRModel, index2);
        parent2.ChangeFieldIndex(idrElement.IDRModel, index1);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("IDR swap event Failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "IDR";
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName.Equals(ControlUsage.LabelProperty))
      {
        this.label.Text = this.IDRModel.Label != null ? this.IDRModel.Label.Text : string.Empty;
      }
      else
      {
        if (!e.PropertyName.Equals(ControlUsage.ValueProperty))
          return;
        this.value.Text = e.NewValue != null ? e.NewValue.ToString() : string.Empty;
      }
    }
  }
}
