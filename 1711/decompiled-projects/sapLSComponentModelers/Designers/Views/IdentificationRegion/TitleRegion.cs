﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.IdentificationRegion.TitleRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure;
using System;
using System.Windows;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.IdentificationRegion2
{
  public class TitleRegion : BaseSelectableControl
  {
    private const string RESOURCE_BACKGROUND = "#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0";
    private const string RESOURCE_TITLE_FONT = "textview.pageheadertitle.font";
    private DisplayTextControl title;

    internal OberonCenterStructure centerStrucuture
    {
      get
      {
        return this.m_ModelObject as OberonCenterStructure;
      }
    }

    public TitleRegion(IModelObject titleRegion)
      : base(titleRegion, false, false)
    {
      this.title = this.centerStrucuture == null || this.centerStrucuture.Title == null ? new DisplayTextControl(TextViewStyles.PageHeaderTitle, "") : new DisplayTextControl(TextViewStyles.PageHeaderTitle, this.centerStrucuture.Title.Text);
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.Padding = new Thickness(1.0);
        this.ControlBackground = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 35.0, GridUnitType.Pixel);
        this.title.Background = (Brush) ResourceUtil.GetLinearGradientBrush("#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0");
        this.title.Padding = new Thickness(5.0, 0.0, 0.0, 0.0);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.title, 0, 0, 1, 3);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Title Region UI initialization failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Title Region";
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (!(e.PropertyName == "Title"))
        return;
      this.title.Text = e.NewValue != null ? e.NewValue.ToString() : (string) null;
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
    }
  }
}
