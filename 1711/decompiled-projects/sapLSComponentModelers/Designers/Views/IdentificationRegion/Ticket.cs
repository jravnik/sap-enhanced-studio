﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.IdentificationRegion.Ticket
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.IdentificationRegion2
{
  public class Ticket : BaseBorderControl
  {
    private const string RESOURCE_BACKGROUND = "#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0";
    private const string DELETECOMMAND = "Delete";
    private const string TEXTFONT = "Arial,11,Normal";
    private TextBlock label;

    internal ControlUsage TicketModel
    {
      get
      {
        return this.m_ModelObject as ControlUsage;
      }
    }

    public Ticket(IModelObject ticketModel)
      : base(ticketModel)
    {
      this.AllowDrop = true;
      this.label = new TextBlock();
      this.label.Text = this.TicketModel.Label != null ? this.TicketModel.Label.Text : (string) null;
      FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.label);
      this.label.FontWeight = FontWeights.Bold;
      this.LoadView();
    }

    public override void LoadView()
    {
      this.Padding = new Thickness(1.0);
      this.ControlBackground = (Brush) ResourceUtil.GetLinearGradientBrush("#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0");
      GridUtil.AddRowDef(this.Grid, 20.0, GridUnitType.Pixel);
      this.label.SetValue(FrameworkElement.VerticalAlignmentProperty, (object) VerticalAlignment.Center);
      this.label.SetValue(FrameworkElement.HorizontalAlignmentProperty, (object) HorizontalAlignment.Left);
      GridUtil.AddColumnDef(this.Grid, 150.0, 150.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.label, 0, 0);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) "Delete")
        return;
      TicketArea parent = this.TicketModel.Parent as TicketArea;
      if (parent == null)
        return;
      parent.RemoveField(this.TicketModel);
    }

    public override string SelectionText
    {
      get
      {
        return nameof (Ticket);
      }
    }

    protected override void OnDragEnter(DragEventArgs e)
    {
      if (e.Data.GetData(typeof (Ticket)) != null)
        e.Effects = e.AllowedEffects;
      else
        e.Effects = DragDropEffects.None;
    }

    protected override void OnBaseVisualControlDrop(IModelObject droppedModel)
    {
      try
      {
        Ticket ticket = droppedModel as Ticket;
        if (ticket == null)
          return;
        WrapPanel parent1 = this.Parent as WrapPanel;
        int index1 = parent1.Children.IndexOf((UIElement) this);
        int index2 = parent1.Children.IndexOf((UIElement) ticket);
        TicketArea parent2 = this.TicketModel.Parent as TicketArea;
        parent2.ChangeFieldIndex(this.TicketModel, index2);
        parent2.ChangeFieldIndex(ticket.TicketModel, index1);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Ticket swap event Failed.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (!e.PropertyName.Equals(ControlUsage.LabelProperty))
        return;
      this.label.Text = e.NewValue != null ? e.NewValue.ToString() : string.Empty;
    }
  }
}
