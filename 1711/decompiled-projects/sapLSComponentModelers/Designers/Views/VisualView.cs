﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualView
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualView : BaseSelectableControl
  {
    private ScrollViewer scrollViewer;
    private Grid visualLayoutCellGrid;
    private TextBlock primaryExplanationText;

    internal View ViewModel
    {
      get
      {
        return this.m_ModelObject as View;
      }
    }

    public VisualView(IModelObject view)
      : base(view)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override ScrollViewer ScrollViewer
    {
      get
      {
        return this.scrollViewer;
      }
    }

    public override string SelectionText
    {
      get
      {
        return "View";
      }
    }

    public override void LoadView()
    {
      try
      {
        this.Margin = new Thickness(1.0);
        this.Padding = new Thickness(1.0);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        if (this.ViewModel.PrimaryHelpExplanation != null && !string.IsNullOrEmpty(this.ViewModel.PrimaryHelpExplanation.Text))
        {
          this.primaryExplanationText = this.ConstructPrimaryHelpTextHolder();
          this.primaryExplanationText.Background = (Brush) ComponentModelersConstants.CONTENT_AREA_BRUSH;
          GridUtil.PlaceElement(this.Grid, (UIElement) this.primaryExplanationText, 0, 0);
          this.primaryExplanationText.Text = this.ViewModel.PrimaryHelpExplanation.Text.Replace("<br/>", "\r\n");
        }
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
        this.scrollViewer = new ScrollViewer();
        this.scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
        this.scrollViewer.CanContentScroll = true;
        this.scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
        this.visualLayoutCellGrid = new Grid();
        this.Margin = new Thickness(1.0);
        this.Padding = new Thickness(1.0);
        for (int index = 0; index < this.ViewModel.Rows; ++index)
          GridUtil.AddRowDef(this.visualLayoutCellGrid, 150.0, 150.0, GridUnitType.Auto);
        for (int index = 0; index < this.ViewModel.Columns; ++index)
          GridUtil.AddColumnDef(this.visualLayoutCellGrid, 1.0, GridUnitType.Star);
        foreach (PaneContainer paneContainer in this.ViewModel.PaneContainers)
          GridUtil.PlaceElement(this.visualLayoutCellGrid, (UIElement) new VisualLayoutCell((IModelObject) paneContainer, (BaseVisualControl) this), paneContainer.Row, paneContainer.Column, paneContainer.RowSpan, paneContainer.ColSpan);
        this.scrollViewer.Content = (object) this.visualLayoutCellGrid;
        GridUtil.PlaceElement(this.Grid, (UIElement) this.scrollViewer, 1, 0);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Layout Control UI initialization failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
        ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
        PaneContainerAddedEventArgs containerAddedEventArgs = e as PaneContainerAddedEventArgs;
        if (containerAddedEventArgs == null)
          return;
        BaseSelectableControl designerControl = parentDesigner.DesignerControlDictionary[containerAddedEventArgs.ReferenceContainer.Id] as BaseSelectableControl;
        parentDesigner.OnDesignerControlSelectionChange((object) designerControl, designerControl.Model);
        designerControl.BringIntoView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the newly added Pane Containers.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
        ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
        PaneContainerRemovedEventArgs removedEventArgs = e as PaneContainerRemovedEventArgs;
        if (removedEventArgs == null)
          return;
        BaseSelectableControl designerControl = parentDesigner.DesignerControlDictionary[removedEventArgs.ReferenceContainer.Id] as BaseSelectableControl;
        parentDesigner.OnDesignerControlSelectionChange((object) designerControl, designerControl.Model);
        designerControl.BringIntoView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the removed Pane Containers.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        if (e.PropertyName == GridLayout.RowsProperty || e.PropertyName == GridLayout.ColumnsProperty)
          this.ReloadView();
        if (!(e.PropertyName == SAP.BYD.LS.UIDesigner.Model.Entities.Controls.View.ExplanationAreaText) && !(e.PropertyName == SAP.BYD.LS.UIDesigner.Model.Entities.Controls.View.ExplanationArea))
          return;
        if (this.ViewModel.PrimaryHelpExplanation != null && !string.IsNullOrEmpty(this.ViewModel.PrimaryHelpExplanation.Text))
        {
          if (this.primaryExplanationText == null)
            this.primaryExplanationText = this.ConstructPrimaryHelpTextHolder();
          if (!this.Grid.Children.Contains((UIElement) this.primaryExplanationText))
            GridUtil.PlaceElement(this.Grid, (UIElement) this.primaryExplanationText, 0, 0);
          this.primaryExplanationText.Text = this.ViewModel.PrimaryHelpExplanation.Text.Replace("<br/>", "\r\n");
        }
        else
        {
          if (this.primaryExplanationText == null || !this.Grid.Children.Contains((UIElement) this.primaryExplanationText))
            return;
          this.Grid.Children.Remove((UIElement) this.primaryExplanationText);
          this.primaryExplanationText = (TextBlock) null;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to change the Property change.", ex));
      }
    }

    private TextBlock ConstructPrimaryHelpTextHolder()
    {
      return new TextBlock()
      {
        TextWrapping = TextWrapping.Wrap
      };
    }

    private VisualLayoutCell GetVisualPaneContainer(PaneContainer pContainer)
    {
      VisualLayoutCell visualLayoutCell = (VisualLayoutCell) null;
      foreach (UIElement child in this.visualLayoutCellGrid.Children)
      {
        visualLayoutCell = child as VisualLayoutCell;
        if (visualLayoutCell != null)
        {
          if (visualLayoutCell.PaneContainer == pContainer)
            break;
        }
      }
      return visualLayoutCell;
    }

    private bool IsVisualLayoutCellPresent(PaneContainer paneContainer)
    {
      foreach (VisualLayoutCell child in this.visualLayoutCellGrid.Children)
      {
        if (child.PaneContainer == paneContainer)
          return true;
      }
      return false;
    }
  }
}
