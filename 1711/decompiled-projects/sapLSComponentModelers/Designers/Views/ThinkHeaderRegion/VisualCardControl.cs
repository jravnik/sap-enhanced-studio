﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ThinkHeaderRegion.VisualCardControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ThinkHeaderRegion
{
  public class VisualCardControl : BaseVisualControl
  {
    private DisplayTextControl m_PrimaryTextControl;
    private DisplayTextControl m_SecondayTextControl;
    private ControlUsage controlUsageElement;

    private CardControl CardControlModel
    {
      get
      {
        return this.m_ModelObject as CardControl;
      }
    }

    public override string SelectionText
    {
      get
      {
        return string.Empty;
      }
    }

    public VisualCardControl(IModelObject model)
      : base(model)
    {
      IModelObject parent = model.Parent;
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      switch (this.CardControlModel.DisplayMode)
      {
        case CardDislayType.SwatchOnly:
          this.AddSwatchControl();
          break;
        case CardDislayType.CardOnly:
          this.AddTextControls();
          break;
        default:
          this.AddSwatchControl();
          this.AddTextControls();
          break;
      }
    }

    private void AddSwatchControl()
    {
      GridUtil.PlaceElement(this.Grid, (UIElement) ResourceUtil.GetImage("sapLSUISkins;component/dt.images/GIF/Image.gif, 40, 40"), 0, 0);
    }

    private void AddTextControls()
    {
      Grid grid = new Grid();
      grid.Margin = new Thickness(10.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) grid, 0, 1);
      ThingHeaderRegion parent = this.Model.Parent as ThingHeaderRegion;
      GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
      if (parent.IsPrimaryEditEnabled)
      {
        this.controlUsageElement = parent.PrimaryEditControl == null ? new ControlUsage((IModelObject) this.CardControlModel, "InputField") : parent.PrimaryEditControl;
        VisualControlUsage visualControlUsage = new VisualControlUsage((IModelObject) this.controlUsageElement);
        GridUtil.PlaceElement(grid, (UIElement) visualControlUsage, 0, 0);
      }
      else
      {
        this.m_PrimaryTextControl = new DisplayTextControl(TextViewStyles.SectionGroupTitle, this.CardControlModel.PrimaryText != null ? this.CardControlModel.PrimaryText.Text : string.Empty);
        this.m_PrimaryTextControl.Margin = new Thickness(0.0, 5.0, 0.0, 0.0);
        GridUtil.PlaceElement(grid, (UIElement) this.m_PrimaryTextControl, 1, 0);
      }
      if (parent.IsSecondaryEditEnabled)
      {
        this.controlUsageElement = parent.SecondaryEditControl == null ? new ControlUsage((IModelObject) this.CardControlModel, "InputField") : parent.SecondaryEditControl;
        VisualControlUsage visualControlUsage = new VisualControlUsage((IModelObject) this.controlUsageElement);
        GridUtil.PlaceElement(grid, (UIElement) visualControlUsage, 2, 0);
      }
      else
      {
        this.m_SecondayTextControl = new DisplayTextControl(TextViewStyles.SectionGroupTitle, this.CardControlModel.SecondaryText != null ? this.CardControlModel.SecondaryText.Text : string.Empty);
        this.m_SecondayTextControl.Margin = new Thickness(0.0, 5.0, 0.0, 0.0);
        GridUtil.PlaceElement(grid, (UIElement) this.m_SecondayTextControl, 3, 0);
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        IModelObject parent = this.Model.Parent;
        this.ReloadView();
        if (e.PropertyName == CardControl.PrimaryTextProperty)
        {
          if (this.m_PrimaryTextControl == null)
            return;
          this.m_PrimaryTextControl.Text = e.NewValue != null ? e.NewValue.ToString() : (string) null;
        }
        else
        {
          if (!(e.PropertyName == CardControl.SecondaryTextProperty) || this.m_SecondayTextControl == null)
            return;
          this.m_SecondayTextControl.Text = e.NewValue != null ? e.NewValue.ToString() : (string) null;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle ModelPropertyChanged event of Card Control", ex));
      }
    }
  }
}
