﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualHeaderRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualHeaderRegion : BaseSelectableControl
  {
    private System.Windows.Controls.Image logo = ResourceUtil.GetImage("sapLSUISkins;component/dt.images/GIF/Image.gif, 50, 50");
    private const string RESOURCE_BACKGROUND = "#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0";
    private DisplayTextControl title;

    public override string SelectionText
    {
      get
      {
        return "Header Region";
      }
    }

    internal PrintHeaderRegion PrintHeader
    {
      get
      {
        return this.m_ModelObject as PrintHeaderRegion;
      }
    }

    public VisualHeaderRegion(IModelObject visualHeadermodel)
      : base(visualHeadermodel)
    {
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      if (this.PrintHeader == null)
        return;
      this.AddTitle();
      this.AddLogo();
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        PrintHeaderRegion printHeaderRegion = sender as PrintHeaderRegion;
        if (printHeaderRegion != null)
        {
          if (e.PropertyName == PrintHeaderRegion.PrintHeaderTitleProperty)
            this.title.Text = printHeaderRegion.Title != null ? printHeaderRegion.Title.Text : (string) null;
          if (e.PropertyName == PrintHeaderRegion.PrintAlignmentProperty)
          {
            if (printHeaderRegion.Alignment == PrintAlignmentType.left)
              this.logo.HorizontalAlignment = HorizontalAlignment.Left;
            else if (printHeaderRegion.Alignment == PrintAlignmentType.center)
              this.logo.HorizontalAlignment = HorizontalAlignment.Center;
            else
              this.logo.HorizontalAlignment = HorizontalAlignment.Right;
          }
        }
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle ModelPropertyChanged event of Header Region", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      base.OnModelObjectAdded(sender, e);
    }

    private void AddTitle()
    {
      this.title = new DisplayTextControl(TextViewStyles.PageHeaderTitle, this.PrintHeader.Title != null ? this.PrintHeader.Title.Text : string.Empty);
      this.title.Background = (Brush) ResourceUtil.GetLinearGradientBrush("#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0");
      this.title.Padding = new Thickness(5.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.title, 0, 0);
    }

    private void AddLogo()
    {
      GridUtil.PlaceElement(this.Grid, (UIElement) this.logo, 0, 1);
    }
  }
}
