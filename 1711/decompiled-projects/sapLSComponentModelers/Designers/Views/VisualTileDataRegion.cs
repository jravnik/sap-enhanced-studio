﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualTileDataRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualTileDataRegion : BaseSelectableControl
  {
    private TileDataRegion TileDataRegion
    {
      get
      {
        return this.m_ModelObject as TileDataRegion;
      }
    }

    public override string SelectionText
    {
      get
      {
        return "TileDataRegion";
      }
    }

    public VisualTileDataRegion(IModelObject model)
      : base(model)
    {
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) ResourceUtil.GetImage("sapLSUISkins;component/dt.images/PNG/Tile.png, 205, 235"), 0, 0);
    }
  }
}
