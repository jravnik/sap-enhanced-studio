﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.TicketRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.IdentificationRegion2;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class TicketRegion : BaseSelectableControl
  {
    private const string TICKET = "Ticket";
    private WrapPanel wrapPanel;

    internal TicketArea TicketCollection
    {
      get
      {
        return this.m_ModelObject as TicketArea;
      }
    }

    public TicketRegion(TicketArea ticketColl)
      : base((IModelObject) ticketColl)
    {
      this.wrapPanel = new WrapPanel();
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.Padding = new Thickness(1.0);
        this.Margin = new Thickness(1.0);
        this.ControlBackground = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        this.wrapPanel.Orientation = Orientation.Vertical;
        this.wrapPanel.ItemHeight = 20.0;
        this.wrapPanel.ClipToBounds = true;
        this.wrapPanel.Children.Clear();
        foreach (IModelObject ticket in this.TicketCollection.Tickets)
          this.wrapPanel.Children.Add((UIElement) new Ticket(ticket));
        this.Grid.Children.Add((UIElement) this.wrapPanel);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading ticket area failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Ticket Region";
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      IModelObject addedModel = e.AddedModel;
      if (addedModel is ControlUsage)
      {
        Ticket ticket = new Ticket((IModelObject) (addedModel as ControlUsage));
        this.wrapPanel.Children.Insert(e.PositionInParent, (UIElement) ticket);
      }
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      if (e.RemovedModel == null)
        return;
      foreach (UIElement child in this.wrapPanel.Children)
      {
        Ticket ticket = child as Ticket;
        if (ticket != null && ticket.TicketModel == e.RemovedModel)
        {
          ticket.Terminate();
          this.wrapPanel.Children.Remove((UIElement) ticket);
          break;
        }
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (!(e.PropertyName == TicketArea.TicketsProperty))
        return;
      this.ReloadView();
    }

    public void AddTicket(IModelObject ticket)
    {
      try
      {
        this.wrapPanel.Children.Add((UIElement) new Ticket(ticket));
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Adding ticket failed.", ex));
      }
    }
  }
}
