﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualFooterRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualFooterRegion : BaseSelectableControl
  {
    private static readonly Brush tableBackgroundBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 212, (byte) 217, (byte) 222));
    private VisualLayout footerContentRegion;

    public override string SelectionText
    {
      get
      {
        return "Footer Region";
      }
    }

    internal PrintFooterRegion PrintFooter
    {
      get
      {
        return this.m_ModelObject as PrintFooterRegion;
      }
    }

    public VisualFooterRegion(IModelObject visualFootermodel)
      : base(visualFootermodel)
    {
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      if (this.PrintFooter == null)
        return;
      this.AddFooterContentRegion();
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      this.ReloadView();
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      base.OnModelObjectAdded(sender, e);
    }

    private void AddFooterContentRegion()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      ScrollViewer scrollViewer = new ScrollViewer();
      scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
      scrollViewer.CanContentScroll = true;
      scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
      scrollViewer.Background = VisualFooterRegion.tableBackgroundBrush;
      scrollViewer.BorderThickness = new Thickness(0.0);
      this.footerContentRegion = new VisualLayout((IModelObject) this.PrintFooter.Content);
      scrollViewer.Content = (object) this.footerContentRegion;
      GridUtil.PlaceElement(this.Grid, (UIElement) scrollViewer, 0, 0);
    }
  }
}
