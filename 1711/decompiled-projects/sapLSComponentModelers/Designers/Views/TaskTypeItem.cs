﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.TaskTypeItem
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class TaskTypeItem
  {
    private string m_Name;
    private string m_Description;
    private object m_Tag;
    private TaskType m_TaType;

    public string Name
    {
      get
      {
        return this.m_Name;
      }
      set
      {
        this.m_Name = value;
      }
    }

    public string Description
    {
      get
      {
        return this.m_Description;
      }
      set
      {
        this.m_Description = value;
      }
    }

    public object Tag
    {
      get
      {
        return this.m_Tag;
      }
      set
      {
        this.m_Tag = value;
      }
    }

    public TaskType TaType
    {
      get
      {
        return this.m_TaType;
      }
      set
      {
        this.m_TaType = value;
      }
    }
  }
}
