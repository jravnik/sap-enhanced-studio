﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion.VisualQAFContextualNavigationRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion
{
  public class VisualQAFContextualNavigationRegion : VisualContextualNavigation
  {
    private QuickActivityContextualNavigationRegion QuickActivityContextualNavigationRegion
    {
      get
      {
        return this.m_ModelObject as QuickActivityContextualNavigationRegion;
      }
    }

    public VisualQAFContextualNavigationRegion(IModelObject model)
      : base(model)
    {
    }

    public override void LoadView()
    {
      try
      {
        this.Padding = new Thickness(1.0);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 25.0, GridUnitType.Auto);
        this.tbModel = this.QuickActivityContextualNavigationRegion.Functionbar;
        this.tbControl = new VisualToolbar((IModelObject) this.tbModel);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.tbControl, 0, 0, 1, 2);
        if (this.QuickActivityContextualNavigationRegion.ViewAll != null)
        {
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          VisualMenuButton visualMenuButton = new VisualMenuButton((IModelObject) this.QuickActivityContextualNavigationRegion.ViewAll);
          if (this.QuickActivityContextualNavigationRegion.YouCanAlsoNavigation != null)
          {
            GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
            GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNavigationElement((IModelObject) this.QuickActivityContextualNavigationRegion.YouCanAlsoNavigation), 0, 2);
            GridUtil.PlaceElement(this.Grid, (UIElement) visualMenuButton, 0, 3);
          }
          else
            GridUtil.PlaceElement(this.Grid, (UIElement) visualMenuButton, 0, 2, 1, 1);
        }
        else
        {
          if (this.QuickActivityContextualNavigationRegion.YouCanAlsoNavigation == null)
            return;
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNavigationElement((IModelObject) this.QuickActivityContextualNavigationRegion.YouCanAlsoNavigation), 0, 2);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("QAFContextualNavigation Layout Control failed.", ex));
      }
    }
  }
}
