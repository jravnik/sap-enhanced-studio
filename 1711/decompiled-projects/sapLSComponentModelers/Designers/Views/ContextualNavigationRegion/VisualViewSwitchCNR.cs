﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion.VisualViewSwitchCNR
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion
{
  public class VisualViewSwitchCNR : VisualContextualNavigation
  {
    private ViewSwitchContextualNavigationRegion ViewSwitchContextualNavigationRegion
    {
      get
      {
        return this.m_ModelObject as ViewSwitchContextualNavigationRegion;
      }
    }

    public VisualViewSwitchCNR(IModelObject model)
      : base(model)
    {
    }

    public override void LoadView()
    {
      try
      {
        this.Padding = new Thickness(1.0);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 25.0, GridUnitType.Auto);
        this.tbModel = this.ViewSwitchContextualNavigationRegion.Functionbar;
        this.tbControl = new VisualToolbar((IModelObject) this.tbModel);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.tbControl, 0, 0, 1, 2);
        if (this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation != null && this.ViewSwitchContextualNavigationRegion.RelatedLinks != null && this.ViewSwitchContextualNavigationRegion.BasicView != null)
        {
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNavigationElement((IModelObject) this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation), 0, 2);
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNavigationElement((IModelObject) this.ViewSwitchContextualNavigationRegion.RelatedLinks), 0, 3);
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualMenuButton((IModelObject) this.ViewSwitchContextualNavigationRegion.BasicView), 0, 4);
        }
        else if (this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation != null && this.ViewSwitchContextualNavigationRegion.BasicView != null && this.ViewSwitchContextualNavigationRegion.RelatedLinks == null)
        {
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNavigationElement((IModelObject) this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation), 0, 2);
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualMenuButton((IModelObject) this.ViewSwitchContextualNavigationRegion.BasicView), 0, 3);
        }
        else if (this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation != null && this.ViewSwitchContextualNavigationRegion.RelatedLinks != null && this.ViewSwitchContextualNavigationRegion.BasicView == null)
        {
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNavigationElement((IModelObject) this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation), 0, 2);
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNavigationElement((IModelObject) this.ViewSwitchContextualNavigationRegion.RelatedLinks), 0, 3);
        }
        else if (this.ViewSwitchContextualNavigationRegion.BasicView != null && this.ViewSwitchContextualNavigationRegion.RelatedLinks != null && this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation == null)
        {
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNavigationElement((IModelObject) this.ViewSwitchContextualNavigationRegion.RelatedLinks), 0, 2);
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualMenuButton((IModelObject) this.ViewSwitchContextualNavigationRegion.BasicView), 0, 3);
        }
        else if (this.ViewSwitchContextualNavigationRegion.BasicView != null && this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation == null && this.ViewSwitchContextualNavigationRegion.RelatedLinks == null)
        {
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualMenuButton((IModelObject) this.ViewSwitchContextualNavigationRegion.BasicView), 0, 2);
        }
        else if (this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation == null && this.ViewSwitchContextualNavigationRegion.RelatedLinks != null && this.ViewSwitchContextualNavigationRegion.BasicView == null)
        {
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNavigationElement((IModelObject) this.ViewSwitchContextualNavigationRegion.RelatedLinks), 0, 2);
        }
        else
        {
          if (this.ViewSwitchContextualNavigationRegion.RelatedLinks != null || this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation == null || this.ViewSwitchContextualNavigationRegion.BasicView != null)
            return;
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNavigationElement((IModelObject) this.ViewSwitchContextualNavigationRegion.YouCanAlsoNavigation), 0, 2);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("ViewSwitchContextualNavigation Layout Control failed.", ex));
      }
    }
  }
}
