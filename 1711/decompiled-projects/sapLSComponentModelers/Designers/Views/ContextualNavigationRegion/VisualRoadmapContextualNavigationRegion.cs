﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion.VisualRoadmapContextualNavigationRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion
{
  public class VisualRoadmapContextualNavigationRegion : VisualContextualNavigation
  {
    private VisualRoadmap roadmapControl;

    private RoadmapContextualNavigationRegion RoadmapContextualNavigationRegion
    {
      get
      {
        return this.m_ModelObject as RoadmapContextualNavigationRegion;
      }
    }

    public VisualRoadmap RoadmapControl
    {
      get
      {
        return this.roadmapControl;
      }
    }

    public VisualRoadmapContextualNavigationRegion(IModelObject model)
      : base(model)
    {
    }

    public override void LoadView()
    {
      try
      {
        this.Padding = new Thickness(1.0);
        GridUtil.AddRowDef(this.Grid, 25.0, GridUnitType.Auto);
        GridUtil.AddRowDef(this.Grid, 25.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        this.roadmapControl = new VisualRoadmap(this.RoadmapContextualNavigationRegion.RoadmapNavigation);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.roadmapControl, 0, 0);
        this.tbControl = new VisualToolbar((IModelObject) this.RoadmapContextualNavigationRegion.Functionbar);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.tbControl, 1, 0);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("RoadMapContextualNavigation Layout Control failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      int index = this.roadmapControl.SelectedStep.Index;
      this.ReloadView();
      this.roadmapControl.SelectStep(index);
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      int index = this.roadmapControl.SelectedStep.Index;
      this.ReloadView();
      this.roadmapControl.SelectStep(index);
      base.OnModelObjectRemoved(sender, e);
    }
  }
}
