﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion.VisualRoadmap
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion
{
  public class VisualRoadmap : BaseSelectableControl
  {
    internal const string CONTEXTMENU_ADDSTEP = "Add Step";
    internal const string CONTEXTMENU_ADDSTEPVARIANT = "Add StepVariant";
    internal const string CONTEXTMENU_DELETESTEP = "Delete Step";
    internal const string CONTEXTMENU_DELETESTEPVARIANT = "Delete StepVariant";
    private static ImageInfo imgBorderLeft;
    private static ImageInfo imgBorderRight;
    private static ImageInfo imgBackgroundFuture;
    private int selectedIndex;
    private int selectedChildIndex;
    private VisualRoadmapItem selectedStep;
    private List<VisualRoadmapItem> steps;

    private RoadmapNavigation RoadmapNavigation
    {
      get
      {
        return this.m_ModelObject as RoadmapNavigation;
      }
    }

    private RoadmapContextualNavigationRegion RoadmapContextualNavigationRegion
    {
      get
      {
        return this.RoadmapNavigation.Parent as RoadmapContextualNavigationRegion;
      }
    }

    public int SelectedIndex
    {
      get
      {
        return this.selectedIndex;
      }
      set
      {
        this.selectedIndex = value;
        this.SelectStep(this.selectedIndex);
      }
    }

    public int SelectedChildIndex
    {
      get
      {
        return this.selectedChildIndex;
      }
      set
      {
        this.selectedChildIndex = value;
        VisualRoadmapItem step = this.steps[this.selectedIndex];
        if (step == null)
          return;
        step.SelectedChildIndex = this.selectedChildIndex;
      }
    }

    public VisualRoadmapItem SelectedStep
    {
      get
      {
        return this.selectedStep;
      }
    }

    private void InitializeResources()
    {
      VisualRoadmap.imgBorderLeft = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Canvas/RoadMap/border_left.png,2,31");
      VisualRoadmap.imgBorderRight = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Canvas/RoadMap/border_right.png,3,31");
      VisualRoadmap.imgBackgroundFuture = ResourceUtil.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/Canvas/RoadMap/background_future.png,1,31");
    }

    private void OnRoadmapSelectionChanged(object sender, RoutedEventArgs e)
    {
      VisualRoadmapItem visualRoadmapItem = sender as VisualRoadmapItem;
      if (visualRoadmapItem == null)
        return;
      this.selectedIndex = visualRoadmapItem.Index;
      this.SelectedChildIndex = 0;
      this.ReloadView();
    }

    public void SelectStep(int stepIndex)
    {
      if (this.steps.Count <= stepIndex)
        return;
      this.steps[stepIndex].SelectMe();
    }

    public VisualRoadmap(RoadmapNavigation roadmapNavigation)
      : base((IModelObject) roadmapNavigation)
    {
      if (VisualRoadmap.imgBorderLeft == null)
        this.InitializeResources();
      GridUtil.AddRowDef(this.Grid, (double) VisualRoadmap.imgBorderLeft.Height, GridUnitType.Pixel);
      this.Content = (object) this.Grid;
      this.steps = new List<VisualRoadmapItem>();
      this.LoadView();
    }

    public override void LoadView()
    {
      this.Grid.Children.Clear();
      this.Grid.RowDefinitions.Clear();
      this.Grid.ColumnDefinitions.Clear();
      this.steps.Clear();
      System.Windows.Controls.Image image1 = ResourceUtil.GetImage(VisualRoadmap.imgBorderLeft);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) image1, 0, 0);
      int index = 0;
      foreach (NavigationItem navigationItem in this.RoadmapNavigation.NavigationItems)
      {
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        VisualRoadmapItem visualRoadmapItem = new VisualRoadmapItem((IModelObject) navigationItem, index, this.selectedIndex);
        GridUtil.PlaceElement(this.Grid, (UIElement) visualRoadmapItem, 0, index + 1);
        visualRoadmapItem.Selected += new RoutedEventHandler(this.OnRoadmapSelectionChanged);
        if (visualRoadmapItem.Index == this.selectedIndex)
          this.selectedStep = visualRoadmapItem;
        this.steps.Add(visualRoadmapItem);
        ++index;
      }
      System.Windows.Controls.Image image2 = ResourceUtil.GetImage(VisualRoadmap.imgBackgroundFuture);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      image2.Stretch = Stretch.Fill;
      image2.Width = double.NaN;
      image2.HorizontalAlignment = HorizontalAlignment.Stretch;
      GridUtil.PlaceElement(this.Grid, (UIElement) image2, 0, index + 1);
      int num1 = index + 1;
      System.Windows.Controls.Image image3 = ResourceUtil.GetImage(VisualRoadmap.imgBorderRight);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) image3, 0, num1 + 1);
      int num2 = num1 + 1;
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      MenuItem parentItem1 = this.AddContextMenuItem("Add");
      this.AddContextMenuItem("Add Step", parentItem1);
      this.AddContextMenuItem("Add StepVariant", parentItem1);
      this.AddSeperatorToContextMenu();
      MenuItem parentItem2 = this.AddContextMenuItem("Delete");
      this.AddContextMenuItem("Delete Step", parentItem2);
      NavigationItem navigationItem = this.RoadmapNavigation.NavigationItems[this.selectedIndex];
      if (navigationItem == null || navigationItem.ChildNavigationItems == null || navigationItem.ChildNavigationItems.Count <= 0)
        return;
      this.AddContextMenuItem("Delete StepVariant", parentItem2);
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (source.Header == (object) "Add Step")
        this.RoadmapContextualNavigationRegion.AddGuidedActivityStep();
      else if (source.Header == (object) "Add StepVariant")
        this.RoadmapContextualNavigationRegion.AddStepVariant(this.RoadmapNavigation.NavigationItems[this.selectedIndex]);
      else if (source.Header == (object) "Delete Step")
        this.RoadmapContextualNavigationRegion.RemoveGuidedActivityStep(this.RoadmapNavigation.NavigationItems[this.selectedIndex]);
      else if (source.Header == (object) "Delete StepVariant")
      {
        NavigationItem navigationItem = this.RoadmapNavigation.NavigationItems[this.selectedIndex];
        if (navigationItem == null)
          return;
        this.RoadmapContextualNavigationRegion.RemoveStepVariant(navigationItem.ChildNavigationItems[this.selectedChildIndex]);
      }
      else
        base.HandleContextMenuItemClick(source);
    }

    public override string SelectionText
    {
      get
      {
        return "Roadmap";
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      int index = this.SelectedStep.Index;
      if (e.PropertyName == Navigation.NavigationItemsProeperty)
        this.ReloadView();
      this.SelectStep(index);
    }
  }
}
