﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion.VisualContextualNavigation
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion
{
  public class VisualContextualNavigation : BaseSelectableControl, IContextMenuContainer
  {
    protected Toolbar tbModel;
    protected VisualToolbar tbControl;

    internal ContextualNavigation ContextNavigation
    {
      get
      {
        return this.m_ModelObject as ContextualNavigation;
      }
    }

    public VisualContextualNavigation(IModelObject navRegion)
      : base(navRegion, false, false)
    {
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.Padding = new Thickness(1.0);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 25.0, GridUnitType.Auto);
        this.tbModel = this.ContextNavigation.Functionbar;
        this.tbControl = new VisualToolbar((IModelObject) this.tbModel);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.tbControl, 0, 0, 1, 2);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("ContextualNavigation Layout Control failed.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        if (e.PropertyName == "UseYouCanAlso" || e.PropertyName == "UseRelatedLinks" || (e.PropertyName == "UseBasicView" || e.PropertyName == "View All"))
          this.ReloadView();
        else if (e.PropertyName == ContextualNavigation.FunctionBarProperty)
        {
          if (this.tbControl != null)
          {
            this.tbControl.Toolbar = this.ContextNavigation.Functionbar;
            this.tbControl.ReloadView();
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update the property changes in the model.", ex));
      }
      base.OnModelPropertyChanged(sender, e);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (this.tbControl == null)
        return;
      this.tbControl.HandleContextMenuItemClickWrapper(source);
    }

    protected override void AddContextMenuItems()
    {
      if (this.tbControl == null)
        return;
      this.tbControl.AddContextMenuItems((IContextMenuContainer) this);
    }

    public override string SelectionText
    {
      get
      {
        return "ContextualNavigation Region";
      }
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
