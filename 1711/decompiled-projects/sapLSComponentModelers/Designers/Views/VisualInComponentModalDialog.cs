﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualInComponentModalDialog
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  internal class VisualInComponentModalDialog : BaseSelectableControl
  {
    private readonly string DELETECOMMAND = "Delete";
    private TextBlock text;

    public ModalDialog InComponentModalDialog
    {
      get
      {
        return this.m_ModelObject as ModalDialog;
      }
    }

    public VisualInComponentModalDialog(IModelObject inComponentModalDialogModel)
      : base(inComponentModalDialogModel)
    {
      this.LoadView();
    }

    public override void LoadView()
    {
      this.Padding = new Thickness(10.0, 0.0, 10.0, 0.0);
      System.Windows.Controls.Image image = new System.Windows.Controls.Image();
      image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.MD48x48);
      image.Stretch = Stretch.None;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) image, 0, 0);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.text = new TextBlock();
      this.text.TextAlignment = TextAlignment.Center;
      this.text.Text = this.InComponentModalDialog.Name;
      GridUtil.PlaceElement(this.Grid, (UIElement) this.text, 1, 0);
    }

    public override string SelectionText
    {
      get
      {
        return "InComponent ModalDialog";
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem(this.DELETECOMMAND);
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) this.DELETECOMMAND)
        return;
      ModalDialogs parent = this.InComponentModalDialog.Parent as ModalDialogs;
      if (parent == null)
        return;
      parent.RemoveModalDialog(this.InComponentModalDialog);
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == ModelObject.NameProperty)
      {
        this.text.Text = e.NewValue.ToString();
      }
      else
      {
        int num = e.PropertyName == "FunctionBar" ? 1 : 0;
      }
    }

    private void OnClick(object sender, MouseButtonEventArgs e)
    {
      if (e.ClickCount <= 1)
        return;
      object tag = (sender is System.Windows.Controls.Image || sender is TextBlock ? (FrameworkElement) (((FrameworkElement) sender).Parent as StackPanel) : (FrameworkElement) (sender as StackPanel)).Tag;
    }
  }
}
