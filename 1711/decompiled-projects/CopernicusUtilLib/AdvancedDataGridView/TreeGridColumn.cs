﻿// Decompiled with JetBrains decompiler
// Type: AdvancedDataGridView.TreeGridColumn
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Drawing;
using System.Windows.Forms;

namespace AdvancedDataGridView
{
  public class TreeGridColumn : DataGridViewTextBoxColumn
  {
    internal Image _defaultNodeImage;

    public TreeGridColumn()
    {
      this.CellTemplate = (DataGridViewCell) new TreeGridCell();
    }

    public override object Clone()
    {
      TreeGridColumn treeGridColumn = (TreeGridColumn) base.Clone();
      treeGridColumn._defaultNodeImage = this._defaultNodeImage;
      return (object) treeGridColumn;
    }

    public Image DefaultNodeImage
    {
      get
      {
        return this._defaultNodeImage;
      }
      set
      {
        this._defaultNodeImage = value;
      }
    }
  }
}
