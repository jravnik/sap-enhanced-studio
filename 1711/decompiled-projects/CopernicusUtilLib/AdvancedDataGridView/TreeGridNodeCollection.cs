﻿// Decompiled with JetBrains decompiler
// Type: AdvancedDataGridView.TreeGridNodeCollection
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace AdvancedDataGridView
{
  public class TreeGridNodeCollection : IList<TreeGridNode>, ICollection<TreeGridNode>, IEnumerable<TreeGridNode>, IList, ICollection, IEnumerable
  {
    internal List<TreeGridNode> _list;
    internal TreeGridNode _owner;

    internal TreeGridNodeCollection(TreeGridNode owner)
    {
      this._owner = owner;
      this._list = new List<TreeGridNode>();
    }

    public void Add(TreeGridNode item)
    {
      item._grid = this._owner._grid;
      bool hasChildren = this._owner.HasChildren;
      item._owner = this;
      this._list.Add(item);
      this._owner.AddChildNode(item);
      if (hasChildren || !this._owner.IsSited)
        return;
      this._owner._grid.InvalidateRow(this._owner.RowIndex);
    }

    public TreeGridNode Add(string text)
    {
      TreeGridNode treeGridNode = new TreeGridNode();
      this.Add(treeGridNode);
      treeGridNode.Cells[0].Value = (object) text;
      return treeGridNode;
    }

    public TreeGridNode Add(params object[] values)
    {
      TreeGridNode treeGridNode = new TreeGridNode();
      this.Add(treeGridNode);
      int index = 0;
      if (values.Length > treeGridNode.Cells.Count)
        return treeGridNode;
      foreach (object obj in values)
      {
        treeGridNode.Cells[index].Value = obj;
        ++index;
      }
      return treeGridNode;
    }

    public void Insert(int index, TreeGridNode item)
    {
      item._grid = this._owner._grid;
      item._owner = this;
      this._list.Insert(index, item);
      this._owner.InsertChildNode(index, item);
    }

    public bool Remove(TreeGridNode item)
    {
      this._owner.RemoveChildNode(item);
      item._grid = (TreeGridView) null;
      return this._list.Remove(item);
    }

    public void RemoveAt(int index)
    {
      TreeGridNode node = this._list[index];
      this._owner.RemoveChildNode(node);
      node._grid = (TreeGridView) null;
      this._list.RemoveAt(index);
    }

    public void Clear()
    {
      this._owner.ClearNodes();
      this._list.Clear();
    }

    public int IndexOf(TreeGridNode item)
    {
      return this._list.IndexOf(item);
    }

    public TreeGridNode this[int index]
    {
      get
      {
        return this._list[index];
      }
      set
      {
        throw new Exception("The method or operation is not implemented.");
      }
    }

    public bool Contains(TreeGridNode item)
    {
      return this._list.Contains(item);
    }

    public void CopyTo(TreeGridNode[] array, int arrayIndex)
    {
      throw new Exception("The method or operation is not implemented.");
    }

    public int Count
    {
      get
      {
        return this._list.Count;
      }
    }

    public bool IsReadOnly
    {
      get
      {
        return false;
      }
    }

    void IList.Remove(object value)
    {
      this.Remove(value as TreeGridNode);
    }

    int IList.Add(object value)
    {
      TreeGridNode treeGridNode = value as TreeGridNode;
      this.Add(treeGridNode);
      return treeGridNode.Index;
    }

    void IList.RemoveAt(int index)
    {
      this.RemoveAt(index);
    }

    void IList.Clear()
    {
      this.Clear();
    }

    bool IList.IsReadOnly
    {
      get
      {
        return this.IsReadOnly;
      }
    }

    bool IList.IsFixedSize
    {
      get
      {
        return false;
      }
    }

    int IList.IndexOf(object item)
    {
      return this.IndexOf(item as TreeGridNode);
    }

    void IList.Insert(int index, object value)
    {
      this.Insert(index, value as TreeGridNode);
    }

    int ICollection.Count
    {
      get
      {
        return this.Count;
      }
    }

    bool IList.Contains(object value)
    {
      return this.Contains(value as TreeGridNode);
    }

    void ICollection.CopyTo(Array array, int index)
    {
      throw new Exception("The method or operation is not implemented.");
    }

    object IList.this[int index]
    {
      get
      {
        return (object) this[index];
      }
      set
      {
        throw new Exception("The method or operation is not implemented.");
      }
    }

    public IEnumerator<TreeGridNode> GetEnumerator()
    {
      return (IEnumerator<TreeGridNode>) this._list.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.GetEnumerator();
    }

    bool ICollection.IsSynchronized
    {
      get
      {
        throw new Exception("The method or operation is not implemented.");
      }
    }

    object ICollection.SyncRoot
    {
      get
      {
        throw new Exception("The method or operation is not implemented.");
      }
    }
  }
}
