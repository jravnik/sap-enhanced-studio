﻿// Decompiled with JetBrains decompiler
// Type: AdvancedDataGridView.TreeGridCell
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace AdvancedDataGridView
{
  public class TreeGridCell : DataGridViewTextBoxCell
  {
    private const int INDENT_WIDTH = 20;
    private const int INDENT_MARGIN = 5;
    private int glyphWidth;
    private int calculatedLeftPadding;
    internal bool IsSited;
    private Padding _previousPadding;
    private int _imageWidth;
    private int _imageHeight;
    private int _imageHeightOffset;

    public TreeGridCell()
    {
      this.glyphWidth = 15;
      this.calculatedLeftPadding = 0;
      this.IsSited = false;
    }

    public override object Clone()
    {
      TreeGridCell treeGridCell = (TreeGridCell) base.Clone();
      treeGridCell.glyphWidth = this.glyphWidth;
      treeGridCell.calculatedLeftPadding = this.calculatedLeftPadding;
      return (object) treeGridCell;
    }

    protected internal virtual void UnSited()
    {
      this.IsSited = false;
      this.Style.Padding = this._previousPadding;
    }

    protected internal virtual void Sited()
    {
      this.IsSited = true;
      this._previousPadding = this.Style.Padding;
      this.UpdateStyle();
    }

    protected internal virtual void UpdateStyle()
    {
      if (!this.IsSited)
        return;
      int level = this.Level;
      Padding previousPadding = this._previousPadding;
      Size preferredSize;
      using (Graphics graphics = this.OwningNode._grid.CreateGraphics())
        preferredSize = this.GetPreferredSize(graphics, this.InheritedStyle, this.RowIndex, new Size(0, 0));
      Image image = this.OwningNode.Image;
      if (image != null)
      {
        this._imageWidth = image.Width + 2;
        this._imageHeight = image.Height + 2;
      }
      else
      {
        this._imageWidth = this.glyphWidth;
        this._imageHeight = 0;
      }
      if (preferredSize.Height < this._imageHeight)
      {
        this.Style.Padding = new Padding(previousPadding.Left + level * 20 + this._imageWidth + 5, previousPadding.Top + this._imageHeight / 2, previousPadding.Right, previousPadding.Bottom + this._imageHeight / 2);
        this._imageHeightOffset = 2;
      }
      else
        this.Style.Padding = new Padding(previousPadding.Left + level * 20 + this._imageWidth + 5, previousPadding.Top, previousPadding.Right, previousPadding.Bottom);
      this.calculatedLeftPadding = (level - 1) * this.glyphWidth + this._imageWidth + 5;
    }

    public int Level
    {
      get
      {
        TreeGridNode owningNode = this.OwningNode;
        if (owningNode != null)
          return owningNode.Level;
        return -1;
      }
    }

    protected virtual int GlyphMargin
    {
      get
      {
        return (this.Level - 1) * 20 + 5;
      }
    }

    protected virtual int GlyphOffset
    {
      get
      {
        return (this.Level - 1) * 20;
      }
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
      TreeGridNode owningNode = this.OwningNode;
      if (owningNode == null)
        return;
      Image image = owningNode.Image;
      if (this._imageHeight == 0 && image != null)
        this.UpdateStyle();
      base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
      Rectangle rectangle = new Rectangle(cellBounds.X + this.GlyphMargin, cellBounds.Y, 20, cellBounds.Height - 1);
      int num1 = rectangle.Width / 2;
      int level = this.Level;
      if (image != null)
      {
        Point point = this._imageHeight <= cellBounds.Height ? new Point(rectangle.X + this.glyphWidth, cellBounds.Height / 2 - this._imageHeight / 2 + cellBounds.Y) : new Point(rectangle.X + this.glyphWidth, cellBounds.Y + this._imageHeightOffset);
        GraphicsContainer container = graphics.BeginContainer();
        graphics.SetClip(cellBounds);
        graphics.DrawImageUnscaled(image, point);
        graphics.EndContainer(container);
      }
      if (owningNode._grid.ShowLines)
      {
        using (Pen pen = new Pen(SystemBrushes.ControlDark, 1f))
        {
          pen.DashStyle = DashStyle.Dot;
          bool isLastSibling = owningNode.IsLastSibling;
          bool isFirstSibling = owningNode.IsFirstSibling;
          if (owningNode.Level == 1)
          {
            if (isFirstSibling && isLastSibling)
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top + cellBounds.Height / 2, rectangle.Right, cellBounds.Top + cellBounds.Height / 2);
            else if (isLastSibling)
            {
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top + cellBounds.Height / 2, rectangle.Right, cellBounds.Top + cellBounds.Height / 2);
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top, rectangle.X + 4, cellBounds.Top + cellBounds.Height / 2);
            }
            else if (isFirstSibling)
            {
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top + cellBounds.Height / 2, rectangle.Right, cellBounds.Top + cellBounds.Height / 2);
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top + cellBounds.Height / 2, rectangle.X + 4, cellBounds.Bottom);
            }
            else
            {
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top + cellBounds.Height / 2, rectangle.Right, cellBounds.Top + cellBounds.Height / 2);
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top, rectangle.X + 4, cellBounds.Bottom);
            }
          }
          else
          {
            if (isLastSibling)
            {
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top + cellBounds.Height / 2, rectangle.Right, cellBounds.Top + cellBounds.Height / 2);
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top, rectangle.X + 4, cellBounds.Top + cellBounds.Height / 2);
            }
            else
            {
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top + cellBounds.Height / 2, rectangle.Right, cellBounds.Top + cellBounds.Height / 2);
              graphics.DrawLine(pen, rectangle.X + 4, cellBounds.Top, rectangle.X + 4, cellBounds.Bottom);
            }
            TreeGridNode parent = owningNode.Parent;
            int num2 = rectangle.X + 4 - 20;
            while (!parent.IsRoot)
            {
              if (parent.HasChildren && !parent.IsLastSibling)
                graphics.DrawLine(pen, num2, cellBounds.Top, num2, cellBounds.Bottom);
              parent = parent.Parent;
              num2 -= 20;
            }
          }
        }
      }
      if (!owningNode.HasChildren && !owningNode._grid.VirtualNodes)
        return;
      if (Application.RenderWithVisualStyles)
      {
        if (owningNode.IsExpanded)
          owningNode._grid.rOpen.DrawBackground((IDeviceContext) graphics, new Rectangle(rectangle.X, rectangle.Y + rectangle.Height / 2 - 4, 10, 10));
        else
          owningNode._grid.rClosed.DrawBackground((IDeviceContext) graphics, new Rectangle(rectangle.X, rectangle.Y + rectangle.Height / 2 - 4, 10, 10));
      }
      else
      {
        int height = 8;
        int width = 8;
        int x = rectangle.X;
        int y = rectangle.Y + rectangle.Height / 2 - 4;
        graphics.DrawRectangle(new Pen(SystemBrushes.ControlDark), x, y, width, height);
        graphics.FillRectangle((Brush) new SolidBrush(Color.White), x + 1, y + 1, width - 1, height - 1);
        graphics.DrawLine(new Pen((Brush) new SolidBrush(Color.Black)), x + 2, y + 4, x + width - 2, y + 4);
        if (owningNode.IsExpanded)
          return;
        graphics.DrawLine(new Pen((Brush) new SolidBrush(Color.Black)), x + 4, y + 2, x + 4, y + height - 2);
      }
    }

    protected override void OnMouseUp(DataGridViewCellMouseEventArgs e)
    {
      base.OnMouseUp(e);
      TreeGridNode owningNode = this.OwningNode;
      if (owningNode == null)
        return;
      owningNode._grid._inExpandCollapseMouseCapture = false;
    }

    protected override void OnMouseDown(DataGridViewCellMouseEventArgs e)
    {
      if (e.Location.X > this.InheritedStyle.Padding.Left)
      {
        base.OnMouseDown(e);
      }
      else
      {
        TreeGridNode owningNode = this.OwningNode;
        if (owningNode == null)
          return;
        owningNode._grid._inExpandCollapseMouseCapture = true;
        if (owningNode.IsExpanded)
          owningNode.Collapse();
        else
          owningNode.Expand();
      }
    }

    public TreeGridNode OwningNode
    {
      get
      {
        return this.OwningRow as TreeGridNode;
      }
    }
  }
}
