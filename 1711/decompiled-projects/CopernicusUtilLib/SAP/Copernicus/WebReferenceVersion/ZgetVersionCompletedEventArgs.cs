﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.WebReferenceVersion.ZgetVersionCompletedEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace SAP.Copernicus.WebReferenceVersion
{
  [GeneratedCode("System.Web.Services", "4.0.30319.17929")]
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  public class ZgetVersionCompletedEventArgs : AsyncCompletedEventArgs
  {
    private object[] results;

    internal ZgetVersionCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState)
      : base(exception, cancelled, userState)
    {
      this.results = results;
    }

    public ZgetVersionResponse Result
    {
      get
      {
        this.RaiseExceptionIfNecessary();
        return (ZgetVersionResponse) this.results[0];
      }
    }
  }
}
