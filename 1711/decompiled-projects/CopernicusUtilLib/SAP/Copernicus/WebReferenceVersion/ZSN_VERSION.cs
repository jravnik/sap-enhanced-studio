﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.WebReferenceVersion.ZSN_VERSION
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Properties;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace SAP.Copernicus.WebReferenceVersion
{
  [DesignerCategory("code")]
  [WebServiceBinding(Name = "ZBN_VERSION", Namespace = "urn:sap-com:document:sap:soap:functions:mc-style")]
  [DebuggerStepThrough]
  [GeneratedCode("System.Web.Services", "4.0.30319.17929")]
  public class ZSN_VERSION : SoapHttpClientProtocol
  {
    private SendOrPostCallback ZgetVersionOperationCompleted;
    private bool useDefaultCredentialsSetExplicitly;

    public ZSN_VERSION()
    {
      this.Url = Settings.Default.CopernicusUtilLib_WebReferenceVersion_ZSN_VERSION;
      if (this.IsLocalFileSystemWebService(this.Url))
      {
        this.UseDefaultCredentials = true;
        this.useDefaultCredentialsSetExplicitly = false;
      }
      else
        this.useDefaultCredentialsSetExplicitly = true;
    }

    public new string Url
    {
      get
      {
        return base.Url;
      }
      set
      {
        if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
          base.UseDefaultCredentials = false;
        base.Url = value;
      }
    }

    public new bool UseDefaultCredentials
    {
      get
      {
        return base.UseDefaultCredentials;
      }
      set
      {
        base.UseDefaultCredentials = value;
        this.useDefaultCredentialsSetExplicitly = true;
      }
    }

    public event ZgetVersionCompletedEventHandler ZgetVersionCompleted;

    [SoapDocumentMethod("urn:sap-com:document:sap:soap:functions:mc-style:ZGET_VERSION_SERVICE:ZgetVersionRequest", ParameterStyle = SoapParameterStyle.Bare, Use = SoapBindingUse.Literal)]
    [return: XmlElement("ZgetVersionResponse", Namespace = "urn:sap-com:document:sap:soap:functions:mc-style")]
    public ZgetVersionResponse ZgetVersion([XmlElement("ZgetVersion", Namespace = "urn:sap-com:document:sap:soap:functions:mc-style")] ZgetVersion ZgetVersion1)
    {
      return (ZgetVersionResponse) this.Invoke(nameof (ZgetVersion), new object[1]{ (object) ZgetVersion1 })[0];
    }

    public void ZgetVersionAsync(ZgetVersion ZgetVersion1)
    {
      this.ZgetVersionAsync(ZgetVersion1, (object) null);
    }

    public void ZgetVersionAsync(ZgetVersion ZgetVersion1, object userState)
    {
      if (this.ZgetVersionOperationCompleted == null)
        this.ZgetVersionOperationCompleted = new SendOrPostCallback(this.OnZgetVersionOperationCompleted);
      this.InvokeAsync("ZgetVersion", new object[1]
      {
        (object) ZgetVersion1
      }, this.ZgetVersionOperationCompleted, userState);
    }

    private void OnZgetVersionOperationCompleted(object arg)
    {
      if (this.ZgetVersionCompleted == null)
        return;
      InvokeCompletedEventArgs completedEventArgs = (InvokeCompletedEventArgs) arg;
      this.ZgetVersionCompleted((object) this, new ZgetVersionCompletedEventArgs(completedEventArgs.Results, completedEventArgs.Error, completedEventArgs.Cancelled, completedEventArgs.UserState));
    }

    public new void CancelAsync(object userState)
    {
      base.CancelAsync(userState);
    }

    private bool IsLocalFileSystemWebService(string url)
    {
      if (url == null || url == string.Empty)
        return false;
      Uri uri = new Uri(url);
      return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
    }
  }
}
