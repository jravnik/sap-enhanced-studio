﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.WebReferenceVersion.ZgetVersion
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.WebReferenceVersion
{
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [XmlType(AnonymousType = true, Namespace = "urn:sap-com:document:sap:soap:functions:mc-style")]
  [GeneratedCode("System.Xml", "4.0.30319.17929")]
  [Serializable]
  public class ZgetVersion
  {
    private string ivPpmsTechnicalNameField;
    private string ivPpmsTechnicalReleaseField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string IvPpmsTechnicalName
    {
      get
      {
        return this.ivPpmsTechnicalNameField;
      }
      set
      {
        this.ivPpmsTechnicalNameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string IvPpmsTechnicalRelease
    {
      get
      {
        return this.ivPpmsTechnicalReleaseField;
      }
      set
      {
        this.ivPpmsTechnicalReleaseField = value;
      }
    }
  }
}
