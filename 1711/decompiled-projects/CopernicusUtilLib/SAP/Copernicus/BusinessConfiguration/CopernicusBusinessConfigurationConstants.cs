﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.CopernicusBusinessConfigurationConstants
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.BusinessConfiguration
{
  public class CopernicusBusinessConfigurationConstants
  {
    public const string ID = "BCSET_ID";
    public const string BCO_PROXY = "BCO_PROXY";
    public const string FINETUNERELEVANT = "BCSET_FINETUNE_RELEVANT";
    public const string DESCRIPTION = "BCSET_DESCRIPTION";
    public const string SAPBCOPROXYNAME = "SAPBCOPROXYNAME";
    public const string TYPE = "BCSET_TYPE";
    public const string SUBTYPE = "BCSET_SUBTYPE";
    public const string PARAM = "BCSET_PARAMETER";
    public const char PARAM_SEPERATOR = ';';
    public const string oldBCFileName = "BusinessConfiguration.xml";
    public const string BCFileSuffix = "bac";
    public const int BACIDLength = 20;
    public const string ESRBONAME = "esrBOName";
    public const string OFFLINE_ENABLED = "Offline";
    public const string BCSetFileSuffix = "bcc";
    public const string BCOFileSuffix = "bco";
    public const string ImplementationProjectTemplateFileSuffix = "bct";
    public const string PHASE_FT = "Fine Tuning";
    public const string ACTIVITY_TYPE_MAINTAIN = "Maintain";
    public const string ACTIVITY_GROUP_PS = "Partner Solutions";
    public const string GO_LIVE_OPTIONAL = "Optional";
    public const string GO_LIVE_MANDATORY = "Mandatory";
    public const string GO_LIVE_NR = "Not Relevant";
    public const string OPTION_TYPE_CFG = "Configuration";
    public const string BAC_BUSINESS_AREA = "BA";
    public const string BAC_BUSINESS_PACKAGE = "BP";
    public const string BAC_BUSINESS_TOPIC = "BT";
    public const string BAC_BUSINESS_OPTION = "BO";
    public const string BAC_OPTIONGROUP = "OG";
    public const string BUSINESS_OPTION = "Business Option";
    public const string BUSINESS_TOPIC = "Business Topic";
    public const string BUSINESS_PACKAGE = "Business Package";
    public const string COUNTRY = "Country";
    public const string PSM_RELEASED = "3";
    public const string PSM_DEPRECATED = "4";

    public enum BCContentType
    {
      BCSet,
      MDRO,
      URL,
    }

    public enum BCContentSubType
    {
      BusinessObject,
      ProcessComponent,
      BPVT,
    }
  }
}
