﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BAC.HeadType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BAC
{
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BusinessConfigurationDefinition")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class HeadType
  {
    private string elementIDField;
    private string elementTypeField;
    private string countryCodeField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ElementID
    {
      get
      {
        return this.elementIDField;
      }
      set
      {
        this.elementIDField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ElementType
    {
      get
      {
        return this.elementTypeField;
      }
      set
      {
        this.elementTypeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string CountryCode
    {
      get
      {
        return this.countryCodeField;
      }
      set
      {
        this.countryCodeField = value;
      }
    }
  }
}
