﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.ValueStructureType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCSetDefinition")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class ValueStructureType
  {
    private ValueFragmentType[] valueFragmentField;
    private bool isDefaultField;
    private bool isDeletableField;

    [XmlElement("ValueFragment", Form = XmlSchemaForm.Unqualified)]
    public ValueFragmentType[] ValueFragment
    {
      get
      {
        return this.valueFragmentField;
      }
      set
      {
        this.valueFragmentField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public bool isDefault
    {
      get
      {
        return this.isDefaultField;
      }
      set
      {
        this.isDefaultField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public bool isDeletable
    {
      get
      {
        return this.isDeletableField;
      }
      set
      {
        this.isDeletableField = value;
      }
    }
  }
}
