﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BCO.DataTypeType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BCO
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCODefinition")]
  [Serializable]
  public enum DataTypeType
  {
    [XmlEnum("1")] Code,
    [XmlEnum("2")] Indicator,
    [XmlEnum("5")] Integer,
    [XmlEnum("6")] Date,
    [XmlEnum("7")] Time,
    [XmlEnum("8")] Percentage,
    [XmlEnum("9")] Text,
  }
}
