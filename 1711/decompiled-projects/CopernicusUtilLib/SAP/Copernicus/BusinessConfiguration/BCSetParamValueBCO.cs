﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.BCSetParamValueBCO
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.BusinessConfiguration.Model;
using System.Collections.Generic;

namespace SAP.Copernicus.BusinessConfiguration
{
  public class BCSetParamValueBCO
  {
    public string NodeID;
    public KeyPair[] Key;
    public KeyPair[] ParamValueDataTypeTupple;
    public string BCOProxyName;
    public string DataType;
    public bool usedOnlyOnce;
    public Dictionary<string, BCSetType> BCSet;

    public BCSetParamValueBCO(string BCOProxyName, string NodeID, KeyPair[] key, KeyPair[] paramValDT, BCSetType BCSet)
    {
      this.BCSet = new Dictionary<string, BCSetType>();
      this.BCSet.Add(BCSet.Head.ID, BCSet);
      this.Key = key;
      this.ParamValueDataTypeTupple = paramValDT;
      this.NodeID = NodeID;
      this.BCOProxyName = BCOProxyName;
      this.usedOnlyOnce = true;
    }

    public bool CodeUsedInAssignment(KeyPair[] key, string codelistBCOProxyName)
    {
      if (key.Length > 0)
      {
        foreach (KeyPair keyPair in this.ParamValueDataTypeTupple)
        {
          if (keyPair.DataType.StartsWith(codelistBCOProxyName) && key[0].Value == keyPair.Value)
            return true;
        }
      }
      return false;
    }

    public bool KeyMatch(KeyPair[] keyChecked, string BCOProxyName, string NodeID)
    {
      if (!this.BCOProxyName.Equals(BCOProxyName) || !this.NodeID.Equals(NodeID))
        return false;
      int num = 0;
      if (keyChecked != null && keyChecked.Length > 0 && keyChecked.Length == this.Key.Length)
      {
        for (int index1 = 0; index1 < keyChecked.Length; ++index1)
        {
          KeyPair keyPair = keyChecked[index1];
          for (int index2 = 0; index2 < this.Key.Length; ++index2)
          {
            if (this.Key[index2].Parameter.Equals(keyPair.Parameter) && (keyPair.Value != null || this.Key[index2].Value == null) && (keyPair.Value == null || this.Key[index2].Value != null) && (keyPair.Value == null && this.Key[index2].Value == null || keyPair.Value.Equals(this.Key[index2].Value)))
            {
              ++num;
              break;
            }
          }
        }
      }
      return num == keyChecked.Length;
    }
  }
}
