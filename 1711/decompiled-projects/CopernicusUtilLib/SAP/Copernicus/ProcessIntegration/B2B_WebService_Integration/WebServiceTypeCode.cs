﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.WebServiceTypeCode
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Xml.Serialization;

namespace SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration
{
  [Serializable]
  public enum WebServiceTypeCode
  {
    [XmlEnum("1")] Soap,
    [XmlEnum("2")] Rest,
  }
}
