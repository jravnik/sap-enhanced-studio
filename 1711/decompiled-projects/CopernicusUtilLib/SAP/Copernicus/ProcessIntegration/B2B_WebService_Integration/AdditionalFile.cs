﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.AdditionalFile
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration
{
  public class AdditionalFile
  {
    public string FileName { get; set; }

    public string FilePath { get; set; }

    public AdditionalFile()
    {
    }

    public AdditionalFile(string pstrFileName, string pstrFilePath)
    {
      this.FileName = pstrFileName;
      this.FilePath = pstrFilePath;
    }
  }
}
