﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.SelectedPortType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration
{
  public class SelectedPortType
  {
    public string Name { get; set; }

    public string Namespace { get; set; }

    public SelectedPortType()
    {
    }

    public SelectedPortType(string Name, string Namespace)
    {
      this.Name = Name;
      this.Namespace = Namespace;
    }
  }
}
