﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CopernicusToolWindowPane
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.Shell;
using SAP.Copernicus.Core.Automation;
using System;
using System.Windows.Forms;
using System.Windows.Input;

namespace SAP.Copernicus.Core.GUI
{
  public class CopernicusToolWindowPane : ToolWindowPane
  {
    protected string helpid;

    public CopernicusToolWindowPane(string helpID)
      : base((IServiceProvider) null)
    {
      this.helpid = helpID;
    }

    protected override bool PreProcessMessage(ref Message m)
    {
      if (m.Msg == 256 && this.helpid != null && Keyboard.IsKeyDown(Key.F1))
        HelpUtil.DisplayF1Help(this.helpid);
      return base.PreProcessMessage(ref m);
    }
  }
}
