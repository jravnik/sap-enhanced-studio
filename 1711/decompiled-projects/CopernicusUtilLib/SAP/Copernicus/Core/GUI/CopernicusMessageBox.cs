﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CopernicusMessageBox
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI
{
  public static class CopernicusMessageBox
  {
    public static DialogResult Show(string text)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text);
      return DialogResult.None;
    }

    public static DialogResult Show(IWin32Window owner, string text)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(owner, text);
      return DialogResult.None;
    }

    public static DialogResult Show(string text, string caption)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text, caption);
      return DialogResult.None;
    }

    public static DialogResult Show(IWin32Window owner, string text, string caption)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(owner, text, caption);
      return DialogResult.None;
    }

    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text, caption, buttons);
      return DialogResult.None;
    }

    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(owner, text, caption, buttons);
      return DialogResult.None;
    }

    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text, caption, buttons, icon);
      return DialogResult.None;
    }

    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(owner, text, caption, buttons, icon);
      return DialogResult.None;
    }

    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text, caption, buttons, icon, defaultButton);
      return DialogResult.None;
    }

    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(owner, text, caption, buttons, icon, defaultButton);
      return DialogResult.None;
    }

    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text, caption, buttons, icon, defaultButton, options);
      return DialogResult.None;
    }

    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(owner, text, caption, buttons, icon, defaultButton, options);
      return DialogResult.None;
    }

    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options, bool displayHelpButton)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text, caption, buttons, icon, defaultButton, options, displayHelpButton);
      return DialogResult.None;
    }

    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options, string helpFilePath)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text, caption, buttons, icon, defaultButton, options, helpFilePath);
      return DialogResult.None;
    }

    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options, string helpFilePath)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(owner, text, caption, buttons, icon, defaultButton, options, helpFilePath);
      return DialogResult.None;
    }

    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options, string helpFilePath, HelpNavigator navigator)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text, caption, buttons, icon, defaultButton, options, helpFilePath, navigator);
      return DialogResult.None;
    }

    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options, string helpFilePath, string keyword)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text, caption, buttons, icon, defaultButton, options, helpFilePath, keyword);
      return DialogResult.None;
    }

    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options, string helpFilePath, HelpNavigator navigator)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(owner, text, caption, buttons, icon, defaultButton, options, helpFilePath, navigator);
      return DialogResult.None;
    }

    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options, string helpFilePath, string keyword)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(owner, text, caption, buttons, icon, defaultButton, options, helpFilePath, keyword);
      return DialogResult.None;
    }

    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options, string helpFilePath, HelpNavigator navigator, object param)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(text, caption, buttons, icon, defaultButton, options, helpFilePath, navigator, param);
      return DialogResult.None;
    }

    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options, string helpFilePath, HelpNavigator navigator, object param)
    {
      if (!Connection.getInstance().HeadLessMode)
        return MessageBox.Show(owner, text, caption, buttons, icon, defaultButton, options, helpFilePath, navigator, param);
      return DialogResult.None;
    }
  }
}
