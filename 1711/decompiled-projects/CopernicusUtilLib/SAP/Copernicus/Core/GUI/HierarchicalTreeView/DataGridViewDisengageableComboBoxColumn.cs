﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.HierarchicalTreeView.DataGridViewDisengageableComboBoxColumn
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.HierarchicalTreeView
{
  public class DataGridViewDisengageableComboBoxColumn : DataGridViewColumn
  {
    public DataGridViewDisengageableComboBoxColumn()
      : base((DataGridViewCell) new DataGridViewDisengageableComboBoxCell())
    {
    }

    public override DataGridViewCell CellTemplate
    {
      get
      {
        return base.CellTemplate;
      }
      set
      {
        if (value != null && !value.GetType().IsAssignableFrom(typeof (DataGridViewDisengageableComboBoxCell)))
          throw new InvalidCastException("Must be a DataGridViewDisengageableComboBoxCell");
        base.CellTemplate = value;
      }
    }
  }
}
