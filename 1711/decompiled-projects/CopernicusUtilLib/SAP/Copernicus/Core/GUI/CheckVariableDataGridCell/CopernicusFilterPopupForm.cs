﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckVariableDataGridCell.CopernicusFilterPopupForm
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Wizard;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.CheckVariableDataGridCell
{
  public abstract class CopernicusFilterPopupForm : Form
  {
    private const int spaceBetweenOKandCancelButton = 50;
    private const int separateWidth = 40;
    private Button btnOK;
    protected Panel buttonPanel;
    protected CopernicusEtchedLine etchedLine1;
    protected Button btnCancel;
    protected Panel pagePanel;
    private bool isSelectionValid;
    protected string selectedFilterValue;
    protected CopernicusPopupClassMetaData metaDataForClass;

    public CopernicusFilterPopupForm()
    {
      this.InitializeComponent();
      this.Icon = Resource.SAPBusinessByDesignStudioIcon;
      this.PropertyChanged += new PropertyChangedEventHandler(this.CopernicusCallbackPopupForm_PropertyChanged);
    }

    public event PropertyChangedEventHandler PropertyChanged;

    private void NotifyPropertyChanged(string info)
    {
      if (this.PropertyChanged == null)
        return;
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(info));
    }

    public bool IsCurrentSelectionValid
    {
      get
      {
        return this.isSelectionValid;
      }
      set
      {
        if (value == this.isSelectionValid)
          return;
        this.isSelectionValid = value;
        this.NotifyPropertyChanged(nameof (IsCurrentSelectionValid));
      }
    }

    private void CopernicusCallbackPopupForm_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (this.btnOK.Enabled == this.isSelectionValid)
        return;
      this.btnOK.Enabled = this.isSelectionValid;
    }

    protected string FilterValue
    {
      get
      {
        return this.selectedFilterValue;
      }
      set
      {
        if (value == null || value.Equals(this.selectedFilterValue))
          return;
        this.selectedFilterValue = value;
      }
    }

    protected CopernicusPopupClassMetaData MetaDataForClass
    {
      get
      {
        return this.metaDataForClass;
      }
      set
      {
        if (value == null || value.Equals((object) this.metaDataForClass))
          return;
        this.metaDataForClass = value;
      }
    }

    protected abstract void setDataFieldContent();

    protected abstract void reloadData();

    protected void setDataFieldContent(string strFilterSetValue, CopernicusPopupClassMetaData classForMeta)
    {
      this.selectedFilterValue = strFilterSetValue;
      this.metaDataForClass = classForMeta;
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    public virtual string ShowModalDialog()
    {
      int num = (int) this.ShowDialog();
      this.setDataFieldContent();
      return this.selectedFilterValue;
    }

    public void AddFilterDataGridView(DataGridView dataGridView)
    {
      this.pagePanel.Controls.Add((Control) dataGridView);
    }

    public virtual string ShowModalDialog(string strFilterSetValue, CopernicusPopupClassMetaData classMeta, out DialogResult result)
    {
      this.setDataFieldContent(strFilterSetValue, classMeta);
      this.reloadData();
      result = this.ShowDialog();
      if (result == DialogResult.OK)
        this.setDataFieldContent();
      return this.selectedFilterValue;
    }

    private void InitializeComponent()
    {
      this.btnOK = new Button();
      this.buttonPanel = new Panel();
      this.btnCancel = new Button();
      this.pagePanel = new Panel();
      this.etchedLine1 = new CopernicusEtchedLine();
      this.buttonPanel.SuspendLayout();
      this.SuspendLayout();
      this.btnOK.DialogResult = DialogResult.OK;
      this.btnOK.Enabled = false;
      this.btnOK.Location = new Point(173, 7);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new Size(75, 23);
      this.btnOK.TabIndex = 0;
      this.btnOK.Text = "OK";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new EventHandler(this.btnOK_Click);
      this.buttonPanel.Controls.Add((Control) this.etchedLine1);
      this.buttonPanel.Controls.Add((Control) this.btnOK);
      this.buttonPanel.Controls.Add((Control) this.btnCancel);
      this.buttonPanel.Dock = DockStyle.Bottom;
      this.buttonPanel.Location = new Point(0, 106);
      this.buttonPanel.Name = "buttonPanel";
      this.buttonPanel.Size = new Size(405, 40);
      this.buttonPanel.TabIndex = 5;
      this.btnCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.btnCancel.DialogResult = DialogResult.Cancel;
      this.btnCancel.FlatStyle = FlatStyle.System;
      this.btnCancel.Location = new Point(236, 7);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new Size(75, 23);
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "Cancel";
      this.pagePanel.AutoSize = true;
      this.pagePanel.Dock = DockStyle.Fill;
      this.pagePanel.Location = new Point(0, 0);
      this.pagePanel.Name = "pagePanel";
      this.pagePanel.Size = new Size(405, 146);
      this.pagePanel.TabIndex = 6;
      this.etchedLine1.AutoScroll = true;
      this.etchedLine1.AutoSize = true;
      this.etchedLine1.Dock = DockStyle.Top;
      this.etchedLine1.Edge = EtchEdge.Top;
      this.etchedLine1.Location = new Point(0, 0);
      this.etchedLine1.Name = "etchedLine1";
      this.etchedLine1.Size = new Size(405, 0);
      this.etchedLine1.TabIndex = 4;
      this.AcceptButton = (IButtonControl) this.btnOK;
      this.ClientSize = new Size(405, 146);
      this.Controls.Add((Control) this.buttonPanel);
      this.Controls.Add((Control) this.pagePanel);
      this.Name = nameof (CopernicusFilterPopupForm);
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Load += new EventHandler(this.onResizeEventHandler);
      this.Resize += new EventHandler(this.onResizeEventHandler);
      this.buttonPanel.ResumeLayout(false);
      this.buttonPanel.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private void onResizeEventHandler(object sender, EventArgs e)
    {
      int x = this.Width - 40 - this.btnCancel.Width;
      int y = this.btnCancel.Location.Y;
      this.btnCancel.Location = new Point(x, y);
      this.btnOK.Location = new Point(x - 50 - this.btnOK.Width, y);
    }
  }
}
