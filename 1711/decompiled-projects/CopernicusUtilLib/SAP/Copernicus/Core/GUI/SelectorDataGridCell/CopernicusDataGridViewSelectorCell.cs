﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.SelectorDataGridCell.CopernicusDataGridViewSelectorCell
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.SelectorDataGridCell
{
  public class CopernicusDataGridViewSelectorCell : DataGridViewTextBoxCell
  {
    private static readonly DataGridViewContentAlignment anyRight = DataGridViewContentAlignment.TopRight | DataGridViewContentAlignment.MiddleRight | DataGridViewContentAlignment.BottomRight;
    private static readonly DataGridViewContentAlignment anyCenter = DataGridViewContentAlignment.TopCenter | DataGridViewContentAlignment.MiddleCenter | DataGridViewContentAlignment.BottomCenter;
    private static Type defaultEditType = typeof (CopernicusDataGridViewSelectorEditingControl);
    private static Type defaultValueType = typeof (string);
    private const int DATAGRIDVIEWSELECTORCELL_defaultRenderingBitmapWidth = 100;
    private const int DATAGRIDVIEWSELECTORCELL_defaultRenderingBitmapHeight = 22;
    internal const string DATAGRIDVIEWSELECTORCELL_defaultCallbackHandler = "";
    private string strCallbackFormClass;
    private string strCallbackFormPackageName;
    [ThreadStatic]
    private static Bitmap renderingBitmap;
    [ThreadStatic]
    private static Bitmap emptyBitmap;
    [ThreadStatic]
    private static UserControlSelectableText paintingContorlforSelector;

    [DllImport("USER32.DLL", CharSet = CharSet.Auto)]
    private static extern short VkKeyScan(char key);

    public CopernicusDataGridViewSelectorCell()
    {
      if (CopernicusDataGridViewSelectorCell.renderingBitmap == null)
        CopernicusDataGridViewSelectorCell.renderingBitmap = new Bitmap(100, 22);
      if (CopernicusDataGridViewSelectorCell.emptyBitmap == null)
        CopernicusDataGridViewSelectorCell.emptyBitmap = new Bitmap(100, 22);
      if (CopernicusDataGridViewSelectorCell.paintingContorlforSelector == null || CopernicusDataGridViewSelectorCell.paintingContorlforSelector.IsDisposedAlready)
      {
        CopernicusDataGridViewSelectorCell.paintingContorlforSelector = new UserControlSelectableText();
        CopernicusDataGridViewSelectorCell.paintingContorlforSelector.BorderStyle = BorderStyle.None;
      }
      this.strCallbackFormClass = "";
      this.strCallbackFormPackageName = "";
    }

    [DefaultValue("")]
    public string strCallbackFormHandler
    {
      get
      {
        return this.strCallbackFormClass;
      }
      set
      {
        if (!(this.strCallbackFormClass != value))
          return;
        this.SetCallbackFormHandler(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    internal void SetCallbackFormHandler(int rowIndex, string value)
    {
      this.strCallbackFormClass = value;
      this.OwnsEditingCopernicusTextSelector(rowIndex);
    }

    [DefaultValue("")]
    public string CallbackFormPackageName
    {
      get
      {
        return this.strCallbackFormPackageName;
      }
      set
      {
        if (!(this.strCallbackFormPackageName != value))
          return;
        this.SetCallbackFormPackageName(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    internal void SetCallbackFormPackageName(int rowIndex, string value)
    {
      this.strCallbackFormPackageName = value;
      this.OwnsEditingCopernicusTextSelector(rowIndex);
    }

    private void OnCommonChange()
    {
      if (this.DataGridView == null || this.DataGridView.IsDisposed || this.DataGridView.Disposing)
        return;
      if (this.RowIndex == -1)
        this.DataGridView.InvalidateColumn(this.ColumnIndex);
      else
        this.DataGridView.UpdateCellValue(this.ColumnIndex, this.RowIndex);
    }

    private bool OwnsEditingCopernicusTextSelector(int rowIndex)
    {
      if (rowIndex == -1 || this.DataGridView == null)
        return false;
      CopernicusDataGridViewSelectorEditingControl editingControl = this.DataGridView.EditingControl as CopernicusDataGridViewSelectorEditingControl;
      if (editingControl != null)
        return rowIndex == editingControl.EditingControlRowIndex;
      return false;
    }

    private CopernicusDataGridViewSelectorEditingControl EditingSelector
    {
      get
      {
        return this.DataGridView.EditingControl as CopernicusDataGridViewSelectorEditingControl;
      }
    }

    public override Type EditType
    {
      get
      {
        return CopernicusDataGridViewSelectorCell.defaultEditType;
      }
    }

    public override Type ValueType
    {
      get
      {
        Type valueType = base.ValueType;
        if (valueType != (Type) null)
          return valueType;
        return CopernicusDataGridViewSelectorCell.defaultValueType;
      }
    }

    public override object DefaultNewRowValue
    {
      get
      {
        return (object) string.Empty;
      }
    }

    public override object Clone()
    {
      CopernicusDataGridViewSelectorCell viewSelectorCell = base.Clone() as CopernicusDataGridViewSelectorCell;
      if (viewSelectorCell != null)
      {
        viewSelectorCell.strCallbackFormHandler = this.strCallbackFormClass;
        viewSelectorCell.strCallbackFormPackageName = this.strCallbackFormPackageName;
      }
      return (object) viewSelectorCell;
    }

    public string InitialNameSpace
    {
      get
      {
        if (this.Tag == null)
          return string.Empty;
        return this.Tag.ToString();
      }
      set
      {
        if (this.Tag == null)
        {
          string empty = string.Empty;
        }
        else
          this.Tag.ToString();
        if (this.Tag != null && (this.Tag == null || !(value != this.Tag.ToString())))
          return;
        this.Tag = (object) value;
      }
    }

    private void TagChangeEvent(string strDataNameSpaceTag, int iRowIndex, int iColumnIndex)
    {
      if (this.RowIndex != iRowIndex || this.ColumnIndex != iColumnIndex || this.Tag != null && (!(this.Tag is string) || this.Tag.ToString().Equals(strDataNameSpaceTag)))
        return;
      this.Tag = (object) strDataNameSpaceTag;
    }

    public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
    {
      base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
      UserControlSelectableText editingControl = this.DataGridView.EditingControl as UserControlSelectableText;
      if (editingControl == null)
        return;
      editingControl.BorderStyle = BorderStyle.None;
      editingControl.CallbackModelFormPackageName = this.strCallbackFormPackageName;
      editingControl.CallbackModelFormClass = this.strCallbackFormHandler;
      editingControl.AddTagChangeMonitor(new Action<string, int, int>(this.TagChangeEvent), this);
      string strCellDataType = this.Value != null ? this.Value.ToString() : string.Empty;
      editingControl.Value = strCellDataType;
      string strCellNamespace = this.Tag != null ? this.Tag.ToString() : string.Empty;
      editingControl.DataNameSpace = strCellNamespace;
      editingControl.IRowIndex = rowIndex;
      editingControl.IColumnIndex = this.ColumnIndex;
      editingControl.TooltipUpdate(strCellDataType, strCellNamespace);
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public override void DetachEditingControl()
    {
      DataGridView dataGridView = this.DataGridView;
      if (dataGridView == null || dataGridView.EditingControl == null)
        throw new InvalidOperationException("Cell is detached or its grid has no editing control.");
      UserControlSelectableText editingControl = dataGridView.EditingControl as UserControlSelectableText;
      base.DetachEditingControl();
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
      if (this.DataGridView == null)
        return;
      base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts & ~(DataGridViewPaintParts.ContentForeground | DataGridViewPaintParts.ErrorIcon));
      Point currentCellAddress = this.DataGridView.CurrentCellAddress;
      if (currentCellAddress.X == this.ColumnIndex && currentCellAddress.Y == rowIndex && this.DataGridView.EditingControl != null)
        return;
      if (CopernicusDataGridViewSelectorCell.PartPainted(paintParts, DataGridViewPaintParts.ContentForeground))
      {
        Rectangle rectangle1 = this.BorderWidths(advancedBorderStyle);
        Rectangle editingControlBounds1 = cellBounds;
        editingControlBounds1.Width -= rectangle1.Right;
        editingControlBounds1.Height -= rectangle1.Bottom;
        if (cellStyle.Padding != Padding.Empty)
        {
          if (this.DataGridView.RightToLeft == RightToLeft.Yes)
            editingControlBounds1.Offset(cellStyle.Padding.Right, cellStyle.Padding.Top);
          else
            editingControlBounds1.Offset(cellStyle.Padding.Left, cellStyle.Padding.Top);
          editingControlBounds1.Width -= cellStyle.Padding.Horizontal;
          editingControlBounds1.Height -= cellStyle.Padding.Vertical;
        }
        Rectangle editingControlBounds2 = this.GetAdjustedEditingControlBounds(editingControlBounds1, cellStyle);
        bool flag = (cellState & DataGridViewElementStates.Selected) != DataGridViewElementStates.None;
        if (CopernicusDataGridViewSelectorCell.renderingBitmap.Width < editingControlBounds2.Width || CopernicusDataGridViewSelectorCell.renderingBitmap.Height < editingControlBounds2.Height)
        {
          CopernicusDataGridViewSelectorCell.renderingBitmap.Dispose();
          CopernicusDataGridViewSelectorCell.renderingBitmap = new Bitmap(editingControlBounds2.Width, editingControlBounds2.Height);
          CopernicusDataGridViewSelectorCell.emptyBitmap.Dispose();
          CopernicusDataGridViewSelectorCell.emptyBitmap = new Bitmap(editingControlBounds2.Width, editingControlBounds2.Height);
        }
        if (CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Parent == null || !CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Parent.Visible)
          CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Parent = (Control) this.DataGridView;
        CopernicusDataGridViewSelectorCell.paintingContorlforSelector.TextAlign = CopernicusDataGridViewSelectorCell.TranslateAlignment(cellStyle.Alignment);
        CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Font = cellStyle.Font;
        CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Width = editingControlBounds2.Width;
        CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Height = editingControlBounds2.Height;
        CopernicusDataGridViewSelectorCell.paintingContorlforSelector.RightToLeft = this.DataGridView.RightToLeft;
        CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Location = new Point(0, -CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Height);
        Color baseColor = !CopernicusDataGridViewSelectorCell.PartPainted(paintParts, DataGridViewPaintParts.SelectionBackground) || !flag ? cellStyle.BackColor : cellStyle.SelectionBackColor;
        if (CopernicusDataGridViewSelectorCell.PartPainted(paintParts, DataGridViewPaintParts.Background))
        {
          if ((int) baseColor.A < (int) byte.MaxValue)
            baseColor = Color.FromArgb((int) byte.MaxValue, baseColor);
          CopernicusDataGridViewSelectorCell.paintingContorlforSelector.BackColor = baseColor;
        }
        Rectangle rectangle2 = new Rectangle(0, 0, editingControlBounds2.Width, editingControlBounds2.Height);
        if (rectangle2.Width > 0 && rectangle2.Height > 0)
        {
          CopernicusDataGridViewSelectorCell.paintingContorlforSelector.CallbackModelFormClass = this.strCallbackFormHandler;
          CopernicusDataGridViewSelectorCell.paintingContorlforSelector.CallbackModelFormPackageName = this.CallbackFormPackageName;
          CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Value = this.Value == null || this.Value.ToString().Length <= 0 ? string.Empty : this.Value.ToString();
          CopernicusDataGridViewSelectorCell.paintingContorlforSelector.DataNameSpace = this.Tag == null || this.Tag.ToString().Length <= 0 ? string.Empty : this.Tag.ToString();
          if (CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Value.Length > 0 || CopernicusDataGridViewSelectorCell.paintingContorlforSelector.DataNameSpace.Length > 0)
            this.ToolTipText = string.Format(CopernicusDataGridViewSelectorCell.paintingContorlforSelector.strToolTipTemplate, (object) CopernicusDataGridViewSelectorCell.paintingContorlforSelector.Value, (object) CopernicusDataGridViewSelectorCell.paintingContorlforSelector.DataNameSpace);
          if (this.Value != null && this.Value.ToString().Length > 0)
          {
            CopernicusDataGridViewSelectorCell.paintingContorlforSelector.DrawToBitmap(CopernicusDataGridViewSelectorCell.renderingBitmap, rectangle2);
            graphics.DrawImage((Image) CopernicusDataGridViewSelectorCell.renderingBitmap, new Rectangle(editingControlBounds2.Location, editingControlBounds2.Size), rectangle2, GraphicsUnit.Pixel);
          }
          else
            graphics.DrawImage((Image) CopernicusDataGridViewSelectorCell.emptyBitmap, new Rectangle(editingControlBounds2.Location, editingControlBounds2.Size), rectangle2, GraphicsUnit.Pixel);
        }
      }
      if (!CopernicusDataGridViewSelectorCell.PartPainted(paintParts, DataGridViewPaintParts.ErrorIcon))
        return;
      base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, DataGridViewPaintParts.ErrorIcon);
    }

    private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
    {
      return (paintParts & paintPart) != DataGridViewPaintParts.None;
    }

    public override void PositionEditingControl(bool setLocation, bool setSize, Rectangle cellBounds, Rectangle cellClip, DataGridViewCellStyle cellStyle, bool singleVerticalBorderAdded, bool singleHorizontalBorderAdded, bool isFirstDisplayedColumn, bool isFirstDisplayedRow)
    {
      Rectangle editingControlBounds = this.GetAdjustedEditingControlBounds(this.PositionEditingPanel(cellBounds, cellClip, cellStyle, singleVerticalBorderAdded, singleHorizontalBorderAdded, isFirstDisplayedColumn, isFirstDisplayedRow), cellStyle);
      this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
      this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
    }

    private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
    {
      ++editingControlBounds.X;
      editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);
      return editingControlBounds;
    }

    protected override Size GetPreferredSize(Graphics graphics, DataGridViewCellStyle cellStyle, int rowIndex, Size constraintSize)
    {
      if (this.DataGridView == null)
        return new Size(-1, -1);
      return base.GetPreferredSize(graphics, cellStyle, rowIndex, constraintSize);
    }

    protected override Rectangle GetErrorIconBounds(Graphics graphics, DataGridViewCellStyle cellStyle, int rowIndex)
    {
      Rectangle errorIconBounds = base.GetErrorIconBounds(graphics, cellStyle, rowIndex);
      errorIconBounds.X = this.DataGridView.RightToLeft != RightToLeft.Yes ? errorIconBounds.Left - 16 : errorIconBounds.Left + 16;
      return errorIconBounds;
    }

    protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
    {
      return base.GetFormattedValue(value, rowIndex, ref cellStyle, valueTypeConverter, formattedValueTypeConverter, context);
    }

    public override bool KeyEntersEditMode(KeyEventArgs e)
    {
      NumberFormatInfo numberFormat = CultureInfo.CurrentCulture.NumberFormat;
      Keys keys = Keys.None;
      string negativeSign = numberFormat.NegativeSign;
      if (!string.IsNullOrEmpty(negativeSign) && negativeSign.Length == 1)
        keys = (Keys) CopernicusDataGridViewSelectorCell.VkKeyScan(negativeSign[0]);
      return (char.IsDigit((char) e.KeyCode) || e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9 || (keys == e.KeyCode || Keys.Subtract == e.KeyCode)) && (!e.Shift && !e.Alt && !e.Control);
    }

    public override string ToString()
    {
      return "DataGridViewNumericUpDownCell { ColumnIndex=" + this.ColumnIndex.ToString((IFormatProvider) CultureInfo.CurrentCulture) + ", RowIndex=" + this.RowIndex.ToString((IFormatProvider) CultureInfo.CurrentCulture) + " }";
    }

    internal static HorizontalAlignment TranslateAlignment(DataGridViewContentAlignment align)
    {
      if ((align & CopernicusDataGridViewSelectorCell.anyRight) != DataGridViewContentAlignment.NotSet)
        return HorizontalAlignment.Right;
      return (align & CopernicusDataGridViewSelectorCell.anyCenter) != DataGridViewContentAlignment.NotSet ? HorizontalAlignment.Center : HorizontalAlignment.Left;
    }
  }
}
