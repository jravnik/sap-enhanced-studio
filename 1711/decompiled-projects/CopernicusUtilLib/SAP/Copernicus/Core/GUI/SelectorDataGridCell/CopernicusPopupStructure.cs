﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.SelectorDataGridCell.CopernicusPopupStructure
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.GUI.SelectorDataGridCell
{
  public class CopernicusPopupStructure
  {
    private static string strSplitor = " - ";
    private string strDataType;
    private string strDataTypeNamespace;

    public string DataType
    {
      get
      {
        return this.strDataType;
      }
      set
      {
        if (value == null || value.Equals(this.strDataType))
          return;
        this.strDataType = value;
      }
    }

    public string DataTypeNamespace
    {
      get
      {
        return this.strDataTypeNamespace;
      }
      set
      {
        if (value == null || value.Equals(this.strDataTypeNamespace))
          return;
        this.strDataTypeNamespace = value;
      }
    }

    public CopernicusPopupStructure(string datatype = null, string nspace = null)
    {
      this.strDataType = datatype;
      this.strDataTypeNamespace = nspace;
    }

    public override string ToString()
    {
      string str = string.Empty;
      if (this.strDataType != null)
      {
        str += this.strDataType;
        if (this.strDataTypeNamespace != null)
          str = str + CopernicusPopupStructure.strSplitor + this.strDataTypeNamespace;
      }
      return str;
    }
  }
}
