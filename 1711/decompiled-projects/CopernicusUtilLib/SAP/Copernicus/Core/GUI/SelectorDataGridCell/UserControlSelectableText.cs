﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.SelectorDataGridCell.UserControlSelectableText
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.SelectorDataGridCell
{
  public class UserControlSelectableText : UserControl, IDisposable
  {
    private ToolTip toolTip = new ToolTip();
    public string strToolTipTemplate = "Data Type: {0}\nNameSpace: {1}";
    private string strCallbackFormClass = string.Empty;
    private string strCallbackFormPackageName = string.Empty;
    public List<CopernicusDataGridViewSelectorCell> listMonitorInstances = new List<CopernicusDataGridViewSelectorCell>();
    private IContainer components;
    private Button btnSelectAction;
    private TextBox textBoxDataType;
    private SplitContainer splitContainer;
    public Action<EventArgs> OnTextValueCallback;
    private Action<string, string> InvokeAction;
    private bool isUserEdit;
    private int irowIndex;
    private int icolumnIndex;
    private string strDataTypeNameSpace;
    private bool isDisposedInstance;

    private void InitializeComponent()
    {
      this.btnSelectAction = new Button();
      this.textBoxDataType = new TextBox();
      this.splitContainer = new SplitContainer();
      this.splitContainer.BeginInit();
      this.splitContainer.Panel1.SuspendLayout();
      this.splitContainer.Panel2.SuspendLayout();
      this.splitContainer.SuspendLayout();
      this.SuspendLayout();
      this.btnSelectAction.Dock = DockStyle.Right;
      this.btnSelectAction.Location = new Point(3, 0);
      this.btnSelectAction.Name = "btnSelectAction";
      this.btnSelectAction.Size = new Size(27, 23);
      this.btnSelectAction.TabIndex = 0;
      this.btnSelectAction.Text = "...";
      this.btnSelectAction.UseVisualStyleBackColor = true;
      this.btnSelectAction.Click += new EventHandler(this.btnSelectAction_Click);
      this.textBoxDataType.BackColor = SystemColors.ButtonHighlight;
      this.textBoxDataType.BorderStyle = BorderStyle.None;
      this.textBoxDataType.Location = new Point(5, 5);
      this.textBoxDataType.Name = "textBoxDataType";
      this.textBoxDataType.ReadOnly = true;
      this.textBoxDataType.Size = new Size(235, 13);
      this.textBoxDataType.TabIndex = 1;
      this.textBoxDataType.WordWrap = false;
      this.splitContainer.BackColor = SystemColors.ButtonHighlight;
      this.splitContainer.Dock = DockStyle.Fill;
      this.splitContainer.Location = new Point(0, 0);
      this.splitContainer.Name = "splitContainer";
      this.splitContainer.Panel1.Controls.Add((Control) this.textBoxDataType);
      this.splitContainer.Panel2.Controls.Add((Control) this.btnSelectAction);
      this.splitContainer.Size = new Size(276, 23);
      this.splitContainer.SplitterDistance = 245;
      this.splitContainer.SplitterWidth = 1;
      this.splitContainer.TabIndex = 2;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.Controls.Add((Control) this.splitContainer);
      this.Name = nameof (UserControlSelectableText);
      this.Size = new Size(276, 23);
      this.splitContainer.Panel1.ResumeLayout(false);
      this.splitContainer.Panel1.PerformLayout();
      this.splitContainer.Panel2.ResumeLayout(false);
      this.splitContainer.EndInit();
      this.splitContainer.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public UserControlSelectableText()
    {
      this.InitializeComponent();
      this.toolTip.AutoPopDelay = 6000;
      this.toolTip.InitialDelay = 500;
      this.toolTip.ReshowDelay = 500;
      this.toolTip.ShowAlways = true;
      this.toolTip.UseAnimation = true;
      this.toolTip.UseFading = true;
      this.textBoxDataType.TextChanged += new EventHandler(this.textBoxDataType_TextChanged);
      this.OnTextValueCallback += new Action<EventArgs>(this.OnValueChanged);
      this.btnSelectAction.Width = 25;
    }

    public void TooltipUpdate(string strCellDataType, string strCellNamespace)
    {
      if (this.InvokeRequired)
      {
        if (this.InvokeAction == null)
          this.InvokeAction = new Action<string, string>(this.UpdateUITooltips);
        this.BeginInvoke((Delegate) this.InvokeAction, (object) strCellDataType, (object) strCellNamespace);
      }
      else
        this.UpdateUITooltips(strCellDataType, strCellNamespace);
    }

    private void UpdateUITooltips(string strCellDataType, string strCellNamespace)
    {
      if (strCellDataType.Length <= 0 && strCellNamespace.Length <= 0)
        return;
      this.toolTip.SetToolTip((Control) this.splitContainer, string.Format(this.strToolTipTemplate, (object) strCellDataType, (object) strCellNamespace));
      this.toolTip.SetToolTip((Control) this.textBoxDataType, string.Format(this.strToolTipTemplate, (object) strCellDataType, (object) strCellNamespace));
      this.toolTip.SetToolTip((Control) this.btnSelectAction, string.Format(this.strToolTipTemplate, (object) strCellDataType, (object) strCellNamespace));
      this.toolTip.SetToolTip((Control) this.splitContainer.Panel1, string.Format(this.strToolTipTemplate, (object) strCellDataType, (object) strCellNamespace));
      this.toolTip.SetToolTip((Control) this.splitContainer.Panel2, string.Format(this.strToolTipTemplate, (object) strCellDataType, (object) strCellNamespace));
    }

    protected virtual void OnValueChanged(EventArgs e)
    {
    }

    private void textBoxDataType_TextChanged(object sender, EventArgs e)
    {
      this.OnTextValueCallback(e);
    }

    public string Value
    {
      get
      {
        return this.textBoxDataType.Text;
      }
      set
      {
        if (value == null || value.Equals(this.textBoxDataType.Text))
          return;
        this.textBoxDataType.Text = value;
      }
    }

    public bool UserEdit
    {
      get
      {
        return this.isUserEdit;
      }
      set
      {
        if (value == this.isUserEdit)
          return;
        this.isUserEdit = value;
      }
    }

    public string CallbackModelFormClass
    {
      get
      {
        return this.strCallbackFormClass;
      }
      set
      {
        if (value == null || value.Equals(this.strCallbackFormClass))
          return;
        this.strCallbackFormClass = value;
      }
    }

    public string CallbackModelFormPackageName
    {
      get
      {
        return this.strCallbackFormPackageName;
      }
      set
      {
        if (value == null || value.Equals(this.strCallbackFormPackageName))
          return;
        this.strCallbackFormPackageName = value;
      }
    }

    public HorizontalAlignment TextAlign
    {
      get
      {
        return this.textBoxDataType.TextAlign;
      }
      set
      {
        if (value == this.textBoxDataType.TextAlign)
          return;
        this.textBoxDataType.TextAlign = value;
      }
    }

    private void btnSelectAction_Click(object sender, EventArgs e)
    {
      this.PerformPopupPlugin();
    }

    private void textBoxDataType_Click(object sender, EventArgs e)
    {
      this.PerformPopupPlugin();
    }

    internal void PerformClickButton()
    {
      this.PerformPopupPlugin();
    }

    private void PerformPopupPlugin()
    {
      if (this.CallbackModelFormPackageName == null || this.CallbackModelFormPackageName.Length == 0)
        throw new InvalidOperationException("CallbackModalForm Interface hasn't been implemented - CallbackModelFromPackageName is null");
      Type type = Assembly.Load(this.CallbackModelFormPackageName).GetType(this.CallbackModelFormClass);
      if (this.CallbackModelFormClass == null || this.CallbackModelFormClass.Length == 0)
        throw new InvalidOperationException("CallbackModalForm Interface hasn't been implemented - CallbackModelFormClass is null");
      if ((Type) null == type)
        throw new InvalidOperationException("CallbackModalForm Interface is incorrect Property");
      CopernicusPopupStructure copernicusPopupStructure = (Activator.CreateInstance(type) as CopernicusPopupForm).ShowModalDialog(this.Value, this.strDataTypeNameSpace);
      this.textBoxDataType.Text = copernicusPopupStructure.DataType;
      this.strDataTypeNameSpace = copernicusPopupStructure.DataTypeNamespace;
      if (copernicusPopupStructure != null && copernicusPopupStructure.DataTypeNamespace != null)
        this.FireTagChangeEvent(copernicusPopupStructure.DataTypeNamespace, this.irowIndex, this.icolumnIndex);
      this.UpdateUITooltips(copernicusPopupStructure.DataType, copernicusPopupStructure.DataTypeNamespace);
    }

    public event Action<string, int, int> OnTagNamespaceCallback;

    public int IRowIndex
    {
      get
      {
        return this.irowIndex;
      }
      set
      {
        this.irowIndex = value;
      }
    }

    public int IColumnIndex
    {
      get
      {
        return this.icolumnIndex;
      }
      set
      {
        this.icolumnIndex = value;
      }
    }

    public string DataNameSpace
    {
      get
      {
        return this.strDataTypeNameSpace;
      }
      set
      {
        if (value == null || value.Equals(this.strDataTypeNameSpace))
          return;
        this.strDataTypeNameSpace = value;
      }
    }

    public void AddTagChangeMonitor(Action<string, int, int> callback, CopernicusDataGridViewSelectorCell _monitorInstance)
    {
      if (this.listMonitorInstances.Contains(_monitorInstance))
        return;
      this.OnTagNamespaceCallback += callback;
    }

    private void FireTagChangeEvent(string strNewDataTypeNameSpace, int iRowIndex, int iColumnIndex)
    {
      if (this.OnTagNamespaceCallback == null)
        return;
      Action<string, int, int> action = Interlocked.CompareExchange<Action<string, int, int>>(ref this.OnTagNamespaceCallback, (Action<string, int, int>) null, (Action<string, int, int>) null);
      if (action == null)
        return;
      action(strNewDataTypeNameSpace, iRowIndex, iColumnIndex);
    }

    public bool IsDisposedAlready
    {
      get
      {
        return this.isDisposedInstance;
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
      this.isDisposedInstance = disposing;
    }

    protected void ProcessKey(Keys key, out bool isProcessedInside)
    {
      if (!this.textBoxDataType.Focused)
      {
        isProcessedInside = false;
      }
      else
      {
        isProcessedInside = false;
        if (key == Keys.Left)
        {
          if ((this.RightToLeft != RightToLeft.No || this.textBoxDataType.SelectionLength == 0 && this.textBoxDataType.SelectionStart == 0) && (this.RightToLeft != RightToLeft.Yes || this.textBoxDataType.SelectionLength == 0 && this.textBoxDataType.SelectionStart == this.textBoxDataType.Text.Length))
            return;
          isProcessedInside = true;
        }
        else if (key == Keys.Right)
        {
          if ((this.RightToLeft != RightToLeft.No || this.textBoxDataType.SelectionLength == 0 && this.textBoxDataType.SelectionStart == this.textBoxDataType.Text.Length) && (this.RightToLeft != RightToLeft.Yes || this.textBoxDataType.SelectionLength == 0 && this.textBoxDataType.SelectionStart == 0))
            return;
          isProcessedInside = true;
        }
        else if (key == Keys.End)
        {
          if (this.textBoxDataType.SelectionLength == this.textBoxDataType.Text.Length)
            return;
          isProcessedInside = true;
        }
        else
        {
          if (key != Keys.Delete || this.textBoxDataType.SelectionLength <= 0 && this.textBoxDataType.SelectionStart >= this.textBoxDataType.Text.Length)
            return;
          isProcessedInside = true;
        }
      }
    }

    protected bool isFocusedText(out string strCurrentTextValue)
    {
      strCurrentTextValue = this.textBoxDataType.Text;
      return this.textBoxDataType.Focused;
    }
  }
}
