﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckComboxDataGridCell.GetCodeListbyFilterName
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.Core.GUI.CheckComboxDataGridCell
{
  public delegate PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC[] GetCodeListbyFilterName(string filterName);
}
