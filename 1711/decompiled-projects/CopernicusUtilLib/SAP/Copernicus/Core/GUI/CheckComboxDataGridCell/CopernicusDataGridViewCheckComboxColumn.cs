﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckComboxDataGridCell.CopernicusDataGridViewCheckComboxColumn
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.CheckComboxDataGridCell
{
  public class CopernicusDataGridViewCheckComboxColumn : DataGridViewColumn
  {
    private static string strCellTemplateError = "Value provided for CellTemplate must be of type SAP.Copernicus.Core.GUI.CheckVariableDataGridCell.CopernicusDataGridViewCheckVariableCell or derive from it.";

    public CopernicusDataGridViewCheckComboxColumn()
      : base((DataGridViewCell) new CopernicusDataGridViewCheckComboxCell())
    {
    }

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public override DataGridViewCell CellTemplate
    {
      get
      {
        return base.CellTemplate;
      }
      set
      {
        CopernicusDataGridViewCheckComboxCell viewCheckComboxCell = value as CopernicusDataGridViewCheckComboxCell;
        if (value != null && viewCheckComboxCell == null)
          throw new InvalidCastException(CopernicusDataGridViewCheckComboxColumn.strCellTemplateError);
        base.CellTemplate = value;
      }
    }

    [Description("Callback Form With SAP.Copernicus.Core.GUI.CustomizedDataGridCell.CallbackFormInterface.")]
    [Category("Behavior")]
    [DefaultValue(false)]
    public bool IsRequiredCellEnable
    {
      get
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusCheckVariableCellTemplate.IsEnableRequiredFilterValue;
      }
      set
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusCheckVariableCellTemplate.IsEnableRequiredFilterValue = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewCheckComboxCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewCheckComboxCell;
          if (cell != null)
            cell.SetIsEnableRequiredValue(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    [Description("Enable Filter Value Selection by Default Value for Column.")]
    [Category("Behavior")]
    [DefaultValue(false)]
    public bool IsFilterValueSelectableForCell
    {
      get
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusCheckVariableCellTemplate.IsEnableRequiredFilterValue;
      }
      set
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusCheckVariableCellTemplate.IsFilterValueSelectableForCell = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewCheckComboxCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewCheckComboxCell;
          if (cell != null)
            cell.SetCallbackFormIsFilterValueSelectableForCell(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    [DefaultValue("")]
    [Description("Callback Form With SAP.Copernicus.Core.GUI.CheckVariableDataGridCell.CopernicusFilterPopupForm.")]
    [Category("Behavior")]
    public string CallbackFormHandler
    {
      get
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusCheckVariableCellTemplate.strCallbackFormHandler;
      }
      set
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusCheckVariableCellTemplate.strCallbackFormHandler = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewCheckComboxCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewCheckComboxCell;
          if (cell != null)
            cell.SetCallbackFormHandler(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    [Description("Callback Form Assembly Name.")]
    [Category("Behavior")]
    [DefaultValue("")]
    public string CallbackFormPackageName
    {
      get
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusCheckVariableCellTemplate.CallbackFormPackageName;
      }
      set
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusCheckVariableCellTemplate.CallbackFormPackageName = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewCheckComboxCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewCheckComboxCell;
          if (cell != null)
            cell.SetCallbackFormPackageName(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    public CopernicusPopupClassMetaData FilterClassMetaData
    {
      get
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusCheckVariableCellTemplate.FilterClassMetaDataForCell;
      }
      set
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusCheckVariableCellTemplate.FilterClassMetaDataForCell = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewCheckComboxCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewCheckComboxCell;
          if (cell != null)
            cell.SetFilterClassMetaDataForCell(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    private CopernicusDataGridViewCheckComboxCell CopernicusCheckVariableCellTemplate
    {
      get
      {
        return (CopernicusDataGridViewCheckComboxCell) this.CellTemplate;
      }
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder(100);
      stringBuilder.Append("CopernicusDataGridViewCheckVariableColumn { Name=");
      stringBuilder.Append(this.Name);
      stringBuilder.Append(", Index=");
      stringBuilder.Append(this.Index.ToString((IFormatProvider) CultureInfo.CurrentCulture));
      stringBuilder.Append(" }");
      return stringBuilder.ToString();
    }
  }
}
