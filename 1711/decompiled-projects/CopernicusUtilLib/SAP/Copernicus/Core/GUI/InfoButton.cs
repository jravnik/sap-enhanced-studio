﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.InfoButton
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI
{
  public class InfoButton : Label
  {
    private string infoText;
    private IContainer components;
    private ToolTip toolTip1;

    public void setInfoText(string t)
    {
      this.infoText = t;
      this.toolTip1.SetToolTip((Control) this, this.infoText);
    }

    public override string Text
    {
      get
      {
        return "";
      }
      set
      {
        base.Text = "";
        this.infoText = value;
      }
    }

    public InfoButton()
    {
      this.InitializeComponent();
      try
      {
        this.Image = EntityImageList.ImageList.Images[56];
      }
      catch (Exception ex)
      {
      }
      this.AutoSize = false;
      this.Size = new Size(16, 16);
      this.Click += new EventHandler(this.infoClick);
    }

    private void infoClick(object sender, EventArgs e)
    {
      int num = (int) MessageBox.Show(this.infoText, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.toolTip1 = new ToolTip(this.components);
      this.SuspendLayout();
      this.ResumeLayout(false);
    }
  }
}
