﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Properties.PropertyAccess
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Properties
{
  public static class PropertyAccess
  {
    private static OptionPageFields generalProps;

    public static OptionPageFields GeneralProps
    {
      set
      {
        PropertyAccess.generalProps = value;
      }
      get
      {
        if (PropertyAccess.generalProps == null)
          PropertyAccess.generalProps = new OptionPageFields();
        return PropertyAccess.generalProps;
      }
    }
  }
}
