﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Properties.OptionPageFields
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.Shell;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Util;
using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Xml;

namespace SAP.Copernicus.Core.Properties
{
    [CLSCompliant(false)]
    [ComVisible(true)]
    public class OptionPageFields : DialogPage
    {
        public bool isLocalBuild = true;
        private string proxyURL = "http://proxy:8080";
        private int connectivityTimeout = 1800000;
        private int xRepSessionTimeOut = 31536000;
        private bool zipJson = true;
        private string helpUrl = "https://help.sap.com/viewer/p/SAP_CLOUD_APPLICATIONS_STUDIO";
        private string helpContentInstallationURL = "http://help.sap.com/saphelp_studio_1402/KTP/Products/bf889ebdf45d4dd19d4a0e0bd39a07fb/GettingStarted/IDE/QG_HelpContent.html";
        private string csnUrl = "http://scn.sap.com/community/business-bydesign/studio";
        private OptionsTriState activateBeforeDebugging = OptionsTriState.Ask;
        public string buildNumber;
        public string buildDate;
        public string buildTaktNumber;
        public string buildLastCL;
        public string release;
        public string codeline;
        public bool isJaproBuild;
        private bool autoUpdateWanted;
        private bool updateOrInstallLater;
        private DateTime updateReminderDelay;
        private int updateReminderDelayDays;
        private bool useMinimalSAPNamespaces;
        private bool proxySupport;
        private bool proxyAuthentication;
        private string proxyUser;
        private string proxyPwd;
        private bool tracingActiveForUser;
        private string tracingUser;
        private bool xboEnhancementNoBrowser;

        public bool AutoUpdateWanted
        {
            get
            {
                return this.autoUpdateWanted;
            }
            set
            {
                this.autoUpdateWanted = value;
            }
        }

        public bool UpdateOrInstallLater
        {
            get
            {
                return this.updateOrInstallLater;
            }
            set
            {
                this.updateOrInstallLater = value;
            }
        }

        public DateTime UpdateReminderDelay
        {
            get
            {
                return this.updateReminderDelay;
            }
            set
            {
                this.updateReminderDelay = value;
            }
        }

        public int UpdateReminderDelayDays
        {
            get
            {
                return this.updateReminderDelayDays;
            }
            set
            {
                this.updateReminderDelayDays = value;
            }
        }

        public string RepositoryBaseComponent
        {
            get
            {
                return "SAP_LEAP";
            }
        }

        public string RepositoryVersion
        {
            get
            {
                return "1711";
            }
        }

        public string RepositoryRelease
        {
            get
            {
                return "1711";
            }
        }

        public string LogonPatchLevel
        {
            get
            {
                return "0";
            }
        }

        public bool UseMinimalSAPNamespaces
        {
            get
            {
                return this.useMinimalSAPNamespaces;
            }
            set
            {
                this.useMinimalSAPNamespaces = value;
            }
        }

        public bool HTTPSSupport
        {
            get
            {
                return true;
            }
        }

        public int ConnectivityTimeout
        {
            get
            {
                return this.connectivityTimeout;
            }
        }

        public string SystemConnections { get; set; }

        public bool ProxySupport
        {
            get
            {
                return this.proxySupport;
            }
            set
            {
                this.proxySupport = value;
            }
        }

        public bool ProxyAuthentication
        {
            get
            {
                return this.proxyAuthentication;
            }
            set
            {
                this.proxyAuthentication = value;
            }
        }

        public string ProxyURL
        {
            get
            {
                return this.proxyURL;
            }
            set
            {
                this.proxyURL = value;
            }
        }

        public string ProxyUser
        {
            get
            {
                return this.proxyUser;
            }
            set
            {
                this.proxyUser = value;
            }
        }

        public string ProxyPwd
        {
            get
            {
                return this.proxyPwd;
            }
            set
            {
                this.proxyPwd = value;
            }
        }

        public int XRepSessionTimeOut
        {
            get
            {
                return this.xRepSessionTimeOut;
            }
            set
            {
                this.xRepSessionTimeOut = value;
            }
        }

        public string VSPath { get; set; }

        public string ContentTraceLocation
        {
            get
            {
                return TraceUtil.Instance.Location;
            }
            set
            {
                TraceUtil.Instance.Location = value;
            }
        }

        public bool ZipJson
        {
            get
            {
                return this.zipJson;
            }
            set
            {
                this.zipJson = value;
            }
        }

        public string HelpURL
        {
            get
            {
                return this.helpUrl;
            }
        }

        public string HelpContentInstallationURL
        {
            get
            {
                return this.helpContentInstallationURL;
            }
        }

        public string CsnUrl
        {
            get
            {
                return this.csnUrl;
            }
        }

        public OptionsTriState ActivateBeforeDebugging
        {
            get
            {
                return this.activateBeforeDebugging;
            }
            set
            {
                this.activateBeforeDebugging = value;
            }
        }

        public bool TracingActiveForUser
        {
            get
            {
                return this.tracingActiveForUser;
            }
            set
            {
                this.tracingActiveForUser = value;
            }
        }

        public string TracingUser
        {
            get
            {
                return this.tracingUser;
            }
            set
            {
                this.tracingUser = value;
            }
        }

        public bool XBOEnhancementNoBrowser
        {
            get
            {
                return this.xboEnhancementNoBrowser;
            }
            set
            {
                this.xboEnhancementNoBrowser = value;
            }
        }

        public void ReadConnectionDataFromProps()
        {
            ConnectionDataSet connections = Connection.getInstance().Connections;
            connections.Tables[0].Clear();
            TextReader input = (TextReader)new StringReader(this.SystemConnections);
            int num = (int)connections.ReadXml((XmlReader)new XmlTextReader(input));
        }

        public void SaveConnectionDataToProps()
        {
            ConnectionDataSet connections = Connection.getInstance().Connections;
            TextWriter w = (TextWriter)new StringWriter();
            connections.WriteXml((XmlWriter)new XmlTextWriter(w));
            this.SystemConnections = w.ToString();
        }

        public void ResetConnectionDataToDefault()
        {
            Connection.getInstance().ResetToDefault();
            this.SaveConnectionDataToProps();
        }

        public void Initialize()
        {
            if (this.SystemConnections != null && this.SystemConnections.Length > 0)
                this.ReadConnectionDataFromProps();
            Assembly callingAssembly = Assembly.GetCallingAssembly();
            StreamReader streamReader = new StreamReader(callingAssembly.GetManifestResourceStream("SAP.Copernicus.Build.version.properties"));
            while (streamReader.Peek() != -1)
            {
                string str1 = streamReader.ReadLine();
                if (str1 != null && str1.Length > 0 && str1.Contains("="))
                {
                    string[] strArray = str1.Split('=');
                    string str2 = strArray[0];
                    string s = strArray[1];
                    if (s != null && !s.StartsWith("@") && !s.StartsWith("$"))
                    {
                        switch (str2)
                        {
                            case "buildNumber":
                                this.isLocalBuild = false;
                                this.buildNumber = s;
                                if (s.Length == 0)
                                {
                                    this.isJaproBuild = true;
                                    this.buildNumber = callingAssembly.GetName().Version.ToString();
                                    this.release = this.buildNumber.Split('.')[0];
                                    continue;
                                }
                                continue;
                            case "buildTimeStamp":
                                this.buildDate = s;
                                this.buildTaktNumber = string.Concat((object)((int)DateTime.Parse(s, (IFormatProvider)new DateTimeFormatInfo()
                                {
                                    FullDateTimePattern = "yyyy'-'MM'-'dd HH':'mm':'ss"
                                }).Subtract(new DateTime(2009, 2, 2)).TotalDays / 14 + 1));
                                continue;
                            case "changeListNumber":
                                this.buildLastCL = s;
                                continue;
                            case "release":
                                this.codeline = s;
                                continue;
                            default:
                                continue;
                        }
                    }
                }
            }
            streamReader.Close();
        }
    }
}
