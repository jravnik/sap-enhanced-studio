﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Tracing.ITracingService
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.InteropServices;

namespace SAP.Copernicus.Core.Tracing
{
  [Guid("92F4A732-9668-43FE-A07A-D90B5FCEC4B2")]
  public interface ITracingService
  {
    bool IsActive { get; }

    void ActivateTrace(bool silent = false);

    void DeactivateTrace(bool silent = false);
  }
}
