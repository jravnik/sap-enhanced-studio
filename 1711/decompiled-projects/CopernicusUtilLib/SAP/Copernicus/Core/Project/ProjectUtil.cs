﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Project.ProjectUtil
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.Automation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace SAP.Copernicus.Core.Project
{
  public static class ProjectUtil
  {
    private static ProjectProperties mockedProject = ProjectProperties.EMPTY;
    private static string mockedSolutionName = (string) null;

    public static void SetMockedProject(string solutionFilePath, ProjectProperties projectProperties)
    {
      if (string.IsNullOrEmpty(solutionFilePath))
        throw new ArgumentNullException(nameof (solutionFilePath));
      if (projectProperties == null)
        throw new ArgumentNullException(nameof (projectProperties));
      string directoryName = Path.GetDirectoryName(solutionFilePath);
      ProjectUtil.mockedSolutionName = Path.GetFileNameWithoutExtension(solutionFilePath);
      ProjectUtil.mockedProject = projectProperties;
      Trace.WriteLine(string.Format("Adding mocked project properties for {0} {1}", (object) directoryName, (object) ProjectUtil.mockedSolutionName));
    }

    private static ProjectProperties GetMockedPropertiesFor(string fileName)
    {
      if (string.IsNullOrEmpty(fileName))
        return ProjectProperties.EMPTY;
      Path.GetDirectoryName(fileName);
      return ProjectUtil.mockedProject;
    }

    public static ProjectItem GetProjectItemForFile(string fileName)
    {
      Solution2 solution = DTEUtil.GetDTE().Solution as Solution2;
      if (solution == null || !solution.IsOpen)
        return (ProjectItem) null;
      return solution.FindProjectItem(fileName);
    }

    public static EnvDTE.Project GetProjectForFile(string fileName)
    {
      Solution2 solution = DTEUtil.GetDTE().Solution as Solution2;
      if (solution == null || !solution.IsOpen)
        return (EnvDTE.Project) null;
      ProjectItem projectItem = solution.FindProjectItem(fileName);
      if (projectItem != null)
        return projectItem.ContainingProject;
      foreach (EnvDTE.Project project in solution.Projects)
      {
        string directoryName = Path.GetDirectoryName(project.FullName);
        if (Path.GetFullPath(fileName).StartsWith(directoryName))
          return project;
      }
      return (EnvDTE.Project) null;
    }

    public static string GetProjectNamespaceForFile(string fileName)
    {
      if (string.IsNullOrEmpty(fileName))
        return (string) null;
      if (!DTEUtil.IsInitialized)
        return ProjectUtil.GetMockedPropertiesFor(fileName).RepositoryNamespace;
      EnvDTE.Project projectForFile = ProjectUtil.GetProjectForFile(fileName);
      if (projectForFile == null)
        return (string) null;
      IVsBuildPropertyStorage buildPropertyStorage = projectForFile.Object as IVsBuildPropertyStorage;
      if (buildPropertyStorage == null)
        return (string) null;
      string pbstrPropValue;
      buildPropertyStorage.GetPropertyValue("RepositoryNamespace", (string) null, 1U, out pbstrPropValue);
      return pbstrPropValue;
    }

    public static ProjectProperties GetProjectPropertiesForFile(string fileName)
    {
      if (string.IsNullOrEmpty(fileName))
        return ProjectProperties.EMPTY;
      if (!DTEUtil.IsInitialized)
        return ProjectUtil.GetMockedPropertiesFor(fileName);
      EnvDTE.Project projectForFile = ProjectUtil.GetProjectForFile(fileName);
      if (projectForFile == null)
        return ProjectProperties.EMPTY;
      IVsBuildPropertyStorage buildPropertyStorage = projectForFile.Object as IVsBuildPropertyStorage;
      if (buildPropertyStorage == null)
        return ProjectProperties.EMPTY;
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      string str1 = "DefaultProcessComponent";
      string pbstrPropValue;
      buildPropertyStorage.GetPropertyValue(str1, (string) null, 1U, out pbstrPropValue);
      dictionary.Add(str1, pbstrPropValue);
      string str2 = "Name";
      buildPropertyStorage.GetPropertyValue(str2, (string) null, 1U, out pbstrPropValue);
      dictionary.Add(str2, pbstrPropValue);
      string str3 = "RepositoryNamespace";
      buildPropertyStorage.GetPropertyValue(str3, (string) null, 1U, out pbstrPropValue);
      dictionary.Add(str3, pbstrPropValue);
      string str4 = "RepositoryRootFolder";
      buildPropertyStorage.GetPropertyValue(str4, (string) null, 1U, out pbstrPropValue);
      dictionary.Add(str4, pbstrPropValue);
      string str5 = "RuntimeNamespacePrefix";
      buildPropertyStorage.GetPropertyValue(str5, (string) null, 1U, out pbstrPropValue);
      dictionary.Add(str5, pbstrPropValue);
      string str6 = "BCSourceFolderInXRep";
      buildPropertyStorage.GetPropertyValue(str6, (string) null, 1U, out pbstrPropValue);
      dictionary.Add(str6, pbstrPropValue);
      string str7 = "ProjectSourceFolderinXRep";
      buildPropertyStorage.GetPropertyValue(str7, (string) null, 1U, out pbstrPropValue);
      dictionary.Add(str7, pbstrPropValue);
      string str8 = "DeploymentUnit";
      buildPropertyStorage.GetPropertyValue(str8, (string) null, 1U, out pbstrPropValue);
      dictionary.Add(str8, pbstrPropValue);
      string str9 = "CompilerVersion";
      buildPropertyStorage.GetPropertyValue(str9, (string) null, 1U, out pbstrPropValue);
      dictionary.Add(str9, pbstrPropValue);
      return new ProjectProperties((IDictionary<string, string>) dictionary);
    }

    public static string GetOpenSolutionName()
    {
      if (!DTEUtil.IsInitialized)
        return ProjectUtil.mockedSolutionName;
      Solution2 solution = (Solution2) DTEUtil.GetDTE().Solution;
      if (solution == null || !solution.IsOpen)
        return (string) null;
      return solution.Properties.Item("Name").Value.ToString();
    }

    public static string GetRuntimeNamespacePrefixForPartnerNamespace(string namespaceName)
    {
      if (!DTEUtil.IsInitialized)
      {
        if (ProjectUtil.mockedProject != null && ProjectUtil.mockedProject.RepositoryNamespace == namespaceName)
          return ProjectUtil.mockedProject.RuntimeNamespacePrefix;
        return (string) null;
      }
      Solution2 solution = (Solution2) DTEUtil.GetDTE().Solution;
      if (solution == null || !solution.IsOpen)
        return (string) null;
      foreach (EnvDTE.Project project in solution.Projects)
      {
        IVsBuildPropertyStorage buildPropertyStorage = project.Object as IVsBuildPropertyStorage;
        if (buildPropertyStorage != null)
        {
          string pbstrPropValue1;
          buildPropertyStorage.GetPropertyValue("RepositoryNamespace", (string) null, 1U, out pbstrPropValue1);
          string pbstrPropValue2;
          buildPropertyStorage.GetPropertyValue("RuntimeNamespacePrefix", (string) null, 1U, out pbstrPropValue2);
          if (namespaceName == pbstrPropValue1)
            return pbstrPropValue2;
        }
      }
      return (string) null;
    }

    public static IList<string> GetNamespacesFromOpenSolution()
    {
      List<string> stringList = new List<string>();
      if (!DTEUtil.IsInitialized)
      {
        if (ProjectUtil.mockedProject == null)
          return (IList<string>) stringList;
        stringList.Add(ProjectUtil.mockedProject.RepositoryNamespace);
        return (IList<string>) stringList;
      }
      Solution2 solution = (Solution2) DTEUtil.GetDTE().Solution;
      if (solution == null || !solution.IsOpen)
        return (IList<string>) stringList;
      foreach (EnvDTE.Project project in solution.Projects)
      {
        IVsBuildPropertyStorage buildPropertyStorage = project.Object as IVsBuildPropertyStorage;
        if (buildPropertyStorage != null)
        {
          string pbstrPropValue = (string) null;
          buildPropertyStorage.GetPropertyValue("RepositoryNamespace", (string) null, 1U, out pbstrPropValue);
          if (!string.IsNullOrEmpty(pbstrPropValue))
            stringList.Add(pbstrPropValue);
        }
      }
      return (IList<string>) stringList;
    }
  }
}
