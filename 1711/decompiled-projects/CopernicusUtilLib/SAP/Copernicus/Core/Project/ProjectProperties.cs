﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Project.ProjectProperties
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Project
{
  [CLSCompliant(true)]
  public class ProjectProperties
  {
    internal static readonly ProjectProperties EMPTY = new ProjectProperties((IDictionary<string, string>) new Dictionary<string, string>());
    public const string RepositoryNamespaceProperty = "RepositoryNamespace";
    public const string RuntimeNamespacePrefixProperty = "RuntimeNamespacePrefix";
    public const string RepositoryRootFolderProperty = "RepositoryRootFolder";
    public const string DefaultProcessComponentProperty = "DefaultProcessComponent";
    public const string ProjectNameProperty = "Name";
    public const string BCSourceFolderInXRepProperty = "BCSourceFolderInXRep";
    public const string ProjectSourceFolderinXRepProperty = "ProjectSourceFolderinXRep";
    public const string DevelopmentPackageProperty = "DevelopmentPackage";
    public const string XRepSolutionProperty = "XRepSolution";
    public const string DeploymentUnitProperty = "DeploymentUnit";
    public const string CompilerVersionProperty = "CompilerVersion";
    private readonly IDictionary<string, string> props;

    public ProjectProperties(IDictionary<string, string> initialProps)
    {
      this.props = (IDictionary<string, string>) new Dictionary<string, string>(initialProps);
    }

    public string RepositoryNamespace
    {
      get
      {
        return this.Get(nameof (RepositoryNamespace));
      }
    }

    public string RuntimeNamespacePrefix
    {
      get
      {
        return this.Get(nameof (RuntimeNamespacePrefix));
      }
    }

    public string RepositoryRootFolder
    {
      get
      {
        return this.Get(nameof (RepositoryRootFolder));
      }
    }

    public string DefaultProcessComponent
    {
      get
      {
        return this.Get(nameof (DefaultProcessComponent));
      }
    }

    public string ProjectName
    {
      get
      {
        return this.Get("Name");
      }
    }

    public string BCSourceFolderInXRep
    {
      get
      {
        return this.Get(nameof (BCSourceFolderInXRep));
      }
    }

    public string ProjectSourceFolderinXRep
    {
      get
      {
        return this.Get(nameof (ProjectSourceFolderinXRep));
      }
    }

    public string DevelopmentPackage
    {
      get
      {
        return this.Get(nameof (DevelopmentPackage));
      }
    }

    public string XRepSolution
    {
      get
      {
        return this.Get(nameof (XRepSolution));
      }
    }

    public string DeploymentUnit
    {
      get
      {
        return this.Get(nameof (DeploymentUnit));
      }
    }

    public string CompilerVersion
    {
      get
      {
        return this.Get(nameof (CompilerVersion));
      }
    }

    public string Get(string name)
    {
      string empty;
      this.props.TryGetValue(name, out empty);
      if (empty == null)
        empty = string.Empty;
      return empty;
    }
  }
}
