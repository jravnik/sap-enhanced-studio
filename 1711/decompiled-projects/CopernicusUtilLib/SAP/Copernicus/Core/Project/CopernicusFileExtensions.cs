﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Project.CopernicusFileExtensions
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Project
{
  public class CopernicusFileExtensions
  {
    public const string PROJECT_FILE_EXTENSION = ".myproj";
    public const string SOLUTION_FILE_EXTENSION = ".sln";
    public const string ABAP_SCRIPTFILE_EXTENSION = ".absl";
    public const string BUSINESS_OBJECT_EXTENSION = ".bo";
    public const string APPROVALPROCESS_EXTENSION = ".approvalprocess";
    public const string APPROVAL_EXTENSION = ".approval";
    public const string WEBSERVICE_EXTENSION = ".webservice";
    public const string EXTERNAL_WEBSERVICE_EXTENSION_EXTENSION = ".xwebservice";
    public const string BUSINESSCONFIGURATION_EXTENSION = ".bac";
    public const string BCSET_EXTENSION = ".bcc";
    public const string TAX_DECISION_TREE_EXTENSION = ".bcctax";
    public const string IMPL_PROJECT_TEMPLATE_EXTENSION = ".bct";
    public const string IMPL_PROJECT_TEMPLATE_EXTENSION_CLOSED = ".bctclosed";
    public const string BCO_EXTENSION = ".bco";
    public const string EXTENSION_ENTITY_EXTENSION = ".xbo";
    public const string EXTENSION_ENTITY_FMT_EXTENSION = ".xft";
    public const string EXTENSION_ENTITY_FMT_EXT_XML = ".xftx";
    public const string EXTENSION_FIELD_PROPERTIES = ".xfp";
    public const string FORMS_XDP_EXTENSION = ".xdp";
    public const string FORMS_FTHD_EXTENSION = ".fthd";
    public const string FORMS_FTGD_EXTENSION = ".ftgd";
    public const string FORMS_XSD_EXTENSION = ".xsd";
    public const string PROCESS_INTEGRATION_EXTENSION = ".pid";
    public const string QUERYDEF_EXTENSION = ".qry";
    public const string DATASOURCE_EXTENSION = ".ds";
    public const string SAM_EXTENSION = ".sam";
    public const string SAM_DIAGRAM_EXTENSION = ".diagram";
    public const string SAM_DIAGRAM_EXTENSION_FULL = ".sam.diagram";
    public const string NODE_EXTENSION = ".node";
    public const string SADL_EXTENSION = ".sadl";
    public const string EXTENSION_SCENARIO_EXTENSION = ".xs";
    public const string NODE_EXTENSION_SCENARIO = ".nxs";
    public const string CUSTOMER_OBJECT_REFERENCES_EXTENSION = ".ref";
    public const string CUSTOM_REUSE_LIBRARY = ".library";
    public const string PDD_EXTENSION = ".pdd";
    public const string UI_COMPONENT_EXTENSION = ".uicomponent";
    public const string UI_DATA_OBJECT_EXTENSION = ".uidataobject";
    public const string UI_SWITCH_EXTENSION = ".uiswitch";
    public const string UI_ANCHOR_EXTENSION = ".uianchor";
    public const string UI_WCVIEW_WOCVIEW = ".WCVIEW.uiwocview";
    public const string UI_WOCVIEW_EXTENSION = ".uiwocview";
    public const string UI_WCF_WOC = ".WCF.uiwoc";
    public const string UI_WOC_EXTENSION = ".uiwoc";
    public const string X_UI_COMPONENT_EXTENSION = ".xuicomponent";
    public const string X_OIF_EXTENSION = ".OIF.xuicomponent";
    public const string X_QAF_EXTENSION = ".QA.xuicomponent";
    public const string X_OWL_EXTENSION = ".OWL.xuicomponent";
    public const string X_FS_EXTENSION = ".FS.xuicomponent";
    public const string X_GAF_EXTENSION = ".GAF.xuicomponent";
    public const string EMBEDDED_COMPONENT_EXTENSION = ".EC.uicomponent";
    public const string OIF_EXTENSION = ".OIF.uicomponent";
    public const string QAF_EXTENSION = ".QA.uicomponent";
    public const string OWL_EXTENSION = ".OWL.uicomponent";
    public const string OVS_EXTENSION = ".OVS.uicomponent";
    public const string FS_EXTENSION = ".FS.uicomponent";
    public const string PTP_EXTENSION = ".PTP.uicomponent";
    public const string GAF_EXTENSION = ".GAF.uicomponent";
    public const string MD_EXTENSION = ".MD.uicomponent";
    public const string QC_EXTENSION = ".QC.uicomponent";
    public const string QV_EXTENSION = ".QV.uicomponent";
    public const string TI_EXTENSION = ".TI.uicomponent";
    public const string TT_EXTENSION = ".TT.uidataobject";
    public const string BO_TT_REF_EXTENSION = ".bott";
    public const string TASKLIST_VIEW_EXTENSION = ".tasklistview";
    public const string TASK_EXTENSION = ".task";
    public const string WSAUTH_EXTN_EXTENSION = ".wsauth";
    public const string ISD_EXTENSION = ".csd";
    public const string WSID_EXTENSION = ".wsid";
    public const string WSDL_EXTENSION = ".wsdl";
    public const string WSID_XSD_SCHEMA_EXTENSION = ".xsd";
    public const string WSID_XML_SCHEMA_EXTENSION = ".xml";
    public const string MASHUP_CATEGORY_EXTENSION = ".PB.uimashup";
    public const string BADI_IMPLEMENTATION = ".enht";
    public const string BADI_FILTER = ".fltr";
    public const string USAGE_EXTENSION = ".usg";
    public const string ATTRIBUTE_EXSTENSION = ".attr";
    public const string XREP_EXTENSION = ".xrep";
    public const string SOLUTION_USER_OPTIONS_EXTENSION = ".suo";
    public const string BOCD_EXTENSION = ".bocd";
    public const string MDRO_EXTENSION = ".run";
    public const string NOTIFICATION_RULE = ".notification";
    public const string CODELIST = ".codelist";
    public const string XLS_EXPORT_TMPL_HEADER = ".xl07tmpl";
    public const string TAX_DECISION_TREE = ".bcctax";
    public const string REPORT_EXTENSION = ".report";
    public const string KEY_FIGURE_EXTENSION = ".kf";
    public const string COMBINED_DS_EXTENSION = ".cds";
    public const string JOIN_DS_EXTENSION = ".jds";
    public const string DOC_EXTENSION = ".doc";
    public const string DOCX_EXTENSION = ".docx";
    public const string PPT_EXTENSION = ".ppt";
    public const string PPTX_EXTENSION = ".pptx";
    public const string XLS_EXTENSION = ".xls";
    public const string XLSX_EXTENSION = ".xlsx";
    public const string PDF_EXTENSION = ".pdf";
    public const string JPG_EXTENSION = ".jpg";
    public const string PNG_EXTENSION = ".png";
    public const string TXT_EXTENSION = ".txt";
    public const string XLF_EXTENSION = ".xlf";
    public const string XODATA_EXTENSION = ".xodata";
    public const string PSD_EXTENSION = ".psd";

    public static List<string> GetAllExtensionsForScreens()
    {
      return new List<string>() { ".WCVIEW.uiwocview", ".WCF.uiwoc", ".uiwoc", ".OIF.xuicomponent", ".QA.xuicomponent", ".OWL.xuicomponent", ".FS.xuicomponent", ".GAF.xuicomponent", ".xuicomponent", ".EC.uicomponent", ".OIF.uicomponent", ".QA.uicomponent", ".OWL.uicomponent", ".OVS.uicomponent", ".FS.uicomponent", ".PTP.uicomponent", ".GAF.uicomponent", ".MD.uicomponent", ".QC.uicomponent", ".QV.uicomponent", ".TI.uicomponent", ".TT.uidataobject", ".bott", ".uicomponent", ".uidataobject", ".uiwocview" };
    }

    public static string GetScreenExtensionOfFile(string filename)
    {
      foreach (string extensionsForScreen in CopernicusFileExtensions.GetAllExtensionsForScreens())
      {
        if (filename.EndsWith(extensionsForScreen))
          return extensionsForScreen;
      }
      return (string) null;
    }

    public static List<string> GetAllExtensionsForDependentNodes()
    {
      return new List<string>() { ".absl", ".fthd", ".xdp", ".xsd", ".xml", ".approvalprocess", ".approval", ".sam", ".diagram", ".qry", ".wsauth", ".fltr", ".wsdl", ".sadl", ".library" };
    }

    public static List<string> GetNoDirectActivation()
    {
      return new List<string>() { ".qry", ".sam", ".diagram", ".sadl", ".task" };
    }

    public static bool NoDirectActivation(string nodeExtension)
    {
      return CopernicusFileExtensions.GetNoDirectActivation().Contains(nodeExtension);
    }

    public static List<string> GetAllExtensionsForVirtaulNodes()
    {
      return new List<string>() { ".node" };
    }

    public static bool IsVirtaulNodeExtension(string nodeExtension)
    {
      return CopernicusFileExtensions.GetAllExtensionsForVirtaulNodes().Contains(nodeExtension);
    }

    public static bool IsDependentNodeExtension(string nodeExtension)
    {
      return CopernicusFileExtensions.GetAllExtensionsForDependentNodes().Contains(nodeExtension);
    }

    public static bool IsFileVisibleInSolutionExplorer(string extension)
    {
      if (!extension.EndsWith(".usg") && !extension.EndsWith(".attr") && (!extension.EndsWith(".myproj") && !extension.EndsWith(".sam")) && (!extension.EndsWith(".xrep") && !extension.EndsWith(".sln") && (!extension.EndsWith(".suo") && !extension.EndsWith(".bocd"))) && (!extension.EndsWith(".tasklistview") && !extension.EndsWith(".xsd") && (!extension.EndsWith(".xfp") && !extension.EndsWith(".xftx")) && (!extension.EndsWith(".bctclosed") && !extension.EndsWith(".xl07tmpl"))))
        return !extension.EndsWith(".uianchor");
      return false;
    }

    public static List<string> GetFileExtensionsAddExistingItems()
    {
      return new List<string>() { ".absl", ".bo", ".approvalprocess", ".approval", ".webservice", ".xodata", ".bac", ".bcc", ".bcctax", ".bct", ".bco", ".xbo", ".xft", ".xdp", ".fthd", ".ftgd", ".xsd", ".pid", ".qry", ".ds", ".diagram", ".sam.diagram", ".sadl", ".xs", ".nxs", ".ref", ".library", ".uicomponent", ".uidataobject", ".uiswitch", ".WCVIEW.uiwocview", ".uiwocview", ".WCF.uiwoc", ".uiwoc", ".xuicomponent", ".OIF.xuicomponent", ".QA.xuicomponent", ".OWL.xuicomponent", ".FS.xuicomponent", ".GAF.xuicomponent", ".EC.uicomponent", ".OIF.uicomponent", ".QA.uicomponent", ".OWL.uicomponent", ".OVS.uicomponent", ".FS.uicomponent", ".PTP.uicomponent", ".GAF.uicomponent", ".MD.uicomponent", ".QC.uicomponent", ".QV.uicomponent", ".TI.uicomponent", ".TT.uidataobject", ".bott", ".task", ".wsauth", ".csd", ".wsid", ".wsdl", ".xml", ".PB.uimashup", ".enht", ".fltr", ".run", ".notification", ".codelist", ".bcctax", ".doc", ".docx", ".ppt", ".pptx", ".xls", ".xlsx", ".pdf", ".jpg", ".png", ".txt", ".xlf", ".psd" };
    }

    public static bool IsAddExistingItemFileExtension(string nodeExtension)
    {
      return CopernicusFileExtensions.GetFileExtensionsAddExistingItems().Contains(nodeExtension.ToLower());
    }
  }
}
