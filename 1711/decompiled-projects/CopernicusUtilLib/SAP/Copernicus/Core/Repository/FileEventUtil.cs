﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.FileEventUtil
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public static class FileEventUtil
  {
    public static bool Match(this FileEventType me, FileEventType other)
    {
      return (me & other) != FileEventType.None;
    }

    public static bool IsEventTypeMask(this FileEventType me)
    {
      return (me - 1 & me) != FileEventType.None;
    }
  }
}
