﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.DataModel.RepositoryDataSet
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.DataModel
{
  [DesignerCategory("code")]
  [XmlSchemaProvider("GetTypedDataSetSchema")]
  [XmlRoot("RepositoryDataSet")]
  [HelpKeyword("vs.data.DataSet")]
  [ToolboxItem(true)]
  [Serializable]
  public class RepositoryDataSet : DataSet
  {
    private SchemaSerializationMode _schemaSerializationMode = SchemaSerializationMode.IncludeSchema;
    private RepositoryDataSet.NamespacesDataTable tableNamespaces;
    private RepositoryDataSet.BusinessObjectsDataTable tableBusinessObjects;
    private RepositoryDataSet.DataTypesDataTable tableDataTypes;
    private RepositoryDataSet.NodesDataTable tableNodes;
    private RepositoryDataSet.ActionsDataTable tableActions;
    private RepositoryDataSet.SolutionsDataTable tableSolutions;
    private RepositoryDataSet.LibrariesDataTable tableLibraries;
    private RepositoryDataSet.MessageTypesDataTable tableMessageTypes;
    private RepositoryDataSet.MsgDataTypeNodesDataTable tableMsgDataTypeNodes;
    private RepositoryDataSet.BAdIsDataTable tableBAdIs;
    private RepositoryDataSet.BAdIFiltersDataTable tableBAdIFilters;
    private RepositoryDataSet.BAdIMethodsDataTable tableBAdIMethods;
    private RepositoryDataSet.ProjectItemSolutionDataTable tableProjectItemSolution;
    private RepositoryDataSet.ProjectItemDataTable tableProjectItem;
    private RepositoryDataSet.FeatureDataTable tableFeature;
    private DataRelation relationNamespaces_BusinessObjects;
    private DataRelation relationNamespaces_Datatypes;
    private DataRelation relationBusinessObjects_Nodes;
    private DataRelation relationNodes_Actions;
    private DataRelation relationNamespaces_MessageTypes;
    private DataRelation relationMessagetypes_DataTypeNodes;
    private DataRelation relationDataTypes_MessageTypes;
    private DataRelation relationDataTypes_DataTypeNodes;
    private DataRelation relationNamespaces_BAdIs;
    private DataRelation relationBAdIs_BAdIFilters;
    private DataRelation relationBAdIs_BAdIMethod;

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public RepositoryDataSet()
    {
      this.BeginInit();
      this.InitClass();
      CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
      base.Tables.CollectionChanged += changeEventHandler;
      base.Relations.CollectionChanged += changeEventHandler;
      this.EndInit();
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    protected RepositoryDataSet(SerializationInfo info, StreamingContext context)
      : base(info, context, false)
    {
      if (this.IsBinarySerialized(info, context))
      {
        this.InitVars(false);
        CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
        this.Tables.CollectionChanged += changeEventHandler;
        this.Relations.CollectionChanged += changeEventHandler;
      }
      else
      {
        string s = (string) info.GetValue("XmlSchema", typeof (string));
        if (this.DetermineSchemaSerializationMode(info, context) == SchemaSerializationMode.IncludeSchema)
        {
          DataSet dataSet = new DataSet();
          dataSet.ReadXmlSchema((XmlReader) new XmlTextReader((TextReader) new StringReader(s)));
          if (dataSet.Tables[nameof (Namespaces)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.NamespacesDataTable(dataSet.Tables[nameof (Namespaces)]));
          if (dataSet.Tables[nameof (BusinessObjects)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.BusinessObjectsDataTable(dataSet.Tables[nameof (BusinessObjects)]));
          if (dataSet.Tables[nameof (DataTypes)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.DataTypesDataTable(dataSet.Tables[nameof (DataTypes)]));
          if (dataSet.Tables[nameof (Nodes)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.NodesDataTable(dataSet.Tables[nameof (Nodes)]));
          if (dataSet.Tables[nameof (Actions)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.ActionsDataTable(dataSet.Tables[nameof (Actions)]));
          if (dataSet.Tables[nameof (Solutions)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.SolutionsDataTable(dataSet.Tables[nameof (Solutions)]));
          if (dataSet.Tables[nameof (Libraries)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.LibrariesDataTable(dataSet.Tables[nameof (Libraries)]));
          if (dataSet.Tables[nameof (MessageTypes)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.MessageTypesDataTable(dataSet.Tables[nameof (MessageTypes)]));
          if (dataSet.Tables[nameof (MsgDataTypeNodes)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.MsgDataTypeNodesDataTable(dataSet.Tables[nameof (MsgDataTypeNodes)]));
          if (dataSet.Tables[nameof (BAdIs)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.BAdIsDataTable(dataSet.Tables[nameof (BAdIs)]));
          if (dataSet.Tables[nameof (BAdIFilters)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.BAdIFiltersDataTable(dataSet.Tables[nameof (BAdIFilters)]));
          if (dataSet.Tables[nameof (BAdIMethods)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.BAdIMethodsDataTable(dataSet.Tables[nameof (BAdIMethods)]));
          if (dataSet.Tables[nameof (ProjectItemSolution)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.ProjectItemSolutionDataTable(dataSet.Tables[nameof (ProjectItemSolution)]));
          if (dataSet.Tables[nameof (ProjectItem)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.ProjectItemDataTable(dataSet.Tables[nameof (ProjectItem)]));
          if (dataSet.Tables[nameof (Feature)] != null)
            base.Tables.Add((DataTable) new RepositoryDataSet.FeatureDataTable(dataSet.Tables[nameof (Feature)]));
          this.DataSetName = dataSet.DataSetName;
          this.Prefix = dataSet.Prefix;
          this.Namespace = dataSet.Namespace;
          this.Locale = dataSet.Locale;
          this.CaseSensitive = dataSet.CaseSensitive;
          this.EnforceConstraints = dataSet.EnforceConstraints;
          this.Merge(dataSet, false, MissingSchemaAction.Add);
          this.InitVars();
        }
        else
          this.ReadXmlSchema((XmlReader) new XmlTextReader((TextReader) new StringReader(s)));
        this.GetSerializationData(info, context);
        CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
        base.Tables.CollectionChanged += changeEventHandler;
        this.Relations.CollectionChanged += changeEventHandler;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [DebuggerNonUserCode]
    public RepositoryDataSet.NamespacesDataTable Namespaces
    {
      get
      {
        return this.tableNamespaces;
      }
    }

    [Browsable(false)]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    public RepositoryDataSet.BusinessObjectsDataTable BusinessObjects
    {
      get
      {
        return this.tableBusinessObjects;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [Browsable(false)]
    public RepositoryDataSet.DataTypesDataTable DataTypes
    {
      get
      {
        return this.tableDataTypes;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    public RepositoryDataSet.NodesDataTable Nodes
    {
      get
      {
        return this.tableNodes;
      }
    }

    [Browsable(false)]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    public RepositoryDataSet.ActionsDataTable Actions
    {
      get
      {
        return this.tableActions;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [Browsable(false)]
    [DebuggerNonUserCode]
    public RepositoryDataSet.SolutionsDataTable Solutions
    {
      get
      {
        return this.tableSolutions;
      }
    }

    [Browsable(false)]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [DebuggerNonUserCode]
    public RepositoryDataSet.LibrariesDataTable Libraries
    {
      get
      {
        return this.tableLibraries;
      }
    }

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public RepositoryDataSet.MessageTypesDataTable MessageTypes
    {
      get
      {
        return this.tableMessageTypes;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [Browsable(false)]
    public RepositoryDataSet.MsgDataTypeNodesDataTable MsgDataTypeNodes
    {
      get
      {
        return this.tableMsgDataTypeNodes;
      }
    }

    [Browsable(false)]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    public RepositoryDataSet.BAdIsDataTable BAdIs
    {
      get
      {
        return this.tableBAdIs;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    public RepositoryDataSet.BAdIFiltersDataTable BAdIFilters
    {
      get
      {
        return this.tableBAdIFilters;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [Browsable(false)]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    public RepositoryDataSet.BAdIMethodsDataTable BAdIMethods
    {
      get
      {
        return this.tableBAdIMethods;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    [Browsable(false)]
    public RepositoryDataSet.ProjectItemSolutionDataTable ProjectItemSolution
    {
      get
      {
        return this.tableProjectItemSolution;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [Browsable(false)]
    public RepositoryDataSet.ProjectItemDataTable ProjectItem
    {
      get
      {
        return this.tableProjectItem;
      }
    }

    [Browsable(false)]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    public RepositoryDataSet.FeatureDataTable Feature
    {
      get
      {
        return this.tableFeature;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [Browsable(true)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
    public override SchemaSerializationMode SchemaSerializationMode
    {
      get
      {
        return this._schemaSerializationMode;
      }
      set
      {
        this._schemaSerializationMode = value;
      }
    }

    [DebuggerNonUserCode]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public new DataTableCollection Tables
    {
      get
      {
        return base.Tables;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    public new DataRelationCollection Relations
    {
      get
      {
        return base.Relations;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    protected override void InitializeDerivedDataSet()
    {
      this.BeginInit();
      this.InitClass();
      this.EndInit();
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    public override DataSet Clone()
    {
      RepositoryDataSet repositoryDataSet = (RepositoryDataSet) base.Clone();
      repositoryDataSet.InitVars();
      repositoryDataSet.SchemaSerializationMode = this.SchemaSerializationMode;
      return (DataSet) repositoryDataSet;
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    protected override bool ShouldSerializeTables()
    {
      return false;
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    protected override bool ShouldSerializeRelations()
    {
      return false;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    protected override void ReadXmlSerializable(XmlReader reader)
    {
      if (this.DetermineSchemaSerializationMode(reader) == SchemaSerializationMode.IncludeSchema)
      {
        this.Reset();
        DataSet dataSet = new DataSet();
        int num = (int) dataSet.ReadXml(reader);
        if (dataSet.Tables["Namespaces"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.NamespacesDataTable(dataSet.Tables["Namespaces"]));
        if (dataSet.Tables["BusinessObjects"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.BusinessObjectsDataTable(dataSet.Tables["BusinessObjects"]));
        if (dataSet.Tables["DataTypes"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.DataTypesDataTable(dataSet.Tables["DataTypes"]));
        if (dataSet.Tables["Nodes"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.NodesDataTable(dataSet.Tables["Nodes"]));
        if (dataSet.Tables["Actions"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.ActionsDataTable(dataSet.Tables["Actions"]));
        if (dataSet.Tables["Solutions"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.SolutionsDataTable(dataSet.Tables["Solutions"]));
        if (dataSet.Tables["Libraries"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.LibrariesDataTable(dataSet.Tables["Libraries"]));
        if (dataSet.Tables["MessageTypes"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.MessageTypesDataTable(dataSet.Tables["MessageTypes"]));
        if (dataSet.Tables["MsgDataTypeNodes"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.MsgDataTypeNodesDataTable(dataSet.Tables["MsgDataTypeNodes"]));
        if (dataSet.Tables["BAdIs"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.BAdIsDataTable(dataSet.Tables["BAdIs"]));
        if (dataSet.Tables["BAdIFilters"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.BAdIFiltersDataTable(dataSet.Tables["BAdIFilters"]));
        if (dataSet.Tables["BAdIMethods"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.BAdIMethodsDataTable(dataSet.Tables["BAdIMethods"]));
        if (dataSet.Tables["ProjectItemSolution"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.ProjectItemSolutionDataTable(dataSet.Tables["ProjectItemSolution"]));
        if (dataSet.Tables["ProjectItem"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.ProjectItemDataTable(dataSet.Tables["ProjectItem"]));
        if (dataSet.Tables["Feature"] != null)
          base.Tables.Add((DataTable) new RepositoryDataSet.FeatureDataTable(dataSet.Tables["Feature"]));
        this.DataSetName = dataSet.DataSetName;
        this.Prefix = dataSet.Prefix;
        this.Namespace = dataSet.Namespace;
        this.Locale = dataSet.Locale;
        this.CaseSensitive = dataSet.CaseSensitive;
        this.EnforceConstraints = dataSet.EnforceConstraints;
        this.Merge(dataSet, false, MissingSchemaAction.Add);
        this.InitVars();
      }
      else
      {
        int num = (int) this.ReadXml(reader);
        this.InitVars();
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    protected override XmlSchema GetSchemaSerializable()
    {
      MemoryStream memoryStream = new MemoryStream();
      this.WriteXmlSchema((XmlWriter) new XmlTextWriter((Stream) memoryStream, (Encoding) null));
      memoryStream.Position = 0L;
      return XmlSchema.Read((XmlReader) new XmlTextReader((Stream) memoryStream), (ValidationEventHandler) null);
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    internal void InitVars()
    {
      this.InitVars(true);
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    internal void InitVars(bool initTable)
    {
      this.tableNamespaces = (RepositoryDataSet.NamespacesDataTable) base.Tables["Namespaces"];
      if (initTable && this.tableNamespaces != null)
        this.tableNamespaces.InitVars();
      this.tableBusinessObjects = (RepositoryDataSet.BusinessObjectsDataTable) base.Tables["BusinessObjects"];
      if (initTable && this.tableBusinessObjects != null)
        this.tableBusinessObjects.InitVars();
      this.tableDataTypes = (RepositoryDataSet.DataTypesDataTable) base.Tables["DataTypes"];
      if (initTable && this.tableDataTypes != null)
        this.tableDataTypes.InitVars();
      this.tableNodes = (RepositoryDataSet.NodesDataTable) base.Tables["Nodes"];
      if (initTable && this.tableNodes != null)
        this.tableNodes.InitVars();
      this.tableActions = (RepositoryDataSet.ActionsDataTable) base.Tables["Actions"];
      if (initTable && this.tableActions != null)
        this.tableActions.InitVars();
      this.tableSolutions = (RepositoryDataSet.SolutionsDataTable) base.Tables["Solutions"];
      if (initTable && this.tableSolutions != null)
        this.tableSolutions.InitVars();
      this.tableLibraries = (RepositoryDataSet.LibrariesDataTable) base.Tables["Libraries"];
      if (initTable && this.tableLibraries != null)
        this.tableLibraries.InitVars();
      this.tableMessageTypes = (RepositoryDataSet.MessageTypesDataTable) base.Tables["MessageTypes"];
      if (initTable && this.tableMessageTypes != null)
        this.tableMessageTypes.InitVars();
      this.tableMsgDataTypeNodes = (RepositoryDataSet.MsgDataTypeNodesDataTable) base.Tables["MsgDataTypeNodes"];
      if (initTable && this.tableMsgDataTypeNodes != null)
        this.tableMsgDataTypeNodes.InitVars();
      this.tableBAdIs = (RepositoryDataSet.BAdIsDataTable) base.Tables["BAdIs"];
      if (initTable && this.tableBAdIs != null)
        this.tableBAdIs.InitVars();
      this.tableBAdIFilters = (RepositoryDataSet.BAdIFiltersDataTable) base.Tables["BAdIFilters"];
      if (initTable && this.tableBAdIFilters != null)
        this.tableBAdIFilters.InitVars();
      this.tableBAdIMethods = (RepositoryDataSet.BAdIMethodsDataTable) base.Tables["BAdIMethods"];
      if (initTable && this.tableBAdIMethods != null)
        this.tableBAdIMethods.InitVars();
      this.tableProjectItemSolution = (RepositoryDataSet.ProjectItemSolutionDataTable) base.Tables["ProjectItemSolution"];
      if (initTable && this.tableProjectItemSolution != null)
        this.tableProjectItemSolution.InitVars();
      this.tableProjectItem = (RepositoryDataSet.ProjectItemDataTable) base.Tables["ProjectItem"];
      if (initTable && this.tableProjectItem != null)
        this.tableProjectItem.InitVars();
      this.tableFeature = (RepositoryDataSet.FeatureDataTable) base.Tables["Feature"];
      if (initTable && this.tableFeature != null)
        this.tableFeature.InitVars();
      this.relationNamespaces_BusinessObjects = this.Relations["Namespaces_BusinessObjects"];
      this.relationNamespaces_Datatypes = this.Relations["Namespaces_Datatypes"];
      this.relationBusinessObjects_Nodes = this.Relations["BusinessObjects_Nodes"];
      this.relationNodes_Actions = this.Relations["Nodes_Actions"];
      this.relationNamespaces_MessageTypes = this.Relations["Namespaces_MessageTypes"];
      this.relationMessagetypes_DataTypeNodes = this.Relations["Messagetypes_DataTypeNodes"];
      this.relationDataTypes_MessageTypes = this.Relations["DataTypes_MessageTypes"];
      this.relationDataTypes_DataTypeNodes = this.Relations["DataTypes_DataTypeNodes"];
      this.relationNamespaces_BAdIs = this.Relations["Namespaces_BAdIs"];
      this.relationBAdIs_BAdIFilters = this.Relations["BAdIs_BAdIFilters"];
      this.relationBAdIs_BAdIMethod = this.Relations["BAdIs_BAdIMethod"];
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    private void InitClass()
    {
      this.DataSetName = nameof (RepositoryDataSet);
      this.Prefix = "";
      this.Namespace = "http://sap.com/copernicus/RepositoryDataSet.xsd";
      this.Locale = new CultureInfo("en-US");
      this.EnforceConstraints = true;
      this.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;
      this.tableNamespaces = new RepositoryDataSet.NamespacesDataTable();
      base.Tables.Add((DataTable) this.tableNamespaces);
      this.tableBusinessObjects = new RepositoryDataSet.BusinessObjectsDataTable();
      base.Tables.Add((DataTable) this.tableBusinessObjects);
      this.tableDataTypes = new RepositoryDataSet.DataTypesDataTable();
      base.Tables.Add((DataTable) this.tableDataTypes);
      this.tableNodes = new RepositoryDataSet.NodesDataTable();
      base.Tables.Add((DataTable) this.tableNodes);
      this.tableActions = new RepositoryDataSet.ActionsDataTable();
      base.Tables.Add((DataTable) this.tableActions);
      this.tableSolutions = new RepositoryDataSet.SolutionsDataTable();
      base.Tables.Add((DataTable) this.tableSolutions);
      this.tableLibraries = new RepositoryDataSet.LibrariesDataTable();
      base.Tables.Add((DataTable) this.tableLibraries);
      this.tableMessageTypes = new RepositoryDataSet.MessageTypesDataTable();
      base.Tables.Add((DataTable) this.tableMessageTypes);
      this.tableMsgDataTypeNodes = new RepositoryDataSet.MsgDataTypeNodesDataTable();
      base.Tables.Add((DataTable) this.tableMsgDataTypeNodes);
      this.tableBAdIs = new RepositoryDataSet.BAdIsDataTable();
      base.Tables.Add((DataTable) this.tableBAdIs);
      this.tableBAdIFilters = new RepositoryDataSet.BAdIFiltersDataTable();
      base.Tables.Add((DataTable) this.tableBAdIFilters);
      this.tableBAdIMethods = new RepositoryDataSet.BAdIMethodsDataTable();
      base.Tables.Add((DataTable) this.tableBAdIMethods);
      this.tableProjectItemSolution = new RepositoryDataSet.ProjectItemSolutionDataTable();
      base.Tables.Add((DataTable) this.tableProjectItemSolution);
      this.tableProjectItem = new RepositoryDataSet.ProjectItemDataTable();
      base.Tables.Add((DataTable) this.tableProjectItem);
      this.tableFeature = new RepositoryDataSet.FeatureDataTable();
      base.Tables.Add((DataTable) this.tableFeature);
      this.relationNamespaces_BusinessObjects = new DataRelation("Namespaces_BusinessObjects", new DataColumn[1]
      {
        this.tableNamespaces.NSNameColumn
      }, new DataColumn[1]
      {
        this.tableBusinessObjects.NSNameColumn
      }, false);
      this.Relations.Add(this.relationNamespaces_BusinessObjects);
      this.relationNamespaces_Datatypes = new DataRelation("Namespaces_Datatypes", new DataColumn[1]
      {
        this.tableNamespaces.NSNameColumn
      }, new DataColumn[1]
      {
        this.tableDataTypes.NSNameColumn
      }, false);
      this.Relations.Add(this.relationNamespaces_Datatypes);
      this.relationBusinessObjects_Nodes = new DataRelation("BusinessObjects_Nodes", new DataColumn[1]
      {
        this.tableBusinessObjects.ProxyNameColumn
      }, new DataColumn[1]
      {
        this.tableNodes.BOProxyNameColumn
      }, false);
      this.Relations.Add(this.relationBusinessObjects_Nodes);
      this.relationNodes_Actions = new DataRelation("Nodes_Actions", new DataColumn[2]
      {
        this.tableNodes.BOProxyNameColumn,
        this.tableNodes.NameColumn
      }, new DataColumn[2]
      {
        this.tableActions.BOProxyNameColumn,
        this.tableActions.NodeProxyNameColumn
      }, false);
      this.Relations.Add(this.relationNodes_Actions);
      this.relationNamespaces_MessageTypes = new DataRelation("Namespaces_MessageTypes", new DataColumn[1]
      {
        this.tableNamespaces.NSNameColumn
      }, new DataColumn[1]
      {
        this.tableMessageTypes.NamespaceColumn
      }, false);
      this.Relations.Add(this.relationNamespaces_MessageTypes);
      this.relationMessagetypes_DataTypeNodes = new DataRelation("Messagetypes_DataTypeNodes", new DataColumn[1]
      {
        this.tableMessageTypes.NameColumn
      }, new DataColumn[1]
      {
        this.tableMsgDataTypeNodes.MsgTypeNameColumn
      }, false);
      this.Relations.Add(this.relationMessagetypes_DataTypeNodes);
      this.relationDataTypes_MessageTypes = new DataRelation("DataTypes_MessageTypes", new DataColumn[1]
      {
        this.tableDataTypes.NameColumn
      }, new DataColumn[1]
      {
        this.tableMessageTypes.DataTypeNameColumn
      }, false);
      this.Relations.Add(this.relationDataTypes_MessageTypes);
      this.relationDataTypes_DataTypeNodes = new DataRelation("DataTypes_DataTypeNodes", new DataColumn[1]
      {
        this.tableDataTypes.NameColumn
      }, new DataColumn[1]
      {
        this.tableMsgDataTypeNodes.DataTypeNameColumn
      }, false);
      this.Relations.Add(this.relationDataTypes_DataTypeNodes);
      this.relationNamespaces_BAdIs = new DataRelation("Namespaces_BAdIs", new DataColumn[1]
      {
        this.tableNamespaces.NSNameColumn
      }, new DataColumn[1]{ this.tableBAdIs.NSNameColumn }, false);
      this.Relations.Add(this.relationNamespaces_BAdIs);
      this.relationBAdIs_BAdIFilters = new DataRelation("BAdIs_BAdIFilters", new DataColumn[1]
      {
        this.tableBAdIs.ProxyNameColumn
      }, new DataColumn[1]
      {
        this.tableBAdIFilters.BAdIProxyNameColumn
      }, false);
      this.Relations.Add(this.relationBAdIs_BAdIFilters);
      this.relationBAdIs_BAdIMethod = new DataRelation("BAdIs_BAdIMethod", new DataColumn[1]
      {
        this.tableBAdIs.ProxyNameColumn
      }, new DataColumn[1]
      {
        this.tableBAdIMethods.BAdIProxyNameColumn
      }, false);
      this.Relations.Add(this.relationBAdIs_BAdIMethod);
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    private bool ShouldSerializeNamespaces()
    {
      return false;
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    private bool ShouldSerializeBusinessObjects()
    {
      return false;
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    private bool ShouldSerializeDataTypes()
    {
      return false;
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    private bool ShouldSerializeNodes()
    {
      return false;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    private bool ShouldSerializeActions()
    {
      return false;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    private bool ShouldSerializeSolutions()
    {
      return false;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    private bool ShouldSerializeLibraries()
    {
      return false;
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    private bool ShouldSerializeMessageTypes()
    {
      return false;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    private bool ShouldSerializeMsgDataTypeNodes()
    {
      return false;
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    private bool ShouldSerializeBAdIs()
    {
      return false;
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    private bool ShouldSerializeBAdIFilters()
    {
      return false;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    private bool ShouldSerializeBAdIMethods()
    {
      return false;
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    private bool ShouldSerializeProjectItemSolution()
    {
      return false;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    private bool ShouldSerializeProjectItem()
    {
      return false;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    private bool ShouldSerializeFeature()
    {
      return false;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    private void SchemaChanged(object sender, CollectionChangeEventArgs e)
    {
      if (e.Action != CollectionChangeAction.Remove)
        return;
      this.InitVars();
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    [DebuggerNonUserCode]
    public static XmlSchemaComplexType GetTypedDataSetSchema(XmlSchemaSet xs)
    {
      RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
      XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
      XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
      xmlSchemaSequence.Items.Add((XmlSchemaObject) new XmlSchemaAny()
      {
        Namespace = repositoryDataSet.Namespace
      });
      schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
      XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
      if (xs.Contains(schemaSerializable.TargetNamespace))
      {
        MemoryStream memoryStream1 = new MemoryStream();
        MemoryStream memoryStream2 = new MemoryStream();
        try
        {
          schemaSerializable.Write((Stream) memoryStream1);
          foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
          {
            memoryStream2.SetLength(0L);
            schema.Write((Stream) memoryStream2);
            if (memoryStream1.Length == memoryStream2.Length)
            {
              memoryStream1.Position = 0L;
              memoryStream2.Position = 0L;
              do
                ;
              while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
              if (memoryStream1.Position == memoryStream1.Length)
                return schemaComplexType;
            }
          }
        }
        finally
        {
          if (memoryStream1 != null)
            memoryStream1.Close();
          if (memoryStream2 != null)
            memoryStream2.Close();
        }
      }
      xs.Add(schemaSerializable);
      return schemaComplexType;
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class ProjectItemSolutionDataTable : TypedTableBase<RepositoryDataSet.ProjectItemSolutionRow>
    {
      private DataColumn columnProjectItemSolutionkey;
      private DataColumn columnProjectItemType;
      private DataColumn columnSAPSolutionType;
      private DataColumn columnPDISubtype;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public ProjectItemSolutionDataTable()
      {
        this.TableName = "ProjectItemSolution";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal ProjectItemSolutionDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected ProjectItemSolutionDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn ProjectItemSolutionkeyColumn
      {
        get
        {
          return this.columnProjectItemSolutionkey;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn ProjectItemTypeColumn
      {
        get
        {
          return this.columnProjectItemType;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn SAPSolutionTypeColumn
      {
        get
        {
          return this.columnSAPSolutionType;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn PDISubtypeColumn
      {
        get
        {
          return this.columnPDISubtype;
        }
      }

      [Browsable(false)]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ProjectItemSolutionRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.ProjectItemSolutionRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ProjectItemSolutionRowChangeEventHandler ProjectItemSolutionRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ProjectItemSolutionRowChangeEventHandler ProjectItemSolutionRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ProjectItemSolutionRowChangeEventHandler ProjectItemSolutionRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ProjectItemSolutionRowChangeEventHandler ProjectItemSolutionRowDeleted;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void AddProjectItemSolutionRow(RepositoryDataSet.ProjectItemSolutionRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ProjectItemSolutionRow AddProjectItemSolutionRow(string ProjectItemSolutionkey, string ProjectItemType, string SAPSolutionType, string PDISubtype)
      {
        RepositoryDataSet.ProjectItemSolutionRow projectItemSolutionRow = (RepositoryDataSet.ProjectItemSolutionRow) this.NewRow();
        object[] objArray = new object[4]
        {
          (object) ProjectItemSolutionkey,
          (object) ProjectItemType,
          (object) SAPSolutionType,
          (object) PDISubtype
        };
        projectItemSolutionRow.ItemArray = objArray;
        this.Rows.Add((DataRow) projectItemSolutionRow);
        return projectItemSolutionRow;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.ProjectItemSolutionRow FindByProjectItemSolutionkey(string ProjectItemSolutionkey)
      {
        return (RepositoryDataSet.ProjectItemSolutionRow) this.Rows.Find(new object[1]
        {
          (object) ProjectItemSolutionkey
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.ProjectItemSolutionDataTable solutionDataTable = (RepositoryDataSet.ProjectItemSolutionDataTable) base.Clone();
        solutionDataTable.InitVars();
        return (DataTable) solutionDataTable;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.ProjectItemSolutionDataTable();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnProjectItemSolutionkey = this.Columns["ProjectItemSolutionkey"];
        this.columnProjectItemType = this.Columns["ProjectItemType"];
        this.columnSAPSolutionType = this.Columns["SAPSolutionType"];
        this.columnPDISubtype = this.Columns["PDISubtype"];
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnProjectItemSolutionkey = new DataColumn("ProjectItemSolutionkey", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProjectItemSolutionkey);
        this.columnProjectItemType = new DataColumn("ProjectItemType", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProjectItemType);
        this.columnSAPSolutionType = new DataColumn("SAPSolutionType", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnSAPSolutionType);
        this.columnPDISubtype = new DataColumn("PDISubtype", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnPDISubtype);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[1]
        {
          this.columnProjectItemSolutionkey
        }, true));
        this.columnProjectItemSolutionkey.AllowDBNull = false;
        this.columnProjectItemSolutionkey.Unique = true;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ProjectItemSolutionRow NewProjectItemSolutionRow()
      {
        return (RepositoryDataSet.ProjectItemSolutionRow) this.NewRow();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.ProjectItemSolutionRow(builder);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.ProjectItemSolutionRow);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.ProjectItemSolutionRowChanged == null)
          return;
        this.ProjectItemSolutionRowChanged((object) this, new RepositoryDataSet.ProjectItemSolutionRowChangeEvent((RepositoryDataSet.ProjectItemSolutionRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.ProjectItemSolutionRowChanging == null)
          return;
        this.ProjectItemSolutionRowChanging((object) this, new RepositoryDataSet.ProjectItemSolutionRowChangeEvent((RepositoryDataSet.ProjectItemSolutionRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.ProjectItemSolutionRowDeleted == null)
          return;
        this.ProjectItemSolutionRowDeleted((object) this, new RepositoryDataSet.ProjectItemSolutionRowChangeEvent((RepositoryDataSet.ProjectItemSolutionRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.ProjectItemSolutionRowDeleting == null)
          return;
        this.ProjectItemSolutionRowDeleting((object) this, new RepositoryDataSet.ProjectItemSolutionRowChangeEvent((RepositoryDataSet.ProjectItemSolutionRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void RemoveProjectItemSolutionRow(RepositoryDataSet.ProjectItemSolutionRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (ProjectItemSolutionDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class SolutionsDataTable : TypedTableBase<RepositoryDataSet.SolutionsRow>
    {
      private DataColumn columnName;
      private DataColumn columnVersion;
      private DataColumn columnStatus;
      private DataColumn columnDescription;
      private DataColumn columnType;
      private DataColumn columnDeleted;
      private DataColumn columnCertificationStatus;
      private DataColumn columnOveralStatus;
      private DataColumn columnPatchSolution;
      private DataColumn columnPDISubType;
      private DataColumn columnDevPartner;
      private DataColumn columnContactPerson;
      private DataColumn columnEMail;
      private DataColumn columnDeploymentUnit;
      private DataColumn columnGlobalName;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public SolutionsDataTable()
      {
        this.TableName = "Solutions";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal SolutionsDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected SolutionsDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn VersionColumn
      {
        get
        {
          return this.columnVersion;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn StatusColumn
      {
        get
        {
          return this.columnStatus;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn DescriptionColumn
      {
        get
        {
          return this.columnDescription;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn TypeColumn
      {
        get
        {
          return this.columnType;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn DeletedColumn
      {
        get
        {
          return this.columnDeleted;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn CertificationStatusColumn
      {
        get
        {
          return this.columnCertificationStatus;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn OveralStatusColumn
      {
        get
        {
          return this.columnOveralStatus;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn PatchSolutionColumn
      {
        get
        {
          return this.columnPatchSolution;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn PDISubTypeColumn
      {
        get
        {
          return this.columnPDISubType;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn DevPartnerColumn
      {
        get
        {
          return this.columnDevPartner;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn ContactPersonColumn
      {
        get
        {
          return this.columnContactPerson;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn EMailColumn
      {
        get
        {
          return this.columnEMail;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn DeploymentUnitColumn
      {
        get
        {
          return this.columnDeploymentUnit;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn GlobalNameColumn
      {
        get
        {
          return this.columnGlobalName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [Browsable(false)]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.SolutionsRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.SolutionsRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.SolutionsRowChangeEventHandler SolutionsRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.SolutionsRowChangeEventHandler SolutionsRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.SolutionsRowChangeEventHandler SolutionsRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.SolutionsRowChangeEventHandler SolutionsRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddSolutionsRow(RepositoryDataSet.SolutionsRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.SolutionsRow AddSolutionsRow(string Name, string Version, string Status, string Description, string Type, bool Deleted, string CertificationStatus, string OveralStatus, string PatchSolution, string PDISubType, string DevPartner, string ContactPerson, string EMail, string DeploymentUnit, string GlobalName)
      {
        RepositoryDataSet.SolutionsRow solutionsRow = (RepositoryDataSet.SolutionsRow) this.NewRow();
        object[] objArray = new object[15]
        {
          (object) Name,
          (object) Version,
          (object) Status,
          (object) Description,
          (object) Type,
          (object) Deleted,
          (object) CertificationStatus,
          (object) OveralStatus,
          (object) PatchSolution,
          (object) PDISubType,
          (object) DevPartner,
          (object) ContactPerson,
          (object) EMail,
          (object) DeploymentUnit,
          (object) GlobalName
        };
        solutionsRow.ItemArray = objArray;
        this.Rows.Add((DataRow) solutionsRow);
        return solutionsRow;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.SolutionsRow FindByNameVersion(string Name, string Version)
      {
        return (RepositoryDataSet.SolutionsRow) this.Rows.Find(new object[2]
        {
          (object) Name,
          (object) Version
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.SolutionsDataTable solutionsDataTable = (RepositoryDataSet.SolutionsDataTable) base.Clone();
        solutionsDataTable.InitVars();
        return (DataTable) solutionsDataTable;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.SolutionsDataTable();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal void InitVars()
      {
        this.columnName = this.Columns["Name"];
        this.columnVersion = this.Columns["Version"];
        this.columnStatus = this.Columns["Status"];
        this.columnDescription = this.Columns["Description"];
        this.columnType = this.Columns["Type"];
        this.columnDeleted = this.Columns["Deleted"];
        this.columnCertificationStatus = this.Columns["CertificationStatus"];
        this.columnOveralStatus = this.Columns["OveralStatus"];
        this.columnPatchSolution = this.Columns["PatchSolution"];
        this.columnPDISubType = this.Columns["PDISubType"];
        this.columnDevPartner = this.Columns["DevPartner"];
        this.columnContactPerson = this.Columns["ContactPerson"];
        this.columnEMail = this.Columns["EMail"];
        this.columnDeploymentUnit = this.Columns["DeploymentUnit"];
        this.columnGlobalName = this.Columns["GlobalName"];
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnVersion = new DataColumn("Version", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnVersion);
        this.columnStatus = new DataColumn("Status", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnStatus);
        this.columnDescription = new DataColumn("Description", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDescription);
        this.columnType = new DataColumn("Type", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnType);
        this.columnDeleted = new DataColumn("Deleted", typeof (bool), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDeleted);
        this.columnCertificationStatus = new DataColumn("CertificationStatus", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCertificationStatus);
        this.columnOveralStatus = new DataColumn("OveralStatus", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnOveralStatus);
        this.columnPatchSolution = new DataColumn("PatchSolution", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnPatchSolution);
        this.columnPDISubType = new DataColumn("PDISubType", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnPDISubType);
        this.columnDevPartner = new DataColumn("DevPartner", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDevPartner);
        this.columnContactPerson = new DataColumn("ContactPerson", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnContactPerson);
        this.columnEMail = new DataColumn("EMail", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnEMail);
        this.columnDeploymentUnit = new DataColumn("DeploymentUnit", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDeploymentUnit);
        this.columnGlobalName = new DataColumn("GlobalName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnGlobalName);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[2]
        {
          this.columnName,
          this.columnVersion
        }, true));
        this.columnName.AllowDBNull = false;
        this.columnVersion.AllowDBNull = false;
        this.columnPatchSolution.MaxLength = 1;
        this.CaseSensitive = true;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.SolutionsRow NewSolutionsRow()
      {
        return (RepositoryDataSet.SolutionsRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.SolutionsRow(builder);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.SolutionsRow);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.SolutionsRowChanged == null)
          return;
        this.SolutionsRowChanged((object) this, new RepositoryDataSet.SolutionsRowChangeEvent((RepositoryDataSet.SolutionsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.SolutionsRowChanging == null)
          return;
        this.SolutionsRowChanging((object) this, new RepositoryDataSet.SolutionsRowChangeEvent((RepositoryDataSet.SolutionsRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.SolutionsRowDeleted == null)
          return;
        this.SolutionsRowDeleted((object) this, new RepositoryDataSet.SolutionsRowChangeEvent((RepositoryDataSet.SolutionsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.SolutionsRowDeleting == null)
          return;
        this.SolutionsRowDeleting((object) this, new RepositoryDataSet.SolutionsRowChangeEvent((RepositoryDataSet.SolutionsRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void RemoveSolutionsRow(RepositoryDataSet.SolutionsRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (SolutionsDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class BAdIFiltersDataTable : TypedTableBase<RepositoryDataSet.BAdIFiltersRow>
    {
      private DataColumn columnName;
      private DataColumn columnType;
      private DataColumn columnDescription;
      private DataColumn columnBAdIProxyName;
      private DataColumn columnMandatory;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public BAdIFiltersDataTable()
      {
        this.TableName = "BAdIFilters";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal BAdIFiltersDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected BAdIFiltersDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn TypeColumn
      {
        get
        {
          return this.columnType;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn DescriptionColumn
      {
        get
        {
          return this.columnDescription;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn BAdIProxyNameColumn
      {
        get
        {
          return this.columnBAdIProxyName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn MandatoryColumn
      {
        get
        {
          return this.columnMandatory;
        }
      }

      [Browsable(false)]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIFiltersRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.BAdIFiltersRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIFiltersRowChangeEventHandler BAdIFiltersRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIFiltersRowChangeEventHandler BAdIFiltersRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIFiltersRowChangeEventHandler BAdIFiltersRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIFiltersRowChangeEventHandler BAdIFiltersRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddBAdIFiltersRow(RepositoryDataSet.BAdIFiltersRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.BAdIFiltersRow AddBAdIFiltersRow(string Name, string Type, string Description, RepositoryDataSet.BAdIsRow parentBAdIsRowByBAdIs_BAdIFilters, bool Mandatory)
      {
        RepositoryDataSet.BAdIFiltersRow badIfiltersRow = (RepositoryDataSet.BAdIFiltersRow) this.NewRow();
        object[] objArray = new object[5]
        {
          (object) Name,
          (object) Type,
          (object) Description,
          null,
          (object) Mandatory
        };
        if (parentBAdIsRowByBAdIs_BAdIFilters != null)
          objArray[3] = parentBAdIsRowByBAdIs_BAdIFilters[2];
        badIfiltersRow.ItemArray = objArray;
        this.Rows.Add((DataRow) badIfiltersRow);
        return badIfiltersRow;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIFiltersRow FindByNameBAdIProxyName(string Name, string BAdIProxyName)
      {
        return (RepositoryDataSet.BAdIFiltersRow) this.Rows.Find(new object[2]
        {
          (object) Name,
          (object) BAdIProxyName
        });
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public override DataTable Clone()
      {
        RepositoryDataSet.BAdIFiltersDataTable ifiltersDataTable = (RepositoryDataSet.BAdIFiltersDataTable) base.Clone();
        ifiltersDataTable.InitVars();
        return (DataTable) ifiltersDataTable;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.BAdIFiltersDataTable();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnName = this.Columns["Name"];
        this.columnType = this.Columns["Type"];
        this.columnDescription = this.Columns["Description"];
        this.columnBAdIProxyName = this.Columns["BAdIProxyName"];
        this.columnMandatory = this.Columns["Mandatory"];
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      private void InitClass()
      {
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnType = new DataColumn("Type", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnType);
        this.columnDescription = new DataColumn("Description", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDescription);
        this.columnBAdIProxyName = new DataColumn("BAdIProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnBAdIProxyName);
        this.columnMandatory = new DataColumn("Mandatory", typeof (bool), (string) null, MappingType.Element);
        this.Columns.Add(this.columnMandatory);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[2]
        {
          this.columnName,
          this.columnBAdIProxyName
        }, true));
        this.columnName.AllowDBNull = false;
        this.columnDescription.DefaultValue = (object) "";
        this.columnBAdIProxyName.AllowDBNull = false;
        this.columnMandatory.DefaultValue = (object) false;
        this.CaseSensitive = true;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIFiltersRow NewBAdIFiltersRow()
      {
        return (RepositoryDataSet.BAdIFiltersRow) this.NewRow();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.BAdIFiltersRow(builder);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.BAdIFiltersRow);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.BAdIFiltersRowChanged == null)
          return;
        this.BAdIFiltersRowChanged((object) this, new RepositoryDataSet.BAdIFiltersRowChangeEvent((RepositoryDataSet.BAdIFiltersRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.BAdIFiltersRowChanging == null)
          return;
        this.BAdIFiltersRowChanging((object) this, new RepositoryDataSet.BAdIFiltersRowChangeEvent((RepositoryDataSet.BAdIFiltersRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.BAdIFiltersRowDeleted == null)
          return;
        this.BAdIFiltersRowDeleted((object) this, new RepositoryDataSet.BAdIFiltersRowChangeEvent((RepositoryDataSet.BAdIFiltersRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.BAdIFiltersRowDeleting == null)
          return;
        this.BAdIFiltersRowDeleting((object) this, new RepositoryDataSet.BAdIFiltersRowChangeEvent((RepositoryDataSet.BAdIFiltersRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void RemoveBAdIFiltersRow(RepositoryDataSet.BAdIFiltersRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (BAdIFiltersDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class BAdIsDataTable : TypedTableBase<RepositoryDataSet.BAdIsRow>
    {
      private DataColumn columnNSName;
      private DataColumn columnName;
      private DataColumn columnProxyName;
      private DataColumn columnDescription;
      private DataColumn columnBusinessObject;
      private DataColumn columnSpotName;
      private DataColumn columnDeploymentUnit;
      private DataColumn columnIsSingleUsed;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public BAdIsDataTable()
      {
        this.TableName = "BAdIs";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal BAdIsDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected BAdIsDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn NSNameColumn
      {
        get
        {
          return this.columnNSName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn ProxyNameColumn
      {
        get
        {
          return this.columnProxyName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn DescriptionColumn
      {
        get
        {
          return this.columnDescription;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn BusinessObjectColumn
      {
        get
        {
          return this.columnBusinessObject;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn SpotNameColumn
      {
        get
        {
          return this.columnSpotName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn DeploymentUnitColumn
      {
        get
        {
          return this.columnDeploymentUnit;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn IsSingleUsedColumn
      {
        get
        {
          return this.columnIsSingleUsed;
        }
      }

      [Browsable(false)]
      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIsRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.BAdIsRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIsRowChangeEventHandler BAdIsRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIsRowChangeEventHandler BAdIsRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIsRowChangeEventHandler BAdIsRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIsRowChangeEventHandler BAdIsRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddBAdIsRow(RepositoryDataSet.BAdIsRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIsRow AddBAdIsRow(RepositoryDataSet.NamespacesRow parentNamespacesRowByNamespaces_BAdIs, string Name, string ProxyName, string Description, string BusinessObject, string SpotName, string DeploymentUnit, bool IsSingleUsed)
      {
        RepositoryDataSet.BAdIsRow badIsRow = (RepositoryDataSet.BAdIsRow) this.NewRow();
        object[] objArray = new object[8]
        {
          null,
          (object) Name,
          (object) ProxyName,
          (object) Description,
          (object) BusinessObject,
          (object) SpotName,
          (object) DeploymentUnit,
          (object) IsSingleUsed
        };
        if (parentNamespacesRowByNamespaces_BAdIs != null)
          objArray[0] = parentNamespacesRowByNamespaces_BAdIs[0];
        badIsRow.ItemArray = objArray;
        this.Rows.Add((DataRow) badIsRow);
        return badIsRow;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIsRow FindByProxyName(string ProxyName)
      {
        return (RepositoryDataSet.BAdIsRow) this.Rows.Find(new object[1]
        {
          (object) ProxyName
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.BAdIsDataTable badIsDataTable = (RepositoryDataSet.BAdIsDataTable) base.Clone();
        badIsDataTable.InitVars();
        return (DataTable) badIsDataTable;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.BAdIsDataTable();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal void InitVars()
      {
        this.columnNSName = this.Columns["NSName"];
        this.columnName = this.Columns["Name"];
        this.columnProxyName = this.Columns["ProxyName"];
        this.columnDescription = this.Columns["Description"];
        this.columnBusinessObject = this.Columns["BusinessObject"];
        this.columnSpotName = this.Columns["SpotName"];
        this.columnDeploymentUnit = this.Columns["DeploymentUnit"];
        this.columnIsSingleUsed = this.Columns["IsSingleUsed"];
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnNSName = new DataColumn("NSName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnNSName);
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnProxyName = new DataColumn("ProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProxyName);
        this.columnDescription = new DataColumn("Description", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDescription);
        this.columnBusinessObject = new DataColumn("BusinessObject", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnBusinessObject);
        this.columnSpotName = new DataColumn("SpotName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnSpotName);
        this.columnDeploymentUnit = new DataColumn("DeploymentUnit", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDeploymentUnit);
        this.columnIsSingleUsed = new DataColumn("IsSingleUsed", typeof (bool), (string) null, MappingType.Element);
        this.Columns.Add(this.columnIsSingleUsed);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[1]
        {
          this.columnProxyName
        }, true));
        this.columnNSName.AllowDBNull = false;
        this.columnName.AllowDBNull = false;
        this.columnProxyName.AllowDBNull = false;
        this.columnProxyName.Unique = true;
        this.CaseSensitive = true;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.BAdIsRow NewBAdIsRow()
      {
        return (RepositoryDataSet.BAdIsRow) this.NewRow();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.BAdIsRow(builder);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.BAdIsRow);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.BAdIsRowChanged == null)
          return;
        this.BAdIsRowChanged((object) this, new RepositoryDataSet.BAdIsRowChangeEvent((RepositoryDataSet.BAdIsRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.BAdIsRowChanging == null)
          return;
        this.BAdIsRowChanging((object) this, new RepositoryDataSet.BAdIsRowChangeEvent((RepositoryDataSet.BAdIsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.BAdIsRowDeleted == null)
          return;
        this.BAdIsRowDeleted((object) this, new RepositoryDataSet.BAdIsRowChangeEvent((RepositoryDataSet.BAdIsRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.BAdIsRowDeleting == null)
          return;
        this.BAdIsRowDeleting((object) this, new RepositoryDataSet.BAdIsRowChangeEvent((RepositoryDataSet.BAdIsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void RemoveBAdIsRow(RepositoryDataSet.BAdIsRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (BAdIsDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class DataTypesDataTable : TypedTableBase<RepositoryDataSet.DataTypesRow>
    {
      private DataColumn columnNSName;
      private DataColumn columnName;
      private DataColumn columnProxyName;
      private DataColumn columnDT_UUID;
      private DataColumn columnLastChangeAt;
      private DataColumn columnBaseDTKeyName;
      private DataColumn columnUsageCategory;
      private DataColumn columnExtensible;
      private DataColumn columnTransitiveHash;
      private DataColumn columnRepresentationTerm;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataTypesDataTable()
      {
        this.TableName = "DataTypes";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal DataTypesDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected DataTypesDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NSNameColumn
      {
        get
        {
          return this.columnNSName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn ProxyNameColumn
      {
        get
        {
          return this.columnProxyName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn DT_UUIDColumn
      {
        get
        {
          return this.columnDT_UUID;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn LastChangeAtColumn
      {
        get
        {
          return this.columnLastChangeAt;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn BaseDTKeyNameColumn
      {
        get
        {
          return this.columnBaseDTKeyName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn UsageCategoryColumn
      {
        get
        {
          return this.columnUsageCategory;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn ExtensibleColumn
      {
        get
        {
          return this.columnExtensible;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn TransitiveHashColumn
      {
        get
        {
          return this.columnTransitiveHash;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn RepresentationTermColumn
      {
        get
        {
          return this.columnRepresentationTerm;
        }
      }

      [Browsable(false)]
      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.DataTypesRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.DataTypesRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.DataTypesRowChangeEventHandler DataTypesRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.DataTypesRowChangeEventHandler DataTypesRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.DataTypesRowChangeEventHandler DataTypesRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.DataTypesRowChangeEventHandler DataTypesRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddDataTypesRow(RepositoryDataSet.DataTypesRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.DataTypesRow AddDataTypesRow(RepositoryDataSet.NamespacesRow parentNamespacesRowByNamespaces_Datatypes, string Name, string ProxyName, string DT_UUID, DateTime LastChangeAt, string BaseDTKeyName, string UsageCategory, bool Extensible, string TransitiveHash, string RepresentationTerm)
      {
        RepositoryDataSet.DataTypesRow dataTypesRow = (RepositoryDataSet.DataTypesRow) this.NewRow();
        object[] objArray = new object[10]
        {
          null,
          (object) Name,
          (object) ProxyName,
          (object) DT_UUID,
          (object) LastChangeAt,
          (object) BaseDTKeyName,
          (object) UsageCategory,
          (object) Extensible,
          (object) TransitiveHash,
          (object) RepresentationTerm
        };
        if (parentNamespacesRowByNamespaces_Datatypes != null)
          objArray[0] = parentNamespacesRowByNamespaces_Datatypes[0];
        dataTypesRow.ItemArray = objArray;
        this.Rows.Add((DataRow) dataTypesRow);
        return dataTypesRow;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.DataTypesRow FindByNSNameName(string NSName, string Name)
      {
        return (RepositoryDataSet.DataTypesRow) this.Rows.Find(new object[2]
        {
          (object) NSName,
          (object) Name
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.DataTypesDataTable dataTypesDataTable = (RepositoryDataSet.DataTypesDataTable) base.Clone();
        dataTypesDataTable.InitVars();
        return (DataTable) dataTypesDataTable;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.DataTypesDataTable();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnNSName = this.Columns["NSName"];
        this.columnName = this.Columns["Name"];
        this.columnProxyName = this.Columns["ProxyName"];
        this.columnDT_UUID = this.Columns["DT_UUID"];
        this.columnLastChangeAt = this.Columns["LastChangeAt"];
        this.columnBaseDTKeyName = this.Columns["BaseDTKeyName"];
        this.columnUsageCategory = this.Columns["UsageCategory"];
        this.columnExtensible = this.Columns["Extensible"];
        this.columnTransitiveHash = this.Columns["TransitiveHash"];
        this.columnRepresentationTerm = this.Columns["RepresentationTerm"];
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      private void InitClass()
      {
        this.columnNSName = new DataColumn("NSName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnNSName);
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnProxyName = new DataColumn("ProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProxyName);
        this.columnDT_UUID = new DataColumn("DT_UUID", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDT_UUID);
        this.columnLastChangeAt = new DataColumn("LastChangeAt", typeof (DateTime), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLastChangeAt);
        this.columnBaseDTKeyName = new DataColumn("BaseDTKeyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnBaseDTKeyName);
        this.columnUsageCategory = new DataColumn("UsageCategory", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnUsageCategory);
        this.columnExtensible = new DataColumn("Extensible", typeof (bool), (string) null, MappingType.Element);
        this.Columns.Add(this.columnExtensible);
        this.columnTransitiveHash = new DataColumn("TransitiveHash", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnTransitiveHash);
        this.columnRepresentationTerm = new DataColumn("RepresentationTerm", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnRepresentationTerm);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[2]
        {
          this.columnNSName,
          this.columnName
        }, true));
        this.columnNSName.AllowDBNull = false;
        this.columnName.AllowDBNull = false;
        this.columnBaseDTKeyName.MaxLength = 30;
        this.columnRepresentationTerm.MaxLength = 2;
        this.CaseSensitive = true;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.DataTypesRow NewDataTypesRow()
      {
        return (RepositoryDataSet.DataTypesRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.DataTypesRow(builder);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.DataTypesRow);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.DataTypesRowChanged == null)
          return;
        this.DataTypesRowChanged((object) this, new RepositoryDataSet.DataTypesRowChangeEvent((RepositoryDataSet.DataTypesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.DataTypesRowChanging == null)
          return;
        this.DataTypesRowChanging((object) this, new RepositoryDataSet.DataTypesRowChangeEvent((RepositoryDataSet.DataTypesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.DataTypesRowDeleted == null)
          return;
        this.DataTypesRowDeleted((object) this, new RepositoryDataSet.DataTypesRowChangeEvent((RepositoryDataSet.DataTypesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.DataTypesRowDeleting == null)
          return;
        this.DataTypesRowDeleting((object) this, new RepositoryDataSet.DataTypesRowChangeEvent((RepositoryDataSet.DataTypesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void RemoveDataTypesRow(RepositoryDataSet.DataTypesRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (DataTypesDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class MsgDataTypeNodesDataTable : TypedTableBase<RepositoryDataSet.MsgDataTypeNodesRow>
    {
      private DataColumn columnMsgTypeName;
      private DataColumn columnName;
      private DataColumn columnParentName;
      private DataColumn columnPath;
      private DataColumn columnDataTypeName;
      private DataColumn columnExtensible;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public MsgDataTypeNodesDataTable()
      {
        this.TableName = "MsgDataTypeNodes";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal MsgDataTypeNodesDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected MsgDataTypeNodesDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn MsgTypeNameColumn
      {
        get
        {
          return this.columnMsgTypeName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn ParentNameColumn
      {
        get
        {
          return this.columnParentName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn PathColumn
      {
        get
        {
          return this.columnPath;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn DataTypeNameColumn
      {
        get
        {
          return this.columnDataTypeName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn ExtensibleColumn
      {
        get
        {
          return this.columnExtensible;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      [Browsable(false)]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.MsgDataTypeNodesRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.MsgDataTypeNodesRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.MsgDataTypeNodesRowChangeEventHandler MsgDataTypeNodesRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.MsgDataTypeNodesRowChangeEventHandler MsgDataTypeNodesRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.MsgDataTypeNodesRowChangeEventHandler MsgDataTypeNodesRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.MsgDataTypeNodesRowChangeEventHandler MsgDataTypeNodesRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddMsgDataTypeNodesRow(RepositoryDataSet.MsgDataTypeNodesRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.MsgDataTypeNodesRow AddMsgDataTypeNodesRow(RepositoryDataSet.MessageTypesRow parentMessageTypesRowByMessagetypes_DataTypeNodes, string Name, string ParentName, string Path, RepositoryDataSet.DataTypesRow parentDataTypesRowByDataTypes_DataTypeNodes, bool Extensible)
      {
        RepositoryDataSet.MsgDataTypeNodesRow dataTypeNodesRow = (RepositoryDataSet.MsgDataTypeNodesRow) this.NewRow();
        object[] objArray = new object[6]
        {
          null,
          (object) Name,
          (object) ParentName,
          (object) Path,
          null,
          (object) Extensible
        };
        if (parentMessageTypesRowByMessagetypes_DataTypeNodes != null)
          objArray[0] = parentMessageTypesRowByMessagetypes_DataTypeNodes[1];
        if (parentDataTypesRowByDataTypes_DataTypeNodes != null)
          objArray[4] = parentDataTypesRowByDataTypes_DataTypeNodes[1];
        dataTypeNodesRow.ItemArray = objArray;
        this.Rows.Add((DataRow) dataTypeNodesRow);
        return dataTypeNodesRow;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.MsgDataTypeNodesRow FindByMsgTypeNameName(string MsgTypeName, string Name)
      {
        return (RepositoryDataSet.MsgDataTypeNodesRow) this.Rows.Find(new object[2]
        {
          (object) MsgTypeName,
          (object) Name
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.MsgDataTypeNodesDataTable typeNodesDataTable = (RepositoryDataSet.MsgDataTypeNodesDataTable) base.Clone();
        typeNodesDataTable.InitVars();
        return (DataTable) typeNodesDataTable;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.MsgDataTypeNodesDataTable();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnMsgTypeName = this.Columns["MsgTypeName"];
        this.columnName = this.Columns["Name"];
        this.columnParentName = this.Columns["ParentName"];
        this.columnPath = this.Columns["Path"];
        this.columnDataTypeName = this.Columns["DataTypeName"];
        this.columnExtensible = this.Columns["Extensible"];
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      private void InitClass()
      {
        this.columnMsgTypeName = new DataColumn("MsgTypeName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnMsgTypeName);
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnParentName = new DataColumn("ParentName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnParentName);
        this.columnPath = new DataColumn("Path", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnPath);
        this.columnDataTypeName = new DataColumn("DataTypeName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDataTypeName);
        this.columnExtensible = new DataColumn("Extensible", typeof (bool), (string) null, MappingType.Element);
        this.Columns.Add(this.columnExtensible);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[2]
        {
          this.columnMsgTypeName,
          this.columnName
        }, true));
        this.columnMsgTypeName.AllowDBNull = false;
        this.columnName.AllowDBNull = false;
        this.CaseSensitive = true;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.MsgDataTypeNodesRow NewMsgDataTypeNodesRow()
      {
        return (RepositoryDataSet.MsgDataTypeNodesRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.MsgDataTypeNodesRow(builder);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.MsgDataTypeNodesRow);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.MsgDataTypeNodesRowChanged == null)
          return;
        this.MsgDataTypeNodesRowChanged((object) this, new RepositoryDataSet.MsgDataTypeNodesRowChangeEvent((RepositoryDataSet.MsgDataTypeNodesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.MsgDataTypeNodesRowChanging == null)
          return;
        this.MsgDataTypeNodesRowChanging((object) this, new RepositoryDataSet.MsgDataTypeNodesRowChangeEvent((RepositoryDataSet.MsgDataTypeNodesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.MsgDataTypeNodesRowDeleted == null)
          return;
        this.MsgDataTypeNodesRowDeleted((object) this, new RepositoryDataSet.MsgDataTypeNodesRowChangeEvent((RepositoryDataSet.MsgDataTypeNodesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.MsgDataTypeNodesRowDeleting == null)
          return;
        this.MsgDataTypeNodesRowDeleting((object) this, new RepositoryDataSet.MsgDataTypeNodesRowChangeEvent((RepositoryDataSet.MsgDataTypeNodesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void RemoveMsgDataTypeNodesRow(RepositoryDataSet.MsgDataTypeNodesRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (MsgDataTypeNodesDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class MessageTypesDataTable : TypedTableBase<RepositoryDataSet.MessageTypesRow>
    {
      private DataColumn columnNamespace;
      private DataColumn columnName;
      private DataColumn columnProxyName;
      private DataColumn columnDataTypeName;
      private DataColumn columnLeadingBOName;
      private DataColumn columnDataTypeFieldName;
      private DataColumn columnLastChangedAt;
      private DataColumn columnTrasitiveHash;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public MessageTypesDataTable()
      {
        this.TableName = "MessageTypes";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal MessageTypesDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected MessageTypesDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NamespaceColumn
      {
        get
        {
          return this.columnNamespace;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn ProxyNameColumn
      {
        get
        {
          return this.columnProxyName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn DataTypeNameColumn
      {
        get
        {
          return this.columnDataTypeName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn LeadingBONameColumn
      {
        get
        {
          return this.columnLeadingBOName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn DataTypeFieldNameColumn
      {
        get
        {
          return this.columnDataTypeFieldName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn LastChangedAtColumn
      {
        get
        {
          return this.columnLastChangedAt;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn TrasitiveHashColumn
      {
        get
        {
          return this.columnTrasitiveHash;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [Browsable(false)]
      [DebuggerNonUserCode]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.MessageTypesRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.MessageTypesRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.MessageTypesRowChangeEventHandler MessageTypesRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.MessageTypesRowChangeEventHandler MessageTypesRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.MessageTypesRowChangeEventHandler MessageTypesRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.MessageTypesRowChangeEventHandler MessageTypesRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddMessageTypesRow(RepositoryDataSet.MessageTypesRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.MessageTypesRow AddMessageTypesRow(RepositoryDataSet.NamespacesRow parentNamespacesRowByNamespaces_MessageTypes, string Name, string ProxyName, RepositoryDataSet.DataTypesRow parentDataTypesRowByDataTypes_MessageTypes, string LeadingBOName, string DataTypeFieldName, DateTime LastChangedAt, string TrasitiveHash)
      {
        RepositoryDataSet.MessageTypesRow messageTypesRow = (RepositoryDataSet.MessageTypesRow) this.NewRow();
        object[] objArray = new object[8]
        {
          null,
          (object) Name,
          (object) ProxyName,
          null,
          (object) LeadingBOName,
          (object) DataTypeFieldName,
          (object) LastChangedAt,
          (object) TrasitiveHash
        };
        if (parentNamespacesRowByNamespaces_MessageTypes != null)
          objArray[0] = parentNamespacesRowByNamespaces_MessageTypes[0];
        if (parentDataTypesRowByDataTypes_MessageTypes != null)
          objArray[3] = parentDataTypesRowByDataTypes_MessageTypes[1];
        messageTypesRow.ItemArray = objArray;
        this.Rows.Add((DataRow) messageTypesRow);
        return messageTypesRow;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.MessageTypesRow FindByNamespaceName(string Namespace, string Name)
      {
        return (RepositoryDataSet.MessageTypesRow) this.Rows.Find(new object[2]
        {
          (object) Namespace,
          (object) Name
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.MessageTypesDataTable messageTypesDataTable = (RepositoryDataSet.MessageTypesDataTable) base.Clone();
        messageTypesDataTable.InitVars();
        return (DataTable) messageTypesDataTable;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.MessageTypesDataTable();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnNamespace = this.Columns["Namespace"];
        this.columnName = this.Columns["Name"];
        this.columnProxyName = this.Columns["ProxyName"];
        this.columnDataTypeName = this.Columns["DataTypeName"];
        this.columnLeadingBOName = this.Columns["LeadingBOName"];
        this.columnDataTypeFieldName = this.Columns["DataTypeFieldName"];
        this.columnLastChangedAt = this.Columns["LastChangedAt"];
        this.columnTrasitiveHash = this.Columns["TrasitiveHash"];
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnNamespace = new DataColumn("Namespace", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnNamespace);
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnProxyName = new DataColumn("ProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProxyName);
        this.columnDataTypeName = new DataColumn("DataTypeName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDataTypeName);
        this.columnLeadingBOName = new DataColumn("LeadingBOName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLeadingBOName);
        this.columnDataTypeFieldName = new DataColumn("DataTypeFieldName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDataTypeFieldName);
        this.columnLastChangedAt = new DataColumn("LastChangedAt", typeof (DateTime), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLastChangedAt);
        this.columnTrasitiveHash = new DataColumn("TrasitiveHash", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnTrasitiveHash);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[2]
        {
          this.columnNamespace,
          this.columnName
        }, true));
        this.columnNamespace.AllowDBNull = false;
        this.columnName.AllowDBNull = false;
        this.CaseSensitive = true;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.MessageTypesRow NewMessageTypesRow()
      {
        return (RepositoryDataSet.MessageTypesRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.MessageTypesRow(builder);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.MessageTypesRow);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.MessageTypesRowChanged == null)
          return;
        this.MessageTypesRowChanged((object) this, new RepositoryDataSet.MessageTypesRowChangeEvent((RepositoryDataSet.MessageTypesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.MessageTypesRowChanging == null)
          return;
        this.MessageTypesRowChanging((object) this, new RepositoryDataSet.MessageTypesRowChangeEvent((RepositoryDataSet.MessageTypesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.MessageTypesRowDeleted == null)
          return;
        this.MessageTypesRowDeleted((object) this, new RepositoryDataSet.MessageTypesRowChangeEvent((RepositoryDataSet.MessageTypesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.MessageTypesRowDeleting == null)
          return;
        this.MessageTypesRowDeleting((object) this, new RepositoryDataSet.MessageTypesRowChangeEvent((RepositoryDataSet.MessageTypesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void RemoveMessageTypesRow(RepositoryDataSet.MessageTypesRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (MessageTypesDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class BusinessObjectsDataTable : TypedTableBase<RepositoryDataSet.BusinessObjectsRow>
    {
      private DataColumn columnNSName;
      private DataColumn columnName;
      private DataColumn columnProxyName;
      private DataColumn columnBO_UUID;
      private DataColumn columnLastChangeAt;
      private DataColumn columnObjectCategory;
      private DataColumn columnTechCategory;
      private DataColumn columnLifecycleStat;
      private DataColumn columnDeploymentUnit;
      private DataColumn columnWriteAccess;
      private DataColumn columnTransitiveHash;
      private DataColumn columnDeprecated;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public BusinessObjectsDataTable()
      {
        this.TableName = "BusinessObjects";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal BusinessObjectsDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected BusinessObjectsDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NSNameColumn
      {
        get
        {
          return this.columnNSName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn ProxyNameColumn
      {
        get
        {
          return this.columnProxyName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn BO_UUIDColumn
      {
        get
        {
          return this.columnBO_UUID;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn LastChangeAtColumn
      {
        get
        {
          return this.columnLastChangeAt;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn ObjectCategoryColumn
      {
        get
        {
          return this.columnObjectCategory;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn TechCategoryColumn
      {
        get
        {
          return this.columnTechCategory;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn LifecycleStatColumn
      {
        get
        {
          return this.columnLifecycleStat;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn DeploymentUnitColumn
      {
        get
        {
          return this.columnDeploymentUnit;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn WriteAccessColumn
      {
        get
        {
          return this.columnWriteAccess;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn TransitiveHashColumn
      {
        get
        {
          return this.columnTransitiveHash;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn DeprecatedColumn
      {
        get
        {
          return this.columnDeprecated;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      [Browsable(false)]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.BusinessObjectsRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.BusinessObjectsRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BusinessObjectsRowChangeEventHandler BusinessObjectsRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BusinessObjectsRowChangeEventHandler BusinessObjectsRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BusinessObjectsRowChangeEventHandler BusinessObjectsRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BusinessObjectsRowChangeEventHandler BusinessObjectsRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddBusinessObjectsRow(RepositoryDataSet.BusinessObjectsRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BusinessObjectsRow AddBusinessObjectsRow(RepositoryDataSet.NamespacesRow parentNamespacesRowByNamespaces_BusinessObjects, string Name, string ProxyName, string BO_UUID, DateTime LastChangeAt, string ObjectCategory, string TechCategory, string LifecycleStat, string DeploymentUnit, string WriteAccess, string TransitiveHash, bool Deprecated)
      {
        RepositoryDataSet.BusinessObjectsRow businessObjectsRow = (RepositoryDataSet.BusinessObjectsRow) this.NewRow();
        object[] objArray = new object[12]
        {
          null,
          (object) Name,
          (object) ProxyName,
          (object) BO_UUID,
          (object) LastChangeAt,
          (object) ObjectCategory,
          (object) TechCategory,
          (object) LifecycleStat,
          (object) DeploymentUnit,
          (object) WriteAccess,
          (object) TransitiveHash,
          (object) Deprecated
        };
        if (parentNamespacesRowByNamespaces_BusinessObjects != null)
          objArray[0] = parentNamespacesRowByNamespaces_BusinessObjects[0];
        businessObjectsRow.ItemArray = objArray;
        this.Rows.Add((DataRow) businessObjectsRow);
        return businessObjectsRow;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BusinessObjectsRow FindByNSNameName(string NSName, string Name)
      {
        return (RepositoryDataSet.BusinessObjectsRow) this.Rows.Find(new object[2]
        {
          (object) NSName,
          (object) Name
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.BusinessObjectsDataTable objectsDataTable = (RepositoryDataSet.BusinessObjectsDataTable) base.Clone();
        objectsDataTable.InitVars();
        return (DataTable) objectsDataTable;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.BusinessObjectsDataTable();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal void InitVars()
      {
        this.columnNSName = this.Columns["NSName"];
        this.columnName = this.Columns["Name"];
        this.columnProxyName = this.Columns["ProxyName"];
        this.columnBO_UUID = this.Columns["BO_UUID"];
        this.columnLastChangeAt = this.Columns["LastChangeAt"];
        this.columnObjectCategory = this.Columns["ObjectCategory"];
        this.columnTechCategory = this.Columns["TechCategory"];
        this.columnLifecycleStat = this.Columns["LifecycleStat"];
        this.columnDeploymentUnit = this.Columns["DeploymentUnit"];
        this.columnWriteAccess = this.Columns["WriteAccess"];
        this.columnTransitiveHash = this.Columns["TransitiveHash"];
        this.columnDeprecated = this.Columns["Deprecated"];
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnNSName = new DataColumn("NSName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnNSName);
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnProxyName = new DataColumn("ProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProxyName);
        this.columnBO_UUID = new DataColumn("BO_UUID", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnBO_UUID);
        this.columnLastChangeAt = new DataColumn("LastChangeAt", typeof (DateTime), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLastChangeAt);
        this.columnObjectCategory = new DataColumn("ObjectCategory", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnObjectCategory);
        this.columnTechCategory = new DataColumn("TechCategory", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnTechCategory);
        this.columnLifecycleStat = new DataColumn("LifecycleStat", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLifecycleStat);
        this.columnDeploymentUnit = new DataColumn("DeploymentUnit", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDeploymentUnit);
        this.columnWriteAccess = new DataColumn("WriteAccess", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnWriteAccess);
        this.columnTransitiveHash = new DataColumn("TransitiveHash", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnTransitiveHash);
        this.columnDeprecated = new DataColumn("Deprecated", typeof (bool), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDeprecated);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[2]
        {
          this.columnNSName,
          this.columnName
        }, true));
        this.columnNSName.AllowDBNull = false;
        this.columnName.AllowDBNull = false;
        this.columnObjectCategory.MaxLength = 2;
        this.columnTechCategory.MaxLength = 2;
        this.columnLifecycleStat.MaxLength = 1;
        this.columnWriteAccess.DefaultValue = (object) "Null";
        this.columnDeprecated.AllowDBNull = false;
        this.columnDeprecated.DefaultValue = (object) false;
        this.CaseSensitive = true;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BusinessObjectsRow NewBusinessObjectsRow()
      {
        return (RepositoryDataSet.BusinessObjectsRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.BusinessObjectsRow(builder);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.BusinessObjectsRow);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.BusinessObjectsRowChanged == null)
          return;
        this.BusinessObjectsRowChanged((object) this, new RepositoryDataSet.BusinessObjectsRowChangeEvent((RepositoryDataSet.BusinessObjectsRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.BusinessObjectsRowChanging == null)
          return;
        this.BusinessObjectsRowChanging((object) this, new RepositoryDataSet.BusinessObjectsRowChangeEvent((RepositoryDataSet.BusinessObjectsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.BusinessObjectsRowDeleted == null)
          return;
        this.BusinessObjectsRowDeleted((object) this, new RepositoryDataSet.BusinessObjectsRowChangeEvent((RepositoryDataSet.BusinessObjectsRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.BusinessObjectsRowDeleting == null)
          return;
        this.BusinessObjectsRowDeleting((object) this, new RepositoryDataSet.BusinessObjectsRowChangeEvent((RepositoryDataSet.BusinessObjectsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void RemoveBusinessObjectsRow(RepositoryDataSet.BusinessObjectsRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (BusinessObjectsDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void NamespacesRowChangeEventHandler(object sender, RepositoryDataSet.NamespacesRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void BusinessObjectsRowChangeEventHandler(object sender, RepositoryDataSet.BusinessObjectsRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void DataTypesRowChangeEventHandler(object sender, RepositoryDataSet.DataTypesRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void NodesRowChangeEventHandler(object sender, RepositoryDataSet.NodesRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void ActionsRowChangeEventHandler(object sender, RepositoryDataSet.ActionsRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void SolutionsRowChangeEventHandler(object sender, RepositoryDataSet.SolutionsRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void LibrariesRowChangeEventHandler(object sender, RepositoryDataSet.LibrariesRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void MessageTypesRowChangeEventHandler(object sender, RepositoryDataSet.MessageTypesRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void MsgDataTypeNodesRowChangeEventHandler(object sender, RepositoryDataSet.MsgDataTypeNodesRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void BAdIsRowChangeEventHandler(object sender, RepositoryDataSet.BAdIsRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void BAdIFiltersRowChangeEventHandler(object sender, RepositoryDataSet.BAdIFiltersRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void BAdIMethodsRowChangeEventHandler(object sender, RepositoryDataSet.BAdIMethodsRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void ProjectItemSolutionRowChangeEventHandler(object sender, RepositoryDataSet.ProjectItemSolutionRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void ProjectItemRowChangeEventHandler(object sender, RepositoryDataSet.ProjectItemRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public delegate void FeatureRowChangeEventHandler(object sender, RepositoryDataSet.FeatureRowChangeEvent e);

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class NamespacesDataTable : TypedTableBase<RepositoryDataSet.NamespacesRow>
    {
      private DataColumn columnNSName;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public NamespacesDataTable()
      {
        this.TableName = "Namespaces";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal NamespacesDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected NamespacesDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NSNameColumn
      {
        get
        {
          return this.columnNSName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [Browsable(false)]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.NamespacesRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.NamespacesRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.NamespacesRowChangeEventHandler NamespacesRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.NamespacesRowChangeEventHandler NamespacesRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.NamespacesRowChangeEventHandler NamespacesRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.NamespacesRowChangeEventHandler NamespacesRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddNamespacesRow(RepositoryDataSet.NamespacesRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.NamespacesRow AddNamespacesRow(string NSName)
      {
        RepositoryDataSet.NamespacesRow namespacesRow = (RepositoryDataSet.NamespacesRow) this.NewRow();
        object[] objArray = new object[1]{ (object) NSName };
        namespacesRow.ItemArray = objArray;
        this.Rows.Add((DataRow) namespacesRow);
        return namespacesRow;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.NamespacesRow FindByNSName(string NSName)
      {
        return (RepositoryDataSet.NamespacesRow) this.Rows.Find(new object[1]
        {
          (object) NSName
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.NamespacesDataTable namespacesDataTable = (RepositoryDataSet.NamespacesDataTable) base.Clone();
        namespacesDataTable.InitVars();
        return (DataTable) namespacesDataTable;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.NamespacesDataTable();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnNSName = this.Columns["NSName"];
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnNSName = new DataColumn("NSName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnNSName);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[1]
        {
          this.columnNSName
        }, true));
        this.columnNSName.AllowDBNull = false;
        this.columnNSName.Unique = true;
        this.CaseSensitive = true;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.NamespacesRow NewNamespacesRow()
      {
        return (RepositoryDataSet.NamespacesRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.NamespacesRow(builder);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.NamespacesRow);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.NamespacesRowChanged == null)
          return;
        this.NamespacesRowChanged((object) this, new RepositoryDataSet.NamespacesRowChangeEvent((RepositoryDataSet.NamespacesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.NamespacesRowChanging == null)
          return;
        this.NamespacesRowChanging((object) this, new RepositoryDataSet.NamespacesRowChangeEvent((RepositoryDataSet.NamespacesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.NamespacesRowDeleted == null)
          return;
        this.NamespacesRowDeleted((object) this, new RepositoryDataSet.NamespacesRowChangeEvent((RepositoryDataSet.NamespacesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.NamespacesRowDeleting == null)
          return;
        this.NamespacesRowDeleting((object) this, new RepositoryDataSet.NamespacesRowChangeEvent((RepositoryDataSet.NamespacesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void RemoveNamespacesRow(RepositoryDataSet.NamespacesRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (NamespacesDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class NodesDataTable : TypedTableBase<RepositoryDataSet.NodesRow>
    {
      private DataColumn columnBOProxyName;
      private DataColumn columnName;
      private DataColumn columnProxyName;
      private DataColumn columnParentNodeName;
      private DataColumn columnWriteAccess;
      private DataColumn columnCategory;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public NodesDataTable()
      {
        this.TableName = "Nodes";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal NodesDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected NodesDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn BOProxyNameColumn
      {
        get
        {
          return this.columnBOProxyName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn ProxyNameColumn
      {
        get
        {
          return this.columnProxyName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn ParentNodeNameColumn
      {
        get
        {
          return this.columnParentNodeName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn WriteAccessColumn
      {
        get
        {
          return this.columnWriteAccess;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn CategoryColumn
      {
        get
        {
          return this.columnCategory;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [Browsable(false)]
      [DebuggerNonUserCode]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.NodesRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.NodesRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.NodesRowChangeEventHandler NodesRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.NodesRowChangeEventHandler NodesRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.NodesRowChangeEventHandler NodesRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.NodesRowChangeEventHandler NodesRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddNodesRow(RepositoryDataSet.NodesRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.NodesRow AddNodesRow(RepositoryDataSet.BusinessObjectsRow parentBusinessObjectsRowByBusinessObjects_Nodes, string Name, string ProxyName, string ParentNodeName, string WriteAccess, string Category)
      {
        RepositoryDataSet.NodesRow nodesRow = (RepositoryDataSet.NodesRow) this.NewRow();
        object[] objArray = new object[6]
        {
          null,
          (object) Name,
          (object) ProxyName,
          (object) ParentNodeName,
          (object) WriteAccess,
          (object) Category
        };
        if (parentBusinessObjectsRowByBusinessObjects_Nodes != null)
          objArray[0] = parentBusinessObjectsRowByBusinessObjects_Nodes[2];
        nodesRow.ItemArray = objArray;
        this.Rows.Add((DataRow) nodesRow);
        return nodesRow;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.NodesRow FindByBOProxyNameName(string BOProxyName, string Name)
      {
        return (RepositoryDataSet.NodesRow) this.Rows.Find(new object[2]
        {
          (object) BOProxyName,
          (object) Name
        });
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public override DataTable Clone()
      {
        RepositoryDataSet.NodesDataTable nodesDataTable = (RepositoryDataSet.NodesDataTable) base.Clone();
        nodesDataTable.InitVars();
        return (DataTable) nodesDataTable;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.NodesDataTable();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnBOProxyName = this.Columns["BOProxyName"];
        this.columnName = this.Columns["Name"];
        this.columnProxyName = this.Columns["ProxyName"];
        this.columnParentNodeName = this.Columns["ParentNodeName"];
        this.columnWriteAccess = this.Columns["WriteAccess"];
        this.columnCategory = this.Columns["Category"];
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      private void InitClass()
      {
        this.columnBOProxyName = new DataColumn("BOProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnBOProxyName);
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnProxyName = new DataColumn("ProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProxyName);
        this.columnParentNodeName = new DataColumn("ParentNodeName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnParentNodeName);
        this.columnWriteAccess = new DataColumn("WriteAccess", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnWriteAccess);
        this.columnCategory = new DataColumn("Category", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCategory);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[2]
        {
          this.columnBOProxyName,
          this.columnName
        }, true));
        this.columnBOProxyName.AllowDBNull = false;
        this.columnName.AllowDBNull = false;
        this.columnCategory.MaxLength = 1;
        this.CaseSensitive = true;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.NodesRow NewNodesRow()
      {
        return (RepositoryDataSet.NodesRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.NodesRow(builder);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.NodesRow);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.NodesRowChanged == null)
          return;
        this.NodesRowChanged((object) this, new RepositoryDataSet.NodesRowChangeEvent((RepositoryDataSet.NodesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.NodesRowChanging == null)
          return;
        this.NodesRowChanging((object) this, new RepositoryDataSet.NodesRowChangeEvent((RepositoryDataSet.NodesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.NodesRowDeleted == null)
          return;
        this.NodesRowDeleted((object) this, new RepositoryDataSet.NodesRowChangeEvent((RepositoryDataSet.NodesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.NodesRowDeleting == null)
          return;
        this.NodesRowDeleting((object) this, new RepositoryDataSet.NodesRowChangeEvent((RepositoryDataSet.NodesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void RemoveNodesRow(RepositoryDataSet.NodesRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (NodesDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class ActionsDataTable : TypedTableBase<RepositoryDataSet.ActionsRow>
    {
      private DataColumn columnBOProxyName;
      private DataColumn columnNodeProxyName;
      private DataColumn columnName;
      private DataColumn columnProxyName;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public ActionsDataTable()
      {
        this.TableName = "Actions";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal ActionsDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected ActionsDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn BOProxyNameColumn
      {
        get
        {
          return this.columnBOProxyName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn NodeProxyNameColumn
      {
        get
        {
          return this.columnNodeProxyName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn ProxyNameColumn
      {
        get
        {
          return this.columnProxyName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      [Browsable(false)]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ActionsRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.ActionsRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ActionsRowChangeEventHandler ActionsRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ActionsRowChangeEventHandler ActionsRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ActionsRowChangeEventHandler ActionsRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ActionsRowChangeEventHandler ActionsRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddActionsRow(RepositoryDataSet.ActionsRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ActionsRow AddActionsRow(string BOProxyName, string NodeProxyName, string Name, string ProxyName)
      {
        RepositoryDataSet.ActionsRow actionsRow = (RepositoryDataSet.ActionsRow) this.NewRow();
        object[] objArray = new object[4]
        {
          (object) BOProxyName,
          (object) NodeProxyName,
          (object) Name,
          (object) ProxyName
        };
        actionsRow.ItemArray = objArray;
        this.Rows.Add((DataRow) actionsRow);
        return actionsRow;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ActionsRow FindByBOProxyNameNodeProxyNameName(string BOProxyName, string NodeProxyName, string Name)
      {
        return (RepositoryDataSet.ActionsRow) this.Rows.Find(new object[3]
        {
          (object) BOProxyName,
          (object) NodeProxyName,
          (object) Name
        });
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public override DataTable Clone()
      {
        RepositoryDataSet.ActionsDataTable actionsDataTable = (RepositoryDataSet.ActionsDataTable) base.Clone();
        actionsDataTable.InitVars();
        return (DataTable) actionsDataTable;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.ActionsDataTable();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnBOProxyName = this.Columns["BOProxyName"];
        this.columnNodeProxyName = this.Columns["NodeProxyName"];
        this.columnName = this.Columns["Name"];
        this.columnProxyName = this.Columns["ProxyName"];
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnBOProxyName = new DataColumn("BOProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnBOProxyName);
        this.columnNodeProxyName = new DataColumn("NodeProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnNodeProxyName);
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnProxyName = new DataColumn("ProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProxyName);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[3]
        {
          this.columnBOProxyName,
          this.columnNodeProxyName,
          this.columnName
        }, true));
        this.columnBOProxyName.AllowDBNull = false;
        this.columnNodeProxyName.AllowDBNull = false;
        this.columnName.AllowDBNull = false;
        this.CaseSensitive = true;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ActionsRow NewActionsRow()
      {
        return (RepositoryDataSet.ActionsRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.ActionsRow(builder);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.ActionsRow);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.ActionsRowChanged == null)
          return;
        this.ActionsRowChanged((object) this, new RepositoryDataSet.ActionsRowChangeEvent((RepositoryDataSet.ActionsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.ActionsRowChanging == null)
          return;
        this.ActionsRowChanging((object) this, new RepositoryDataSet.ActionsRowChangeEvent((RepositoryDataSet.ActionsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.ActionsRowDeleted == null)
          return;
        this.ActionsRowDeleted((object) this, new RepositoryDataSet.ActionsRowChangeEvent((RepositoryDataSet.ActionsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.ActionsRowDeleting == null)
          return;
        this.ActionsRowDeleting((object) this, new RepositoryDataSet.ActionsRowChangeEvent((RepositoryDataSet.ActionsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void RemoveActionsRow(RepositoryDataSet.ActionsRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (ActionsDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class LibrariesDataTable : TypedTableBase<RepositoryDataSet.LibrariesRow>
    {
      private DataColumn columnProxyName;
      private DataColumn columnName;
      private DataColumn columnNamespace;
      private DataColumn columnLastChangeAt;
      private DataColumn columnTypeCode;
      private DataColumn columnCallType;
      private DataColumn columnTransitiveHash;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public LibrariesDataTable()
      {
        this.TableName = "Libraries";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal LibrariesDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected LibrariesDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn ProxyNameColumn
      {
        get
        {
          return this.columnProxyName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NamespaceColumn
      {
        get
        {
          return this.columnNamespace;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn LastChangeAtColumn
      {
        get
        {
          return this.columnLastChangeAt;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn TypeCodeColumn
      {
        get
        {
          return this.columnTypeCode;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn CallTypeColumn
      {
        get
        {
          return this.columnCallType;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn TransitiveHashColumn
      {
        get
        {
          return this.columnTransitiveHash;
        }
      }

      [Browsable(false)]
      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.LibrariesRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.LibrariesRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.LibrariesRowChangeEventHandler LibrariesRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.LibrariesRowChangeEventHandler LibrariesRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.LibrariesRowChangeEventHandler LibrariesRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.LibrariesRowChangeEventHandler LibrariesRowDeleted;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void AddLibrariesRow(RepositoryDataSet.LibrariesRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.LibrariesRow AddLibrariesRow(string ProxyName, string Name, string Namespace, DateTime LastChangeAt, string TypeCode, string CallType, string TransitiveHash)
      {
        RepositoryDataSet.LibrariesRow librariesRow = (RepositoryDataSet.LibrariesRow) this.NewRow();
        object[] objArray = new object[7]
        {
          (object) ProxyName,
          (object) Name,
          (object) Namespace,
          (object) LastChangeAt,
          (object) TypeCode,
          (object) CallType,
          (object) TransitiveHash
        };
        librariesRow.ItemArray = objArray;
        this.Rows.Add((DataRow) librariesRow);
        return librariesRow;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.LibrariesRow FindByProxyName(string ProxyName)
      {
        return (RepositoryDataSet.LibrariesRow) this.Rows.Find(new object[1]
        {
          (object) ProxyName
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.LibrariesDataTable librariesDataTable = (RepositoryDataSet.LibrariesDataTable) base.Clone();
        librariesDataTable.InitVars();
        return (DataTable) librariesDataTable;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.LibrariesDataTable();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnProxyName = this.Columns["ProxyName"];
        this.columnName = this.Columns["Name"];
        this.columnNamespace = this.Columns["Namespace"];
        this.columnLastChangeAt = this.Columns["LastChangeAt"];
        this.columnTypeCode = this.Columns["TypeCode"];
        this.columnCallType = this.Columns["CallType"];
        this.columnTransitiveHash = this.Columns["TransitiveHash"];
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      private void InitClass()
      {
        this.columnProxyName = new DataColumn("ProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProxyName);
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnNamespace = new DataColumn("Namespace", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnNamespace);
        this.columnLastChangeAt = new DataColumn("LastChangeAt", typeof (DateTime), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLastChangeAt);
        this.columnTypeCode = new DataColumn("TypeCode", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnTypeCode);
        this.columnCallType = new DataColumn("CallType", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCallType);
        this.columnTransitiveHash = new DataColumn("TransitiveHash", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnTransitiveHash);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[1]
        {
          this.columnProxyName
        }, true));
        this.columnProxyName.AllowDBNull = false;
        this.columnProxyName.Unique = true;
        this.columnName.AllowDBNull = false;
        this.columnNamespace.AllowDBNull = false;
        this.columnTypeCode.MaxLength = 2;
        this.columnCallType.MaxLength = 1;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.LibrariesRow NewLibrariesRow()
      {
        return (RepositoryDataSet.LibrariesRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.LibrariesRow(builder);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.LibrariesRow);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.LibrariesRowChanged == null)
          return;
        this.LibrariesRowChanged((object) this, new RepositoryDataSet.LibrariesRowChangeEvent((RepositoryDataSet.LibrariesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.LibrariesRowChanging == null)
          return;
        this.LibrariesRowChanging((object) this, new RepositoryDataSet.LibrariesRowChangeEvent((RepositoryDataSet.LibrariesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.LibrariesRowDeleted == null)
          return;
        this.LibrariesRowDeleted((object) this, new RepositoryDataSet.LibrariesRowChangeEvent((RepositoryDataSet.LibrariesRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.LibrariesRowDeleting == null)
          return;
        this.LibrariesRowDeleting((object) this, new RepositoryDataSet.LibrariesRowChangeEvent((RepositoryDataSet.LibrariesRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void RemoveLibrariesRow(RepositoryDataSet.LibrariesRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (LibrariesDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class BAdIMethodsDataTable : TypedTableBase<RepositoryDataSet.BAdIMethodsRow>
    {
      private DataColumn columnName;
      private DataColumn columnBAdIProxyName;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public BAdIMethodsDataTable()
      {
        this.TableName = "BAdIMethods";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal BAdIMethodsDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected BAdIMethodsDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn NameColumn
      {
        get
        {
          return this.columnName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn BAdIProxyNameColumn
      {
        get
        {
          return this.columnBAdIProxyName;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [Browsable(false)]
      [DebuggerNonUserCode]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.BAdIMethodsRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.BAdIMethodsRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIMethodsRowChangeEventHandler BAdIMethodsRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIMethodsRowChangeEventHandler BAdIMethodsRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIMethodsRowChangeEventHandler BAdIMethodsRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.BAdIMethodsRowChangeEventHandler BAdIMethodsRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddBAdIMethodsRow(RepositoryDataSet.BAdIMethodsRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.BAdIMethodsRow AddBAdIMethodsRow(string Name, RepositoryDataSet.BAdIsRow parentBAdIsRowByBAdIs_BAdIMethod)
      {
        RepositoryDataSet.BAdIMethodsRow badImethodsRow = (RepositoryDataSet.BAdIMethodsRow) this.NewRow();
        object[] objArray = new object[2]
        {
          (object) Name,
          null
        };
        if (parentBAdIsRowByBAdIs_BAdIMethod != null)
          objArray[1] = parentBAdIsRowByBAdIs_BAdIMethod[2];
        badImethodsRow.ItemArray = objArray;
        this.Rows.Add((DataRow) badImethodsRow);
        return badImethodsRow;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIMethodsRow FindByNameBAdIProxyName(string Name, string BAdIProxyName)
      {
        return (RepositoryDataSet.BAdIMethodsRow) this.Rows.Find(new object[2]
        {
          (object) Name,
          (object) BAdIProxyName
        });
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public override DataTable Clone()
      {
        RepositoryDataSet.BAdIMethodsDataTable imethodsDataTable = (RepositoryDataSet.BAdIMethodsDataTable) base.Clone();
        imethodsDataTable.InitVars();
        return (DataTable) imethodsDataTable;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.BAdIMethodsDataTable();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnName = this.Columns["Name"];
        this.columnBAdIProxyName = this.Columns["BAdIProxyName"];
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      private void InitClass()
      {
        this.columnName = new DataColumn("Name", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnName);
        this.columnBAdIProxyName = new DataColumn("BAdIProxyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnBAdIProxyName);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[2]
        {
          this.columnName,
          this.columnBAdIProxyName
        }, true));
        this.columnName.AllowDBNull = false;
        this.columnBAdIProxyName.AllowDBNull = false;
        this.CaseSensitive = true;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIMethodsRow NewBAdIMethodsRow()
      {
        return (RepositoryDataSet.BAdIMethodsRow) this.NewRow();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.BAdIMethodsRow(builder);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.BAdIMethodsRow);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.BAdIMethodsRowChanged == null)
          return;
        this.BAdIMethodsRowChanged((object) this, new RepositoryDataSet.BAdIMethodsRowChangeEvent((RepositoryDataSet.BAdIMethodsRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.BAdIMethodsRowChanging == null)
          return;
        this.BAdIMethodsRowChanging((object) this, new RepositoryDataSet.BAdIMethodsRowChangeEvent((RepositoryDataSet.BAdIMethodsRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.BAdIMethodsRowDeleted == null)
          return;
        this.BAdIMethodsRowDeleted((object) this, new RepositoryDataSet.BAdIMethodsRowChangeEvent((RepositoryDataSet.BAdIMethodsRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.BAdIMethodsRowDeleting == null)
          return;
        this.BAdIMethodsRowDeleting((object) this, new RepositoryDataSet.BAdIMethodsRowChangeEvent((RepositoryDataSet.BAdIMethodsRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void RemoveBAdIMethodsRow(RepositoryDataSet.BAdIMethodsRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (BAdIMethodsDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class ProjectItemDataTable : TypedTableBase<RepositoryDataSet.ProjectItemRow>
    {
      private DataColumn columnProjectItemKey;
      private DataColumn columnProjectItemType;
      private DataColumn columnDescription;
      private DataColumn columnLMContentType;
      private DataColumn columnKeyUserRequired;
      private DataColumn columnAddItemRelevant;
      private DataColumn columnDelAllowedInMaintenance;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public ProjectItemDataTable()
      {
        this.TableName = "ProjectItem";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal ProjectItemDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected ProjectItemDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn ProjectItemKeyColumn
      {
        get
        {
          return this.columnProjectItemKey;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn ProjectItemTypeColumn
      {
        get
        {
          return this.columnProjectItemType;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn DescriptionColumn
      {
        get
        {
          return this.columnDescription;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn LMContentTypeColumn
      {
        get
        {
          return this.columnLMContentType;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn KeyUserRequiredColumn
      {
        get
        {
          return this.columnKeyUserRequired;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn AddItemRelevantColumn
      {
        get
        {
          return this.columnAddItemRelevant;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn DelAllowedInMaintenanceColumn
      {
        get
        {
          return this.columnDelAllowedInMaintenance;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [Browsable(false)]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.ProjectItemRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.ProjectItemRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ProjectItemRowChangeEventHandler ProjectItemRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ProjectItemRowChangeEventHandler ProjectItemRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ProjectItemRowChangeEventHandler ProjectItemRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.ProjectItemRowChangeEventHandler ProjectItemRowDeleted;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void AddProjectItemRow(RepositoryDataSet.ProjectItemRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ProjectItemRow AddProjectItemRow(string ProjectItemKey, string ProjectItemType, string Description, string LMContentType, string KeyUserRequired, string AddItemRelevant, string DelAllowedInMaintenance)
      {
        RepositoryDataSet.ProjectItemRow projectItemRow = (RepositoryDataSet.ProjectItemRow) this.NewRow();
        object[] objArray = new object[7]
        {
          (object) ProjectItemKey,
          (object) ProjectItemType,
          (object) Description,
          (object) LMContentType,
          (object) KeyUserRequired,
          (object) AddItemRelevant,
          (object) DelAllowedInMaintenance
        };
        projectItemRow.ItemArray = objArray;
        this.Rows.Add((DataRow) projectItemRow);
        return projectItemRow;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ProjectItemRow FindByProjectItemKey(string ProjectItemKey)
      {
        return (RepositoryDataSet.ProjectItemRow) this.Rows.Find(new object[1]
        {
          (object) ProjectItemKey
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.ProjectItemDataTable projectItemDataTable = (RepositoryDataSet.ProjectItemDataTable) base.Clone();
        projectItemDataTable.InitVars();
        return (DataTable) projectItemDataTable;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.ProjectItemDataTable();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal void InitVars()
      {
        this.columnProjectItemKey = this.Columns["ProjectItemKey"];
        this.columnProjectItemType = this.Columns["ProjectItemType"];
        this.columnDescription = this.Columns["Description"];
        this.columnLMContentType = this.Columns["LMContentType"];
        this.columnKeyUserRequired = this.Columns["KeyUserRequired"];
        this.columnAddItemRelevant = this.Columns["AddItemRelevant"];
        this.columnDelAllowedInMaintenance = this.Columns["DelAllowedInMaintenance"];
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      private void InitClass()
      {
        this.columnProjectItemKey = new DataColumn("ProjectItemKey", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProjectItemKey);
        this.columnProjectItemType = new DataColumn("ProjectItemType", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnProjectItemType);
        this.columnDescription = new DataColumn("Description", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDescription);
        this.columnLMContentType = new DataColumn("LMContentType", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLMContentType);
        this.columnKeyUserRequired = new DataColumn("KeyUserRequired", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnKeyUserRequired);
        this.columnAddItemRelevant = new DataColumn("AddItemRelevant", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnAddItemRelevant);
        this.columnDelAllowedInMaintenance = new DataColumn("DelAllowedInMaintenance", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDelAllowedInMaintenance);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[1]
        {
          this.columnProjectItemKey
        }, true));
        this.columnProjectItemKey.AllowDBNull = false;
        this.columnProjectItemKey.Unique = true;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ProjectItemRow NewProjectItemRow()
      {
        return (RepositoryDataSet.ProjectItemRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.ProjectItemRow(builder);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.ProjectItemRow);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.ProjectItemRowChanged == null)
          return;
        this.ProjectItemRowChanged((object) this, new RepositoryDataSet.ProjectItemRowChangeEvent((RepositoryDataSet.ProjectItemRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.ProjectItemRowChanging == null)
          return;
        this.ProjectItemRowChanging((object) this, new RepositoryDataSet.ProjectItemRowChangeEvent((RepositoryDataSet.ProjectItemRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.ProjectItemRowDeleted == null)
          return;
        this.ProjectItemRowDeleted((object) this, new RepositoryDataSet.ProjectItemRowChangeEvent((RepositoryDataSet.ProjectItemRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.ProjectItemRowDeleting == null)
          return;
        this.ProjectItemRowDeleting((object) this, new RepositoryDataSet.ProjectItemRowChangeEvent((RepositoryDataSet.ProjectItemRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void RemoveProjectItemRow(RepositoryDataSet.ProjectItemRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (ProjectItemDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class FeatureDataTable : TypedTableBase<RepositoryDataSet.FeatureRow>
    {
      private DataColumn columnFeatureKey;
      private DataColumn columnFeature;
      private DataColumn columnDescription;
      private DataColumn columnSAPSolutionType;
      private DataColumn columnPDISubtype;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public FeatureDataTable()
      {
        this.TableName = "Feature";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal FeatureDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected FeatureDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn FeatureKeyColumn
      {
        get
        {
          return this.columnFeatureKey;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn FeatureColumn
      {
        get
        {
          return this.columnFeature;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataColumn DescriptionColumn
      {
        get
        {
          return this.columnDescription;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn SAPSolutionTypeColumn
      {
        get
        {
          return this.columnSAPSolutionType;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataColumn PDISubtypeColumn
      {
        get
        {
          return this.columnPDISubtype;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [Browsable(false)]
      [DebuggerNonUserCode]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.FeatureRow this[int index]
      {
        get
        {
          return (RepositoryDataSet.FeatureRow) this.Rows[index];
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.FeatureRowChangeEventHandler FeatureRowChanging;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.FeatureRowChangeEventHandler FeatureRowChanged;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.FeatureRowChangeEventHandler FeatureRowDeleting;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public event RepositoryDataSet.FeatureRowChangeEventHandler FeatureRowDeleted;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void AddFeatureRow(RepositoryDataSet.FeatureRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.FeatureRow AddFeatureRow(string FeatureKey, string Feature, string Description, string SAPSolutionType, string PDISubtype)
      {
        RepositoryDataSet.FeatureRow featureRow = (RepositoryDataSet.FeatureRow) this.NewRow();
        object[] objArray = new object[5]
        {
          (object) FeatureKey,
          (object) Feature,
          (object) Description,
          (object) SAPSolutionType,
          (object) PDISubtype
        };
        featureRow.ItemArray = objArray;
        this.Rows.Add((DataRow) featureRow);
        return featureRow;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.FeatureRow FindByFeatureKey(string FeatureKey)
      {
        return (RepositoryDataSet.FeatureRow) this.Rows.Find(new object[1]
        {
          (object) FeatureKey
        });
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        RepositoryDataSet.FeatureDataTable featureDataTable = (RepositoryDataSet.FeatureDataTable) base.Clone();
        featureDataTable.InitVars();
        return (DataTable) featureDataTable;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new RepositoryDataSet.FeatureDataTable();
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal void InitVars()
      {
        this.columnFeatureKey = this.Columns["FeatureKey"];
        this.columnFeature = this.Columns["Feature"];
        this.columnDescription = this.Columns["Description"];
        this.columnSAPSolutionType = this.Columns["SAPSolutionType"];
        this.columnPDISubtype = this.Columns["PDISubtype"];
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnFeatureKey = new DataColumn("FeatureKey", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnFeatureKey);
        this.columnFeature = new DataColumn("Feature", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnFeature);
        this.columnDescription = new DataColumn("Description", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDescription);
        this.columnSAPSolutionType = new DataColumn("SAPSolutionType", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnSAPSolutionType);
        this.columnPDISubtype = new DataColumn("PDISubtype", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnPDISubtype);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[1]
        {
          this.columnFeatureKey
        }, true));
        this.columnFeatureKey.AllowDBNull = false;
        this.columnFeatureKey.Unique = true;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.FeatureRow NewFeatureRow()
      {
        return (RepositoryDataSet.FeatureRow) this.NewRow();
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new RepositoryDataSet.FeatureRow(builder);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override Type GetRowType()
      {
        return typeof (RepositoryDataSet.FeatureRow);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.FeatureRowChanged == null)
          return;
        this.FeatureRowChanged((object) this, new RepositoryDataSet.FeatureRowChangeEvent((RepositoryDataSet.FeatureRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.FeatureRowChanging == null)
          return;
        this.FeatureRowChanging((object) this, new RepositoryDataSet.FeatureRowChangeEvent((RepositoryDataSet.FeatureRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.FeatureRowDeleted == null)
          return;
        this.FeatureRowDeleted((object) this, new RepositoryDataSet.FeatureRowChangeEvent((RepositoryDataSet.FeatureRow) e.Row, e.Action));
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.FeatureRowDeleting == null)
          return;
        this.FeatureRowDeleting((object) this, new RepositoryDataSet.FeatureRowChangeEvent((RepositoryDataSet.FeatureRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void RemoveFeatureRow(RepositoryDataSet.FeatureRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = repositoryDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (FeatureDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = repositoryDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    public class NamespacesRow : DataRow
    {
      private RepositoryDataSet.NamespacesDataTable tableNamespaces;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal NamespacesRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableNamespaces = (RepositoryDataSet.NamespacesDataTable) this.Table;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string NSName
      {
        get
        {
          return (string) this[this.tableNamespaces.NSNameColumn];
        }
        set
        {
          this[this.tableNamespaces.NSNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BusinessObjectsRow[] GetBusinessObjectsRows()
      {
        if (this.Table.ChildRelations["Namespaces_BusinessObjects"] == null)
          return new RepositoryDataSet.BusinessObjectsRow[0];
        return (RepositoryDataSet.BusinessObjectsRow[]) this.GetChildRows(this.Table.ChildRelations["Namespaces_BusinessObjects"]);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.DataTypesRow[] GetDataTypesRows()
      {
        if (this.Table.ChildRelations["Namespaces_Datatypes"] == null)
          return new RepositoryDataSet.DataTypesRow[0];
        return (RepositoryDataSet.DataTypesRow[]) this.GetChildRows(this.Table.ChildRelations["Namespaces_Datatypes"]);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.MessageTypesRow[] GetMessageTypesRows()
      {
        if (this.Table.ChildRelations["Namespaces_MessageTypes"] == null)
          return new RepositoryDataSet.MessageTypesRow[0];
        return (RepositoryDataSet.MessageTypesRow[]) this.GetChildRows(this.Table.ChildRelations["Namespaces_MessageTypes"]);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.BAdIsRow[] GetBAdIsRows()
      {
        if (this.Table.ChildRelations["Namespaces_BAdIs"] == null)
          return new RepositoryDataSet.BAdIsRow[0];
        return (RepositoryDataSet.BAdIsRow[]) this.GetChildRows(this.Table.ChildRelations["Namespaces_BAdIs"]);
      }
    }

    public class BusinessObjectsRow : DataRow
    {
      private RepositoryDataSet.BusinessObjectsDataTable tableBusinessObjects;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal BusinessObjectsRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableBusinessObjects = (RepositoryDataSet.BusinessObjectsDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string NSName
      {
        get
        {
          return (string) this[this.tableBusinessObjects.NSNameColumn];
        }
        set
        {
          this[this.tableBusinessObjects.NSNameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Name
      {
        get
        {
          return (string) this[this.tableBusinessObjects.NameColumn];
        }
        set
        {
          this[this.tableBusinessObjects.NameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string ProxyName
      {
        get
        {
          try
          {
            return (string) this[this.tableBusinessObjects.ProxyNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'ProxyName' in table 'BusinessObjects' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBusinessObjects.ProxyNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string BO_UUID
      {
        get
        {
          try
          {
            return (string) this[this.tableBusinessObjects.BO_UUIDColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'BO_UUID' in table 'BusinessObjects' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBusinessObjects.BO_UUIDColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DateTime LastChangeAt
      {
        get
        {
          try
          {
            return (DateTime) this[this.tableBusinessObjects.LastChangeAtColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'LastChangeAt' in table 'BusinessObjects' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBusinessObjects.LastChangeAtColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string ObjectCategory
      {
        get
        {
          try
          {
            return (string) this[this.tableBusinessObjects.ObjectCategoryColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'ObjectCategory' in table 'BusinessObjects' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBusinessObjects.ObjectCategoryColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string TechCategory
      {
        get
        {
          try
          {
            return (string) this[this.tableBusinessObjects.TechCategoryColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'TechCategory' in table 'BusinessObjects' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBusinessObjects.TechCategoryColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string LifecycleStat
      {
        get
        {
          try
          {
            return (string) this[this.tableBusinessObjects.LifecycleStatColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'LifecycleStat' in table 'BusinessObjects' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBusinessObjects.LifecycleStatColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string DeploymentUnit
      {
        get
        {
          if (this.IsDeploymentUnitNull())
            return (string) null;
          return (string) this[this.tableBusinessObjects.DeploymentUnitColumn];
        }
        set
        {
          this[this.tableBusinessObjects.DeploymentUnitColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string WriteAccess
      {
        get
        {
          if (this.IsWriteAccessNull())
            return (string) null;
          return (string) this[this.tableBusinessObjects.WriteAccessColumn];
        }
        set
        {
          this[this.tableBusinessObjects.WriteAccessColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string TransitiveHash
      {
        get
        {
          if (this.IsTransitiveHashNull())
            return (string) null;
          return (string) this[this.tableBusinessObjects.TransitiveHashColumn];
        }
        set
        {
          this[this.tableBusinessObjects.TransitiveHashColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool Deprecated
      {
        get
        {
          return (bool) this[this.tableBusinessObjects.DeprecatedColumn];
        }
        set
        {
          this[this.tableBusinessObjects.DeprecatedColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.NamespacesRow NamespacesRow
      {
        get
        {
          return (RepositoryDataSet.NamespacesRow) this.GetParentRow(this.Table.ParentRelations["Namespaces_BusinessObjects"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["Namespaces_BusinessObjects"]);
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsProxyNameNull()
      {
        return this.IsNull(this.tableBusinessObjects.ProxyNameColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetProxyNameNull()
      {
        this[this.tableBusinessObjects.ProxyNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsBO_UUIDNull()
      {
        return this.IsNull(this.tableBusinessObjects.BO_UUIDColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetBO_UUIDNull()
      {
        this[this.tableBusinessObjects.BO_UUIDColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsLastChangeAtNull()
      {
        return this.IsNull(this.tableBusinessObjects.LastChangeAtColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetLastChangeAtNull()
      {
        this[this.tableBusinessObjects.LastChangeAtColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsObjectCategoryNull()
      {
        return this.IsNull(this.tableBusinessObjects.ObjectCategoryColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetObjectCategoryNull()
      {
        this[this.tableBusinessObjects.ObjectCategoryColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsTechCategoryNull()
      {
        return this.IsNull(this.tableBusinessObjects.TechCategoryColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetTechCategoryNull()
      {
        this[this.tableBusinessObjects.TechCategoryColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsLifecycleStatNull()
      {
        return this.IsNull(this.tableBusinessObjects.LifecycleStatColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetLifecycleStatNull()
      {
        this[this.tableBusinessObjects.LifecycleStatColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsDeploymentUnitNull()
      {
        return this.IsNull(this.tableBusinessObjects.DeploymentUnitColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetDeploymentUnitNull()
      {
        this[this.tableBusinessObjects.DeploymentUnitColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsWriteAccessNull()
      {
        return this.IsNull(this.tableBusinessObjects.WriteAccessColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetWriteAccessNull()
      {
        this[this.tableBusinessObjects.WriteAccessColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsTransitiveHashNull()
      {
        return this.IsNull(this.tableBusinessObjects.TransitiveHashColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetTransitiveHashNull()
      {
        this[this.tableBusinessObjects.TransitiveHashColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.NodesRow[] GetNodesRows()
      {
        if (this.Table.ChildRelations["BusinessObjects_Nodes"] == null)
          return new RepositoryDataSet.NodesRow[0];
        return (RepositoryDataSet.NodesRow[]) this.GetChildRows(this.Table.ChildRelations["BusinessObjects_Nodes"]);
      }
    }

    public class DataTypesRow : DataRow
    {
      private RepositoryDataSet.DataTypesDataTable tableDataTypes;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal DataTypesRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableDataTypes = (RepositoryDataSet.DataTypesDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string NSName
      {
        get
        {
          return (string) this[this.tableDataTypes.NSNameColumn];
        }
        set
        {
          this[this.tableDataTypes.NSNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Name
      {
        get
        {
          return (string) this[this.tableDataTypes.NameColumn];
        }
        set
        {
          this[this.tableDataTypes.NameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string ProxyName
      {
        get
        {
          try
          {
            return (string) this[this.tableDataTypes.ProxyNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'ProxyName' in table 'DataTypes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableDataTypes.ProxyNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string DT_UUID
      {
        get
        {
          try
          {
            return (string) this[this.tableDataTypes.DT_UUIDColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'DT_UUID' in table 'DataTypes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableDataTypes.DT_UUIDColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DateTime LastChangeAt
      {
        get
        {
          try
          {
            return (DateTime) this[this.tableDataTypes.LastChangeAtColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'LastChangeAt' in table 'DataTypes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableDataTypes.LastChangeAtColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string BaseDTKeyName
      {
        get
        {
          if (this.IsBaseDTKeyNameNull())
            return (string) null;
          return (string) this[this.tableDataTypes.BaseDTKeyNameColumn];
        }
        set
        {
          this[this.tableDataTypes.BaseDTKeyNameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string UsageCategory
      {
        get
        {
          if (this.IsUsageCategoryNull())
            return (string) null;
          return (string) this[this.tableDataTypes.UsageCategoryColumn];
        }
        set
        {
          this[this.tableDataTypes.UsageCategoryColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool Extensible
      {
        get
        {
          try
          {
            return (bool) this[this.tableDataTypes.ExtensibleColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Extensible' in table 'DataTypes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableDataTypes.ExtensibleColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string TransitiveHash
      {
        get
        {
          if (this.IsTransitiveHashNull())
            return (string) null;
          return (string) this[this.tableDataTypes.TransitiveHashColumn];
        }
        set
        {
          this[this.tableDataTypes.TransitiveHashColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string RepresentationTerm
      {
        get
        {
          if (this.IsRepresentationTermNull())
            return (string) null;
          return (string) this[this.tableDataTypes.RepresentationTermColumn];
        }
        set
        {
          this[this.tableDataTypes.RepresentationTermColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.NamespacesRow NamespacesRow
      {
        get
        {
          return (RepositoryDataSet.NamespacesRow) this.GetParentRow(this.Table.ParentRelations["Namespaces_Datatypes"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["Namespaces_Datatypes"]);
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsProxyNameNull()
      {
        return this.IsNull(this.tableDataTypes.ProxyNameColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetProxyNameNull()
      {
        this[this.tableDataTypes.ProxyNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsDT_UUIDNull()
      {
        return this.IsNull(this.tableDataTypes.DT_UUIDColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetDT_UUIDNull()
      {
        this[this.tableDataTypes.DT_UUIDColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsLastChangeAtNull()
      {
        return this.IsNull(this.tableDataTypes.LastChangeAtColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetLastChangeAtNull()
      {
        this[this.tableDataTypes.LastChangeAtColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsBaseDTKeyNameNull()
      {
        return this.IsNull(this.tableDataTypes.BaseDTKeyNameColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetBaseDTKeyNameNull()
      {
        this[this.tableDataTypes.BaseDTKeyNameColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsUsageCategoryNull()
      {
        return this.IsNull(this.tableDataTypes.UsageCategoryColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetUsageCategoryNull()
      {
        this[this.tableDataTypes.UsageCategoryColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsExtensibleNull()
      {
        return this.IsNull(this.tableDataTypes.ExtensibleColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetExtensibleNull()
      {
        this[this.tableDataTypes.ExtensibleColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsTransitiveHashNull()
      {
        return this.IsNull(this.tableDataTypes.TransitiveHashColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetTransitiveHashNull()
      {
        this[this.tableDataTypes.TransitiveHashColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsRepresentationTermNull()
      {
        return this.IsNull(this.tableDataTypes.RepresentationTermColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetRepresentationTermNull()
      {
        this[this.tableDataTypes.RepresentationTermColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.MessageTypesRow[] GetMessageTypesRows()
      {
        if (this.Table.ChildRelations["DataTypes_MessageTypes"] == null)
          return new RepositoryDataSet.MessageTypesRow[0];
        return (RepositoryDataSet.MessageTypesRow[]) this.GetChildRows(this.Table.ChildRelations["DataTypes_MessageTypes"]);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.MsgDataTypeNodesRow[] GetMsgDataTypeNodesRows()
      {
        if (this.Table.ChildRelations["DataTypes_DataTypeNodes"] == null)
          return new RepositoryDataSet.MsgDataTypeNodesRow[0];
        return (RepositoryDataSet.MsgDataTypeNodesRow[]) this.GetChildRows(this.Table.ChildRelations["DataTypes_DataTypeNodes"]);
      }
    }

    public class NodesRow : DataRow
    {
      private RepositoryDataSet.NodesDataTable tableNodes;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal NodesRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableNodes = (RepositoryDataSet.NodesDataTable) this.Table;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string BOProxyName
      {
        get
        {
          return (string) this[this.tableNodes.BOProxyNameColumn];
        }
        set
        {
          this[this.tableNodes.BOProxyNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Name
      {
        get
        {
          return (string) this[this.tableNodes.NameColumn];
        }
        set
        {
          this[this.tableNodes.NameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string ProxyName
      {
        get
        {
          try
          {
            return (string) this[this.tableNodes.ProxyNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'ProxyName' in table 'Nodes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableNodes.ProxyNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string ParentNodeName
      {
        get
        {
          if (this.IsParentNodeNameNull())
            return (string) null;
          return (string) this[this.tableNodes.ParentNodeNameColumn];
        }
        set
        {
          this[this.tableNodes.ParentNodeNameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string WriteAccess
      {
        get
        {
          if (this.IsWriteAccessNull())
            return (string) null;
          return (string) this[this.tableNodes.WriteAccessColumn];
        }
        set
        {
          this[this.tableNodes.WriteAccessColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Category
      {
        get
        {
          if (this.IsCategoryNull())
            return (string) null;
          return (string) this[this.tableNodes.CategoryColumn];
        }
        set
        {
          this[this.tableNodes.CategoryColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BusinessObjectsRow BusinessObjectsRow
      {
        get
        {
          return (RepositoryDataSet.BusinessObjectsRow) this.GetParentRow(this.Table.ParentRelations["BusinessObjects_Nodes"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["BusinessObjects_Nodes"]);
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsProxyNameNull()
      {
        return this.IsNull(this.tableNodes.ProxyNameColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetProxyNameNull()
      {
        this[this.tableNodes.ProxyNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsParentNodeNameNull()
      {
        return this.IsNull(this.tableNodes.ParentNodeNameColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetParentNodeNameNull()
      {
        this[this.tableNodes.ParentNodeNameColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsWriteAccessNull()
      {
        return this.IsNull(this.tableNodes.WriteAccessColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetWriteAccessNull()
      {
        this[this.tableNodes.WriteAccessColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsCategoryNull()
      {
        return this.IsNull(this.tableNodes.CategoryColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetCategoryNull()
      {
        this[this.tableNodes.CategoryColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ActionsRow[] GetActionsRows()
      {
        if (this.Table.ChildRelations["Nodes_Actions"] == null)
          return new RepositoryDataSet.ActionsRow[0];
        return (RepositoryDataSet.ActionsRow[]) this.GetChildRows(this.Table.ChildRelations["Nodes_Actions"]);
      }
    }

    public class ActionsRow : DataRow
    {
      private RepositoryDataSet.ActionsDataTable tableActions;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal ActionsRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableActions = (RepositoryDataSet.ActionsDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string BOProxyName
      {
        get
        {
          return (string) this[this.tableActions.BOProxyNameColumn];
        }
        set
        {
          this[this.tableActions.BOProxyNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string NodeProxyName
      {
        get
        {
          return (string) this[this.tableActions.NodeProxyNameColumn];
        }
        set
        {
          this[this.tableActions.NodeProxyNameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Name
      {
        get
        {
          return (string) this[this.tableActions.NameColumn];
        }
        set
        {
          this[this.tableActions.NameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string ProxyName
      {
        get
        {
          try
          {
            return (string) this[this.tableActions.ProxyNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'ProxyName' in table 'Actions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableActions.ProxyNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.NodesRow NodesRowParent
      {
        get
        {
          return (RepositoryDataSet.NodesRow) this.GetParentRow(this.Table.ParentRelations["Nodes_Actions"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["Nodes_Actions"]);
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsProxyNameNull()
      {
        return this.IsNull(this.tableActions.ProxyNameColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetProxyNameNull()
      {
        this[this.tableActions.ProxyNameColumn] = Convert.DBNull;
      }
    }

    public class SolutionsRow : DataRow
    {
      private RepositoryDataSet.SolutionsDataTable tableSolutions;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal SolutionsRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableSolutions = (RepositoryDataSet.SolutionsDataTable) this.Table;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Name
      {
        get
        {
          return (string) this[this.tableSolutions.NameColumn];
        }
        set
        {
          this[this.tableSolutions.NameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Version
      {
        get
        {
          return (string) this[this.tableSolutions.VersionColumn];
        }
        set
        {
          this[this.tableSolutions.VersionColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Status
      {
        get
        {
          try
          {
            return (string) this[this.tableSolutions.StatusColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Status' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.StatusColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Description
      {
        get
        {
          try
          {
            return (string) this[this.tableSolutions.DescriptionColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Description' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.DescriptionColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Type
      {
        get
        {
          try
          {
            return (string) this[this.tableSolutions.TypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Type' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.TypeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool Deleted
      {
        get
        {
          try
          {
            return (bool) this[this.tableSolutions.DeletedColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Deleted' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.DeletedColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string CertificationStatus
      {
        get
        {
          try
          {
            return (string) this[this.tableSolutions.CertificationStatusColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'CertificationStatus' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.CertificationStatusColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string OveralStatus
      {
        get
        {
          try
          {
            return (string) this[this.tableSolutions.OveralStatusColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'OveralStatus' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.OveralStatusColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string PatchSolution
      {
        get
        {
          if (this.IsPatchSolutionNull())
            return (string) null;
          return (string) this[this.tableSolutions.PatchSolutionColumn];
        }
        set
        {
          this[this.tableSolutions.PatchSolutionColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string PDISubType
      {
        get
        {
          if (this.IsPDISubTypeNull())
            return (string) null;
          return (string) this[this.tableSolutions.PDISubTypeColumn];
        }
        set
        {
          this[this.tableSolutions.PDISubTypeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string DevPartner
      {
        get
        {
          try
          {
            return (string) this[this.tableSolutions.DevPartnerColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'DevPartner' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.DevPartnerColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string ContactPerson
      {
        get
        {
          try
          {
            return (string) this[this.tableSolutions.ContactPersonColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'ContactPerson' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.ContactPersonColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string EMail
      {
        get
        {
          try
          {
            return (string) this[this.tableSolutions.EMailColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'EMail' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.EMailColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string DeploymentUnit
      {
        get
        {
          try
          {
            return (string) this[this.tableSolutions.DeploymentUnitColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'DeploymentUnit' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.DeploymentUnitColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string GlobalName
      {
        get
        {
          try
          {
            return (string) this[this.tableSolutions.GlobalNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'GlobalName' in table 'Solutions' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableSolutions.GlobalNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsStatusNull()
      {
        return this.IsNull(this.tableSolutions.StatusColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetStatusNull()
      {
        this[this.tableSolutions.StatusColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsDescriptionNull()
      {
        return this.IsNull(this.tableSolutions.DescriptionColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetDescriptionNull()
      {
        this[this.tableSolutions.DescriptionColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsTypeNull()
      {
        return this.IsNull(this.tableSolutions.TypeColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetTypeNull()
      {
        this[this.tableSolutions.TypeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsDeletedNull()
      {
        return this.IsNull(this.tableSolutions.DeletedColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetDeletedNull()
      {
        this[this.tableSolutions.DeletedColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsCertificationStatusNull()
      {
        return this.IsNull(this.tableSolutions.CertificationStatusColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetCertificationStatusNull()
      {
        this[this.tableSolutions.CertificationStatusColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsOveralStatusNull()
      {
        return this.IsNull(this.tableSolutions.OveralStatusColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetOveralStatusNull()
      {
        this[this.tableSolutions.OveralStatusColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsPatchSolutionNull()
      {
        return this.IsNull(this.tableSolutions.PatchSolutionColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetPatchSolutionNull()
      {
        this[this.tableSolutions.PatchSolutionColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsPDISubTypeNull()
      {
        return this.IsNull(this.tableSolutions.PDISubTypeColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetPDISubTypeNull()
      {
        this[this.tableSolutions.PDISubTypeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsDevPartnerNull()
      {
        return this.IsNull(this.tableSolutions.DevPartnerColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetDevPartnerNull()
      {
        this[this.tableSolutions.DevPartnerColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsContactPersonNull()
      {
        return this.IsNull(this.tableSolutions.ContactPersonColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetContactPersonNull()
      {
        this[this.tableSolutions.ContactPersonColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsEMailNull()
      {
        return this.IsNull(this.tableSolutions.EMailColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetEMailNull()
      {
        this[this.tableSolutions.EMailColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsDeploymentUnitNull()
      {
        return this.IsNull(this.tableSolutions.DeploymentUnitColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetDeploymentUnitNull()
      {
        this[this.tableSolutions.DeploymentUnitColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsGlobalNameNull()
      {
        return this.IsNull(this.tableSolutions.GlobalNameColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetGlobalNameNull()
      {
        this[this.tableSolutions.GlobalNameColumn] = Convert.DBNull;
      }
    }

    public class LibrariesRow : DataRow
    {
      private RepositoryDataSet.LibrariesDataTable tableLibraries;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal LibrariesRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableLibraries = (RepositoryDataSet.LibrariesDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string ProxyName
      {
        get
        {
          return (string) this[this.tableLibraries.ProxyNameColumn];
        }
        set
        {
          this[this.tableLibraries.ProxyNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Name
      {
        get
        {
          return (string) this[this.tableLibraries.NameColumn];
        }
        set
        {
          this[this.tableLibraries.NameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Namespace
      {
        get
        {
          return (string) this[this.tableLibraries.NamespaceColumn];
        }
        set
        {
          this[this.tableLibraries.NamespaceColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DateTime LastChangeAt
      {
        get
        {
          try
          {
            return (DateTime) this[this.tableLibraries.LastChangeAtColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'LastChangeAt' in table 'Libraries' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableLibraries.LastChangeAtColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string TypeCode
      {
        get
        {
          try
          {
            return (string) this[this.tableLibraries.TypeCodeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'TypeCode' in table 'Libraries' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableLibraries.TypeCodeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string CallType
      {
        get
        {
          try
          {
            return (string) this[this.tableLibraries.CallTypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'CallType' in table 'Libraries' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableLibraries.CallTypeColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string TransitiveHash
      {
        get
        {
          if (this.IsTransitiveHashNull())
            return (string) null;
          return (string) this[this.tableLibraries.TransitiveHashColumn];
        }
        set
        {
          this[this.tableLibraries.TransitiveHashColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsLastChangeAtNull()
      {
        return this.IsNull(this.tableLibraries.LastChangeAtColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetLastChangeAtNull()
      {
        this[this.tableLibraries.LastChangeAtColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsTypeCodeNull()
      {
        return this.IsNull(this.tableLibraries.TypeCodeColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetTypeCodeNull()
      {
        this[this.tableLibraries.TypeCodeColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsCallTypeNull()
      {
        return this.IsNull(this.tableLibraries.CallTypeColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetCallTypeNull()
      {
        this[this.tableLibraries.CallTypeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsTransitiveHashNull()
      {
        return this.IsNull(this.tableLibraries.TransitiveHashColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetTransitiveHashNull()
      {
        this[this.tableLibraries.TransitiveHashColumn] = Convert.DBNull;
      }
    }

    public class MessageTypesRow : DataRow
    {
      private RepositoryDataSet.MessageTypesDataTable tableMessageTypes;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal MessageTypesRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableMessageTypes = (RepositoryDataSet.MessageTypesDataTable) this.Table;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Namespace
      {
        get
        {
          return (string) this[this.tableMessageTypes.NamespaceColumn];
        }
        set
        {
          this[this.tableMessageTypes.NamespaceColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Name
      {
        get
        {
          return (string) this[this.tableMessageTypes.NameColumn];
        }
        set
        {
          this[this.tableMessageTypes.NameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string ProxyName
      {
        get
        {
          try
          {
            return (string) this[this.tableMessageTypes.ProxyNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'ProxyName' in table 'MessageTypes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableMessageTypes.ProxyNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string DataTypeName
      {
        get
        {
          if (this.IsDataTypeNameNull())
            return string.Empty;
          return (string) this[this.tableMessageTypes.DataTypeNameColumn];
        }
        set
        {
          this[this.tableMessageTypes.DataTypeNameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string LeadingBOName
      {
        get
        {
          if (this.IsLeadingBONameNull())
            return string.Empty;
          return (string) this[this.tableMessageTypes.LeadingBONameColumn];
        }
        set
        {
          this[this.tableMessageTypes.LeadingBONameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string DataTypeFieldName
      {
        get
        {
          if (this.IsDataTypeFieldNameNull())
            return string.Empty;
          return (string) this[this.tableMessageTypes.DataTypeFieldNameColumn];
        }
        set
        {
          this[this.tableMessageTypes.DataTypeFieldNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DateTime LastChangedAt
      {
        get
        {
          try
          {
            return (DateTime) this[this.tableMessageTypes.LastChangedAtColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'LastChangedAt' in table 'MessageTypes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableMessageTypes.LastChangedAtColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string TrasitiveHash
      {
        get
        {
          if (this.IsTrasitiveHashNull())
            return (string) null;
          return (string) this[this.tableMessageTypes.TrasitiveHashColumn];
        }
        set
        {
          this[this.tableMessageTypes.TrasitiveHashColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.NamespacesRow NamespacesRow
      {
        get
        {
          return (RepositoryDataSet.NamespacesRow) this.GetParentRow(this.Table.ParentRelations["Namespaces_MessageTypes"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["Namespaces_MessageTypes"]);
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.DataTypesRow DataTypesRow
      {
        get
        {
          return (RepositoryDataSet.DataTypesRow) this.GetParentRow(this.Table.ParentRelations["DataTypes_MessageTypes"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["DataTypes_MessageTypes"]);
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsProxyNameNull()
      {
        return this.IsNull(this.tableMessageTypes.ProxyNameColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetProxyNameNull()
      {
        this[this.tableMessageTypes.ProxyNameColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsDataTypeNameNull()
      {
        return this.IsNull(this.tableMessageTypes.DataTypeNameColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetDataTypeNameNull()
      {
        this[this.tableMessageTypes.DataTypeNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsLeadingBONameNull()
      {
        return this.IsNull(this.tableMessageTypes.LeadingBONameColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetLeadingBONameNull()
      {
        this[this.tableMessageTypes.LeadingBONameColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsDataTypeFieldNameNull()
      {
        return this.IsNull(this.tableMessageTypes.DataTypeFieldNameColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetDataTypeFieldNameNull()
      {
        this[this.tableMessageTypes.DataTypeFieldNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsLastChangedAtNull()
      {
        return this.IsNull(this.tableMessageTypes.LastChangedAtColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetLastChangedAtNull()
      {
        this[this.tableMessageTypes.LastChangedAtColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsTrasitiveHashNull()
      {
        return this.IsNull(this.tableMessageTypes.TrasitiveHashColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetTrasitiveHashNull()
      {
        this[this.tableMessageTypes.TrasitiveHashColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.MsgDataTypeNodesRow[] GetMsgDataTypeNodesRows()
      {
        if (this.Table.ChildRelations["Messagetypes_DataTypeNodes"] == null)
          return new RepositoryDataSet.MsgDataTypeNodesRow[0];
        return (RepositoryDataSet.MsgDataTypeNodesRow[]) this.GetChildRows(this.Table.ChildRelations["Messagetypes_DataTypeNodes"]);
      }
    }

    public class MsgDataTypeNodesRow : DataRow
    {
      private RepositoryDataSet.MsgDataTypeNodesDataTable tableMsgDataTypeNodes;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal MsgDataTypeNodesRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableMsgDataTypeNodes = (RepositoryDataSet.MsgDataTypeNodesDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string MsgTypeName
      {
        get
        {
          return (string) this[this.tableMsgDataTypeNodes.MsgTypeNameColumn];
        }
        set
        {
          this[this.tableMsgDataTypeNodes.MsgTypeNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Name
      {
        get
        {
          return (string) this[this.tableMsgDataTypeNodes.NameColumn];
        }
        set
        {
          this[this.tableMsgDataTypeNodes.NameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string ParentName
      {
        get
        {
          try
          {
            return (string) this[this.tableMsgDataTypeNodes.ParentNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'ParentName' in table 'MsgDataTypeNodes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableMsgDataTypeNodes.ParentNameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Path
      {
        get
        {
          try
          {
            return (string) this[this.tableMsgDataTypeNodes.PathColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Path' in table 'MsgDataTypeNodes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableMsgDataTypeNodes.PathColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string DataTypeName
      {
        get
        {
          try
          {
            return (string) this[this.tableMsgDataTypeNodes.DataTypeNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'DataTypeName' in table 'MsgDataTypeNodes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableMsgDataTypeNodes.DataTypeNameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool Extensible
      {
        get
        {
          try
          {
            return (bool) this[this.tableMsgDataTypeNodes.ExtensibleColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Extensible' in table 'MsgDataTypeNodes' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableMsgDataTypeNodes.ExtensibleColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.MessageTypesRow MessageTypesRow
      {
        get
        {
          return (RepositoryDataSet.MessageTypesRow) this.GetParentRow(this.Table.ParentRelations["Messagetypes_DataTypeNodes"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["Messagetypes_DataTypeNodes"]);
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.DataTypesRow DataTypesRow
      {
        get
        {
          return (RepositoryDataSet.DataTypesRow) this.GetParentRow(this.Table.ParentRelations["DataTypes_DataTypeNodes"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["DataTypes_DataTypeNodes"]);
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsParentNameNull()
      {
        return this.IsNull(this.tableMsgDataTypeNodes.ParentNameColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetParentNameNull()
      {
        this[this.tableMsgDataTypeNodes.ParentNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsPathNull()
      {
        return this.IsNull(this.tableMsgDataTypeNodes.PathColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetPathNull()
      {
        this[this.tableMsgDataTypeNodes.PathColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsDataTypeNameNull()
      {
        return this.IsNull(this.tableMsgDataTypeNodes.DataTypeNameColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetDataTypeNameNull()
      {
        this[this.tableMsgDataTypeNodes.DataTypeNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsExtensibleNull()
      {
        return this.IsNull(this.tableMsgDataTypeNodes.ExtensibleColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetExtensibleNull()
      {
        this[this.tableMsgDataTypeNodes.ExtensibleColumn] = Convert.DBNull;
      }
    }

    public class BAdIsRow : DataRow
    {
      private RepositoryDataSet.BAdIsDataTable tableBAdIs;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal BAdIsRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableBAdIs = (RepositoryDataSet.BAdIsDataTable) this.Table;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string NSName
      {
        get
        {
          return (string) this[this.tableBAdIs.NSNameColumn];
        }
        set
        {
          this[this.tableBAdIs.NSNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Name
      {
        get
        {
          return (string) this[this.tableBAdIs.NameColumn];
        }
        set
        {
          this[this.tableBAdIs.NameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string ProxyName
      {
        get
        {
          return (string) this[this.tableBAdIs.ProxyNameColumn];
        }
        set
        {
          this[this.tableBAdIs.ProxyNameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Description
      {
        get
        {
          try
          {
            return (string) this[this.tableBAdIs.DescriptionColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Description' in table 'BAdIs' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBAdIs.DescriptionColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string BusinessObject
      {
        get
        {
          try
          {
            return (string) this[this.tableBAdIs.BusinessObjectColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'BusinessObject' in table 'BAdIs' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBAdIs.BusinessObjectColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string SpotName
      {
        get
        {
          try
          {
            return (string) this[this.tableBAdIs.SpotNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'SpotName' in table 'BAdIs' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBAdIs.SpotNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string DeploymentUnit
      {
        get
        {
          try
          {
            return (string) this[this.tableBAdIs.DeploymentUnitColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'DeploymentUnit' in table 'BAdIs' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBAdIs.DeploymentUnitColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsSingleUsed
      {
        get
        {
          try
          {
            return (bool) this[this.tableBAdIs.IsSingleUsedColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'IsSingleUsed' in table 'BAdIs' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBAdIs.IsSingleUsedColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.NamespacesRow NamespacesRow
      {
        get
        {
          return (RepositoryDataSet.NamespacesRow) this.GetParentRow(this.Table.ParentRelations["Namespaces_BAdIs"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["Namespaces_BAdIs"]);
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsDescriptionNull()
      {
        return this.IsNull(this.tableBAdIs.DescriptionColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetDescriptionNull()
      {
        this[this.tableBAdIs.DescriptionColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsBusinessObjectNull()
      {
        return this.IsNull(this.tableBAdIs.BusinessObjectColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetBusinessObjectNull()
      {
        this[this.tableBAdIs.BusinessObjectColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsSpotNameNull()
      {
        return this.IsNull(this.tableBAdIs.SpotNameColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetSpotNameNull()
      {
        this[this.tableBAdIs.SpotNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsDeploymentUnitNull()
      {
        return this.IsNull(this.tableBAdIs.DeploymentUnitColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetDeploymentUnitNull()
      {
        this[this.tableBAdIs.DeploymentUnitColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsIsSingleUsedNull()
      {
        return this.IsNull(this.tableBAdIs.IsSingleUsedColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetIsSingleUsedNull()
      {
        this[this.tableBAdIs.IsSingleUsedColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIFiltersRow[] GetBAdIFiltersRows()
      {
        if (this.Table.ChildRelations["BAdIs_BAdIFilters"] == null)
          return new RepositoryDataSet.BAdIFiltersRow[0];
        return (RepositoryDataSet.BAdIFiltersRow[]) this.GetChildRows(this.Table.ChildRelations["BAdIs_BAdIFilters"]);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.BAdIMethodsRow[] GetBAdIMethodsRows()
      {
        if (this.Table.ChildRelations["BAdIs_BAdIMethod"] == null)
          return new RepositoryDataSet.BAdIMethodsRow[0];
        return (RepositoryDataSet.BAdIMethodsRow[]) this.GetChildRows(this.Table.ChildRelations["BAdIs_BAdIMethod"]);
      }
    }

    public class BAdIFiltersRow : DataRow
    {
      private RepositoryDataSet.BAdIFiltersDataTable tableBAdIFilters;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal BAdIFiltersRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableBAdIFilters = (RepositoryDataSet.BAdIFiltersDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Name
      {
        get
        {
          return (string) this[this.tableBAdIFilters.NameColumn];
        }
        set
        {
          this[this.tableBAdIFilters.NameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Type
      {
        get
        {
          try
          {
            return (string) this[this.tableBAdIFilters.TypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Type' in table 'BAdIFilters' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBAdIFilters.TypeColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Description
      {
        get
        {
          try
          {
            return (string) this[this.tableBAdIFilters.DescriptionColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Description' in table 'BAdIFilters' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBAdIFilters.DescriptionColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string BAdIProxyName
      {
        get
        {
          return (string) this[this.tableBAdIFilters.BAdIProxyNameColumn];
        }
        set
        {
          this[this.tableBAdIFilters.BAdIProxyNameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool Mandatory
      {
        get
        {
          try
          {
            return (bool) this[this.tableBAdIFilters.MandatoryColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Mandatory' in table 'BAdIFilters' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableBAdIFilters.MandatoryColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIsRow BAdIsRow
      {
        get
        {
          return (RepositoryDataSet.BAdIsRow) this.GetParentRow(this.Table.ParentRelations["BAdIs_BAdIFilters"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["BAdIs_BAdIFilters"]);
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsTypeNull()
      {
        return this.IsNull(this.tableBAdIFilters.TypeColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetTypeNull()
      {
        this[this.tableBAdIFilters.TypeColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsDescriptionNull()
      {
        return this.IsNull(this.tableBAdIFilters.DescriptionColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetDescriptionNull()
      {
        this[this.tableBAdIFilters.DescriptionColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsMandatoryNull()
      {
        return this.IsNull(this.tableBAdIFilters.MandatoryColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetMandatoryNull()
      {
        this[this.tableBAdIFilters.MandatoryColumn] = Convert.DBNull;
      }
    }

    public class BAdIMethodsRow : DataRow
    {
      private RepositoryDataSet.BAdIMethodsDataTable tableBAdIMethods;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal BAdIMethodsRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableBAdIMethods = (RepositoryDataSet.BAdIMethodsDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Name
      {
        get
        {
          return (string) this[this.tableBAdIMethods.NameColumn];
        }
        set
        {
          this[this.tableBAdIMethods.NameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string BAdIProxyName
      {
        get
        {
          return (string) this[this.tableBAdIMethods.BAdIProxyNameColumn];
        }
        set
        {
          this[this.tableBAdIMethods.BAdIProxyNameColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.BAdIsRow BAdIsRow
      {
        get
        {
          return (RepositoryDataSet.BAdIsRow) this.GetParentRow(this.Table.ParentRelations["BAdIs_BAdIMethod"]);
        }
        set
        {
          this.SetParentRow((DataRow) value, this.Table.ParentRelations["BAdIs_BAdIMethod"]);
        }
      }
    }

    public class ProjectItemSolutionRow : DataRow
    {
      private RepositoryDataSet.ProjectItemSolutionDataTable tableProjectItemSolution;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal ProjectItemSolutionRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableProjectItemSolution = (RepositoryDataSet.ProjectItemSolutionDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string ProjectItemSolutionkey
      {
        get
        {
          return (string) this[this.tableProjectItemSolution.ProjectItemSolutionkeyColumn];
        }
        set
        {
          this[this.tableProjectItemSolution.ProjectItemSolutionkeyColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string ProjectItemType
      {
        get
        {
          try
          {
            return (string) this[this.tableProjectItemSolution.ProjectItemTypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'ProjectItemType' in table 'ProjectItemSolution' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableProjectItemSolution.ProjectItemTypeColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string SAPSolutionType
      {
        get
        {
          try
          {
            return (string) this[this.tableProjectItemSolution.SAPSolutionTypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'SAPSolutionType' in table 'ProjectItemSolution' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableProjectItemSolution.SAPSolutionTypeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string PDISubtype
      {
        get
        {
          try
          {
            return (string) this[this.tableProjectItemSolution.PDISubtypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'PDISubtype' in table 'ProjectItemSolution' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableProjectItemSolution.PDISubtypeColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsProjectItemTypeNull()
      {
        return this.IsNull(this.tableProjectItemSolution.ProjectItemTypeColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetProjectItemTypeNull()
      {
        this[this.tableProjectItemSolution.ProjectItemTypeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsSAPSolutionTypeNull()
      {
        return this.IsNull(this.tableProjectItemSolution.SAPSolutionTypeColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetSAPSolutionTypeNull()
      {
        this[this.tableProjectItemSolution.SAPSolutionTypeColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsPDISubtypeNull()
      {
        return this.IsNull(this.tableProjectItemSolution.PDISubtypeColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetPDISubtypeNull()
      {
        this[this.tableProjectItemSolution.PDISubtypeColumn] = Convert.DBNull;
      }
    }

    public class ProjectItemRow : DataRow
    {
      private RepositoryDataSet.ProjectItemDataTable tableProjectItem;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      internal ProjectItemRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableProjectItem = (RepositoryDataSet.ProjectItemDataTable) this.Table;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string ProjectItemKey
      {
        get
        {
          return (string) this[this.tableProjectItem.ProjectItemKeyColumn];
        }
        set
        {
          this[this.tableProjectItem.ProjectItemKeyColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string ProjectItemType
      {
        get
        {
          try
          {
            return (string) this[this.tableProjectItem.ProjectItemTypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'ProjectItemType' in table 'ProjectItem' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableProjectItem.ProjectItemTypeColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string Description
      {
        get
        {
          try
          {
            return (string) this[this.tableProjectItem.DescriptionColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Description' in table 'ProjectItem' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableProjectItem.DescriptionColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string LMContentType
      {
        get
        {
          try
          {
            return (string) this[this.tableProjectItem.LMContentTypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'LMContentType' in table 'ProjectItem' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableProjectItem.LMContentTypeColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string KeyUserRequired
      {
        get
        {
          try
          {
            return (string) this[this.tableProjectItem.KeyUserRequiredColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'KeyUserRequired' in table 'ProjectItem' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableProjectItem.KeyUserRequiredColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string AddItemRelevant
      {
        get
        {
          try
          {
            return (string) this[this.tableProjectItem.AddItemRelevantColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'AddItemRelevant' in table 'ProjectItem' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableProjectItem.AddItemRelevantColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string DelAllowedInMaintenance
      {
        get
        {
          if (this.IsDelAllowedInMaintenanceNull())
            return (string) null;
          return (string) this[this.tableProjectItem.DelAllowedInMaintenanceColumn];
        }
        set
        {
          this[this.tableProjectItem.DelAllowedInMaintenanceColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsProjectItemTypeNull()
      {
        return this.IsNull(this.tableProjectItem.ProjectItemTypeColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetProjectItemTypeNull()
      {
        this[this.tableProjectItem.ProjectItemTypeColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsDescriptionNull()
      {
        return this.IsNull(this.tableProjectItem.DescriptionColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetDescriptionNull()
      {
        this[this.tableProjectItem.DescriptionColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsLMContentTypeNull()
      {
        return this.IsNull(this.tableProjectItem.LMContentTypeColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetLMContentTypeNull()
      {
        this[this.tableProjectItem.LMContentTypeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsKeyUserRequiredNull()
      {
        return this.IsNull(this.tableProjectItem.KeyUserRequiredColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetKeyUserRequiredNull()
      {
        this[this.tableProjectItem.KeyUserRequiredColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsAddItemRelevantNull()
      {
        return this.IsNull(this.tableProjectItem.AddItemRelevantColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetAddItemRelevantNull()
      {
        this[this.tableProjectItem.AddItemRelevantColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsDelAllowedInMaintenanceNull()
      {
        return this.IsNull(this.tableProjectItem.DelAllowedInMaintenanceColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetDelAllowedInMaintenanceNull()
      {
        this[this.tableProjectItem.DelAllowedInMaintenanceColumn] = Convert.DBNull;
      }
    }

    public class FeatureRow : DataRow
    {
      private RepositoryDataSet.FeatureDataTable tableFeature;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      internal FeatureRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableFeature = (RepositoryDataSet.FeatureDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string FeatureKey
      {
        get
        {
          return (string) this[this.tableFeature.FeatureKeyColumn];
        }
        set
        {
          this[this.tableFeature.FeatureKeyColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Feature
      {
        get
        {
          try
          {
            return (string) this[this.tableFeature.FeatureColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Feature' in table 'Feature' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableFeature.FeatureColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public string Description
      {
        get
        {
          try
          {
            return (string) this[this.tableFeature.DescriptionColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'Description' in table 'Feature' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableFeature.DescriptionColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string SAPSolutionType
      {
        get
        {
          try
          {
            return (string) this[this.tableFeature.SAPSolutionTypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'SAPSolutionType' in table 'Feature' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableFeature.SAPSolutionTypeColumn] = (object) value;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public string PDISubtype
      {
        get
        {
          try
          {
            return (string) this[this.tableFeature.PDISubtypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("The value for column 'PDISubtype' in table 'Feature' is DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableFeature.PDISubtypeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsFeatureNull()
      {
        return this.IsNull(this.tableFeature.FeatureColumn);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public void SetFeatureNull()
      {
        this[this.tableFeature.FeatureColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsDescriptionNull()
      {
        return this.IsNull(this.tableFeature.DescriptionColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetDescriptionNull()
      {
        this[this.tableFeature.DescriptionColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public bool IsSAPSolutionTypeNull()
      {
        return this.IsNull(this.tableFeature.SAPSolutionTypeColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetSAPSolutionTypeNull()
      {
        this[this.tableFeature.SAPSolutionTypeColumn] = Convert.DBNull;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public bool IsPDISubtypeNull()
      {
        return this.IsNull(this.tableFeature.PDISubtypeColumn);
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public void SetPDISubtypeNull()
      {
        this[this.tableFeature.PDISubtypeColumn] = Convert.DBNull;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class NamespacesRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.NamespacesRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public NamespacesRowChangeEvent(RepositoryDataSet.NamespacesRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.NamespacesRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class BusinessObjectsRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.BusinessObjectsRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public BusinessObjectsRowChangeEvent(RepositoryDataSet.BusinessObjectsRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.BusinessObjectsRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class DataTypesRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.DataTypesRow eventRow;
      private DataRowAction eventAction;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataTypesRowChangeEvent(RepositoryDataSet.DataTypesRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.DataTypesRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class NodesRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.NodesRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public NodesRowChangeEvent(RepositoryDataSet.NodesRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.NodesRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class ActionsRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.ActionsRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public ActionsRowChangeEvent(RepositoryDataSet.ActionsRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.ActionsRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class SolutionsRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.SolutionsRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public SolutionsRowChangeEvent(RepositoryDataSet.SolutionsRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.SolutionsRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class LibrariesRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.LibrariesRow eventRow;
      private DataRowAction eventAction;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public LibrariesRowChangeEvent(RepositoryDataSet.LibrariesRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.LibrariesRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class MessageTypesRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.MessageTypesRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public MessageTypesRowChangeEvent(RepositoryDataSet.MessageTypesRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.MessageTypesRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class MsgDataTypeNodesRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.MsgDataTypeNodesRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public MsgDataTypeNodesRowChangeEvent(RepositoryDataSet.MsgDataTypeNodesRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.MsgDataTypeNodesRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class BAdIsRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.BAdIsRow eventRow;
      private DataRowAction eventAction;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public BAdIsRowChangeEvent(RepositoryDataSet.BAdIsRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIsRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class BAdIFiltersRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.BAdIFiltersRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public BAdIFiltersRowChangeEvent(RepositoryDataSet.BAdIFiltersRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIFiltersRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class BAdIMethodsRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.BAdIMethodsRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public BAdIMethodsRowChangeEvent(RepositoryDataSet.BAdIMethodsRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public RepositoryDataSet.BAdIMethodsRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class ProjectItemSolutionRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.ProjectItemSolutionRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public ProjectItemSolutionRowChangeEvent(RepositoryDataSet.ProjectItemSolutionRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.ProjectItemSolutionRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class ProjectItemRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.ProjectItemRow eventRow;
      private DataRowAction eventAction;

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public ProjectItemRowChangeEvent(RepositoryDataSet.ProjectItemRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.ProjectItemRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
    public class FeatureRowChangeEvent : EventArgs
    {
      private RepositoryDataSet.FeatureRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public FeatureRowChangeEvent(RepositoryDataSet.FeatureRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      [DebuggerNonUserCode]
      public RepositoryDataSet.FeatureRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }
  }
}
