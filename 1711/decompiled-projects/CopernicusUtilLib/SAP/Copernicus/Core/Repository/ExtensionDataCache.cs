﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtensionDataCache
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SAP.Copernicus.Core.Repository
{
  public class ExtensionDataCache
  {
    protected static ExtensionDataCache instance = new ExtensionDataCache();
    public static string extDataTypeDefeaultNameSpace = "http://sap.com/xi/AP/Common/GDT";
    private List<BOListElement> boList = new List<BOListElement>();
    private List<XBOHeader> xboHeaderList = new List<XBOHeader>();
    private List<ExtensionField> fieldList = new List<ExtensionField>();
    private List<XBOListElement> xboList = new List<XBOListElement>();
    private List<MessageAssignment> messageAssignmentList = new List<MessageAssignment>();
    private List<PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE> messageList = new List<PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE>();
    private Dictionary<NodeKey, List<Action>> actionList = new Dictionary<NodeKey, List<Action>>((IEqualityComparer<NodeKey>) new NodeKey.EqualityComparer());
    private List<ExtensionFieldType> extFieldTypes = new List<ExtensionFieldType>();
    internal const string TRACE_CATEGORY = "ExtensionDataCache";
    private List<PDI_EXT_S_EXT_FIELDS> fields;
    private PDI_EXT_S_SCENARIO[] scenarios;

    public List<ExtensionFieldType> ExtFieldTypes
    {
      get
      {
        if (this.extFieldTypes.Count == 0)
        {
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "Amount",
            ExtensibilityName = "AMOUNT",
            EsrNameSpace = ExtensionDataCache.extDataTypeDefeaultNameSpace,
            EsrName = "Amount",
            ProxyName = "APC_S_AMOUNT"
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "Date",
            ExtensibilityName = "DATE",
            EsrNameSpace = ExtensionDataCache.extDataTypeDefeaultNameSpace,
            EsrName = "Date",
            ProxyName = "APC_V_DATE"
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "DateTime",
            ExtensibilityName = "DATETIME",
            EsrNameSpace = ExtensionDataCache.extDataTypeDefeaultNameSpace,
            EsrName = "DateTime",
            ProxyName = "GDT_GLOBAL_DATE_TIME"
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "DecimalValue",
            ExtensibilityName = "DEC",
            EsrName = "DecimalValue",
            ProxyName = string.Empty
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "EmailURI",
            ExtensibilityName = "EMAIL_URI",
            EsrNameSpace = ExtensionDataCache.extDataTypeDefeaultNameSpace,
            EsrName = "EmailURI",
            ProxyName = "APC_S_EMAIL_URI"
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "Identifier",
            ExtensibilityName = "PDI_ID",
            EsrNameSpace = ExtensionDataCache.extDataTypeDefeaultNameSpace,
            EsrName = "Identifier",
            ProxyName = "APC_S_IDENTIFIER"
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "Indicator",
            ExtensibilityName = "CHECKBOX",
            EsrNameSpace = ExtensionDataCache.extDataTypeDefeaultNameSpace,
            EsrName = "Indicator",
            ProxyName = "APC_V_INDICATOR"
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "Quantity",
            ExtensibilityName = "QUANTITY",
            EsrNameSpace = ExtensionDataCache.extDataTypeDefeaultNameSpace,
            EsrName = "Quantity",
            ProxyName = "APC_S_QUANTITY"
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "Text",
            ExtensibilityName = "TEXT",
            EsrName = "Text",
            ProxyName = string.Empty
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "ShortText",
            ExtensibilityName = "SHORT_TEXT",
            EsrName = "ShortText",
            ProxyName = string.Empty
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "MediumText",
            ExtensibilityName = "MED_TEXT",
            EsrName = "MediumText",
            ProxyName = string.Empty
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "LongText",
            ExtensibilityName = "LONG_TEXT",
            EsrName = "LongText",
            ProxyName = string.Empty
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "LANGUAGEINDEPENDENT_EXTENDED_Text",
            ExtensibilityName = "TEXT",
            EsrName = "Text",
            ProxyName = string.Empty
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "Time",
            ExtensibilityName = "TIME",
            EsrNameSpace = ExtensionDataCache.extDataTypeDefeaultNameSpace,
            EsrName = "Time",
            ProxyName = "APC_V_TIME"
          });
          this.extFieldTypes.Add(new ExtensionFieldType()
          {
            Name = "WebURI",
            ExtensibilityName = "WEB_URI",
            EsrNameSpace = ExtensionDataCache.extDataTypeDefeaultNameSpace,
            EsrName = "WebURI",
            ProxyName = "APC_V_WEB_URI"
          });
        }
        return this.extFieldTypes;
      }
    }

    public static ExtensionDataCache GetInstance()
    {
      return ExtensionDataCache.instance;
    }

    public void Reset(bool excludeBoList)
    {
      List<BOListElement> boListElementList = new List<BOListElement>();
      List<PDI_EXT_S_EXT_FIELDS> pdiExtSExtFieldsList = new List<PDI_EXT_S_EXT_FIELDS>();
      if (excludeBoList)
      {
        boListElementList = this.boList;
        pdiExtSExtFieldsList = this.fields;
      }
      this.Reset();
      Trace.WriteLine(string.Format("Cache reset.", (object) nameof (ExtensionDataCache)));
      if (!excludeBoList)
        return;
      this.boList = boListElementList;
      this.fields = pdiExtSExtFieldsList;
      Trace.WriteLine(string.Format("Cache reset: BoList excluded from reset.", (object) nameof (ExtensionDataCache)));
    }

    public void Reset()
    {
      this.boList = new List<BOListElement>();
      this.fieldList = new List<ExtensionField>();
      this.fields = (List<PDI_EXT_S_EXT_FIELDS>) null;
      this.actionList.Clear();
      this.messageAssignmentList = new List<MessageAssignment>();
      this.messageList = new List<PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE>();
      this.scenarios = (PDI_EXT_S_SCENARIO[]) null;
      ExtScenDataCache.GetInstance().Reset();
      Trace.WriteLine(string.Format("Cache reset.", (object) nameof (ExtensionDataCache)));
    }

    public List<string> GetBONodeNames(string namespaceName, string boName)
    {
      string openSolutionName = ProjectUtil.GetOpenSolutionName();
      BOListElement bo;
      if (this.getBO(namespaceName, boName, openSolutionName, out bo))
        return bo.getNodeNames();
      return new List<string>();
    }

    public bool IsRootNodeExtensible(string namespaceName, string boName)
    {
      foreach (string boNodeName in this.GetBONodeNames(namespaceName, boName))
      {
        if (boNodeName.ToUpper().Equals("ROOT"))
          return true;
      }
      return false;
    }

    public bool IsNodeExtensible(string namespaceName, string boName, string boNode)
    {
      foreach (PDI_EXT_S_EXT_NODE pdiExtSExtNode in this.GetBONodesData(namespaceName, boName))
      {
        string str = pdiExtSExtNode.BO_ESR_NAMESPACE.Replace("http://sap.com/xi/", string.Empty).Replace("/", ".");
        str.Trim();
        if (str == namespaceName && pdiExtSExtNode.BO_ESR_NAME == boName && (pdiExtSExtNode.ND_NAME == boNode && pdiExtSExtNode.IS_EXTENSIBLE == "X"))
          return true;
      }
      return false;
    }

    public PDI_EXT_S_EXT_NODE GetRootNode(string namespaceName, string boName)
    {
      PDI_EXT_S_EXT_NODE pdiExtSExtNode = new PDI_EXT_S_EXT_NODE();
      string str = "http://sap.com/xi/" + namespaceName.Replace(".", "/");
      str.Trim();
      foreach (PDI_EXT_S_EXT_FIELDS field in this.fields)
      {
        if (field.BO_ESR_NAME.Equals(boName) && field.BO_ESR_NAMESPACE.Equals(str) && field.PARENT_NODE_PRX_NAME.Equals(""))
        {
          pdiExtSExtNode.MAX_BO_PRX_NAME = field.MAX_BO_PRX_NAME;
          pdiExtSExtNode.HI_LEVEL = "1";
          pdiExtSExtNode.ND_NAME = field.NODE_ESR_NAME;
          pdiExtSExtNode.PARENT_NODE_NAME = field.PARENT_NODE_PRX_NAME;
          pdiExtSExtNode.IS_EXTENSIBLE = "";
          pdiExtSExtNode.PRX_ND_NAME = field.NODE_PRX_NAME;
          pdiExtSExtNode.PRX_BO_NAME = field.BO_PRX_NAME;
          return pdiExtSExtNode;
        }
      }
      return pdiExtSExtNode;
    }

    public List<PDI_EXT_S_EXT_NODE> GetBONodesData(string namespaceName, string boName)
    {
      string openSolutionName = ProjectUtil.GetOpenSolutionName();
      return this.GetBONodesData(namespaceName, boName, openSolutionName);
    }

    public List<PDI_EXT_S_EXT_NODE> GetBONodesData(string namespaceName, string boName, string solution)
    {
      BOListElement bo;
      if (this.getBO(namespaceName, boName, solution, out bo))
        return bo.getNodes();
      return new List<PDI_EXT_S_EXT_NODE>();
    }

    private bool getBO(string namespaceName, string boName, string solution, out BOListElement bo)
    {
      foreach (BOListElement bo1 in this.boList)
      {
        if (bo1.getNamespace() == namespaceName && bo1.getBoName() == boName)
        {
          bo = bo1;
          return true;
        }
      }
      if (this.loadBoData(namespaceName, boName, solution, out bo))
      {
        this.boList.Add(bo);
        return true;
      }
      bo = (BOListElement) null;
      return false;
    }

    private void AddActions(PDI_EXT_S_EXT_ACTION[] actions)
    {
      if (actions == null || actions.Length == 0)
        return;
      Array.Sort<PDI_EXT_S_EXT_ACTION>(actions, (IComparer<PDI_EXT_S_EXT_ACTION>) new ActionComparer());
      PDI_EXT_S_EXT_ACTION pdiExtSExtAction = (PDI_EXT_S_EXT_ACTION) null;
      List<Action> actionList = (List<Action>) null;
      foreach (PDI_EXT_S_EXT_ACTION action1 in actions)
      {
        Action action2 = new Action(action1.PRX_ACTION_NAME, action1.ESR_ACTION_NAME, !string.IsNullOrEmpty(action1.PSM_RELEASED), !string.IsNullOrEmpty(action1.CUSTOM_ACTION), !string.IsNullOrEmpty(action1.NODE_OFF_ENABLED));
        if (pdiExtSExtAction == null || pdiExtSExtAction.PRX_BO_NAME != action1.PRX_BO_NAME || pdiExtSExtAction.PRX_NODE_NAME != action1.PRX_NODE_NAME)
        {
          if (actionList != null)
            this.actionList[new NodeKey(pdiExtSExtAction.PRX_BO_NAME, pdiExtSExtAction.PRX_NODE_NAME)] = actionList;
          actionList = new List<Action>() { action2 };
        }
        else
          actionList.Add(action2);
        pdiExtSExtAction = action1;
      }
      if (actionList == null)
        return;
      this.actionList[new NodeKey(pdiExtSExtAction.PRX_BO_NAME, pdiExtSExtAction.PRX_NODE_NAME)] = actionList;
    }

    private bool loadBoData(string namespaceName, string boName, string solution, out BOListElement bo)
    {
      PDI_EXT_S_EXT_NODE[] ET_EXT_NODES = (PDI_EXT_S_EXT_NODE[]) null;
      PDI_EXT_S_EXT_FIELDS[] ET_NODES_FIELDS = (PDI_EXT_S_EXT_FIELDS[]) null;
      PDI_EXT_S_EXT_FIELD_SCENARIOS[] ET_EXT_FIELD_SCENARIOS = (PDI_EXT_S_EXT_FIELD_SCENARIOS[]) null;
      PDI_EXT_S_EXT_ACTION[] ET_EXT_ACTION = (PDI_EXT_S_EXT_ACTION[]) null;
      int num;
      try
      {
        new ExtensionHandler().GetExtensibleNodes(boName, namespaceName, solution, out ET_EXT_NODES, out ET_NODES_FIELDS, out ET_EXT_FIELD_SCENARIOS, out ET_EXT_ACTION);
        this.fields = new List<PDI_EXT_S_EXT_FIELDS>((IEnumerable<PDI_EXT_S_EXT_FIELDS>) ET_NODES_FIELDS);
        num = ET_EXT_NODES.Length;
      }
      catch (Exception ex)
      {
        Trace.TraceError("Error in Extensible Nodes:" + (object) ex);
        num = 0;
      }
      this.AddActions(ET_EXT_ACTION);
      if (num > 0)
      {
        List<PDI_EXT_S_EXT_NODE> nodeDataList = new List<PDI_EXT_S_EXT_NODE>((IEnumerable<PDI_EXT_S_EXT_NODE>) ET_EXT_NODES);
        List<string> nodeNameList = new List<string>();
        foreach (PDI_EXT_S_EXT_NODE pdiExtSExtNode in nodeDataList)
          nodeNameList.Add(pdiExtSExtNode.ND_NAME);
        bo = new BOListElement(namespaceName, boName, nodeDataList, nodeNameList);
        return true;
      }
      bo = (BOListElement) null;
      return false;
    }

    public bool checkFieldExists(string namespaceName, string boName, string nodeName, string fieldName, out bool inStandard, out string usedBoNamespace, out string usedBoName, out string usedNodeName, out string usedFieldType, out bool usedIsReference, out string boNameRef, out string nodeNameRef, out bool defExists, out string boNameDef, out string nodeNameDef, out bool kut)
    {
      Trace.WriteLine(string.Format("CheckFieldExists Field {0} in node {1} of BO {2}...", (object) fieldName, (object) nodeName, (object) boName), nameof (ExtensionDataCache));
      if (this.fields == null)
      {
        Trace.WriteLine(string.Format("CheckFieldExists: Cache is reset. Start loading data from backend."), nameof (ExtensionDataCache));
        ExtensionDataCache.GetInstance().GetBONodesData(namespaceName, boName);
        Trace.WriteLine(string.Format("CheckFieldExists: Loading data from backend completed."), nameof (ExtensionDataCache));
      }
      usedIsReference = false;
      boNameRef = "";
      nodeNameRef = "";
      defExists = false;
      boNameDef = "";
      nodeNameDef = "";
      kut = false;
      List<PDI_EXT_S_EXT_FIELDS> pdiExtSExtFieldsList = new List<PDI_EXT_S_EXT_FIELDS>();
      foreach (PDI_EXT_S_EXT_FIELDS field in this.fields)
      {
        if (fieldName.Equals(field.FIELD_ESR_NAME))
        {
          if (field.EXTENDED.Equals("X"))
          {
            usedBoNamespace = field.BO_ESR_NAMESPACE;
            usedBoName = field.BO_ESR_NAME;
            usedNodeName = field.NODE_ESR_NAME;
            if (!this.getFieldTypeName(field.FIELD_TYPE, out usedFieldType))
              usedFieldType = field.FIELD_TYPE;
            usedIsReference = false;
            boNameRef = field.BO_ESR_NAME;
            pdiExtSExtFieldsList.Add(field);
          }
          else
          {
            inStandard = true;
            usedBoNamespace = field.BO_ESR_NAMESPACE;
            usedBoName = field.BO_ESR_NAME;
            usedNodeName = field.NODE_ESR_NAME;
            usedFieldType = field.FIELD_TYPE;
            usedIsReference = false;
            boNameRef = field.BO_ESR_NAME;
            return true;
          }
        }
      }
      if (pdiExtSExtFieldsList.Count > 0)
      {
        usedBoNamespace = pdiExtSExtFieldsList[0].BO_ESR_NAMESPACE;
        usedBoName = pdiExtSExtFieldsList[0].BO_ESR_NAME;
        usedNodeName = pdiExtSExtFieldsList[0].NODE_ESR_NAME;
        if (!this.getFieldTypeName(pdiExtSExtFieldsList[0].FIELD_TYPE, out usedFieldType))
          usedFieldType = pdiExtSExtFieldsList[0].FIELD_TYPE;
        inStandard = false;
        usedIsReference = false;
        foreach (PDI_EXT_S_EXT_FIELDS pdiExtSExtFields in pdiExtSExtFieldsList)
        {
          if (pdiExtSExtFields.FIELD_TYPE_CAT.Equals("RF"))
          {
            usedIsReference = true;
            boNameRef = pdiExtSExtFields.BO_ESR_NAME;
            nodeNameRef = pdiExtSExtFields.NODE_ESR_NAME;
          }
          else if (pdiExtSExtFields.FIELD_TYPE_CAT.Equals("DEF"))
          {
            defExists = true;
            boNameDef = pdiExtSExtFields.BO_ESR_NAME;
            nodeNameDef = pdiExtSExtFields.NODE_ESR_NAME;
            Trace.WriteLine(string.Format("ExtensionDataCache: checkFieldExists: Field {0} invalid in node {1} of BO {2}: It exists already in BO {3} node {4}", (object) fieldName, (object) nodeName, (object) boName, (object) boNameDef, (object) nodeNameDef), nameof (ExtensionDataCache));
          }
          if (!pdiExtSExtFields.FIELD_OWNER_CODE.Equals("P"))
            kut = true;
        }
        if (kut && !defExists)
        {
          defExists = true;
          boNameDef = usedBoName;
          nodeNameDef = usedNodeName;
        }
        if (!kut && !defExists)
        {
          defExists = true;
          boNameDef = usedBoName;
          nodeNameDef = usedNodeName;
          foreach (PDI_EXT_S_EXT_FIELDS pdiExtSExtFields in pdiExtSExtFieldsList)
          {
            if (!pdiExtSExtFields.FIELD_TYPE.Equals(""))
            {
              boNameDef = pdiExtSExtFields.BO_ESR_NAME;
              nodeNameDef = pdiExtSExtFields.NODE_ESR_NAME;
              break;
            }
          }
        }
        return true;
      }
      usedBoNamespace = "";
      usedBoName = "";
      usedNodeName = "";
      usedFieldType = "";
      inStandard = false;
      usedIsReference = false;
      boNameRef = "";
      defExists = false;
      boNameDef = "";
      return false;
    }

    public List<Action> getActionsOfNode(string boProxyName, string nodeProxyName)
    {
      List<Action> actionList;
      if (this.actionList.TryGetValue(new NodeKey(boProxyName, nodeProxyName), out actionList))
        return actionList;
      return (List<Action>) null;
    }

    public List<NodeNameExt> getAllNodes(string boNamespace, string boName)
    {
      if (this.fields == null)
        ExtensionDataCache.GetInstance().GetBONodesData(boNamespace, boName);
      string str = "";
      List<NodeNameExt> nodeNameExtList = new List<NodeNameExt>();
      foreach (PDI_EXT_S_EXT_FIELDS field in this.fields)
      {
        if (field.BO_ESR_NAMESPACE.Replace("http://sap.com/xi/", "").Replace("/", ".") == boNamespace && field.BO_ESR_NAME == boName && (!field.NODE_PRX_NAME.Equals(str) && !field.EXTENDED.Equals("X")))
        {
          str = field.NODE_PRX_NAME;
          NodeNameExt nodeNameExt = new NodeNameExt(field.NODE_ESR_NAME, field.NODE_PRX_NAME, field.PARENT_NODE_PRX_NAME, field.BO_PRX_NAME, field.RELEASED, field.DISABLED, field.OFF_ENABLED);
          nodeNameExtList.Add(nodeNameExt);
        }
      }
      return nodeNameExtList;
    }

    public bool checkTypeIsValid(string typeName)
    {
      ExtensionFieldType extTypeInfo = new ExtensionFieldType();
      return this.checkTypeIsValid(typeName, out extTypeInfo);
    }

    public bool checkTypeIsValid(string typeName, out ExtensionFieldType extTypeInfo)
    {
      foreach (ExtensionFieldType extFieldType in this.ExtFieldTypes)
      {
        if (extFieldType.Name == typeName)
        {
          extTypeInfo = extFieldType;
          return true;
        }
      }
      extTypeInfo = (ExtensionFieldType) null;
      return false;
    }

    private bool getFieldTypeName(string extensibilityTypeName, out string typeName)
    {
      foreach (ExtensionFieldType extFieldType in this.ExtFieldTypes)
      {
        if (extFieldType.ExtensibilityName.Equals(extensibilityTypeName))
        {
          typeName = extFieldType.Name;
          return true;
        }
      }
      typeName = "";
      return false;
    }

    public bool addField(string boNamespace, string boName, string name, string type, string node, string label, string tooltip, string length, string decimals, bool transient, string defaultValue, string defaultCode, PDI_EXT_S_ID_TARGET_ATTRIBUTE relation, out string BOName, out string BONode, out string defaultValueError)
    {
      bool flag = false;
      BOName = string.Empty;
      BONode = string.Empty;
      defaultValueError = string.Empty;
      foreach (ExtensionField field in this.fieldList)
      {
        if (field.getName().Equals(name))
        {
          int num = this.fieldList.Where<ExtensionField>((Func<ExtensionField, bool>) (efname => efname.getName() == name)).Count<ExtensionField>();
          if (type != string.Empty && num >= 1)
          {
            if (num == 1)
            {
              if (!field.getType().Equals(string.Empty))
              {
                flag = true;
                BOName = field.getBoName();
                BONode = field.getNode();
                Trace.WriteLine(string.Format("AddField: Field {0} already in fieldList: BO {1} node {2} type {3}.", (object) name, (object) BOName, (object) BONode, (object) field.getType()), nameof (ExtensionDataCache));
                break;
              }
              flag = false;
            }
          }
          else
            flag = false;
        }
      }
      if (flag)
        return false;
      this.fieldList.Add(new ExtensionField(boNamespace, boName, name, type, node, label, tooltip, length, decimals, transient, defaultValue, defaultCode, relation));
      return true;
    }

    private bool mapDefaults(string type, List<string> boDefaultsList, out string defaultCode, out string defaultValue)
    {
      defaultCode = "";
      defaultValue = "";
      if (boDefaultsList.Count == 2)
      {
        switch (type)
        {
          case "Indicator":
            defaultValue = boDefaultsList[0];
            if (defaultValue != "true" && defaultValue != "false")
              return false;
            break;
          case "Text":
            defaultValue = boDefaultsList[0];
            break;
          case "Decimal":
            defaultValue = boDefaultsList[0];
            break;
          case "Date":
            defaultValue = boDefaultsList[0];
            break;
          case "Time":
            defaultValue = boDefaultsList[0];
            break;
          case "EmailURI":
            defaultValue = boDefaultsList[0];
            break;
          case "WebURI":
            defaultValue = boDefaultsList[0];
            break;
          case "Identifier":
            defaultValue = boDefaultsList[0];
            break;
          default:
            defaultValue = this.extractDefault("CONTENT", boDefaultsList);
            break;
        }
      }
      else
      {
        switch (type)
        {
          case "Amount":
            defaultValue = this.extractDefault("CONTENT", boDefaultsList);
            defaultCode = this.extractDefault("CURRENCYCODE", boDefaultsList);
            break;
          case "Quantity":
            defaultValue = this.extractDefault("CONTENT", boDefaultsList);
            defaultCode = this.extractDefault("UNITCODE", boDefaultsList);
            break;
          default:
            defaultValue = this.extractDefault("CONTENT", boDefaultsList);
            break;
        }
      }
      return true;
    }

    private string extractDefault(string keyword, List<string> boDefaultsList)
    {
      string str = (string) null;
      bool flag = false;
      int num = 0;
      int index = 0;
      foreach (string boDefaults in boDefaultsList)
      {
        if (flag)
          return boDefaultsList[index];
        if (boDefaults.ToUpper().Equals(keyword))
        {
          flag = true;
          index = num + 2;
        }
        ++num;
      }
      return str;
    }

    public bool addField(string boNamespace, string boName, string name, string type, string node, string length, string decimals, string defaultValue, string defaultCode, PDI_EXT_S_ID_TARGET_ATTRIBUTE relation, out string errboName, out string errboNode, out string defValError)
    {
      return this.addField(boNamespace, boName, name, type, node, "", "", length, decimals, false, defaultValue, defaultCode, relation, out errboName, out errboNode, out defValError);
    }

    public List<ExtensionField> getFields(string boNamespace, string boName)
    {
      List<ExtensionField> extensionFieldList = new List<ExtensionField>();
      foreach (ExtensionField field in this.fieldList)
      {
        if (field.getNamespace() == boNamespace && field.getBoName() == boName)
          extensionFieldList.Add(field);
      }
      return extensionFieldList;
    }

    public List<PDI_EXT_S_EXT_FIELDS> getActivatedFields(string namespaceName, string boName)
    {
      List<PDI_EXT_S_EXT_FIELDS> pdiExtSExtFieldsList = new List<PDI_EXT_S_EXT_FIELDS>();
      if (this.fields == null)
        ExtensionDataCache.GetInstance().GetBONodesData(namespaceName, boName);
      foreach (PDI_EXT_S_EXT_FIELDS field in this.fields)
      {
        if (field.FIELD_TYPE_CAT == "DEF")
          pdiExtSExtFieldsList.Add(field);
      }
      return pdiExtSExtFieldsList;
    }

    public void getXBOInfo(string xRepPath, out string boNamespace, out string boName)
    {
      foreach (XBOListElement xbo in this.xboList)
      {
        if (xbo.getXBO().Equals(xRepPath))
        {
          boNamespace = xbo.getNamespace();
          boName = xbo.getBO();
          return;
        }
      }
      IDictionary<string, string> attribs = (IDictionary<string, string>) null;
      string content;
      new XRepHandler().Read(xRepPath, out content, out attribs);
      foreach (string key1 in attribs.Keys.Where<string>((Func<string, bool>) (n =>
      {
        if (!n.StartsWith("~"))
          return n.Contains("~");
        return false;
      })).ToList<string>())
      {
        string key2 = key1.Split('~')[0];
        attribs.Add(key2, attribs[key1]);
        attribs.Remove(key1);
      }
      if (!attribs.TryGetValue("esrBOName", out boName))
        boName = string.Empty;
      if (!attribs.TryGetValue("BONamespace", out boNamespace))
        boNamespace = attribs.TryGetValue("esrBONameSpace", out boNamespace) ? "http://sap.com/xi/" + boNamespace.Replace(".", "/") : string.Empty;
      if (!(boNamespace != string.Empty) || !(boName != string.Empty))
        return;
      this.xboList.Add(new XBOListElement(xRepPath, boName, boNamespace));
    }

    public bool addMessageAssignment(string node, string message)
    {
      bool flag1 = false;
      foreach (PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE message1 in this.messageList)
      {
        if (message1.ID.Trim().Equals(message))
        {
          flag1 = true;
          break;
        }
      }
      if (!flag1)
        return false;
      bool flag2 = false;
      foreach (MessageAssignment messageAssignment in this.messageAssignmentList)
      {
        if (messageAssignment.NodeProxy.Equals(node) && messageAssignment.Message.Equals(message))
        {
          flag2 = true;
          return true;
        }
      }
      if (flag2)
        return false;
      this.messageAssignmentList.Add(new MessageAssignment(node, message));
      return true;
    }

    public PDI_EXT_S_MESSAGES[] getMessages()
    {
      PDI_EXT_S_MESSAGES[] pdiExtSMessagesArray = new PDI_EXT_S_MESSAGES[this.messageAssignmentList.Count];
      int index = 0;
      foreach (MessageAssignment messageAssignment in this.messageAssignmentList)
      {
        pdiExtSMessagesArray[index] = new PDI_EXT_S_MESSAGES();
        pdiExtSMessagesArray[index].NODE_PROXY_NAME = messageAssignment.NodeProxy;
        pdiExtSMessagesArray[index].MESSAGE_ESR_NAME = messageAssignment.Message;
        ++index;
      }
      return pdiExtSMessagesArray;
    }

    public void setMessage(PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE message)
    {
      int index = this.messageList.FindIndex((Predicate<PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE>) (x => this.FindMesgListID(x, message.ID)));
      if (index >= 0)
        this.messageList.RemoveAt(index);
      this.messageList.Add(message);
    }

    private bool FindMesgListID(PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE msg, string id)
    {
      return msg.ID == id;
    }

    private bool FindMessageListID(string msg, string id)
    {
      return msg == id;
    }

    public PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE[] getDeclaredMessages()
    {
      return this.messageList.ToArray();
    }

    public List<MessageAssignment> getRaisedMessages()
    {
      return this.messageAssignmentList;
    }

    public XBOHeader updateXBOHeaderList(string boNamespaceName, string boName, string xRepPath)
    {
      XBOHeader xboHeader;
      try
      {
        xboHeader = this.xboHeaderList.Where<XBOHeader>((Func<XBOHeader, bool>) (xboH => xboH.getXboFileName() == xRepPath)).First<XBOHeader>();
      }
      catch
      {
        IDictionary<string, string> attribs = (IDictionary<string, string>) null;
        XRepHandler xrepHandler = new XRepHandler();
        string content = (string) null;
        xrepHandler.Read(xRepPath, out content, out attribs);
        foreach (string key1 in attribs.Keys.Where<string>((Func<string, bool>) (n =>
        {
          if (!n.StartsWith("~"))
            return n.Contains("~");
          return false;
        })).ToList<string>())
        {
          string key2 = key1.Split('~')[0];
          attribs.Add(key2, attribs[key1]);
          attribs.Remove(key1);
        }
        string boNamespaceName1;
        if (!attribs.TryGetValue("esrBONameSpace", out boNamespaceName1))
          boNamespaceName1 = boNamespaceName;
        string boName1;
        if (!attribs.TryGetValue("esrBOName", out boName1))
          boName1 = boName;
        xboHeader = new XBOHeader(boNamespaceName1, boName1, xRepPath);
        this.xboHeaderList.Add(xboHeader);
      }
      return xboHeader;
    }

    public XBOHeader getXBOHeader(string xRepPath)
    {
      XBOHeader xboHeader = (XBOHeader) null;
      try
      {
        xboHeader = this.xboHeaderList.Where<XBOHeader>((Func<XBOHeader, bool>) (xboH => xboH.getXboFileName() == xRepPath)).First<XBOHeader>();
      }
      catch
      {
      }
      return xboHeader;
    }

    public bool check(string boNamespace, string boName, string xboFile, out string refBoName, out string refNodeName, out string fieldName)
    {
      if (this.fields == null)
        ExtensionDataCache.GetInstance().GetBONodesData(boNamespace, boName);
      List<PDI_EXT_S_EXT_FIELDS> pdiExtSExtFieldsList1 = new List<PDI_EXT_S_EXT_FIELDS>();
      foreach (PDI_EXT_S_EXT_FIELDS field in this.fields)
      {
        if (field.FIELD_TYPE_CAT.Equals("DEF") && field.BO_ESR_NAMESPACE.Equals(boNamespace) && field.BO_ESR_NAME.Equals(boName))
          pdiExtSExtFieldsList1.Add(field);
      }
      if (pdiExtSExtFieldsList1.Count > 0)
      {
        List<PDI_EXT_S_EXT_FIELDS> pdiExtSExtFieldsList2 = new List<PDI_EXT_S_EXT_FIELDS>();
        foreach (PDI_EXT_S_EXT_FIELDS pdiExtSExtFields in pdiExtSExtFieldsList1)
        {
          bool flag = false;
          foreach (ExtensionField field in this.fieldList)
          {
            if (field.getName().Equals(pdiExtSExtFields.FIELD_ESR_NAME) && !field.getType().Equals(""))
            {
              flag = true;
              break;
            }
          }
          if (!flag)
            pdiExtSExtFieldsList2.Add(pdiExtSExtFields);
        }
        if (pdiExtSExtFieldsList2.Count > 0)
        {
          List<PDI_EXT_S_EXT_FIELDS> pdiExtSExtFieldsList3 = new List<PDI_EXT_S_EXT_FIELDS>();
          foreach (PDI_EXT_S_EXT_FIELDS field in this.fields)
          {
            if (field.FIELD_TYPE_CAT.Equals("RF"))
              pdiExtSExtFieldsList3.Add(field);
          }
          if (pdiExtSExtFieldsList3.Count > 0)
          {
            foreach (PDI_EXT_S_EXT_FIELDS pdiExtSExtFields1 in pdiExtSExtFieldsList2)
            {
              foreach (PDI_EXT_S_EXT_FIELDS pdiExtSExtFields2 in pdiExtSExtFieldsList3)
              {
                if (pdiExtSExtFields2.FIELD_ESR_NAME.Equals(pdiExtSExtFields1.FIELD_ESR_NAME))
                {
                  refBoName = pdiExtSExtFields2.BO_ESR_NAME;
                  refNodeName = pdiExtSExtFields2.NODE_ESR_NAME;
                  fieldName = pdiExtSExtFields1.FIELD_ESR_NAME;
                  return false;
                }
              }
            }
          }
        }
      }
      refBoName = "";
      refNodeName = "";
      fieldName = "";
      return true;
    }
  }
}
