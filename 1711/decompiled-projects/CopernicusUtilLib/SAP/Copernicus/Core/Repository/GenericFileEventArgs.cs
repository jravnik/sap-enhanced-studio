﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.GenericFileEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class GenericFileEventArgs
  {
    public GenericFileEventArgs(bool overallStatus, bool deltaMode = false, bool migrationMode = false)
    {
      this.OverallStatus = overallStatus;
      this.DeltaMode = deltaMode;
      this.MigrationMode = migrationMode;
    }

    public bool OverallStatus { get; private set; }

    public bool DeltaMode { get; private set; }

    public bool MigrationMode { get; private set; }

    public override bool Equals(object obj)
    {
      GenericFileEventArgs genericFileEventArgs = obj as GenericFileEventArgs;
      return genericFileEventArgs != null && genericFileEventArgs.DeltaMode == this.DeltaMode && genericFileEventArgs.OverallStatus == this.OverallStatus;
    }

    public override int GetHashCode()
    {
      return this.OverallStatus.GetHashCode() ^ this.DeltaMode.GetHashCode();
    }

    public override string ToString()
    {
      return string.Format("[OverallStatus:{0},DeltaMode:{1}]", (object) this.OverallStatus, (object) this.DeltaMode);
    }
  }
}
