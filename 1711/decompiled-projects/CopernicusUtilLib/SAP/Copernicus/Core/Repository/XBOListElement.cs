﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.XBOListElement
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class XBOListElement
  {
    private string xbo;
    private string boName;
    private string boNamespace;

    public XBOListElement(string xbo, string boName, string boNamespace)
    {
      this.xbo = xbo;
      this.boName = boName;
      this.boNamespace = boNamespace;
    }

    public string getXBO()
    {
      return this.xbo;
    }

    public string getBO()
    {
      return this.boName;
    }

    public string getNamespace()
    {
      return this.boNamespace;
    }
  }
}
