﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.XBOHeader
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class XBOHeader
  {
    private string boNamespaceName;
    private string boName;
    private string xboFilePath;

    public XBOHeader(string boNamespaceName, string boName, string xboFilePath)
    {
      this.boNamespaceName = boNamespaceName;
      this.boName = boName;
      this.xboFilePath = xboFilePath;
    }

    public string getNamespace()
    {
      return this.boNamespaceName;
    }

    public string getBoName()
    {
      return this.boName;
    }

    public string getXboFileName()
    {
      return this.xboFilePath;
    }
  }
}
