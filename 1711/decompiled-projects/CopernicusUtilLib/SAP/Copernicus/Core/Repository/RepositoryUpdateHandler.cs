﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.RepositoryUpdateHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Project;

namespace SAP.Copernicus.Core.Repository
{
  public class RepositoryUpdateHandler
  {
    public static void boLifeCycleUpdate(string fpath)
    {
      RepositoryDataCache.GetInstance().RefreshBOFolderNode(ProjectUtil.GetProjectNamespaceForFile(fpath));
    }

    public static void codelistLifeCycleUpdate(string fpath)
    {
      RepositoryDataCache.GetInstance().RefreshDTFolderNode(ProjectUtil.GetProjectNamespaceForFile(fpath));
    }
  }
}
