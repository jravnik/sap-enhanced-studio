﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ActionComparer
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Repository
{
  internal class ActionComparer : IComparer<PDI_EXT_S_EXT_ACTION>
  {
    public int Compare(PDI_EXT_S_EXT_ACTION x, PDI_EXT_S_EXT_ACTION y)
    {
      int num1 = x.PRX_BO_NAME.CompareTo(y.PRX_BO_NAME);
      if (num1 != 0)
        return num1;
      int num2 = x.PRX_NODE_NAME.CompareTo(y.PRX_NODE_NAME);
      if (num2 != 0)
        return num2;
      return x.ESR_ACTION_NAME.CompareTo(y.ESR_ACTION_NAME);
    }
  }
}
