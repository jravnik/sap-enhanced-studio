﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.FileHandlerRegistry
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;

namespace SAP.Copernicus.Core.Repository
{
  public class FileHandlerRegistry
  {
    private readonly string SAVE_EVENT_NAME = typeof (EventHandlerOnFileSave).Name;
    private readonly string DELETION_EVENT_NAME = typeof (EventHandlerOnFileDeletion).Name;
    private readonly string PRE_DELETION_EVENT_NAME = typeof (EventHandlerOnPreFileDeletion).Name;
    private readonly string POST_ACTIVATE_EVENT_NAME = typeof (EventHandlerPostGenricActivate).Name;
    private readonly string POST_CLEAN_EVENT_NAME = typeof (EventHandlerPostGenricClean).Name;
    private readonly string POST_CHECK_EVENT_NAME = typeof (EventHandlerPostGenricCheck).Name;
    private readonly string PRE_ACTIVATE_EVENT_NAME = typeof (EventHandlerPreGenericActivate).Name;
    private readonly string PRE_CHECK_EVENT_NAME = typeof (EventHandlerPreGenericCheck).Name;
    private readonly Dictionary<string, Delegate> eventTable = new Dictionary<string, Delegate>();
    private const string TRACE_CATEGORY = "FileHandlerRegistry";
    protected static FileHandlerRegistry instance;

    public static FileHandlerRegistry INSTANCE
    {
      get
      {
        if (FileHandlerRegistry.instance == null)
          FileHandlerRegistry.instance = new FileHandlerRegistry();
        return FileHandlerRegistry.instance;
      }
    }

    [ImportMany(typeof (GenericFileEventHandler))]
    protected IEnumerable<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>> EventTable { get; set; }

    private FileHandlerRegistry()
    {
      VSPackageUtil.CompositionContainer.ComposeParts((object) this);
    }

    private void AddHandler(string fileEnding, Delegate handler, string eventHandlerType)
    {
      if ((object) handler == null)
        return;
      string key = fileEnding + eventHandlerType;
      lock (this.eventTable)
      {
        Delegate a;
        if (this.eventTable.TryGetValue(key, out a))
          this.eventTable[key] = Delegate.Combine(a, handler);
        else
          this.eventTable[key] = handler;
      }
    }

    public void AddHandler(string fileEnding, EventHandlerOnFileSave handler)
    {
      this.AddHandler(fileEnding, (Delegate) handler, this.SAVE_EVENT_NAME);
    }

    public void AddHandler(string fileEnding, EventHandlerOnFileDeletion handler)
    {
      this.AddHandler(fileEnding, (Delegate) handler, this.DELETION_EVENT_NAME);
    }

    public void AddPreFileDeletionHandler(string fileEnding, EventHandlerOnPreFileDeletion handler)
    {
      this.AddHandler(fileEnding, (Delegate) handler, this.PRE_DELETION_EVENT_NAME);
    }

    public void AddPostActivateHandler(string fileEnding, EventHandlerPostGenricActivate2 handler)
    {
      this.AddHandler(fileEnding, (Delegate) handler, this.POST_ACTIVATE_EVENT_NAME);
    }

    public void AddPostActivateHandler(string fileEnding, EventHandlerPostGenricActivate handler)
    {
      this.AddHandler(fileEnding, (Delegate) handler, this.POST_ACTIVATE_EVENT_NAME);
    }

    public void AddPostCleanHandler(string fileEnding, EventHandlerPostGenricClean handler)
    {
      this.AddHandler(fileEnding, (Delegate) handler, this.POST_CLEAN_EVENT_NAME);
    }

    public void AddPostCleanHandler(string fileEnding, EventHandlerPostGenricClean2 handler)
    {
      this.AddHandler(fileEnding, (Delegate) handler, this.POST_CLEAN_EVENT_NAME);
    }

    public void AddPostCheckHandler(string fileEnding, EventHandlerPostGenricCheck handler)
    {
      this.AddHandler(fileEnding, (Delegate) handler, this.POST_CHECK_EVENT_NAME);
    }

    public void AddPreActivateHandler(string fileEnding, EventHandlerPreGenericActivate handler)
    {
      this.AddHandler(fileEnding, (Delegate) handler, this.PRE_ACTIVATE_EVENT_NAME);
    }

    public void AddPreCheckHandler(string fileEnding, EventHandlerPreGenericCheck handler)
    {
      this.AddHandler(fileEnding, (Delegate) handler, this.PRE_CHECK_EVENT_NAME);
    }

    private void RemoveHandler(Delegate handler, string eventHandlerType)
    {
      if ((object) handler == null)
        return;
      lock (this.eventTable)
      {
        List<string> stringList = new List<string>();
        foreach (string key in this.eventTable.Keys)
          stringList.Add(key);
        foreach (string index in stringList)
        {
          if (index.EndsWith(eventHandlerType) && (object) this.eventTable[index] != null)
            this.eventTable[index] = Delegate.Remove(this.eventTable[index], handler);
        }
      }
    }

    public void RemoveHandler(EventHandlerOnFileSave handler)
    {
      this.RemoveHandler((Delegate) handler, this.SAVE_EVENT_NAME);
    }

    public void RemoveHandler(EventHandlerOnFileDeletion handler)
    {
      this.RemoveHandler((Delegate) handler, this.DELETION_EVENT_NAME);
    }

    public void RemovePostActivateHandler(EventHandlerPostGenricActivate handler)
    {
      this.RemoveHandler((Delegate) handler, this.POST_ACTIVATE_EVENT_NAME);
    }

    public void RemovePostActivateHandler(EventHandlerPostGenricActivate2 handler)
    {
      this.RemoveHandler((Delegate) handler, this.POST_ACTIVATE_EVENT_NAME);
    }

    public void RemovePostCleanHandler(EventHandlerPostGenricClean handler)
    {
      this.RemoveHandler((Delegate) handler, this.POST_CLEAN_EVENT_NAME);
    }

    public void RemovePostCleanHandler(EventHandlerPostGenricClean2 handler)
    {
      this.RemoveHandler((Delegate) handler, this.POST_CLEAN_EVENT_NAME);
    }

    public void RemovePostCheckHandler(EventHandlerPostGenricCheck handler)
    {
      this.RemoveHandler((Delegate) handler, this.POST_CHECK_EVENT_NAME);
    }

    public void RemovePreActivateHandler(EventHandlerPreGenericActivate handler)
    {
      this.RemoveHandler((Delegate) handler, this.PRE_ACTIVATE_EVENT_NAME);
    }

    public void RemovePreCheckHandler(EventHandlerPreGenericCheck handler)
    {
      this.RemoveHandler((Delegate) handler, this.PRE_CHECK_EVENT_NAME);
    }

    public void RaiseFileSaveEvent(string filePath, string[] dependentFilePaths, string content, ProjectProperties properties)
    {
      string fileEndingFor = FileHandlerRegistry.GetFileEndingFor(filePath);
      if (fileEndingFor == null)
        return;
      string key = fileEndingFor + this.SAVE_EVENT_NAME;
      if (!this.eventTable.ContainsKey(key))
        return;
      EventHandlerOnFileSave handlerOnFileSave;
      lock (this.eventTable)
        handlerOnFileSave = (EventHandlerOnFileSave) this.eventTable[key];
      if (handlerOnFileSave == null)
        return;
      handlerOnFileSave(filePath, dependentFilePaths, content, properties);
    }

    public void RaisePreFileDeletionEvent(string filePath)
    {
      string fileEndingFor = FileHandlerRegistry.GetFileEndingFor(filePath);
      if (fileEndingFor == null)
        return;
      string key = fileEndingFor + this.PRE_DELETION_EVENT_NAME;
      if (!this.eventTable.ContainsKey(key))
        return;
      EventHandlerOnPreFileDeletion onPreFileDeletion;
      lock (this.eventTable)
        onPreFileDeletion = (EventHandlerOnPreFileDeletion) this.eventTable[key];
      if (onPreFileDeletion == null)
        return;
      onPreFileDeletion(filePath);
    }

    public void RaiseFileDeletionEvent(string filePath)
    {
      string fileEndingFor = FileHandlerRegistry.GetFileEndingFor(filePath);
      if (fileEndingFor == null)
        return;
      string key = fileEndingFor + this.DELETION_EVENT_NAME;
      if (!this.eventTable.ContainsKey(key))
        return;
      EventHandlerOnFileDeletion handlerOnFileDeletion;
      lock (this.eventTable)
        handlerOnFileDeletion = (EventHandlerOnFileDeletion) this.eventTable[key];
      if (handlerOnFileDeletion == null)
        return;
      handlerOnFileDeletion(filePath);
    }

    public void RaisePostGenericActivateEvent(string filePath, bool activateSuccessful)
    {
      string fileEndingFor = FileHandlerRegistry.GetFileEndingFor(filePath);
      if (fileEndingFor == null)
        return;
      string key = fileEndingFor + this.POST_ACTIVATE_EVENT_NAME;
      if (!this.eventTable.ContainsKey(key))
        return;
      Delegate @delegate;
      lock (this.eventTable)
        @delegate = this.eventTable[key];
      if ((object) @delegate == null)
        return;
      foreach (Delegate invocation in @delegate.GetInvocationList())
      {
        if (invocation is EventHandlerPostGenricActivate)
          ((EventHandlerPostGenricActivate) invocation)(filePath);
        else if (invocation is EventHandlerPostGenricActivate2)
          ((EventHandlerPostGenricActivate2) invocation)(filePath, activateSuccessful);
      }
    }

    public void RaisePostGenericCleanEvent(string filePath, bool cleanSuccessful)
    {
      string fileEndingFor = FileHandlerRegistry.GetFileEndingFor(filePath);
      if (fileEndingFor == null)
        return;
      string key = fileEndingFor + this.POST_CLEAN_EVENT_NAME;
      if (!this.eventTable.ContainsKey(key))
        return;
      Delegate @delegate;
      lock (this.eventTable)
        @delegate = this.eventTable[key];
      if ((object) @delegate == null)
        return;
      foreach (Delegate invocation in @delegate.GetInvocationList())
      {
        if (invocation is EventHandlerPostGenricClean)
          ((EventHandlerPostGenricClean) invocation)(filePath);
        else if (invocation is EventHandlerPostGenricClean2)
          ((EventHandlerPostGenricClean2) invocation)(filePath, cleanSuccessful);
      }
    }

    public void RaisePostGenericCheckEvent(string filePath, bool checkSuccessful)
    {
      string fileEndingFor = FileHandlerRegistry.GetFileEndingFor(filePath);
      if (fileEndingFor == null)
        return;
      string key = fileEndingFor + this.POST_CHECK_EVENT_NAME;
      if (!this.eventTable.ContainsKey(key))
        return;
      Delegate @delegate;
      lock (this.eventTable)
        @delegate = this.eventTable[key];
      if (!(@delegate is EventHandlerPostGenricCheck))
        return;
      ((EventHandlerPostGenricCheck) @delegate)(filePath, checkSuccessful);
    }

    public bool RaisePreActivateEvent(string filePath)
    {
      bool overallStatus = true;
      string fileEndingFor = FileHandlerRegistry.GetFileEndingFor(filePath);
      if (fileEndingFor == null)
        return overallStatus;
      string key = fileEndingFor + this.PRE_ACTIVATE_EVENT_NAME;
      if (this.eventTable.ContainsKey(key))
      {
        EventHandlerPreGenericActivate preGenericActivate;
        lock (this.eventTable)
          preGenericActivate = (EventHandlerPreGenericActivate) this.eventTable[key];
        if (preGenericActivate != null)
        {
          foreach (EventHandlerPreGenericActivate invocation in preGenericActivate.GetInvocationList())
          {
            bool myStatus;
            invocation(filePath, overallStatus, out myStatus);
            if (!myStatus)
              overallStatus = false;
          }
        }
      }
      return overallStatus;
    }

    public bool RaisePreCheckEvent(string filePath)
    {
      bool overallStatus = true;
      string fileEndingFor = FileHandlerRegistry.GetFileEndingFor(filePath);
      if (fileEndingFor == null)
        return overallStatus;
      string key = fileEndingFor + this.PRE_CHECK_EVENT_NAME;
      if (this.eventTable.ContainsKey(key))
      {
        EventHandlerPreGenericCheck handlerPreGenericCheck;
        lock (this.eventTable)
          handlerPreGenericCheck = (EventHandlerPreGenericCheck) this.eventTable[key];
        if (handlerPreGenericCheck != null)
        {
          foreach (EventHandlerPreGenericCheck invocation in handlerPreGenericCheck.GetInvocationList())
          {
            bool myStatus;
            invocation(filePath, overallStatus, out myStatus);
            if (!myStatus)
              overallStatus = false;
          }
        }
      }
      return overallStatus;
    }

    public bool CanHandle(string filepath, Type delegationType)
    {
      bool flag = false;
      lock (this.eventTable)
      {
        string key = FileHandlerRegistry.GetFileEndingFor(filepath) + delegationType.Name;
        if (this.eventTable.ContainsKey(key))
        {
          Delegate @delegate = (Delegate) null;
          this.eventTable.TryGetValue(key, out @delegate);
          if ((object) @delegate != null)
            flag = true;
        }
      }
      return flag;
    }

    public bool CanHandleSave(string silentSaveAsName)
    {
      return this.CanHandle(silentSaveAsName, typeof (EventHandlerOnFileSave));
    }

    public bool CanHandlePostActivate(string filepath)
    {
      return this.CanHandle(filepath, typeof (EventHandlerPostGenricActivate));
    }

    public bool CanHandlePostClean(string filepath)
    {
      return this.CanHandle(filepath, typeof (EventHandlerPostGenricClean));
    }

    public bool CanHandlePostCheck(string filepath)
    {
      return this.CanHandle(filepath, typeof (EventHandlerPostGenricCheck));
    }

    public bool CanHandlePreActivate(string filepath)
    {
      return this.CanHandle(filepath, typeof (EventHandlerPreGenericActivate));
    }

    public bool CanHandlePreCheck(string filepath)
    {
      return this.CanHandle(filepath, typeof (EventHandlerPreGenericCheck));
    }

    public static string GetFileEndingFor(string fileName)
    {
      if (string.IsNullOrEmpty(fileName) || !fileName.Contains<char>('.'))
        return (string) null;
      int startIndex = fileName.LastIndexOf('.') + 1;
      if (fileName.Length <= startIndex)
        return (string) null;
      return fileName.Substring(startIndex);
    }

    public bool RaiseGenericEvent(string fileName, FileEventType eventType, bool overallStatus = true, bool descendingPriority = false, bool deltaMode = false, bool migrationMode = false)
    {
      if (string.IsNullOrEmpty(fileName))
        throw new ArgumentException("fileName must not be bull or empty");
      if (eventType.IsEventTypeMask())
        throw new ArgumentException("eventType must not be an event mask");
      foreach (Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData> lazy in !descendingPriority ? (IEnumerable<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>>) this.EventTable.Where<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>>((Func<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>, bool>) (hdl => hdl.Metadata.EventTypeMask.Match(eventType))).OrderBy<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>, int>((Func<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>, int>) (hdl => hdl.Metadata.Priority)) : (IEnumerable<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>>) this.EventTable.Where<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>>((Func<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>, bool>) (hdl => hdl.Metadata.EventTypeMask.Match(eventType))).OrderByDescending<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>, int>((Func<Lazy<GenericFileEventHandler, IFileEvenHandlertMetaData>, int>) (hdl => hdl.Metadata.Priority)))
      {
        GenericFileEventHandler fileEventHandler;
        try
        {
          fileEventHandler = lazy.Value;
        }
        catch (CompositionContractMismatchException ex)
        {
          Trace.WriteLine(string.Format("Error: invalid file handler registered {0}", (object) ex.Message), nameof (FileHandlerRegistry));
          continue;
        }
        try
        {
          if (!fileEventHandler(fileName, new GenericFileEventArgs(overallStatus, deltaMode, migrationMode)))
            overallStatus = false;
        }
        catch (InvalidOperationException ex)
        {
        }
      }
      return overallStatus;
    }
  }
}
