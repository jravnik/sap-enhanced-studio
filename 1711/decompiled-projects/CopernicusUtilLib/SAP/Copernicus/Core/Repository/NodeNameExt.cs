﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeNameExt
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class NodeNameExt
  {
    public string Name { get; set; }

    public string ProxyName { get; set; }

    public string ParentProxyName { get; set; }

    public string ReleasedNode { get; set; }

    public string DisabledNode { get; set; }

    public string BoProxyName { get; set; }

    public string OffEnabled { get; set; }

    public NodeNameExt(string name, string proxyName, string parentProxyName, string boProxyName, string nodeReleased, string nodeDisabled, string offEnabled)
    {
      this.Name = name;
      this.ProxyName = proxyName;
      this.ParentProxyName = parentProxyName;
      this.ReleasedNode = nodeReleased;
      this.DisabledNode = nodeDisabled;
      this.BoProxyName = boProxyName;
      this.OffEnabled = offEnabled;
    }
  }
}
