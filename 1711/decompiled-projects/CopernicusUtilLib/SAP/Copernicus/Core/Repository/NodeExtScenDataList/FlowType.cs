﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeExtScenDataList.FlowType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.NodeExtScenDataList
{
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/NodeExtensionScenarioList")]
  [DebuggerStepThrough]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [Serializable]
  public class FlowType
  {
    private string bo_connection_orderField;
    private string bo_connection_descriptionField;
    private string source_bo_nameField;
    private string source_bo_node_nameField;
    private string is_source_hiddenField;
    private string target_bo_nameField;
    private string target_bo_node_nameField;
    private string is_target_hiddenField;
    private ReferenceFieldType[] reference_field_keysField;
    private string message_typeField;
    private string service_prxField;
    private string operation_prxField;
    private string direction_Field;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string bo_connection_order
    {
      get
      {
        return this.bo_connection_orderField;
      }
      set
      {
        this.bo_connection_orderField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string bo_connection_description
    {
      get
      {
        return this.bo_connection_descriptionField;
      }
      set
      {
        this.bo_connection_descriptionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string source_bo_name
    {
      get
      {
        return this.source_bo_nameField;
      }
      set
      {
        this.source_bo_nameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string source_bo_node_name
    {
      get
      {
        return this.source_bo_node_nameField;
      }
      set
      {
        this.source_bo_node_nameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string is_source_hidden
    {
      get
      {
        return this.is_source_hiddenField;
      }
      set
      {
        this.is_source_hiddenField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string target_bo_name
    {
      get
      {
        return this.target_bo_nameField;
      }
      set
      {
        this.target_bo_nameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string target_bo_node_name
    {
      get
      {
        return this.target_bo_node_nameField;
      }
      set
      {
        this.target_bo_node_nameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string is_target_hidden
    {
      get
      {
        return this.is_target_hiddenField;
      }
      set
      {
        this.is_target_hiddenField = value;
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified)]
    [XmlArrayItem("reference_field_key", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
    public ReferenceFieldType[] reference_field_keys
    {
      get
      {
        return this.reference_field_keysField;
      }
      set
      {
        this.reference_field_keysField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string message_type
    {
      get
      {
        return this.message_typeField;
      }
      set
      {
        this.message_typeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string service_prx
    {
      get
      {
        return this.service_prxField;
      }
      set
      {
        this.service_prxField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string operation_prx
    {
      get
      {
        return this.operation_prxField;
      }
      set
      {
        this.operation_prxField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string direction
    {
      get
      {
        return this.direction_Field;
      }
      set
      {
        this.direction_Field = value;
      }
    }
  }
}
