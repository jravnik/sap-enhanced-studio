﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.UserInterface.BOSearchForm
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Repository.UserInterface
{
  public class BOSearchForm : Form
  {
    private Button buttonCancel;
    private Button buttonOK;
    private BOSearchControl searchControl;
    private List<RepositoryDataSet.BusinessObjectsRow> selectedBORows;

    public event BOSearchForm.BusinessObjectSelectionChangedEventHandler BusinessObjectSelectionChangedEvent;

    internal void FireBusinessObjectSelectionChangedEvent(List<RepositoryDataSet.BusinessObjectsRow> selectedRows)
    {
      if (this.BusinessObjectSelectionChangedEvent == null)
        return;
      this.BusinessObjectSelectionChangedEvent((object) this, selectedRows);
    }

    public void overrideNameLabel(string name)
    {
      this.searchControl.overrideNameLabel(name);
    }

    public List<RepositoryDataSet.BusinessObjectsRow> SelectedBusinessObjectRows
    {
      get
      {
        return this.selectedBORows;
      }
    }

    public BOSearchForm()
    {
      this.InitializeComponent();
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (BOSearchForm));
      this.searchControl = new BOSearchControl();
      this.buttonCancel = new Button();
      this.buttonOK = new Button();
      this.SuspendLayout();
      this.searchControl.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.searchControl.Location = new Point(1, 12);
      this.searchControl.Name = "searchControl";
      this.searchControl.Size = new Size(477, 443);
      this.searchControl.TabIndex = 0;
      this.searchControl.BusinessObjectSelectionChangedEvent += new BOSearchControl.BusinessObjectSelectionChangedEventHandler(this.searchControl_BOChanged);
      this.buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonCancel.Location = new Point(393, 459);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new Size(75, 23);
      this.buttonCancel.TabIndex = 1;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
      this.buttonOK.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonOK.Location = new Point(312, 459);
      this.buttonOK.Name = "buttonOK";
      this.buttonOK.Size = new Size(75, 23);
      this.buttonOK.TabIndex = 1;
      this.buttonOK.Text = "OK";
      this.buttonOK.UseVisualStyleBackColor = true;
      this.buttonOK.Click += new EventHandler(this.buttonOK_Click);
      this.ClientSize = new Size(480, 494);
      this.Controls.Add((Control) this.buttonOK);
      this.Controls.Add((Control) this.buttonCancel);
      this.Controls.Add((Control) this.searchControl);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (BOSearchForm);
      this.Text = "Search for Business Object";
      this.ResumeLayout(false);
    }

    public void useNamespace(bool show, bool changeable, string preset)
    {
      this.searchControl.useNamespace(show, changeable, preset);
    }

    public void useBOName(bool show, bool changeable, string preset)
    {
      this.searchControl.useBOName(show, changeable, preset);
    }

    public void useBOProxyName(bool show, bool changeable, string preset)
    {
      this.searchControl.useBOProxyName(show, changeable, preset);
    }

    public void useCategory(bool show, bool changeable, BusinessObjectCategory preset)
    {
      this.searchControl.useCategory(show, changeable, preset);
    }

    public void useTechCategory(bool show, bool changeable, BusinessObjectTechnicalCategory preset)
    {
      this.searchControl.useTechCategory(show, changeable, preset);
    }

    public void useDeploymentUnit(bool show, bool changeable, string preset)
    {
      this.searchControl.useDeploymentUnit(show, changeable, preset);
    }

    public void useWriteAccess(bool use, bool show, bool changeable, bool preset)
    {
      this.searchControl.useWriteAccess(use, show, changeable, preset);
    }

    public void useOnlySAPNameSpaces(bool show, bool changeable, bool preset)
    {
      this.searchControl.useOnlySAPNameSpaces(show, changeable, preset);
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
      this.selectedBORows = (List<RepositoryDataSet.BusinessObjectsRow>) null;
      this.Dispose();
    }

    private void buttonOK_Click(object sender, EventArgs e)
    {
      this.Dispose();
    }

    private void searchControl_BOChanged(object sender, List<RepositoryDataSet.BusinessObjectsRow> selectedBOs)
    {
      if (selectedBOs != null && selectedBOs.Count > 0)
      {
        this.selectedBORows = selectedBOs;
        this.buttonOK.Enabled = true;
      }
      else
      {
        this.selectedBORows = (List<RepositoryDataSet.BusinessObjectsRow>) null;
        this.buttonOK.Enabled = false;
      }
      this.FireBusinessObjectSelectionChangedEvent(selectedBOs);
    }

    public delegate void BusinessObjectSelectionChangedEventHandler(object sender, List<RepositoryDataSet.BusinessObjectsRow> selectedRows);
  }
}
