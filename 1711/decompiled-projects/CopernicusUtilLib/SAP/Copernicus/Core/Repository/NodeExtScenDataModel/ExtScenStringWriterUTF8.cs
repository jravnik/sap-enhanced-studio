﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeExtScenDataModel.ExtScenStringWriterUTF8
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.IO;
using System.Text;

namespace SAP.Copernicus.Core.Repository.NodeExtScenDataModel
{
  public class ExtScenStringWriterUTF8 : StringWriter
  {
    public override Encoding Encoding
    {
      get
      {
        return Encoding.UTF8;
      }
    }
  }
}
