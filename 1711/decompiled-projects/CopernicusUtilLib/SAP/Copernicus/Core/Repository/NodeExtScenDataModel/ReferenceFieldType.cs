﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeExtScenDataModel.ReferenceFieldType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.NodeExtScenDataModel
{
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/NodeExtensionScenarioDefinition")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [Serializable]
  public class ReferenceFieldType
  {
    private string reference_field_bundle_keyField;
    private string reference_field_nameField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string reference_field_bundle_key
    {
      get
      {
        return this.reference_field_bundle_keyField;
      }
      set
      {
        this.reference_field_bundle_keyField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string reference_field_name
    {
      get
      {
        return this.reference_field_nameField;
      }
      set
      {
        this.reference_field_nameField = value;
      }
    }
  }
}
