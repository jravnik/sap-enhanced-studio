﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeExtScenDataModel.ExtensionScenarioTypeEqualityComparer
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Repository.NodeExtScenDataModel
{
  public class ExtensionScenarioTypeEqualityComparer : IEqualityComparer<ExtensionScenarioType>
  {
    public bool Equals(ExtensionScenarioType s1, ExtensionScenarioType s2)
    {
      return s1.scenario_name == s2.scenario_name;
    }

    public int GetHashCode(ExtensionScenarioType s)
    {
      return s.scenario_name.GetHashCode();
    }
  }
}
