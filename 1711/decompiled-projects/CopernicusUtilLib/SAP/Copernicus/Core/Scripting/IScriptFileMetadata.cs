﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Scripting.IScriptFileMetadata
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Scripting
{
  public interface IScriptFileMetadata
  {
    bool OfflineOnlineEnabled { get; set; }

    bool OnlineFileExists { get; set; }

    bool CreateOnlineFile { get; set; }

    SnippetType? Type { get; }

    SnippetSubType? SubType { get; }

    string ProjectNamespace { set; get; }

    string CustomFileName { set; get; }

    bool MassProcessing { set; get; }

    bool Offline { set; get; }

    void SetMassProcessing(bool massProc);

    void SetOffline(bool offline);

    string BOName { set; get; }

    string NodeName { set; get; }

    string Namespace { set; get; }

    string ActionName { get; }

    string PIName { set; get; }

    bool IsExtensibility { set; get; }

    string ExtBOName { set; get; }

    string FMTName { set; get; }

    string FMTNameSpace { set; get; }

    string FMTRootElementName { set; get; }

    string FMTTypingDataType { set; get; }

    string FMTType { set; get; }

    string RuntimeNamespacePrefix { set; get; }

    string AppExitName { set; get; }

    string AppExitESName { set; get; }

    string AppExitMethodName { set; get; }

    string AppExitBadiProxyName { set; get; }

    string AppExitInputParameterDataType { set; get; }

    string AppExitOutputParameterDataType { set; get; }

    string CRLLibraryName { set; get; }

    string CRLFunctionName { set; get; }

    string CRLFunctionTemplateAdditionalImport { set; get; }

    string CRLFunctionTemplateReturnStatement { set; get; }

    string GetCustomValue(string key);

    void SetCustomValue(string key, string value);

    void SetBOContext(string boName, string nodeName, string ns);

    void SetXBOContext(string extBOName, string extNodeName, string projectNS);

    void SetSnippetType(SnippetType type, SnippetSubType subType);

    void SetActionValidationType(string actionName);

    void SetActionType(string actionName);

    void SetPIType(string PIName, SnippetSubType subType, string projectNS);

    void SetFMTType(string fmtName, string fmtNS, string fmtDataType, string runtimeNamespacePrefix);

    void SetAppExitType(string projectNS, string exitName, string exitESName, string exitMethodName, string badiProxyName, string inputParamDTName, string outputParamDTName, string deploymentUnit);

    void ParseFromDict(IDictionary<string, string> attribDict);

    Dictionary<string, string> GetAsDict();

    void Retrieve(string fullPath);

    string GetProjectNamespace();

    string RenderFileName(string customBOName = null, bool offline = false);

    string RenderXRepPath(bool deprecated = false);
  }
}
