﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Scripting.SFMKey
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Scripting
{
  public enum SFMKey
  {
    BOName,
    NodeName,
    Namespace,
    SnippetType,
    SnippetSubType,
    ActionName,
    PIName,
    CustomFileName,
    IsExt,
    ExtBOName,
    ProjectNamespace,
    FMTName,
    FMTNamespace,
    FMTNodeName,
    FMTTypingDataType,
    FMTType,
    FMTRootElementName,
    MassProcessing,
    RuntimeNamespacePrefix,
    AppExitName,
    AppExitProxyName,
    AppExitMethodName,
    AppExitBAdIProxyName,
    AppExitInputParameterDataType,
    AppExitOutputParameterDataType,
    DeploymentUnit,
    AppExitESName,
    CRLLibraryName,
    CRLFunctionName,
    CRLTemplateHash,
    Offline,
  }
}
