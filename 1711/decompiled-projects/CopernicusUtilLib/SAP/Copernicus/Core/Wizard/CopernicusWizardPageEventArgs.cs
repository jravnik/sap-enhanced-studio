﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Wizard.CopernicusWizardPageEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;

namespace SAP.Copernicus.Core.Wizard
{
  public class CopernicusWizardPageEventArgs : CancelEventArgs
  {
    private string _newPage;

    public string NewPage
    {
      get
      {
        return this._newPage;
      }
      set
      {
        this._newPage = value;
      }
    }
  }
}
