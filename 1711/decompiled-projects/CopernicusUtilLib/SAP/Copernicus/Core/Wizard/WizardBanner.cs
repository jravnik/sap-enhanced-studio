﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Wizard.WizardBanner
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Wizard
{
  public class WizardBanner : UserControl
  {
    protected Label titleLabel;
    protected Label subtitleLabel;
    private CopernicusEtchedLine etchedLine1;
    private ToolTip toolTip1;
    private Label HelpIcon;
    private IContainer components;

    public event WizardBanner.HelpRequestedEventHandler HelpRequested;

    protected virtual void RaiseHelpQuested()
    {
      if (this.HelpRequested == null)
        return;
      this.HelpRequested((object) this);
    }

    public WizardBanner()
    {
      this.InitializeComponent();
      this.SetStyle(ControlStyles.Selectable, false);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.titleLabel = new Label();
      this.subtitleLabel = new Label();
      this.HelpIcon = new Label();
      this.etchedLine1 = new CopernicusEtchedLine();
      this.toolTip1 = new ToolTip(this.components);
      this.SuspendLayout();
      this.titleLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.titleLabel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.titleLabel.Location = new Point(16, 8);
      this.titleLabel.Name = "titleLabel";
      this.titleLabel.Size = new Size(481, 16);
      this.titleLabel.TabIndex = 0;
      this.titleLabel.Text = "Title";
      this.subtitleLabel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.subtitleLabel.Location = new Point(40, 24);
      this.subtitleLabel.Name = "subtitleLabel";
      this.subtitleLabel.Size = new Size(468, 32);
      this.subtitleLabel.TabIndex = 1;
      this.subtitleLabel.Text = "Subtitle";
      this.HelpIcon.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.HelpIcon.Image = (Image) ActionIcons.Help;
      this.HelpIcon.Location = new Point(514, 8);
      this.HelpIcon.Name = "HelpIcon";
      this.HelpIcon.Size = new Size(24, 24);
      this.HelpIcon.TabIndex = 2;
      this.toolTip1.SetToolTip((Control) this.HelpIcon, "Show Help");
      this.HelpIcon.Click += new EventHandler(this.OnHelpRequested);
      this.etchedLine1.AutoScroll = true;
      this.etchedLine1.AutoSize = true;
      this.etchedLine1.Dock = DockStyle.Bottom;
      this.etchedLine1.Edge = EtchEdge.Bottom;
      this.etchedLine1.Location = new Point(0, 56);
      this.etchedLine1.Name = "etchedLine1";
      this.etchedLine1.Size = new Size(456, 8);
      this.etchedLine1.TabIndex = 0;
      this.BackColor = SystemColors.Window;
      this.Controls.Add((Control) this.HelpIcon);
      this.Controls.Add((Control) this.subtitleLabel);
      this.Controls.Add((Control) this.titleLabel);
      this.Name = nameof (WizardBanner);
      this.Size = new Size(541, 64);
      this.ResumeLayout(false);
    }

    [Category("Appearance")]
    public string Title
    {
      get
      {
        return this.titleLabel.Text;
      }
      set
      {
        this.titleLabel.Text = value;
      }
    }

    [Category("Appearance")]
    public string Subtitle
    {
      get
      {
        return this.subtitleLabel.Text;
      }
      set
      {
        this.subtitleLabel.Text = value;
      }
    }

    private void OnHelpRequested(object sender, EventArgs e)
    {
      this.RaiseHelpQuested();
    }

    public delegate void HelpRequestedEventHandler(object sender);
  }
}
