﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.XMLUtil
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.IO;
using System.Xml;

namespace SAP.Copernicus.Core.Util
{
  public class XMLUtil
  {
    private XMLUtil()
    {
    }

    public static string prettyPrint(string xml)
    {
      XmlDocument xmlDocument = new XmlDocument();
      if (string.IsNullOrEmpty(xml))
        return xml;
      xmlDocument.LoadXml(xml);
      XmlNodeReader xmlNodeReader = new XmlNodeReader((XmlNode) xmlDocument);
      StringWriter stringWriter = new StringWriter();
      new XmlTextWriter((TextWriter) stringWriter)
      {
        Formatting = Formatting.Indented,
        Indentation = 1,
        IndentChar = '\t'
      }.WriteNode((XmlReader) xmlNodeReader, true);
      return stringWriter.ToString();
    }
  }
}
