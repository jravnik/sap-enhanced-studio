﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.AssemblyTimestamp
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;

namespace SAP.Copernicus.Core.Util
{
  [AttributeUsage(AttributeTargets.Assembly)]
  public class AssemblyTimestamp : Attribute
  {
    private string _timeStamp = "dev";

    public string TimeStamp
    {
      get
      {
        return this._timeStamp;
      }
    }

    public AssemblyTimestamp(string timeStamp)
    {
      this._timeStamp = timeStamp;
    }
  }
}
