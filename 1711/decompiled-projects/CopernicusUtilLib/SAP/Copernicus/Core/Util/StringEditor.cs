﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.StringEditor
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAP.Copernicus.Core.Util
{
  public static class StringEditor
  {
    public static TextSpan GetTextSpan(string Code)
    {
      return new TextSpan()
      {
        iStartLine = 0,
        iStartIndex = 0,
        iEndLine = Code.Count<char>((Func<char, bool>) (x => (int) x == 10)),
        iEndIndex = Code.Length - Code.LastIndexOf('\n')
      };
    }

    public static string ApplyEdits(string Code, IList<EditSpan> EditSpans)
    {
      if (EditSpans == null || EditSpans.Count == 0)
        return Code;
      EditSpan[] array = EditSpans.OrderBy<EditSpan, int>((Func<EditSpan, int>) (esp => esp.Span.iStartLine)).ThenBy<EditSpan, int>((Func<EditSpan, int>) (esp => esp.Span.iEndLine)).ThenBy<EditSpan, int>((Func<EditSpan, int>) (esp => esp.Span.iEndIndex)).ToArray<EditSpan>();
      if (array.Length > 1)
      {
        for (int index = array.Length - 2; index >= 0; --index)
        {
          TextSpan span1 = array[index].Span;
          TextSpan span2 = array[index + 1].Span;
          if (span1.iEndLine > span2.iStartLine)
            throw new Exception();
          if (span1.iEndLine == span2.iStartLine && span1.iEndIndex > span2.iStartIndex)
            throw new Exception();
        }
      }
      StringBuilder stringBuilder = new StringBuilder(Code);
      for (int index = array.Length - 1; index >= 0; --index)
      {
        EditSpan editSpan = array[index];
        Code = Code.Substring(0, editSpan.Span.iStartIndex) + editSpan.Text + Code.Substring(editSpan.Span.iEndIndex);
      }
      return Code;
    }
  }
}
