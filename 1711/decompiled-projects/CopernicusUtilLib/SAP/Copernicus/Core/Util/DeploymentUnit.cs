﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.DeploymentUnit
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.ResourceAccess.TestMocks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.Core.Util
{
  public class DeploymentUnit
  {
    protected readonly string ProxyName;
    protected readonly string Name;
    protected readonly string SemanticalName;
    protected readonly string PSMReleaseStatus;
    private static DeploymentUnit[] DeploymentUnits;

    public static DeploymentUnit[] Values
    {
      get
      {
        if (DeploymentUnit.DeploymentUnits == null)
          DeploymentUnit.DeploymentUnits = new RepositoryQueryHandler().Query_DeploymentUnits();
        return DeploymentUnit.DeploymentUnits;
      }
    }

    public static string[] DeploymentUnitNames
    {
      get
      {
        if (TestController.TestMode)
          return DeploymentUnitMock.DeploymentUnitNames;
        string[] strArray = new string[DeploymentUnit.Values.Length];
        for (int index = 0; index < DeploymentUnit.Values.Length; ++index)
          strArray[index] = DeploymentUnit.Values[index].Name;
        return strArray;
      }
    }

    public static string[] DeploymentUnitProxyNames
    {
      get
      {
        if (TestController.TestMode)
          return DeploymentUnitMock.DeploymentUnitProxyNames;
        string[] strArray = new string[DeploymentUnit.Values.Length];
        for (int index = 0; index < DeploymentUnit.Values.Length; ++index)
          strArray[index] = DeploymentUnit.Values[index].ProxyName;
        return strArray;
      }
    }

    public DeploymentUnit(string proxyName, string name, string semanticalName, string psmReleaseStatus)
    {
      this.ProxyName = proxyName;
      this.Name = name;
      this.SemanticalName = semanticalName;
      this.PSMReleaseStatus = psmReleaseStatus;
    }

    public static bool IsValidDeploymentUnit(string MDRSNameOfDU)
    {
      if (TestController.TestMode)
        return DeploymentUnitMock.IsValidDeploymentUnit(MDRSNameOfDU);
      return ((IEnumerable<DeploymentUnit>) DeploymentUnit.Values).First<DeploymentUnit>((Func<DeploymentUnit, bool>) (e => e.Name == MDRSNameOfDU)) != null;
    }

    public static string DUProxyNameForName(string MDRSNameOfDU)
    {
      if (TestController.TestMode)
        return DeploymentUnitMock.DUProxyNameForName(MDRSNameOfDU);
      try
      {
        return ((IEnumerable<DeploymentUnit>) DeploymentUnit.Values).First<DeploymentUnit>((Func<DeploymentUnit, bool>) (e => e.Name == MDRSNameOfDU)).ProxyName;
      }
      catch
      {
        return (string) null;
      }
    }

    public static DeploymentUnit fromDUProxyName(string DUProxyName)
    {
      if (DUProxyName.Equals(""))
        return (DeploymentUnit) null;
      if (TestController.TestMode)
        return DeploymentUnitMock.fromDUProxyName(DUProxyName);
      return ((IEnumerable<DeploymentUnit>) DeploymentUnit.Values).First<DeploymentUnit>((Func<DeploymentUnit, bool>) (e => e.ProxyName == DUProxyName));
    }

    public override string ToString()
    {
      return this.SemanticalName;
    }

    public static string GetSematicNameFromESRName(string MDRSNameOfDU)
    {
      if (TestController.TestMode)
        return DeploymentUnitMock.GetSematicNameFromESRName(MDRSNameOfDU);
      return ((IEnumerable<DeploymentUnit>) DeploymentUnit.Values).First<DeploymentUnit>((Func<DeploymentUnit, bool>) (e => e.Name == MDRSNameOfDU)).SemanticalName;
    }

    public static DeploymentUnit fromString(string str)
    {
      if (TestController.TestMode)
        return DeploymentUnitMock.fromString(str);
      if (str.Length == 0)
        return (DeploymentUnit) null;
      return ((IEnumerable<DeploymentUnit>) DeploymentUnit.Values).First<DeploymentUnit>((Func<DeploymentUnit, bool>) (e => e.SemanticalName == str));
    }

    public string GetDeploymentUnitProxyName()
    {
      return this.ProxyName;
    }

    public string GetDeploymentUnitName()
    {
      return this.Name;
    }

    public string GetDeploymentUnitSemanticalName()
    {
      return this.SemanticalName;
    }
  }
}
