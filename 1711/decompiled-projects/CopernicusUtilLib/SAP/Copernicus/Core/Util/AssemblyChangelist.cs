﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.AssemblyChangelist
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;

namespace SAP.Copernicus.Core.Util
{
  [AttributeUsage(AttributeTargets.Assembly)]
  public class AssemblyChangelist : Attribute
  {
    private string _changeList = string.Empty;

    public string ChangeList
    {
      get
      {
        return this._changeList;
      }
    }

    public AssemblyChangelist(string changeList)
    {
      this._changeList = changeList;
    }
  }
}
