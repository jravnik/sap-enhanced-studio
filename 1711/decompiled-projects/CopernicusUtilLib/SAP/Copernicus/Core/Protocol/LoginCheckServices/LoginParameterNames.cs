﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginParameterNames
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public static class LoginParameterNames
  {
    public const string DELETE_SESSION = "delete_session";
    public const string SAP_ALIAS = "sap-alias";
    public const string SAP_USER = "sap-user";
    public const string SAP_PASSWORD = "sap-password";
    public const string SYS_LOGIN_PASSWORD = "sap-system-login-password";
    public const string SYS_LOGIN_PASSWORD_NEW = "sap-system-login-passwordnew";
    public const string SYS_LOGIN_PASSWORD_REPEAT = "sap-system-login-passwordrepeat";
    public const string SYS_LOGIN_INPUT_PROCESSING = "sap-system-login-oninputprocessing";
  }
}
