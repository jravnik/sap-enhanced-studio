﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.ILoginResult
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public interface ILoginResult
  {
    List<ILoginMessage> Messages { get; }

    LoginState State { get; }

    bool CertificateSupport { get; }

    IDictionary<LoginActionType, ILoginAction> Actions { get; }

    IDictionary<string, ILoginParameter> Parameters { get; }

    List<ILoginSession> Sessions { get; }
  }
}
