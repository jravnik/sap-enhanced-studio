﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginSession
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public class LoginSession : ILoginSession
  {
    private readonly string _client;
    private readonly string _userId;
    private readonly string _terminal;
    private readonly string _time;

    internal LoginSession(string client, string userId, string terminal, string time)
    {
      this._client = client;
      this._userId = userId;
      this._terminal = terminal;
      this._time = time;
    }

    public string Client
    {
      get
      {
        return this._client;
      }
    }

    public string UserId
    {
      get
      {
        return this._userId;
      }
    }

    public string Terminal
    {
      get
      {
        return this._terminal;
      }
    }

    public string Time
    {
      get
      {
        return this._time;
      }
    }
  }
}
