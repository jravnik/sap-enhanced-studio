﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginParameterValue
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  internal sealed class LoginParameterValue : ILoginSelectableValue
  {
    private readonly string _key;
    private readonly string _display;
    private volatile string _toString;

    internal LoginParameterValue(string key)
      : this(key, (string) null)
    {
    }

    internal LoginParameterValue(string key, string display)
    {
      this._key = key;
      this._display = display;
    }

    public string Key
    {
      get
      {
        return this._key;
      }
    }

    public string Display
    {
      get
      {
        return this._display;
      }
    }

    public override string ToString()
    {
      if (this._toString == null)
        this._toString = this._key + ", " + this._display;
      return this._toString;
    }
  }
}
