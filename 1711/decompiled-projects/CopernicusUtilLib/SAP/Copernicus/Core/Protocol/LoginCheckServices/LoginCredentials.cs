﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginCredentials
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public class LoginCredentials
  {
    private readonly string _username;
    private readonly string _password;
    private readonly bool _isAlias;

    public LoginCredentials(string username, string password)
      : this(username, password, true)
    {
    }

    protected LoginCredentials(string username, string password, bool isAlias)
    {
      this._username = username != null ? username : string.Empty;
      this._password = password != null ? password : string.Empty;
      this._isAlias = isAlias;
    }

    public string Username
    {
      get
      {
        return this._username;
      }
    }

    public string Password
    {
      get
      {
        return this._password;
      }
    }

    public bool IsAlias
    {
      get
      {
        return this._isAlias;
      }
    }
  }
}
