﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.ProtocolException
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol
{
    public class ProtocolException : Exception
    {
        private string details;
        private ProtocolException.ErrorArea area;
        private List<SAP.Copernicus.Core.Protocol.JSON.Message> messages;

        public ProtocolException(ProtocolException.ErrorArea area, List<SAP.Copernicus.Core.Protocol.JSON.Message> msgs)
        {
            this.area = area;
            this.messages = msgs;
        }

        public ProtocolException(ProtocolException.ErrorArea area, string message)
          : base(message)
        {
            this.area = area;
        }

        public ProtocolException(ProtocolException.ErrorArea area, string message, string details)
          : this(area, message)
        {
            this.details = details;
        }

        public ProtocolException(ProtocolException.ErrorArea area, Exception ex)
          : base("", ex)
        {
            this.area = area;
            this.fillDetailsFromException(ex);
        }

        public ProtocolException(ProtocolException.ErrorArea area, string message, Exception ex)
          : base(message, ex)
        {
            this.area = area;
            this.fillDetailsFromException(ex);
        }

        private void fillDetailsFromException(Exception ex)
        {
            this.details = ex.Message + ": " + ex.StackTrace;
        }

        public ProtocolException.ErrorArea getArea()
        {
            return this.area;
        }

        public string getDetails()
        {
            return this.details;
        }

        public void showPopup()
        {
            Trace.WriteLine(this.ToString());
            Trace.WriteLine(base.ToString());
            if (this.messages == null || this.messages.Count < 1)
            {
                int num1 = (int)CopernicusMessageBox.Show(this.ToString(), Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                if (Connection.getInstance().HeadLessMode)
                    return;
                MessageDialog md = new MessageDialog(messages);
                if (!md.BlockForm)
                {
                    int num = (int)md.ShowDialog();
                }
            }
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (this.area != ProtocolException.ErrorArea.NONE)
                stringBuilder.Append("ProtocolException (").Append((object)this.area).Append("): ");
            if (this.Message != null)
                stringBuilder.Append(this.Message);
            if (this.details != null)
                stringBuilder.Append(" Details: ").Append(this.details);
            if (this.messages != null)
            {
                foreach (SAP.Copernicus.Core.Protocol.JSON.Message message in this.messages)
                    stringBuilder.Append(" " + message.ToString());
            }
            return stringBuilder.ToString();
        }

        public enum ErrorArea
        {
            NONE,
            CLIENT,
            SERVER,
        }
    }
}
