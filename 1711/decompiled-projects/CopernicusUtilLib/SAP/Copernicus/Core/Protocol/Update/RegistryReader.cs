﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.RegistryReader
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Properties;
using System;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.Update
{
  public class RegistryReader
  {
    public bool IsInternalBuild
    {
      get
      {
        try
        {
          return PropertyAccess.GeneralProps.codeline.IndexOf("REL") == -1;
        }
        catch
        {
          return true;
        }
      }
    }

    public bool UpdateOrInstallLater
    {
      get
      {
        return PropertyAccess.GeneralProps.UpdateOrInstallLater;
      }
      set
      {
        PropertyAccess.GeneralProps.UpdateOrInstallLater = value;
        PropertyAccess.GeneralProps.SaveSettingsToStorage();
      }
    }

    public bool AutoUpdateWanted
    {
      get
      {
        try
        {
          return PropertyAccess.GeneralProps.AutoUpdateWanted;
        }
        catch
        {
          return false;
        }
      }
      set
      {
        PropertyAccess.GeneralProps.AutoUpdateWanted = value;
        PropertyAccess.GeneralProps.SaveSettingsToStorage();
      }
    }

    public bool Reminder
    {
      get
      {
        try
        {
          return false;
        }
        catch
        {
          return false;
        }
      }
      set
      {
        if (!value)
          return;
        PropertyAccess.GeneralProps.UpdateReminderDelay = DateTime.Now.AddDays((double) RegistryReader.UpdateReminderDelayDays);
        PropertyAccess.GeneralProps.SaveSettingsToStorage();
      }
    }

    public static int UpdateReminderDelayDays
    {
      get
      {
        if (PropertyAccess.GeneralProps.UpdateReminderDelayDays == 0)
        {
          PropertyAccess.GeneralProps.UpdateReminderDelayDays = 1;
          PropertyAccess.GeneralProps.SaveSettingsToStorage();
        }
        return PropertyAccess.GeneralProps.UpdateReminderDelayDays;
      }
      set
      {
        PropertyAccess.GeneralProps.UpdateReminderDelayDays = value;
        PropertyAccess.GeneralProps.SaveSettingsToStorage();
      }
    }

    public static DateTime GetInstallationDate()
    {
      return File.GetLastWriteTime(Application.ExecutablePath).ToUniversalTime();
    }

    public static DateTime GetBuildDate()
    {
      DateTimeFormatInfo dateTimeFormatInfo = new DateTimeFormatInfo();
      dateTimeFormatInfo.FullDateTimePattern = "yyyy'-'MM'-'dd HH':'mm':'ss";
      try
      {
        return DateTime.Parse(PropertyAccess.GeneralProps.buildDate, (IFormatProvider) dateTimeFormatInfo);
      }
      catch
      {
        return RegistryReader.GetInstallationDate();
      }
    }
  }
}
