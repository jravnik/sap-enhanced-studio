﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.ControlDownloadPopup
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Drawing;
using System.Net;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.Update
{
  public class ControlDownloadPopup : UserControl
  {
    private IContainer components;
    private ProgressBar progress_download;
    private Label label_progress;
    public WebClient Client;
    private string url;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.progress_download = new ProgressBar();
      this.label_progress = new Label();
      this.SuspendLayout();
      this.progress_download.Location = new Point(34, 46);
      this.progress_download.Name = "progress_download";
      this.progress_download.Size = new Size(354, 26);
      this.progress_download.TabIndex = 3;
      this.label_progress.AutoSize = true;
      this.label_progress.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label_progress.Location = new Point(31, 27);
      this.label_progress.Name = "label_progress";
      this.label_progress.Size = new Size(120, 13);
      this.label_progress.TabIndex = 2;
      this.label_progress.Text = "Download Progress: Connecting...";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.progress_download);
      this.Controls.Add((Control) this.label_progress);
      this.Name = nameof (ControlDownloadPopup);
      this.Size = new Size(420, 100);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public event ControlDownloadPopup.DownloadFinishedHandler downloadFinished;

    public event ControlDownloadPopup.DownloadErrorHandler downloadError;

    public ControlDownloadPopup(WebClient client, string url)
    {
      this.InitializeComponent();
      this.Dock = DockStyle.Fill;
      this.Client = client;
      this.url = url;
      client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(this.client_DownloadProgressChanged);
      client.DownloadFileCompleted += new AsyncCompletedEventHandler(this.client_DownloadFileCompleted);
    }

    public void startDownload()
    {
      if (NetworkInterface.GetIsNetworkAvailable())
      {
        this.Client.DownloadFileAsync(new Uri(this.url), UpdateHandler.AppDataPath + "\\CopernicusIsolatedShell.msi");
      }
      else
      {
        int num = (int) MessageBox.Show("Please check your internet connection.");
      }
    }

    private void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
    {
      double d = double.Parse(e.BytesReceived.ToString()) / double.Parse(e.TotalBytesToReceive.ToString()) * 100.0;
      this.progress_download.Value = int.Parse(Math.Truncate(d).ToString());
      this.label_progress.Text = "Download Progress: " + Math.Truncate(d).ToString() + "%";
    }

    private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
    {
      if (e.Error != null)
        this.downloadError();
      else
        this.downloadFinished();
    }

    public delegate void DownloadFinishedHandler();

    public delegate void DownloadErrorHandler();
  }
}
