﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.UpdateHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Properties;
using System;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.Update
{
  public class UpdateHandler
  {
    public static string AppDataPath = Environment.GetEnvironmentVariable("temp").Replace("\"", "") + "\\SAP\\Copernicus\\Update";
    private RegistryReader registryReader = new RegistryReader();
    public bool IsManualUpdate;
    private string urlToMsi;
    private WebClient client;
    private bool dontShowMessage;
    private FormUpdateContainer containerForm;
    private ControlDownloadPopupQ1 q1PopupControl;
    private ControlDownloadPopup dPopupControl;
    private ControlDownloadPopupError ePopupControl;

    public UpdateHandler(bool isManualUpdate)
    {
      this.IsManualUpdate = isManualUpdate;
      if (UpdateHandler.AppDataPath == null)
        throw new ArgumentNullException("The 'temp' Environment Variable is not set");
      if (Directory.Exists(UpdateHandler.AppDataPath))
        return;
      Directory.CreateDirectory(UpdateHandler.AppDataPath);
    }

    private bool isInternalBuild
    {
      get
      {
        return this.registryReader.IsInternalBuild;
      }
    }

    public bool Reminder
    {
      get
      {
        return this.registryReader.Reminder;
      }
      set
      {
        this.registryReader.Reminder = value;
      }
    }

    public bool AutoUpdateWanted
    {
      get
      {
        return this.registryReader.AutoUpdateWanted;
      }
      set
      {
        this.registryReader.AutoUpdateWanted = value;
      }
    }

    public bool UpdateOrInstallLater
    {
      get
      {
        return this.registryReader.UpdateOrInstallLater;
      }
      set
      {
        this.registryReader.UpdateOrInstallLater = value;
      }
    }

    public static int UpdateReminderDelayDays
    {
      get
      {
        return RegistryReader.UpdateReminderDelayDays;
      }
      set
      {
        RegistryReader.UpdateReminderDelayDays = value;
      }
    }

    public void StartCheckForUpdate()
    {
      if (this.CheckForUpdate())
      {
        this.StartUpdateProcess();
      }
      else
      {
        if (this.dontShowMessage)
          return;
        int num = (int) CopernicusMessageBox.Show("You currently have the latest version installed.", Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
      }
    }

    public bool CheckForUpdate()
    {
      if (this.Reminder && !this.IsManualUpdate)
        return false;
      int num1 = PropertyAccess.GeneralProps.isJaproBuild ? 1 : 0;
      int num2 = PropertyAccess.GeneralProps.isLocalBuild ? 1 : 0;
      IHandlerMsi handlerMsi;
      if (this.isInternalBuild && this.IsManualUpdate)
      {
        switch (new FormBuildSelection().ShowDialog())
        {
          case DialogResult.Yes:
            handlerMsi = (IHandlerMsi) new HandlerInternalMsi();
            break;
          case DialogResult.No:
            handlerMsi = (IHandlerMsi) new HandlerExternalMsi();
            break;
          default:
            this.dontShowMessage = true;
            return false;
        }
      }
      else
      {
        if (this.isInternalBuild && !this.IsManualUpdate)
          return false;
        handlerMsi = (IHandlerMsi) new HandlerExternalMsi();
      }
      try
      {
        if (!handlerMsi.IsAvailableUpdate())
          return false;
        this.urlToMsi = handlerMsi.GetUrl();
        this.client = handlerMsi.GetWebClient();
        this.Reminder = true;
        return true;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    public void StartUpdateProcess()
    {
      this.containerForm = new FormUpdateContainer();
      this.q1PopupControl = new ControlDownloadPopupQ1();
      this.q1PopupControl.IsManualUpdate = this.IsManualUpdate;
      this.q1PopupControl.downloadStart += new ControlDownloadPopupQ1.DownloadStartHandler(this.downloadUpdate);
      this.q1PopupControl.downloadLater += new ControlDownloadPopupQ1.DownloadLaterHandler(this.downloadLater);
      this.containerForm.FormClosing += (FormClosingEventHandler) ((sender, e) => this.UpdateOrInstallLater = true);
      this.containerForm.Controls.Add((Control) this.q1PopupControl);
      int num = (int) this.containerForm.ShowDialog();
    }

    protected void downloadUpdate()
    {
      this.dPopupControl = new ControlDownloadPopup(this.client, this.urlToMsi);
      this.dPopupControl.downloadFinished += new ControlDownloadPopup.DownloadFinishedHandler(this.installUpdate);
      this.dPopupControl.downloadError += new ControlDownloadPopup.DownloadErrorHandler(this.showErrorPopup);
      this.containerForm.FormClosing += (FormClosingEventHandler) ((sender, e) =>
      {
        this.dPopupControl.Client.CancelAsync();
        this.UpdateOrInstallLater = true;
      });
      this.containerForm.Controls.Remove((Control) this.q1PopupControl);
      this.containerForm.Controls.Add((Control) this.dPopupControl);
      this.dPopupControl.startDownload();
    }

    protected void downloadLater()
    {
      this.Reminder = true;
      this.UpdateOrInstallLater = true;
      this.containerForm.Close();
    }

    protected void installUpdate()
    {
      this.containerForm.Close();
      this.UpdateOrInstallLater = false;
      new Updater().update();
    }

    protected void showErrorPopup()
    {
      this.ePopupControl = new ControlDownloadPopupError(HandlerExternalMsi.urlToTabelle);
      this.containerForm.Controls.Remove((Control) this.dPopupControl);
      this.containerForm.Controls.Add((Control) this.ePopupControl);
    }
  }
}
