﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.ControlDownloadPopupQ1
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.Update
{
  [Docking(DockingBehavior.Ask)]
  public class ControlDownloadPopupQ1 : UserControl
  {
    private IContainer components;
    private Label label_info;
    private Button button_later;
    private Button button_yes;

    public event ControlDownloadPopupQ1.DownloadStartHandler downloadStart;

    public event ControlDownloadPopupQ1.DownloadLaterHandler downloadLater;

    public bool IsManualUpdate
    {
      set
      {
        if (!value)
          return;
        this.label_info.Text = "A new version of SAP Cloud Applications Studio is available.\r\nPlease save your work before installing the new version.\r\nDo you wish to download and install it now?";
      }
    }

    public ControlDownloadPopupQ1()
    {
      this.InitializeComponent();
      this.Dock = DockStyle.Fill;
    }

    private void button_yes_Click(object sender, EventArgs e)
    {
      this.downloadStart();
    }

    private void button_later_Click(object sender, EventArgs e)
    {
      this.downloadLater();
    }

    private void label_info_Click(object sender, EventArgs e)
    {
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.label_info = new Label();
      this.button_later = new Button();
      this.button_yes = new Button();
      this.SuspendLayout();
      this.label_info.AutoSize = true;
      this.label_info.Location = new Point(19, 17);
      this.label_info.Name = "label_info";
      this.label_info.Size = new Size(304, 26);
      this.label_info.TabIndex = 5;
      this.label_info.Text = "A new version of SAP Cloud Applications Studio is available.\r\nDo you wish to download and install it now?";
      this.label_info.Click += new EventHandler(this.label_info_Click);
      this.button_later.Location = new Point(329, 66);
      this.button_later.Name = "button_later";
      this.button_later.Size = new Size(78, 23);
      this.button_later.TabIndex = 4;
      this.button_later.Text = "Install Later";
      this.button_later.UseVisualStyleBackColor = true;
      this.button_later.Click += new EventHandler(this.button_later_Click);
      this.button_yes.Location = new Point(270, 66);
      this.button_yes.Name = "button_yes";
      this.button_yes.Size = new Size(53, 23);
      this.button_yes.TabIndex = 3;
      this.button_yes.Text = "Yes";
      this.button_yes.UseVisualStyleBackColor = true;
      this.button_yes.Click += new EventHandler(this.button_yes_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.label_info);
      this.Controls.Add((Control) this.button_later);
      this.Controls.Add((Control) this.button_yes);
      this.Name = nameof (ControlDownloadPopupQ1);
      this.Size = new Size(420, 100);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public delegate void DownloadStartHandler();

    public delegate void DownloadLaterHandler();
  }
}
