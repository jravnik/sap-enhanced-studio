﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.ControlDownloadPopupError
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.Update
{
  public class ControlDownloadPopupError : UserControl
  {
    private IContainer components;
    private LinkLabel linkLabel1;
    private string url;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.linkLabel1 = new LinkLabel();
      this.SuspendLayout();
      this.linkLabel1.AutoSize = true;
      this.linkLabel1.Location = new Point(19, 17);
      this.linkLabel1.Name = "linkLabel1";
      this.linkLabel1.Size = new Size(287, 26);
      this.linkLabel1.TabIndex = 1;
      this.linkLabel1.TabStop = true;
      this.linkLabel1.Text = "An error has occurred while trying to download the installer. \r\nPlease click here to get the installer manually.";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.linkLabel1);
      this.Name = nameof (ControlDownloadPopupError);
      this.Size = new Size(420, 100);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public ControlDownloadPopupError(string url)
    {
      this.url = url;
      this.InitializeComponent();
    }

    private void ControlDownloadPopupError_Load(object sender, EventArgs e)
    {
      this.linkLabel1.Links.Add(this.linkLabel1.Text.IndexOf("here"), 4, (object) this.url);
    }

    private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
    }
  }
}
