﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.FormUpdateContainer
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.Update
{
  public class FormUpdateContainer : Form
  {
    private IContainer components;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormUpdateContainer));
      this.SuspendLayout();
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(424, 101);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.Name = nameof (FormUpdateContainer);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "SAP Cloud Applications Studio";
      this.ResumeLayout(false);
    }

    public FormUpdateContainer()
    {
      this.InitializeComponent();
      this.FormBorderStyle = FormBorderStyle.FixedSingle;
    }
  }
}
