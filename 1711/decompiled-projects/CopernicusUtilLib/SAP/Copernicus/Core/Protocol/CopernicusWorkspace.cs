﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.CopernicusWorkspace
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Util;
using System;
using System.IO;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol
{
  internal class CopernicusWorkspace
  {
    private static readonly CopernicusWorkspace instance = new CopernicusWorkspace();
    private const string LOCK_FILE_NAME = ".lock";
    private const int DUMMY_BUF_SIZE = 1;
    private FileStream lockFile;

    public static CopernicusWorkspace Instance
    {
      get
      {
        return CopernicusWorkspace.instance;
      }
    }

    private CopernicusWorkspace()
    {
    }

    public LogonData EstablishBackendSession(ConnectionDataSet.SystemDataRow systemDataRow, string userName, bool optionalHeadless = false)
    {
      LogonData logonData1 = (LogonData) null;
      LogonHandler logonHandler = new LogonHandler();
      LogonData logonData2;
      try
      {
        logonData2 = logonHandler.CheckAliasLogon(systemDataRow, userName);
      }
      catch (ProtocolException ex)
      {
        return (LogonData) null;
      }
      bool XrepSessionExist = false;
      string xrepSessionID = CopernicusWorkspace.Instance.FetchXrepSessionIDFromWorkspace(systemDataRow.Name, logonData2.backend_user, out XrepSessionExist);
      CopernicusWorkspace instance = CopernicusWorkspace.Instance;
      if (this.LockWorkspace(systemDataRow.Name, logonData2.backend_user))
      {
        logonData1 = WebUtil.CheckHttpAuthentication(systemDataRow, logonData2.backend_user, xrepSessionID, optionalHeadless);
        if (logonData1 == null)
          return logonData1;
        logonData1.backend_user = logonData2.backend_user;
        systemDataRow.Client = int.Parse(logonData1.client);
        string validatedXrepSessionId = logonData1.ValidatedXRepSessionID;
        if (!xrepSessionID.Equals(validatedXrepSessionId))
        {
          string XRepSessionID = validatedXrepSessionId;
          instance.PersistXrepSessionIDToWorkspace(systemDataRow.Name, logonData2.backend_user, XRepSessionID);
        }
      }
      else
      {
        int num = (int) CopernicusMessageBox.Show(string.Format(Resource.MsgAllreadyLockedOn.ToString(), (object) systemDataRow.Name), Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      return logonData1;
    }

    public bool LockWorkspace(string ConnectionName, string userName)
    {
      string path = SAP.Copernicus.Core.Util.Util.ProjectsLocation + "\\" + ConnectionName;
      if (string.IsNullOrEmpty(path))
        throw new ArgumentNullException("workspaceFolder");
      try
      {
        string fullPath = Path.GetFullPath(path);
        if (this.GetCurrentlyLockedWorkspaceFolder() == fullPath)
          return true;
        this.UnlockWorkspace();
        if (!Directory.Exists(fullPath))
          Directory.CreateDirectory(fullPath);
        this.lockFile = File.Create(Path.Combine(fullPath, ".lock"), 1, FileOptions.DeleteOnClose);
      }
      catch (Exception ex)
      {
        return false;
      }
      return true;
    }

    public void UnlockWorkspace()
    {
      if (this.lockFile == null)
        return;
      this.GetCurrentlyLockedWorkspaceFolder();
      try
      {
        this.lockFile.Close();
      }
      catch (Exception ex)
      {
      }
      finally
      {
        this.lockFile = (FileStream) null;
      }
    }

    public string FetchXrepSessionIDFromWorkspace(string ConnectionName, string userName, out bool XrepSessionExist)
    {
      XrepSessionExist = false;
      string str = LocalFileHandler.ReadFileFromDisk(Path.Combine(SAP.Copernicus.Core.Util.Util.ProjectsLocation + "\\" + ConnectionName, userName + ".session"));
      if (!string.IsNullOrEmpty(str))
        XrepSessionExist = true;
      return str;
    }

    public void PersistXrepSessionIDToWorkspace(string ConnectionName, string userName, string XRepSessionID)
    {
      string filePath = Path.Combine(SAP.Copernicus.Core.Util.Util.ProjectsLocation + "\\" + ConnectionName, userName.ToUpper() + ".session");
      LocalFileHandler.CreateFileOnDisk(XRepSessionID, filePath, true);
    }

    public string GetCurrentlyLockedWorkspaceFolder()
    {
      if (this.lockFile == null)
        return (string) null;
      return Path.GetDirectoryName(this.lockFile.Name);
    }
  }
}
