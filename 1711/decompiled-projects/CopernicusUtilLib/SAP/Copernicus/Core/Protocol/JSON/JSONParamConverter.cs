﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.JSONParamConverter
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.JSON
{
  public static class JSONParamConverter
  {
    public static List<SyntaxErrorInfo> ET_MESSAGES2SyntaxErrorInfoList(PDI_SFW_S_MESSAGE[] etMessages)
    {
      List<SyntaxErrorInfo> syntaxErrorInfoList = new List<SyntaxErrorInfo>();
      foreach (PDI_SFW_S_MESSAGE etMessage in etMessages)
      {
        SyntaxErrorInfo syntaxErrorInfo = new SyntaxErrorInfo(etMessage.TYPE, etMessage.METHOD, etMessage.MESSAGE);
        syntaxErrorInfoList.Add(syntaxErrorInfo);
      }
      return syntaxErrorInfoList;
    }
  }
}
