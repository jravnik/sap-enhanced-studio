﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.__EXT__PDI_REF_UI_CONV_XML2MODEL
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class __EXT__PDI_REF_UI_CONV_XML2MODEL : AbstractRemoteFunction<__EXT__PDI_REF_UI_CONV_XML2MODEL.ImportingType, __EXT__PDI_REF_UI_CONV_XML2MODEL.ExportingType, __EXT__PDI_REF_UI_CONV_XML2MODEL.ChangingType, __EXT__PDI_REF_UI_CONV_XML2MODEL.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0383AC1ED2ADDECFDE9CA205C1";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_XREP_XML;
      [DataMember]
      public string IV_READ_ALL_CUSTOM_FIELDS;
      [DataMember]
      public string IV_PRODUCT_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public __EXT__S_PDI_REF_UI_CUSTOM_FIELD[] ET_CUSTOM_FIELD_REFERENCES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
