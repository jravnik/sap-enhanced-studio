﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_GET_DOCU_SERVICE
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_GET_DOCU_SERVICE : AbstractRemoteFunction<PDI_RI_GET_DOCU_SERVICE.ImportingType, PDI_RI_GET_DOCU_SERVICE.ExportingType, PDI_RI_GET_DOCU_SERVICE.ChangingType, PDI_RI_GET_DOCU_SERVICE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011ED197AD14533B1895D6";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BO_NAME;
      [DataMember]
      public string IV_NODE_NAME;
      [DataMember]
      public string IV_ACTION_NAME;
      [DataMember]
      public string IV_ASSOCIATION_NAME;
      [DataMember]
      public string IV_QUERY_NAME;
      [DataMember]
      public string IV_NODE_ELEMENT;
      [DataMember]
      public string IV_DT_NAME;
      [DataMember]
      public string IV_BADI_NAME;
      [DataMember]
      public string IV_ISI_NAME;
      [DataMember]
      public string IV_DTD_NAME;
      [DataMember]
      public string IV_KTD_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_RESULT;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
