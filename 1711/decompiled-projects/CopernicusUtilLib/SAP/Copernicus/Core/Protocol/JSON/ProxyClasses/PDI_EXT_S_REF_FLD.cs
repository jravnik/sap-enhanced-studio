﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_S_REF_FLD
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_S_REF_FLD
  {
    [DataMember]
    public string TYPE;
    [DataMember]
    public string BUNDLE;
    [DataMember]
    public string FIELD;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string UI_MODEL;
    [DataMember]
    public string UI_MODEL_PATH;
    [DataMember]
    public string BO_ESR_NAME;
    [DataMember]
    public string BO_ESR_NAMESPACE;
  }
}
