﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.ExtensionHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class ExtensionHandler : JSONHandler
  {
    private static readonly Encoding encUTF8 = Encoding.UTF8;
    private bool usePSM;

    public ExtensionHandler()
    {
      this.usePSM = SAP.Copernicus.Core.Protocol.Connection.getInstance().UsePSM;
    }

    public bool GetExtensibleNodes(string IV_BO_ESRNAME, string IV_BO_ESRNAMESPACE, string IV_SOLUTION, out PDI_EXT_S_EXT_NODE[] ET_EXT_NODES, out PDI_EXT_S_EXT_FIELDS[] ET_NODES_FIELDS, out PDI_EXT_S_EXT_FIELD_SCENARIOS[] ET_EXT_FIELD_SCENARIOS, out PDI_EXT_S_EXT_ACTION[] ET_EXT_ACTION)
    {
      PDI_EXT_GET_EXTENSIBLE_NODES getExtensibleNodes = new PDI_EXT_GET_EXTENSIBLE_NODES();
      ET_NODES_FIELDS = (PDI_EXT_S_EXT_FIELDS[]) null;
      ET_EXT_NODES = (PDI_EXT_S_EXT_NODE[]) null;
      ET_EXT_FIELD_SCENARIOS = (PDI_EXT_S_EXT_FIELD_SCENARIOS[]) null;
      ET_EXT_ACTION = (PDI_EXT_S_EXT_ACTION[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        getExtensibleNodes.Importing = new PDI_EXT_GET_EXTENSIBLE_NODES.ImportingType();
        getExtensibleNodes.Importing.IV_BO_ESRNAME = IV_BO_ESRNAME;
        getExtensibleNodes.Importing.IV_BO_ESRNAMESPACE = IV_BO_ESRNAMESPACE;
        getExtensibleNodes.Importing.IV_SOLUTION = IV_SOLUTION;
        jsonClient.callFunctionModule((SAPFunctionModule) getExtensibleNodes, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) getExtensibleNodes, false, false);
        if (getExtensibleNodes.Exporting != null)
        {
          ET_NODES_FIELDS = getExtensibleNodes.Exporting.ET_NODES_FIELDS;
          ET_EXT_NODES = !this.usePSM ? getExtensibleNodes.Exporting.ET_EXT_NODES : ((IEnumerable<PDI_EXT_S_EXT_NODE>) getExtensibleNodes.Exporting.ET_EXT_NODES).Where<PDI_EXT_S_EXT_NODE>((Func<PDI_EXT_S_EXT_NODE, bool>) (node => node.PSM_RELEASED == "X")).ToArray<PDI_EXT_S_EXT_NODE>();
          ET_EXT_FIELD_SCENARIOS = getExtensibleNodes.Exporting.ET_EXT_FIELD_SCENARIOS;
          ET_EXT_ACTION = getExtensibleNodes.Exporting.ET_EXT_ACTION;
          return true;
        }
      }
      catch (ThreadInterruptedException ex)
      {
        Trace.TraceInformation("Error in JSONClient: " + ex.Message);
      }
      catch (ThreadAbortException ex)
      {
        Trace.TraceInformation("Error in JSONClient: " + ex.Message);
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      return false;
    }

    public PDI_EXT_S_REF_FLD[] GetUIComponents(string IV_XBO_FILENAME, string IV_PRJ_NAME, string IV_PRJ_NAMESPACE, out string EV_ERROR)
    {
      EV_ERROR = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_EXT_GET_UI_MODELS_OF_BO extGetUiModelsOfBo = new PDI_EXT_GET_UI_MODELS_OF_BO();
        extGetUiModelsOfBo.Importing = new PDI_EXT_GET_UI_MODELS_OF_BO.ImportingType();
        extGetUiModelsOfBo.Importing.IV_XBO_FILENAME = IV_XBO_FILENAME;
        extGetUiModelsOfBo.Importing.IV_PRJ_NAME = IV_PRJ_NAME;
        extGetUiModelsOfBo.Importing.IV_PRJ_NAMESPACE = IV_PRJ_NAMESPACE;
        jsonClient.callFunctionModule((SAPFunctionModule) extGetUiModelsOfBo, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) extGetUiModelsOfBo, false, false);
        if (extGetUiModelsOfBo.Exporting != null)
        {
          foreach (PDI_RI_S_MESSAGE pdiRiSMessage in extGetUiModelsOfBo.Exporting.ET_MESSAGES)
            EV_ERROR = pdiRiSMessage.TEXT;
          return extGetUiModelsOfBo.Exporting.ET_REFERENCE_FIELDS;
        }
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      return (PDI_EXT_S_REF_FLD[]) null;
    }

    public bool RegisterEXF(string BO_ESR_Name, string BO_ESR_Namespace, string XREP_Path, bool createABSL, bool checkMaintenance, PDI_EXT_S_EXT_FIELDS[] EXT_Nodes, PDI_EXT_S_EXT_FIELD_SCENARIOS[] fieldScenarios, PDI_EXT_S_MESSAGES[] messages, out string EV_ERROR, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      EV_ERROR = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_EXT_FACADE_AND_EXF_CONFIG facadeAndExfConfig = new PDI_EXT_FACADE_AND_EXF_CONFIG();
        facadeAndExfConfig.Importing = new PDI_EXT_FACADE_AND_EXF_CONFIG.ImportingType();
        facadeAndExfConfig.Importing.IV_ESR_BO_NAME = BO_ESR_Name;
        facadeAndExfConfig.Importing.IV_ESR_NAMESPACE = BO_ESR_Namespace;
        facadeAndExfConfig.Importing.IV_XREP_PATH = XREP_Path;
        facadeAndExfConfig.Importing.IT_NODES = EXT_Nodes;
        facadeAndExfConfig.Importing.IT_EXT_FIELD_SCENARIOS = fieldScenarios;
        facadeAndExfConfig.Importing.IT_EXT_MESSAGES = messages;
        facadeAndExfConfig.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        facadeAndExfConfig.Importing.IV_ABSL = createABSL;
        facadeAndExfConfig.Importing.IV_MAINTENANCE_CHECK = checkMaintenance;
        jsonClient.callFunctionModule((SAPFunctionModule) facadeAndExfConfig, false, false, false);
        if (facadeAndExfConfig.Exporting != null)
        {
          ET_MESSAGES = facadeAndExfConfig.Exporting.ET_MESSAGES;
          return facadeAndExfConfig.Exporting.EV_SUCCESS.Equals("X");
        }
        this.reportServerSideProtocolException((SAPFunctionModule) facadeAndExfConfig, false, false);
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      return false;
    }

    public bool GetXrepFiles(string IV_SOLUTION, bool IV_XS_FILES, bool IV_XBO_FILES, bool IV_XFP_FILE, out OSLS_XREP_MASS_VERS_CONTENT[] ET_CONTENT_XS_FILES, out OSLS_XREP_MASS_VERS_CONTENT[] ET_CONTENT_XBO_FILES, out PDI_EXT_S_XFP_FILE_DETAILS[] ET_XFP_FILE_DETAILS)
    {
      ET_CONTENT_XS_FILES = (OSLS_XREP_MASS_VERS_CONTENT[]) null;
      ET_CONTENT_XBO_FILES = (OSLS_XREP_MASS_VERS_CONTENT[]) null;
      ET_XFP_FILE_DETAILS = (PDI_EXT_S_XFP_FILE_DETAILS[]) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_EXT_GET_XREP_FILES pdiExtGetXrepFiles = new PDI_EXT_GET_XREP_FILES();
      pdiExtGetXrepFiles.Importing = new PDI_EXT_GET_XREP_FILES.ImportingType();
      pdiExtGetXrepFiles.Importing.IV_SOLUTION = IV_SOLUTION;
      pdiExtGetXrepFiles.Importing.IV_XS_FILES = IV_XS_FILES;
      pdiExtGetXrepFiles.Importing.IV_XBO_FILES = IV_XBO_FILES;
      pdiExtGetXrepFiles.Importing.IV_XFP_FILE = IV_XFP_FILE;
      jsonClient.callFunctionModule((SAPFunctionModule) pdiExtGetXrepFiles, false, false, false);
      if (pdiExtGetXrepFiles.Exporting == null)
        return false;
      ET_CONTENT_XS_FILES = pdiExtGetXrepFiles.Exporting.ET_CONTENT_XS_FILES;
      ET_CONTENT_XBO_FILES = pdiExtGetXrepFiles.Exporting.ET_CONTENT_XBO_FILES;
      ET_XFP_FILE_DETAILS = pdiExtGetXrepFiles.Exporting.ET_XFP_FILE_DETAILS;
      PDI_RI_S_MESSAGE[] etMessages = pdiExtGetXrepFiles.Exporting.ET_MESSAGES;
      return true;
    }

    public bool GetNodeExtXrepFiles(string IV_SOLUTION, string IV_BO_NAMESPACE, string IV_BO_NAME, string IV_BO_NODE, string IV_EXTNEDED_NODE, string IV_SCENARIO)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_NODE_EXT_GET_XREP_FILES nodeExtGetXrepFiles = new PDI_NODE_EXT_GET_XREP_FILES();
      nodeExtGetXrepFiles.Importing = new PDI_NODE_EXT_GET_XREP_FILES.ImportingType();
      nodeExtGetXrepFiles.Importing.IV_SOLUTION = IV_SOLUTION;
      nodeExtGetXrepFiles.Importing.IV_BO_NAME = IV_BO_NAME;
      nodeExtGetXrepFiles.Importing.IV_BO_NAMESPACE = IV_BO_NAMESPACE;
      nodeExtGetXrepFiles.Importing.IV_BO_NODE = IV_BO_NODE;
      nodeExtGetXrepFiles.Importing.IV_EXT_NODE = IV_EXTNEDED_NODE;
      nodeExtGetXrepFiles.Importing.IV_SCENARIO = IV_SCENARIO;
      jsonClient.callFunctionModule((SAPFunctionModule) nodeExtGetXrepFiles, false, false, false);
      if (nodeExtGetXrepFiles.Exporting == null)
        return false;
      string evExists = nodeExtGetXrepFiles.Exporting.EV_EXISTS;
      PDI_RI_S_MESSAGE[] etMessages = nodeExtGetXrepFiles.Exporting.ET_MESSAGES;
      return evExists.Equals("X");
    }

    public string checkBOExistence(string solution, string esrBoName, string esrBoNameSpace, out bool OfflineEnabled)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_EXT_CHECK_BO_EXISTENCE checkBoExistence = new PDI_EXT_CHECK_BO_EXISTENCE();
        checkBoExistence.Importing = new PDI_EXT_CHECK_BO_EXISTENCE.ImportingType();
        checkBoExistence.Importing.IV_SOLUTION = solution;
        checkBoExistence.Importing.IV_ESR_BO_NAME = esrBoName;
        checkBoExistence.Importing.IV_ESR_BO_NAMESPACE = esrBoNameSpace;
        jsonClient.callFunctionModule((SAPFunctionModule) checkBoExistence, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) checkBoExistence, false, false);
        if (checkBoExistence.Exporting != null)
        {
          string empty = string.Empty;
          string evErrorCode = checkBoExistence.Exporting.EV_ERROR_CODE;
          OfflineEnabled = checkBoExistence.Exporting.EV_OFFLINE == "X";
          return evErrorCode;
        }
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      OfflineEnabled = false;
      return string.Empty;
    }

    public bool GetFormFields(string IV_XBO_FILENAME, string IV_PRJ_NAME, string IV_PRJ_NAMESPACE, out PDI_EXT_S_EXT_FORM_FIELDS[] ET_FORM_FIELDS, out string EV_ERROR, out string EV_BO_NAME, out string EV_NUMBER_OF_NODES)
    {
      EV_ERROR = (string) null;
      ET_FORM_FIELDS = (PDI_EXT_S_EXT_FORM_FIELDS[]) null;
      EV_BO_NAME = (string) null;
      EV_NUMBER_OF_NODES = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_EXT_GET_FORM_EXT_FIELDS getFormExtFields = new PDI_EXT_GET_FORM_EXT_FIELDS();
        getFormExtFields.Importing = new PDI_EXT_GET_FORM_EXT_FIELDS.ImportingType();
        getFormExtFields.Importing.IV_XBO_FILENAME = IV_XBO_FILENAME;
        getFormExtFields.Importing.IV_PRJ_NAME = IV_PRJ_NAME;
        getFormExtFields.Importing.IV_PRJ_NAMESPACE = IV_PRJ_NAMESPACE;
        jsonClient.callFunctionModule((SAPFunctionModule) getFormExtFields, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) getFormExtFields, false, false);
        if (getFormExtFields.Exporting != null)
        {
          ET_FORM_FIELDS = getFormExtFields.Exporting.ET_FIELD_INFO;
          EV_BO_NAME = getFormExtFields.Exporting.EV_BO_NAME;
          EV_NUMBER_OF_NODES = getFormExtFields.Exporting.EV_NUMBER_OF_NODES;
          foreach (PDI_RI_S_MESSAGE pdiRiSMessage in getFormExtFields.Exporting.ET_MESSAGES)
            EV_ERROR = pdiRiSMessage.TEXT;
        }
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      return true;
    }

    public bool GetTransportRequest(string IV_XREPSOLUTION, out string EV_TRKORR)
    {
      EV_TRKORR = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_LM_PRODUCT_GET_TRKORR productGetTrkorr = new PDI_LM_PRODUCT_GET_TRKORR();
        productGetTrkorr.Importing = new PDI_LM_PRODUCT_GET_TRKORR.ImportingType();
        productGetTrkorr.Importing.IV_COMPONENT_NAME = IV_XREPSOLUTION;
        jsonClient.callFunctionModule((SAPFunctionModule) productGetTrkorr, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) productGetTrkorr, false, false);
        if (productGetTrkorr.Exporting != null)
          EV_TRKORR = productGetTrkorr.Exporting.EV_TRKORR;
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      return true;
    }

    public bool GetExtScenarios(PDI_EXT_S_BO_NODE_KEY[] IT_BO_NODES, out string resultBase64)
    {
      resultBase64 = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_EXT_GET_SCENARIOS pdiExtGetScenarios = new PDI_EXT_GET_SCENARIOS();
        pdiExtGetScenarios.Importing = new PDI_EXT_GET_SCENARIOS.ImportingType();
        pdiExtGetScenarios.Importing.IT_BO_NODES = IT_BO_NODES;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiExtGetScenarios, false, false, false);
        resultBase64 = pdiExtGetScenarios.Exporting.EV_SCENARIOS;
        return true;
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
        return false;
      }
    }

    public bool GetNodeExtScenarios(PDI_NODE_EXT_S_BO_NODE_KEY[] IT_BO_NODES, string solutionPrefix, out string resultBase64, out string altKeyExists, out string scenarioUsed)
    {
      resultBase64 = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_NODE_EXT_GET_SCENARIOS nodeExtGetScenarios = new PDI_NODE_EXT_GET_SCENARIOS();
        nodeExtGetScenarios.Importing = new PDI_NODE_EXT_GET_SCENARIOS.ImportingType();
        nodeExtGetScenarios.Importing.IT_BO_NODES = IT_BO_NODES;
        solutionPrefix = solutionPrefix.Replace("_", "");
        nodeExtGetScenarios.Importing.IV_SOLUTION_PREFIX = solutionPrefix;
        jsonClient.callFunctionModule((SAPFunctionModule) nodeExtGetScenarios, false, false, false);
        resultBase64 = nodeExtGetScenarios.Exporting.EV_SCENARIOS;
        scenarioUsed = nodeExtGetScenarios.Exporting.EV_SCENARIO_USED;
        altKeyExists = nodeExtGetScenarios.Exporting.EV_ALT_KEY_EXISTS;
        return true;
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
        altKeyExists = string.Empty;
        scenarioUsed = string.Empty;
        return false;
      }
    }
  }
}
