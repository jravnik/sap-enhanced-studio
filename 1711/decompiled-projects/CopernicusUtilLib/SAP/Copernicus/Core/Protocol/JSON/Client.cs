﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Client
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol.ConnectionModel;

namespace SAP.Copernicus.Core.Protocol.JSON
{
  public class Client
  {
    private static Client instance = new Client();
    private JSONClient jsonClient;

    private Client()
    {
    }

    public static Client getInstance()
    {
      return Client.instance;
    }

    public JSONClient getJSONClient(bool stateful = false)
    {
      if (!stateful && this.jsonClient != null)
        return this.jsonClient;
      SAP.Copernicus.Core.Protocol.Connection instance1 = SAP.Copernicus.Core.Protocol.Connection.getInstance();
      if (!instance1.isConnected())
        instance1.connect();
      ConnectionDataSet.SystemDataRow connectedSystem = instance1.getConnectedSystem();
      int client = connectedSystem.Client;
      string user = connectedSystem.User;
      string jsonHttpUrl = instance1.GetJsonHttpURL();
      int connectivityTimeout = PropertyAccess.GeneralProps.ConnectivityTimeout;
      JSONClient instance2 = JSONClient.getInstance(new com.sap.JSONConnector.Connection(jsonHttpUrl, SAP.Copernicus.Core.Util.Util.clientIntToString(client), user, connectedSystem.SecurePassword, stateful, connectivityTimeout));
      if (!stateful)
        this.jsonClient = instance2;
      return instance2;
    }

    public JSONClient getJSONTestClient(bool stateful = false)
    {
      SAP.Copernicus.Core.Protocol.Connection instance = SAP.Copernicus.Core.Protocol.Connection.getInstance();
      if (!instance.isConnected())
        instance.connect();
      ConnectionDataSet.SystemDataRow connectedSystem = instance.getConnectedSystem();
      int client = connectedSystem.Client;
      string user = connectedSystem.User;
      string jsonTestHttpUrl = instance.GetJsonTestHttpURL();
      int connectivityTimeout = PropertyAccess.GeneralProps.ConnectivityTimeout;
      return JSONClient.getInstance(new com.sap.JSONConnector.Connection(jsonTestHttpUrl, SAP.Copernicus.Core.Util.Util.clientIntToString(client), user, connectedSystem.SecurePassword, stateful, connectivityTimeout));
    }

    public void reset()
    {
      this.jsonClient = (JSONClient) null;
    }
  }
}
