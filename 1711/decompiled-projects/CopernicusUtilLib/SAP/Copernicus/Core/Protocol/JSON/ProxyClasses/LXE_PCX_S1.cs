﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.LXE_PCX_S1
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class LXE_PCX_S1
  {
    [DataMember]
    public string TEXTKEY;
    [DataMember]
    public string S_TEXT;
    [DataMember]
    public string T_TEXT;
    [DataMember]
    public string UNITMLT;
    [DataMember]
    public string UPPCASE;
    [DataMember]
    public string TEXTTYPE;
  }
}
