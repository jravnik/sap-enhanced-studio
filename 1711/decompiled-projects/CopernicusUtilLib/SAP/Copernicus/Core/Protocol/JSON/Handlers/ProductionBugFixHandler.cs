﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.ProductionBugFixHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class ProductionBugFixHandler : JSONHandler
  {
    public bool productionBugFixCheckUser(string solution)
    {
      try
      {
        bool flag = false;
        PDI_1O_PROD_FIX_CHK_USER obj = new PDI_1O_PROD_FIX_CHK_USER();
        obj.Importing = new PDI_1O_PROD_FIX_CHK_USER.ImportingType();
        obj.Importing.IV_SOLUTION_NAME = solution;
        obj.Importing.IV_LANGUAGE = "E";
        Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) obj, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) obj, true, false);
        if (obj.Exporting != null)
          flag = obj.Exporting.EV_SUCCESS.Equals("X");
        return flag;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }

    public bool productionBugFixRevert(string path, string solutionVersion)
    {
      try
      {
        bool flag = false;
        PDI_1O_PROD_FIX_REVERT pdi1OProdFixRevert = new PDI_1O_PROD_FIX_REVERT();
        pdi1OProdFixRevert.Importing = new PDI_1O_PROD_FIX_REVERT.ImportingType();
        pdi1OProdFixRevert.Importing.IV_PATH = path;
        pdi1OProdFixRevert.Importing.IV_SESSION_ID = Connection.getInstance().GetXRepSessionID();
        pdi1OProdFixRevert.Importing.IV_MINORVERSION = solutionVersion;
        Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) pdi1OProdFixRevert, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdi1OProdFixRevert, true, false);
        if (pdi1OProdFixRevert.Exporting != null)
          flag = pdi1OProdFixRevert.Exporting.EV_SUCCESS.Equals("X");
        return flag;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }

    public bool PerformanceCheck(string path)
    {
      try
      {
        bool flag = false;
        PDI_RI_PERFORMANCE_CHECK performanceCheck = new PDI_RI_PERFORMANCE_CHECK();
        performanceCheck.Importing = new PDI_RI_PERFORMANCE_CHECK.ImportingType();
        performanceCheck.Importing.IT_XREP_FILE_PATH = new string[1];
        performanceCheck.Importing.IT_XREP_FILE_PATH[0] = path;
        Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) performanceCheck, false, false, false);
        ErrorSink.Instance.ClearTasksFor(Origin.All);
        if (performanceCheck.Exporting != null)
        {
          flag = performanceCheck.Exporting.EV_SUCCESS.Equals("X");
          ErrorSink.Instance.AddMessages(performanceCheck.Exporting.ET_MESSAGES, Resource.MessagePrefixCheck, Origin.GenericEditorCheck);
        }
        return flag;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }
  }
}
