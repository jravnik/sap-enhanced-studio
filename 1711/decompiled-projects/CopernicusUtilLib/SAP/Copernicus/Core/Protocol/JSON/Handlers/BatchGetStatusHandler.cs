﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.BatchGetStatusHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class BatchGetStatusHandler : JSONHandler
  {
    public PDI_LM_BATCH_GET_STATUS.ExportingType getBatchStatus(string jobname, string jobcount, string appLog, out PDI_LM_T_MSG_LIST[] msgs)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_LM_BATCH_GET_STATUS lmBatchGetStatus = new PDI_LM_BATCH_GET_STATUS();
      lmBatchGetStatus.Importing = new PDI_LM_BATCH_GET_STATUS.ImportingType();
      lmBatchGetStatus.Importing.IV_APPLOG_ID = appLog;
      lmBatchGetStatus.Importing.IV_JOBNAME = jobname;
      lmBatchGetStatus.Importing.IV_JOBCNT = jobcount;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) lmBatchGetStatus, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      msgs = lmBatchGetStatus.Exporting.ET_MSG_LIST;
      if (lmBatchGetStatus.Exporting != null)
      {
        if (!(lmBatchGetStatus.Exporting.EV_SUCCESS == "") || lmBatchGetStatus.Exporting.ET_MESSAGES == null || lmBatchGetStatus.Exporting.ET_MESSAGES.Length <= 0)
          return lmBatchGetStatus.Exporting;
        this.reportServerSideProtocolException((SAPFunctionModule) lmBatchGetStatus, false, false);
      }
      return (PDI_LM_BATCH_GET_STATUS.ExportingType) null;
    }
  }
}
