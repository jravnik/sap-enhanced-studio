﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_VIEW_ELEM_PROP
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_S_VIEW_ELEM_PROP
  {
    [DataMember]
    public PDI_RI_S_KEY KEY;
    [DataMember]
    public PDI_RI_S_SUPPORTED_TYPE SUPPORTED_TYPE;
    [DataMember]
    public string HAS_DESCRIPTION;
    [DataMember]
    public string NEEDS_COMPOUNDING;
  }
}
