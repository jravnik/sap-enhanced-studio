﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_TAX_TYPE
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_TAX_TYPE
  {
    public string TAX_TYPEfield;
    public string DESCRIPTIONfield;

    [DataMember]
    public string TAX_TYPE
    {
      get
      {
        return this.TAX_TYPEfield;
      }
      set
      {
        this.TAX_TYPEfield = value;
      }
    }

    [DataMember]
    public string DESCRIPTION
    {
      get
      {
        return this.DESCRIPTIONfield;
      }
      set
      {
        this.DESCRIPTIONfield = value;
      }
    }
  }
}
