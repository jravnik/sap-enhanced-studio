﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_AVAILABLE_LANGUAGES
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_GET_AVAILABLE_LANGUAGES : AbstractRemoteFunction<PDI_GET_AVAILABLE_LANGUAGES.ImportingType, PDI_GET_AVAILABLE_LANGUAGES.ExportingType, PDI_GET_AVAILABLE_LANGUAGES.ChangingType, PDI_GET_AVAILABLE_LANGUAGES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01138A1EE0AFEA2765D152DC26";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public ZLANG_STRUC[] E_SUPP_LANG;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string[] ET_CHECK_INFO;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
