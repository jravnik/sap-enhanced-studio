﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.BCT_S_TEMPLATE_CONTEXT
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class BCT_S_TEMPLATE_CONTEXT
  {
    [DataMember]
    public string ELEMENT_ID;
    [DataMember]
    public string CONTENT_ID;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string BO_NAMESPACE;
    [DataMember]
    public string BO_NAME;
    [DataMember]
    public string NODE;
    [DataMember]
    public string OPERATION;
    [DataMember]
    public string IS_WORKITEM;
    [DataMember]
    public string IS_INSCOPE;
  }
}
