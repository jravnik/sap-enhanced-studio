﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PRODUCT
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PRODUCT
  {
    [DataMember]
    public string PRODUCT;
    [DataMember]
    public string MASTER_LANGUAGE;
    [DataMember]
    public string PRODUCT_TYPE;
    [DataMember]
    public string PPMS_NR;
    [DataMember]
    public string PARTNER;
    [DataMember]
    public string PARTNER_ID;
    [DataMember]
    public string IS_DELETED;
    [DataMember]
    public string DEV_PARTNER;
    [DataMember]
    public string CONTACT_PERSON;
    [DataMember]
    public string EMAIL;
  }
}
