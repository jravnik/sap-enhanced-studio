﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_TRC_QUERY_TRACE_BY_USER
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_TRC_QUERY_TRACE_BY_USER : AbstractRemoteFunction<PDI_TRC_QUERY_TRACE_BY_USER.ImportingType, PDI_TRC_QUERY_TRACE_BY_USER.ExportingType, PDI_TRC_QUERY_TRACE_BY_USER.ChangingType, PDI_TRC_QUERY_TRACE_BY_USER.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return nameof (PDI_TRC_QUERY_TRACE_BY_USER);
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_USER_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_TRACE_LIST;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
