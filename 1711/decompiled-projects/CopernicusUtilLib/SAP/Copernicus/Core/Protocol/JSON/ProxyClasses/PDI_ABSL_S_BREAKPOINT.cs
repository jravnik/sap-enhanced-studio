﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_S_BREAKPOINT
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_S_BREAKPOINT
  {
    [DataMember]
    public string XREP_FILE_PATH;
    [DataMember]
    public int LINE;
    [DataMember]
    public string SOURCE_HASH;
    [DataMember]
    public PDI_ABSL_S_SPAN SPAN;
    [DataMember]
    public PDI_ABSL_S_ABAP_LINE ABAP_LINE;
    [DataMember]
    public string ERROR_TEXT;
    private static IComparer<PDI_ABSL_S_BREAKPOINT> compareByFilePathAndLine;

    public static IComparer<PDI_ABSL_S_BREAKPOINT> CompareByFilePathAndLine
    {
      get
      {
        if (PDI_ABSL_S_BREAKPOINT.compareByFilePathAndLine == null)
          PDI_ABSL_S_BREAKPOINT.compareByFilePathAndLine = (IComparer<PDI_ABSL_S_BREAKPOINT>) new PDI_ABSL_S_BREAKPOINT.CompareByFilePathAndLineHelper();
        return PDI_ABSL_S_BREAKPOINT.compareByFilePathAndLine;
      }
    }

    private class CompareByFilePathAndLineHelper : IComparer<PDI_ABSL_S_BREAKPOINT>
    {
      public int Compare(PDI_ABSL_S_BREAKPOINT x, PDI_ABSL_S_BREAKPOINT y)
      {
        int num = x.XREP_FILE_PATH.CompareTo(y.XREP_FILE_PATH);
        if (num != 0)
          return num;
        return x.LINE.CompareTo(y.LINE);
      }
    }
  }
}
