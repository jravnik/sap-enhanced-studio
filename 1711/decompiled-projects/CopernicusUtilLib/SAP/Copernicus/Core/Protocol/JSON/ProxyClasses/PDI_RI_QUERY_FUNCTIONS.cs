﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_QUERY_FUNCTIONS
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_QUERY_FUNCTIONS : AbstractRemoteFunction<PDI_RI_QUERY_FUNCTIONS.ImportingType, PDI_RI_QUERY_FUNCTIONS.ExportingType, PDI_RI_QUERY_FUNCTIONS.ChangingType, PDI_RI_QUERY_FUNCTIONS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011ED196F57E06C40FD987";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string[] IT_DT_PROXYNAME;
      [DataMember]
      public string[] IT_BO_PROXYNAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string[] ET_RESULT_FUNCTIONS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
