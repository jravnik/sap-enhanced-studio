﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.WEBSERVICE_EXTENSION_DATATYPES
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class WEBSERVICE_EXTENSION_DATATYPES
  {
    [DataMember]
    public string MANDT;
    [DataMember]
    public string SOLUTION_NAME;
    [DataMember]
    public string EXTERNAL_INTERFACE_NAME;
    [DataMember]
    public string EXTERNAL_OPERATION_NAME;
    [DataMember]
    public string INTERNAL_INTERFACE_NAME;
    [DataMember]
    public string INTERNAL_OPERATION_NAME;
    [DataMember]
    public string TRANSFORMATION_DEF_CLASS_NAME;
    [DataMember]
    public string TRANSFORMATION_DEF_METH_NAME;
    [DataMember]
    public string INPUT_DATA_TYPE_NAME;
    [DataMember]
    public string OUTPUT_DATA_TYPE_NAME;
  }
}
