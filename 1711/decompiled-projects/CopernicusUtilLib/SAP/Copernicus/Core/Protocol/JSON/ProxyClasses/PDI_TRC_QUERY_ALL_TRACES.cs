﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_TRC_QUERY_ALL_TRACES
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_TRC_QUERY_ALL_TRACES : AbstractRemoteFunction<PDI_TRC_QUERY_ALL_TRACES.ImportingType, PDI_TRC_QUERY_ALL_TRACES.ExportingType, PDI_TRC_QUERY_ALL_TRACES.ChangingType, PDI_TRC_QUERY_ALL_TRACES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194EC88B8EF2C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SYNCH;
      [DataMember]
      public string IV_USER_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_LOCAL_TRACE_LIST;
      [DataMember]
      public string EV_INCIDENT_TRACE_LIST;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
