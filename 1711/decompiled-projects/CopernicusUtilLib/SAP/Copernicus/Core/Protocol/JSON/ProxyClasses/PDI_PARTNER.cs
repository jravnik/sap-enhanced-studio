﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PARTNER
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PARTNER
  {
    [DataMember]
    public string MANDT;
    [DataMember]
    public string PARTNER;
    [DataMember]
    public string PARTNER_ID;
    [DataMember]
    public string APPLICATION_COMP;
    [DataMember]
    public string PARTNERDOMAIN;
    [DataMember]
    public string PARTNER_CLASSISC;
    [DataMember]
    public string PARTNER_RESP_ORG;
    [DataMember]
    public string PSM_RELEVANT_IND;
    [DataMember]
    public string IS_MULTI_CUSTOMER_ENABLED;
  }
}
