﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_WS_TEMPLATE_ACTION
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_WS_TEMPLATE_ACTION : AbstractRemoteFunction<PDI_BC_WS_TEMPLATE_ACTION.ImportingType, PDI_BC_WS_TEMPLATE_ACTION.ExportingType, PDI_BC_WS_TEMPLATE_ACTION.ChangingType, PDI_BC_WS_TEMPLATE_ACTION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01151C1EE094F4F127DA4C09A7";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_NAMESPACE;
      [DataMember]
      public string IV_TEMPLATE_ID;
      [DataMember]
      public string IV_ACTION;
      [DataMember]
      public string IV_TEMPLATE_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
