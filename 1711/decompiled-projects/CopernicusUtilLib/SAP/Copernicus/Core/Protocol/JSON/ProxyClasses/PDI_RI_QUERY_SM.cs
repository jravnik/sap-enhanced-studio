﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_QUERY_SM
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_QUERY_SM : AbstractRemoteFunction<PDI_RI_QUERY_SM.ImportingType, PDI_RI_QUERY_SM.ExportingType, PDI_RI_QUERY_SM.ChangingType, PDI_RI_QUERY_SM.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DEFB2DD0AE03C8C0887";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public bool IV_SMT;
      [DataMember]
      public string IV_NAMESPACE;
      [DataMember]
      public string IV_NAME;
      [DataMember]
      public PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SM_SEL[] IT_SEL;
      [DataMember]
      public bool IV_CONV;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SM[] ET_PDI_QUERY_SM;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_QUERY_SM.PDI_RI_T_QUERY_SMT[] ET_PDI_QUERY_SMT;
    }

    [DataContract]
    public class PDI_RI_T_QUERY_SM
    {
      [DataMember]
      public string NAME;
      [DataMember]
      public string NAMESPACE;
      [DataMember]
      public string PROXYNAME;
    }

    [DataContract]
    public class PDI_RI_T_QUERY_SMT
    {
      [DataMember]
      public string NAME;
      [DataMember]
      public string PROXYNAME;
      [DataMember]
      public string SMTG_PROXYNAME;
      [DataMember]
      public string SMTG_NAME;
      [DataMember]
      public string SMTG_NAMESPACE;
    }

    [DataContract]
    public class PDI_RI_T_QUERY_SM_SEL
    {
      [DataMember]
      public string NAMESPACE;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
