﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.BusinessConfigurationHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class BusinessConfigurationHandler : JSONHandler, IBusinessConfigurationHandler
  {
    public void convertToCodelist(string toBeConvertedFilesXrepPath, out string[] createdFilesXrepPaths, out string[] deletedFilesXrepPaths)
    {
      createdFilesXrepPaths = (string[]) null;
      deletedFilesXrepPaths = (string[]) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_CODELIST_CONVERSION codelistConversion = new PDI_BC_CODELIST_CONVERSION();
      codelistConversion.Importing = new PDI_BC_CODELIST_CONVERSION.ImportingType();
      codelistConversion.Importing.IV_XREP_PATH = toBeConvertedFilesXrepPath;
      codelistConversion.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) codelistConversion, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (codelistConversion.Exporting == null)
        return;
      if (codelistConversion.Exporting.EV_SUCCESS == "X")
      {
        createdFilesXrepPaths = codelistConversion.Exporting.ET_FILE_CREATED;
        deletedFilesXrepPaths = codelistConversion.Exporting.ET_FILE_DELETED;
      }
      else
        this.reportServerSideProtocolException((SAPFunctionModule) codelistConversion, false, false);
    }

    public bool createBCView(string nsname, string bcoproxyname, out PDI_BC_BCVIEW_MAINTAIN.ExportingType ecoInfo)
    {
      return this.maintainBCView(nsname, bcoproxyname, "C", out ecoInfo);
    }

    public bool deleteBCView(string nsname, string bcoproxyname)
    {
      return this.maintainBCView(nsname, bcoproxyname, "D");
    }

    private bool maintainBCView(string nsname, string bcoproxyname, string mode)
    {
      PDI_BC_BCVIEW_MAINTAIN.ExportingType ecoInfo;
      return this.maintainBCView(nsname, bcoproxyname, mode, out ecoInfo);
    }

    private bool maintainBCView(string nsname, string bcoproxyname, string mode, out PDI_BC_BCVIEW_MAINTAIN.ExportingType ecoInfo)
    {
      ecoInfo = (PDI_BC_BCVIEW_MAINTAIN.ExportingType) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_BCVIEW_MAINTAIN bcBcviewMaintain = new PDI_BC_BCVIEW_MAINTAIN();
      bcBcviewMaintain.Importing = new PDI_BC_BCVIEW_MAINTAIN.ImportingType();
      bcBcviewMaintain.Importing.IV_NAMESPACE = nsname;
      bcBcviewMaintain.Importing.IV_PROXY_NAME = bcoproxyname;
      bcBcviewMaintain.Importing.IV_MODE = mode;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) bcBcviewMaintain, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return false;
      }
      if (bcBcviewMaintain.Exporting != null)
      {
        ecoInfo = bcBcviewMaintain.Exporting;
        if (bcBcviewMaintain.Exporting.EV_SUCCESS == "")
        {
          this.reportServerSideProtocolException((SAPFunctionModule) bcBcviewMaintain, false, false);
          return false;
        }
      }
      return true;
    }

    public BCT_S_BCSET_LIST[] checkBCSetOfBCO(string nsname, string bconame)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_BCO_BCSET_INFO pdiBcBcoBcsetInfo = new PDI_BC_BCO_BCSET_INFO();
      pdiBcBcoBcsetInfo.Importing = new PDI_BC_BCO_BCSET_INFO.ImportingType();
      pdiBcBcoBcsetInfo.Importing.IV_BCO_NAME = bconame;
      pdiBcBcoBcsetInfo.Importing.IV_NAMESPACE = nsname;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) pdiBcBcoBcsetInfo, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (BCT_S_BCSET_LIST[]) null;
      }
      return pdiBcBcoBcsetInfo.Exporting.ET_BCSET_LIST;
    }

    public PDI_BC_GET_ACTIVITIES.ExportingType getActivities(string wsid)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_GET_ACTIVITIES pdiBcGetActivities = new PDI_BC_GET_ACTIVITIES();
      pdiBcGetActivities.Importing = new PDI_BC_GET_ACTIVITIES.ImportingType();
      pdiBcGetActivities.Importing.IV_WSID = wsid;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) pdiBcGetActivities, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (PDI_BC_GET_ACTIVITIES.ExportingType) null;
      }
      return pdiBcGetActivities.Exporting;
    }

    public string getImplementationProjectTemplateID(string abapNamespace, string templateID, string templateType)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_WS_TEMPLATE_ID pdiBcWsTemplateId = new PDI_BC_WS_TEMPLATE_ID();
      pdiBcWsTemplateId.Importing = new PDI_BC_WS_TEMPLATE_ID.ImportingType();
      pdiBcWsTemplateId.Importing.IV_NAMESPACE = abapNamespace;
      pdiBcWsTemplateId.Importing.IV_TEMPLATE_ID = templateID.ToUpper();
      pdiBcWsTemplateId.Importing.IV_TEMPLATE_TYPE = templateType;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) pdiBcWsTemplateId, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (string) null;
      }
      if (pdiBcWsTemplateId.Exporting == null || !(pdiBcWsTemplateId.Exporting.EV_SUCCESS == ""))
        return pdiBcWsTemplateId.Exporting.EV_WS_ID;
      this.reportServerSideProtocolException((SAPFunctionModule) pdiBcWsTemplateId, false, false);
      return (string) null;
    }

    public bool completeImplementationProjectTemplate(string abapNamespace, string templateID, string templateType)
    {
      return this.wsAction(abapNamespace, templateID, "Complete", templateType);
    }

    public bool reopenImplementationProjectTemplate(string abapNamespace, string templateID, string templateType)
    {
      return this.wsAction(abapNamespace, templateID, "Reopen", templateType);
    }

    private bool wsAction(string abapNamespace, string templateID, string action, string templateType)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_WS_TEMPLATE_ACTION wsTemplateAction = new PDI_BC_WS_TEMPLATE_ACTION();
      wsTemplateAction.Importing = new PDI_BC_WS_TEMPLATE_ACTION.ImportingType();
      wsTemplateAction.Importing.IV_NAMESPACE = abapNamespace;
      wsTemplateAction.Importing.IV_TEMPLATE_ID = templateID;
      wsTemplateAction.Importing.IV_TEMPLATE_TYPE = templateType;
      wsTemplateAction.Importing.IV_ACTION = action;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) wsTemplateAction, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return false;
      }
      if (wsTemplateAction.Exporting != null)
      {
        if (wsTemplateAction.Exporting.EV_SUCCESS == "")
        {
          try
          {
            this.reportServerSideProtocolException((SAPFunctionModule) wsTemplateAction, false, false);
          }
          catch (Exception ex)
          {
            return false;
          }
        }
      }
      return true;
    }

    public PDI_S_BC_VALUE_DESCRIPTION[] getBCOValueHelp(string bcoProxyName, string bcNodeID, string fieldName, string abapNamespace, BCT_S_FIELD_VALUE_RANGE[] valueFilter)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_BCO_VALUE_HELP pdiBcBcoValueHelp = new PDI_BC_BCO_VALUE_HELP();
      pdiBcBcoValueHelp.Importing = new PDI_BC_BCO_VALUE_HELP.ImportingType();
      pdiBcBcoValueHelp.Importing.IV_NODE_ID = bcNodeID;
      pdiBcBcoValueHelp.Importing.IV_FIELD_NAME = fieldName;
      pdiBcBcoValueHelp.Importing.IV_NAMESPACE = abapNamespace;
      pdiBcBcoValueHelp.Importing.IT_VALUE_FILTER = valueFilter;
      pdiBcBcoValueHelp.Importing.IV_BCO_PROXYNAME = bcoProxyName;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) pdiBcBcoValueHelp, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (PDI_S_BC_VALUE_DESCRIPTION[]) null;
      }
      if (pdiBcBcoValueHelp.Exporting == null)
        return (PDI_S_BC_VALUE_DESCRIPTION[]) null;
      if (pdiBcBcoValueHelp.Exporting.EV_SUCCESS == "")
        this.reportServerSideProtocolException((SAPFunctionModule) pdiBcBcoValueHelp, false, false);
      return pdiBcBcoValueHelp.Exporting.ET_VALUE_DESCRIPTION;
    }

    public PDI_S_BC_VALUE_DESCRIPTION[] getBCOForeignKeyValues(string bcNodeID, string fieldName, string abapNamespace, BCT_S_FIELD_VALUE_RANGE[] valueFilter)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_BCO_FOREIGN_KEY_VALUES foreignKeyValues = new PDI_BC_BCO_FOREIGN_KEY_VALUES();
      foreignKeyValues.Importing = new PDI_BC_BCO_FOREIGN_KEY_VALUES.ImportingType();
      foreignKeyValues.Importing.IV_NODE_ID = bcNodeID;
      foreignKeyValues.Importing.IV_FIELD_NAME = fieldName;
      foreignKeyValues.Importing.IV_NAMESPACE = abapNamespace;
      foreignKeyValues.Importing.IT_VALUE_FILTER = valueFilter;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) foreignKeyValues, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (PDI_S_BC_VALUE_DESCRIPTION[]) null;
      }
      if (foreignKeyValues.Exporting != null)
      {
        if (!(foreignKeyValues.Exporting.EV_SUCCESS == ""))
          return foreignKeyValues.Exporting.ET_VALUE_DESCRIPTION;
        this.reportServerSideProtocolException((SAPFunctionModule) foreignKeyValues, false, false);
      }
      return (PDI_S_BC_VALUE_DESCRIPTION[]) null;
    }

    public PDI_S_BC_VALUE_DESCRIPTION[] getCodeValues(string nsName, string codeProxyName)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_BCO_FOREIGN_KEY_VALUES foreignKeyValues = new PDI_BC_BCO_FOREIGN_KEY_VALUES();
      foreignKeyValues.Importing = new PDI_BC_BCO_FOREIGN_KEY_VALUES.ImportingType();
      foreignKeyValues.Importing.IV_NAMESPACE = nsName;
      foreignKeyValues.Importing.IV_CODE_PROXYNAME = codeProxyName;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) foreignKeyValues, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (PDI_S_BC_VALUE_DESCRIPTION[]) null;
      }
      if (foreignKeyValues.Exporting != null)
      {
        if (!(foreignKeyValues.Exporting.EV_SUCCESS == ""))
          return foreignKeyValues.Exporting.ET_VALUE_DESCRIPTION;
        this.reportServerSideProtocolException((SAPFunctionModule) foreignKeyValues, false, false);
      }
      return (PDI_S_BC_VALUE_DESCRIPTION[]) null;
    }

    public void deployBusinessConfiguration(string iv_component_name, string sessionID, bool bacDefined = false)
    {
      CopernicusStatusBar.Instance.ShowMessage(Resource.BCDeploymentStarted);
      Cursor.Current = Cursors.WaitCursor;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_BAC_DEPLOYMENT_BATCH bacDeploymentBatch = new PDI_BC_BAC_DEPLOYMENT_BATCH();
      bacDeploymentBatch.Importing = new PDI_BC_BAC_DEPLOYMENT_BATCH.ImportingType();
      bacDeploymentBatch.Importing.IV_COMPONENT_NAME = iv_component_name;
      bacDeploymentBatch.Importing.IV_SESSION_ID = sessionID;
      if (bacDefined)
        bacDeploymentBatch.Importing.IV_BAC_DEFINED = "X";
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) bacDeploymentBatch, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return;
      }
      if (bacDeploymentBatch.Exporting != null)
      {
        if (bacDeploymentBatch.Exporting.EV_SUCCESS == "X")
        {
          bool flag = false;
          int millisecondsTimeout = 750;
          string evApplogId = bacDeploymentBatch.Exporting.EV_APPLOG_ID;
          string evJobcnt = bacDeploymentBatch.Exporting.EV_JOBCNT;
          string evJobname = bacDeploymentBatch.Exporting.EV_JOBNAME;
          BatchGetStatusHandler getStatusHandler = new BatchGetStatusHandler();
          PDI_LM_BATCH_GET_STATUS.ExportingType exportingType = (PDI_LM_BATCH_GET_STATUS.ExportingType) null;
          while (!flag)
          {
            Cursor.Current = Cursors.WaitCursor;
            Thread.Sleep(millisecondsTimeout);
            if (millisecondsTimeout < 6000)
              millisecondsTimeout *= 2;
            PDI_LM_T_MSG_LIST[] msgs = (PDI_LM_T_MSG_LIST[]) null;
            exportingType = getStatusHandler.getBatchStatus(evJobname, evJobcnt, evApplogId, out msgs);
            if (exportingType.EV_JOB_STATE.Equals("05") || exportingType.EV_JOB_STATE.Equals("07"))
              flag = true;
            if (exportingType.ET_MESSAGES.Length > 0)
              CopernicusStatusBar.Instance.ShowMessage(exportingType.ET_MESSAGES[exportingType.ET_MESSAGES.Length - 1].TEXT);
          }
          Cursor.Current = Cursors.Default;
          if ((exportingType.EV_SUCCESS == "" || exportingType.EV_PROC_SUCC == "") && (exportingType.ET_MESSAGES != null && exportingType.ET_MESSAGES.Length > 0))
          {
            List<SAP.Copernicus.Core.Protocol.JSON.Message> msgs = new List<SAP.Copernicus.Core.Protocol.JSON.Message>();
            foreach (PDI_RI_S_MESSAGE pdiRiSMessage in exportingType.ET_MESSAGES)
            {
              SAP.Copernicus.Core.Protocol.JSON.Message.MessageSeverity severity = SAP.Copernicus.Core.Protocol.JSON.Message.MessageSeverity.FromABAPCode(pdiRiSMessage.SEVERITY);
              msgs.Add(new SAP.Copernicus.Core.Protocol.JSON.Message(severity, pdiRiSMessage.TEXT));
            }
            new ProtocolException(ProtocolException.ErrorArea.SERVER, msgs).showPopup();
            return;
          }
        }
        else if (bacDeploymentBatch.Exporting.EV_SUCCESS == "")
        {
          CopernicusStatusBar.Instance.ShowMessage(Resource.BCDeploymentFailed);
          this.reportServerSideProtocolException((SAPFunctionModule) bacDeploymentBatch, false, false);
          return;
        }
      }
      CopernicusStatusBar.Instance.ShowMessage(Resource.BCDeploymentFinished);
    }

    public BAC_PDI_S_TREE_ELEMENT[] getBACTreeNodes(int types, string[] bac_ids)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_GET_BAC_ELEMENTS bcGetBacElements = new PDI_BC_GET_BAC_ELEMENTS();
      bcGetBacElements.Importing = new PDI_BC_GET_BAC_ELEMENTS.ImportingType();
      bcGetBacElements.Importing.IV_TYPES = types;
      if (bac_ids != null)
      {
        bcGetBacElements.Importing.IT_BAC_IDS = new BCT_S_BAC_ELEMENT_ID[bac_ids.Length];
        for (int index = 0; index < bac_ids.Length; ++index)
          bcGetBacElements.Importing.IT_BAC_IDS[index] = new BCT_S_BAC_ELEMENT_ID()
          {
            ELEMENT_ID = bac_ids[index]
          };
      }
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) bcGetBacElements, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (BAC_PDI_S_TREE_ELEMENT[]) null;
      }
      if (bcGetBacElements.Exporting != null)
      {
        if (!(bcGetBacElements.Exporting.EV_SUCCESS == "") || bcGetBacElements.Exporting.ET_MESSAGES == null || bcGetBacElements.Exporting.ET_MESSAGES.Length <= 0)
          return bcGetBacElements.Exporting.ET_BAC_NODES;
        this.reportServerSideProtocolException((SAPFunctionModule) bcGetBacElements, false, false);
      }
      return (BAC_PDI_S_TREE_ELEMENT[]) null;
    }

    public BAC_PDI_S_TREE_ELEMENT[] getBACTreeNodesBySemanticLabel(string[] bac_ids)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_GET_BAC_ELEMENTS bcGetBacElements = new PDI_BC_GET_BAC_ELEMENTS();
      bcGetBacElements.Importing = new PDI_BC_GET_BAC_ELEMENTS.ImportingType();
      bcGetBacElements.Importing.IV_SEMANTIC_LABEL = true;
      if (bac_ids != null)
      {
        bcGetBacElements.Importing.IT_BAC_IDS = new BCT_S_BAC_ELEMENT_ID[bac_ids.Length];
        for (int index = 0; index < bac_ids.Length; ++index)
          bcGetBacElements.Importing.IT_BAC_IDS[index] = new BCT_S_BAC_ELEMENT_ID()
          {
            ELEMENT_ID = bac_ids[index]
          };
      }
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) bcGetBacElements, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (BAC_PDI_S_TREE_ELEMENT[]) null;
      }
      if (bcGetBacElements.Exporting != null)
      {
        if (!(bcGetBacElements.Exporting.EV_SUCCESS == "") || bcGetBacElements.Exporting.ET_MESSAGES == null || bcGetBacElements.Exporting.ET_MESSAGES.Length <= 0)
          return bcGetBacElements.Exporting.ET_BAC_NODES;
        this.reportServerSideProtocolException((SAPFunctionModule) bcGetBacElements, false, false);
      }
      return (BAC_PDI_S_TREE_ELEMENT[]) null;
    }

    public PDI_S_BCO_SCHEMA_NODE[] getBCOInformation(string proxyname, string abapNamespace, out string bcoDescription, out PDI_S_APPLICATION_F4[] valueHelp, out bool ftViewExists)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_BCO_INFORMATION bcBcoInformation = new PDI_BC_BCO_INFORMATION();
      bcBcoInformation.Importing = new PDI_BC_BCO_INFORMATION.ImportingType();
      bcBcoInformation.Importing.IV_PROXY_NAME = proxyname;
      bcBcoInformation.Importing.IV_NAMESPACE = abapNamespace;
      bcoDescription = (string) null;
      valueHelp = (PDI_S_APPLICATION_F4[]) null;
      ftViewExists = false;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) bcBcoInformation, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (PDI_S_BCO_SCHEMA_NODE[]) null;
      }
      if (bcBcoInformation.Exporting != null)
      {
        if (bcBcoInformation.Exporting.EV_SUCCESS == "" && bcBcoInformation.Exporting.ET_MESSAGES != null && bcBcoInformation.Exporting.ET_MESSAGES.Length > 0)
        {
          this.reportServerSideProtocolException((SAPFunctionModule) bcBcoInformation, false, false);
        }
        else
        {
          bcoDescription = bcBcoInformation.Exporting.EV_BCO_DESCRIPTION;
          valueHelp = bcBcoInformation.Exporting.ET_APPLICATION_F4;
          if (bcBcoInformation.Exporting.EV_FT_VIEW_EXIST != null && bcBcoInformation.Exporting.EV_FT_VIEW_EXIST.ToUpper().Equals("X"))
            ftViewExists = true;
          return bcBcoInformation.Exporting.ET_BCO_INFO;
        }
      }
      return (PDI_S_BCO_SCHEMA_NODE[]) null;
    }

    public bool checkConsistency(string content, PDI_S_CC_BCNODE_LINE[] nodes, out BCE_S_DTE_CHECK_ERROR_OCC[] occurences)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_BCO_CONS_CHECK pdiBcBcoConsCheck = new PDI_BC_BCO_CONS_CHECK();
      pdiBcBcoConsCheck.Importing = new PDI_BC_BCO_CONS_CHECK.ImportingType();
      pdiBcBcoConsCheck.Importing.IV_CONTENT = content;
      pdiBcBcoConsCheck.Importing.IT_SCHEMA_IDS = nodes;
      occurences = (BCE_S_DTE_CHECK_ERROR_OCC[]) null;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) pdiBcBcoConsCheck, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return false;
      }
      if (pdiBcBcoConsCheck.Exporting == null || !(pdiBcBcoConsCheck.Exporting.EV_SUCCESS == ""))
        return true;
      if (pdiBcBcoConsCheck.Exporting.ET_MESSAGES != null)
      {
        if (pdiBcBcoConsCheck.Exporting.ET_MESSAGES.Length > 0)
        {
          try
          {
            this.reportServerSideProtocolException((SAPFunctionModule) pdiBcBcoConsCheck, false, false);
          }
          catch (Exception ex)
          {
          }
          occurences = pdiBcBcoConsCheck.Exporting.ET_OCCURRENCES;
        }
      }
      return false;
    }

    public PDI_BC_COUNTRY_CODE[] getCountryCode(string langu)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_GET_COUNTRY_CODES bcGetCountryCodes = new PDI_BC_GET_COUNTRY_CODES();
      bcGetCountryCodes.Importing = new PDI_BC_GET_COUNTRY_CODES.ImportingType();
      bcGetCountryCodes.Importing.IV_LANGU = langu;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) bcGetCountryCodes, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (PDI_BC_COUNTRY_CODE[]) null;
      }
      if (bcGetCountryCodes.Exporting != null)
      {
        if (!(bcGetCountryCodes.Exporting.EV_SUCCESS == "") || bcGetCountryCodes.Exporting.ET_MESSAGES == null || bcGetCountryCodes.Exporting.ET_MESSAGES.Length <= 0)
          return bcGetCountryCodes.Exporting.ET_COUNTRY_CODES;
        this.reportServerSideProtocolException((SAPFunctionModule) bcGetCountryCodes, false, false);
      }
      return (PDI_BC_COUNTRY_CODE[]) null;
    }

    public PDI_BC_CODE_GDT[] getCodeGDT(string nspace)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_GET_CODE_GDT pdiBcGetCodeGdt = new PDI_BC_GET_CODE_GDT();
      pdiBcGetCodeGdt.Importing = new PDI_BC_GET_CODE_GDT.ImportingType();
      pdiBcGetCodeGdt.Importing.IV_NAMESPACE = nspace;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) pdiBcGetCodeGdt, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (PDI_BC_CODE_GDT[]) null;
      }
      if (pdiBcGetCodeGdt.Exporting != null)
      {
        if (!(pdiBcGetCodeGdt.Exporting.EV_SUCCESS == "") || pdiBcGetCodeGdt.Exporting.ET_MESSAGES == null || pdiBcGetCodeGdt.Exporting.ET_MESSAGES.Length <= 0)
          return pdiBcGetCodeGdt.Exporting.ET_CODES;
        this.reportServerSideProtocolException((SAPFunctionModule) pdiBcGetCodeGdt, false, false);
      }
      return (PDI_BC_CODE_GDT[]) null;
    }
  }
}
