﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_ADM_SESSION_READ_USERS
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_ADM_SESSION_READ_USERS : AbstractRemoteFunction<PDI_RI_ADM_SESSION_READ_USERS.ImportingType, PDI_RI_ADM_SESSION_READ_USERS.ExportingType, PDI_RI_ADM_SESSION_READ_USERS.ChangingType, PDI_RI_ADM_SESSION_READ_USERS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DEFB1AB1FDA319B5969";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_ADM_SESSION_USER[] ET_USER_IDENTITY;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
