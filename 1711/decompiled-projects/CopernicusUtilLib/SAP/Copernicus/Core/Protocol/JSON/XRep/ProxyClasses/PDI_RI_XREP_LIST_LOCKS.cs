﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_XREP_LIST_LOCKS
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses
{
  [DataContract]
  public class PDI_RI_XREP_LIST_LOCKS : AbstractRemoteFunction<PDI_RI_XREP_LIST_LOCKS.ImportingType, PDI_RI_XREP_LIST_LOCKS.ExportingType, PDI_RI_XREP_LIST_LOCKS.ChangingType, PDI_RI_XREP_LIST_LOCKS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB722EC9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_USER;
      [DataMember]
      public string IV_PATH;
      [DataMember]
      public string IV_SESSION_ID;
      [DataMember]
      public string[] IT_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_XREP_LOCK[] ET_LOCK;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
