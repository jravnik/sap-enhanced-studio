﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PROJECT_ITEM
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PROJECT_ITEM
  {
    [DataMember]
    public string PROJECT_ITEM_KEY;
    [DataMember]
    public string PROJ_ITEM_TYPE;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string LM_CONTENT_TYPE;
    [DataMember]
    public string KEY_USER_REQUIRED;
    [DataMember]
    public string ADD_ITEM_RELEVANT;
    [DataMember]
    public string DEL_ALLOWED_IN_MAINT;
  }
}
