﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_BOG_SYNTAX_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_BOG_SYNTAX_CHECK : AbstractRemoteFunction<PDI_RI_BOG_SYNTAX_CHECK.ImportingType, PDI_RI_BOG_SYNTAX_CHECK.ExportingType, PDI_RI_BOG_SYNTAX_CHECK.ChangingType, PDI_RI_BOG_SYNTAX_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB7216C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ESR_NAMESPACE;
      [DataMember]
      public string IV_ESR_BO_NAME;
      [DataMember]
      public string IV_PROXY_BO_NAME;
      [DataMember]
      public string IV_ROOT_PATH;
      [DataMember]
      public string IV_PARTNER_NAMESPACE;
      [DataMember]
      public string IV_XREP_SESSION_ID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_SFW_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
