﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_WSID_GET_TEMPLATE_REQUEST
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_WSID_GET_TEMPLATE_REQUEST : AbstractRemoteFunction<PDI_WSID_GET_TEMPLATE_REQUEST.ImportingType, PDI_WSID_GET_TEMPLATE_REQUEST.ExportingType, PDI_WSID_GET_TEMPLATE_REQUEST.ChangingType, PDI_WSID_GET_TEMPLATE_REQUEST.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19D807D9E00955700";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_OUTBOUND_SERVICE_INTERFACE;
      [DataMember]
      public string IV_OPERATION_PRX_NAME;
      [DataMember]
      public string IV_OPERATION_NAME;
      [DataMember]
      public string IV_XML_NAMESPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_TEMPLATE_REQUEST;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
