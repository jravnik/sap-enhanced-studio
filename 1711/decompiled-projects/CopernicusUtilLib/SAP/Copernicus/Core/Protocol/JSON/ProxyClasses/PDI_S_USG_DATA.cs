﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_USG_DATA
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_USG_DATA
  {
    [DataMember]
    public string SOURCE_FILE_NAME;
    [DataMember]
    public string TARGET_ENTITY_NAME;
    [DataMember]
    public int USAGE_TYPE;
    [DataMember]
    public char CROSS_DU;
    [DataMember]
    public char WRITE_ACCESS;
    [DataMember]
    public char DEFINES;
    [DataMember]
    public string PROCESS_COMPONENT;
  }
}
