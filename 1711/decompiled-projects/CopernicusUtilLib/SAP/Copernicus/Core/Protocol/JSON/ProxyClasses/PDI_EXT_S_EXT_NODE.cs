﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_S_EXT_NODE
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_S_EXT_NODE
  {
    [DataMember]
    public string BO_ESR_NAMESPACE;
    [DataMember]
    public string BO_ESR_NAME;
    [DataMember]
    public string MAX_BO_PRX_NAME;
    [DataMember]
    public string ND_NAME;
    [DataMember]
    public string PRX_BO_NAME;
    [DataMember]
    public string PRX_ND_NAME;
    [DataMember]
    public string NODE_ID;
    [DataMember]
    public string ND_UUID;
    [DataMember]
    public string PARENT_NODE_NAME;
    [DataMember]
    public string ORDINAL_NUMBER;
    [DataMember]
    public string IS_EXTENSIBLE;
    [DataMember]
    public string HI_LEVEL;
    [DataMember]
    public string PSM_RELEASED;
    [DataMember]
    public string OFF_ENABLED;
  }
}
