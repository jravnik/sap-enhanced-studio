﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.ABAPUnitTestHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class ABAPUnitTestHandler : JSONHandler
  {
    public bool ExecuteABAPUnitTest(string abapUnitTestClass, string contextData, out PDI_RI_S_MESSAGE[] messages)
    {
      messages = new PDI_RI_S_MESSAGE[0];
      try
      {
        JSONClient jsonTestClient = Client.getInstance().getJSONTestClient(false);
        PDI_HT_UNIT_TEST pdiHtUnitTest = new PDI_HT_UNIT_TEST();
        pdiHtUnitTest.Importing = new PDI_HT_UNIT_TEST.ImportingType();
        pdiHtUnitTest.Importing.IV_ABAP_CLASS_NAME = abapUnitTestClass;
        pdiHtUnitTest.Importing.IV_CONTEXT_DATA = contextData;
        jsonTestClient.callFunctionModule((SAPFunctionModule) pdiHtUnitTest, false, false, false);
        if (pdiHtUnitTest.Exporting != null)
        {
          string evSuccess = pdiHtUnitTest.Exporting.EV_SUCCESS;
          messages = pdiHtUnitTest.Exporting.ET_MESSAGES;
          foreach (PDI_RI_S_MESSAGE pdiRiSMessage in messages)
          {
            if (pdiRiSMessage.SEVERITY.Equals("T") && pdiRiSMessage.TEXT.StartsWith("Tolerable Assertion Error: '"))
              pdiRiSMessage.TEXT = pdiRiSMessage.TEXT.Substring(28, pdiRiSMessage.TEXT.Length - 29);
            if (pdiRiSMessage.SEVERITY.Equals("C") && pdiRiSMessage.TEXT.StartsWith("Critical Assertion Error: '"))
              pdiRiSMessage.TEXT = pdiRiSMessage.TEXT.Substring(27, pdiRiSMessage.TEXT.Length - 28);
            if (pdiRiSMessage.SEVERITY.Equals("F") && pdiRiSMessage.TEXT.StartsWith("Fatal Assertion Error: '"))
              pdiRiSMessage.TEXT = pdiRiSMessage.TEXT.Substring(25, pdiRiSMessage.TEXT.Length - 26);
          }
          return evSuccess.Equals("X");
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }
  }
}
