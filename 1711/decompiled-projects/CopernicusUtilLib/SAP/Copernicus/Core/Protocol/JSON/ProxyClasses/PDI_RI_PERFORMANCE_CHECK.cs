﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_PERFORMANCE_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_RI_PERFORMANCE_CHECK : AbstractRemoteFunction<PDI_RI_PERFORMANCE_CHECK.ImportingType, PDI_RI_PERFORMANCE_CHECK.ExportingType, PDI_RI_PERFORMANCE_CHECK.ChangingType, PDI_RI_PERFORMANCE_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F51EE7A4A5B34BB478C026";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string[] IT_XREP_FILE_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_LM_T_MSG_LIST[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
