﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_T_APPLOG_MSG
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_T_APPLOG_MSG
  {
    [DataMember]
    public string MANDT;
    [DataMember]
    public string LOG_ID;
    [DataMember]
    public string LOG_TIMESTAMP;
    [DataMember]
    public string LOG_MSG;
    [DataMember]
    public string LOG_SEVERITY;
    [DataMember]
    public string LOG_LEVEL;
    [DataMember]
    public string LOG_DETAIL;
  }
}
