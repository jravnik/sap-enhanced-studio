﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.BODLBackendCompilerHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class BODLBackendCompilerHandler : JSONHandler
  {
    public bool binaryMode;

    public IDictionary<string, string> Compile(string bodlSource, string mdrsNamespace, string filePath, string compilerOption, bool withABSL, bool withQuery, bool withSAM, out bool success, out PDI_LM_S_MSG_LIST[] ET_MSG_LIST)
    {
      return this.Compile(bodlSource, mdrsNamespace, filePath, withABSL, withQuery, withSAM, compilerOption, out success, out ET_MSG_LIST);
    }

    private IDictionary<string, string> Compile(string bodlSource, string mdrsNamespace, string filePath, bool withABSL, bool withQuery, bool withSAM, string compilerOption, out bool success, out PDI_LM_S_MSG_LIST[] ET_MSG_LIST)
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      ET_MSG_LIST = (PDI_LM_S_MSG_LIST[]) null;
      success = true;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_BODL_COMPILER pdiBodlCompiler = new PDI_BODL_COMPILER();
        pdiBodlCompiler.Importing = new PDI_BODL_COMPILER.ImportingType();
        pdiBodlCompiler.Importing.IV_MDRS_NAMESPACE = mdrsNamespace;
        pdiBodlCompiler.Importing.IV_XREP_FILE_PATH = filePath;
        pdiBodlCompiler.Importing.IV_WITH_ABSL = withABSL;
        pdiBodlCompiler.Importing.IV_WITH_QUERY = withQuery;
        pdiBodlCompiler.Importing.IV_WITH_SAM = withSAM;
        pdiBodlCompiler.Importing.IV_COMPILER_OPTION = compilerOption;
        pdiBodlCompiler.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        if (bodlSource != null && !this.binaryMode)
          bodlSource = Convert.ToBase64String(Encoding.UTF8.GetBytes(bodlSource));
        pdiBodlCompiler.Importing.IV_BODL_SOURCE = bodlSource;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiBodlCompiler, false, false, false);
        if (pdiBodlCompiler.Exporting != null)
        {
          ET_MSG_LIST = pdiBodlCompiler.Exporting.ET_MSG_LIST;
          success = pdiBodlCompiler.Exporting.EV_SUCCESS.Equals("X");
          foreach (OSLS_XREP_FILE_ATTR oslsXrepFileAttr in pdiBodlCompiler.Exporting.ET_ATTR)
          {
            if (dictionary.ContainsKey(oslsXrepFileAttr.NAME))
              dictionary.Remove(oslsXrepFileAttr.NAME);
            dictionary.Add(oslsXrepFileAttr.NAME, oslsXrepFileAttr.VALUE);
          }
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (IDictionary<string, string>) dictionary;
    }
  }
}
