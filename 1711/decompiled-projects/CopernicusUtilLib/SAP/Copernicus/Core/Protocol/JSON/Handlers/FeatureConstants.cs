﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.FeatureConstants
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class FeatureConstants
  {
    public const string queryFeed = "QRY_FEED";
    public const string queryTTnav = "QRY_ES_TT_NAV";
    public const string firstImplProjTempl = "IMPL_PROJ_TMPL";
    public const string fineTuneImplProjTempl = "FT_IMPL_PROJ_TMPL";
  }
}
