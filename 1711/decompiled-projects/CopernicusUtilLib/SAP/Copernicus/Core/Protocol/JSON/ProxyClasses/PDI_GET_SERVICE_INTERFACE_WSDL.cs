﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_SERVICE_INTERFACE_WSDL
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_GET_SERVICE_INTERFACE_WSDL : AbstractRemoteFunction<PDI_GET_SERVICE_INTERFACE_WSDL.ImportingType, PDI_GET_SERVICE_INTERFACE_WSDL.ExportingType, PDI_GET_SERVICE_INTERFACE_WSDL.ChangingType, PDI_GET_SERVICE_INTERFACE_WSDL.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0520391ED389883E64A3D3844C";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SERVICE_INTERFACE_PRX_NAME;
      [DataMember]
      public string IV_SERVICE_INTERFACE_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_WSDL_URI;
      [DataMember]
      public string EV_WSDL_CONTENT;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
