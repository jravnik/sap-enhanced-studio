﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRODUCT_CHECK_BATCH
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_LM_PRODUCT_CHECK_BATCH : AbstractRemoteFunction<PDI_LM_PRODUCT_CHECK_BATCH.ImportingType, PDI_LM_PRODUCT_CHECK_BATCH.ExportingType, PDI_LM_PRODUCT_CHECK_BATCH.ChangingType, PDI_LM_PRODUCT_CHECK_BATCH.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F91ED5B29961B8C79EC115";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_USER;
      [DataMember]
      public string IV_MODE;
      [DataMember]
      public PDI_S_CHECK_SCOPE IS_CHECK_SCOPE;
      [DataMember]
      public string IV_SESSION_ID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_JOBNAME;
      [DataMember]
      public string EV_JOBCNT;
      [DataMember]
      public string EV_APPLOG_ID;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
