﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_IS_BCS_FOR_SAPBCO_ALLWD
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_IS_BCS_FOR_SAPBCO_ALLWD : AbstractRemoteFunction<PDI_LM_IS_BCS_FOR_SAPBCO_ALLWD.ImportingType, PDI_LM_IS_BCS_FOR_SAPBCO_ALLWD.ExportingType, PDI_LM_IS_BCS_FOR_SAPBCO_ALLWD.ChangingType, PDI_LM_IS_BCS_FOR_SAPBCO_ALLWD.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19CCEEB05FD865D0F";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_IS_ALLOWED;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
