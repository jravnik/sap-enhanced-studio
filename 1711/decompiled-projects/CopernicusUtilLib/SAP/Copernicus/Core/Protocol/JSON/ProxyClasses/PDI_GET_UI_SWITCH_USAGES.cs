﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_UI_SWITCH_USAGES
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_GET_UI_SWITCH_USAGES : AbstractRemoteFunction<PDI_GET_UI_SWITCH_USAGES.ImportingType, PDI_GET_UI_SWITCH_USAGES.ExportingType, PDI_GET_UI_SWITCH_USAGES.ChangingType, PDI_GET_UI_SWITCH_USAGES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E03836E1ED2ACE76F15EFB1968D";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_UI_SWITCH_ID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string[] ET_UICHANGE_TR_XREP_PATHS;
      [DataMember]
      public string[] ET_UI_XREP_PATHS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
