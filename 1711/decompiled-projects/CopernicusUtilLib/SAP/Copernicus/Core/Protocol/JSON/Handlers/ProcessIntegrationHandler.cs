﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.ProcessIntegrationHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class ProcessIntegrationHandler : JSONHandler
  {
    public bool ActivatePI(string XrepPath)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_PI_ACTIVATE pdiPiActivate = new PDI_PI_ACTIVATE();
        pdiPiActivate.Importing = new PDI_PI_ACTIVATE.ImportingType();
        pdiPiActivate.Importing.IV_XREP_PATH = XrepPath;
        pdiPiActivate.Importing.IV_CONFIG = "X";
        jsonClient.callFunctionModule((SAPFunctionModule) pdiPiActivate, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiPiActivate, false, false);
        if (pdiPiActivate.Exporting != null)
          return pdiPiActivate.Exporting.EV_SUCCESS.Equals("X");
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }

    public bool ConfigurePI(string XrepPath, bool enabled)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_PI_CONFIG pdiPiConfig = new PDI_PI_CONFIG();
        pdiPiConfig.Importing = new PDI_PI_CONFIG.ImportingType();
        pdiPiConfig.Importing.IV_XREP_PATH = XrepPath;
        pdiPiConfig.Importing.IV_ENABLED = enabled ? "X" : "";
        jsonClient.callFunctionModule((SAPFunctionModule) pdiPiConfig, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiPiConfig, false, false);
        if (pdiPiConfig.Exporting != null)
          return pdiPiConfig.Exporting.EV_SUCCESS.Equals("X");
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }

    public bool DeletePI(string XrepPath)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_PI_DELETE pdiPiDelete = new PDI_PI_DELETE();
        pdiPiDelete.Importing = new PDI_PI_DELETE.ImportingType();
        pdiPiDelete.Importing.IV_XREP_PATH = XrepPath;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiPiDelete, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiPiDelete, false, false);
        if (pdiPiDelete.Exporting != null)
          return pdiPiDelete.Exporting.EV_SUCCESS.Equals("X");
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }

    public bool GetSchemaDefinition(string XrepPath, out string schemaDefinition)
    {
      schemaDefinition = "";
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_PI_GET_SCHEMA pdiPiGetSchema = new PDI_PI_GET_SCHEMA();
        pdiPiGetSchema.Importing = new PDI_PI_GET_SCHEMA.ImportingType();
        pdiPiGetSchema.Importing.IV_XREP_PATH = XrepPath;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiPiGetSchema, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiPiGetSchema, false, false);
        if (pdiPiGetSchema.Exporting != null)
        {
          schemaDefinition = pdiPiGetSchema.Exporting.EV_SCHEMA;
          return pdiPiGetSchema.Exporting.EV_SUCCESS.Equals("X");
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }

    public void CheckWSDL(string IV_URI, out string EV_MESSAGE, out string EV_SUCCESS)
    {
      PDI_PI_CHECK_WSDL pdiPiCheckWsdl = new PDI_PI_CHECK_WSDL();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiPiCheckWsdl.Importing = new PDI_PI_CHECK_WSDL.ImportingType();
        pdiPiCheckWsdl.Importing.IV_URI = IV_URI;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiPiCheckWsdl, false, false, false);
      }
      catch (Exception ex)
      {
        EV_MESSAGE = "Severe error in JSONClient: " + ex.Message;
        EV_SUCCESS = "";
        return;
      }
      EV_MESSAGE = pdiPiCheckWsdl.Exporting.EV_MESSAGE;
      EV_SUCCESS = pdiPiCheckWsdl.Exporting.EV_SUCCESS;
    }
  }
}
