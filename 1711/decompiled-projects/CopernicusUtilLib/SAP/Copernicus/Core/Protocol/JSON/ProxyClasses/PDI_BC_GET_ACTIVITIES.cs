﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_GET_ACTIVITIES
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_GET_ACTIVITIES : AbstractRemoteFunction<PDI_BC_GET_ACTIVITIES.ImportingType, PDI_BC_GET_ACTIVITIES.ExportingType, PDI_BC_GET_ACTIVITIES.ChangingType, PDI_BC_GET_ACTIVITIES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0135921ED0ABA3FADE36975FA0";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_WSID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_S_ACTIVITY[] ET_ACTIVITIES;
      [DataMember]
      public string EV_DES_ACC_ENABLED;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
