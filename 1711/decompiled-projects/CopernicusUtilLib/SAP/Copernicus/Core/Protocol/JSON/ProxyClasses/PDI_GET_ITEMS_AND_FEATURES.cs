﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_ITEMS_AND_FEATURES
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_GET_ITEMS_AND_FEATURES : AbstractRemoteFunction<PDI_GET_ITEMS_AND_FEATURES.ImportingType, PDI_GET_ITEMS_AND_FEATURES.ExportingType, PDI_GET_ITEMS_AND_FEATURES.ChangingType, PDI_GET_ITEMS_AND_FEATURES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E028DAE1ED28B98E1F56FCED26A";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SAP_SOLUTION_TYPE;
      [DataMember]
      public string IV_PDI_SUBTYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_PROJECT_ITEM_SOLUTION[] ET_PROJECT_ITEM_SOLUTION;
      [DataMember]
      public PDI_PROJECT_ITEM[] ET_PROJECT_ITEM;
      [DataMember]
      public PDI_FEATURE[] ET_FEATURE;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
