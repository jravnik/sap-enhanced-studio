﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.UIGenerationHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class UIGenerationHandler : JSONHandler
  {
    public string UpdateScoping(string workcenterName, string workcenterViewName, out string returnValue)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_UI_UPDATE_SCOPING pdiUiUpdateScoping = new PDI_UI_UPDATE_SCOPING();
        pdiUiUpdateScoping.Importing = new PDI_UI_UPDATE_SCOPING.ImportingType();
        string[] strArray1 = new string[1]{ workcenterName };
        pdiUiUpdateScoping.Importing.IT_WORKCENTERS = strArray1;
        string[] strArray2 = new string[1]{ workcenterViewName };
        pdiUiUpdateScoping.Importing.IT_WOCVIEWS = strArray2;
        pdiUiUpdateScoping.Importing.IV_TRIGGER_RBAM_GENERATION = true;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiUiUpdateScoping, false, false, false);
        if (pdiUiUpdateScoping.Exporting != null)
        {
          returnValue = pdiUiUpdateScoping.Exporting.EV_RC;
          return returnValue;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      returnValue = "empty";
      return returnValue;
    }
  }
}
