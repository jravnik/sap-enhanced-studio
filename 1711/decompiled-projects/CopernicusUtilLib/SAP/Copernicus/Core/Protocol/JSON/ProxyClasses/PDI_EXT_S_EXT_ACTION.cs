﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_S_EXT_ACTION
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_S_EXT_ACTION
  {
    [DataMember]
    public string PRX_BO_NAME;
    [DataMember]
    public string PRX_NODE_NAME;
    [DataMember]
    public string PRX_ACTION_NAME;
    [DataMember]
    public string ESR_ACTION_NAME;
    [DataMember]
    public string PSM_RELEASED;
    [DataMember]
    public string CUSTOM_ACTION;
    [DataMember]
    public string NODE_OFF_ENABLED;
  }
}
