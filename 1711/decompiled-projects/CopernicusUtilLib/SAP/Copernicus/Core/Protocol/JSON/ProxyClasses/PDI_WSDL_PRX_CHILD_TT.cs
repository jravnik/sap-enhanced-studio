﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_WSDL_PRX_CHILD_TT
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_WSDL_PRX_CHILD_TT
  {
    [DataMember]
    public string UUID;
    [DataMember]
    public string ES_NAME;
    [DataMember]
    public string PRX_COMPONENT_NAME;
    [DataMember]
    public string PRX_COMPONENT_TYPE;
    [DataMember]
    public string XSD_TYPE;
    [DataMember]
    public string MDRS_DATA_TYPE;
    [DataMember]
    public string MDRS_DATA_TYPE_NAME;
  }
}
