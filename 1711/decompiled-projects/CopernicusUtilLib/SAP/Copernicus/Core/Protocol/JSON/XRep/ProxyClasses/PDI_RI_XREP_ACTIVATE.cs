﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_XREP_ACTIVATE
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses
{
  [DataContract]
  public class PDI_RI_XREP_ACTIVATE : AbstractRemoteFunction<PDI_RI_XREP_ACTIVATE.ImportingType, PDI_RI_XREP_ACTIVATE.ExportingType, PDI_RI_XREP_ACTIVATE.ChangingType, PDI_RI_XREP_ACTIVATE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB7224C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string[] IT_PATH;
      [DataMember]
      public string IV_LANGU;
      [DataMember]
      public string IV_SKIP_ACTIVE_FILES;
      [DataMember]
      public string IV_FORCE_UNLOCK;
      [DataMember]
      public string IV_SESSION_ID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_XREP_FILE_ATTR[] ET_ATTR;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
