﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.BAC_PDI_S_TREE_ELEMENT
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class BAC_PDI_S_TREE_ELEMENT
  {
    [DataMember]
    public string ELEMENT_ID;
    [DataMember]
    public string LONG_TEXT;
    [DataMember]
    public string TYPE;
    [DataMember]
    public int SEQUENCE_NO;
    [DataMember]
    public string PARENT_ID;
    [DataMember]
    public BAC_PDI_S_SEM_LABEL[] SEMANTIC_LABELS;
    [DataMember]
    public string EXCLUSIVE;
  }
}
