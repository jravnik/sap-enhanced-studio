﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RESTRICTION_ENTRY_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RESTRICTION_ENTRY_CHECK : AbstractRemoteFunction<PDI_RESTRICTION_ENTRY_CHECK.ImportingType, PDI_RESTRICTION_ENTRY_CHECK.ExportingType, PDI_RESTRICTION_ENTRY_CHECK.ChangingType, PDI_RESTRICTION_ENTRY_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F51ED6AFF16C5DC0858B65";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_USER;
      [DataMember]
      public string[] IT_CFG_KEY;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_FEATURE_RESTRICTION_RES[] ET_CONF_VALUES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
