﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.ABSLDumpHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class ABSLDumpHandler : JSONHandler
  {
    public PDI_ABSL_S_DUMP[] QueryDumps(string dateFrom, string dateTo, string timeFrom, string timeTo, string userName, string solutionName)
    {
      if (!SAP.Copernicus.Core.Protocol.Connection.getInstance().isConnected())
      {
        ErrorSink instance = ErrorSink.Instance;
        Severity severity = Severity.Info;
        string analysisLogonRequired = Resource.DumpAnalysisLogonRequired;
        int num1 = 1073741824;
        int num2 = (int) severity;
        instance.AddTask(analysisLogonRequired, (Origin) num1, (Severity) num2, null);
        return new PDI_ABSL_S_DUMP[0];
      }
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ABSL_QUERY_DUMPS pdiAbslQueryDumps = new PDI_ABSL_QUERY_DUMPS();
      pdiAbslQueryDumps.Importing = new PDI_ABSL_QUERY_DUMPS.ImportingType();
      pdiAbslQueryDumps.Importing.IV_DATE_FROM = dateFrom;
      pdiAbslQueryDumps.Importing.IV_DATE_TO = dateTo;
      pdiAbslQueryDumps.Importing.IV_TIME_FROM = timeFrom;
      pdiAbslQueryDumps.Importing.IV_TIME_TO = timeTo;
      pdiAbslQueryDumps.Importing.IV_UNAME = userName;
      pdiAbslQueryDumps.Importing.IV_SOLUTION = solutionName;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiAbslQueryDumps, false, false, false);
      }
      catch (ProtocolException ex)
      {
      }
      if (pdiAbslQueryDumps.Exporting != null && pdiAbslQueryDumps.Exporting.ET_DUMP_LIST != null)
        return pdiAbslQueryDumps.Exporting.ET_DUMP_LIST;
      return new PDI_ABSL_S_DUMP[0];
    }

    public PDI_ABSL_S_STACKS[] QueryStacks(string date, string time, string transId)
    {
      if (!SAP.Copernicus.Core.Protocol.Connection.getInstance().isConnected())
      {
        ErrorSink instance = ErrorSink.Instance;
        Severity severity = Severity.Info;
        string analysisLogonRequired = Resource.DumpAnalysisLogonRequired;
        int num1 = 1073741824;
        int num2 = (int) severity;
        instance.AddTask(analysisLogonRequired, (Origin) num1, (Severity) num2, null);
        return new PDI_ABSL_S_STACKS[0];
      }
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ABSL_QUERY_STACKS pdiAbslQueryStacks = new PDI_ABSL_QUERY_STACKS();
      pdiAbslQueryStacks.Importing = new PDI_ABSL_QUERY_STACKS.ImportingType();
      pdiAbslQueryStacks.Importing.IV_DATE = date;
      pdiAbslQueryStacks.Importing.IV_TIME = time;
      pdiAbslQueryStacks.Importing.IV_TRANSID = transId;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiAbslQueryStacks, false, false, false);
      }
      catch (ProtocolException ex)
      {
      }
      if (pdiAbslQueryStacks.Exporting != null && pdiAbslQueryStacks.Exporting.ET_DETAIL_LIST != null)
        return pdiAbslQueryStacks.Exporting.ET_DETAIL_LIST;
      return new PDI_ABSL_S_STACKS[0];
    }
  }
}
