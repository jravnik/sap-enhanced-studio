﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_SOLUTION_HEADER
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_SOLUTION_HEADER
  {
    [DataMember]
    public string CHANGE_DATETIME;
    [DataMember]
    public string PROJECT_NAME;
    [DataMember]
    public string PROJECT_KEY;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string VERSION_ID;
    [DataMember]
    public string VERSION_STATUS;
    [DataMember]
    public string VERSION_STATUS_TEXT;
    [DataMember]
    public string PREVIOUS_PHASE;
    [DataMember]
    public string PREVIOUS_PHASE_STATUS;
    [DataMember]
    public string PHASE;
    [DataMember]
    public string PHASE_STATUS;
    [DataMember]
    public PDI_1O_SOLUTION_VERSION[] SOLUTION_VERSION_LIST;
    [DataMember]
    public string HELP_TEXT;
    [DataMember]
    public string ORIGIN_PROJECT_NAME;
    [DataMember]
    public string ORIGIN_PROJECT_DESCRIPTION;
    [DataMember]
    public string CORRECTION_PROJECT_NAME;
    [DataMember]
    public string CORRECTION_PROJECT_DESCRIPTION;
    [DataMember]
    public string IS_ENABLED;
    [DataMember]
    public string PROJECT_TYPE;
    [DataMember]
    public string IS_PATCHSOL_REQUIRED;
    [DataMember]
    public string EXTERNAL_NAMESPACE;
    [DataMember]
    public string EV_RES_REL;
    [DataMember]
    public string EV_DOWNLOAD_STATUS;
    [DataMember]
    public string EV_ASSEMBLE_STATUS;
    [DataMember]
    public string EV_ACT_STATUS;
    [DataMember]
    public string EV_IS_SPLIT_ENABLED;
    [DataMember]
    public string EV_IS_SPLIT_JOB_RUNNING;
    [DataMember]
    public string EV_REFRESH;
    [DataMember]
    public string EV_IS_PATCH_JOB_RUNNING;

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct PROJECT_TYPES
    {
      public const string One_Off = "ONE_OFF";
      public const string Multi_Cust = "MULTI_CUST";
      public const string Template = "TEMPLATE";
    }
  }
}
