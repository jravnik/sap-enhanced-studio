﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_GET_UNAME_FOR_ALIAS
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_GET_UNAME_FOR_ALIAS : AbstractRemoteFunction<PDI_LM_GET_UNAME_FOR_ALIAS.ImportingType, PDI_LM_GET_UNAME_FOR_ALIAS.ExportingType, PDI_LM_GET_UNAME_FOR_ALIAS.ChangingType, PDI_LM_GET_UNAME_FOR_ALIAS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19DFE474CB9888B14";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public PDI_T_BAPIBNAME[] IT_BAPIBNAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_T_BAPIALIAS[] ET_BAPIALIAS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
