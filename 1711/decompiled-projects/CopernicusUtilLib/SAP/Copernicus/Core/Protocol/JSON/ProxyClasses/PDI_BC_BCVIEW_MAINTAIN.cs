﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_BCVIEW_MAINTAIN
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_BCVIEW_MAINTAIN : AbstractRemoteFunction<PDI_BC_BCVIEW_MAINTAIN.ImportingType, PDI_BC_BCVIEW_MAINTAIN.ExportingType, PDI_BC_BCVIEW_MAINTAIN.ChangingType, PDI_BC_BCVIEW_MAINTAIN.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0225F71EE1908775DD7C291EC5";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PROXY_NAME;
      [DataMember]
      public string IV_NAMESPACE;
      [DataMember]
      public string IV_MODE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_ECO_PROXY;
      [DataMember]
      public string EV_ECO_NAME;
      [DataMember]
      public string EV_MDRS_NAMESPACE;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
