﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.BOGenerationHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class BOGenerationHandler : JSONHandler
  {
    public void GenerateBO(string nsName, string boName)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_BOG_GENERATE pdiRiBogGenerate = new PDI_RI_BOG_GENERATE();
        pdiRiBogGenerate.Importing = new PDI_RI_BOG_GENERATE.ImportingType();
        pdiRiBogGenerate.Importing.IV_ESR_NAMESPACE = nsName;
        pdiRiBogGenerate.Importing.IV_ESR_BO_NAME = boName;
        pdiRiBogGenerate.Importing.IV_PROXY = "X";
        pdiRiBogGenerate.Importing.IV_BOPF = "X";
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiBogGenerate, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiRiBogGenerate, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
    }

    public void DeleteBO(string nsName, string boName)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      try
      {
        PDI_RI_BOG_DELETE pdiRiBogDelete = new PDI_RI_BOG_DELETE();
        pdiRiBogDelete.Importing = new PDI_RI_BOG_DELETE.ImportingType();
        pdiRiBogDelete.Importing.IV_ESR_NAMESPACE = nsName;
        pdiRiBogDelete.Importing.IV_ESR_BO_NAME = boName;
        pdiRiBogDelete.Importing.IV_PROXY = "X";
        pdiRiBogDelete.Importing.IV_BOPF = "X";
        pdiRiBogDelete.Importing.IV_MDRS = "X";
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiBogDelete, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiRiBogDelete, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
    }

    public List<SyntaxErrorInfo> CheckScriptSyntax(string nsName, string boName, string[] rootPathElements, string partnerNS)
    {
      List<SyntaxErrorInfo> syntaxErrorInfoList = new List<SyntaxErrorInfo>();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_BOG_SYNTAX_CHECK riBogSyntaxCheck = new PDI_RI_BOG_SYNTAX_CHECK();
        riBogSyntaxCheck.Importing = new PDI_RI_BOG_SYNTAX_CHECK.ImportingType();
        riBogSyntaxCheck.Importing.IV_ESR_NAMESPACE = nsName;
        riBogSyntaxCheck.Importing.IV_ESR_BO_NAME = boName;
        riBogSyntaxCheck.Importing.IV_PROXY_BO_NAME = RepositoryDataCache.GetInstance().GetProxyNameFromBO(nsName, boName);
        if (partnerNS != null)
          riBogSyntaxCheck.Importing.IV_PARTNER_NAMESPACE = partnerNS;
        riBogSyntaxCheck.Importing.IV_ROOT_PATH = string.Join("/", rootPathElements);
        riBogSyntaxCheck.Importing.IV_XREP_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        jsonClient.callFunctionModule((SAPFunctionModule) riBogSyntaxCheck, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) riBogSyntaxCheck, false, false);
        syntaxErrorInfoList = JSONParamConverter.ET_MESSAGES2SyntaxErrorInfoList(riBogSyntaxCheck.Exporting.ET_MESSAGES);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return syntaxErrorInfoList;
    }
  }
}
