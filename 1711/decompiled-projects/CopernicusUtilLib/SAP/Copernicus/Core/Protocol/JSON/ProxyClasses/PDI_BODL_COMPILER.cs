﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BODL_COMPILER
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BODL_COMPILER : AbstractRemoteFunction<PDI_BODL_COMPILER.ImportingType, PDI_BODL_COMPILER.ExportingType, PDI_BODL_COMPILER.ChangingType, PDI_BODL_COMPILER.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19ABDD66217CACF9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BODL_SOURCE;
      [DataMember]
      public string IV_MDRS_NAMESPACE;
      [DataMember]
      public string IV_XREP_FILE_PATH;
      [DataMember]
      public bool IV_WITH_ABSL;
      [DataMember]
      public bool IV_WITH_QUERY;
      [DataMember]
      public bool IV_WITH_SAM;
      [DataMember]
      public string IV_COMPILER_OPTION;
      [DataMember]
      public string IV_SESSION_ID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_LM_S_MSG_LIST[] ET_MSG_LIST;
      [DataMember]
      public OSLS_XREP_FILE_ATTR[] ET_ATTR;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
