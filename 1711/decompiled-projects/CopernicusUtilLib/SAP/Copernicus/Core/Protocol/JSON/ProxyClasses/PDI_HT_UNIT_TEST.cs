﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_HT_UNIT_TEST
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_HT_UNIT_TEST : AbstractRemoteFunction<PDI_HT_UNIT_TEST.ImportingType, PDI_HT_UNIT_TEST.ExportingType, PDI_HT_UNIT_TEST.ChangingType, PDI_HT_UNIT_TEST.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return nameof (PDI_HT_UNIT_TEST);
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ABAP_CLASS_NAME;
      [DataMember]
      public string IV_CONTEXT_DATA;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
