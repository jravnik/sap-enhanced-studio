﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_WSDL_PARSED_DATA_TT
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_WSDL_PARSED_DATA_TT
  {
    [DataMember]
    public string OPERATION_NAME;
    [DataMember]
    public string MESSAGE_TYPE_PRX_NAME;
    [DataMember]
    public PDI_WSDL_PRX_COMP_TT[] PARSED_OUT_DATA;
    [DataMember]
    public char MESSAGE_TYPE;
  }
}
