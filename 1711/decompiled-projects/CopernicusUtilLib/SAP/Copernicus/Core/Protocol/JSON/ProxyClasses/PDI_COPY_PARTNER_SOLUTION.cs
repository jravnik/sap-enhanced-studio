﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_COPY_PARTNER_SOLUTION
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_COPY_PARTNER_SOLUTION : AbstractRemoteFunction<PDI_COPY_PARTNER_SOLUTION.ImportingType, PDI_COPY_PARTNER_SOLUTION.ExportingType, PDI_COPY_PARTNER_SOLUTION.ChangingType, PDI_COPY_PARTNER_SOLUTION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return nameof (PDI_COPY_PARTNER_SOLUTION);
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ORIGINAL_SOLUTION_NAME;
      [DataMember]
      public string IV_ORIGINAL_SOLUTION_VERSION;
      [DataMember]
      public string IV_NEW_SOLUTION_NAME;
      [DataMember]
      public string IV_NEW_SOLUTION_DESCRIPTION;
      [DataMember]
      public string IV_NEW_SOL_TYPE;
      [DataMember]
      public string IV_NEW_SOL_DU;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_S_PRODUCT_VERS_AND_COMPS ES_NEW_PRODUCT;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
