﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_GET_SOLUTION_INFOS
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_GET_SOLUTION_INFOS : AbstractRemoteFunction<PDI_LM_GET_SOLUTION_INFOS.ImportingType, PDI_LM_GET_SOLUTION_INFOS.ExportingType, PDI_LM_GET_SOLUTION_INFOS.ChangingType, PDI_LM_GET_SOLUTION_INFOS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0123D21EE0AB912384576697A8";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION_NAME;
      [DataMember]
      public string IV_DETAIL_LOG;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_S_PRODUCT_STATUS ES_PRODUCT_DATA;
      [DataMember]
      public PDI_S_PATCH_STATUS ES_PATCH_DATA;
      [DataMember]
      public PDI_T_APPLOG_MSG[] ET_APPLOG;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
