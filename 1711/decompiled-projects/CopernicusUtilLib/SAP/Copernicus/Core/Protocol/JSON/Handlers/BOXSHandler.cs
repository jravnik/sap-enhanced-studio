﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.BOXSHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class BOXSHandler : JSONHandler
  {
    public List<string> getReuseTypes()
    {
      List<string> stringList = new List<string>();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_A2X_GET_REUSE_INFO riA2XGetReuseInfo = new PDI_RI_A2X_GET_REUSE_INFO();
        riA2XGetReuseInfo.Importing = new PDI_RI_A2X_GET_REUSE_INFO.ImportingType();
        riA2XGetReuseInfo.Importing.IV_TRG_DO_NAME = string.Empty;
        jsonClient.callFunctionModule((SAPFunctionModule) riA2XGetReuseInfo, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) riA2XGetReuseInfo, false, false);
        if (riA2XGetReuseInfo.Exporting != null)
        {
          for (int index = 0; index < riA2XGetReuseInfo.Exporting.ET_REUSE_TYPES.Length; ++index)
          {
            if (!stringList.Contains(riA2XGetReuseInfo.Exporting.ET_REUSE_TYPES[index].TRG_DO_NAME))
              stringList.Add(riA2XGetReuseInfo.Exporting.ET_REUSE_TYPES[index].TRG_DO_NAME);
          }
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return stringList;
    }

    public string FetchWSDLUrl(string xprep_path)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_GET_SERV_DEPLT_DETAILS servDepltDetails = new PDI_RI_GET_SERV_DEPLT_DETAILS();
        servDepltDetails.Importing = new PDI_RI_GET_SERV_DEPLT_DETAILS.ImportingType();
        servDepltDetails.Importing.IV_PATH = xprep_path;
        jsonClient.callFunctionModule((SAPFunctionModule) servDepltDetails, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) servDepltDetails, false, false);
        if (servDepltDetails.Exporting != null)
        {
          string evSuccess = servDepltDetails.Exporting.EV_SUCCESS;
          return servDepltDetails.Exporting.EV_WSDL_URL;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return string.Empty;
    }

    public ViewNameDetails[] checkViewName(string boName, string viewName, string nameSpace, out bool success)
    {
      ViewNameDetails[] viewNameDetailsArray = (ViewNameDetails[]) null;
      success = false;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_A2X_VALIDATE_VIEW_NAME validateViewName = new PDI_RI_A2X_VALIDATE_VIEW_NAME();
        validateViewName.Importing = new PDI_RI_A2X_VALIDATE_VIEW_NAME.ImportingType();
        validateViewName.Importing.IV_BOV_NAME = viewName;
        validateViewName.Importing.IV_BO_NAME = boName;
        validateViewName.Importing.IV_NAME_SPACE = nameSpace;
        jsonClient.callFunctionModule((SAPFunctionModule) validateViewName, false, false, false);
        if (validateViewName.Exporting != null)
        {
          string evSuccess = validateViewName.Exporting.EV_SUCCESS;
          success = evSuccess.Equals("X");
          viewNameDetailsArray = validateViewName.Exporting.ET_BOV_NAME;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return viewNameDetailsArray;
    }

    public void CheckExternalUiConsistency(string extuiXrepPath, out PDI_RI_S_MESSAGE[] messages, out bool success)
    {
      success = false;
      messages = (PDI_RI_S_MESSAGE[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_EXTUI_CHECK pdiRiExtuiCheck = new PDI_RI_EXTUI_CHECK();
        pdiRiExtuiCheck.Importing = new PDI_RI_EXTUI_CHECK.ImportingType();
        pdiRiExtuiCheck.Importing.IV_XREP_PATH = extuiXrepPath;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiExtuiCheck, false, false, false);
        if (pdiRiExtuiCheck.Exporting == null)
          return;
        string evSuccess = pdiRiExtuiCheck.Exporting.EV_SUCCESS;
        messages = pdiRiExtuiCheck.Exporting.ET_MESSAGES;
        success = evSuccess.Equals("X");
        if (success)
          return;
        this.reportServerSideProtocolException((SAPFunctionModule) pdiRiExtuiCheck, false, false);
      }
      catch (Exception ex)
      {
        if (ex is ProtocolException)
          return;
        this.reportClientSideProtocolException(ex);
      }
    }

    public PDI_RI_S_MESSAGE[] checkSIName(string siName, string siNameSpace, out bool success)
    {
      success = false;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_A2X_VALIDATE_SI_NAME a2XValidateSiName = new PDI_RI_A2X_VALIDATE_SI_NAME();
        a2XValidateSiName.Importing = new PDI_RI_A2X_VALIDATE_SI_NAME.ImportingType();
        a2XValidateSiName.Importing.IV_SIIF_NAME = siName;
        a2XValidateSiName.Importing.IV_SI_NAMESPACE = siNameSpace;
        jsonClient.callFunctionModule((SAPFunctionModule) a2XValidateSiName, false, false, false);
        if (a2XValidateSiName.Exporting != null)
        {
          string evSuccess = a2XValidateSiName.Exporting.EV_SUCCESS;
          PDI_RI_S_MESSAGE[] etMessages = a2XValidateSiName.Exporting.ET_MESSAGES;
          success = evSuccess.Equals("X");
          return etMessages;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (PDI_RI_S_MESSAGE[]) null;
    }

    public PDI_RI_T_SI_INFO[] getSIList(string siNamespace)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_A2X_GET_SI_LIST pdiRiA2XGetSiList = new PDI_RI_A2X_GET_SI_LIST();
        pdiRiA2XGetSiList.Importing = new PDI_RI_A2X_GET_SI_LIST.ImportingType();
        if (!string.IsNullOrEmpty(siNamespace))
          pdiRiA2XGetSiList.Importing.IV_NAMESPACE = siNamespace;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiA2XGetSiList, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiRiA2XGetSiList, false, false);
        if (pdiRiA2XGetSiList.Exporting != null)
        {
          if (pdiRiA2XGetSiList.Exporting.EV_SUCCESS.Equals("X"))
            return pdiRiA2XGetSiList.Exporting.ET_SI_LIST;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (PDI_RI_T_SI_INFO[]) null;
    }

    public PDI_RI_T_SI_OP_DETAIL[] getSIInfo(string siName, string siNamespace)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_A2X_GET_SI_INFO pdiRiA2XGetSiInfo = new PDI_RI_A2X_GET_SI_INFO();
        pdiRiA2XGetSiInfo.Importing = new PDI_RI_A2X_GET_SI_INFO.ImportingType();
        pdiRiA2XGetSiInfo.Importing.IV_SI_NAME = siName;
        pdiRiA2XGetSiInfo.Importing.IV_SI_NAMESPACE = siNamespace;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiA2XGetSiInfo, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiRiA2XGetSiInfo, false, false);
        if (pdiRiA2XGetSiInfo.Exporting != null)
        {
          if (pdiRiA2XGetSiInfo.Exporting.EV_SUCCESS.Equals("X"))
            return pdiRiA2XGetSiInfo.Exporting.ET_SI_OPN_LIST;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (PDI_RI_T_SI_OP_DETAIL[]) null;
    }

    public PDI_RI_T_EXT_UI_INFO[] getExtUiMOProxyName(string filePath, bool callFromClean)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_GET_EXT_UI_INFO pdiRiGetExtUiInfo = new PDI_RI_GET_EXT_UI_INFO();
        pdiRiGetExtUiInfo.Importing = new PDI_RI_GET_EXT_UI_INFO.ImportingType();
        pdiRiGetExtUiInfo.Importing.IV_XREP_PATH = filePath;
        if (callFromClean)
          pdiRiGetExtUiInfo.Importing.IV_CLEAN_CALL = "X";
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiGetExtUiInfo, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiRiGetExtUiInfo, false, false);
        if (pdiRiGetExtUiInfo.Exporting != null)
        {
          if (pdiRiGetExtUiInfo.Exporting.EV_SUCCESS.Equals("X"))
            return pdiRiGetExtUiInfo.Exporting.ET_EXT_UI_INFO;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (PDI_RI_T_EXT_UI_INFO[]) null;
    }
  }
}
