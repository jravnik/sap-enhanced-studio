﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_A2X_GET_SI_INFO
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_A2X_GET_SI_INFO : AbstractRemoteFunction<PDI_RI_A2X_GET_SI_INFO.ImportingType, PDI_RI_A2X_GET_SI_INFO.ExportingType, PDI_RI_A2X_GET_SI_INFO.ChangingType, PDI_RI_A2X_GET_SI_INFO.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DEFB1F273E1B28514CE";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SI_NAME;
      [DataMember]
      public string IV_SI_NAMESPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_T_SI_OP_DETAIL[] ET_SI_OPN_LIST;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
