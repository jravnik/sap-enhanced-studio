﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_XREP_SAVE
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses
{
  [DataContract]
  public class PDI_RI_XREP_SAVE : AbstractRemoteFunction<PDI_RI_XREP_SAVE.ImportingType, PDI_RI_XREP_SAVE.ExportingType, PDI_RI_XREP_SAVE.ChangingType, PDI_RI_XREP_SAVE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194EC88B8EE0C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember(Order = 1)]
      public string IV_PATH;
      [DataMember(Order = 4)]
      public string IV_CONTENT;
      [DataMember(Order = 2)]
      public string IV_SESSION_ID;
      [DataMember(Order = 3)]
      public OSLS_XREP_FILE_ATTR[] IT_ATTR;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
