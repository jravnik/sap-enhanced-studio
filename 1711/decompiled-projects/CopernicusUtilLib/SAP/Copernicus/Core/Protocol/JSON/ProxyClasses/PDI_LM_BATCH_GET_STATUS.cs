﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_BATCH_GET_STATUS
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_BATCH_GET_STATUS : AbstractRemoteFunction<PDI_LM_BATCH_GET_STATUS.ImportingType, PDI_LM_BATCH_GET_STATUS.ExportingType, PDI_LM_BATCH_GET_STATUS.ChangingType, PDI_LM_BATCH_GET_STATUS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E028DAE1EE1ABC81D042AF5C962";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_JOBNAME;
      [DataMember]
      public string IV_JOBCNT;
      [DataMember]
      public string IV_APPLOG_ID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_PROC_SUCC;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_JOB_STATE;
      [DataMember]
      public PDI_LM_T_MSG_LIST[] ET_MSG_LIST;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
