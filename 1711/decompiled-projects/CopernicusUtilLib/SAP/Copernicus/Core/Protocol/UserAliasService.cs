﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.UserAliasService
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.Core.Protocol
{
  public class UserAliasService
  {
    internal static UserAliasService instance;
    internal UserAliasHandler aliasHandler;
    internal List<PDI_T_BAPIALIAS> userAliasBuffer;

    private UserAliasService()
    {
    }

    public static UserAliasService GetInstance()
    {
      if (UserAliasService.instance == null)
      {
        UserAliasService.instance = new UserAliasService();
        UserAliasService.instance.aliasHandler = new UserAliasHandler();
      }
      return UserAliasService.instance;
    }

    public string GetUserAlias(string user)
    {
      if (user == "")
        return "";
      PDI_T_BAPIBNAME[] userNames = new PDI_T_BAPIBNAME[1]
      {
        new PDI_T_BAPIBNAME()
      };
      userNames[0].BAPIBNAME = user;
      List<PDI_T_BAPIALIAS> userAliasList = this.GetUserAliasList(userNames);
      if (userAliasList[0].BAPIBNAME == user)
        return userAliasList[0].BAPIALIAS;
      return (string) null;
    }

    public List<PDI_T_BAPIALIAS> GetUserAliasList(PDI_T_BAPIBNAME[] userNames)
    {
      List<PDI_T_BAPIALIAS> pdiTBapialiasList = new List<PDI_T_BAPIALIAS>();
      if (this.userAliasBuffer != null)
      {
        List<PDI_T_BAPIBNAME> pdiTBapibnameList = new List<PDI_T_BAPIBNAME>();
        foreach (PDI_T_BAPIBNAME userName in userNames)
        {
          PDI_T_BAPIBNAME user = userName;
          PDI_T_BAPIALIAS pdiTBapialias = this.userAliasBuffer.Find((Predicate<PDI_T_BAPIALIAS>) (e => e.BAPIBNAME == user.BAPIBNAME));
          if (pdiTBapialias != null)
            pdiTBapialiasList.Add(pdiTBapialias);
          else
            pdiTBapibnameList.Add(user);
        }
        if (pdiTBapibnameList.Count > 0)
        {
          List<PDI_T_BAPIALIAS> list = ((IEnumerable<PDI_T_BAPIALIAS>) this.aliasHandler.GetAliasForUserName(pdiTBapibnameList.ToArray())).ToList<PDI_T_BAPIALIAS>();
          this.userAliasBuffer.AddRange((IEnumerable<PDI_T_BAPIALIAS>) list);
          pdiTBapialiasList.AddRange((IEnumerable<PDI_T_BAPIALIAS>) list);
        }
      }
      else if (userNames.Length > 0)
      {
        pdiTBapialiasList = ((IEnumerable<PDI_T_BAPIALIAS>) this.aliasHandler.GetAliasForUserName(userNames)).ToList<PDI_T_BAPIALIAS>();
        this.userAliasBuffer = pdiTBapialiasList;
      }
      return pdiTBapialiasList;
    }
  }
}
