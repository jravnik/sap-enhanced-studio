﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.ResourceAccess
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.ResourceAccess.TestMocks;
using SAP.Copernicus.Core.Util;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;

namespace SAP.Copernicus.Core.ResourceAccess
{
  public class ResourceAccess : IResourceAccess
  {
    private static IResourceAccess ResourceAccessor;
    protected Connection Connection;
    protected RepositoryDataCache RepositoryDataCache;

    public static IResourceAccess GetInstance()
    {
      return SAP.Copernicus.Core.ResourceAccess.ResourceAccess.GetInstance(false, true);
    }

    public static IResourceAccess GetInstance(bool usePSM, bool mockMDRS = true)
    {
      if (TestController.TestMode)
      {
        SAP.Copernicus.Core.ResourceAccess.ResourceAccess.ResourceAccessor = (IResourceAccess) new MockResourceAccess();
        ((MockResourceAccess) SAP.Copernicus.Core.ResourceAccess.ResourceAccess.ResourceAccessor).Initialize(usePSM, mockMDRS);
        SAP.Copernicus.Core.ResourceAccess.ResourceAccess.InitializeVSPackageUtil();
      }
      else if (SAP.Copernicus.Core.ResourceAccess.ResourceAccess.ResourceAccessor == null)
      {
        SAP.Copernicus.Core.ResourceAccess.ResourceAccess.ResourceAccessor = (IResourceAccess) new SAP.Copernicus.Core.ResourceAccess.ResourceAccess();
        ((SAP.Copernicus.Core.ResourceAccess.ResourceAccess) SAP.Copernicus.Core.ResourceAccess.ResourceAccess.ResourceAccessor).Initialize();
      }
      return SAP.Copernicus.Core.ResourceAccess.ResourceAccess.ResourceAccessor;
    }

    private static void InitializeVSPackageUtil()
    {
      string[] strArray = new string[14]
      {
        "Copernicus.dll",
        "CopernicusUtilLib.dll",
        "SAP.Copernicus.Model.dll",
        "SAP.SamEditor.Dsl.dll",
        "SAP.SamEditor.DslPackage.dll",
        "BOCompilerCommon.dll",
        "CopernicusBusinessConfiguration.dll",
        "CopernicusDataPrivacy",
        "BOLanguage.dll",
        "ASLanguage.dll",
        "ASUtilLib.dll",
        "CopernicusExtension.dll",
        "BOLanguageTest.dll",
        "CopernicusTest.dll"
      };
      AggregateCatalog aggregateCatalog = new AggregateCatalog();
      foreach (string codeBase in strArray)
      {
        try
        {
          aggregateCatalog.Catalogs.Add((ComposablePartCatalog) new AssemblyCatalog(codeBase));
        }
        catch (FileNotFoundException ex)
        {
        }
      }
      VSPackageUtil.CompositionContainer = new CompositionContainer((ComposablePartCatalog) aggregateCatalog, new ExportProvider[0]);
    }

    public virtual void Initialize()
    {
      this.Connection = Connection.getInstance();
      this.RepositoryDataCache = RepositoryDataCache.GetInstance();
    }

    public Connection GetConnection()
    {
      return this.Connection;
    }

    public RepositoryDataCache GetRepositoryDataCache()
    {
      return this.RepositoryDataCache;
    }
  }
}
