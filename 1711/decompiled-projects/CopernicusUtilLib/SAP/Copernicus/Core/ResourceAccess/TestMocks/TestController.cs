﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.TestController
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks
{
  public static class TestController
  {
    private static string testInputRootPath;

    public static event TestController.EventHandlerOnPathUpdate PathUpdateEventRegistration;

    public static bool TestMode { set; get; }

    public static string TestResultStorage { set; get; }

    public static string TestInputRootPath
    {
      set
      {
        TestController.testInputRootPath = value;
        TestController.PathUpdateEventRegistration(TestController.testInputRootPath);
      }
      get
      {
        return TestController.testInputRootPath;
      }
    }

    public delegate void EventHandlerOnPathUpdate(string newFilePath);
  }
}
