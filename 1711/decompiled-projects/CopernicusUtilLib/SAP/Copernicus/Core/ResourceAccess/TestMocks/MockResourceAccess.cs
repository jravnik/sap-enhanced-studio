﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.MockResourceAccess
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.ResourceAccess.TestMocks.Core;

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks
{
  internal class MockResourceAccess : SAP.Copernicus.Core.ResourceAccess.ResourceAccess
  {
    public void Initialize(bool usePSM, bool mockMDRS = true)
    {
      this.Connection = (Connection) new MockConnection(usePSM);
      if (!mockMDRS)
        return;
      this.RepositoryDataCache = (RepositoryDataCache) new MockRepositoryDataCache();
    }
  }
}
