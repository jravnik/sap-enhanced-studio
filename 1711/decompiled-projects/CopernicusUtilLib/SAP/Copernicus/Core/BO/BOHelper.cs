﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.BO.BOHelper
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Repository;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.BO
{
  public class BOHelper
  {
    private BOHelper()
    {
    }

    public static bool BOWasSaved(string boNS, string boName)
    {
      if (!string.IsNullOrEmpty(RepositoryDataCache.GetInstance().GetProxyNameFromBO(boNS, boName)))
        return true;
      int num = (int) CopernicusMessageBox.Show(string.Format(Resource.BOSaveMissing, (object) boName), Resource.BOCheckTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      return false;
    }
  }
}
