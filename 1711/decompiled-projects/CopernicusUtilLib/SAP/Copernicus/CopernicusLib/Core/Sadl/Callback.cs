﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.Callback
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [XmlRoot(ElementName = "callback", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class Callback
  {
    [XmlAttribute("name")]
    public string Name { get; set; }

    [XmlAttribute("classname")]
    public string Classname { get; set; }

    [XmlAttribute("language")]
    public string Language { get; set; }

    [XmlAttribute("targetBO")]
    public string TargetBO { get; set; }

    [XmlAttribute("targetNode")]
    public string TargetNode { get; set; }
  }
}
