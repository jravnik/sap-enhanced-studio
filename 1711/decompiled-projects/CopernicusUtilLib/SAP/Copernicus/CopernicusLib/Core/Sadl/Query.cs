﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.Query
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [XmlRoot(ElementName = "query", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [Serializable]
  public class Query
  {
    [XmlAttribute("bo")]
    public string BOProxyName { get; set; }

    [XmlAttribute("name")]
    public string QueryName { get; set; }

    [XmlAttribute("node")]
    public string QueryNode { get; set; }

    [XmlAttribute("type")]
    public string Type { get; set; }

    [XmlElement("inputStructure")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.InputStructure[] InputStructure { get; set; }

    [XmlElement("outputStructure")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.OutputStructure[] OutputStructure { get; set; }
  }
}
