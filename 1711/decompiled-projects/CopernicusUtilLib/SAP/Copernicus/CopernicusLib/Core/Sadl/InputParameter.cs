﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.InputParameter
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [XmlRoot(ElementName = "inputParameter", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://sap.com/sap.nw.f.sadl")]
  [Serializable]
  public class InputParameter
  {
    [XmlAttribute("name")]
    public string Name { set; get; }

    [XmlAttribute("type")]
    public string Type { set; get; }

    [XmlElement(ElementName = "attribute")]
    public List<StructureAttribute> Attribute { set; get; }

    [XmlElement(ElementName = "inputParameter")]
    public List<InputParameter> InputParameterList { set; get; }
  }
}
