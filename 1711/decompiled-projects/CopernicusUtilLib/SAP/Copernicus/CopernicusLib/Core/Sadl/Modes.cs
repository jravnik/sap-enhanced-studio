﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.Modes
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [DesignerCategory("code")]
  [XmlRoot(ElementName = "modes", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [Serializable]
  public class Modes
  {
    private SAP.Copernicus.CopernicusLib.Core.Sadl.Mode[] modeField;

    [XmlElement("mode")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.Mode[] Mode
    {
      get
      {
        return this.modeField;
      }
      set
      {
        this.modeField = value;
      }
    }
  }
}
