﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.Conversions
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [XmlRoot(ElementName = "conversions", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class Conversions
  {
    private SAP.Copernicus.CopernicusLib.Core.Sadl.Conversion[] conversionField;

    [XmlElement("conversion")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.Conversion[] Conversion
    {
      get
      {
        return this.conversionField;
      }
      set
      {
        this.conversionField = value;
      }
    }
  }
}
