﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.ResultSet
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [XmlRoot(ElementName = "resultSet", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [Serializable]
  public class ResultSet
  {
    private SAP.Copernicus.CopernicusLib.Core.Sadl.Structure[] structureField;

    [XmlElement("structure")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.Structure[] Structure
    {
      get
      {
        return this.structureField;
      }
      set
      {
        this.structureField = value;
      }
    }
  }
}
