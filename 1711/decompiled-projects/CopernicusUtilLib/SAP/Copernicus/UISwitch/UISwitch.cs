﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.UISwitch
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [XmlType(AnonymousType = true, Namespace = "http://www.sap.com/a1s/cd/oberon/uimodelchange-1.0")]
  [DesignerCategory("code")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlRoot(IsNullable = false, Namespace = "http://www.sap.com/a1s/cd/oberon/uimodelchange-1.0")]
  [DebuggerStepThrough]
  [Serializable]
  public class UISwitch
  {
    private TextBlockType[] textPoolField;
    private string idField;
    private string nameTextPoolIdField;
    private string descriptionTextPoolIdField;

    [XmlArray(Form = XmlSchemaForm.Unqualified)]
    [XmlArrayItem("TextBlock", IsNullable = false, Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
    public TextBlockType[] TextPool
    {
      get
      {
        return this.textPoolField;
      }
      set
      {
        this.textPoolField = value;
      }
    }

    [XmlAttribute]
    public string id
    {
      get
      {
        return this.idField;
      }
      set
      {
        this.idField = value;
      }
    }

    [XmlAttribute]
    public string nameTextPoolId
    {
      get
      {
        return this.nameTextPoolIdField;
      }
      set
      {
        this.nameTextPoolIdField = value;
      }
    }

    [XmlAttribute]
    public string descriptionTextPoolId
    {
      get
      {
        return this.descriptionTextPoolIdField;
      }
      set
      {
        this.descriptionTextPoolIdField = value;
      }
    }
  }
}
