﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.NamedModelEntity
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
  [XmlInclude(typeof (PropertyType))]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [Serializable]
  public class NamedModelEntity : ModelEntity
  {
    private string nameField;

    [XmlAttribute]
    public string name
    {
      get
      {
        return this.nameField;
      }
      set
      {
        this.nameField = value;
      }
    }
  }
}
