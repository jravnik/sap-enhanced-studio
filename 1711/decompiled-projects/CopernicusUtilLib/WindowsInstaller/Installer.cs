﻿// Decompiled with JetBrains decompiler
// Type: WindowsInstaller.Installer
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace WindowsInstaller
{
  [TypeIdentifier]
  [CompilerGenerated]
  [Guid("000C1090-0000-0000-C000-000000000046")]
  [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
  [ComImport]
  public interface Installer
  {
    [DispId(4)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.Interface)]
    Database OpenDatabase([MarshalAs(UnmanagedType.BStr), In] string DatabasePath, [MarshalAs(UnmanagedType.Struct), In] object OpenMode);
  }
}
