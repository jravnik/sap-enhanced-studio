﻿// Decompiled with JetBrains decompiler
// Type: WindowsInstaller.Record
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace WindowsInstaller
{
  [CompilerGenerated]
  [Guid("000C1093-0000-0000-C000-000000000046")]
  [TypeIdentifier]
  [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
  [ComImport]
  public interface Record
  {
    [DispId(1)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.BStr)]
    string get_StringData([In] int Field);

    [DispId(1)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    void set_StringData([In] int Field, [MarshalAs(UnmanagedType.BStr), In] string value);
  }
}
