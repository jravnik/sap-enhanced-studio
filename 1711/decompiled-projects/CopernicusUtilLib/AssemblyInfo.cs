﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTrademark("SAP and Business ByDesign are trademark(s) or registered trademark(s) of SAP AG in Germany and in several other countries.")]
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyCopyright("© 2011 SAP AG. All rights reserved.")]
[assembly: ComVisible(false)]
[assembly: AssemblyProduct("SAP® Business ByDesign™")]
[assembly: AssemblyFileVersion("142.0.3211.0035")]
[assembly: InternalsVisibleTo("CopernicusTest")]
[assembly: Guid("3945fdb1-6f89-4aa6-8d78-54a6cf89af15")]
[assembly: AssemblyTitle("CopernicusUtilLib")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SAP AG")]
[assembly: AssemblyVersion("142.0.3211.35")]
