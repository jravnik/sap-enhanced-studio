﻿// Decompiled with JetBrains decompiler
// Type: com.sap.JSONConnector.Services.JSONConnector.OBERON_S_LOGGING
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace com.sap.JSONConnector.Services.JSONConnector
{
  [DataContract]
  public class OBERON_S_LOGGING
  {
    [DataMember]
    public string ID;
    [DataMember]
    public string TYPE;
    [DataMember]
    public string TIMESTAMPL;
    [DataMember]
    public string DESCRIPTION;
  }
}
