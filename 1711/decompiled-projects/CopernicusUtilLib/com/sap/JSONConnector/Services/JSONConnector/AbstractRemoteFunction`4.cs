﻿// Decompiled with JetBrains decompiler
// Type: com.sap.JSONConnector.Services.JSONConnector.AbstractRemoteFunction`4
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace com.sap.JSONConnector.Services.JSONConnector
{
  [DataContract]
  public abstract class AbstractRemoteFunction<I, E, C, T> : SAPFunctionModule where I : class where E : class where C : class where T : class
  {
    [DataMember(Name = "IMPORTING")]
    public I Importing { get; set; }

    [DataMember(Name = "EXPORTING")]
    public E Exporting { get; set; }

    [DataMember(Name = "CHANGING")]
    public C Changing { get; set; }

    [DataMember(Name = "TABLES")]
    public T Tables { get; set; }

    [DataMember(Name = "LOG")]
    public LogType Log { get; set; }

    public abstract string FunctionName { get; }
  }
}
