﻿// Decompiled with JetBrains decompiler
// Type: com.sap.JSONConnector.Services.JSONConnector.LogEntry
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace com.sap.JSONConnector.Services.JSONConnector
{
  [DataContract]
  public class LogEntry
  {
    [DataMember(Name = "ID")]
    public string Id { get; set; }

    [DataMember(Name = "TYPE")]
    public string Type { get; set; }

    [DataMember(Name = "TIMESTAMPL")]
    public string Timestamp { get; set; }

    [DataMember(Name = "DESCRIPTION")]
    public string Description { get; set; }
  }
}
