﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.NodeExtensionScenario.NodeExtScenEditorFactory
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.CustomEditor;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.NodeExtensionScenario
{
  [Guid("91E76BDD-C112-4996-AE02-36BDB447B053")]
  public sealed class NodeExtScenEditorFactory : CustomEditorFactory<NodeExtScenEditorPane>
  {
  }
}
