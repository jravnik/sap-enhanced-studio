﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.Resource
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus.Extension.Mashup
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  public class Resource
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    internal Resource()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) Resource.resourceMan, (object) null))
          Resource.resourceMan = new ResourceManager("SAP.Copernicus.Extension.Mashup.Resource", typeof (Resource).Assembly);
        return Resource.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return Resource.resourceCulture;
      }
      set
      {
        Resource.resourceCulture = value;
      }
    }

    public static Bitmap AuthoringMashupFileNode
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (AuthoringMashupFileNode), Resource.resourceCulture);
      }
    }

    public static string BYD_STUDIO
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (BYD_STUDIO), Resource.resourceCulture);
      }
    }

    public static string CanDeleteItem
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (CanDeleteItem), Resource.resourceCulture);
      }
    }

    public static Bitmap CreateDataMashup16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (CreateDataMashup16x16), Resource.resourceCulture);
      }
    }

    public static Bitmap CreateHtmlMashup16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (CreateHtmlMashup16x16), Resource.resourceCulture);
      }
    }

    public static Bitmap CreateRestService16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (CreateRestService16x16), Resource.resourceCulture);
      }
    }

    public static Bitmap CreateRssAtomService16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (CreateRssAtomService16x16), Resource.resourceCulture);
      }
    }

    public static Bitmap CreateSoapService16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (CreateSoapService16x16), Resource.resourceCulture);
      }
    }

    public static Bitmap CreateURLMashup16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (CreateURLMashup16x16), Resource.resourceCulture);
      }
    }

    public static string EnableAdminMode
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (EnableAdminMode), Resource.resourceCulture);
      }
    }

    public static string KeyUserDisabled
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (KeyUserDisabled), Resource.resourceCulture);
      }
    }

    public static Bitmap MashupComponentNode
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (MashupComponentNode), Resource.resourceCulture);
      }
    }

    public static Bitmap MashupComponentUsageNode
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (MashupComponentUsageNode), Resource.resourceCulture);
      }
    }

    public static Bitmap MashupPortBinding
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (MashupPortBinding), Resource.resourceCulture);
      }
    }

    public static Bitmap MashupPortBindingInactive
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (MashupPortBindingInactive), Resource.resourceCulture);
      }
    }

    public static Bitmap MashupPortBindingNode16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (MashupPortBindingNode16x16), Resource.resourceCulture);
      }
    }

    public static Bitmap MashupRootNode
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (MashupRootNode), Resource.resourceCulture);
      }
    }

    public static Bitmap MashupServiceNode
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (MashupServiceNode), Resource.resourceCulture);
      }
    }

    public static string MsgBYD
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgBYD), Resource.resourceCulture);
      }
    }

    public static string PBFileExisted
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (PBFileExisted), Resource.resourceCulture);
      }
    }

    public static string ReferencedByUIs
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ReferencedByUIs), Resource.resourceCulture);
      }
    }

    public static Bitmap ReferenceUIComponent
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (ReferenceUIComponent), Resource.resourceCulture);
      }
    }

    public static Bitmap ShortcutFactSheetFloorplan16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (ShortcutFactSheetFloorplan16x16), Resource.resourceCulture);
      }
    }

    public static Bitmap ShortcutGuidedActivityFloorplan16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (ShortcutGuidedActivityFloorplan16x16), Resource.resourceCulture);
      }
    }

    public static Bitmap ShortcutObjectInstanceFloorplan16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (ShortcutObjectInstanceFloorplan16x16), Resource.resourceCulture);
      }
    }

    public static Bitmap ShortcutObjectWorklistFloorplan16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (ShortcutObjectWorklistFloorplan16x16), Resource.resourceCulture);
      }
    }

    public static Bitmap ShortcutQuickActivityFloorplan16x16
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (ShortcutQuickActivityFloorplan16x16), Resource.resourceCulture);
      }
    }
  }
}
