﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupWizards.MashupWizardHelper
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SAP.Copernicus.Extension.Mashup.MashupWizards
{
  public static class MashupWizardHelper
  {
    public static T[] GetEnumValues<T>()
    {
      Type type = typeof (T);
      if (!type.IsEnum)
        throw new ArgumentException("Type '" + type.Name + "' is not an enum");
      List<T> objList = new List<T>();
      foreach (FieldInfo fieldInfo in ((IEnumerable<FieldInfo>) type.GetFields()).Where<FieldInfo>((Func<FieldInfo, bool>) (field => field.IsLiteral)))
      {
        object obj = fieldInfo.GetValue((object) type);
        objList.Add((T) obj);
      }
      return objList.ToArray();
    }

    public enum SemanticCategories
    {
      BusinessFinance,
      LocationTravel,
      NewsReference,
      ProductivityTools,
      SocialCommunication,
    }
  }
}
