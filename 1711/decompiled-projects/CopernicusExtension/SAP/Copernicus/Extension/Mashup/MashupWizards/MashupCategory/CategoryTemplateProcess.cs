﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupWizards.MashupCategory.CategoryTemplateProcess
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.TextTemplating;
using System.IO;
using System.Reflection;
using System.Text;

namespace SAP.Copernicus.Extension.Mashup.MashupWizards.MashupCategory
{
  public class CategoryTemplateProcess : ITemplateProvider
  {
    private readonly Assembly assembly;
    private static CategoryTemplateProcess templates;

    public static CategoryTemplateProcess Instance
    {
      get
      {
        if (CategoryTemplateProcess.templates == null)
          CategoryTemplateProcess.templates = new CategoryTemplateProcess();
        return CategoryTemplateProcess.templates;
      }
    }

    private CategoryTemplateProcess()
    {
      this.assembly = Assembly.GetExecutingAssembly();
    }

    public bool ReadTemplate(string requestFileName, out string content, out string location)
    {
      content = string.Empty;
      location = requestFileName;
      try
      {
        using (Stream manifestResourceStream = this.assembly.GetManifestResourceStream("SAP.Copernicus.Extension.Mashup.MashupWizards.MashupCategory." + requestFileName))
        {
          StreamReader streamReader = new StreamReader(manifestResourceStream, Encoding.UTF8);
          content = streamReader.ReadToEnd();
          return true;
        }
      }
      catch (IOException ex)
      {
        return false;
      }
    }
  }
}
