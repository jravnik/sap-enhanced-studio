﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.UIComponentRefWithMashupFactory
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Project;
using SAP.CopernicusProjectView;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode
{
  public class UIComponentRefWithMashupFactory : ICopernicusHierarchyUIComponentFactory, SUIComponentFactory
  {
    public HierarchyNode CreateNode(CopernicusProjectNode root, string strXRepPath)
    {
      return (HierarchyNode) new UIComponentRefWithMashup((ProjectNode) root, strXRepPath);
    }
  }
}
