﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.UIComponentRefWithMashup
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Project;
using SAP.Copernicus.Extension.Mashup.MashupProjectNode.NodeMenusFilter;
using SAP.Copernicus.Extension.UIDesignerHelper;
using System;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode
{
  [CLSCompliant(false)]
  [ComVisible(true)]
  public class UIComponentRefWithMashup : CopernicusExtVirtualNode
  {
    private string strXRepPathOfUIComponent;
    private bool isValidUIComponentForCurrentSolution;
    private string[] relativeSegments;

    public UIComponentRefWithMashup(ProjectNode root, string strXRepPathOfUIComponent)
      : base(root, (string) null)
    {
      this.strXRepPathOfUIComponent = strXRepPathOfUIComponent;
      this.strVirtualNodeCaption = "->" + this.strXRepPathOfUIComponent.Substring(this.strXRepPathOfUIComponent.LastIndexOf("/") + 1);
    }

    public override string Url
    {
      get
      {
        return this.strXRepPathOfUIComponent;
      }
    }

    public override bool CanExecuteCommand
    {
      get
      {
        return true;
      }
    }

    public override object GetIconHandle(bool open)
    {
      string[] strArray = this.strXRepPathOfUIComponent.Split('.');
      if (strArray[1].Equals("QA"))
        return (object) SAP.Copernicus.Extension.Mashup.Resource.ShortcutQuickActivityFloorplan16x16.GetHicon();
      if (strArray[1].Equals("OIF"))
        return (object) SAP.Copernicus.Extension.Mashup.Resource.ShortcutObjectInstanceFloorplan16x16.GetHicon();
      if (strArray[1].Equals("OWL"))
        return (object) SAP.Copernicus.Extension.Mashup.Resource.ShortcutObjectWorklistFloorplan16x16.GetHicon();
      return (object) SAP.Copernicus.Extension.Mashup.Resource.ShortcutGuidedActivityFloorplan16x16.GetHicon();
    }

    protected override int QueryStatusOnNode(Guid cmdGroup, uint cmd, IntPtr pCmdText, ref QueryStatusResult result)
    {
      int num = 0;
      if (cmdGroup == Microsoft.VisualStudio.Shell.VsMenus.guidVsUIHierarchyWindowCmds)
      {
        if ((int) cmd == 2)
          result = QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
      }
      else if (cmdGroup == PkgCmdIDList.guidCopernicusCmdSet)
      {
        if ((int) cmd == 401)
          result |= QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
      }
      else
      {
        if (!(cmdGroup == PkgCmdIDList.cmdSetMashupUsage))
          return MashupNodeMenusFilter.FilterQueryStatusOnNode(cmdGroup, cmd, pCmdText, ref result);
        if ((int) cmd == 1285)
          result |= QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
      }
      return num;
    }

    protected override int ExecCommandOnNode(Guid cmdGroup, uint cmd, uint nCmdexecopt, IntPtr pvaIn, IntPtr pvaOut)
    {
      if (cmdGroup == Microsoft.VisualStudio.Shell.VsMenus.guidVsUIHierarchyWindowCmds)
      {
        if ((int) cmd == 2)
        {
          UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(this.strXRepPathOfUIComponent, false);
          return 0;
        }
      }
      else if (cmdGroup == PkgCmdIDList.cmdSetMashupUsage && (int) cmd == 1285)
      {
        UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(this.strXRepPathOfUIComponent, false);
        return 0;
      }
      return -2147221248;
    }
  }
}
