﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.AuthoringMashupFileNodeFactory
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Project;
using SAP.CopernicusProjectView;
using System.IO;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode
{
  internal class AuthoringMashupFileNodeFactory : ICopernicusHierarchyFileNodeFactory, SAuthoringMashupFileNodeFactory
  {
    public HierarchyNode CreateNode(CopernicusProjectNode root, FileInfo element)
    {
      return (HierarchyNode) new AuthoringMashupFileNode(root, element);
    }
  }
}
