﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler.DictinaryExtesion
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.Collections.Generic;
using System.IO;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler
{
  public static class DictinaryExtesion
  {
    public static Dictionary<string, FileSystemWatcher> Clone(this Dictionary<string, FileSystemWatcher> originDict)
    {
      Dictionary<string, FileSystemWatcher> dictionary = new Dictionary<string, FileSystemWatcher>();
      foreach (string key in originDict.Keys)
        dictionary.Add(key, originDict[key]);
      return dictionary;
    }
  }
}
