﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.CopernicusStatusBarExtension
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.GUI;
using System.Diagnostics;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode
{
  public static class CopernicusStatusBarExtension
  {
    private static uint pdwCookie = 0;
    private static IVsStatusbar statusBar = (IVsStatusbar) Package.GetGlobalService(typeof (SVsStatusbar));

    public static void ShowMessageExThreadSwitch(this IStatus proxy, string message)
    {
      int pfFrozen;
      CopernicusStatusBarExtension.statusBar.IsFrozen(out pfFrozen);
      if (pfFrozen == 1)
        Trace.Assert(CopernicusStatusBarExtension.statusBar.FreezeOutput(0) == 0);
      Trace.Assert(CopernicusStatusBarExtension.statusBar.Progress(ref CopernicusStatusBarExtension.pdwCookie, 1, message, 0U, 100U) == 0);
    }

    public static void StartProcessExThreadSwitch(this IStatus proxy, string message)
    {
      int pfFrozen;
      CopernicusStatusBarExtension.statusBar.IsFrozen(out pfFrozen);
      if (pfFrozen == 1)
        Trace.Assert(CopernicusStatusBarExtension.statusBar.FreezeOutput(0) == 0);
      object pvIcon = (object) (short) 0;
      Trace.Assert(CopernicusStatusBarExtension.statusBar.Animation(1, ref pvIcon) == 0);
      Trace.Assert(CopernicusStatusBarExtension.statusBar.Progress(ref CopernicusStatusBarExtension.pdwCookie, 1, message, 0U, 100U) == 0);
    }

    public static void EndProcessExThreadSwitch(this IStatus proxym, string message)
    {
      int pfFrozen;
      CopernicusStatusBarExtension.statusBar.IsFrozen(out pfFrozen);
      if (pfFrozen == 1)
        Trace.Assert(CopernicusStatusBarExtension.statusBar.FreezeOutput(0) == 0);
      Trace.Assert(CopernicusStatusBarExtension.statusBar.Progress(ref CopernicusStatusBarExtension.pdwCookie, 1, message, 0U, 100U) == 0);
      object pvIcon = (object) (short) 0;
      Trace.Assert(CopernicusStatusBarExtension.statusBar.Animation(0, ref pvIcon) == 0);
    }
  }
}
