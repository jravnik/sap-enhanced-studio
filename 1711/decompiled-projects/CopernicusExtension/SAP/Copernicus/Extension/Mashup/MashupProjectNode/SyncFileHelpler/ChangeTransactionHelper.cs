﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler.ChangeTransactionHelper
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Extension.Mashup.HtmlMashup.MashupJsonUtil.MashupHandler;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler
{
  public class ChangeTransactionHelper
  {
    private static string strTransactionFolderBar = "/SAP_BYD_UI_FLEX/CHANGE_TRANSACTIONS/Partner/AddMashupButtonIntoFunctionBar/";
    private static string strTransactionPort = "/SAP_BYD_UI_FLEX/CHANGE_TRANSACTIONS/Partner/AddMashupPaneWithoutPort/";
    private static ChangeTransactionHelper _instance;

    private ChangeTransactionHelper()
    {
    }

    public static ChangeTransactionHelper Instance
    {
      get
      {
        if (ChangeTransactionHelper._instance == null)
          ChangeTransactionHelper._instance = new ChangeTransactionHelper();
        return ChangeTransactionHelper._instance;
      }
    }

    public int getChangeTransactionCount()
    {
      return this.ReloadMashupdependency(ChangeTransactionHelper.strTransactionFolderBar) + this.ReloadMashupdependency(ChangeTransactionHelper.strTransactionPort);
    }

    public int ReloadMashupdependency(string xrepPath)
    {
      return new JSONOSLSXREP_LIST_CONTENT().invokeReadChangeTransaction(xrepPath);
    }
  }
}
