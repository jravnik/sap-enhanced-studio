﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.IObjectSafety
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  [Guid("2403cf30-fee5-4d6f-a2c8-b2aba94b29ee")]
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  internal interface IObjectSafety
  {
    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetInterfaceSafetyOptions(ref Guid riid, out int pwdSupportedOptions, out int pwdEnableOptions);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetInterfaceSafetyOptions(ref Guid riid, int dwOptionSetMask, int dwEnabledOptions);
  }
}
