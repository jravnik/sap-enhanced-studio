﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper
{
  internal static class NativeMethods
  {
    [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
    internal static extern IntPtr InternetOpen(string lpszAgent, int dwAccessType, string lpszProxyName, string lpszProxyBypass, int dwFlags);

    [DllImport("wininet.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static extern bool InternetCloseHandle(IntPtr hInternet);

    [DllImport("wininet.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern bool InternetSetOption(IntPtr hInternet, INTERNET_OPTION dwOption, IntPtr lpBuffer, int lpdwBufferLength);

    [DllImport("wininet.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern bool InternetQueryOption(IntPtr hInternet, INTERNET_OPTION dwOption, ref INTERNET_PER_CONN_OPTION_LIST OptionList, ref int lpdwBufferLength);

    [DllImport("wininet.dll", SetLastError = true)]
    public static extern long DeleteUrlCacheEntry(string lpszUrlName);
  }
}
