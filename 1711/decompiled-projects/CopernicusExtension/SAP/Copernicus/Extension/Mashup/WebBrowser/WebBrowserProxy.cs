﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.WebBrowserProxy
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Extension.Mashup.EmbedBrowser;
using SAP.Copernicus.Extension.Mashup.MashupProjectNode.NodeMenusFilter;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  public class WebBrowserProxy
  {
    private static string strHtmlMashupTypeOnLine = "Html_Mashup";
    private static string strHtmlMashupQAFBuilder = "/SAP_BYD_TF/Mashups/AppMgmt/HtmlMashup_QAF.QA.uicomponent";
    private static string strHtmlMashupQAFInport = "PopulateHTMLMashup_In";
    private static string strHtmlMashupLogOnPage = "Logon";
    private static string strHtmlMashupCreatePageTitle = "New HTML Mashup - SAP Business ByDesign";
    private static string strHtmlMashupEditPageTitlePattern = "Edit HTML Mashup: (\\w+) - SAP Business ByDesign";
    private static string strUrlMashupTypeOnLine = "Url_Mashup";
    private static string strUrlMashupQAFBuilder = "/SAP_BYD_TF/Mashups/AppMgmt/UrlMashup_QAF.QA.uicomponent";
    private static string strUrlMashupQAFInport = "PopulateURLMashup_In";
    private static string strUrlMashupLogOnPage = "Logon";
    private static string strUrlMashupCreatePageTitle = "New URL Mashup - SAP Business ByDesign";
    private static string strUrlMashupEditPageTitlePattern = "Edit URL Mashup: (\\w+) - SAP Business ByDesign";
    private static string strDataMashupTypeOnLine = "Data_Mashup";
    private static string strDataMashupCreateGAFBuilder = "/SAP_BYD_TF/Mashups/AppMgmt/DataMashup_GAF.GAF.uicomponent";
    private static string strDataMashupCreateGAFInport = "NewDataMashup_In";
    private static string strDataMashupLogOnPage = "Logon";
    private static string strDataMashupCreatePageTitle = "New Data Mashup - SAP Business ByDesign";
    private static string strDataMashupEditPageTitlePattern = "Edit DATA Mashup: (\\w+) - SAP Business ByDesign";
    private static string strDataMashupEditOIFBuilder = "/SAP_BYD_TF/Mashups/AppMgmt/DataMashup_OIF.OIF.uicomponent";
    private static string strDataMashupNewEditInport = "EditDataMashup_In";
    private static string strRssAtomServiceTypeOnLine = "Feed_Service";
    private static string strRssAtomServiceQAFBuilder = "/SAP_BYD_TF/Mashups/AppMgmt/WebService_RSS_QAF.QA.uicomponent";
    private static string strRssAtomServiceQAFInport = "PopulateWebService_In";
    private static string strRssAtomServiceLogOnPage = "Logon";
    private static string strRssAtomServiceCreatePageTitle = "New HTML Mashup - SAP Business ByDesign";
    private static string strRssAtomServiceEditPageTitlePattern = "Edit HTML Mashup: (\\w+) - SAP Business ByDesign";
    private static string strRestServiceTypeOnLine = "Rest_Service";
    private static string strRestServiceQAFBuilder = "/SAP_BYD_TF/Mashups/AppMgmt/WebService_REST_QAF.QA.uicomponent";
    private static string strRestServiceQAFInport = "PopulateWebService_In";
    private static string strRestServiceLogOnPage = "Logon";
    private static string strRestServiceCreatePageTitle = "New REST Service - SAP Business ByDesign";
    private static string strRestServiceEditPageTitlePattern = "Edit REST Service: (\\w+) - SAP Business ByDesign";
    private static string strSoapServiceTypeOnLine = "Wsdl_Service";
    private static string strSoapServiceQAFBuilder = "/SAP_BYD_TF/Mashups/AppMgmt/WebService_SOAP_QAF.QA.uicomponent";
    private static string strSoapServiceQAFInport = "PopulateWebService_In";
    private static string strSoapServiceLogOnPage = "Logon";
    private static string strSoapServiceCreatePageTitle = "New SOAP Service - SAP Business ByDesign";
    private static string strSoapServiceEditPageTitlePattern = "Edit SOAP Service: (\\w+) - SAP Business ByDesign";
    private static string strEditingEditorStack = "SAP.Copernicus.Extension.Mashup.WebBrowser::DeleteMashupItemOnLineByMashupId - WebBrowser.Quit()&Dic.Remove() Exception";
    private static Dictionary<OnlineMashupType, Dictionary<MashupActionCategory, MashupConfigurationItem>> MashupStableConfiguration = new Dictionary<OnlineMashupType, Dictionary<MashupActionCategory, MashupConfigurationItem>>();
    private static Dictionary<string, OnlineMashupType> MashupTypeConfiguration = new Dictionary<string, OnlineMashupType>();
    private static OnlineMashupType _htmlMashupType = new OnlineMashupType(OnlineMashupCategory.HTMLMashup, WebBrowserProxy.strHtmlMashupQAFBuilder, WebBrowserProxy.strHtmlMashupQAFInport, WebBrowserProxy.strHtmlMashupLogOnPage);
    private int isSessionVadliate = 1;
    private static WebBrowserProxy instance;
    private Dictionary<string, MashupItemOnLineEditor> EditForMashupIdToEditor;
    private List<MashupItemOnLineEditor> CreationToEditor;
    private static OnlineMashupType _urlMashupType;
    private static OnlineMashupType _dataMashupType;
    private static OnlineMashupType _rssatomServiceType;
    private static OnlineMashupType _restServiceType;
    private static OnlineMashupType _soapServiceType;

    private WebBrowserProxy()
    {
      this.EditForMashupIdToEditor = new Dictionary<string, MashupItemOnLineEditor>();
      this.CreationToEditor = new List<MashupItemOnLineEditor>();
    }

    public static WebBrowserProxy getInstance()
    {
      if (WebBrowserProxy.instance == null)
        WebBrowserProxy.instance = new WebBrowserProxy();
      return WebBrowserProxy.instance;
    }

    internal static MashupConfigurationItem getMashupConfigItem(OnlineMashupType _mashupType, MashupActionCategory _actionType)
    {
      Dictionary<MashupActionCategory, MashupConfigurationItem> dictionary = (Dictionary<MashupActionCategory, MashupConfigurationItem>) null;
      MashupConfigurationItem configurationItem = (MashupConfigurationItem) null;
      if (WebBrowserProxy.MashupStableConfiguration.TryGetValue(_mashupType, out dictionary))
        dictionary.TryGetValue(_actionType, out configurationItem);
      return configurationItem;
    }

    internal void RemoveCreationOnLineEditor(MashupItemOnLineEditor _browser)
    {
      lock (this.CreationToEditor)
        this.CreationToEditor.Remove(_browser);
    }

    internal void AddCreationOnLineEditor(MashupItemOnLineEditor _browser)
    {
      lock (this.CreationToEditor)
        this.CreationToEditor.Add(_browser);
    }

    internal void ConvertToCreationItemtoEditor(MashupItemOnLineEditor _browser, string strMashupId)
    {
      this.RemoveCreationOnLineEditor(_browser);
      this.AddEditOnLineEditor(strMashupId, _browser);
      _browser.StatusOfEditor = EditorStatus.EditMashupUI;
      _browser.MashupItemId = strMashupId;
    }

    internal void ConvertToCreationItemtoEditorForSaveandClose(MashupItemOnLineEditor _browser, string strMashupId)
    {
      this.RemoveCreationOnLineEditor(_browser);
    }

    internal void RemoveEditOnLineEditor(string strMashupItemId)
    {
      lock (this.EditForMashupIdToEditor)
        this.EditForMashupIdToEditor.Remove(strMashupItemId);
    }

    internal void AddEditOnLineEditor(string strMashupItemId, MashupItemOnLineEditor _browser)
    {
      MashupItemOnLineEditor itemOnLineEditor;
      Trace.Assert(!this.EditForMashupIdToEditor.TryGetValue(strMashupItemId, out itemOnLineEditor), "Edit Item has been already Existed");
      lock (this.EditForMashupIdToEditor)
        this.EditForMashupIdToEditor.Add(strMashupItemId, _browser);
    }

    internal MashupItemOnLineEditor TryGetEditOnLineEditor(string strMashupItemId)
    {
      MashupItemOnLineEditor itemOnLineEditor = (MashupItemOnLineEditor) null;
      this.EditForMashupIdToEditor.TryGetValue(strMashupItemId, out itemOnLineEditor);
      return itemOnLineEditor;
    }

    public static string HtmlMashupStringTypeOnline
    {
      get
      {
        return WebBrowserProxy.strHtmlMashupTypeOnLine;
      }
    }

    private static OnlineMashupType HtmlMashupType
    {
      get
      {
        return WebBrowserProxy._htmlMashupType;
      }
    }

    public static string UrlMashupStringTypeOnline
    {
      get
      {
        return WebBrowserProxy.strUrlMashupTypeOnLine;
      }
    }

    private static OnlineMashupType UrlMashupType
    {
      get
      {
        return WebBrowserProxy._urlMashupType;
      }
    }

    public static string DataMashupStringTypeOnline
    {
      get
      {
        return WebBrowserProxy.strDataMashupTypeOnLine;
      }
    }

    private static OnlineMashupType DataMashupType
    {
      get
      {
        return WebBrowserProxy._dataMashupType;
      }
    }

    public static string RssAtomServiceStringTypeOnline
    {
      get
      {
        return WebBrowserProxy.strRssAtomServiceTypeOnLine;
      }
    }

    private static OnlineMashupType RssAtomServiceType
    {
      get
      {
        return WebBrowserProxy._rssatomServiceType;
      }
    }

    public static string RestServiceStringTypeOnline
    {
      get
      {
        return WebBrowserProxy.strRestServiceTypeOnLine;
      }
    }

    private static OnlineMashupType RestServiceType
    {
      get
      {
        return WebBrowserProxy._restServiceType;
      }
    }

    public static string SoapServiceStringTypeOnline
    {
      get
      {
        return WebBrowserProxy.strSoapServiceTypeOnLine;
      }
    }

    private static OnlineMashupType SoapServiceType
    {
      get
      {
        return WebBrowserProxy._soapServiceType;
      }
    }

    static WebBrowserProxy()
    {
      Dictionary<MashupActionCategory, MashupConfigurationItem> dictionary1 = new Dictionary<MashupActionCategory, MashupConfigurationItem>();
      dictionary1.Add(MashupActionCategory.CreateMashup, new MashupConfigurationItem(WebBrowserProxy._htmlMashupType, MashupActionCategory.CreateMashup, WebBrowserProxy.strHtmlMashupCreatePageTitle, AuthoringType.MashupAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      dictionary1.Add(MashupActionCategory.EditMashup, new MashupConfigurationItem(WebBrowserProxy._htmlMashupType, MashupActionCategory.EditMashup, WebBrowserProxy.strHtmlMashupEditPageTitlePattern, AuthoringType.MashupAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      WebBrowserProxy.MashupTypeConfiguration.Add(WebBrowserProxy.strHtmlMashupTypeOnLine, WebBrowserProxy._htmlMashupType);
      WebBrowserProxy.MashupStableConfiguration.Add(WebBrowserProxy._htmlMashupType, dictionary1);
      WebBrowserProxy._urlMashupType = new OnlineMashupType(OnlineMashupCategory.URLMashup, WebBrowserProxy.strUrlMashupQAFBuilder, WebBrowserProxy.strUrlMashupQAFInport, WebBrowserProxy.strUrlMashupLogOnPage);
      Dictionary<MashupActionCategory, MashupConfigurationItem> dictionary2 = new Dictionary<MashupActionCategory, MashupConfigurationItem>();
      dictionary2.Add(MashupActionCategory.CreateMashup, new MashupConfigurationItem(WebBrowserProxy._urlMashupType, MashupActionCategory.CreateMashup, WebBrowserProxy.strUrlMashupCreatePageTitle, AuthoringType.MashupAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      dictionary2.Add(MashupActionCategory.EditMashup, new MashupConfigurationItem(WebBrowserProxy._urlMashupType, MashupActionCategory.EditMashup, WebBrowserProxy.strUrlMashupEditPageTitlePattern, AuthoringType.MashupAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      WebBrowserProxy.MashupTypeConfiguration.Add(WebBrowserProxy.strUrlMashupTypeOnLine, WebBrowserProxy._urlMashupType);
      WebBrowserProxy.MashupStableConfiguration.Add(WebBrowserProxy._urlMashupType, dictionary2);
      WebBrowserProxy._dataMashupType = new OnlineMashupType(OnlineMashupCategory.DataMashup, WebBrowserProxy.strDataMashupCreateGAFBuilder, WebBrowserProxy.strDataMashupCreateGAFInport, WebBrowserProxy.strDataMashupLogOnPage);
      Dictionary<MashupActionCategory, MashupConfigurationItem> dictionary3 = new Dictionary<MashupActionCategory, MashupConfigurationItem>();
      dictionary3.Add(MashupActionCategory.CreateMashup, new MashupConfigurationItem(WebBrowserProxy._dataMashupType, MashupActionCategory.CreateMashup, WebBrowserProxy.strDataMashupCreatePageTitle, AuthoringType.MashupAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      dictionary3.Add(MashupActionCategory.EditMashup, new MashupConfigurationItem(WebBrowserProxy._dataMashupType, MashupActionCategory.EditMashup, WebBrowserProxy.strDataMashupEditPageTitlePattern, AuthoringType.MashupAuthoring, true, WebBrowserProxy.strDataMashupEditOIFBuilder, WebBrowserProxy.strDataMashupNewEditInport, new Dictionary<string, string>()
      {
        {
          "param.IsInEditMode",
          "true"
        }
      }));
      WebBrowserProxy.MashupTypeConfiguration.Add(WebBrowserProxy.strDataMashupTypeOnLine, WebBrowserProxy._dataMashupType);
      WebBrowserProxy.MashupStableConfiguration.Add(WebBrowserProxy._dataMashupType, dictionary3);
      WebBrowserProxy._rssatomServiceType = new OnlineMashupType(OnlineMashupCategory.RSSAtomService, WebBrowserProxy.strRssAtomServiceQAFBuilder, WebBrowserProxy.strRssAtomServiceQAFInport, WebBrowserProxy.strRssAtomServiceLogOnPage);
      Dictionary<MashupActionCategory, MashupConfigurationItem> dictionary4 = new Dictionary<MashupActionCategory, MashupConfigurationItem>();
      dictionary4.Add(MashupActionCategory.CreateMashup, new MashupConfigurationItem(WebBrowserProxy._rssatomServiceType, MashupActionCategory.CreateMashup, WebBrowserProxy.strRssAtomServiceCreatePageTitle, AuthoringType.ServiceAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      dictionary4.Add(MashupActionCategory.EditMashup, new MashupConfigurationItem(WebBrowserProxy._rssatomServiceType, MashupActionCategory.EditMashup, WebBrowserProxy.strRssAtomServiceEditPageTitlePattern, AuthoringType.ServiceAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      WebBrowserProxy.MashupTypeConfiguration.Add(WebBrowserProxy.strRssAtomServiceTypeOnLine, WebBrowserProxy._rssatomServiceType);
      WebBrowserProxy.MashupStableConfiguration.Add(WebBrowserProxy._rssatomServiceType, dictionary4);
      WebBrowserProxy._restServiceType = new OnlineMashupType(OnlineMashupCategory.RESTService, WebBrowserProxy.strRestServiceQAFBuilder, WebBrowserProxy.strRestServiceQAFInport, WebBrowserProxy.strRestServiceLogOnPage);
      Dictionary<MashupActionCategory, MashupConfigurationItem> dictionary5 = new Dictionary<MashupActionCategory, MashupConfigurationItem>();
      dictionary5.Add(MashupActionCategory.CreateMashup, new MashupConfigurationItem(WebBrowserProxy._restServiceType, MashupActionCategory.CreateMashup, WebBrowserProxy.strRestServiceCreatePageTitle, AuthoringType.ServiceAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      dictionary5.Add(MashupActionCategory.EditMashup, new MashupConfigurationItem(WebBrowserProxy._restServiceType, MashupActionCategory.EditMashup, WebBrowserProxy.strRestServiceEditPageTitlePattern, AuthoringType.ServiceAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      WebBrowserProxy.MashupTypeConfiguration.Add(WebBrowserProxy.strRestServiceTypeOnLine, WebBrowserProxy._restServiceType);
      WebBrowserProxy.MashupStableConfiguration.Add(WebBrowserProxy._restServiceType, dictionary5);
      WebBrowserProxy._soapServiceType = new OnlineMashupType(OnlineMashupCategory.SOAPService, WebBrowserProxy.strSoapServiceQAFBuilder, WebBrowserProxy.strSoapServiceQAFInport, WebBrowserProxy.strSoapServiceLogOnPage);
      Dictionary<MashupActionCategory, MashupConfigurationItem> dictionary6 = new Dictionary<MashupActionCategory, MashupConfigurationItem>();
      dictionary6.Add(MashupActionCategory.CreateMashup, new MashupConfigurationItem(WebBrowserProxy._soapServiceType, MashupActionCategory.CreateMashup, WebBrowserProxy.strSoapServiceCreatePageTitle, AuthoringType.ServiceAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      dictionary6.Add(MashupActionCategory.EditMashup, new MashupConfigurationItem(WebBrowserProxy._soapServiceType, MashupActionCategory.EditMashup, WebBrowserProxy.strSoapServiceEditPageTitlePattern, AuthoringType.ServiceAuthoring, false, (string) null, (string) null, (Dictionary<string, string>) null));
      WebBrowserProxy.MashupTypeConfiguration.Add(WebBrowserProxy.strSoapServiceTypeOnLine, WebBrowserProxy._soapServiceType);
      WebBrowserProxy.MashupStableConfiguration.Add(WebBrowserProxy._soapServiceType, dictionary6);
      AppDomain.CurrentDomain.ProcessExit += new EventHandler(WebBrowserProxy.WebBrowserProxyHostProxy_Exited);
    }

    private static void WebBrowserProxyHostProxy_Exited(object sender, EventArgs e)
    {
      WebBrowserProxy.getInstance().CloseAllRunningBrowser();
    }

    public void AsynCreateMashupItemOnLine(string strMashupItemType, string strInputNameWizard)
    {
      string EV_SUCCESS;
      string EV_ENABLED;
      MashupNodeMenusFilter.isKeyUserEnabled(out EV_SUCCESS, out EV_ENABLED);
      if (EV_SUCCESS != "X" || EV_ENABLED != "X")
      {
        if (!CopernicusMessageBox.Show(SAP.Copernicus.Extension.Mashup.Resource.EnableAdminMode, SAP.Copernicus.Extension.Mashup.Resource.MsgBYD, MessageBoxButtons.YesNo).Equals((object) DialogResult.Yes))
          return;
        MashupNodeMenusFilter.setKeyUser("X");
      }
      ThreadPool.QueueUserWorkItem((WaitCallback) (state =>
      {
        if (this.isSessionVadliate == 0)
          IESecurityOption.CleanUpSession();
        IESecurityOption.EnableSecurityOption();
        OnlineMashupType _mashupType = (OnlineMashupType) null;
        if (!WebBrowserProxy.MashupTypeConfiguration.TryGetValue(strMashupItemType, out _mashupType))
          throw new InvalidOperationException("MashupType Must Define in advance");
        new MashupItemOnLineEditor((string) null, EditorStatus.CreateMashupUI).NavigateToTargetQAFBuilder(_mashupType, MashupActionCategory.CreateMashup, (string) null, strInputNameWizard);
      }));
    }

    public void AsynEditMashupItemOnLine(string strMashupItemType, string strMashupItemId)
    {
      ThreadPool.QueueUserWorkItem((WaitCallback) (state =>
      {
        IESecurityOption.EnableSecurityOption();
        OnlineMashupType _mashupType = (OnlineMashupType) null;
        if (!WebBrowserProxy.MashupTypeConfiguration.TryGetValue(strMashupItemType, out _mashupType))
          throw new InvalidOperationException("MashupType Must Define in advance");
        new MashupItemOnLineEditor((string) null, EditorStatus.CreateMashupUI).NavigateToTargetQAFBuilder(_mashupType, MashupActionCategory.EditMashup, strMashupItemId, (string) null);
      }));
    }

    public void AsynInvokeSessionValidation()
    {
      ThreadPool.QueueUserWorkItem((WaitCallback) (StateIconIndex =>
      {
        ConnectionDataSet.SystemDataRow connectedSystem = Connection.getInstance().ConnectedSystem;
        string name = connectedSystem.Name;
        int client = connectedSystem.Client;
        string user = connectedSystem.User;
        string xrepSessionID = LocalFileHandler.ReadFileFromDisk(Path.Combine(SAP.Copernicus.Core.Util.Util.ProjectsLocation + "\\" + name, user + ".session"));
        Trace.Assert(!string.IsNullOrEmpty(xrepSessionID));
        LogonData logonData = (LogonData) null;
        try
        {
          logonData = WebUtil.CheckHttpAuthentication(connectedSystem, user, xrepSessionID, true);
        }
        catch (Exception ex)
        {
          Trace.WriteLine(ex.StackTrace);
        }
        if (logonData != null)
          return;
        Interlocked.CompareExchange(ref this.isSessionVadliate, 0, 1);
      }));
    }

    public void DeleteMashupItemOnLineByMashupId(string strMashupItemId)
    {
      MashupItemOnLineEditor _webBrowserforMashupItem = (MashupItemOnLineEditor) null;
      if (!this.EditForMashupIdToEditor.TryGetValue(strMashupItemId, out _webBrowserforMashupItem))
        return;
      Trace.Assert(_webBrowserforMashupItem != null, "SAP.Copernicus.Extension.Mashup.WebBrowser::public void DeleteMashupItemOnLineByMashupId(string strMashupItemId) -> WebBrowser Exist in dictionary, but return null for proxy of WebBrowser");
      this.CloseEditingEditorForMashupPipeId(strMashupItemId, _webBrowserforMashupItem, WebBrowserProxy.strEditingEditorStack);
    }

    private void CloseEditingEditorForMashupPipeId(string strMashupItemId, MashupItemOnLineEditor _webBrowserforMashupItem, string strCallStack)
    {
      try
      {
        if (_webBrowserforMashupItem.InnerWebBrowser.InvokeRequired)
          _webBrowserforMashupItem.InnerWebBrowser.Invoke(new Action(delegate { _webBrowserforMashupItem.InnerWebBrowser.Close(); }));
        else
          _webBrowserforMashupItem.InnerWebBrowser.Close();
        lock (this.EditForMashupIdToEditor)
          this.EditForMashupIdToEditor.Remove(strMashupItemId);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(strCallStack);
        Trace.WriteLine("Message - {0}", ex.Message);
        Trace.WriteLine("StackTrace - {1}", ex.StackTrace);
      }
    }

    private void CloseCreationEditor(MashupItemOnLineEditor _webBrowserforMashupItem, string strCallStack)
    {
      try
      {
        if (_webBrowserforMashupItem.InnerWebBrowser.InvokeRequired)
          _webBrowserforMashupItem.InnerWebBrowser.Invoke(new Action(delegate { _webBrowserforMashupItem.InnerWebBrowser.Close(); }));
        else
          _webBrowserforMashupItem.InnerWebBrowser.Close();
        lock (this.CreationToEditor)
          this.CreationToEditor.Remove(_webBrowserforMashupItem);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(strCallStack);
        Trace.WriteLine("Message - {0}", ex.Message);
        Trace.WriteLine("StackTrace - {1}", ex.StackTrace);
      }
    }

    public void CloseAllRunningBrowser()
    {
      using (Dictionary<string, MashupItemOnLineEditor>.ValueCollection.Enumerator enumerator = this.EditForMashupIdToEditor.Clone().Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          MashupItemOnLineEditor formEditorForEditting = enumerator.Current;
          if (formEditorForEditting.InnerWebBrowser.InvokeRequired)
            formEditorForEditting.InnerWebBrowser.Invoke(new Action(delegate
            {
              if (formEditorForEditting.InnerWebBrowser == null)
                return;
              formEditorForEditting.InnerWebBrowser.Close();
            }));
          else if (formEditorForEditting.InnerWebBrowser != null)
            formEditorForEditting.InnerWebBrowser.Close();
        }
      }
      this.EditForMashupIdToEditor.Clear();
      using (List<MashupItemOnLineEditor>.Enumerator enumerator = this.CreationToEditor.Clone().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          MashupItemOnLineEditor formEditorForCreation = enumerator.Current;
          if (formEditorForCreation.InnerWebBrowser.InvokeRequired)
            formEditorForCreation.InnerWebBrowser.Invoke(new Action(delegate
            {
              if (formEditorForCreation.InnerWebBrowser == null)
                return;
              formEditorForCreation.InnerWebBrowser.Close();
            }));
          else if (formEditorForCreation.InnerWebBrowser != null)
            formEditorForCreation.InnerWebBrowser.Close();
        }
      }
      this.CreationToEditor.Clear();
    }
  }
}
