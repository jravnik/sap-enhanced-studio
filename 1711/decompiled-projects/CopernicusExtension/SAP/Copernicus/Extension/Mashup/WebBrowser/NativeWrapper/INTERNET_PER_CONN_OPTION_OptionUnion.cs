﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.INTERNET_PER_CONN_OPTION_OptionUnion
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper
{
  [StructLayout(LayoutKind.Explicit)]
  public struct INTERNET_PER_CONN_OPTION_OptionUnion
  {
    [FieldOffset(0)]
    public int dwValue;
    [FieldOffset(0)]
    public IntPtr pszValue;
    [FieldOffset(0)]
    public System.Runtime.InteropServices.ComTypes.FILETIME ftValue;
  }
}
