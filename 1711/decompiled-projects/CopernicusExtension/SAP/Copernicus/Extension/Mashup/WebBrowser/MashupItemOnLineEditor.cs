﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.MashupItemOnLineEditor
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Protocol;
using System;
using System.Diagnostics;
using System.Threading;
using System.Web;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  public class MashupItemOnLineEditor
  {
    private string strMashupItemId;
    private EditorStatus statusOfEditor;
    private MashupActionCategory actionOfQAFStatus;
    private WebBrowserWinFormHost m_hostedBrowser;
    private OnlineMashupType mashupType;
    private string strLogOnPageTile;
    private string strFinalizePageTitlePattern;

    public string MashupItemId
    {
      get
      {
        return this.strMashupItemId;
      }
      set
      {
        if (value == null || !(value != this.strMashupItemId))
          return;
        this.strMashupItemId = value;
      }
    }

    public EditorStatus StatusOfEditor
    {
      get
      {
        return this.statusOfEditor;
      }
      set
      {
        if (value == this.statusOfEditor)
          return;
        this.statusOfEditor = value;
      }
    }

    public MashupActionCategory ActionOfQAF
    {
      get
      {
        return this.actionOfQAFStatus;
      }
    }

    public WebBrowserWinFormHost InnerWebBrowser
    {
      get
      {
        return this.m_hostedBrowser;
      }
    }

    public OnlineMashupType MashupType
    {
      get
      {
        return this.mashupType;
      }
    }

    public string LogOnPageTitle
    {
      get
      {
        return this.strLogOnPageTile;
      }
    }

    public MashupItemOnLineEditor(string strMashupItemId = null, EditorStatus statusOfEditor = EditorStatus.CreateMashupUI)
    {
      this.strMashupItemId = strMashupItemId;
      this.statusOfEditor = statusOfEditor;
    }

    internal void NavigateToTargetQAFBuilder(OnlineMashupType _mashupType, MashupActionCategory _actionOfQAFStatus, string strMashupItemId = null, string strInputNameWizard = null)
    {
      MashupConfigurationItem mashupConfigItem = WebBrowserProxy.getMashupConfigItem(_mashupType, _actionOfQAFStatus);
      this.mashupType = _mashupType;
      this.strMashupItemId = strMashupItemId;
      this.actionOfQAFStatus = _actionOfQAFStatus;
      Trace.WriteLine(string.Format("{0} {1} {2}", (object) _mashupType.ToString(), (object) _actionOfQAFStatus.ToString(), (object) strMashupItemId));
      string str = (string) null;
      if (this.actionOfQAFStatus == MashupActionCategory.CreateMashup)
      {
        str = mashupConfigItem.ToString();
        this.statusOfEditor = EditorStatus.CreateMashupUI;
      }
      else if (this.actionOfQAFStatus == MashupActionCategory.EditMashup)
      {
        if (strMashupItemId == null)
          throw new InvalidOperationException("MashupItemId shouldn't be null");
        str = mashupConfigItem.ToString() + strMashupItemId;
        this.statusOfEditor = EditorStatus.EditMashupUI;
      }
      string strSystemHttps = Connection.getInstance().ConnectedSystem.IsSSLPortNull() ? "http" : "https://" + Connection.getInstance().ConnectedSystem.Host + str + "&param.IsPartnerMode=true";
      if (this.actionOfQAFStatus == MashupActionCategory.CreateMashup)
        strSystemHttps = strSystemHttps + "&param.DisplayName=" + HttpUtility.HtmlEncode(strInputNameWizard);
      WebBrowserProxy instance = WebBrowserProxy.getInstance();
      if (this.statusOfEditor == EditorStatus.EditMashupUI)
      {
        MashupItemOnLineEditor editOnLineEditor = instance.TryGetEditOnLineEditor(this.strMashupItemId);
        if (editOnLineEditor != null)
          this.m_hostedBrowser = editOnLineEditor.m_hostedBrowser;
      }
      if (this.m_hostedBrowser == null)
      {
        this.strLogOnPageTile = this.MashupType.MashupTargetLogOnPageTitle;
        this.strFinalizePageTitlePattern = mashupConfigItem.HtmlMashupFinalizedPageOrPattern;
        if (this.statusOfEditor == EditorStatus.CreateMashupUI)
          instance.AddCreationOnLineEditor(this);
        else if (this.statusOfEditor == EditorStatus.EditMashupUI)
          instance.AddEditOnLineEditor(this.strMashupItemId, this);
        InternetProxy _proxy = UtilExtension.GetHttpProxy();
        Thread thread = new Thread((ThreadStart) (() =>
        {
          this.m_hostedBrowser = new WebBrowserWinFormHost(_proxy, this);
          this.m_hostedBrowser.NavigationURL = strSystemHttps;
          Application.Run((Form) this.m_hostedBrowser);
        }));
        thread.SetApartmentState(ApartmentState.STA);
        thread.Start();
      }
      else
        this.m_hostedBrowser.Navigate(strSystemHttps);
    }
  }
}
