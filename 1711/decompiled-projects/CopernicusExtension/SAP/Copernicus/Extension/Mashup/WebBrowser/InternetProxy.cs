﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.InternetProxy
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  public class InternetProxy
  {
    public static readonly InternetProxy NoProxy = new InternetProxy()
    {
      Address = string.Empty,
      Password = string.Empty,
      ProxyName = string.Empty,
      UserName = string.Empty
    };
    private static List<InternetProxy> proxyList = (List<InternetProxy>) null;

    public string ProxyName { get; set; }

    public string Address { get; set; }

    public string UserName { get; set; }

    public string Password { get; set; }

    public override bool Equals(object obj)
    {
      if (!(obj is InternetProxy))
        return false;
      InternetProxy internetProxy = obj as InternetProxy;
      bool flag1 = true;
      bool flag2 = this.Address == null == (internetProxy.Address == null) && (internetProxy.Address == null ? flag1 : flag1 && this.Address.Equals(internetProxy.Address, StringComparison.OrdinalIgnoreCase));
      if (!flag2)
        return flag2;
      bool flag3 = this.UserName == null == (internetProxy.UserName == null) && (internetProxy.UserName == null ? flag2 : flag2 && this.UserName.EndsWith(internetProxy.UserName, StringComparison.OrdinalIgnoreCase));
      if (!flag3)
        return flag3;
      return this.Password == null == (internetProxy.Password == null) && (internetProxy.Password == null ? flag3 : flag3 && this.Password.Equals(internetProxy.Password, StringComparison.Ordinal));
    }

    public override int GetHashCode()
    {
      return this.Address.GetHashCode() + this.UserName.GetHashCode() + this.Password.GetHashCode();
    }

    public static List<InternetProxy> ProxyList
    {
      get
      {
        if (InternetProxy.proxyList == null)
        {
          InternetProxy.proxyList = new List<InternetProxy>();
          try
          {
            foreach (XElement element in XElement.Load("ProxyList.xml").Elements((XName) "Proxy"))
              InternetProxy.proxyList.Add(new InternetProxy()
              {
                ProxyName = element.Element((XName) "ProxyName").Value,
                Address = element.Element((XName) "Address").Value,
                UserName = element.Element((XName) "UserName").Value,
                Password = element.Element((XName) "Password").Value
              });
          }
          catch (Exception ex)
          {
          }
        }
        return InternetProxy.proxyList;
      }
    }
  }
}
