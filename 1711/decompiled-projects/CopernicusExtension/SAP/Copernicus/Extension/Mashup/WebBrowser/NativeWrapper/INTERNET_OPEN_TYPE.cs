﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.INTERNET_OPEN_TYPE
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper
{
  public enum INTERNET_OPEN_TYPE
  {
    INTERNET_OPEN_TYPE_PRECONFIG = 0,
    INTERNET_OPEN_TYPE_DIRECT = 1,
    INTERNET_OPEN_TYPE_PROXY = 3,
    INTERNET_OPEN_TYPE_PRECONFIG_WITH_NO_AUTOPROXY = 4,
  }
}
