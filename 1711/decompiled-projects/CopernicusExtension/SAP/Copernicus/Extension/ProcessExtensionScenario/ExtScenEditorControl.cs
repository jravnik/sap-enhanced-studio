﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenEditorControl
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Shell;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.ProcessExtensionScenario
{
  public class ExtScenEditorControl : UserControl
  {
    private XRepMapper xRepMapper = XRepMapper.GetInstance();
    private IContainer components;
    private TabPage general;
    private ExtScenSelectionViewControl extScenSelectionControl;
    private Label title;
    private TabControl tabControlExtScenario;
    private ImageList imageList1;
    private ToolTip ProcExtSceanrioInfo_Tooltip;
    private string filePath;
    private bool isMaintenanceMode;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (ExtScenEditorControl));
      this.general = new TabPage();
      this.extScenSelectionControl = new ExtScenSelectionViewControl();
      this.title = new Label();
      this.imageList1 = new ImageList(this.components);
      this.tabControlExtScenario = new TabControl();
      this.ProcExtSceanrioInfo_Tooltip = new ToolTip(this.components);
      this.general.SuspendLayout();
      this.tabControlExtScenario.SuspendLayout();
      this.SuspendLayout();
      this.general.AutoScroll = true;
      this.general.BackColor = SystemColors.Control;
      this.general.BackgroundImageLayout = ImageLayout.None;
      this.general.Controls.Add((Control) this.extScenSelectionControl);
      this.general.Controls.Add((Control) this.title);
      this.general.Location = new Point(4, 4);
      this.general.Name = "general";
      this.general.Padding = new Padding(3);
      this.general.Size = new Size(617, 773);
      this.general.TabIndex = 0;
      this.general.Text = "General";
      this.extScenSelectionControl.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.extScenSelectionControl.BackColor = SystemColors.Control;
      this.extScenSelectionControl.BackgroundImageLayout = ImageLayout.None;
      this.extScenSelectionControl.Dock = DockStyle.Fill;
      this.extScenSelectionControl.Location = new Point(3, 5);
      this.extScenSelectionControl.Margin = new Padding(4, 5, 4, 5);
      this.extScenSelectionControl.MinimumSize = new Size(434, 566);
      this.extScenSelectionControl.Name = "extScenSelectionControl";
      this.extScenSelectionControl.Size = new Size(611, 765);
      this.extScenSelectionControl.TabIndex = 2;
      this.extScenSelectionControl.ExScenCheckedEvent += new ExtScenSelectionViewControl.ExScenCheckedHandler(this.OnScenarioChecked);
      this.title.AutoSize = true;
      this.title.Dock = DockStyle.Top;
      this.title.Font = new Font("Microsoft Sans Serif", 1f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.title.Location = new Point(3, 3);
      this.title.Name = "title";
      this.title.Size = new Size(0, 2);
      this.title.TabIndex = 0;
      this.title.Click += new EventHandler(this.label1_Click);
      this.imageList1.ImageStream = (ImageListStreamer) componentResourceManager.GetObject("imageList1.ImageStream");
      this.imageList1.TransparentColor = Color.Transparent;
      this.imageList1.Images.SetKeyName(0, "InformationMark.png");
      this.tabControlExtScenario.Alignment = TabAlignment.Bottom;
      this.tabControlExtScenario.Controls.Add((Control) this.general);
      this.tabControlExtScenario.Dock = DockStyle.Fill;
      this.tabControlExtScenario.Location = new Point(0, 0);
      this.tabControlExtScenario.Name = "tabControlExtScenario";
      this.tabControlExtScenario.SelectedIndex = 0;
      this.tabControlExtScenario.Size = new Size(625, 799);
      this.tabControlExtScenario.TabIndex = 0;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoScroll = true;
      this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.BackColor = Color.Transparent;
      this.Controls.Add((Control) this.tabControlExtScenario);
      this.Name = nameof (ExtScenEditorControl);
      this.Size = new Size(625, 799);
      this.Load += new EventHandler(this.ExtScenEditorControl_Load);
      this.general.ResumeLayout(false);
      this.general.PerformLayout();
      this.tabControlExtScenario.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public void LoadFiles(string xsFilePath)
    {
      try
      {
        this.extScenSelectionControl.loadScenarioXML(xsFilePath);
        if (XRepMapper.GetInstance().GetSolutionEditStatus() == XRepMapper.EditModes.Locked)
          this.Enabled = false;
      }
      catch (Exception ex)
      {
        if (ex.Message.ToString() != string.Empty)
        {
          CopernicusExtensionPackage.Instance.ErrorListProvider.ClearErrorTaskItems(xsFilePath, 1);
          string errorMessage = ex.Message.ToString();
          CopernicusExtensionPackage.Instance.ErrorListProvider.CreateErrorTaskItem(this, xsFilePath, errorMessage, TaskErrorCategory.Error, 1, 0);
        }
      }
      this.filePath = xsFilePath;
      string xbo = (string) null;
      string node = (string) null;
      bool enabled = true;
      if (ExtScenDataCache.GetInstance().isScenarioInUse(this.extScenSelectionControl.getScenarioName(), out xbo, out node))
        enabled = false;
      this.extScenSelectionControl.setChomboBoxProperties(enabled);
      if (!enabled && this.Enabled)
      {
        string errorMessage = string.Format(SAP.Copernicus.Extension.Resources.ScenarioInUseMsg, (object) xbo, (object) node);
        CopernicusExtensionPackage.Instance.ErrorListProvider.CreateErrorTaskItem(this, xsFilePath, errorMessage, TaskErrorCategory.Message, 1, 0);
      }
      if (XRepMapper.GetInstance().GetSolutionEditStatus() != XRepMapper.EditModes.InMaintenance)
        return;
      this.isMaintenanceMode = true;
    }

    public string SaveFile(string xsFilePath)
    {
      string scenarioName = (string) null;
      string boName = (string) null;
      string boNameSpace = (string) null;
      string boNodeName = (string) null;
      string scenarioXml = this.extScenSelectionControl.getScenarioXML(out scenarioName, out boName, out boNameSpace, out boNodeName);
      this.extScenSelectionControl.saveModelToFileSystem(xsFilePath);
      IDictionary<string, string> fileAttribsToBesaved = (IDictionary<string, string>) new Dictionary<string, string>();
      fileAttribsToBesaved.Add("ExtensionScenarioName", scenarioName);
      fileAttribsToBesaved.Add("esrBONameSpace", boNameSpace);
      fileAttribsToBesaved.Add("esrBOName", boName);
      fileAttribsToBesaved.Add("esrBONodeNAME", boNodeName);
      string xbo = (string) null;
      string node = (string) null;
      ExtScenDataCache instance = ExtScenDataCache.GetInstance();
      if (!instance.isScenarioInUse(scenarioName, out xbo, out node))
      {
        this.xRepMapper.SaveAndActivateLocalFileInXRep(xsFilePath.ToString(), scenarioXml, fileAttribsToBesaved, false);
        instance.Reset();
      }
      else
      {
        string errorMessage = string.Format(SAP.Copernicus.Extension.Resources.ScenarioIsReferenced, (object) scenarioName, (object) xbo, (object) node);
        CopernicusExtensionPackage.Instance.ErrorListProvider.CreateErrorTaskItem(this, xsFilePath, errorMessage, TaskErrorCategory.Warning, 1, 0);
      }
      return xsFilePath.Substring(0, xsFilePath.LastIndexOf("."));
    }

    public void closeEditor()
    {
      CopernicusExtensionPackage.Instance.ErrorListProvider.ClearErrorTaskItems();
    }

    public ExtScenEditorControl()
    {
      this.InitializeComponent();
      this.extScenSelectionControl.ContentChanged += new EventHandler(this.RaiseContentChanged);
    }

    private void label1_Click(object sender, EventArgs e)
    {
    }

    private void OnScenarioChecked(object sender, ExScenCheckedEventArgs e)
    {
    }

    private void ExtScenEditorControl_Load(object sender, EventArgs e)
    {
      this.extScenSelectionControl.ExScenSelectionErrorEvent += new ExtScenSelectionViewControl.ExScenSelectionErrorEventHandler(this.OnErrorEvent);
    }

    private void OnErrorEvent(object sender, ExScenSelectionErrorEventArgs ex)
    {
      if (ex.ErrorText.ToString() != string.Empty)
      {
        CopernicusExtensionPackage.Instance.ErrorListProvider.ClearErrorTaskItems(this.filePath, 1);
        CopernicusExtensionPackage.Instance.ErrorListProvider.CreateErrorTaskItem(this, this.filePath, ex.ErrorText.ToString(), TaskErrorCategory.Error, 1, 0);
      }
      else
        CopernicusExtensionPackage.Instance.ErrorListProvider.ClearErrorTaskItems(this.filePath, 1);
    }

    public event EventHandler ContentChanged;

    private void RaiseContentChanged(object sender, EventArgs e)
    {
      if (this.ContentChanged == null)
        return;
      this.ContentChanged(sender, e);
    }
  }
}
