﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionFirstPage
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Wizard;
using SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.ProcessExtensionScenario
{
  internal class ExtScenSelectionFirstPage : InternalWizardPage
  {
    private ExtScenSelectionViewControl extScenSelectionViewControl1;
    private ExtScenSelectionWizard parentWizard;
    private Label ErrorText;

    private void InitializeComponent()
    {
      this.ErrorText = new Label();
      this.extScenSelectionViewControl1 = new ExtScenSelectionViewControl();
      this.SuspendLayout();
      this.Banner.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.Banner.AutoSize = true;
      this.Banner.Dock = DockStyle.None;
      this.Banner.Size = new Size(718, 64);
      this.Banner.Subtitle = "Create an extension scenario based on SAP Business ByDesign templates.";
      this.Banner.Title = "";
      this.Banner.Load += new EventHandler(this.Banner_Load);
      this.ErrorText.AutoSize = true;
      this.ErrorText.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.ErrorText.ForeColor = Color.Red;
      this.ErrorText.Location = new Point(18, 776);
      this.ErrorText.Name = "ErrorText";
      this.ErrorText.Size = new Size(70, 18);
      this.ErrorText.TabIndex = 2;
      this.ErrorText.Text = "ErrorText";
      this.extScenSelectionViewControl1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.extScenSelectionViewControl1.AutoSize = true;
      this.extScenSelectionViewControl1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.extScenSelectionViewControl1.BackColor = Color.Transparent;
      this.extScenSelectionViewControl1.BackgroundImageLayout = ImageLayout.None;
      this.extScenSelectionViewControl1.Location = new Point(4, 60);
      this.extScenSelectionViewControl1.Margin = new Padding(4, 5, 4, 5);
      this.extScenSelectionViewControl1.MinimumSize = new Size(434, 566);
      this.extScenSelectionViewControl1.Name = "extScenSelectionViewControl1";
      this.extScenSelectionViewControl1.Size = new Size(734, 674);
      this.extScenSelectionViewControl1.TabIndex = 1;
      this.extScenSelectionViewControl1.ExScenSelectionErrorEvent += new ExtScenSelectionViewControl.ExScenSelectionErrorEventHandler(this.OnErrorEvent);
      this.extScenSelectionViewControl1.ExScenCheckedEvent += new ExtScenSelectionViewControl.ExScenCheckedHandler(this.OnScenarioChecked);
      this.AutoScaleMode = AutoScaleMode.Inherit;
      this.AutoScroll = false;
      this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.BackColor = SystemColors.Control;
      this.Controls.Add((Control) this.extScenSelectionViewControl1);
      this.Controls.Add((Control) this.ErrorText);
      this.ForeColor = SystemColors.ActiveCaptionText;
      this.MaximumSize = new Size(900, 780);
      this.Name = nameof (ExtScenSelectionFirstPage);
      this.Size = new Size(739, 780);
      this.SetActive += new CancelEventHandler(this.FormExtFieldsSelectionFirstPage_SetActive);
      this.WizardFinish += new CancelEventHandler(this.FormExtFieldsSelectionFirstPage_WizardFinish);
      this.Controls.SetChildIndex((Control) this.ErrorText, 0);
      this.Controls.SetChildIndex((Control) this.Banner, 0);
      this.Controls.SetChildIndex((Control) this.extScenSelectionViewControl1, 0);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public ExtScenSelectionFirstPage(ExtScenSelectionWizard p1)
      : base(HELP_IDS.BDS_PROCESSEXTENSION_CREATE)
    {
      this.parentWizard = p1;
      this.InitializeComponent();
      this.ErrorText.Hide();
    }

    private void FormExtFieldsSelectionFirstPage_SetActive(object sender, CancelEventArgs e)
    {
      this.SetWizardButtons(WizardPageType.FirstAndLast);
      this.EnableFinishButton(false);
    }

    private void FormExtFieldsSelectionFirstPage_WizardBack(object sender, CopernicusWizardPageEventArgs e)
    {
    }

    private void FormExtFieldsSelectionFirstPage_WizardFinish(object sender, CancelEventArgs e)
    {
      if (this.extScenSelectionViewControl1.noScenarioChecked() == 0)
      {
        this.ErrorText.Text = string.Format(Resources.NO_SCENARIOS_CHECKED);
      }
      else
      {
        string selNamespace = string.Empty;
        string selBO = string.Empty;
        string selNode = string.Empty;
        bool selection = this.extScenSelectionViewControl1.getSelection(out selNamespace, out selBO, out selNode);
        string scenarioXml = this.extScenSelectionViewControl1.getScenarioXML(this.parentWizard.xsFileName);
        if (selection)
          this.parentWizard.FinishWizard(selNamespace, selBO, selNode, scenarioXml);
        else
          this.ErrorText.Text = "Selection Parameter not consistent.";
      }
    }

    private void OnErrorEvent(object sender, ExScenSelectionErrorEventArgs e)
    {
      this.ErrorText.Text = e.ErrorText;
      this.ErrorText.BringToFront();
      this.ErrorText.Show();
    }

    private void OnScenarioChecked(object sender, ExScenCheckedEventArgs e)
    {
      if (e.ScenChecked)
        this.EnableFinishButton(true);
      else
        this.EnableFinishButton(false);
    }

    private void Banner_Load(object sender, EventArgs e)
    {
    }
  }
}
