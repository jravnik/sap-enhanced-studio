﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView.TreeModel.FlowNode
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView.TreeModel
{
  public class FlowNode : BaseNode
  {
    private int flow_idx = 10;
    private string flow = "scen_flow";
    private string nsName;

    public FlowNode(string scName, string scTooltip)
      : base(scName)
    {
      this.nsName = scName;
      this.Text = this.nsName;
      this.ToolTipText = scTooltip;
      this.ImageIndex = this.flow_idx;
      this.SelectedImageIndex = this.flow_idx;
    }

    public new string getData()
    {
      return this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.Namespace;
    }

    public override string Namespace
    {
      get
      {
        return this.nsName;
      }
    }

    public string getNodeScenarioType
    {
      get
      {
        return this.flow;
      }
    }

    public override void refresh()
    {
    }
  }
}
