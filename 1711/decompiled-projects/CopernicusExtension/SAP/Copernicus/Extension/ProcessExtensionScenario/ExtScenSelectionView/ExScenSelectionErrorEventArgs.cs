﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView.ExScenSelectionErrorEventArgs
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System;

namespace SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView
{
  public class ExScenSelectionErrorEventArgs : EventArgs
  {
    private string errorText;

    public string ErrorText
    {
      get
      {
        return this.errorText;
      }
    }

    public ExScenSelectionErrorEventArgs(string errorText)
    {
      this.errorText = errorText;
    }
  }
}
