﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenFileHandler
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;

namespace SAP.Copernicus.Extension.ProcessExtensionScenario
{
  public class ExtScenFileHandler
  {
    private static ExtScenFileHandler instance;

    private ExtScenFileHandler()
    {
    }

    public static ExtScenFileHandler Instance
    {
      get
      {
        if (ExtScenFileHandler.instance == null)
          ExtScenFileHandler.instance = new ExtScenFileHandler();
        return ExtScenFileHandler.instance;
      }
    }

    public void OnFileSave(string filePath, string[] dependentFileNames, string content, ProjectProperties properties)
    {
    }

    public void OnFileDeletion(string filePath)
    {
      string fullPath = XRepMapper.GetInstance().GetXrepPathforLocalFile(filePath).Replace("/SRC", "/GEN");
      XRepHandler xrepHandler = new XRepHandler();
      try
      {
        xrepHandler.Delete(fullPath);
      }
      catch (Exception ex)
      {
        int num = (int) CopernicusMessageBox.Show("An exception occured. File " + fullPath + " could not be deleted. The exception was: " + ex.ToString());
      }
    }

    public void OnFileActivate(string filepath)
    {
    }
  }
}
