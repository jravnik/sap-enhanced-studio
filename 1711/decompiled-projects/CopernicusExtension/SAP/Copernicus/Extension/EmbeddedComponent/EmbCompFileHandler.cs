﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.EmbeddedComponent.EmbCompFileHandler
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Project;

namespace SAP.Copernicus.Extension.EmbeddedComponent
{
  public class EmbCompFileHandler
  {
    private static EmbCompFileHandler instance;

    private EmbCompFileHandler()
    {
    }

    public static EmbCompFileHandler Instance
    {
      get
      {
        if (EmbCompFileHandler.instance == null)
          EmbCompFileHandler.instance = new EmbCompFileHandler();
        return EmbCompFileHandler.instance;
      }
    }

    public void OnFileSave(string filePath, string[] dependentFileNames, string content, ProjectProperties properties)
    {
    }

    public void OnFileDeletion(string filePath)
    {
    }

    public void OnFileActivate(string filepath)
    {
    }
  }
}
