﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.LanguageService.XBOLanguageConfiguration
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.LanguageService
{
  internal static class XBOLanguageConfiguration
  {
    public const string Extension = ".xbo";
    public const string Name = "Business Object Ext.";
  }
}
