﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.LanguageService.XBODataType
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.LanguageService
{
  public struct XBODataType
  {
    public string Description;
    public string DisplayText;
    public int Glyph;
    public string Name;
    public string NameSpace;
    public string ProxyName;
    public string ExtFieldType;
    public FieldTypeCategory FieldTypeCategory;

    public XBODataType(string description, string displayText, int glyph, string name, string nameSpace, string proxyName, string extFieldType, FieldTypeCategory fieldTypeCategory)
    {
      this.Description = description;
      this.DisplayText = displayText;
      this.Glyph = glyph;
      this.Name = name;
      this.NameSpace = nameSpace;
      this.ProxyName = proxyName;
      this.ExtFieldType = extFieldType;
      this.FieldTypeCategory = fieldTypeCategory;
    }

    public XBODataType(string description, string displayText, int glyph, string name, string nameSpace, string proxyName, FieldTypeCategory fieldTypeCategory)
    {
      this.Description = description;
      this.DisplayText = displayText;
      this.Glyph = glyph;
      this.Name = name;
      this.NameSpace = nameSpace;
      this.ProxyName = proxyName;
      this.ExtFieldType = "";
      this.FieldTypeCategory = fieldTypeCategory;
    }
  }
}
