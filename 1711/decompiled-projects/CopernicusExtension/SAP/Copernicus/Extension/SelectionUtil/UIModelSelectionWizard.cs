﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.SelectionUtil.UIModelSelectionWizard
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using EnvDTE;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.TemplateWizard;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Wizard;
using SAP.Copernicus.Extension.UIDesignerHelper;
using SAP.Copernicus.uidesigner.integration.model;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.SelectionUtil
{
  public class UIModelSelectionWizard : CopernicusWizardSheet, IWizard
  {
    private readonly string projectNamespace;
    private readonly string BOName;

    public UIModelSelectionWizard()
    {
      this.InitializeComponent();
      this.projectNamespace = CopernicusProjectSystemUtil.getSelectedProject().RepositoryNamespace;
      this.BOName = CopernicusProjectSystemUtil.getSelectedNodeName();
      this.Pages.Add((object) new UIModelSelectionFirstPage(this));
      this.helpid = HELP_IDS.BDS_BOEXTENSION_CREATE;
    }

    public void FinishWizard(List<string> selectedFloorPlans)
    {
      int num1 = 0;
      foreach (string selectedFloorPlan in selectedFloorPlans)
      {
        if (num1 != 0)
        {
          while (!InteractionWithUIDesigner.IsUIDesignerLoadDone())
            System.Threading.Thread.Sleep(1);
        }
        UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(selectedFloorPlan, false);
        string xboName = CopernicusProjectSystemUtil.GetXBOName();
        int num2 = selectedFloorPlan.LastIndexOf("/");
        string str1 = selectedFloorPlan.Substring(num2 + 1).Replace("uicomponent", "xuicomponent");
        HierarchyNode parentNode = CopernicusProjectSystemUtil.getSelectedNode();
        if (!(parentNode is ProjectNode) && !(parentNode is FolderNode))
          parentNode = parentNode.Parent;
        string str2 = Path.Combine(Path.GetDirectoryName(parentNode.Url), str1);
        if (!new FileInfo(str2).Exists)
        {
          IDictionary<string, string> fileAttributesToBeSaved = (IDictionary<string, string>) new Dictionary<string, string>();
          fileAttributesToBeSaved.Add("XUIComponent", selectedFloorPlan);
          fileAttributesToBeSaved.Add("xboName", xboName);
          string fileContent = selectedFloorPlan;
          (CopernicusProjectSystemUtil.getSelectedProject().ProjectMgr as CopernicusProjectNode).AddFileNode(str1, fileContent, fileAttributesToBeSaved, parentNode, (Encoding) null, false);
          new XRepHandler().Activate(XRepMapper.GetInstance().GetXrepPathforLocalFile(str2), false, (string) null);
        }
        ++num1;
      }
    }

    void IWizard.BeforeOpeningFile(ProjectItem projectItem)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.ProjectFinishedGenerating(EnvDTE.Project project)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.ProjectItemFinishedGenerating(ProjectItem projectItem)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.RunFinished()
    {
      int num = (int) this.ShowDialog();
    }

    void IWizard.RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
    {
    }

    bool IWizard.ShouldAddProjectItem(string filePath)
    {
      return false;
    }

    private void InitializeComponent()
    {
      this.buttonPanel.SuspendLayout();
      this.SuspendLayout();
      this.pagePanel.Paint += new PaintEventHandler(this.pagePanel_Paint);
      this.AcceptButton = (IButtonControl) this.nextButton;
      this.ClientSize = new Size(384, 141);
      this.Name = nameof (UIModelSelectionWizard);
      this.Text = "Enhance Screen";
      this.buttonPanel.ResumeLayout(false);
      this.buttonPanel.PerformLayout();
      this.ResumeLayout(false);
    }

    private void pagePanel_Paint(object sender, PaintEventArgs e)
    {
    }
  }
}
