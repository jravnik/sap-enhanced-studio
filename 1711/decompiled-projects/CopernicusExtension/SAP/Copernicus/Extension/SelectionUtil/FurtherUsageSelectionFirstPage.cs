﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.SelectionUtil.FurtherUsageSelectionFirstPage
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Wizard;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.SelectionUtil
{
  internal class FurtherUsageSelectionFirstPage : InternalWizardPage
  {
    private string noFormExists = string.Empty;
    private KutFieldMetaInfo fieldInfo = new KutFieldMetaInfo();
    private readonly string projectNamespace;
    private readonly string projectName;
    private readonly string FileName;
    private readonly string solution;
    private Label lblError;
    private string BOName;
    private string errorText;
    private Label HeaderLine;
    private string NoNodes;
    private int AddCount;
    private GroupBox grpBoxExtFields;
    private FurtherUsageSelectionWizard parentWizard;
    private PDI_EXT_S_EXT_FORM_FIELDS[] FormFieldList;
    private string maxBusinessObjectName;
    private string maxBusinessObjectNamespace;
    private selTabs selectedTab;

    private void InitializeComponent()
    {
      this.grpBoxExtFields = new GroupBox();
      this.SuspendLayout();
      this.Banner.Size = new Size(587, 64);
      this.Banner.Subtitle = "Select an extension field to add to Further Usage";
      this.Banner.Title = "Enhance Further Usage";
      this.grpBoxExtFields.Location = new Point(22, 83);
      this.grpBoxExtFields.Name = "grpBoxExtFields";
      this.grpBoxExtFields.Size = new Size(521, 355);
      this.grpBoxExtFields.TabIndex = 6;
      this.grpBoxExtFields.TabStop = false;
      this.grpBoxExtFields.Text = "Extension Fields:";
      this.Controls.Add((Control) this.grpBoxExtFields);
      this.Name = nameof (FurtherUsageSelectionFirstPage);
      this.Size = new Size(587, 533);
      this.SetActive += new CancelEventHandler(this.FurtherUsageSelectionFirstPage_SetActive);
      this.WizardBack += new WizardPageEventHandler(this.FurtherUsageSelectionFirstPage_WizardBack);
      this.WizardFinish += new CancelEventHandler(this.FurtherUsageSelectionFirstPage_WizardFinish);
      this.Controls.SetChildIndex((Control) this.grpBoxExtFields, 0);
      this.Controls.SetChildIndex((Control) this.Banner, 0);
      this.ResumeLayout(false);
    }

    public FurtherUsageSelectionFirstPage(FurtherUsageSelectionWizard p1)
      : base(HELP_IDS.BDS_BOEXTENSION_CREATE)
    {
      this.projectNamespace = CopernicusProjectSystemUtil.getSelectedProject().RepositoryNamespace;
      this.FileName = CopernicusProjectSystemUtil.GetXRepPathforSelectedNode().Split(new string[1]
      {
        "SRC/"
      }, StringSplitOptions.None)[1];
      FurtherUsageSelectionFirstPage.GetXBOInfoAttribs(CopernicusProjectSystemUtil.GetXRepPathforSelectedNode(), out this.solution);
      this.projectName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
      this.selectedTab = p1.selTab;
      this.parentWizard = p1;
      this.InitializeComponent();
      this.Banner.Title = this.getBannerTitle();
      this.Banner.Subtitle = this.getBannerSubtitle();
      this.addRadioButtons();
    }

    private void addRadioButtons()
    {
      ExtensionHandler extensionHandler = new ExtensionHandler();
      Cursor.Current = Cursors.WaitCursor;
      extensionHandler.GetFormFields(this.FileName, this.projectName, this.projectNamespace, out this.FormFieldList, out this.errorText, out this.BOName, out this.NoNodes);
      this.AddCount = (int) Convert.ToInt16(this.NoNodes);
      RadioButton[] radioButtonArray = new RadioButton[this.FormFieldList.Length + this.AddCount];
      if (this.errorText != null)
      {
        this.lblError = new Label();
        this.lblError.Location = new Point(17, 30);
        this.lblError.Size = new Size(400, 200);
        this.lblError.Dock = DockStyle.Fill;
        this.grpBoxExtFields.Controls.Add((Control) this.lblError);
        if (this.selectedTab == selTabs.EntSearch)
          this.lblError.Text = string.Format(SAP.Copernicus.Extension.Resources.ESEnhance, (object) this.errorText.Replace("No form message types exist for ", string.Empty));
        else
          this.lblError.Text = string.Format(SAP.Copernicus.Extension.Resources.ReportEnhance, (object) this.errorText.Replace("No form message types exist for ", string.Empty));
        this.lblError.ForeColor = Color.Red;
        this.grpBoxExtFields.Size = new Size(521, 80);
        this.Size = new Size(587, 350);
      }
      else
      {
        string str = (string) null;
        int index = 0;
        int num = 0;
        bool flag1 = true;
        foreach (PDI_EXT_S_EXT_FORM_FIELDS formField in this.FormFieldList)
        {
          if (this.selectedTab == selTabs.Reports)
          {
            bool flag2 = false;
            foreach (PDI_EXT_S_EXT_FIELDS activatedField in ExtensionDataCache.GetInstance().getActivatedFields(formField.ESR_BO_NAMESPACE, formField.ESR_BO_NAME))
            {
              if (formField.ESR_BO_NODE_NAME == activatedField.NODE_ESR_NAME && formField.ESR_FIELD_NAME == activatedField.FIELD_ESR_NAME && activatedField.TRANSIENT == "X")
                flag2 = true;
            }
            if (flag2)
              continue;
          }
          if (str != formField.UI_TEXT_BO_NODE)
          {
            if (flag1)
              flag1 = false;
            else
              num += 2;
            this.HeaderLine = new Label();
            this.HeaderLine.Location = new Point(17, 30 + (index + num) * 25);
            this.HeaderLine.Size = new Size(270, 13);
            this.grpBoxExtFields.Controls.Add((Control) this.HeaderLine);
            this.HeaderLine.Text = formField.UI_TEXT_BO_NODE + ":";
            this.HeaderLine.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point, (byte) 0);
            str = formField.UI_TEXT_BO_NODE;
            radioButtonArray[index] = new RadioButton();
            radioButtonArray[index].Text = formField.ESR_FIELD_NAME;
            radioButtonArray[index].Location = new Point(17, 30 + (index + 1 + num) * 25);
            radioButtonArray[index].AutoCheck = true;
            if (index == 0)
            {
              radioButtonArray[index].Checked = true;
              this.fieldInfo.businessObjectName = formField.ESR_BO_NAME;
              this.fieldInfo.businessObjectNamespace = formField.ESR_BO_NAMESPACE;
              this.fieldInfo.businessObjectNodeName = formField.ESR_BO_NODE_NAME;
              this.fieldInfo.esrFieldName = formField.ESR_FIELD_NAME;
              this.fieldInfo.esrFieldNamespace = formField.ESR_FIELD_NAMESPACE;
              this.fieldInfo.esrFieldUiText = formField.UI_TEXT_FIELD_NAME;
              this.fieldInfo.businessObjectNodeUiText = formField.UI_TEXT_BO_NODE;
              this.fieldInfo.fieldHasReferenceBoNode = formField.FIELD_HAS_REFERENCE_BO_NODE;
              this.maxBusinessObjectName = formField.MAX_BO_ESR_NAME;
              this.maxBusinessObjectNamespace = formField.MAX_BO_ESR_NAMESPACE;
            }
            else
              radioButtonArray[index].Checked = false;
            radioButtonArray[index].CheckedChanged += new EventHandler(this.rBut_CheckedChanged);
            radioButtonArray[index].Size = new Size(304, 24);
            this.grpBoxExtFields.Controls.Add((Control) radioButtonArray[index]);
            ++index;
            this.grpBoxExtFields.Size = new Size(521, 50 + (index + 1 + num) * 25);
            this.Size = new Size(587, 200 + (index + 1 + this.AddCount + num) * 25);
          }
          else
          {
            radioButtonArray[index] = new RadioButton();
            radioButtonArray[index].Text = formField.ESR_FIELD_NAME;
            radioButtonArray[index].Location = new Point(17, 30 + (index + 1 + num) * 25);
            radioButtonArray[index].AutoCheck = true;
            if (index == 0)
            {
              radioButtonArray[index].Checked = true;
              this.fieldInfo.businessObjectName = formField.ESR_BO_NAME;
              this.fieldInfo.businessObjectNamespace = formField.ESR_BO_NAMESPACE;
              this.fieldInfo.businessObjectNodeName = formField.ESR_BO_NODE_NAME;
              this.fieldInfo.esrFieldName = formField.ESR_FIELD_NAME;
              this.fieldInfo.esrFieldNamespace = formField.ESR_FIELD_NAMESPACE;
              this.fieldInfo.esrFieldUiText = formField.UI_TEXT_FIELD_NAME;
              this.fieldInfo.businessObjectNodeUiText = formField.UI_TEXT_BO_NODE;
              this.fieldInfo.fieldHasReferenceBoNode = formField.FIELD_HAS_REFERENCE_BO_NODE;
              this.maxBusinessObjectName = formField.MAX_BO_ESR_NAME;
              this.maxBusinessObjectNamespace = formField.MAX_BO_ESR_NAMESPACE;
            }
            else
              radioButtonArray[index].Checked = false;
            radioButtonArray[index].CheckedChanged += new EventHandler(this.rBut_CheckedChanged);
            radioButtonArray[index].Size = new Size(304, 24);
            this.grpBoxExtFields.Controls.Add((Control) radioButtonArray[index]);
            ++index;
            this.grpBoxExtFields.Size = new Size(521, 50 + (index + 1 + num) * 25);
            this.Size = new Size(587, 200 + (index + 1 + num + this.AddCount) * 25);
          }
        }
      }
      Cursor.Current = Cursors.Default;
    }

    private void rBut_CheckedChanged(object sender, EventArgs e)
    {
      if (!(sender is RadioButton))
        return;
      RadioButton radioButton = sender as RadioButton;
      if (!radioButton.Checked)
        return;
      foreach (PDI_EXT_S_EXT_FORM_FIELDS formField in this.FormFieldList)
      {
        if (formField.ESR_FIELD_NAME == radioButton.Text)
        {
          this.fieldInfo.businessObjectName = formField.ESR_BO_NAME;
          this.fieldInfo.businessObjectNamespace = formField.ESR_BO_NAMESPACE;
          this.fieldInfo.businessObjectNodeName = formField.ESR_BO_NODE_NAME;
          this.fieldInfo.esrFieldName = formField.ESR_FIELD_NAME;
          this.fieldInfo.esrFieldNamespace = formField.ESR_FIELD_NAMESPACE;
          this.fieldInfo.esrFieldUiText = formField.UI_TEXT_FIELD_NAME;
          this.fieldInfo.businessObjectNodeUiText = formField.UI_TEXT_BO_NODE;
          this.fieldInfo.fieldHasReferenceBoNode = formField.FIELD_HAS_REFERENCE_BO_NODE;
          this.maxBusinessObjectName = formField.MAX_BO_ESR_NAME;
          this.maxBusinessObjectNamespace = formField.MAX_BO_ESR_NAMESPACE;
        }
      }
    }

    private void FurtherUsageSelectionFirstPage_SetActive(object sender, CancelEventArgs e)
    {
      this.SetWizardButtons(WizardPageType.FirstAndLast);
      if (this.errorText == null)
        return;
      this.EnableFinishButton(false);
    }

    private void FurtherUsageSelectionFirstPage_WizardBack(object sender, CopernicusWizardPageEventArgs e)
    {
    }

    private void FurtherUsageSelectionFirstPage_WizardFinish(object sender, CancelEventArgs e)
    {
      if (this.maxBusinessObjectName != null && !this.maxBusinessObjectName.Equals(string.Empty))
      {
        this.fieldInfo.businessObjectName = this.maxBusinessObjectName;
        this.fieldInfo.businessObjectNamespace = this.maxBusinessObjectNamespace;
      }
      this.parentWizard.FinishWizard(this.fieldInfo);
    }

    public static void GetXBOInfoAttribs(string xRepPath, out string Solution)
    {
      IDictionary<string, string> attribs = (IDictionary<string, string>) null;
      string content;
      new XRepHandler().Read(xRepPath, out content, out attribs);
      foreach (string key1 in attribs.Keys.Where<string>((Func<string, bool>) (n =>
      {
        if (!n.StartsWith("~"))
          return n.Contains("~");
        return false;
      })).ToList<string>())
      {
        string key2 = key1.Split('~')[0];
        attribs.Add(key2, attribs[key1]);
        attribs.Remove(key1);
      }
      if (attribs.TryGetValue("~TARGET_SOLUTION", out Solution) || attribs.TryGetValue("~SOLUTION", out Solution))
        return;
      Solution = string.Empty;
    }

    private string getBannerTitle()
    {
      return this.selectedTab != selTabs.EntSearch ? string.Format(SAP.Copernicus.Extension.Resources.FurtherUsageTitleReport) : string.Format(SAP.Copernicus.Extension.Resources.FurtherUsageTitleES);
    }

    private string getBannerSubtitle()
    {
      return this.selectedTab != selTabs.EntSearch ? string.Format(SAP.Copernicus.Extension.Resources.FurtherUsageSubTitleReport) : string.Format(SAP.Copernicus.Extension.Resources.FurtherUsageSubtitleES);
    }
  }
}
