﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.SelectionUtil.FormExtFieldsSelectionWizard
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Core.Wizard;
using SAP.CopernicusProjectView;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.SelectionUtil
{
    public class FormExtFieldsSelectionWizard : CopernicusWizardSheet, IWizard
    {
        private readonly string projectNamespace;
        private readonly string BOName;

        public FormExtFieldsSelectionWizard()
        {
            this.InitializeComponent();
            this.projectNamespace = CopernicusProjectSystemUtil.getSelectedProject().RepositoryNamespace;
            this.BOName = CopernicusProjectSystemUtil.getSelectedNodeName();
            this.Pages.Add((object)new FormExtFieldsSelectionFirstPage(this));
            this.helpid = HELP_IDS.BDS_BOEXTENSION_CREATE;
        }

        void IWizard.BeforeOpeningFile(ProjectItem projectItem)
        {
            Trace.WriteLine("Enter here");
        }

        void IWizard.ProjectFinishedGenerating(Project project)
        {
            Trace.WriteLine("Enter here");
        }

        void IWizard.ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
            Trace.WriteLine("Enter here");
        }

        void IWizard.RunFinished()
        {
            int num = (int)this.ShowDialog();
        }

        void IWizard.RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
        }

        bool IWizard.ShouldAddProjectItem(string filePath)
        {
            return false;
        }

        private void InitializeComponent()
        {
            this.buttonPanel.SuspendLayout();
            this.SuspendLayout();
            this.AcceptButton = (IButtonControl)this.nextButton;
            this.ClientSize = new Size(384, 141);
            this.Name = nameof(FormExtFieldsSelectionWizard);
            this.Text = "Enhance Form";
            this.buttonPanel.ResumeLayout(false);
            this.buttonPanel.PerformLayout();
            this.ResumeLayout(false);
        }

        public void FinishWizard(KutFieldMetaInfo fieldInfo)
        {
            ConnectionDataSet.SystemDataRow connectedSystem = Connection.getInstance().getConnectedSystem();
            int client = connectedSystem.Client;
            string str;
            if (client < 100)
            {
                str = "0" + (object)client;
                if (client < 10)
                    str = "0" + str;
            }
            else
                str = string.Concat((object)client);
            string language = connectedSystem.Language;

            string paramsHTML;
            string pathHTML;

            paramsHTML =
                "\"BusinessObjectName\":\"" + fieldInfo.businessObjectName + "\"," +
                "\"BusinessObjectNamespace\":\"" + fieldInfo.businessObjectNamespace + "\"," +
                "\"BusinessObjectNodeName\":\"" + fieldInfo.businessObjectNodeName + "\"," +
                "\"EsrFieldName\":\"" + fieldInfo.esrFieldName + "\"," +
                "\"EsrNamespace\":\"" + fieldInfo.esrFieldNamespace + "\"," +
                "\"EsrFieldNameUiText\":\"" + fieldInfo.esrFieldUiText + "\"," +
                "\"BONodeUIText\":\"" + fieldInfo.businessObjectNodeUiText + "\"," +
                "\"FieldHasReferenceBONode\":\"" + fieldInfo.fieldHasReferenceBoNode + "\"," +
                "\"ShowEntSearch\":\"\"," +
                "\"ShowReports\":\"\"," +
                "\"ShowForms\":\"X\"";

            pathHTML = "{\"inPort\":\"OpenExtFieldFurtherUsageByBONode\",\"target\":\"/SAP_BYD_UI_REUSE/ExtFurtherUsage/ExtensibilityFurtherUsageComponent.QA.uicomponent\",\"params\":{" + paramsHTML + "},\"lists\":{},\"wcContext\":{}}";
            pathHTML = Base64Encode(pathHTML);
            pathHTML = "/sap/public/ap/ui/repository/SAP_UI/HTMLOBERON5/client.html?app.component=/SAP_UI_CT/Main/root.uiccwoc&rootWindow=X&redirectUrl=/sap/public/byd/runtime#n=" + pathHTML;

            string url;

            if (!Connection.getInstance().getConnectedSystem().Host.ToUpper().Contains("CRM"))
            {
                url = WebUtil.BuildURL(Connection.getInstance().getConnectedSystem(), pathHTML);
            }
            else
            {
                url = WebUtil.BuildURL(Connection.getInstance().getConnectedSystem(), "/sap/public/ap/ui/repository/SAP_BYD_UI/Runtime/StartPage.html?" + ("sap-client=" + str) + "&sap-system-login-basic_auth=X" + ("&sap-language=" + language) + "&app.component=/SAP_BYD_UI_REUSE/ExtFurtherUsage/ExtensibilityFurtherUsageComponent.QA.uicomponent" + "&app.inport=OpenExtFieldFurtherUsageByBONode" + ("&param.BusinessObjectName=" + fieldInfo.businessObjectName) + ("&param.BusinessObjectNamespace=" + fieldInfo.businessObjectNamespace) + ("&param.BusinessObjectNodeName=" + fieldInfo.businessObjectNodeName) + ("&param.EsrFieldName=" + fieldInfo.esrFieldName) + ("&param.EsrNamespace=" + fieldInfo.esrFieldNamespace) + ("&param.EsrFieldNameUiText=" + fieldInfo.esrFieldUiText) + ("&param.BONodeUIText=" + fieldInfo.businessObjectNodeUiText) + "&param.ShowEntSearch=&param.ShowReports=&param.ShowForms=X");
            }

            Clipboard.SetText(url);

            if (!PropertyAccess.GeneralProps.XBOEnhancementNoBrowser)
            {
                ExternalApplication.LaunchExternalBrowser(url);
            }
        }

        private string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
