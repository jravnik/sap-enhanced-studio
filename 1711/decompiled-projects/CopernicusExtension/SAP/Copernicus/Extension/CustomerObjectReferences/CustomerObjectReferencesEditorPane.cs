﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.CustomerObjectReferences.CustomerObjectReferencesEditorPane
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.CustomEditor;
using System;

namespace SAP.Copernicus.Extension.CustomerObjectReferences
{
  public sealed class CustomerObjectReferencesEditorPane : CustomEditorPane<CustomerObjectReferencesEditorFactory, CustomerObjectReferencesEditorControl>
  {
    public CustomerObjectReferencesEditorPane()
    {
      this.UIControl.ContentChanged += new EventHandler(this.ViewContentChange);
    }

    protected override Guid GetPackageGuid()
    {
      return typeof (CopernicusExtensionPackage).GUID;
    }

    protected override string GetFileExtension()
    {
      return ".ref";
    }

    protected override void SaveFile(string fileName)
    {
      IVsFileChangeEx service = (IVsFileChangeEx) this.GetService(typeof (SVsFileChangeEx));
      service.IgnoreFile(0U, fileName, -1);
      this.UIControl.SaveFile(fileName);
      service.SyncFile(fileName);
      service.IgnoreFile(0U, fileName, 0);
    }

    protected override void LoadFile(string fileName)
    {
      this.UIControl.FileChange = (IVsFileChangeEx) this.GetService(typeof (SVsFileChangeEx));
      this.UIControl.LoadFile(fileName);
    }

    private void ViewContentChange(object sender, EventArgs e)
    {
      this.OnContentChanged();
    }

    protected override void OnCreate()
    {
      this.InitializeF1Help(HELP_IDS.BDS_EXTENSIBILITY_REFERENCES);
    }
  }
}
