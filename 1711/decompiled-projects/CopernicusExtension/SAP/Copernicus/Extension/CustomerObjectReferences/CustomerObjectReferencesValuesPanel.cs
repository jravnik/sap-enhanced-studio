﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.CustomerObjectReferences.CustomerObjectReferencesValuesPanel
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Shell;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.CopernicusProjectView;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.CustomerObjectReferences
{
  public class CustomerObjectReferencesValuesPanel : UserControl
  {
    private const string IS_SELECTED_COLUMN = "isSelected";
    private const string BO_NAME_COLUMN = "boName";
    private const string BO_NAMESPACE_COLUMN = "boNamespace";
    private const string BO_NODE_NAME_COLUMN = "boNodeName";
    private const string FIELD_NAME_COLUMN = "fieldName";
    private const string LABEL_COLUMN = "label";
    private const string FIELD_TYPE_COLUMN = "fieldType";
    private const string HAS_ERRORS_COLUMN = "hasErrors";
    private const string ERROR_MESSAGE_COLUMN = "errorMessage";
    private const string ABAP_TRUE = "X";
    private const string ABAP_FALSE = "";
    private __EXT__S_PDI_REF_UI_CUSTOM_FIELD[] backendModel;
    private DataTable uiModel;
    private string fileName;
    private GroupBox groupBoxBasics;
    private DataGridView tableView;
    private IContainer components;
    private DataGridViewCheckBoxColumn isSelected;
    private DataGridViewTextBoxColumn boName;
    private DataGridViewTextBoxColumn boNodeName;
    private DataGridViewTextBoxColumn fieldName;
    private DataGridViewTextBoxColumn label;
    private DataGridViewTextBoxColumn fieldType;

    public event ValuesChangedEventHandler ContentChanged;

    public CustomerObjectReferencesValuesPanel()
    {
      this.InitializeComponent();
    }

    public void PopulateTable(__EXT__S_PDI_REF_UI_CUSTOM_FIELD[] backendModel, string fileName)
    {
      this.fileName = fileName;
      this.backendModel = backendModel;
      this.createUiModel();
      this.convertBackendToUiModel();
      this.tableView.DataSource = (object) this.uiModel;
      this.tableView.CurrentCellDirtyStateChanged += new EventHandler(this.tableView_currentCellDirtyStateChanged);
      this.tableView.CellValueChanged += new DataGridViewCellEventHandler(this.tableView_CellValueChanged);
      this.tableView.Sorted += new EventHandler(this.tableView_Sorted);
      this.hideColumns(new string[3]
      {
        "boNamespace",
        "hasErrors",
        "errorMessage"
      });
      this.setBusinessObjectToolTip();
      this.handleDisplayOfErrors();
    }

    public __EXT__S_PDI_REF_UI_CUSTOM_FIELD[] GetBackendModel()
    {
      this.convertUiToBackendModel();
      return this.backendModel;
    }

    private void createUiModel()
    {
      this.uiModel = new DataTable();
      this.uiModel.CaseSensitive = true;
      DataColumn[] dataColumnArray = new DataColumn[5];
      this.uiModel.Columns.Add(new DataColumn("isSelected", Type.GetType("System.Boolean"))
      {
        ReadOnly = false
      });
      DataColumn column1 = new DataColumn("boName", Type.GetType("System.String"));
      column1.ReadOnly = true;
      this.uiModel.Columns.Add(column1);
      dataColumnArray[0] = column1;
      DataColumn column2 = new DataColumn("boNamespace", Type.GetType("System.String"));
      column2.ReadOnly = true;
      this.uiModel.Columns.Add(column2);
      dataColumnArray[1] = column2;
      DataColumn column3 = new DataColumn("boNodeName", Type.GetType("System.String"));
      column3.ReadOnly = true;
      this.uiModel.Columns.Add(column3);
      dataColumnArray[2] = column3;
      DataColumn column4 = new DataColumn("fieldName", Type.GetType("System.String"));
      column4.ReadOnly = true;
      this.uiModel.Columns.Add(column4);
      dataColumnArray[3] = column4;
      this.uiModel.Columns.Add(new DataColumn("label", Type.GetType("System.String"))
      {
        ReadOnly = true
      });
      DataColumn column5 = new DataColumn("fieldType", Type.GetType("System.String"));
      column5.ReadOnly = true;
      this.uiModel.Columns.Add(column5);
      dataColumnArray[4] = column5;
      this.uiModel.Columns.Add(new DataColumn("hasErrors", Type.GetType("System.Boolean"))
      {
        ReadOnly = true
      });
      this.uiModel.Columns.Add(new DataColumn("errorMessage", Type.GetType("System.String"))
      {
        ReadOnly = true
      });
      this.uiModel.PrimaryKey = dataColumnArray;
    }

    private void convertBackendToUiModel()
    {
      if (this.backendModel == null)
        return;
      foreach (__EXT__S_PDI_REF_UI_CUSTOM_FIELD refUiCustomField in this.backendModel)
      {
        if (!(refUiCustomField.ERROR_OCCURRED == "X") || !(refUiCustomField.SELECTED == ""))
        {
          DataRow row = this.uiModel.NewRow();
          row["isSelected"] = (object) (refUiCustomField.SELECTED == "X");
          row["boName"] = (object) refUiCustomField.BO_NAME;
          row["boNodeName"] = (object) refUiCustomField.BO_NODE_NAME;
          row["fieldName"] = (object) refUiCustomField.FIELD_NAME;
          row["label"] = (object) refUiCustomField.FIELD_LABEL;
          row["fieldType"] = (object) refUiCustomField.FIELD_TYPE_DESCRIPTION;
          row["boNamespace"] = (object) refUiCustomField.BO_NAMESPACE;
          row["hasErrors"] = (object) (refUiCustomField.ERROR_OCCURRED == "X");
          row["errorMessage"] = (object) refUiCustomField.MESSAGE_TEXT;
          this.uiModel.Rows.Add(row);
        }
      }
    }

    private void convertUiToBackendModel()
    {
      if (this.backendModel == null)
        return;
      foreach (__EXT__S_PDI_REF_UI_CUSTOM_FIELD refUiCustomField in this.backendModel)
      {
        if (this.uiModel.Rows != null)
        {
          DataRow dataRow = this.uiModel.Rows.Find(this.buildKey((object) refUiCustomField.BO_NAME, (object) refUiCustomField.BO_NAMESPACE, (object) refUiCustomField.BO_NODE_NAME, (object) refUiCustomField.FIELD_NAME, (object) refUiCustomField.FIELD_TYPE_DESCRIPTION));
          if (dataRow != null)
            refUiCustomField.SELECTED = !(bool) dataRow["isSelected"] ? "" : "X";
        }
      }
    }

    private void setBusinessObjectToolTip()
    {
      foreach (DataGridViewRow row in (IEnumerable) this.tableView.Rows)
      {
        if (this.uiModel.Rows != null)
        {
          DataRow dataRow = this.uiModel.Rows.Find(this.buildKey(row.Cells["boName"].Value, row.Cells["boNamespace"].Value, row.Cells["boNodeName"].Value, row.Cells["fieldName"].Value, row.Cells["fieldType"].Value));
          if (dataRow != null)
            row.Cells["boName"].ToolTipText = (string) dataRow["boNamespace"];
        }
      }
    }

    private void hideColumns(string[] columns)
    {
      foreach (string column in columns)
      {
        if (this.tableView.Columns.Contains(column))
          this.tableView.Columns[column].Visible = false;
      }
    }

    private object[] buildKey(object boName, object boNamespace, object boNodeName, object fieldName, object fieldType)
    {
      return new object[5]{ boName, boNamespace, boNodeName, fieldName, fieldType };
    }

    private void tableView_currentCellDirtyStateChanged(object sender, EventArgs e)
    {
      if (!this.tableView.IsCurrentCellDirty)
        return;
      this.tableView.CommitEdit(DataGridViewDataErrorContexts.Commit);
    }

    private void tableView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
    {
      if (e.ColumnIndex == 0)
        this.ContentChanged(sender, (EventArgs) e);
      this.handleDisplayOfErrors();
    }

    private void tableView_Sorted(object sender, EventArgs e)
    {
      this.handleDisplayOfErrors();
    }

    private void handleDisplayOfErrors()
    {
      CopernicusFileConsistancyErrorListProvider errorListProvider = (CopernicusFileConsistancyErrorListProvider) null;
      CopernicusProjectViewPackage instance = CopernicusProjectViewPackage.instance;
      if (instance != null)
      {
        errorListProvider = instance.ErrorListProvider;
        errorListProvider.ClearErrorTaskItems(this.fileName);
      }
      foreach (DataGridViewRow row in (IEnumerable) this.tableView.Rows)
      {
        if ((bool) row.Cells["hasErrors"].Value)
        {
          if ((bool) row.Cells["isSelected"].Value)
          {
            if (errorListProvider != null)
              errorListProvider.CreateErrorTaskItem(this.fileName, (string) row.Cells["errorMessage"].Value, TaskErrorCategory.Error, 0, row.Index);
            row.Cells["isSelected"].ErrorText = (string) row.Cells["errorMessage"].Value;
          }
          else
          {
            row.Cells["isSelected"].ReadOnly = true;
            row.Cells["isSelected"].ToolTipText = (string) row.Cells["errorMessage"].Value;
            row.Cells["isSelected"].ErrorText = "";
            row.DefaultCellStyle.BackColor = Color.WhiteSmoke;
            row.DefaultCellStyle.SelectionBackColor = Color.WhiteSmoke;
            (row.Cells["isSelected"] as DataGridViewCheckBoxCell).FlatStyle = FlatStyle.Flat;
          }
        }
      }
      if (errorListProvider == null)
        return;
      errorListProvider.ResumeRefresh();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.groupBoxBasics = new GroupBox();
      this.tableView = new DataGridView();
      this.isSelected = new DataGridViewCheckBoxColumn();
      this.boName = new DataGridViewTextBoxColumn();
      this.boNodeName = new DataGridViewTextBoxColumn();
      this.fieldName = new DataGridViewTextBoxColumn();
      this.label = new DataGridViewTextBoxColumn();
      this.fieldType = new DataGridViewTextBoxColumn();
      this.groupBoxBasics.SuspendLayout();
      ((ISupportInitialize) this.tableView).BeginInit();
      this.SuspendLayout();
      this.groupBoxBasics.AutoSize = true;
      this.groupBoxBasics.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.groupBoxBasics.Controls.Add((Control) this.tableView);
      this.groupBoxBasics.Dock = DockStyle.Fill;
      this.groupBoxBasics.Location = new Point(0, 0);
      this.groupBoxBasics.Name = "groupBoxBasics";
      this.groupBoxBasics.Size = new Size(1264, 467);
      this.groupBoxBasics.TabIndex = 1;
      this.groupBoxBasics.TabStop = false;
      this.tableView.AllowUserToAddRows = false;
      this.tableView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
      this.tableView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
      this.tableView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.tableView.Columns.AddRange((DataGridViewColumn) this.isSelected, (DataGridViewColumn) this.boName, (DataGridViewColumn) this.boNodeName, (DataGridViewColumn) this.fieldName, (DataGridViewColumn) this.label, (DataGridViewColumn) this.fieldType);
      this.tableView.Dock = DockStyle.Fill;
      this.tableView.EditMode = DataGridViewEditMode.EditOnEnter;
      this.tableView.Location = new Point(3, 16);
      this.tableView.MultiSelect = false;
      this.tableView.Name = "tableView";
      this.tableView.RowHeadersVisible = false;
      this.tableView.ShowEditingIcon = false;
      this.tableView.ShowRowErrors = false;
      this.tableView.Size = new Size(1258, 448);
      this.tableView.TabIndex = 0;
      this.isSelected.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
      this.isSelected.DataPropertyName = "isSelected";
      this.isSelected.FillWeight = 43.14721f;
      this.isSelected.HeaderText = "Used";
      this.isSelected.Name = "isSelected";
      this.isSelected.SortMode = DataGridViewColumnSortMode.Automatic;
      this.isSelected.Width = 57;
      this.boName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.boName.DataPropertyName = "boName";
      this.boName.FillWeight = 100f;
      this.boName.HeaderText = "Business Object";
      this.boName.MaxInputLength = 120;
      this.boName.Name = "boName";
      this.boName.ReadOnly = true;
      this.boNodeName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.boNodeName.DataPropertyName = "boNodeName";
      this.boNodeName.FillWeight = 40f;
      this.boNodeName.HeaderText = "Node";
      this.boNodeName.MaxInputLength = 120;
      this.boNodeName.Name = "boNodeName";
      this.boNodeName.ReadOnly = true;
      this.fieldName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.fieldName.DataPropertyName = "fieldName";
      this.fieldName.FillWeight = 200f;
      this.fieldName.HeaderText = "Field Name";
      this.fieldName.MaxInputLength = 120;
      this.fieldName.Name = "fieldName";
      this.fieldName.ReadOnly = true;
      this.label.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.label.DataPropertyName = "label";
      this.label.FillWeight = 160f;
      this.label.HeaderText = "Label";
      this.label.MaxInputLength = 120;
      this.label.Name = "label";
      this.label.ReadOnly = true;
      this.fieldType.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.fieldType.DataPropertyName = "fieldType";
      this.fieldType.FillWeight = 40f;
      this.fieldType.HeaderText = "Type";
      this.fieldType.MaxInputLength = 120;
      this.fieldType.Name = "fieldType";
      this.fieldType.ReadOnly = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.groupBoxBasics);
      this.Name = nameof (CustomerObjectReferencesValuesPanel);
      this.Size = new Size(1264, 467);
      this.groupBoxBasics.ResumeLayout(false);
      ((ISupportInitialize) this.tableView).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
