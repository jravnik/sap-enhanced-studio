﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.CustomerObjectReferences.CustomerObjectReferencesEditorControl
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Util;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.CustomerObjectReferences
{
  public class CustomerObjectReferencesEditorControl : UserControl
  {
    private bool isDirty;
    private bool isLoaded;
    private IVsFileChangeEx fileChange;
    private CustomerObjectReferencesValuesPanel customerObjectReferencesValuesPanel;
    private GroupBox textGroup;
    private TextBox description;
    private ComponentResourceManager resources;
    private IContainer components;

    public CustomerObjectReferencesEditorControl()
    {
      this.InitializeComponent();
    }

    public IVsFileChangeEx FileChange
    {
      set
      {
        this.fileChange = value;
      }
      get
      {
        return this.fileChange;
      }
    }

    public void LoadFile(string fileName)
    {
      string content;
      IDictionary<string, string> attribs;
      new XRepHandler().Read(XRepMapper.GetInstance().GetXrepPathforLocalFile(fileName), out content, out attribs);
      if (content != null)
      {
        __EXT__S_PDI_REF_UI_CUSTOM_FIELD[] backendModel = new CustomerObjectReferencesHandler().ConvertXML2Model(content);
        if (backendModel != null)
          this.customerObjectReferencesValuesPanel.PopulateTable(backendModel, fileName);
      }
      this.customerObjectReferencesValuesPanel.ContentChanged += new ValuesChangedEventHandler(this.raiseContentChanged);
      this.isLoaded = true;
    }

    public void SaveFile(string fileName)
    {
      string str = new CustomerObjectReferencesHandler().ConvertModel2XML(this.customerObjectReferencesValuesPanel.GetBackendModel());
      try
      {
        XRepMapper instance = XRepMapper.GetInstance();
        string pathforLocalFile = instance.GetXrepPathforLocalFile(fileName);
        IDictionary<string, string> attributesForFile = this.getAttributesForFile(fileName);
        instance.SaveLocalFileToXRep(pathforLocalFile, str, attributesForFile, false, (string) null, false);
      }
      catch (ProtocolException ex)
      {
        int num = (int) CopernicusMessageBox.Show(this.resources.GetString("backendSaveError"));
        return;
      }
      if (LocalFileHandler.CreateFileOnDisk(str, fileName, true) == null)
      {
        int num1 = (int) CopernicusMessageBox.Show(this.resources.GetString("localSaveError"));
      }
      else
        this.isDirty = false;
    }

    private void onSizeChanged(object sender, EventArgs e)
    {
      this.textGroup.Size = new Size(this.Size.Width - 10, 60);
      this.customerObjectReferencesValuesPanel.Size = new Size(this.Size.Width - 10, this.Size.Height - this.textGroup.Size.Height - 20);
    }

    private IDictionary<string, string> getAttributesForFile(string fileName)
    {
      string content;
      IDictionary<string, string> attribs;
      new XRepHandler().Read(XRepMapper.GetInstance().GetXrepPathforLocalFile(fileName), false, out content, out attribs);
      return attribs;
    }

    public event EventHandler ContentChanged;

    private void raiseContentChanged(object sender, EventArgs e)
    {
      if (this.isDirty || !this.isLoaded)
        return;
      this.isDirty = true;
      if (this.ContentChanged == null)
        return;
      this.ContentChanged(sender, e);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.resources = new ComponentResourceManager(typeof (CustomerObjectReferencesEditorControl));
      this.customerObjectReferencesValuesPanel = new CustomerObjectReferencesValuesPanel();
      this.textGroup = new GroupBox();
      this.description = new TextBox();
      this.textGroup.SuspendLayout();
      this.SuspendLayout();
      this.customerObjectReferencesValuesPanel.Dock = DockStyle.Bottom;
      this.customerObjectReferencesValuesPanel.Location = new Point(0, 0);
      this.customerObjectReferencesValuesPanel.Name = "customerObjectReferencesValuesPanel";
      this.customerObjectReferencesValuesPanel.Padding = new Padding(5, 0, 5, 5);
      this.customerObjectReferencesValuesPanel.Size = new Size(208, 467);
      this.customerObjectReferencesValuesPanel.TabIndex = 0;
      this.textGroup.Controls.Add((Control) this.description);
      this.textGroup.Location = new Point(5, 10);
      this.textGroup.Name = "textGroup";
      this.textGroup.Size = new Size(200, 100);
      this.textGroup.TabIndex = 1;
      this.textGroup.TabStop = false;
      this.textGroup.Text = this.resources.GetString("textGroup.Text");
      this.description.BorderStyle = BorderStyle.None;
      this.description.Dock = DockStyle.Fill;
      this.description.Margin = new Padding(10);
      this.description.Multiline = true;
      this.description.ScrollBars = ScrollBars.Vertical;
      this.description.Name = "description";
      this.description.ReadOnly = true;
      this.description.Size = new Size(194, 81);
      this.description.TabIndex = 0;
      this.description.Text = this.resources.GetString("description.Text");
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.Controls.Add((Control) this.textGroup);
      this.Controls.Add((Control) this.customerObjectReferencesValuesPanel);
      this.Name = nameof (CustomerObjectReferencesEditorControl);
      this.Size = new Size(208, 467);
      this.SizeChanged += new EventHandler(this.onSizeChanged);
      this.textGroup.ResumeLayout(false);
      this.textGroup.PerformLayout();
      this.ResumeLayout(false);
    }
  }
}
