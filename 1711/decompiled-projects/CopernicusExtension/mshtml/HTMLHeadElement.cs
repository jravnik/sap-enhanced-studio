﻿// Decompiled with JetBrains decompiler
// Type: mshtml.HTMLHeadElement
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace mshtml
{
  [CompilerGenerated]
  [CoClass(typeof (object))]
  [Guid("3050F561-98B5-11CF-BB82-00AA00BDCE0B")]
  [TypeIdentifier]
  [ComImport]
  public interface HTMLHeadElement : DispHTMLHeadElement, HTMLElementEvents_Event
  {
  }
}
