﻿// Decompiled with JetBrains decompiler
// Type: mshtml.IHTMLScriptElement
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace mshtml
{
  [TypeIdentifier]
  [Guid("3050F28B-98B5-11CF-BB82-00AA00BDCE0B")]
  [CompilerGenerated]
  [ComImport]
  public interface IHTMLScriptElement
  {
    [SpecialName]
    void _VtblGap1_6();

    string text { [DispId(1006)] [param: MarshalAs(UnmanagedType.BStr), In] set; [DispId(1006)] [return: MarshalAs(UnmanagedType.BStr)] get; }

    [SpecialName]
    void _VtblGap2_5();

    string type { [DispId(1009)] [param: MarshalAs(UnmanagedType.BStr), In] set; [DispId(1009)] [return: MarshalAs(UnmanagedType.BStr)] get; }
  }
}
