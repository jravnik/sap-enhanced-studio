﻿// Decompiled with JetBrains decompiler
// Type: mshtml.IHTMLFormElement
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace mshtml
{
  [CompilerGenerated]
  [Guid("3050F1F7-98B5-11CF-BB82-00AA00BDCE0B")]
  [TypeIdentifier]
  [ComImport]
  public interface IHTMLFormElement : IEnumerable
  {
    [SpecialName]
    void _VtblGap1_17();

    [DispId(1009)]
    void submit();
  }
}
