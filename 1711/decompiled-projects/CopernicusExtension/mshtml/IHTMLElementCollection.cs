﻿// Decompiled with JetBrains decompiler
// Type: mshtml.IHTMLElementCollection
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace mshtml
{
  [Guid("3050F21F-98B5-11CF-BB82-00AA00BDCE0B")]
  [TypeIdentifier]
  [CompilerGenerated]
  [ComImport]
  public interface IHTMLElementCollection : IEnumerable
  {
    [SpecialName]
    void _VtblGap1_4();

    [DispId(0)]
    [return: MarshalAs(UnmanagedType.IDispatch)]
    object item([MarshalAs(UnmanagedType.Struct), In, Optional] object name, [MarshalAs(UnmanagedType.Struct), In, Optional] object index);
  }
}
