﻿// Decompiled with JetBrains decompiler
// Type: mshtml.IHTMLDOMNode
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace mshtml
{
  [CompilerGenerated]
  [Guid("3050F5DA-98B5-11CF-BB82-00AA00BDCE0B")]
  [TypeIdentifier]
  [ComImport]
  public interface IHTMLDOMNode
  {
    [SpecialName]
    void _VtblGap1_12();

    [DispId(-2147417039)]
    [return: MarshalAs(UnmanagedType.Interface)]
    IHTMLDOMNode appendChild([MarshalAs(UnmanagedType.Interface), In] IHTMLDOMNode newChild);
  }
}
