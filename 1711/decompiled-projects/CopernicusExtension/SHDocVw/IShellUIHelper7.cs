﻿// Decompiled with JetBrains decompiler
// Type: SHDocVw.IShellUIHelper7
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SHDocVw
{
  [TypeIdentifier]
  [Guid("66DEBCF2-05B0-4F07-B49B-B96241A65DB2")]
  [CompilerGenerated]
  [ComImport]
  public interface IShellUIHelper7 : IShellUIHelper6, IShellUIHelper5, IShellUIHelper4, IShellUIHelper3, IShellUIHelper2, IShellUIHelper
  {
  }
}
