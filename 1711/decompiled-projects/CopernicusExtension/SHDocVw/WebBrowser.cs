﻿// Decompiled with JetBrains decompiler
// Type: SHDocVw.WebBrowser
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SHDocVw
{
  [CoClass(typeof (object))]
  [Guid("D30C1661-CDAF-11D0-8A3E-00C04FC9E26E")]
  [CompilerGenerated]
  [TypeIdentifier]
  [ComImport]
  public interface WebBrowser : IWebBrowser2, IWebBrowserApp, IWebBrowser, DWebBrowserEvents2_Event
  {
  }
}
