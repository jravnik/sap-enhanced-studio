﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Formatter.TextBuffer
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System.Text;

namespace SAP.Copernicus.ABAPScriptLanguage.Formatter
{
  public class TextBuffer
  {
    private StringBuilder text = new StringBuilder();

    public void AddText(string Text)
    {
      this.text.Append(Text);
    }

    public void Clear()
    {
      this.text.Clear();
    }

    public string GetText()
    {
      return this.text.ToString();
    }
  }
}
