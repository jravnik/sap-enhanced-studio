﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Formatter.SourceFormatter
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Antlr.Runtime;
using Antlr.Runtime.Tree;
using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.ABAPScriptLanguage.IntelliSense;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAP.Copernicus.ABAPScriptLanguage.Formatter
{
  public class SourceFormatter
  {
    public bool InsertTabs { get; private set; }

    public int IndentSize { get; private set; }

    public bool DoNotDeleteMultipleSpaces { get; private set; }

    public int TabSize { get; private set; }

    public void Format(string Code, List<EditSpan> Mgr, TextSpan Span)
    {
      this.FormatPrivate(Code, Span, new LanguagePreferences()
      {
        InsertTabs = false,
        IndentSize = 4,
        TabSize = 4
      }).PopulateList(Mgr);
    }

    public void Format(string Code, EditArray Mgr, TextSpan Span, LanguagePreferences Preferences)
    {
      this.FormatPrivate(Code, Span, Preferences).PopulateEditArray(Mgr);
    }

    private EditManager FormatPrivate(string Code, TextSpan Span, LanguagePreferences Preferences)
    {
      this.InsertTabs = Preferences.InsertTabs;
      this.IndentSize = Preferences.IndentSize;
      this.TabSize = Preferences.TabSize;
      this.DoNotDeleteMultipleSpaces = false;
      ByDScriptingLanguageLexer lexer = new ByDScriptingLanguageLexer((ICharStream) new ANTLRStringStream(Code));
      ByDScriptingLanguageParser dscriptingLanguageParser = new ByDScriptingLanguageParser(lexer, new CodeCompletionContext());
      AstParserRuleReturnScope<CommonTree, IToken> parserRuleReturnScope = dscriptingLanguageParser.program();
      EditManager Edits = new EditManager(this);
      if (dscriptingLanguageParser.NumberOfSyntaxErrors == 0)
      {
        TokenReader Reader = new TokenReader(this, lexer);
        int StartIndex;
        int EndIndex;
        this.GetTokenBorderIndices(Reader, parserRuleReturnScope.Tree, this.GetLexerTextSpan(Code, Span), out StartIndex, out EndIndex);
        CodeContextInfo CodeContext = new CodeContextInfo(this, StartIndex, EndIndex);
        this.FormatSpacesAndComments(Reader, Edits, CodeContext, false, true, false);
        if (parserRuleReturnScope.Tree != null)
        {
          if (parserRuleReturnScope.Tree.IsNil)
          {
            for (int i = 0; i < parserRuleReturnScope.Tree.ChildCount; ++i)
            {
              this.FormatTree(Reader, Edits, (CommonTree) parserRuleReturnScope.Tree.GetChild(i), CodeContext, (StatementContextInfo) null, false);
              if (i < parserRuleReturnScope.Tree.ChildCount - 1)
                this.FormatSpacesAndComments(Reader, Edits, CodeContext, false, true, true);
            }
          }
          else
          {
            this.FormatSpacesAndComments(Reader, Edits, CodeContext, false, true, false);
            this.FormatTree(Reader, Edits, parserRuleReturnScope.Tree, CodeContext, (StatementContextInfo) null, false);
          }
          this.FormatSpacesAndComments(Reader, Edits, CodeContext, false, true, false);
        }
      }
      return Edits;
    }

    private LexerTextSpan GetLexerTextSpan(string Code, TextSpan Span)
    {
      string[] strArray = Code.Split(new string[1]
      {
        Environment.NewLine
      }, StringSplitOptions.None);
      LexerTextSpan lexerTextSpan = new LexerTextSpan();
      int index;
      for (index = 0; index < strArray.Length; ++index)
      {
        if (index < Span.iStartLine)
          lexerTextSpan.StartIndex += strArray[index].Length;
        else if (index == Span.iStartLine)
        {
          lexerTextSpan.StartIndex += Span.iStartIndex;
          break;
        }
        lexerTextSpan.StartIndex += Environment.NewLine.Length;
      }
      lexerTextSpan.EndIndex = lexerTextSpan.StartIndex;
      for (; index < strArray.Length; ++index)
      {
        if (index < Span.iEndLine)
          lexerTextSpan.EndIndex += strArray[index].Length;
        else if (index == Span.iEndLine)
        {
          lexerTextSpan.EndIndex += Span.iEndIndex;
          break;
        }
        lexerTextSpan.EndIndex += Environment.NewLine.Length;
      }
      return lexerTextSpan;
    }

    private void GetTokenBorderIndices(TokenReader Reader, CommonTree RootTree, LexerTextSpan TextSpan, out int StartIndex, out int EndIndex)
    {
      StartIndex = 0;
      while (Reader.Token.Type != -1)
        Reader.ReadNextToken();
      EndIndex = Reader.TokenIndex;
      Reader.Reset();
    }

    private void FormatTree(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext, bool InsideStatement)
    {
      CodeContext.AddStatementStack();
      CodeContext.CheckForIndentInitialization(Reader);
      switch (Tree.Type)
      {
        case 88:
        case 100:
        case 42:
        case 43:
        case 55:
          this.FormatIForELSEIForELSECLAUSEorSWITCHorWHILE(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 93:
        case 40:
        case 26:
          this.FormatCOLLECTIONOForELEMENTSOForTYPEOF(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 95:
          this.FormatUNARY(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 97:
          this.FormatVALUES(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 98:
          this.FormatVAR(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 72:
          this.FormatNONE(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 74:
          this.FormatNSPACE(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 77:
        case 4:
          this.FormatPATHorABSID(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 78:
          this.FormatQUALID(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 79:
        case 83:
          this.FormatRETURNorRAISE(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 34:
          this.FormatDEFAULTCLAUSE(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 46:
          this.FormatEXEC(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 49:
          this.FormatFOREACHorFOREACHDECL(Reader, Edits, Tree, CodeContext, StatementContext, false);
          break;
        case 50:
          this.FormatFOREACHorFOREACHDECL(Reader, Edits, Tree, CodeContext, StatementContext, true);
          break;
        case 54:
          this.FormatIF(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 57:
          this.FormatIMPORT(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 59:
        case 14:
          this.FormatBINARYorLAMBDA(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 20:
          this.FormatCALL(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 24:
          this.FormatCASECLAUSE(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 31:
          this.FormatSingleNameStatement(Reader, Edits, Tree, CodeContext, StatementContext, "continue");
          break;
        case 5:
          this.FormatABSLID(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 11:
          this.FormatASSIGNMENT(Reader, Edits, Tree, CodeContext, StatementContext);
          break;
        case 16:
          this.FormatSingleNameStatement(Reader, Edits, Tree, CodeContext, StatementContext, "break");
          break;
        default:
          throw new SourceFormatterCriticalException("Unknown tree type \"" + (object) Tree.Type + "\"");
      }
      CodeContext.RemoveStatementStack();
    }

    private void FormatIMPORT(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      string[] strArray1;
      if (Tree.ChildCount <= 2)
        strArray1 = new string[4]{ "$0", "#ws", "$1", ";" };
      else
        strArray1 = new string[8]
        {
          "$0",
          "#ws",
          "$1",
          "#ws",
          "as",
          "#ws",
          "$2",
          ";"
        };
      string[] strArray2 = strArray1;
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) strArray2);
    }

    private void FormatVAR(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      bool flag = Tree.ChildCount > 3;
      string[] strArray;
      if (Tree.GetChild(2).Type == 72)
      {
        if (flag)
          strArray = new string[8]
          {
            "$0",
            "#ws",
            "$1",
            "#ws",
            "=",
            "#ws",
            "$3",
            ";"
          };
        else
          strArray = new string[4]{ "$0", "#ws", "$1", ";" };
      }
      else if (flag)
        strArray = new string[10]
        {
          "$0",
          "#ws",
          "$1",
          "#ws",
          ":",
          "#ws",
          "$2",
          "=",
          "$3",
          ";"
        };
      else
        strArray = new string[8]
        {
          "$0",
          "#ws",
          "$1",
          "#ws",
          ":",
          "#ws",
          "$2",
          ";"
        };
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) strArray);
    }

    private void FormatEXEC(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) new string[2]
      {
        "$0",
        ";"
      });
    }

    private void FormatIF(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      List<string> stringList = new List<string>();
      for (int index = 1; index < Tree.ChildCount; ++index)
      {
        stringList.Add("$" + (object) index);
        stringList.Add("#nl");
      }
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, true, (IEnumerable<string>) stringList);
    }

    private void FormatASSIGNMENT(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) new string[6]
      {
        "$0",
        "#ws",
        "=",
        "#ws",
        "$1",
        ";"
      });
    }

    private void FormatIForELSEIForELSECLAUSEorSWITCHorWHILE(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      string[] strArray = (string[]) null;
      bool flag = false;
      switch (Tree.Type)
      {
        case 88:
          strArray = new string[1]{ "switch" };
          flag = true;
          break;
        case 100:
          strArray = new string[1]{ "while" };
          flag = true;
          break;
        case 42:
          strArray = new string[1]{ "else" };
          flag = false;
          StatementContext = (StatementContextInfo) null;
          break;
        case 43:
          strArray = new string[3]{ "else", "#ws", "if" };
          flag = true;
          StatementContext = (StatementContextInfo) null;
          break;
        case 55:
          strArray = new string[1]{ "if" };
          flag = true;
          break;
      }
      List<string> stringList = new List<string>((IEnumerable<string>) strArray);
      if (flag)
        stringList.AddRange((IEnumerable<string>) new string[4]
        {
          "#ws",
          "(",
          "$1",
          ")"
        });
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) stringList);
      this.FormatBlock(Reader, Edits, Tree, CodeContext);
    }

    private void FormatCASECLAUSE(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) new string[3]
      {
        "$0",
        "#ws",
        "$1"
      });
      this.FormatBlock(Reader, Edits, Tree, CodeContext);
    }

    private void FormatVALUES(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      List<string> Format = new List<string>();
      this.BuildCommaSeparatedList(0, Tree.ChildCount - 1, Format);
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) Format);
    }

    private void FormatDEFAULTCLAUSE(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) new string[1]
      {
        "$0"
      });
      this.FormatBlock(Reader, Edits, Tree, CodeContext);
    }

    private void FormatFOREACHorFOREACHDECL(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext, bool HasDeclaration)
    {
      List<string> stringList = new List<string>((IEnumerable<string>) new string[3]
      {
        "$0",
        "#ws",
        "("
      });
      if (HasDeclaration)
        stringList.AddRange((IEnumerable<string>) new string[2]
        {
          "var",
          "#ws"
        });
      stringList.AddRange((IEnumerable<string>) new string[6]
      {
        "$1",
        "#ws",
        "in",
        "#ws",
        "$2",
        ")"
      });
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) stringList);
      this.FormatBlock(Reader, Edits, Tree, CodeContext);
    }

    private void FormatRETURNorRAISE(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      List<string> stringList = new List<string>((IEnumerable<string>) new string[1]
      {
        "$0"
      });
      if (Tree.ChildCount > 1)
        stringList.AddRange((IEnumerable<string>) new string[2]
        {
          "#ws",
          "$1"
        });
      stringList.Add(";");
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) stringList);
    }

    private void FormatCALL(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      List<string> Format = new List<string>((IEnumerable<string>) new string[2]
      {
        "$2",
        "$0"
      });
      this.BuildCommaSeparatedList(3, Tree.ChildCount - 1, Format);
      Format.Add("$1");
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, true, (IEnumerable<string>) Format);
    }

    private void FormatNSPACE(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      List<string> stringList = new List<string>();
      for (int index = 1; index < Tree.ChildCount; ++index)
      {
        stringList.Add("$" + (object) index);
        if (index < Tree.ChildCount - 1)
          stringList.Add(".");
      }
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, true, (IEnumerable<string>) stringList);
    }

    private void FormatPATHorABSID(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, true, (IEnumerable<string>) new string[3]
      {
        "$1",
        "$0",
        "$2"
      });
    }

    private void FormatBINARYorLAMBDA(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, true, (IEnumerable<string>) new string[5]
      {
        "$1",
        "#ws",
        "$0",
        "#ws",
        "$2"
      });
    }

    private void FormatUNARY(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, true, (IEnumerable<string>) new string[2]
      {
        "$0",
        "$1"
      });
    }

    private void FormatCOLLECTIONOForELEMENTSOForTYPEOF(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, true, (IEnumerable<string>) new string[3]
      {
        "$0",
        "#ws",
        "$1"
      });
    }

    private void FormatQUALID(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, true, (IEnumerable<string>) new string[3]
      {
        "$0",
        "::",
        "$1"
      });
    }

    private void FormatABSLID(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, true, (IEnumerable<string>) new string[1]
      {
        Tree.Text
      });
    }

    private void FormatNONE(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, true, (IEnumerable<string>) new string[1]
      {
        ";"
      });
    }

    private void FormatSingleNameStatement(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext, string Name)
    {
      this.FormatFinal(Reader, Edits, Tree, CodeContext, StatementContext, false, (IEnumerable<string>) new string[2]
      {
        Name,
        ";"
      });
    }

    private void FormatFinal(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext, StatementContextInfo StatementContext, bool DontIndentFirstLine, IEnumerable<string> Format)
    {
      bool flag = false;
      if (StatementContext == null)
      {
        StatementContext = new StatementContextInfo();
        flag = true;
      }
      using (IEnumerator<string> enumerator = Format.GetEnumerator())
      {
label_36:
        while (enumerator.MoveNext())
        {
          string str = enumerator.Current;
          if (str.StartsWith("$"))
          {
            int i = int.Parse(str.Substring(1));
            CommonTree child = (CommonTree) Tree.GetChild(i);
            if (child == null)
              throw new SourceFormatterCriticalException("Child \"" + (object) i + "\" could not be found in tree \"" + Tree.Text + "\"");
            if (child.ChildCount == 0)
            {
              str = child.Text;
            }
            else
            {
              this.FormatTree(Reader, Edits, child, CodeContext, StatementContext, true);
              str = (string) null;
            }
          }
          if (str != null && str.StartsWith("#"))
          {
            List<IToken> tokenList = new List<IToken>();
            switch (str.Substring(1))
            {
              case "ws":
                this.FormatSpacesAndComments(Reader, Edits, CodeContext, true, true, true);
                break;
              case "nl":
                this.FormatSpacesAndComments(Reader, Edits, CodeContext, true, true, false);
                this.EnsureNewLineAtCurrentToken(Reader, Edits, CodeContext);
                break;
              case null:
                return;
              default:
                return;
            }
          }
          else if (str != null)
          {
            this.FormatSpacesAndComments(Reader, Edits, CodeContext, true, true, false);
            if (Reader.Token.Text != str)
            {
              while (Reader.Token.Type == 64)
              {
                StatementContext.OpeningBraceFound();
                Reader.ReadNextToken();
                this.FormatSpacesAndComments(Reader, Edits, CodeContext, true, true, false);
              }
              if (Reader.Token.Text != str)
                throw new SourceFormatterCriticalException("Unexpected token \"" + Reader.Token.Text + "\", expected \"" + str + "\" (Ln " + (object) Reader.CurrentLine + ", Ch " + (object) Reader.CurrentIndex + ")");
            }
            if (str == "(")
              StatementContext.AddBraceMark();
            else if (str == ")")
              StatementContext.RemoveBraceMark();
            if (Reader.IsNewLine)
              DontIndentFirstLine = false;
            if (!DontIndentFirstLine)
            {
              if (!StatementContext.IndentOffsetSpecified)
                StatementContext.IndentOffset = Edits.CorrectIndentSpacesIfNewLine(Reader, CodeContext.Indent);
              else
                Edits.CorrectIndentSpacesIfNewLine(Reader, Math.Max(Reader.SpaceCountSinceLastToken + StatementContext.IndentOffset, 0));
            }
            Reader.ReadNextToken();
            while (true)
            {
              if (StatementContext.NextClosingBraceIsUnexpected() && Reader.Token.Type == 84)
              {
                StatementContext.ClosingBraceFound();
                this.FormatSpacesAndComments(Reader, Edits, CodeContext, true, true, false);
                Reader.ReadNextToken();
              }
              else
                goto label_36;
            }
          }
        }
      }
      while (StatementContext.NextClosingBraceIsUnexpected())
      {
        this.FormatSpacesAndComments(Reader, Edits, CodeContext, true, true, false);
        if (Reader.Token.Type == 84)
        {
          Reader.ReadNextToken();
          StatementContext.ClosingBraceFound();
        }
        else
          break;
      }
      if (!flag)
        return;
      StatementContext.Close();
    }

    private int GetBlockStartChildIndex(CommonTree Tree)
    {
      switch (Tree.Type)
      {
        case 88:
        case 100:
        case 55:
        case 24:
        case 43:
          return 2;
        case 49:
        case 50:
          return 3;
        case 34:
        case 42:
          return 1;
        default:
          return -1;
      }
    }

    private void FormatBlock(TokenReader Reader, EditManager Edits, CommonTree Tree, CodeContextInfo CodeContext)
    {
      bool flag1 = Reader.TokenIndex >= CodeContext.StartTokenIndex && Reader.TokenIndex <= CodeContext.EndTokenIndex;
      int count = Edits.Count;
      this.FormatSpacesAndComments(Reader, Edits, CodeContext, false, true, false);
      int blockStartChildIndex = this.GetBlockStartChildIndex(Tree);
      if (Reader.Token.Type == 62)
      {
        if (flag1)
          Edits.CorrectIndentSpacesIfNewLine(Reader, CodeContext.Indent);
        bool isNewLine = Reader.IsNewLine;
        TextSpan Span1 = new TextSpan();
        if (!isNewLine && flag1)
        {
          Span1.iStartLine = Span1.iEndLine = Reader.CurrentLine;
          Span1.iStartIndex = Reader.CurrentIndex - Reader.CharCountSinceLastToken;
          Span1.iEndIndex = Reader.CurrentIndex;
        }
        Reader.ReadNextToken();
        CodeContext.IncreaseIndent();
        int currentLine1 = Reader.CurrentLine;
        this.FormatSpacesAndComments(Reader, Edits, CodeContext, false, false, false);
        bool flag2 = Reader.CurrentLine > currentLine1;
        TextSpan Span2 = new TextSpan();
        if (!flag2 && flag1)
        {
          Span2.iStartLine = Span2.iEndLine = Reader.CurrentLine;
          Span2.iStartIndex = Reader.CurrentIndex - Reader.CharCountSinceLastToken;
          Span2.iEndIndex = Reader.CurrentIndex;
        }
        int indent = CodeContext.Indent;
        int currentLine2 = Reader.CurrentLine;
        if (Tree.ChildCount > blockStartChildIndex && (Tree.Children[blockStartChildIndex].Type != 72 || Tree.ChildCount > blockStartChildIndex + 1))
        {
          for (int index = blockStartChildIndex; index < Tree.ChildCount; ++index)
          {
            this.FormatTree(Reader, Edits, (CommonTree) Tree.Children[index], CodeContext, (StatementContextInfo) null, true);
            this.FormatSpacesAndComments(Reader, Edits, CodeContext, false, true, true);
          }
        }
        else
          this.FormatSpacesAndComments(Reader, Edits, CodeContext, false, true, false);
        CodeContext.DecreaseIndent();
        if (Reader.Token.Type != 82)
          throw new SourceFormatterCriticalException("Closing curly brace expected, \"" + Reader.Token.Text + "\" found");
        if (flag1)
        {
          Edits.CorrectIndentSpacesIfNewLine(Reader, CodeContext.Indent);
          bool flag3 = false;
          if (flag2 || Reader.CurrentLine > currentLine2)
          {
            if (!isNewLine)
            {
              flag3 = true;
              Edits.Replace(Span1, Environment.NewLine + this.GetIndentString(CodeContext.Indent));
            }
            if (!flag2)
              Edits.Replace(Span2, Environment.NewLine + this.GetIndentString(indent));
            if (this.EnsureNewLineAtCurrentToken(Reader, Edits, CodeContext))
              Edits.InsertAtCurrentPosition(Reader, this.GetIndentString(CodeContext.Indent));
          }
          if (!isNewLine && !flag3)
          {
            Edits.Replace(Span1, " ");
            Edits.Replace(Span2, " ");
          }
        }
        Reader.ReadNextToken();
      }
      else
      {
        if (flag1)
          Edits.IncreaseIndentInEdits(count, Edits.Count - count);
        CodeContext.IncreaseIndent();
        this.FormatSpacesAndComments(Reader, Edits, CodeContext, false, true, true);
        this.FormatTree(Reader, Edits, (CommonTree) Tree.Children[blockStartChildIndex], CodeContext, (StatementContextInfo) null, false);
        CodeContext.DecreaseIndent();
      }
    }

    private void FormatSpacesAndComments(TokenReader Reader, EditManager Edits, CodeContextInfo CodeContext, bool NoNewLineAfterMultilineComment, bool DeleteEndingSpaces, bool EnsureWhitespace)
    {
      int num1 = 0;
      TextBuffer textBuffer = Reader.CreateTextBuffer();
      while (Reader.Token.Type == 85 || Reader.Token.Type == 67 || Reader.Token.Type == 101)
      {
        if (Reader.TokenIndex < CodeContext.StartTokenIndex || Reader.TokenIndex > CodeContext.EndTokenIndex)
        {
          Reader.ReadNextToken();
        }
        else
        {
          CodeContext.CheckForIndentInitialization(Reader);
          switch (Reader.Token.Type)
          {
            case 67:
              string[] strArray = Reader.Token.Text.Split(new string[1]
              {
                Environment.NewLine
              }, StringSplitOptions.None);
              bool isNewLine = Reader.IsNewLine;
              bool flag1 = !NoNewLineAfterMultilineComment && (Reader.IsNewLine || strArray.Length != 1);
              if (isNewLine)
              {
                int num2 = CodeContext.Indent - Reader.SpaceCountSinceLastToken;
                if (num2 != 0)
                {
                  Edits.CorrectIndentSpacesInSpan(Reader.CurrentLine, 0, Reader.CurrentIndex, Reader.SpaceCountSinceLastToken, CodeContext.Indent);
                  for (int index = 1; index < strArray.Length; ++index)
                  {
                    int EndIndex = 0;
                    int Indent = num2;
                    for (; EndIndex < strArray[index].Length; ++EndIndex)
                    {
                      if ((int) strArray[index][EndIndex] == 32)
                        ++Indent;
                      else if ((int) strArray[index][EndIndex] == 9)
                        Indent += this.TabSize;
                      else
                        break;
                    }
                    if (Indent < 0)
                      Indent = 0;
                    Edits.CorrectIndentSpacesInSpan(Reader.CurrentLine + index, 0, EndIndex, Indent - num2, Indent);
                  }
                }
              }
              Reader.ReadNextToken();
              if (flag1)
              {
                this.EnsureNewLineAtCurrentToken(Reader, Edits, CodeContext);
                continue;
              }
              continue;
            case 85:
              Edits.CorrectIndentSpacesIfNewLine(Reader, CodeContext.Indent);
              Reader.ReadNextToken();
              continue;
            case 101:
              bool flag2 = false;
              if (Reader.Token.Text == "\r")
              {
                num1 = -1;
                Reader.ReadNextToken();
                flag2 = true;
              }
              if (Reader.Token.Text == "\n")
              {
                flag2 = false;
                if (Reader.SpaceCountSinceLastToken > 0)
                {
                  int num2 = Reader.CurrentIndex + num1;
                  TextSpan Span = new TextSpan();
                  Span.iStartLine = Span.iEndLine = Reader.CurrentLine;
                  Span.iStartIndex = num2 - Reader.CharCountSinceLastToken;
                  Span.iEndIndex = num2;
                  Edits.Delete(Span);
                  num1 = 0;
                }
              }
              if (!flag2)
              {
                Reader.ReadNextToken();
                continue;
              }
              continue;
            default:
              continue;
          }
        }
      }
      if (Reader.TokenIndex <= CodeContext.StartTokenIndex || Reader.TokenIndex > CodeContext.EndTokenIndex)
        return;
      Reader.RemoveTextBuffer(textBuffer);
      string source1 = textBuffer.GetText();
      if (!this.DoNotDeleteMultipleSpaces && DeleteEndingSpaces && !Reader.IsNewLine)
      {
        int num2 = Math.Min(Reader.CharCountSinceLastToken, source1.Length);
        string source2 = source1.Substring(0, source1.Length - num2);
        int num3 = Reader.CurrentIndex - num2;
        if (num2 > 0 && EnsureWhitespace && (int) source1.Last<char>() == 32 && (string.IsNullOrEmpty(source2) || (int) source2.Last<char>() != 10))
        {
          --num2;
          EnsureWhitespace = false;
        }
        if (num2 > 0)
        {
          TextSpan Span = new TextSpan();
          Span.iStartLine = Span.iEndLine = Reader.CurrentLine;
          Span.iStartIndex = num3;
          Span.iEndIndex = num3 + num2;
          Edits.Delete(Span);
          Reader.ResetCountsSinceLastToken();
          source1 = source2;
        }
      }
      if (!EnsureWhitespace)
        return;
      string String = "";
      if (!Reader.IsNewLine && (string.IsNullOrEmpty(source1) || (int) source1.Last<char>() != 32 && (int) source1.Last<char>() != 10))
        String = " ";
      Edits.InsertAtCurrentPosition(Reader, String);
    }

    private void BuildCommaSeparatedList(int From, int To, List<string> Format)
    {
      for (int index = From; index <= To; ++index)
      {
        Format.Add("$" + (object) index);
        if (index < To)
        {
          Format.Add(",");
          Format.Add("#ws");
        }
      }
    }

    private bool EnsureNewLineAtCurrentToken(TokenReader Reader, EditManager Edits, CodeContextInfo CodeContext)
    {
      if (Reader.IsNewLine || Reader.TokenIndex < CodeContext.StartTokenIndex)
        return false;
      bool flag = false;
      int num1 = Reader.CurrentIndex - Reader.CharCountSinceLastToken;
      int num2 = 0;
      while (!flag && Reader.Token.Type == 101)
      {
        if (Reader.Token.Text == "\n")
          flag = true;
        else if (Reader.Token.Text != "\r")
          ++num2;
        Reader.ReadNextToken();
      }
      if (flag)
      {
        TextSpan Span = new TextSpan();
        Span.iStartLine = Span.iEndLine = Reader.CurrentLine;
        Span.iStartIndex = num1;
        Span.iEndIndex = num1 + num2;
        Edits.Delete(Span);
      }
      else
      {
        TextSpan Span = new TextSpan();
        Span.iStartLine = Span.iEndLine = Reader.CurrentLine;
        Span.iStartIndex = num1;
        Span.iEndIndex = Reader.CurrentIndex;
        Edits.Replace(Span, Environment.NewLine);
        Reader.SetNewLine();
      }
      return !flag;
    }

    public string GetIndentString(int Indent)
    {
      StringBuilder stringBuilder = new StringBuilder();
      if (this.InsertTabs)
      {
        int count = Math.DivRem(Indent, this.TabSize, out Indent);
        stringBuilder.Append(new string('\t', count));
      }
      if (Indent > 0)
        stringBuilder.Append(new string(' ', Indent));
      return stringBuilder.ToString();
    }

    public int IncreaseGivenIndent(int Indent)
    {
      return Indent + (this.InsertTabs ? this.TabSize : this.IndentSize);
    }

    public int DecreaseGivenIndent(int Indent)
    {
      return Math.Max(Indent - (this.InsertTabs ? this.TabSize : this.IndentSize), 0);
    }
  }
}
