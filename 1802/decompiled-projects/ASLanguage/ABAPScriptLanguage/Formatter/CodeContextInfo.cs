﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Formatter.CodeContextInfo
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

namespace SAP.Copernicus.ABAPScriptLanguage.Formatter
{
  public class CodeContextInfo
  {
    public const int MaxStatementStacks = 200;
    private SourceFormatter source;

    public int StatementStacks { get; private set; }

    public int Indent { get; set; }

    public int StartTokenIndex { get; private set; }

    public int EndTokenIndex { get; private set; }

    public CodeContextInfo(SourceFormatter Source, int StartTokenIndex, int EndTokenIndex)
    {
      this.Indent = -1;
      this.source = Source;
      this.StartTokenIndex = StartTokenIndex;
      this.EndTokenIndex = EndTokenIndex;
    }

    public void AddStatementStack()
    {
      ++this.StatementStacks;
      if (this.StatementStacks > 200)
        throw new SourceFormatterStatementStackException("The statement stack size has passed the maximum limit");
    }

    public void RemoveStatementStack()
    {
      --this.StatementStacks;
    }

    public void IncreaseIndent()
    {
      if (this.Indent < 0)
        return;
      this.Indent = this.source.IncreaseGivenIndent(this.Indent);
    }

    public void DecreaseIndent()
    {
      if (this.Indent < 0)
        return;
      this.Indent = this.source.DecreaseGivenIndent(this.Indent);
    }

    public void CheckForIndentInitialization(TokenReader Reader)
    {
      if (this.Indent >= 0 || !Reader.IsNewLine || Reader.TokenIndex != this.StartTokenIndex)
        return;
      this.Indent = this.StartTokenIndex == 0 ? 0 : Reader.SpaceCountSinceLastToken;
    }
  }
}
