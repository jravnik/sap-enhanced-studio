﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Formatter.TokenReader
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.ABAPScriptLanguage.Formatter
{
  public class TokenReader
  {
    private List<TextBuffer> buffers = new List<TextBuffer>();
    private SourceFormatter source;
    private ByDScriptingLanguageLexer lexer;

    public IToken Token { get; private set; }

    public int TokenIndex { get; private set; }

    public int SpaceCountSinceLastToken { get; private set; }

    public int CharCountSinceLastToken { get; private set; }

    public int CurrentLine { get; private set; }

    public int CurrentIndex { get; private set; }

    public bool IsNewLine { get; private set; }

    public TokenReader(SourceFormatter Source, ByDScriptingLanguageLexer lexer)
    {
      this.source = Source;
      this.lexer = lexer;
      this.Reset();
    }

    public TextBuffer CreateTextBuffer()
    {
      TextBuffer textBuffer = new TextBuffer();
      this.buffers.Add(textBuffer);
      return textBuffer;
    }

    public void RemoveTextBuffer(TextBuffer Buffer)
    {
      this.buffers.Remove(Buffer);
    }

    public void SetNewLine()
    {
      this.ResetCountsSinceLastToken();
      this.IsNewLine = true;
    }

    public void ResetCountsSinceLastToken()
    {
      this.SpaceCountSinceLastToken = 0;
      this.CharCountSinceLastToken = 0;
    }

    public void Reset()
    {
      this.lexer.Reset();
      this.TokenIndex = -1;
      this.ResetCountsSinceLastToken();
      this.ReadNextToken();
      this.CurrentLine = 0;
      this.CurrentIndex = 0;
      this.IsNewLine = true;
    }

    public void ReadNextToken()
    {
      if (this.Token != null)
      {
        string source = this.Token.Text;
        if (source.Contains<char>('\n'))
        {
          string[] strArray = source.Split('\n');
          this.IsNewLine = true;
          this.SpaceCountSinceLastToken = 0;
          this.CharCountSinceLastToken = 0;
          this.CurrentIndex = 0;
          this.CurrentLine += strArray.Length - 1;
          source = ((IEnumerable<string>) strArray).Last<string>();
        }
        if (!string.IsNullOrEmpty(source))
        {
          int num1 = 0;
          int num2 = 0;
          bool flag = true;
          foreach (char ch in source.Reverse<char>())
          {
            if (flag)
            {
              if ((int) ch == 32)
              {
                ++num1;
                ++num2;
              }
              else if ((int) ch == 9)
              {
                num1 += this.source.TabSize;
                ++num2;
              }
              else if ((int) ch != 13)
                flag = false;
            }
            ++this.CurrentIndex;
          }
          if (num2 == source.Length)
          {
            this.SpaceCountSinceLastToken += num1;
            this.CharCountSinceLastToken += num2;
          }
          else
          {
            this.SpaceCountSinceLastToken = num1;
            this.CharCountSinceLastToken = num2;
            this.IsNewLine = false;
          }
        }
      }
      foreach (TextBuffer buffer in this.buffers)
        buffer.AddText(this.Token.Text);
      this.Token = this.lexer.NextToken();
      ++this.TokenIndex;
    }
  }
}
