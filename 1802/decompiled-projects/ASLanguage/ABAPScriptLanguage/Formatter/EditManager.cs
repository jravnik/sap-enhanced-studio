﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Formatter.EditManager
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.ABAPScriptLanguage.Formatter
{
  public class EditManager
  {
    private List<EditSpanInfo> edits = new List<EditSpanInfo>();
    private SourceFormatter source;

    public int Count
    {
      get
      {
        return this.edits.Count;
      }
    }

    public EditManager(SourceFormatter Source)
    {
      this.source = Source;
    }

    public int CorrectIndentSpacesIfNewLine(TokenReader Reader, int Indent)
    {
      if (!Reader.IsNewLine)
        return 0;
      int StartIndex = 0;
      int currentIndex = Reader.CurrentIndex;
      if (Reader.CurrentIndex != Reader.CharCountSinceLastToken)
        StartIndex = currentIndex;
      return this.CorrectIndentSpacesInSpan(Reader.CurrentLine, StartIndex, currentIndex, Reader.SpaceCountSinceLastToken, Indent);
    }

    public int CorrectIndentSpacesInSpan(int LineNumber, int StartIndex, int EndIndex, int EndIndexInSpaces, int Indent)
    {
      if (EndIndex == 0 && Indent == 0)
        return 0;
      TextSpan toReplace = new TextSpan();
      toReplace.iStartLine = toReplace.iEndLine = LineNumber;
      toReplace.iStartIndex = StartIndex;
      toReplace.iEndIndex = EndIndex;
      this.edits.Add(new EditSpanInfo(EditSpanType.Indent, new EditSpan(toReplace, this.source.GetIndentString(Indent)), Indent));
      return Indent - EndIndexInSpaces;
    }

    public void InsertAtCurrentPosition(TokenReader Reader, string String)
    {
      this.InsertAt(Reader.CurrentLine, Reader.CurrentIndex, String);
    }

    public void InsertAt(int LineNumber, int CharIndex, string String)
    {
      if (string.IsNullOrEmpty(String))
        return;
      TextSpan toReplace = new TextSpan();
      toReplace.iStartLine = toReplace.iEndLine = LineNumber;
      toReplace.iStartIndex = toReplace.iEndIndex = CharIndex;
      this.edits.Add(new EditSpanInfo(EditSpanType.Insert, new EditSpan(toReplace, String)));
    }

    public void Delete(TextSpan Span)
    {
      if (Span.iStartLine == Span.iEndLine && Span.iStartIndex == Span.iEndIndex)
        return;
      this.edits.Add(new EditSpanInfo(EditSpanType.Delete, new EditSpan(Span, "")));
    }

    public void Replace(TextSpan Span, string Replace)
    {
      this.edits.Add(new EditSpanInfo(EditSpanType.Replace, new EditSpan(Span, Replace)));
    }

    public void IncreaseIndentInEdits(int StartIndex, int Count)
    {
      for (int index = 0; index < Count; ++index)
      {
        if (this.edits[StartIndex + index].Type == EditSpanType.Indent)
          this.edits[StartIndex + index].EditSpan.Text = this.source.GetIndentString(this.source.IncreaseGivenIndent(this.edits[StartIndex + index].Amount));
      }
    }

    public void PopulateList(List<EditSpan> List)
    {
      List.AddRange(this.edits.Select<EditSpanInfo, EditSpan>((Func<EditSpanInfo, EditSpan>) (x => x.EditSpan)));
    }

    public void PopulateEditArray(EditArray List)
    {
      foreach (EditSpan editSpan in this.edits.Select<EditSpanInfo, EditSpan>((Func<EditSpanInfo, EditSpan>) (x => x.EditSpan)))
        List.Add(editSpan);
    }
  }
}
