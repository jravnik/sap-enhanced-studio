﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis.DumpAnalysisWindow
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis
{
  [Guid("028A8F59-057A-45DD-9B52-56ED53CC02B9")]
  public class DumpAnalysisWindow : CopernicusToolWindowPane
  {
    public DumpAnalysisWindow()
      : base(HELP_IDS.BDS_DUMPANALYSIS)
    {
      this.Caption = VSPackage.DumpAnalysisTitle;
      this.BitmapResourceID = 300;
      this.BitmapIndex = 0;
      this.Content = (object) new DumpAnalysisControl();
    }
  }
}
