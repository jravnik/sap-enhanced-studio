﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis.DumpAnalysisSelectionAdvancedDialog
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis
{
  public class DumpAnalysisSelectionAdvancedDialog : Form
  {
    private static AutoCompleteStringCollection userAutoComplete = new AutoCompleteStringCollection();
    private ErrorProvider userErrorProvider = new ErrorProvider();
    private ErrorProvider solutionErrorProvider = new ErrorProvider();
    private IContainer components;
    private Button SearchButton;
    private Button CancelButton;
    private DateTimePicker DateFromDateTimePicker;
    private DateTimePicker DateToDateTimePicker;
    private Label SolutionLabel;
    private Label UserLabel;
    private TextBox UserTextBox;
    private Label FromLabel;
    private Label ToLabel;
    private DateTimePicker TimeFromDateTimePicker;
    private DateTimePicker TimeToDateTimePicker;
    private ComboBox SolutionComboBox;
    private DateTime dateTimeFrom;
    private DateTime dateTimeTo;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.SearchButton = new Button();
      this.CancelButton = new Button();
      this.DateFromDateTimePicker = new DateTimePicker();
      this.DateToDateTimePicker = new DateTimePicker();
      this.SolutionLabel = new Label();
      this.UserLabel = new Label();
      this.UserTextBox = new TextBox();
      this.FromLabel = new Label();
      this.ToLabel = new Label();
      this.TimeFromDateTimePicker = new DateTimePicker();
      this.TimeToDateTimePicker = new DateTimePicker();
      this.SolutionComboBox = new ComboBox();
      this.SuspendLayout();
      this.SearchButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.SearchButton.DialogResult = DialogResult.OK;
      this.SearchButton.Location = new Point(138, 135);
      this.SearchButton.Name = "SearchButton";
      this.SearchButton.Size = new Size(75, 23);
      this.SearchButton.TabIndex = 7;
      this.SearchButton.Text = "Search";
      this.SearchButton.UseVisualStyleBackColor = true;
      this.SearchButton.Click += new EventHandler(this.SearchButton_Click);
      this.CancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.CancelButton.DialogResult = DialogResult.Cancel;
      this.CancelButton.Location = new Point(219, 135);
      this.CancelButton.Name = "CancelButton";
      this.CancelButton.Size = new Size(75, 23);
      this.CancelButton.TabIndex = 8;
      this.CancelButton.Text = "Cancel";
      this.CancelButton.UseVisualStyleBackColor = true;
      this.DateFromDateTimePicker.Format = DateTimePickerFormat.Short;
      this.DateFromDateTimePicker.Location = new Point(78, 7);
      this.DateFromDateTimePicker.Name = "DateFromDateTimePicker";
      this.DateFromDateTimePicker.ShowCheckBox = true;
      this.DateFromDateTimePicker.Size = new Size(113, 20);
      this.DateFromDateTimePicker.TabIndex = 1;
      this.DateFromDateTimePicker.ValueChanged += new EventHandler(this.DateFromDateTimePicker_ValueChanged);
      this.DateToDateTimePicker.Format = DateTimePickerFormat.Short;
      this.DateToDateTimePicker.Location = new Point(78, 33);
      this.DateToDateTimePicker.Name = "DateToDateTimePicker";
      this.DateToDateTimePicker.ShowCheckBox = true;
      this.DateToDateTimePicker.Size = new Size(113, 20);
      this.DateToDateTimePicker.TabIndex = 3;
      this.DateToDateTimePicker.ValueChanged += new EventHandler(this.DateToDateTimePicker_ValueChanged);
      this.SolutionLabel.AutoSize = true;
      this.SolutionLabel.Location = new Point(12, 91);
      this.SolutionLabel.Name = "SolutionLabel";
      this.SolutionLabel.Size = new Size(48, 13);
      this.SolutionLabel.TabIndex = 4;
      this.SolutionLabel.Text = "Solution:";
      this.UserLabel.AutoSize = true;
      this.UserLabel.Location = new Point(13, 62);
      this.UserLabel.Name = "UserLabel";
      this.UserLabel.Size = new Size(32, 13);
      this.UserLabel.TabIndex = 5;
      this.UserLabel.Text = "User:";
      this.UserTextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.UserTextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
      this.UserTextBox.CharacterCasing = CharacterCasing.Upper;
      this.UserTextBox.Location = new Point(78, 59);
      this.UserTextBox.MaxLength = 30;
      this.UserTextBox.Name = "UserTextBox";
      this.UserTextBox.Size = new Size(113, 20);
      this.UserTextBox.TabIndex = 5;
      this.UserTextBox.TextChanged += new EventHandler(this.UserTextBox_TextChanged);
      this.FromLabel.AutoSize = true;
      this.FromLabel.Location = new Point(12, 10);
      this.FromLabel.Name = "FromLabel";
      this.FromLabel.Size = new Size(33, 13);
      this.FromLabel.TabIndex = 8;
      this.FromLabel.Text = "From:";
      this.ToLabel.AutoSize = true;
      this.ToLabel.Location = new Point(12, 36);
      this.ToLabel.Name = "ToLabel";
      this.ToLabel.Size = new Size(23, 13);
      this.ToLabel.TabIndex = 9;
      this.ToLabel.Text = "To:";
      this.TimeFromDateTimePicker.CustomFormat = "HH:mm:ss";
      this.TimeFromDateTimePicker.Format = DateTimePickerFormat.Time;
      this.TimeFromDateTimePicker.Location = new Point(197, 6);
      this.TimeFromDateTimePicker.Name = "TimeFromDateTimePicker";
      this.TimeFromDateTimePicker.ShowCheckBox = true;
      this.TimeFromDateTimePicker.ShowUpDown = true;
      this.TimeFromDateTimePicker.Size = new Size(97, 20);
      this.TimeFromDateTimePicker.TabIndex = 2;
      this.TimeToDateTimePicker.Format = DateTimePickerFormat.Time;
      this.TimeToDateTimePicker.Location = new Point(197, 33);
      this.TimeToDateTimePicker.Name = "TimeToDateTimePicker";
      this.TimeToDateTimePicker.ShowCheckBox = true;
      this.TimeToDateTimePicker.ShowUpDown = true;
      this.TimeToDateTimePicker.Size = new Size(96, 20);
      this.TimeToDateTimePicker.TabIndex = 4;
      this.SolutionComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.SolutionComboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.SolutionComboBox.Cursor = Cursors.IBeam;
      this.SolutionComboBox.FormattingEnabled = true;
      this.SolutionComboBox.Location = new Point(78, 88);
      this.SolutionComboBox.MaxLength = 10;
      this.SolutionComboBox.Name = "SolutionComboBox";
      this.SolutionComboBox.Size = new Size(215, 21);
      this.SolutionComboBox.Sorted = true;
      this.SolutionComboBox.TabIndex = 6;
      this.SolutionComboBox.KeyDown += new KeyEventHandler(this.SolutionComboBox_KeyDown);
      this.AcceptButton = (IButtonControl) this.SearchButton;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(311, 170);
      this.Controls.Add((Control) this.SolutionComboBox);
      this.Controls.Add((Control) this.TimeToDateTimePicker);
      this.Controls.Add((Control) this.TimeFromDateTimePicker);
      this.Controls.Add((Control) this.ToLabel);
      this.Controls.Add((Control) this.FromLabel);
      this.Controls.Add((Control) this.UserTextBox);
      this.Controls.Add((Control) this.UserLabel);
      this.Controls.Add((Control) this.SolutionLabel);
      this.Controls.Add((Control) this.DateToDateTimePicker);
      this.Controls.Add((Control) this.DateFromDateTimePicker);
      this.Controls.Add((Control) this.CancelButton);
      this.Controls.Add((Control) this.SearchButton);
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.Name = nameof (DumpAnalysisSelectionAdvancedDialog);
      this.Text = "Advanced Selection";
      this.Load += new EventHandler(this.DumpAnalysisSelectionAdvancedDialog_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public DateTime DateTimeFrom
    {
      get
      {
        return this.dateTimeFrom;
      }
      set
      {
        this.dateTimeFrom = value;
        this.DateFromDateTimePicker.Value = this.TimeFromDateTimePicker.Value = this.dateTimeFrom;
      }
    }

    public bool DateFromChecked
    {
      get
      {
        return this.DateFromDateTimePicker.Checked;
      }
      set
      {
        this.DateFromDateTimePicker.Checked = value;
        this.TimeFromDateTimePicker.Enabled = value;
      }
    }

    public bool TimeFromChecked
    {
      get
      {
        return this.TimeFromDateTimePicker.Checked;
      }
      set
      {
        this.TimeFromDateTimePicker.Checked = value;
      }
    }

    public DateTime DateTimeTo
    {
      get
      {
        return this.dateTimeTo;
      }
      set
      {
        this.dateTimeTo = value;
        this.DateToDateTimePicker.Value = this.TimeToDateTimePicker.Value = this.dateTimeTo;
      }
    }

    public bool DateToChecked
    {
      get
      {
        return this.DateToDateTimePicker.Checked;
      }
      set
      {
        this.DateToDateTimePicker.Checked = value;
        this.TimeToDateTimePicker.Enabled = value;
      }
    }

    public bool TimeToChecked
    {
      get
      {
        return this.TimeToDateTimePicker.Checked;
      }
      set
      {
        this.TimeToDateTimePicker.Checked = value;
      }
    }

    public string User
    {
      get
      {
        return this.UserTextBox.Text;
      }
      set
      {
        this.UserTextBox.Text = value;
      }
    }

    public string SolutionPrefix
    {
      get
      {
        if (this.SolutionCaption != null && this.SolutionCaption.Length >= 10)
          return this.SolutionCaption.Substring(0, 10);
        return string.Empty;
      }
      set
      {
        this.SolutionCaption = this.GetCaptionForSolution(value);
      }
    }

    public string SolutionCaption
    {
      get
      {
        return this.SolutionComboBox.Text;
      }
      set
      {
        this.SolutionComboBox.Text = value;
      }
    }

    public DumpAnalysisSelectionAdvancedDialog()
    {
      this.InitializeComponent();
      this.DateFromDateTimePicker.Value = DateTime.Now.Date;
      this.DateToDateTimePicker.Value = DateTime.Now.Date;
      this.TimeFromDateTimePicker.Value = DateTime.Now.Date;
      this.TimeToDateTimePicker.Value = this.TimeFromDateTimePicker.Value.AddDays(1.0).AddSeconds(-1.0);
      this.userErrorProvider.SetIconAlignment((Control) this.UserTextBox, ErrorIconAlignment.MiddleRight);
      this.userErrorProvider.SetIconPadding((Control) this.UserTextBox, 2);
      this.userErrorProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;
      this.solutionErrorProvider.SetIconAlignment((Control) this.SolutionComboBox, ErrorIconAlignment.MiddleRight);
      this.solutionErrorProvider.SetIconPadding((Control) this.SolutionComboBox, 2);
      this.solutionErrorProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;
      this.initializeComboboxSolution();
    }

    private void initializeComboboxSolution()
    {
      foreach (RepositoryDataSet.SolutionsRow solutionsRow in (RepositoryDataSet.SolutionsRow[]) RepositoryDataCache.GetInstance().RepositoryDataSet.Tables["Solutions"].Select())
        this.SolutionComboBox.Items.Add((object) this.FormatSolutionCaption(solutionsRow.Name, solutionsRow.Description));
      this.SolutionComboBox.Items.Add((object) string.Empty);
    }

    private string GetCaptionForSolution(string solutionPrefix)
    {
      RepositoryDataSet.SolutionsRow solutionsRow = (RepositoryDataSet.SolutionsRow) ((IEnumerable<DataRow>) RepositoryDataCache.GetInstance().RepositoryDataSet.Tables["Solutions"].Select("Name='" + solutionPrefix + "'")).FirstOrDefault<DataRow>();
      if (solutionsRow == null)
        return solutionPrefix;
      return this.FormatSolutionCaption(solutionPrefix, solutionsRow.Description);
    }

    private string FormatSolutionCaption(string solutionPrefix, string solutionDescription)
    {
      return string.Format("{0} ({1})", (object) solutionPrefix, (object) solutionDescription);
    }

    private void SearchButton_Click(object sender, EventArgs e)
    {
      if (!this.UserTextBox.Text.Equals(string.Empty) && !Regex.IsMatch(this.UserTextBox.Text, "^[a-zA-Z0-9]+$"))
      {
        this.userErrorProvider.SetError((Control) this.UserTextBox, Resources.DumpAnalysisUserInvalidCharacters);
        this.DialogResult = DialogResult.None;
      }
      string solutionPrefix = this.SolutionPrefix;
      if (!solutionPrefix.Equals(string.Empty))
      {
        if (solutionPrefix.Length != 10)
        {
          this.solutionErrorProvider.SetError((Control) this.SolutionComboBox, Resources.DumpAnalysisSolutionLength);
          this.DialogResult = DialogResult.None;
        }
        else if (!Regex.IsMatch(solutionPrefix, "^[a-zA-Z0-9_]+$"))
        {
          this.solutionErrorProvider.SetError((Control) this.SolutionComboBox, Resources.DumpAnalysisSolutionInvalidCharacters);
          this.DialogResult = DialogResult.None;
        }
        else if (solutionPrefix.Substring(0, 1) != "Y" || solutionPrefix.Substring(9, 1) != "_")
        {
          this.solutionErrorProvider.SetError((Control) this.SolutionComboBox, Resources.DumpAnalysisSolutionStartEnd);
          this.DialogResult = DialogResult.None;
        }
      }
      this.dateTimeFrom = !this.DateFromDateTimePicker.Checked ? DateTimePicker.MinimumDateTime : this.DateFromDateTimePicker.Value.Date;
      this.dateTimeFrom = !this.TimeFromDateTimePicker.Checked ? this.dateTimeFrom.Date + DateTimePicker.MinimumDateTime.TimeOfDay : this.dateTimeFrom.Date + this.TimeFromDateTimePicker.Value.TimeOfDay;
      this.dateTimeTo = !this.DateToDateTimePicker.Checked ? DateTimePicker.MaximumDateTime : this.DateToDateTimePicker.Value.Date;
      this.dateTimeTo = !this.TimeToDateTimePicker.Checked ? this.dateTimeTo.Date.AddDays(1.0).AddSeconds(-1.0) : this.dateTimeTo.Date + this.TimeToDateTimePicker.Value.TimeOfDay;
      if (this.UserTextBox.Text.Equals(string.Empty))
        return;
      DumpAnalysisSelectionAdvancedDialog.userAutoComplete.Add(this.UserTextBox.Text);
    }

    private void SolutionTextBox_TextChanged(object sender, EventArgs e)
    {
      this.solutionErrorProvider.SetError((Control) this.SolutionComboBox, string.Empty);
    }

    private void UserTextBox_TextChanged(object sender, EventArgs e)
    {
      this.userErrorProvider.SetError((Control) this.UserTextBox, string.Empty);
    }

    private void DumpAnalysisSelectionAdvancedDialog_Load(object sender, EventArgs e)
    {
      this.UserTextBox.AutoCompleteCustomSource = DumpAnalysisSelectionAdvancedDialog.userAutoComplete;
    }

    private void DateFromDateTimePicker_ValueChanged(object sender, EventArgs e)
    {
      if (!this.DateFromDateTimePicker.Checked)
        this.TimeFromDateTimePicker.Enabled = false;
      else
        this.TimeFromDateTimePicker.Enabled = true;
    }

    private void DateToDateTimePicker_ValueChanged(object sender, EventArgs e)
    {
      if (!this.DateToDateTimePicker.Checked)
        this.TimeToDateTimePicker.Enabled = false;
      else
        this.TimeToDateTimePicker.Enabled = true;
    }

    private void SolutionComboBox_KeyDown(object sender, KeyEventArgs e)
    {
      this.SolutionComboBox.DroppedDown = false;
    }
  }
}
