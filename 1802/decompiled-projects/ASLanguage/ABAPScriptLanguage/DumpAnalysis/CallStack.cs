﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis.CallStack
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis
{
  public class CallStack
  {
    public string Path { get; private set; }

    public string Line { get; private set; }

    public string Column { get; private set; }

    public string Solution { get; private set; }

    public string Status { get; private set; }

    public string ToolTip { get; private set; }

    public string ToolTipPath { get; private set; }

    public CallStack(PDI_ABSL_S_STACKS callStack)
    {
      this.Path = callStack.PATH;
      this.Solution = callStack.SOLUTION;
      this.Status = callStack.STATUS;
      if (callStack.PATH.Length == 0)
      {
        this.Path = "n/a";
        this.ToolTipPath = Resources.DumpAnalysisTooltipNoExistOrActivation;
        this.Line = "n/a";
        this.Column = "n/a";
        this.ToolTip = Resources.DumpAnalysisTooltipPositionNotDetermined;
        this.Status = "unreachable";
      }
      else
      {
        if (callStack.STATUS.Equals("unreachable"))
        {
          this.Line = "n/a";
          this.Column = "n/a";
          this.ToolTip = Resources.DumpAnalysisTooltipPositionNotDetermined;
        }
        else
        {
          this.Line = callStack.LINE == 0 ? string.Empty : callStack.LINE.ToString();
          this.Column = callStack.COL == 0 ? string.Empty : callStack.COL.ToString();
          this.ToolTip = callStack.STATUS.Equals("latest") ? Resources.DumpAnalysisTooltipPositionUpToDate : Resources.DumpAnalysisTooltipPositionChanged;
        }
        this.ToolTipPath = callStack.PATH;
      }
    }
  }
}
