﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis.Dump
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis
{
  public class Dump
  {
    public DateTime Date { get; private set; }

    public string DateString { get; private set; }

    public string Time { get; private set; }

    public string User { get; private set; }

    public string Error { get; private set; }

    public string ErrorText { get; private set; }

    public string Solution { get; private set; }

    public string Path { get; private set; }

    public string Line { get; private set; }

    public string Column { get; private set; }

    public string Status { get; private set; }

    public string Transid { get; private set; }

    public string ToolTip { get; private set; }

    public string ToolTipPath { get; private set; }

    public Dump(PDI_ABSL_S_DUMP dump)
    {
      DateTime localTime = DateTime.Parse(dump.DATUM + (object) 'T' + dump.UZEIT + (object) 'Z').ToLocalTime();
      this.Date = localTime.Date;
      this.DateString = this.Date.ToShortDateString();
      this.Time = localTime.ToLongTimeString();
      this.User = dump.USERALIAS;
      this.Error = dump.ERRID;
      this.ErrorText = dump.ERRTEXT;
      this.Solution = dump.SOLUTION;
      this.Path = dump.PATH;
      this.Status = dump.STATUS;
      this.Transid = dump.TRANSID;
      if (dump.PATH.Length == 0)
      {
        this.Path = "n/a";
        this.ToolTipPath = Resources.DumpAnalysisTooltipNoExistOrActivation;
        this.Line = "n/a";
        this.Column = "n/a";
        this.ToolTip = Resources.DumpAnalysisTooltipPositionNotDetermined;
        this.Status = "unreachable";
      }
      else
      {
        if (dump.STATUS.Equals("unreachable"))
        {
          this.Line = "n/a";
          this.Column = "n/a";
          this.ToolTip = Resources.DumpAnalysisTooltipPositionNotDetermined;
        }
        else
        {
          this.Line = dump.LINE == 0 ? string.Empty : dump.LINE.ToString();
          this.Column = dump.COL == 0 ? string.Empty : dump.COL.ToString();
          this.ToolTip = dump.STATUS.Equals("latest") ? Resources.DumpAnalysisTooltipPositionUpToDate : Resources.DumpAnalysisTooltipPositionChanged;
        }
        this.ToolTipPath = dump.PATH;
      }
    }
  }
}
