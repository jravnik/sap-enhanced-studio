﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Classification.TextViewListener
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;
using System;
using System.ComponentModel.Composition;

namespace SAP.Copernicus.ABAPScriptLanguage.Classification
{
  [TextViewRole("DOCUMENT")]
  [Export(typeof (IWpfTextViewCreationListener))]
  [ContentType("ByD script")]
  internal class TextViewListener : IWpfTextViewCreationListener
  {
    public void TextViewCreated(IWpfTextView textView)
    {
      textView.Properties.GetOrCreateSingletonProperty<TextViewCloseRegistration>((Func<TextViewCloseRegistration>) (() => new TextViewCloseRegistration(textView)));
    }
  }
}
