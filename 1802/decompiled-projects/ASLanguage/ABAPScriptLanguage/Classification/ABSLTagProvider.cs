﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Classification.ABSLTagProvider
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;
using System.ComponentModel.Composition;

namespace SAP.Copernicus.ABAPScriptLanguage.Classification
{
  [TagType(typeof (ClassificationTag))]
  [Export(typeof (ITaggerProvider))]
  [ContentType("ByD script")]
  internal sealed class ABSLTagProvider : ITaggerProvider
  {
    [Import]
    internal IClassificationTypeRegistryService ClassificationTypeRegistry;

    public ITagger<T> CreateTagger<T>(ITextBuffer buffer) where T : ITag
    {
      return new ABSLTokenTagger(buffer, this.ClassificationTypeRegistry) as ITagger<T>;
    }
  }
}
