﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Classification.TextViewCloseRegistration
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using System;

namespace SAP.Copernicus.ABAPScriptLanguage.Classification
{
  internal class TextViewCloseRegistration
  {
    private IWpfTextView TextView;
    private string FilePath;

    public TextViewCloseRegistration(IWpfTextView textView)
    {
      this.TextView = textView;
      this.FilePath = textView.TextBuffer.Properties.GetProperty<ITextDocument>((object) typeof (ITextDocument)).FilePath;
      this.TextView.Closed += new EventHandler(this.OnTextViewClosed);
    }

    private void OnTextViewClosed(object sender, EventArgs e)
    {
      TaggingCache.Instance.InvalidateForFile(this.FilePath);
    }
  }
}
