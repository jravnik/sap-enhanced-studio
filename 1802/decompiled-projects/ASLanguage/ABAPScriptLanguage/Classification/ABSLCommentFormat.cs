﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Classification.ABSLCommentFormat
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;
using System.ComponentModel.Composition;
using System.Windows.Media;

namespace SAP.Copernicus.ABAPScriptLanguage.Classification
{
  [ClassificationType(ClassificationTypeNames = "ABSLComment")]
  [UserVisible(true)]
  [Order(Before = "Default Priority")]
  [Name("ABSLComment")]
  [Export(typeof (EditorFormatDefinition))]
  internal sealed class ABSLCommentFormat : ClassificationFormatDefinition
  {
    public ABSLCommentFormat()
    {
      this.DisplayName = Resources.ClassificationFormatNameComment;
      this.ForegroundColor = new Color?(Colors.DarkGreen);
    }
  }
}
