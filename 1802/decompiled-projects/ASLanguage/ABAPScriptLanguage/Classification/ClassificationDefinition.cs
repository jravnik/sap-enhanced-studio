﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Classification.ClassificationDefinition
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;
using System.ComponentModel.Composition;

namespace SAP.Copernicus.ABAPScriptLanguage.Classification
{
  internal static class ClassificationDefinition
  {
    [Name("ABSLKeyword")]
    [Export(typeof (ClassificationTypeDefinition))]
    internal static ClassificationTypeDefinition ABSLKeyword;
    [Export(typeof (ClassificationTypeDefinition))]
    [Name("ABSLIdentifier")]
    internal static ClassificationTypeDefinition ABSLIdentifier;
    [Export(typeof (ClassificationTypeDefinition))]
    [Name("ABSLStringLiteral")]
    internal static ClassificationTypeDefinition ABSLStringLiteral;
    [Name("ABSLNumericLiteral")]
    [Export(typeof (ClassificationTypeDefinition))]
    internal static ClassificationTypeDefinition ABSLNumericLiteral;
    [Export(typeof (ClassificationTypeDefinition))]
    [Name("ABSLText")]
    internal static ClassificationTypeDefinition ABSLText;
    [Name("ABSLComment")]
    [Export(typeof (ClassificationTypeDefinition))]
    internal static ClassificationTypeDefinition ABSLComment;
  }
}
