﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Classification.ABSLTokenTagger
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Tagging;
using SAP.Copernicus.ABAPScriptLanguage.Env;
using SAP.Copernicus.ABAPScriptLanguage.IntelliSense;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SAP.Copernicus.ABAPScriptLanguage.Classification
{
  internal sealed class ABSLTokenTagger : ITagger<ClassificationTag>
  {
    private ITextBuffer TextBuffer;
    private IDictionary<ABSLTokenType, IClassificationType> ABSLTypes;

    internal ABSLTokenTagger(ITextBuffer buffer, IClassificationTypeRegistryService typeService)
    {
      this.TextBuffer = buffer;
      buffer.Changed += new EventHandler<TextContentChangedEventArgs>(this.OnBufferChanged);
      this.ABSLTypes = (IDictionary<ABSLTokenType, IClassificationType>) new Dictionary<ABSLTokenType, IClassificationType>();
      this.ABSLTypes[ABSLTokenType.Keyword] = typeService.GetClassificationType("ABSLKeyword");
      this.ABSLTypes[ABSLTokenType.Identifier] = typeService.GetClassificationType("ABSLIdentifier");
      this.ABSLTypes[ABSLTokenType.StringLiteral] = typeService.GetClassificationType("ABSLStringLiteral");
      this.ABSLTypes[ABSLTokenType.NumericLiteral] = typeService.GetClassificationType("ABSLNumericLiteral");
      this.ABSLTypes[ABSLTokenType.Text] = typeService.GetClassificationType("ABSLText");
      this.ABSLTypes[ABSLTokenType.Comment] = typeService.GetClassificationType("ABSLComment");
    }

    public void OnBufferChanged(object sender, TextContentChangedEventArgs args)
    {
      if (args.Changes.Count == 0)
        return;
      ITextDocument property;
      if (this.TextBuffer.Properties.TryGetProperty<ITextDocument>((object) typeof (ITextDocument), out property))
        TaggingCache.Instance.InvalidateForFile(property.FilePath);
      ITextSnapshot after = args.After;
      after.GetText();
      ITextChange change = args.Changes[0];
      int numberFromPosition = after.GetLineNumberFromPosition(change.NewSpan.Start);
      ITextSnapshotLine lineFromLineNumber1 = after.GetLineFromLineNumber(numberFromPosition);
      string str = "";
      if (change.Delta < 0)
        str = this.ReinsertChangeAndGetLine(after, change, numberFromPosition);
      if (!lineFromLineNumber1.GetText().Contains("/*") && !lineFromLineNumber1.GetText().Contains("*/") && (!str.Contains("/*") && !str.Contains("*/")))
        return;
      for (int lineNumber = 0; lineNumber < after.LineCount; ++lineNumber)
      {
        if (lineNumber != numberFromPosition)
        {
          ITextSnapshotLine lineFromLineNumber2 = after.GetLineFromLineNumber(lineNumber);
          this.TagsChanged((object) this, new SnapshotSpanEventArgs(new SnapshotSpan(after, Span.FromBounds((int) lineFromLineNumber2.Start, (int) lineFromLineNumber2.End))));
        }
      }
    }

    private string ReinsertChangeAndGetLine(ITextSnapshot snapshot, ITextChange change, int idx)
    {
      return snapshot.TextBuffer.CurrentSnapshot.GetText().Insert(change.OldPosition, change.OldText).Split('\n')[idx];
    }

    public event EventHandler<SnapshotSpanEventArgs> TagsChanged;

    public IEnumerable<ITagSpan<ClassificationTag>> GetTags(NormalizedSnapshotSpanCollection spans)
    {
      if (spans.Count == 0)
        Trace.WriteLine("No spans provided for tagging");
      else if (this.TextBuffer.Properties.ContainsProperty((object) typeof (ITextDocument)))
      {
        string filePath = ((ITextDocument) this.TextBuffer.Properties[(object) typeof (ITextDocument)]).FilePath;
        ITextSnapshot snapshot = this.TextBuffer.CurrentSnapshot;
        string completeSource = snapshot.GetText();
        CodeCompletionContext completionContext = TaggingCache.Instance.GetOrCreateContextForFile(filePath, completeSource);
        foreach (SnapshotSpan span in spans)
        {
          int startLine = span.Start.GetContainingLine().LineNumber;
          int endLine = span.End.GetContainingLine().LineNumber;
          for (int spanLine = startLine; spanLine <= endLine; ++spanLine)
          {
            foreach (VirtualToken token in completionContext.TokensToTagForLine(spanLine))
            {
              SnapshotSpan? tokenSpan = new SnapshotSpan?();
              try
              {
                tokenSpan = new SnapshotSpan?(new SnapshotSpan(snapshot, new Span(token.StartIndex, token.Length)));
              }
              catch (ArgumentOutOfRangeException ex)
              {
              }
              if (tokenSpan.HasValue && tokenSpan.Value.IntersectsWith(span))
              {
                ABSLTokenType abslTokenType = TokenHelper.MapVirtualTokenToABSLTokenType(token);
                yield return (ITagSpan<ClassificationTag>) new TagSpan<ClassificationTag>(tokenSpan.Value, new ClassificationTag(this.ABSLTypes[abslTokenType]));
              }
            }
          }
        }
      }
    }
  }
}
