﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Env.TokenHelper
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using SAP.Copernicus.ABAPScriptLanguage.Classification;
using SAP.Copernicus.ABAPScriptLanguage.IntelliSense;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.ABAPScriptLanguage.Env
{
  public static class TokenHelper
  {
    public static Dictionary<int, string> KeywordTokenToStringMapper = new Dictionary<int, string>()
    {
      {
        10,
        "as"
      },
      {
        57,
        "import"
      },
      {
        54,
        "if"
      },
      {
        41,
        "else"
      },
      {
        88,
        "switch"
      },
      {
        23,
        "case"
      },
      {
        33,
        "default"
      },
      {
        100,
        "while"
      },
      {
        49,
        "foreach"
      },
      {
        58,
        "in"
      },
      {
        31,
        "continue"
      },
      {
        16,
        "break"
      },
      {
        79,
        "raise"
      },
      {
        98,
        "var"
      },
      {
        27,
        "collectionof"
      },
      {
        44,
        "elementsof"
      },
      {
        83,
        "return"
      }
    };
    public static int[] KeywordTokenTypes = TokenHelper.KeywordTokenToStringMapper.Keys.ToArray<int>();
    public static int[] PathClosingTokens = new int[7]
    {
      62,
      82,
      64,
      84,
      35,
      12,
      30
    };

    public static ABSLTokenType MapVirtualTokenToABSLTokenType(VirtualToken token)
    {
      ABSLTokenType abslTokenType = ABSLTokenType.Text;
      if (token.Type == 5)
        abslTokenType = ABSLTokenType.Identifier;
      else if (token.Type == 32 || token.Type == 75 || token.Type == 17)
        abslTokenType = ABSLTokenType.NumericLiteral;
      else if (token.Type == 86 || token.Type == 18)
        abslTokenType = ABSLTokenType.StringLiteral;
      else if (((IEnumerable<int>) TokenHelper.KeywordTokenTypes).Contains<int>(token.Type))
        abslTokenType = ABSLTokenType.Keyword;
      else if (token.Channel == 99)
        abslTokenType = ABSLTokenType.Comment;
      return abslTokenType;
    }

    public static string[] GetAllowedKeywordStrings(CodeCompletionContext completionContext)
    {
      List<string> stringList = new List<string>();
      foreach (int allowedTokenType in completionContext.AllowedTokenTypes)
      {
        if (((IEnumerable<int>) TokenHelper.KeywordTokenTypes).Contains<int>(allowedTokenType))
        {
          string str = TokenHelper.KeywordTokenToStringMapper[allowedTokenType];
          if (string.IsNullOrEmpty(completionContext.PartialTokenText) || str.StartsWith(completionContext.PartialTokenText))
            stringList.Add(str);
        }
      }
      return stringList.ToArray();
    }

    public static ASDeclaration[] GetAllowedKeywordDeclarations(CodeCompletionContext completionContext)
    {
      string[] allowedKeywordStrings = TokenHelper.GetAllowedKeywordStrings(completionContext);
      ASDeclaration[] asDeclarationArray = new ASDeclaration[allowedKeywordStrings.Length];
      int num = 0;
      foreach (string name in allowedKeywordStrings)
        asDeclarationArray[num++] = new ASDeclaration(name, 6);
      return asDeclarationArray;
    }

    public static bool PossibleTokensContainKeywords(int[] possibleTokens)
    {
      foreach (int possibleToken in possibleTokens)
      {
        if (((IEnumerable<int>) TokenHelper.KeywordTokenTypes).Contains<int>(possibleToken))
          return true;
      }
      return false;
    }
  }
}
