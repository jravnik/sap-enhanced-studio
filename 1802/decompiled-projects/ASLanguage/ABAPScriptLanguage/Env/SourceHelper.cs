﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Env.SourceHelper
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.ABAPScriptLanguage.IntelliSense;

namespace SAP.Copernicus.ABAPScriptLanguage.Env
{
  public static class SourceHelper
  {
    private const string DELIM = ";";
    private const string CC_ID = "CC;";
    private const string FE_DELIM = "){}";
    private const string FE_CC_ID = "CC){}";
    private const string FE_CC_VAR_ID = "CC in CC){}";
    private const string SWITCH_DELIM = "){}";
    private const string SWITCH_CC_ID_DELIM = "CC ){}";

    public static string AddDelimAtCursor(CodeCompletionContext completionContext, ParseRequest req, bool keepTextBehindCursor = false)
    {
      CodeCompletionMode ccMode = completionContext.CCMode;
      if (ccMode == CodeCompletionMode.None)
        return req.Text;
      string str1 = "";
      switch (completionContext.SourceModification)
      {
        case CodeCompletionContext.SourceModificationMode.None:
          str1 = ccMode == CodeCompletionMode.TopLevel || ccMode == CodeCompletionMode.MethodTip && completionContext.InsertIdentifier ? "CC;" : ";";
          break;
        case CodeCompletionContext.SourceModificationMode.ForeachVariable:
          str1 = "CC in CC){}";
          break;
        case CodeCompletionContext.SourceModificationMode.ForeachAfterIn:
          str1 = ccMode == CodeCompletionMode.TopLevel || ccMode == CodeCompletionMode.MethodTip && completionContext.InsertIdentifier ? "CC){}" : "){}";
          break;
        case CodeCompletionContext.SourceModificationMode.SwitchValue:
          str1 = ccMode == CodeCompletionMode.TopLevel || ccMode == CodeCompletionMode.MethodTip && completionContext.InsertIdentifier ? "CC ){}" : "){}";
          break;
      }
      string str2 = SourceHelper.GetStringBeforeCursor(req) + str1;
      if (keepTextBehindCursor)
        str2 += SourceHelper.GetStringBehindCursor(req);
      return str2;
    }

    private static string GetStringBeforeCursor(ParseRequest req)
    {
      int piPos;
      int piVirtualSpaces;
      req.View.GetNearestPosition(req.Line, req.Col, out piPos, out piVirtualSpaces);
      return req.Text.Substring(0, piPos);
    }

    private static string GetStringBehindCursor(ParseRequest req)
    {
      int piPos;
      int piVirtualSpaces;
      req.View.GetNearestPosition(req.Line, req.Col, out piPos, out piVirtualSpaces);
      return req.Text.Substring(piPos);
    }

    public static string ExtractTextSpanFromString(string text, TextSpan textSpan)
    {
      if (text == null || text.Length == 0)
        return text;
      string[] strArray = text.Split('\n');
      int num1 = 0;
      int startIndex = 0;
      int num2 = 0;
      for (int index = 0; index < strArray.Length; ++index)
      {
        string str = strArray[index];
        if (index == textSpan.iStartLine)
          startIndex = num1 + textSpan.iStartIndex;
        if (index == textSpan.iEndLine)
        {
          num2 = num1 + textSpan.iEndIndex;
          break;
        }
        num1 += str.Length + 1;
      }
      text = text.Substring(startIndex, num2 - startIndex);
      return text;
    }

    internal static void RecalculateTextPosition(IVsTextView view, int line, int col, out int outLine, out int outCol)
    {
      int piPos;
      int piVirtualSpaces;
      view.GetNearestPosition(line, col, out piPos, out piVirtualSpaces);
      view.GetLineAndColumn(piPos, out outLine, out outCol);
    }
  }
}
