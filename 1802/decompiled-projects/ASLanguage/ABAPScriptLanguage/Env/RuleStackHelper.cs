﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Env.RuleStackHelper
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SAP.Copernicus.ABAPScriptLanguage.Env
{
  public static class RuleStackHelper
  {
    public static bool InAntlrRuleDeep(string ruleName)
    {
      return ((IEnumerable<StackFrame>) new StackTrace().GetFrames()).Where<StackFrame>((Func<StackFrame, bool>) (n => n.GetMethod().Name == ruleName)).Count<StackFrame>() > 0;
    }
  }
}
