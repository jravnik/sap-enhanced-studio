﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ABSLBreakpoint
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class ABSLBreakpoint : IDebugBoundBreakpoint2
  {
    private object lockToken = new object();
    private DebugEngine m_engine;
    private bool m_deleted;
    private bool m_enabled;
    private BreakpointResolution m_breakpointResolution;
    private uint m_hitCount;
    public PDI_ABSL_S_ABAP_LINE abapBreakpointLocation;

    public PendingBreakpoint m_pendingBreakpoint { get; private set; }

    public ABSLBreakpoint(DebugEngine engine, PendingBreakpoint pendingBreakpoint, BreakpointResolution breakpointResolution, bool enabled)
    {
      this.m_engine = engine;
      this.m_deleted = false;
      this.m_enabled = enabled;
      this.m_pendingBreakpoint = pendingBreakpoint;
      this.m_breakpointResolution = breakpointResolution;
      this.m_hitCount = 0U;
    }

    int IDebugBoundBreakpoint2.Delete()
    {
      this.m_deleted = true;
      return 0;
    }

    int IDebugBoundBreakpoint2.Enable(int fEnable)
    {
      this.m_enabled = fEnable != 0;
      return 0;
    }

    int IDebugBoundBreakpoint2.GetBreakpointResolution(out IDebugBreakpointResolution2 ppBPResolution)
    {
      ppBPResolution = (IDebugBreakpointResolution2) this.m_breakpointResolution;
      return 0;
    }

    int IDebugBoundBreakpoint2.GetHitCount(out uint pdwHitCount)
    {
      pdwHitCount = this.m_hitCount;
      return 0;
    }

    int IDebugBoundBreakpoint2.GetPendingBreakpoint(out IDebugPendingBreakpoint2 ppPendingBreakpoint)
    {
      ppPendingBreakpoint = (IDebugPendingBreakpoint2) this.m_pendingBreakpoint;
      return 0;
    }

    int IDebugBoundBreakpoint2.GetState(enum_BP_STATE[] pState)
    {
      pState[0] = enum_BP_STATE.BPS_NONE;
      if (this.m_deleted)
        pState[0] = enum_BP_STATE.BPS_DELETED;
      else if (this.m_enabled)
        pState[0] = enum_BP_STATE.BPS_ENABLED;
      else if (!this.m_enabled)
        pState[0] = enum_BP_STATE.BPS_DISABLED;
      return 0;
    }

    int IDebugBoundBreakpoint2.SetCondition(BP_CONDITION bpCondition)
    {
      throw new NotImplementedException();
    }

    int IDebugBoundBreakpoint2.SetHitCount(uint dwHitCount)
    {
      this.m_hitCount = dwHitCount;
      return 0;
    }

    int IDebugBoundBreakpoint2.SetPassCount(BP_PASSCOUNT bpPassCount)
    {
      throw new NotImplementedException();
    }
  }
}
