﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ABAPDebugSession
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using Microsoft.VisualStudio.Debugger.Interop;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.Core.Protocol.JSON;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  public class ABAPDebugSession : JSONHandler
  {
    internal List<DebugThread> m_DebugThreadList = new List<DebugThread>();
    private DebugEngine engine;
    private uint m_NextThreadID;
    private bool m_terminated;
    public readonly Guid ProcessGUID;
    public readonly string breakpointUser;
    public readonly string breakpointUserAlias;
    private PendingBreakpointBatch createBPBatch;
    private PendingBreakpointBatch deleteBPBatch;
    public readonly FifoExecution fifoExecution;

    public static PDI_ABSL_S_DBG_INIT_INFO InitInfo { get; private set; }

    internal static ABAPDebugSession createInstance(DebugEngine engine, out PDI_RI_S_MESSAGE[] messages)
    {
      PDI_ABSL_S_DBG_INIT_INFO initInfo;
      string breakpointUserAlias;
      if (!ABAPDebugSession.InitalizeDebugger(out messages, out initInfo, out breakpointUserAlias))
        return (ABAPDebugSession) null;
      ABAPDebugSession.InitInfo = initInfo;
      return new ABAPDebugSession(engine, initInfo.BREAKPOINT_USER, breakpointUserAlias);
    }

    private ABAPDebugSession(DebugEngine engine, string breakpointUser, string breakpointUserAlias)
    {
      this.engine = engine;
      this.ProcessGUID = Guid.NewGuid();
      this.m_NextThreadID = 0U;
      this.m_terminated = false;
      this.breakpointUser = breakpointUser;
      this.breakpointUserAlias = breakpointUserAlias;
      this.fifoExecution = new FifoExecution();
      this.createBPBatch = new PendingBreakpointBatch(this.fifoExecution, new MassPendingBreakpointOperation(this.AddBreakpointsBatchHandler));
      this.deleteBPBatch = new PendingBreakpointBatch(this.fifoExecution, new MassPendingBreakpointOperation(this.DeleteBreakpointsBatchHandler));
    }

    private static bool InitalizeDebugger(out PDI_RI_S_MESSAGE[] messages, out PDI_ABSL_S_DBG_INIT_INFO initInfo, out string breakpointUserAlias)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
      pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
      pdiAbslDbg.Importing.IV_BREAKPOINT_USER_ALIAS = ASDebuggerUIUtils.GetUserAliasForDebuggingFromOptionsDialog();
      pdiAbslDbg.Importing.IV_OPERATION = 5;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        pdiAbslDbg.Exporting = (PDI_ABSL_DBG.ExportingType) null;
      }
      if (pdiAbslDbg.Exporting != null)
      {
        messages = pdiAbslDbg.Exporting.ET_MESSAGES;
        initInfo = pdiAbslDbg.Exporting.ES_INIT_INFO;
        breakpointUserAlias = pdiAbslDbg.Importing.IV_BREAKPOINT_USER_ALIAS;
        if (breakpointUserAlias == "")
        {
          breakpointUserAlias = SAP.Copernicus.Core.Protocol.Connection.getInstance().getLoggedInUserAlias();
          if (breakpointUserAlias == null || breakpointUserAlias == "")
            breakpointUserAlias = initInfo.BREAKPOINT_USER;
        }
        return pdiAbslDbg.Exporting.EV_SUCCESS != "";
      }
      messages = (PDI_RI_S_MESSAGE[]) null;
      initInfo = (PDI_ABSL_S_DBG_INIT_INFO) null;
      breakpointUserAlias = (string) null;
      return false;
    }

    internal void StartListener()
    {
      new Thread(new ParameterizedThreadStart(this.ListenerThreadFunc))
      {
        Name = "ABSL debugger listener loop"
      }.Start();
    }

    private void ListenerThreadFunc(object _jsonClient)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      Semaphore listenerSempahore = new Semaphore(1, 1);
      while (true)
      {
        PDI_ABSL_DBG pdiAbslDbg;
        do
        {
          listenerSempahore.WaitOne();
          pdiAbslDbg = new PDI_ABSL_DBG();
          pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
          pdiAbslDbg.Importing.IV_OPERATION = 1;
          pdiAbslDbg.Importing.IV_BREAKPOINT_USER = this.breakpointUser;
          try
          {
            jsonClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
          }
          catch (Exception ex)
          {
            Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
            pdiAbslDbg.Exporting = (PDI_ABSL_DBG.ExportingType) null;
          }
          if (this.m_terminated)
            return;
          if (pdiAbslDbg.Exporting == null)
          {
            ErrorEvent.Send(this.engine, (DebugThread) null, enum_MESSAGETYPE.MT_MESSAGEBOX, string.Format(Resources.DebuggerListenerConnectionLostPopup), 16U);
            OutputStringEvent.Send(this.engine, (DebugThread) null, string.Format(Resources.DebuggerListenerConnectionLostOuputWindow));
            return;
          }
          if (!(pdiAbslDbg.Exporting.EV_SUCCESS != "") || pdiAbslDbg.Exporting.ET_WAITING_DEBUGGEE.Length <= 0)
            goto label_10;
        }
        while (this.AttachWaitingDebuggee(pdiAbslDbg.Exporting.ET_WAITING_DEBUGGEE, listenerSempahore));
        listenerSempahore.Release();
        continue;
label_10:
        listenerSempahore.Release();
      }
    }

    internal void DeleteBreakPoint(PendingBreakpoint pendingBreakpoint)
    {
      if (this.deleteBPBatch.Add(pendingBreakpoint))
        return;
      this.deleteBPBatch = this.deleteBPBatch.CopyAndAdd(pendingBreakpoint);
    }

    internal void AddBreakPoint(PendingBreakpoint pendingBreakpoint)
    {
      if (this.createBPBatch.Add(pendingBreakpoint))
        return;
      this.createBPBatch = this.createBPBatch.CopyAndAdd(pendingBreakpoint);
    }

    private void AddBreakpointsBatchHandler(List<PendingBreakpoint> pendingBreakpoints)
    {
      lock (this.m_DebugThreadList)
      {
        foreach (DebugThread debugThread in this.m_DebugThreadList)
          debugThread.Schedule_AddDynamicBreakpoints(pendingBreakpoints);
      }
      this.AddStaticBreakpoints(pendingBreakpoints);
    }

    private void AddStaticBreakpoints(List<PendingBreakpoint> pendingBreakpoints)
    {
      Trace.WriteLine(string.Format("ABSL Debugger - AddStaticBreakpoints begin {0}", (object) pendingBreakpoints.Count));
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
      PDI_ABSL_S_BREAKPOINT[] pdiAbslSBreakpointArray = new PDI_ABSL_S_BREAKPOINT[pendingBreakpoints.Count];
      Tuple<PDI_ABSL_S_BREAKPOINT, PendingBreakpoint>[] tupleArray = new Tuple<PDI_ABSL_S_BREAKPOINT, PendingBreakpoint>[pendingBreakpoints.Count];
      for (int index = 0; index < pendingBreakpoints.Count; ++index)
      {
        pdiAbslSBreakpointArray[index] = new PDI_ABSL_S_BREAKPOINT();
        pdiAbslSBreakpointArray[index].LINE = pendingBreakpoints[index].line;
        pdiAbslSBreakpointArray[index].XREP_FILE_PATH = XRepMapper.GetInstance().GetXrepPathforLocalFile(pendingBreakpoints[index].documentName);
        pdiAbslSBreakpointArray[index].SOURCE_HASH = EngineUtils.GetHashForABSLFile(pendingBreakpoints[index].documentName);
        tupleArray[index] = Tuple.Create<PDI_ABSL_S_BREAKPOINT, PendingBreakpoint>(pdiAbslSBreakpointArray[index], pendingBreakpoints[index]);
      }
      pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
      pdiAbslDbg.Importing.IV_OPERATION = 100;
      pdiAbslDbg.Importing.IV_BREAKPOINT_USER = this.breakpointUser;
      pdiAbslDbg.Importing.IT_BREAKPOINT = pdiAbslSBreakpointArray;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        pdiAbslDbg.Exporting = (PDI_ABSL_DBG.ExportingType) null;
      }
      if (pdiAbslDbg.Exporting != null && pdiAbslDbg.Exporting.EV_SUCCESS != "")
      {
        Array.Sort<PDI_ABSL_S_BREAKPOINT>(pdiAbslDbg.Exporting.ET_BREAKPOINT, PDI_ABSL_S_BREAKPOINT.CompareByFilePathAndLine);
        foreach (Tuple<PDI_ABSL_S_BREAKPOINT, PendingBreakpoint> tuple in tupleArray)
        {
          int index = Array.BinarySearch<PDI_ABSL_S_BREAKPOINT>(pdiAbslDbg.Exporting.ET_BREAKPOINT, tuple.Item1, PDI_ABSL_S_BREAKPOINT.CompareByFilePathAndLine);
          if (index < 0)
          {
            tuple.Item2.RaiseErrorEvent(Resources.DebuggerUnknownBreakpointErrorTooltip);
          }
          else
          {
            PDI_ABSL_S_BREAKPOINT pdiAbslSBreakpoint = pdiAbslDbg.Exporting.ET_BREAKPOINT[index];
            if (pdiAbslSBreakpoint.ABAP_LINE.METHOD_INCLUDE.Length > 0)
              tuple.Item2.RaiseBoundEvent(pdiAbslSBreakpoint.ABAP_LINE);
            else
              tuple.Item2.RaiseErrorEvent(EngineUtils.EnsureDelimiter(pdiAbslSBreakpoint.ERROR_TEXT));
          }
        }
      }
      else
      {
        string errorMessage = pdiAbslDbg.Exporting == null || pdiAbslDbg.Exporting.ET_MESSAGES.Length <= 0 ? Resources.DebuggerUnknownBreakpointErrorTooltip : EngineUtils.PDIMessagesToString(pdiAbslDbg.Exporting.ET_MESSAGES);
        foreach (PendingBreakpoint pendingBreakpoint in pendingBreakpoints)
          pendingBreakpoint.RaiseErrorEvent(errorMessage);
      }
      Trace.WriteLine(string.Format("ABSL Debugger - AddStaticBreakpoints end {0}", (object) pendingBreakpoints.Count));
    }

    internal void DeleteBreakpointsBatchHandler(List<PendingBreakpoint> pendingBreakpoints)
    {
      lock (this.m_DebugThreadList)
      {
        foreach (DebugThread debugThread in this.m_DebugThreadList)
          debugThread.Schedule_DeleteDynamicBreakpoints(pendingBreakpoints);
      }
      this.DeleteStaticBreakpoints(pendingBreakpoints);
    }

    private void DeleteStaticBreakpoints(List<PendingBreakpoint> pendingBreakpoints)
    {
      Trace.WriteLine(string.Format("ABSL Debugger - DeleteStaticBreakpoints begin {0}", (object) pendingBreakpoints.Count));
      List<PDI_ABSL_S_BREAKPOINT> pdiAbslSBreakpointList = new List<PDI_ABSL_S_BREAKPOINT>();
      for (int index = 0; index < pendingBreakpoints.Count; ++index)
      {
        PDI_ABSL_S_ABAP_LINE boundLocation = pendingBreakpoints[index].BoundLocation;
        if (boundLocation != null)
          pdiAbslSBreakpointList.Add(new PDI_ABSL_S_BREAKPOINT()
          {
            LINE = pendingBreakpoints[index].line,
            XREP_FILE_PATH = XRepMapper.GetInstance().GetXrepPathforLocalFile(pendingBreakpoints[index].documentName),
            ABAP_LINE = boundLocation
          });
      }
      if (pdiAbslSBreakpointList.Count > 0)
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
        pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
        pdiAbslDbg.Importing.IV_OPERATION = 101;
        pdiAbslDbg.Importing.IV_BREAKPOINT_USER = this.breakpointUser;
        pdiAbslDbg.Importing.IT_BREAKPOINT = pdiAbslSBreakpointList.ToArray();
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
        }
        catch (Exception ex)
        {
          Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        }
      }
      PENDING_BP_STATE_INFO[] pState = new PENDING_BP_STATE_INFO[1];
      foreach (PendingBreakpoint pendingBreakpoint in pendingBreakpoints)
      {
        ((IDebugPendingBreakpoint2) pendingBreakpoint).GetState(pState);
        if (pState[0].state == enum_PENDING_BP_STATE.PBPS_DELETED)
          pendingBreakpoint.OnBoundBreakpointDeleted();
      }
      Trace.WriteLine(string.Format("ABSL Debugger - DeleteStaticBreakpoints end {0}", (object) pendingBreakpoints.Count));
    }

    internal bool StopListener()
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
      pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
      pdiAbslDbg.Importing.IV_BREAKPOINT_USER = this.breakpointUser;
      pdiAbslDbg.Importing.IV_OPERATION = 4;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        pdiAbslDbg.Exporting = (PDI_ABSL_DBG.ExportingType) null;
      }
      if (pdiAbslDbg.Exporting != null)
        return pdiAbslDbg.Exporting.EV_SUCCESS != "";
      return false;
    }

    private bool AttachWaitingDebuggee(PDI_ABSL_S_DEBUGGEE[] waitingDebuggee, Semaphore listenerSempahore)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(true);
      PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
      pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
      pdiAbslDbg.Importing.IV_OPERATION = 2;
      pdiAbslDbg.Importing.IV_MDRS_NAMESPACE = EngineUtils.GetCurrentSolutionNamespace();
      pdiAbslDbg.Importing.IV_BREAKPOINT_USER = this.breakpointUser;
      pdiAbslDbg.Importing.IT_WAITING_DEBUGGEE = waitingDebuggee;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        pdiAbslDbg.Exporting = (PDI_ABSL_DBG.ExportingType) null;
      }
      if (pdiAbslDbg.Exporting == null || pdiAbslDbg.Exporting.EV_SUCCESS == "")
      {
        ErrorEvent.Send(this.engine, (DebugThread) null, enum_MESSAGETYPE.MT_MESSAGEBOX, string.Format(Resources.DebuggerAttachErrorPopup, (object) EngineUtils.PDIMessagesToString(pdiAbslDbg.Exporting.ET_MESSAGES)), 16U);
        return false;
      }
      string xrepProjectEntities = XRepMapper.GetInstance().GetLocalPathforXRepProjectEntities(pdiAbslDbg.Exporting.EV_XREP_FILE_PATH);
      TextSpan textspan = pdiAbslDbg.Exporting.ES_BREAKPOINT_SPAN.textspan;
      DebugThread debugThread = new DebugThread(this.engine, ++this.m_NextThreadID, pdiAbslDbg.Exporting.ES_ATTACHED_DEBUGGEE, jsonClient, this, listenerSempahore);
      debugThread.threadContext = new ThreadContext(this.engine, debugThread, xrepProjectEntities, textspan, pdiAbslDbg.Exporting.ET_VARIABLE, pdiAbslDbg.Exporting.ET_CALLSTACK);
      debugThread.waitingForInput = true;
      ThreadCreateEvent.Send(this.engine, debugThread);
      lock (this.m_DebugThreadList)
        this.m_DebugThreadList.Add(debugThread);
      PendingBreakpoint documentNameAndLineSpan = this.engine.breakPointManager.GetByDocumentNameAndLineSpan(xrepProjectEntities, textspan.iStartLine, textspan.iEndLine);
      if (documentNameAndLineSpan != null)
        documentNameAndLineSpan.RaiseBreakEvent(debugThread);
      else
        SteppingCompleteEvent.Send(this.engine, debugThread);
      if (!EngineUtils.CheckBackendHashForABSLFile(pdiAbslDbg.Exporting.EV_SOURCE_HASH, xrepProjectEntities))
        this.engine.ShowSourceOutOfDatePopup(EngineUtils.PDIMessagesToString(pdiAbslDbg.Exporting.ET_MESSAGES));
      return true;
    }

    internal void TerminateProcess()
    {
      this.m_terminated = true;
      this.StopListener();
      this.fifoExecution.QueueUserWorkItem((WaitCallback) (ignored =>
      {
        foreach (DebugThread debugThread in this.m_DebugThreadList)
          debugThread.Schedule_Detach();
      }), (object) null, false);
      try
      {
        ProgramDestroyEvent.Send(this.engine, 1U);
      }
      catch (InvalidOperationException ex)
      {
      }
    }
  }
}
