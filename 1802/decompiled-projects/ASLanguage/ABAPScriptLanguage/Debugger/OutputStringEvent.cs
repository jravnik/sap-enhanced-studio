﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.OutputStringEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class OutputStringEvent : AsynchronousEvent, IDebugOutputStringEvent2
  {
    public const string IID = "569C4BB1-7B82-46FC-AE28-4536DDAD753E";
    private string outputString;

    public static void Send(DebugEngine engine, DebugThread thread, string outputString)
    {
      OutputStringEvent outputStringEvent = new OutputStringEvent(outputString);
      engine.Send((IDebugEvent2) outputStringEvent, "569C4BB1-7B82-46FC-AE28-4536DDAD753E", (IDebugThread2) thread);
    }

    private OutputStringEvent(string outputString)
    {
      this.outputString = outputString;
    }

    int IDebugOutputStringEvent2.GetString(out string pbstrString)
    {
      pbstrString = this.outputString;
      return 0;
    }
  }
}
