﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.StackFrame
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class StackFrame : IDebugStackFrame2, IDebugExpressionContext2, IDebugProperty2
  {
    private Dictionary<string, VariableInformation> m_parsedExpressions = new Dictionary<string, VariableInformation>();
    private const uint MaxMillisecondsTimeout = 1000;
    private DebugEngine m_engine;
    private DebugThread m_thread;
    private PDI_ABSL_S_DBG_STACK_LEVEL m_stackLevel;
    private string m_documentName;
    private string m_frameName;
    private TextSpan m_span;
    private PDI_ABSL_S_VARIABLE[] m_variables;
    private VariableInformation[] m_locals;
    private bool m_dataAvailable;
    private ManualResetEvent m_dataRequestedEvent;
    public readonly FRAMEINFO FrameInfo;

    public int StackPosition
    {
      get
      {
        return this.m_stackLevel.STACK_POSITION;
      }
    }

    internal StackFrame(DebugEngine engine, DebugThread thread, PDI_ABSL_S_DBG_STACK_LEVEL stackLevel)
    {
      this.m_engine = engine;
      this.m_thread = thread;
      this.m_stackLevel = stackLevel;
      this.m_documentName = XRepMapper.GetInstance().GetLocalPathforXRepProjectEntities(stackLevel.XREP_FILE_PATH);
      this.m_span = stackLevel.SPAN.textspan;
      string projectFolderXrepPath = XRepMapper.GetInstance().GetProjectFolderXRepPath();
      this.m_frameName = string.Format("{0} Line {1}", !stackLevel.XREP_FILE_PATH.StartsWith(projectFolderXrepPath + "/") ? (object) stackLevel.XREP_FILE_PATH : (object) stackLevel.XREP_FILE_PATH.Substring(projectFolderXrepPath.Length + 1), (object) this.m_stackLevel.SPAN.START_LINE);
      this.SetFrameInfo(out this.FrameInfo);
    }

    internal StackFrame(DebugEngine engine, DebugThread thread, PDI_ABSL_S_DBG_STACK_LEVEL stackLevel, PDI_ABSL_S_VARIABLE[] variables)
      : this(engine, thread, stackLevel)
    {
      this.SetData(variables);
    }

    private bool CheckDataAvailable(uint millisecondsTimeout)
    {
      if (this.m_dataAvailable)
        return true;
      if (this.m_dataRequestedEvent == null)
      {
        this.m_dataRequestedEvent = new ManualResetEvent(false);
        this.m_engine.ABAPSession.fifoExecution.QueueUserWorkItem((WaitCallback) (ignored => this.m_thread.Schedule_GetStackFrameInfo(this, this.m_dataRequestedEvent)), (object) null, false);
      }
      return this.m_dataRequestedEvent.WaitOne((int) Math.Min(millisecondsTimeout, 1000U));
    }

    public void SetData(PDI_ABSL_S_VARIABLE[] variables)
    {
      this.m_variables = variables;
      this.m_locals = new VariableInformation[this.m_variables.Length];
      for (int index = 0; index < this.m_variables.Length; ++index)
        this.AddLocal(this.m_variables[index], ref this.m_locals[index]);
      this.m_dataAvailable = true;
    }

    private void AddLocal(PDI_ABSL_S_VARIABLE variable, ref VariableInformation local)
    {
      try
      {
        VariableValueParser variableValueParser = new VariableValueParser(variable);
        local = variableValueParser.DeserializeValue();
      }
      catch (XmlException ex)
      {
        local = new VariableInformation();
        local.m_name = variable.ABSL_NAME;
        local.m_typeName = variable.TYPE;
        local.m_value = variable.VALUE;
      }
    }

    public void SetFrameInfo(out FRAMEINFO frameInfo)
    {
      frameInfo = new FRAMEINFO();
      frameInfo.m_bstrFuncName = this.m_frameName;
      frameInfo.m_dwValidFields |= enum_FRAMEINFO_FLAGS.FIF_FUNCNAME;
      frameInfo.m_bstrLanguage = "";
      frameInfo.m_dwValidFields |= enum_FRAMEINFO_FLAGS.FIF_LANGUAGE;
      frameInfo.m_addrMin = 0UL;
      frameInfo.m_addrMax = 0UL;
      frameInfo.m_dwValidFields |= enum_FRAMEINFO_FLAGS.FIF_STACKRANGE;
      frameInfo.m_pFrame = (IDebugStackFrame2) this;
      frameInfo.m_dwValidFields |= enum_FRAMEINFO_FLAGS.FIF_FRAME;
      frameInfo.m_fHasDebugInfo = 1;
      frameInfo.m_dwValidFields |= enum_FRAMEINFO_FLAGS.FIF_DEBUGINFO;
      frameInfo.m_fStaleCode = 0;
      frameInfo.m_dwValidFields |= enum_FRAMEINFO_FLAGS.FIF_STALECODE;
    }

    int IDebugStackFrame2.EnumProperties(enum_DEBUGPROP_INFO_FLAGS dwFields, uint nRadix, ref Guid guidFilter, uint dwTimeout, out uint pcelt, out IEnumDebugPropertyInfo2 ppEnum)
    {
      if (guidFilter != DebugConstants.guidFilterLocalsPlusArgs && guidFilter != DebugConstants.guidFilterAllLocalsPlusArgs && (guidFilter != DebugConstants.guidFilterAllLocals && guidFilter != DebugConstants.guidFilterLocals))
      {
        pcelt = 0U;
        ppEnum = (IEnumDebugPropertyInfo2) null;
        return -2147467263;
      }
      if (!this.CheckDataAvailable(dwTimeout))
      {
        DebugPropertyMessage debugPropertyMessage = new DebugPropertyMessage("Data not available yet", enum_DBG_ATTRIB_FLAGS.DBG_ATTRIB_VALUE_TIMEOUT);
        pcelt = 1U;
        DEBUG_PROPERTY_INFO[] data = new DEBUG_PROPERTY_INFO[1]
        {
          debugPropertyMessage.PropertyInfo
        };
        ppEnum = (IEnumDebugPropertyInfo2) new PropertyInfoEnum(data);
        return 0;
      }
      pcelt = (uint) this.m_locals.Length;
      DEBUG_PROPERTY_INFO[] data1 = new DEBUG_PROPERTY_INFO[this.m_locals.Length];
      for (int index = 0; index < data1.Length; ++index)
      {
        DebugProperty debugProperty = new DebugProperty(this.m_locals[index]);
        data1[index] = debugProperty.ConstructDebugPropertyInfo(enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_ALL);
      }
      ppEnum = (IEnumDebugPropertyInfo2) new PropertyInfoEnum(data1);
      return 0;
    }

    int IDebugStackFrame2.GetCodeContext(out IDebugCodeContext2 ppCodeCxt)
    {
      ppCodeCxt = (IDebugCodeContext2) new DebugCodeContext(this.m_engine, this.m_documentName, this.m_span.iStartLine);
      return 0;
    }

    int IDebugStackFrame2.GetDebugProperty(out IDebugProperty2 ppProperty)
    {
      ppProperty = (IDebugProperty2) this;
      return 0;
    }

    int IDebugStackFrame2.GetDocumentContext(out IDebugDocumentContext2 docContext)
    {
      TEXT_POSITION begPos = new TEXT_POSITION();
      TEXT_POSITION endPos = new TEXT_POSITION();
      begPos.dwColumn = (uint) (this.m_span.iStartIndex - 1);
      begPos.dwLine = (uint) (this.m_span.iStartLine - 1);
      endPos.dwColumn = (uint) (this.m_span.iEndIndex - 1);
      endPos.dwLine = (uint) (this.m_span.iEndLine - 1);
      docContext = (IDebugDocumentContext2) new DebugDocumentContext(this.m_documentName, begPos, endPos, (DebugCodeContext) null);
      return 0;
    }

    int IDebugStackFrame2.GetExpressionContext(out IDebugExpressionContext2 ppExprCxt)
    {
      ppExprCxt = (IDebugExpressionContext2) this;
      return 0;
    }

    int IDebugStackFrame2.GetInfo(enum_FRAMEINFO_FLAGS dwFieldSpec, uint nRadix, FRAMEINFO[] pFrameInfo)
    {
      pFrameInfo[0] = this.FrameInfo;
      return 0;
    }

    int IDebugStackFrame2.GetLanguageInfo(ref string pbstrLanguage, ref Guid pguidLanguage)
    {
      pbstrLanguage = "ABSL";
      pguidLanguage = DebugConstants.LanguageGuid;
      return 0;
    }

    int IDebugStackFrame2.GetName(out string pbstrName)
    {
      pbstrName = this.m_frameName;
      return 0;
    }

    int IDebugStackFrame2.GetPhysicalStackRange(out ulong paddrMin, out ulong paddrMax)
    {
      throw new NotImplementedException();
    }

    int IDebugStackFrame2.GetThread(out IDebugThread2 ppThread)
    {
      ppThread = (IDebugThread2) this.m_thread;
      return 0;
    }

    int IDebugExpressionContext2.GetName(out string pbstrName)
    {
      throw new NotImplementedException();
    }

    int IDebugExpressionContext2.ParseText(string pszCode, enum_PARSEFLAGS dwFlags, uint nRadix, out IDebugExpression2 ppExpr, out string pbstrError, out uint pichError)
    {
      pbstrError = "";
      pichError = 0U;
      ppExpr = (IDebugExpression2) new DebugExpression(this.m_engine, this.m_thread, pszCode);
      return 0;
    }

    int IDebugProperty2.EnumChildren(enum_DEBUGPROP_INFO_FLAGS dwFields, uint dwRadix, ref Guid guidFilter, enum_DBG_ATTRIB_FLAGS dwAttribFilter, string pszNameFilter, uint dwTimeout, out IEnumDebugPropertyInfo2 ppEnum)
    {
      Guid guidFilter1 = guidFilter;
      uint pcelt;
      return ((IDebugStackFrame2) this).EnumProperties(dwFields, dwRadix, ref guidFilter1, dwTimeout, out pcelt, out ppEnum);
    }

    int IDebugProperty2.GetDerivedMostProperty(out IDebugProperty2 ppDerivedMost)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetExtendedInfo(ref Guid guidExtendedInfo, out object pExtendedInfo)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetMemoryBytes(out IDebugMemoryBytes2 ppMemoryBytes)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetMemoryContext(out IDebugMemoryContext2 ppMemory)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetParent(out IDebugProperty2 ppParent)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetPropertyInfo(enum_DEBUGPROP_INFO_FLAGS dwFields, uint dwRadix, uint dwTimeout, IDebugReference2[] rgpArgs, uint dwArgCount, DEBUG_PROPERTY_INFO[] pPropertyInfo)
    {
      rgpArgs = (IDebugReference2[]) null;
      DEBUG_PROPERTY_INFO debugPropertyInfo = new DEBUG_PROPERTY_INFO();
      debugPropertyInfo.dwAttrib |= enum_DBG_ATTRIB_FLAGS.DBG_ATTRIB_VALUE_READONLY;
      debugPropertyInfo.dwAttrib |= enum_DBG_ATTRIB_FLAGS.DBG_ATTRIB_OBJ_IS_EXPANDABLE;
      debugPropertyInfo.pProperty = (IDebugProperty2) this;
      debugPropertyInfo.dwFields |= enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_PROP;
      pPropertyInfo[0] = debugPropertyInfo;
      return 0;
    }

    int IDebugProperty2.GetReference(out IDebugReference2 ppReference)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetSize(out uint pdwSize)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.SetValueAsReference(IDebugReference2[] rgpArgs, uint dwArgCount, IDebugReference2 pValue, uint dwTimeout)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.SetValueAsString(string pszValue, uint dwRadix, uint dwTimeout)
    {
      throw new NotImplementedException();
    }
  }
}
