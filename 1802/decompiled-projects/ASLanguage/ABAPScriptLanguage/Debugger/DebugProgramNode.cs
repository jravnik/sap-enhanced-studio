﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebugProgramNode
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using SAP.Copernicus.Core.Protocol;
using System;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class DebugProgramNode : IDebugProgramNode2
  {
    private readonly AD_PROCESS_ID m_processId;

    public DebugProgramNode(AD_PROCESS_ID processId)
    {
      this.m_processId = processId;
    }

    int IDebugProgramNode2.GetEngineInfo(out string engineName, out Guid engineGuid)
    {
      engineName = "ABSL Debugger";
      engineGuid = DebugConstants.DebugEngineGuid;
      return 0;
    }

    int IDebugProgramNode2.GetHostPid(AD_PROCESS_ID[] pHostProcessId)
    {
      pHostProcessId[0].ProcessIdType = this.m_processId.ProcessIdType;
      pHostProcessId[0].dwProcessId = this.m_processId.dwProcessId;
      pHostProcessId[0].guidProcessId = this.m_processId.guidProcessId;
      return 0;
    }

    public int GetHostName(enum_GETHOSTNAME_TYPE dwHostNameType, out string pbstrHostName)
    {
      pbstrHostName = Connection.getInstance().GetConnectionName();
      return 0;
    }

    public int GetProgramName(out string pbstrProgramName)
    {
      pbstrProgramName = "ABSL debug engine";
      return 0;
    }

    public int Attach_V7(IDebugProgram2 pMDMProgram, IDebugEventCallback2 pCallback, uint dwReason)
    {
      return -2147467263;
    }

    public int DetachDebugger_V7()
    {
      return -2147467263;
    }

    public int GetHostMachineName_V7(out string hostMachineName)
    {
      hostMachineName = (string) null;
      return -2147467263;
    }
  }
}
