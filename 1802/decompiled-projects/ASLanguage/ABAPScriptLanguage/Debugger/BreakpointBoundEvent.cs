﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.BreakpointBoundEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class BreakpointBoundEvent : AsynchronousEvent, IDebugBreakpointBoundEvent2
  {
    public const string IID = "1DDDB704-CF99-4B8A-B746-DABB01DD13A0";
    private IDebugPendingBreakpoint2 m_pendingBreakpoint;
    private IDebugBoundBreakpoint2 m_boundBreakpoint;

    private BreakpointBoundEvent(IDebugPendingBreakpoint2 pendingBreakpoint, IDebugBoundBreakpoint2 boundBreakpoint)
    {
      this.m_pendingBreakpoint = pendingBreakpoint;
      this.m_boundBreakpoint = boundBreakpoint;
    }

    public static void Send(DebugEngine engine, IDebugPendingBreakpoint2 pendingBreakpoint, IDebugBoundBreakpoint2 boundBreakpoint)
    {
      BreakpointBoundEvent breakpointBoundEvent = new BreakpointBoundEvent(pendingBreakpoint, boundBreakpoint);
      engine.Send((IDebugEvent2) breakpointBoundEvent, "1DDDB704-CF99-4B8A-B746-DABB01DD13A0", (IDebugThread2) null);
    }

    int IDebugBreakpointBoundEvent2.EnumBoundBreakpoints(out IEnumDebugBoundBreakpoints2 ppEnum)
    {
      IDebugBoundBreakpoint2[] breakpoints = new IDebugBoundBreakpoint2[1]
      {
        this.m_boundBreakpoint
      };
      ppEnum = (IEnumDebugBoundBreakpoints2) new BoundBreakpointsEnum(breakpoints);
      return 0;
    }

    int IDebugBreakpointBoundEvent2.GetPendingBreakpoint(out IDebugPendingBreakpoint2 ppPendingBP)
    {
      ppPendingBP = this.m_pendingBreakpoint;
      return 0;
    }
  }
}
