﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.LoadCompleteEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class LoadCompleteEvent : StoppingEvent, IDebugLoadCompleteEvent2
  {
    public const string IID = "B1844850-1349-45D4-9F12-495212F5EB0B";

    public static void Send(DebugEngine engine, DebugThread thread)
    {
      LoadCompleteEvent loadCompleteEvent = new LoadCompleteEvent();
      engine.Send((IDebugEvent2) loadCompleteEvent, "B1844850-1349-45D4-9F12-495212F5EB0B", (IDebugThread2) thread);
    }
  }
}
