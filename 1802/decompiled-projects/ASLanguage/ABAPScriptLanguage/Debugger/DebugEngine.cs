﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebugEngine
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using SAP.Copernicus.ABAPScriptLanguage.Tracing;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Tracing;
using SAP.Copernicus.Core.Util;
using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  [ComVisible(true)]
  [Guid("BEAA8428-BF9F-4673-A596-31B1E325D918")]
  public class DebugEngine : IDebugEngine2, IDebugEngineLaunch2, IDebugProgram3, IDebugProgram2
  {
    private IDebugEventCallback2 callback;
    private Guid programID;
    private bool breakpointErrorPopupDisplayed;
    private bool sourceOutOfDatePopupDisplayed;

    internal BreakpointManager breakPointManager { get; private set; }

    public ABAPDebugSession ABAPSession { get; private set; }

    public DebugEngine()
    {
      this.breakPointManager = new BreakpointManager(this);
    }

    ~DebugEngine()
    {
      if (this.ABAPSession == null)
        return;
      this.ABAPSession.TerminateProcess();
    }

    internal void ShowBreakpointErrorPopup()
    {
      if (this.breakpointErrorPopupDisplayed)
        return;
      this.breakpointErrorPopupDisplayed = true;
      MessageEvent.Send(this, (DebugThread) null, Resources.DebuggerBreakpointErrorPopup, 48U);
    }

    internal void ShowSourceOutOfDatePopup(string text)
    {
      if (this.sourceOutOfDatePopupDisplayed)
        return;
      this.sourceOutOfDatePopupDisplayed = true;
      MessageEvent.Send(this, (DebugThread) null, text == null || text == "" ? Resources.DebuggerSourceDoesNotFitToRuntimePopup : text, 48U);
    }

    private void ShowDebuggerStartInfo()
    {
      ASDebuggerUIUtils.SetStatusbarText(string.Format(Resources.DebuggerStartedForUserStatusbar, (object) this.ABAPSession.breakpointUserAlias));
      OutputStringEvent.Send(this, (DebugThread) null, string.Format(Resources.DebuggerStartedForUserOutputWindow, (object) this.ABAPSession.breakpointUserAlias));
    }

    public int Attach(IDebugProgram2[] rgpPrograms, IDebugProgramNode2[] rgpProgramNodes, uint celtPrograms, IDebugEventCallback2 pCallback, enum_ATTACH_REASON dwReason)
    {
      EngineUtils.RequireOk(rgpPrograms[0].GetProgramId(out this.programID));
      EngineCreateEvent.Send(this);
      ProgramCreateEvent.Send(this);
      this.ABAPSession.StartListener();
      this.ShowDebuggerStartInfo();
      return 0;
    }

    public int CauseBreak()
    {
      return 1;
    }

    public int ContinueFromSynchronousEvent(IDebugEvent2 pEvent)
    {
      if (pEvent is IDebugProgramDestroyEvent2)
      {
        this.callback = (IDebugEventCallback2) null;
        this.programID = Guid.Empty;
        this.ABAPSession = (ABAPDebugSession) null;
      }
      return 0;
    }

    public int CreatePendingBreakpoint(IDebugBreakpointRequest2 pBPRequest, out IDebugPendingBreakpoint2 ppPendingBP)
    {
      ppPendingBP = (IDebugPendingBreakpoint2) null;
      this.breakPointManager.CreatePendingBreakpoint(pBPRequest, out ppPendingBP);
      return 0;
    }

    public int DestroyProgram(IDebugProgram2 pProgram)
    {
      return -2147218687;
    }

    public int GetEngineId(out Guid pguidEngine)
    {
      pguidEngine = DebugConstants.DebugEngineGuid;
      return 0;
    }

    public int RemoveAllSetExceptions(ref Guid guidType)
    {
      return -2147467263;
    }

    public int RemoveSetException(EXCEPTION_INFO[] pException)
    {
      return -2147467263;
    }

    public int SetException(EXCEPTION_INFO[] pException)
    {
      return -2147467263;
    }

    public int SetLocale(ushort wLangID)
    {
      return 0;
    }

    public int SetMetric(string pszMetric, object varValue)
    {
      return -2147467263;
    }

    public int SetRegistryRoot(string pszRegistryRoot)
    {
      return 0;
    }

    public int CanTerminateProcess(IDebugProcess2 pProcess)
    {
      return 0;
    }

    public int LaunchSuspended(string pszServer, IDebugPort2 pPort, string pszExe, string pszArgs, string pszDir, string bstrEnv, string pszOptions, enum_LAUNCH_FLAGS dwLaunchFlags, uint hStdInput, uint hStdOutput, uint hStdError, IDebugEventCallback2 pCallback, out IDebugProcess2 ppProcess)
    {
      PDI_RI_S_MESSAGE[] messages;
      this.ABAPSession = ABAPDebugSession.createInstance(this, out messages);
      if (this.ABAPSession == null)
      {
        ASDebuggerUIUtils.DisplayMessagesInCopernicusMsgBox(string.Format(Resources.DebuggerStartErrorPopup, (object) EngineUtils.PDIMessagesToString(messages)));
        ppProcess = (IDebugProcess2) null;
        return -2147467259;
      }
      this.callback = pCallback;
      EngineUtils.RequireOk(pPort.GetProcess(new AD_PROCESS_ID()
      {
        ProcessIdType = 1U,
        guidProcessId = this.ABAPSession.ProcessGUID
      }, out ppProcess));
      return 0;
    }

    public int ResumeProcess(IDebugProcess2 pProcess)
    {
      if (this.callback == null)
        return -2147467259;
      Guid pguidProcessId;
      pProcess.GetProcessId(out pguidProcessId);
      if (pguidProcessId != this.ABAPSession.ProcessGUID)
        return 1;
      IDebugPort2 ppPort;
      EngineUtils.RequireOk(pProcess.GetPort(out ppPort));
      IDebugPortNotify2 ppPortNotify;
      EngineUtils.RequireOk(((IDebugDefaultPort2) ppPort).GetPortNotify(out ppPortNotify));
      AD_PROCESS_ID[] pProcessId = new AD_PROCESS_ID[1];
      EngineUtils.RequireOk(pProcess.GetPhysicalProcessId(pProcessId));
      EngineUtils.RequireOk(ppPortNotify.AddProgramNode((IDebugProgramNode2) new DebugProgramNode(pProcessId[0])));
      return 0;
    }

    private void StopTracing()
    {
      (VSPackageUtil.ServiceProvider.GetService(typeof (ITracingService)) as ITracingService).DeactivateTrace(true);
    }

    public int TerminateProcess(IDebugProcess2 pProcess)
    {
      Guid pguidProcessId;
      pProcess.GetProcessId(out pguidProcessId);
      if (pguidProcessId != this.ABAPSession.ProcessGUID)
        return 1;
      this.ABAPSession.TerminateProcess();
      return 0;
    }

    public int CanDetach()
    {
      return 1;
    }

    public int Continue(IDebugThread2 pThread)
    {
      return -2147467263;
    }

    public int Detach()
    {
      this.breakPointManager.ClearBoundBreakpoints();
      this.programID = Guid.Empty;
      return 0;
    }

    public int EnumCodeContexts(IDebugDocumentPosition2 pDocPos, out IEnumDebugCodeContexts2 ppEnum)
    {
      ppEnum = (IEnumDebugCodeContexts2) null;
      return -2147467263;
    }

    public int EnumCodePaths(string pszHint, IDebugCodeContext2 pStart, IDebugStackFrame2 pFrame, int fSource, out IEnumCodePaths2 ppEnum, out IDebugCodeContext2 ppSafety)
    {
      ppEnum = (IEnumCodePaths2) null;
      ppSafety = (IDebugCodeContext2) null;
      return -2147467263;
    }

    public int EnumModules(out IEnumDebugModules2 ppEnum)
    {
      ppEnum = (IEnumDebugModules2) null;
      return -2147467263;
    }

    public int EnumThreads(out IEnumDebugThreads2 ppEnum)
    {
      ppEnum = (IEnumDebugThreads2) new ThreadEnum((IDebugThread2[]) this.ABAPSession.m_DebugThreadList.ToArray());
      return 0;
    }

    public int ExecuteOnThread(IDebugThread2 pThread)
    {
      this.ABAPSession.fifoExecution.QueueUserWorkItem((WaitCallback) (ignored => ((DebugThread) pThread).Schedule_StepOrContinue(DebuggerOperation.CONTINUE)), (object) null, false);
      return 0;
    }

    public int GetDebugProperty(out IDebugProperty2 ppProperty)
    {
      ppProperty = (IDebugProperty2) null;
      return -2147467263;
    }

    public int GetDisassemblyStream(enum_DISASSEMBLY_STREAM_SCOPE dwScope, IDebugCodeContext2 pCodeContext, out IDebugDisassemblyStream2 ppDisassemblyStream)
    {
      ppDisassemblyStream = (IDebugDisassemblyStream2) null;
      return -2147467263;
    }

    public int GetENCUpdate(out object ppUpdate)
    {
      ppUpdate = (object) null;
      return -2147467263;
    }

    public int GetEngineInfo(out string pbstrEngine, out Guid pguidEngine)
    {
      pbstrEngine = "ABSL Debugger";
      pguidEngine = DebugConstants.DebugEngineGuid;
      return 0;
    }

    public int GetMemoryBytes(out IDebugMemoryBytes2 ppMemoryBytes)
    {
      ppMemoryBytes = (IDebugMemoryBytes2) null;
      return -2147467263;
    }

    public int GetName(out string pbstrName)
    {
      pbstrName = "ABSL Debug Engine";
      return 0;
    }

    public int GetProgramId(out Guid pguidProgramId)
    {
      pguidProgramId = this.programID;
      return !(pguidProgramId == Guid.Empty) ? 0 : -2147467259;
    }

    public int Step(IDebugThread2 pThread, enum_STEPKIND sk, enum_STEPUNIT Step)
    {
      DebuggerOperation operation = DebuggerOperation.NOT_SUPPORTED;
      switch (sk)
      {
        case enum_STEPKIND.STEP_INTO:
          operation = DebuggerOperation.STEP_INTO;
          break;
        case enum_STEPKIND.STEP_OVER:
          operation = DebuggerOperation.STEP_OVER;
          break;
        case enum_STEPKIND.STEP_OUT:
          operation = DebuggerOperation.STEP_OUT;
          break;
      }
      if (operation != DebuggerOperation.NOT_SUPPORTED)
        this.ABAPSession.fifoExecution.QueueUserWorkItem((WaitCallback) (ignored => ((DebugThread) pThread).Schedule_StepOrContinue(operation)), (object) null, false);
      return 0;
    }

    public int Terminate()
    {
      this.StopTracing();
      DebugRealtimeTrace.Instance.Stop();
      ASDebuggerUIUtils.SetStatusbarText(Resources.DebuggerStoppedStatusbar);
      OutputStringEvent.Send(this, (DebugThread) null, Resources.DebuggerStoppedOutputWindow);
      return 0;
    }

    public int WriteDump(enum_DUMPTYPE DUMPTYPE, string pszDumpUrl)
    {
      return -2147467263;
    }

    internal void Send(IDebugEvent2 eventObject, string iidEvent, IDebugProgram2 program, IDebugThread2 thread)
    {
      if (this.callback == null)
        return;
      Guid riidEvent = new Guid(iidEvent);
      uint pdwAttrib;
      eventObject.GetAttributes(out pdwAttrib);
      try
      {
        Marshal.ThrowExceptionForHR(this.callback.Event((IDebugEngine2) this, (IDebugProcess2) null, program, thread, eventObject, ref riidEvent, pdwAttrib));
      }
      catch (InvalidCastException ex)
      {
      }
    }

    internal void Send(IDebugEvent2 eventObject, string iidEvent, IDebugThread2 thread)
    {
      this.Send(eventObject, iidEvent, (IDebugProgram2) this, thread);
    }

    public int EnumPrograms(out IEnumDebugPrograms2 programs)
    {
      programs = (IEnumDebugPrograms2) null;
      return -2147467263;
    }

    public int Attach(IDebugEventCallback2 pCallback)
    {
      return -2147467263;
    }

    public int GetProcess(out IDebugProcess2 process)
    {
      process = (IDebugProcess2) null;
      return -2147467263;
    }

    public int Execute()
    {
      return -2147467263;
    }
  }
}
