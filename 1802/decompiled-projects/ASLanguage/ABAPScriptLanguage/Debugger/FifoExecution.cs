﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.FifoExecution
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System.Collections.Generic;
using System.Threading;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  public class FifoExecution
  {
    private Queue<WorkItem> _workItems = new Queue<WorkItem>();
    private bool _delegateQueuedOrRunning;

    public void QueueUserWorkItem(WaitCallback callback, object state, bool requiresAsyncExec = true)
    {
      WorkItem workItem = new WorkItem()
      {
        Callback = callback,
        State = state,
        Context = ExecutionContext.Capture()
      };
      lock (this._workItems)
      {
        if (!requiresAsyncExec && !this._delegateQueuedOrRunning)
        {
          workItem.Execute();
        }
        else
        {
          this._workItems.Enqueue(workItem);
          if (this._delegateQueuedOrRunning)
            return;
          this._delegateQueuedOrRunning = true;
          ThreadPool.UnsafeQueueUserWorkItem(new WaitCallback(this.ProcessQueuedItems), (object) null);
        }
      }
    }

    private void ProcessQueuedItems(object ignored)
    {
      while (true)
      {
        WorkItem workItem;
        lock (this._workItems)
        {
          if (this._workItems.Count == 0)
          {
            this._delegateQueuedOrRunning = false;
            break;
          }
          workItem = this._workItems.Dequeue();
        }
        try
        {
          workItem.Execute();
        }
        catch
        {
          ThreadPool.UnsafeQueueUserWorkItem(new WaitCallback(this.ProcessQueuedItems), (object) null);
          throw;
        }
      }
    }
  }
}
