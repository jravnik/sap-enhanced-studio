﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ProvideDebugEngineAttribute
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Shell;
using System;
using System.IO;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class ProvideDebugEngineAttribute : RegistrationAttribute
  {
    private readonly string _id;
    private readonly string _name;
    private readonly Type _programProvider;
    private readonly Type _debugEngine;

    public ProvideDebugEngineAttribute(string name, Type programProvider, Type debugEngine, string id)
    {
      this._name = name;
      this._programProvider = programProvider;
      this._debugEngine = debugEngine;
      this._id = id;
    }

    public override void Register(RegistrationAttribute.RegistrationContext context)
    {
      RegistrationAttribute.Key key1 = context.CreateKey("AD7Metrics\\Engine\\" + this._id);
      key1.SetValue("Name", (object) this._name);
      key1.SetValue("CLSID", (object) this._debugEngine.GUID.ToString("B"));
      if (this._programProvider != (Type) null)
        key1.SetValue("ProgramProvider", (object) this._programProvider.GUID.ToString("B"));
      key1.SetValue("Attach", (object) 0);
      key1.SetValue("AddressBP", (object) 0);
      key1.SetValue("AutoSelectPriority", (object) 6);
      key1.SetValue("CallstackBP", (object) 0);
      key1.SetValue("Exceptions", (object) 0);
      key1.SetValue("SetNextStatement", (object) 0);
      key1.SetValue("RemoteDebugging", (object) 1);
      key1.SetValue("HitCountBP", (object) 0);
      key1.SetValue("EngineClass", (object) this._debugEngine.FullName);
      key1.SetValue("EngineAssembly", (object) this._debugEngine.Assembly.FullName);
      key1.SetValue("LoadProgramProviderUnderWOW64", (object) 1);
      key1.SetValue("AlwaysLoadProgramProviderLocal", (object) 1);
      key1.SetValue("LoadUnderWOW64", (object) 1);
      using (RegistrationAttribute.Key subkey = key1.CreateSubkey("IncompatibleList"))
      {
        subkey.SetValue("guidCOMPlusNativeEng", (object) "{92EF0900-2251-11D2-B72E-0000F87572EF}");
        subkey.SetValue("guidCOMPlusOnlyEng", (object) "{449EC4CC-30D2-4032-9256-EE18EB41B62B}");
        subkey.SetValue("guidScriptEng", (object) "{F200A7E7-DEA5-11D0-B854-00A0244A1DE2}");
        subkey.SetValue("guidNativeOnlyEng", (object) "{3B476D35-A401-11D2-AAD4-00C04F990171}");
      }
      RegistrationAttribute.Key key2 = context.CreateKey("CLSID");
      RegistrationAttribute.Key subkey1 = key2.CreateSubkey(this._debugEngine.GUID.ToString("B"));
      subkey1.SetValue("Assembly", (object) this._debugEngine.Assembly.FullName);
      subkey1.SetValue("Class", (object) this._debugEngine.FullName);
      subkey1.SetValue("InprocServer32", (object) context.InprocServerPath);
      subkey1.SetValue("CodeBase", (object) Path.Combine(context.ComponentPath, this._debugEngine.Module.Name));
      subkey1.SetValue("ThreadingModel", (object) "Free");
      if (this._programProvider != (Type) null)
      {
        RegistrationAttribute.Key subkey2 = key2.CreateSubkey(this._programProvider.GUID.ToString("B"));
        subkey2.SetValue("Assembly", (object) this._programProvider.Assembly.FullName);
        subkey2.SetValue("Class", (object) this._programProvider.FullName);
        subkey2.SetValue("InprocServer32", (object) context.InprocServerPath);
        subkey2.SetValue("CodeBase", (object) Path.Combine(context.ComponentPath, this._debugEngine.Module.Name));
        subkey2.SetValue("ThreadingModel", (object) "Free");
      }
      using (RegistrationAttribute.Key key3 = context.CreateKey("ExceptionAssistant\\KnownEngines\\" + this._id))
        key3.SetValue("", (object) this._name);
    }

    public override void Unregister(RegistrationAttribute.RegistrationContext context)
    {
    }
  }
}
