﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.BreakpointErrorEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class BreakpointErrorEvent : AsynchronousEvent, IDebugBreakpointErrorEvent2
  {
    public const string IID = "ABB0CA42-F82B-4622-84E4-6903AE90F210";
    private IDebugErrorBreakpoint2 errorBreakpoint;

    public BreakpointErrorEvent(IDebugErrorBreakpoint2 errorBreakpoint)
    {
      this.errorBreakpoint = errorBreakpoint;
    }

    public static void Send(DebugEngine engine, IDebugErrorBreakpoint2 errorBreakpoint)
    {
      BreakpointErrorEvent breakpointErrorEvent = new BreakpointErrorEvent(errorBreakpoint);
      engine.Send((IDebugEvent2) breakpointErrorEvent, "ABB0CA42-F82B-4622-84E4-6903AE90F210", (IDebugThread2) null);
    }

    int IDebugBreakpointErrorEvent2.GetErrorBreakpoint(out IDebugErrorBreakpoint2 ppErrorBP)
    {
      ppErrorBP = this.errorBreakpoint;
      return 0;
    }
  }
}
