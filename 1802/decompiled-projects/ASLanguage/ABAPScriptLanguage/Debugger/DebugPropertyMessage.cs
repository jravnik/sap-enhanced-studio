﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebugPropertyMessage
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using System;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class DebugPropertyMessage : IDebugProperty2
  {
    private string text;
    private enum_DBG_ATTRIB_FLAGS attribute;
    private DEBUG_PROPERTY_INFO propertyInfo;

    public DEBUG_PROPERTY_INFO PropertyInfo
    {
      get
      {
        return this.propertyInfo;
      }
    }

    public DebugPropertyMessage(string text, enum_DBG_ATTRIB_FLAGS attribute)
    {
      this.text = text;
      this.attribute = attribute;
      this.ConstructDebugPropertyInfo();
    }

    private void ConstructDebugPropertyInfo()
    {
      this.propertyInfo = new DEBUG_PROPERTY_INFO();
      this.propertyInfo.bstrFullName = this.text;
      this.propertyInfo.dwFields |= enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_FULLNAME;
      this.propertyInfo.bstrName = this.text;
      this.propertyInfo.dwFields |= enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NAME;
      this.propertyInfo.dwAttrib = this.attribute | enum_DBG_ATTRIB_FLAGS.DBG_ATTRIB_VALUE_READONLY;
      this.propertyInfo.dwFields |= enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_ATTRIB;
    }

    int IDebugProperty2.EnumChildren(enum_DEBUGPROP_INFO_FLAGS dwFields, uint dwRadix, ref Guid guidFilter, enum_DBG_ATTRIB_FLAGS dwAttribFilter, string pszNameFilter, uint dwTimeout, out IEnumDebugPropertyInfo2 ppEnum)
    {
      ppEnum = (IEnumDebugPropertyInfo2) null;
      return 1;
    }

    int IDebugProperty2.GetDerivedMostProperty(out IDebugProperty2 ppDerivedMost)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetExtendedInfo(ref Guid guidExtendedInfo, out object pExtendedInfo)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetMemoryBytes(out IDebugMemoryBytes2 ppMemoryBytes)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetMemoryContext(out IDebugMemoryContext2 ppMemory)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetParent(out IDebugProperty2 ppParent)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetPropertyInfo(enum_DEBUGPROP_INFO_FLAGS dwFields, uint dwRadix, uint dwTimeout, IDebugReference2[] rgpArgs, uint dwArgCount, DEBUG_PROPERTY_INFO[] pPropertyInfo)
    {
      pPropertyInfo[0] = this.propertyInfo;
      rgpArgs = (IDebugReference2[]) null;
      return 0;
    }

    int IDebugProperty2.GetReference(out IDebugReference2 ppReference)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetSize(out uint pdwSize)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.SetValueAsReference(IDebugReference2[] rgpArgs, uint dwArgCount, IDebugReference2 pValue, uint dwTimeout)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.SetValueAsString(string pszValue, uint dwRadix, uint dwTimeout)
    {
      throw new NotImplementedException();
    }
  }
}
