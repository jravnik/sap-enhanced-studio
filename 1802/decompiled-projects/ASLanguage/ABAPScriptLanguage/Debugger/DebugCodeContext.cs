﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebugCodeContext
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using System;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class DebugCodeContext : IDebugCodeContext2, IDebugMemoryContext2, IDebugCodeContext100
  {
    private DebugEngine engine;
    public readonly string FileName;
    public readonly int LineNumber;
    private IDebugDocumentContext2 m_documentContext;

    internal DebugCodeContext(DebugEngine engine, string fileName, int lineNumber)
    {
      this.engine = engine;
      this.FileName = fileName;
      this.LineNumber = lineNumber;
    }

    internal void SetDocumentContext(IDebugDocumentContext2 docContext)
    {
      this.m_documentContext = docContext;
    }

    int IDebugCodeContext2.Add(ulong dwCount, out IDebugMemoryContext2 ppMemCxt)
    {
      ppMemCxt = (IDebugMemoryContext2) null;
      return -2147467263;
    }

    int IDebugCodeContext2.Compare(enum_CONTEXT_COMPARE contextCompare, IDebugMemoryContext2[] compareToItems, uint compareToLength, out uint foundIndex)
    {
      foundIndex = uint.MaxValue;
      for (uint index = 0; index < compareToLength; ++index)
      {
        DebugCodeContext compareToItem = compareToItems[(int)((UIntPtr)index)] as DebugCodeContext;
        if (compareToItem != null && object.ReferenceEquals((object) this.engine, (object) compareToItem.engine))
        {
          bool flag;
          switch (contextCompare)
          {
            case enum_CONTEXT_COMPARE.CONTEXT_EQUAL:
              flag = this.LineNumber == compareToItem.LineNumber;
              break;
            case enum_CONTEXT_COMPARE.CONTEXT_LESS_THAN:
              flag = this.LineNumber < compareToItem.LineNumber;
              break;
            case enum_CONTEXT_COMPARE.CONTEXT_GREATER_THAN:
              flag = this.LineNumber > compareToItem.LineNumber;
              break;
            case enum_CONTEXT_COMPARE.CONTEXT_LESS_THAN_OR_EQUAL:
              flag = this.LineNumber <= compareToItem.LineNumber;
              break;
            case enum_CONTEXT_COMPARE.CONTEXT_GREATER_THAN_OR_EQUAL:
              flag = this.LineNumber >= compareToItem.LineNumber;
              break;
            case enum_CONTEXT_COMPARE.CONTEXT_SAME_SCOPE:
            case enum_CONTEXT_COMPARE.CONTEXT_SAME_FUNCTION:
              flag = this.LineNumber == compareToItem.LineNumber && this.FileName == compareToItem.FileName;
              break;
            case enum_CONTEXT_COMPARE.CONTEXT_SAME_MODULE:
            case enum_CONTEXT_COMPARE.CONTEXT_SAME_PROCESS:
              flag = true;
              break;
            default:
              return -2147467263;
          }
          if (flag)
          {
            foundIndex = index;
            return 0;
          }
        }
      }
      return 1;
    }

    int IDebugCodeContext2.GetDocumentContext(out IDebugDocumentContext2 ppSrcCxt)
    {
      ppSrcCxt = this.m_documentContext;
      return 0;
    }

    int IDebugCodeContext2.GetInfo(enum_CONTEXT_INFO_FIELDS dwFields, CONTEXT_INFO[] pinfo)
    {
      try
      {
        pinfo[0].dwFields = (enum_CONTEXT_INFO_FIELDS) 0;
        if ((dwFields & enum_CONTEXT_INFO_FIELDS.CIF_ADDRESS) != (enum_CONTEXT_INFO_FIELDS) 0)
        {
          pinfo[0].bstrAddress = this.LineNumber.ToString();
          pinfo[0].dwFields |= enum_CONTEXT_INFO_FIELDS.CIF_ADDRESS;
        }
        int num1 = (int) (dwFields & enum_CONTEXT_INFO_FIELDS.CIF_ADDRESSOFFSET);
        int num2 = (int) (dwFields & enum_CONTEXT_INFO_FIELDS.CIF_ADDRESSABSOLUTE);
        int num3 = (int) (dwFields & enum_CONTEXT_INFO_FIELDS.CIF_MODULEURL);
        int num4 = (int) (dwFields & enum_CONTEXT_INFO_FIELDS.CIF_FUNCTION);
        int num5 = (int) (dwFields & enum_CONTEXT_INFO_FIELDS.CIF_FUNCTIONOFFSET);
        return 0;
      }
      catch
      {
        return 1;
      }
    }

    int IDebugCodeContext2.GetLanguageInfo(ref string pbstrLanguage, ref Guid pguidLanguage)
    {
      if (this.m_documentContext == null)
        return 1;
      this.m_documentContext.GetLanguageInfo(ref pbstrLanguage, ref pguidLanguage);
      return 0;
    }

    int IDebugCodeContext2.GetName(out string pbstrName)
    {
      throw new NotImplementedException();
    }

    int IDebugCodeContext2.Subtract(ulong dwCount, out IDebugMemoryContext2 ppMemCxt)
    {
      throw new NotImplementedException();
    }

    int IDebugMemoryContext2.Add(ulong dwCount, out IDebugMemoryContext2 ppMemCxt)
    {
      throw new NotImplementedException();
    }

    int IDebugMemoryContext2.Compare(enum_CONTEXT_COMPARE Compare, IDebugMemoryContext2[] rgpMemoryContextSet, uint dwMemoryContextSetLen, out uint pdwMemoryContext)
    {
      throw new NotImplementedException();
    }

    int IDebugMemoryContext2.GetInfo(enum_CONTEXT_INFO_FIELDS dwFields, CONTEXT_INFO[] pinfo)
    {
      throw new NotImplementedException();
    }

    int IDebugMemoryContext2.GetName(out string pbstrName)
    {
      throw new NotImplementedException();
    }

    int IDebugMemoryContext2.Subtract(ulong dwCount, out IDebugMemoryContext2 ppMemCxt)
    {
      throw new NotImplementedException();
    }

    int IDebugCodeContext100.GetProgram(out IDebugProgram2 ppProgram)
    {
      ppProgram = (IDebugProgram2) this.engine;
      return 0;
    }
  }
}
