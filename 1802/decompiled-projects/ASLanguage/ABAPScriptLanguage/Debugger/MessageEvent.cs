﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.MessageEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class MessageEvent : AsynchronousEvent, IDebugMessageEvent2
  {
    public const string IID = "3BDB28CF-DBD2-4D24-AF03-01072B67EB9E";
    private string messageString;
    private uint messageType;

    public static void Send(DebugEngine engine, DebugThread thread, string messageString, uint messageType)
    {
      MessageEvent messageEvent = new MessageEvent(messageString, messageType);
      engine.Send((IDebugEvent2) messageEvent, "3BDB28CF-DBD2-4D24-AF03-01072B67EB9E", (IDebugThread2) thread);
    }

    private MessageEvent(string messageString, uint messageType)
    {
      this.messageString = messageString;
      this.messageType = messageType;
    }

    public int GetMessage(enum_MESSAGETYPE[] pMessageType, out string pbstrMessage, out uint pdwType, out string pbstrHelpFileName, out uint pdwHelpId)
    {
      pMessageType[0] = enum_MESSAGETYPE.MT_MESSAGEBOX;
      pbstrMessage = this.messageString;
      pdwType = this.messageType;
      pbstrHelpFileName = (string) null;
      pdwHelpId = 0U;
      return 0;
    }

    public int SetResponse(uint dwResponse)
    {
      return 0;
    }
  }
}
