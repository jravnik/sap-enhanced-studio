﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ErrorEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class ErrorEvent : AsynchronousEvent, IDebugErrorEvent2
  {
    public const string IID = "FDB7A36C-8C53-41DA-A337-8BD86B14D5CB";
    private enum_MESSAGETYPE messageType;
    private string messageText;
    private uint errorType;

    public static void Send(DebugEngine engine, DebugThread thread, enum_MESSAGETYPE messageType, string messageText, uint errorType)
    {
      ErrorEvent errorEvent = new ErrorEvent(messageType, messageText, errorType);
      engine.Send((IDebugEvent2) errorEvent, "FDB7A36C-8C53-41DA-A337-8BD86B14D5CB", (IDebugThread2) thread);
    }

    int IDebugErrorEvent2.GetErrorMessage(enum_MESSAGETYPE[] pMessageType, out string pbstrErrorFormat, out int phrErrorReason, out uint pdwType, out string pbstrHelpFileName, out uint pdwHelpId)
    {
      pMessageType[0] = this.messageType;
      pbstrErrorFormat = this.messageText;
      phrErrorReason = 0;
      pdwType = this.errorType;
      pbstrHelpFileName = (string) null;
      pdwHelpId = 0U;
      return 0;
    }

    private ErrorEvent(enum_MESSAGETYPE messageType, string messageText, uint errorType)
    {
      this.messageType = messageType;
      this.messageText = messageText;
      this.errorType = errorType;
    }
  }
}
