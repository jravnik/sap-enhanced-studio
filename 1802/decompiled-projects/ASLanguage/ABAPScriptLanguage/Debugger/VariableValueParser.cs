﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.VariableValueParser
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System.Collections.Generic;
using System.Xml;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class VariableValueParser
  {
    private XmlDocument m_variableValueDoc;
    private XmlNodeReader m_reader;
    private VariableInformation m_variableInformation;

    public VariableValueParser(PDI_ABSL_S_VARIABLE variable)
    {
      this.m_variableInformation = new VariableInformation();
      this.m_variableInformation.m_name = variable.ABSL_NAME;
      this.m_variableInformation.m_typeName = variable.TYPE;
      this.m_variableValueDoc = new XmlDocument();
      this.m_variableValueDoc.LoadXml(variable.VALUE);
    }

    public VariableInformation DeserializeValue()
    {
      if (this.m_reader == null)
        this.m_reader = new XmlNodeReader((XmlNode) this.m_variableValueDoc.DocumentElement);
      if (!this.m_reader.IsStartElement("data"))
        return this.m_variableInformation;
      this.m_variableInformation = this.ParseValue(this.m_variableInformation);
      this.m_reader.Close();
      return this.m_variableInformation;
    }

    protected VariableInformation ParseValue(VariableInformation currentVariable)
    {
      this.m_reader.Read();
      switch (this.m_reader.LocalName)
      {
        case "simple":
          currentVariable.m_typeName = this.GetDescriptiveContent("type");
          currentVariable.m_value = this.GetDescriptiveContent("value");
          this.m_reader.Read();
          break;
        case "struct":
          currentVariable.m_typeName = this.GetDescriptiveContent("type");
          currentVariable.m_value = this.GetDescriptiveContent("description");
          currentVariable.children = this.GetComponents();
          if (this.m_reader.LocalName != "struct")
          {
            this.m_reader.Read();
            break;
          }
          break;
        case "table":
          currentVariable.m_typeName = this.GetDescriptiveContent("type");
          currentVariable.m_value = this.GetDescriptiveContent("description");
          currentVariable.children = this.GetLines();
          if (this.m_reader.LocalName != "table")
          {
            this.m_reader.Read();
            break;
          }
          break;
      }
      return currentVariable;
    }

    protected string GetDescriptiveContent(string tagName)
    {
      string str = "";
      this.m_reader.Read();
      if (this.m_reader.IsStartElement(tagName))
      {
        this.m_reader.Read();
        if (this.m_reader.HasValue)
        {
          str = this.m_reader.Value;
          this.m_reader.Read();
        }
      }
      return str;
    }

    protected VariableInformation[] GetComponents()
    {
      List<VariableInformation> variableInformationList = new List<VariableInformation>();
      this.m_reader.Read();
      while (this.m_reader.IsStartElement("comp"))
      {
        VariableInformation currentVariable = new VariableInformation();
        currentVariable.m_name = this.GetDescriptiveContent("name");
        this.m_reader.Read();
        VariableInformation variableInformation = this.ParseValue(currentVariable);
        variableInformationList.Add(variableInformation);
        this.m_reader.Read();
        this.m_reader.Read();
        this.m_reader.Read();
      }
      if (variableInformationList.Count > 0)
        return variableInformationList.ToArray();
      return (VariableInformation[]) null;
    }

    protected VariableInformation[] GetLines()
    {
      List<VariableInformation> variableInformationList = new List<VariableInformation>();
      this.m_reader.Read();
      while (this.m_reader.IsStartElement("line"))
      {
        VariableInformation currentVariable = new VariableInformation();
        currentVariable.m_name = this.GetDescriptiveContent("index");
        currentVariable.m_typeName = this.GetDescriptiveContent("type");
        currentVariable.m_value = this.GetDescriptiveContent("description");
        currentVariable.children = this.GetColumns();
        if (currentVariable.children == null && this.m_reader.IsStartElement("value"))
        {
          currentVariable = this.ParseValue(currentVariable);
          this.m_reader.Read();
          this.m_reader.Read();
          this.m_reader.Read();
        }
        variableInformationList.Add(currentVariable);
        this.m_reader.Read();
      }
      if (variableInformationList.Count > 0)
        return variableInformationList.ToArray();
      return (VariableInformation[]) null;
    }

    protected VariableInformation[] GetColumns()
    {
      List<VariableInformation> variableInformationList = new List<VariableInformation>();
      this.m_reader.Read();
      while (this.m_reader.IsStartElement("column"))
      {
        VariableInformation currentVariable = new VariableInformation();
        currentVariable.m_name = this.GetDescriptiveContent("name");
        if (this.m_reader.IsStartElement("value"))
          return (VariableInformation[]) null;
        this.m_reader.Read();
        VariableInformation variableInformation = this.ParseValue(currentVariable);
        variableInformationList.Add(variableInformation);
        this.m_reader.Read();
        this.m_reader.Read();
        this.m_reader.Read();
      }
      if (variableInformationList.Count > 0)
        return variableInformationList.ToArray();
      return (VariableInformation[]) null;
    }
  }
}
