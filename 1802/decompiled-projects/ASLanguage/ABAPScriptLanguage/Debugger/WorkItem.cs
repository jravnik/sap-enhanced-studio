﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.WorkItem
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System.Threading;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class WorkItem
  {
    private static ContextCallback _contextCallback = (ContextCallback) (s =>
    {
      WorkItem workItem = (WorkItem) s;
      workItem.Callback(workItem.State);
    });
    public WaitCallback Callback;
    public object State;
    public ExecutionContext Context;

    public void Execute()
    {
      if (this.Context != null)
        ExecutionContext.Run(this.Context, WorkItem._contextCallback, (object) this);
      else
        this.Callback(this.State);
    }
  }
}
