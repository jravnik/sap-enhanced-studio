﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.FrameInfoEnum
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class FrameInfoEnum : DebugEnum<FRAMEINFO, IEnumDebugFrameInfo2>, IEnumDebugFrameInfo2
  {
    public FrameInfoEnum(FRAMEINFO[] data)
      : base(data)
    {
    }

    public new int Next(uint celt, FRAMEINFO[] rgelt, ref uint celtFetched)
    {
      return base.Next(celt, rgelt, out celtFetched);
    }
  }
}
