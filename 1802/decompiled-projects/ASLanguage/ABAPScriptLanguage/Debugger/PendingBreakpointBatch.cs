﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.PendingBreakpointBatch
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System.Collections.Generic;
using System.Threading;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class PendingBreakpointBatch
  {
    private List<PendingBreakpoint> batchList;
    private FifoExecution fifoExecution;
    private MassPendingBreakpointOperation massPendingBreakpointOperation;

    internal PendingBreakpointBatch(FifoExecution fifoExecution, MassPendingBreakpointOperation massABSLBreakpointOperation)
    {
      this.fifoExecution = fifoExecution;
      this.massPendingBreakpointOperation = massABSLBreakpointOperation;
      this.batchList = new List<PendingBreakpoint>();
    }

    private void StartProcessing()
    {
      this.fifoExecution.QueueUserWorkItem((WaitCallback) (state =>
      {
        Thread.Sleep(50);
        lock (this.batchList)
        {
          this.massPendingBreakpointOperation(this.batchList);
          this.batchList.Clear();
        }
      }), (object) null, true);
    }

    internal bool Add(PendingBreakpoint pendingBreakpoint)
    {
      if (!Monitor.TryEnter((object) this.batchList))
        return false;
      try
      {
        bool flag = this.batchList.Count == 0;
        this.batchList.Add(pendingBreakpoint);
        if (flag)
          this.StartProcessing();
      }
      finally
      {
        Monitor.Exit((object) this.batchList);
      }
      return true;
    }

    internal PendingBreakpointBatch CopyAndAdd(PendingBreakpoint abslBP)
    {
      PendingBreakpointBatch pendingBreakpointBatch = new PendingBreakpointBatch(this.fifoExecution, this.massPendingBreakpointOperation);
      pendingBreakpointBatch.Add(abslBP);
      return pendingBreakpointBatch;
    }
  }
}
