﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ExpressionEvaluationCompleteEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class ExpressionEvaluationCompleteEvent : AsynchronousEvent, IDebugExpressionEvaluationCompleteEvent2
  {
    public const string IID = "C0E13A85-238A-4800-8315-D947C960A843";
    private readonly IDebugExpression2 expression;
    private readonly IDebugProperty2 property;

    public static void Send(DebugEngine engine, DebugThread thread, IDebugExpression2 expression, IDebugProperty2 property)
    {
      ExpressionEvaluationCompleteEvent evaluationCompleteEvent = new ExpressionEvaluationCompleteEvent(expression, property);
      engine.Send((IDebugEvent2) evaluationCompleteEvent, "C0E13A85-238A-4800-8315-D947C960A843", (IDebugThread2) thread);
    }

    private ExpressionEvaluationCompleteEvent(IDebugExpression2 expression, IDebugProperty2 property)
    {
      this.expression = expression;
      this.property = property;
    }

    public int GetExpression(out IDebugExpression2 ppExpr)
    {
      ppExpr = this.expression;
      return 0;
    }

    public int GetResult(out IDebugProperty2 ppResult)
    {
      ppResult = this.property;
      return 0;
    }
  }
}
