﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.EngineCreateEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class EngineCreateEvent : AsynchronousEvent, IDebugEngineCreateEvent2
  {
    public const string IID = "FE5B734C-759D-4E59-AB04-F103343BDD06";
    private IDebugEngine2 engine;

    private EngineCreateEvent(DebugEngine engine)
    {
      this.engine = (IDebugEngine2) engine;
    }

    public static void Send(DebugEngine engine)
    {
      EngineCreateEvent engineCreateEvent = new EngineCreateEvent(engine);
      engine.Send((IDebugEvent2) engineCreateEvent, "FE5B734C-759D-4E59-AB04-F103343BDD06", (IDebugProgram2) null, (IDebugThread2) null);
    }

    int IDebugEngineCreateEvent2.GetEngine(out IDebugEngine2 engine)
    {
      engine = this.engine;
      return 0;
    }
  }
}
