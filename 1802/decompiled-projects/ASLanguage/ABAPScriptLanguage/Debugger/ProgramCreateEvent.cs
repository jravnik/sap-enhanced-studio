﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ProgramCreateEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class ProgramCreateEvent : AsynchronousEvent, IDebugProgramCreateEvent2
  {
    public const string IID = "96CD11EE-ECD4-4E89-957E-B5D496FC4139";

    internal static void Send(DebugEngine engine)
    {
      ProgramCreateEvent programCreateEvent = new ProgramCreateEvent();
      engine.Send((IDebugEvent2) programCreateEvent, "96CD11EE-ECD4-4E89-957E-B5D496FC4139", (IDebugThread2) null);
    }
  }
}
