﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ProvideDebugLanguageAttribute
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Shell;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class ProvideDebugLanguageAttribute : RegistrationAttribute
  {
    private readonly string _guid;
    private readonly string _languageName;
    private readonly string _engineGuid;

    public ProvideDebugLanguageAttribute(string languageName, string guid, string engineGuid)
    {
      this._languageName = languageName;
      this._guid = guid;
      this._engineGuid = engineGuid;
    }

    public override void Register(RegistrationAttribute.RegistrationContext context)
    {
      context.CreateKey("Languages\\Language Services\\" + this._languageName + "\\Debugger Languages\\" + this._guid).SetValue("", (object) this._languageName);
      RegistrationAttribute.Key key = context.CreateKey("AD7Metrics\\ExpressionEvaluator\\" + this._guid + "\\{994B45C4-E6E9-11D2-903F-00C04FA302A1}\\Engine");
      key.SetValue("Language", (object) this._languageName);
      key.SetValue("Name", (object) this._languageName);
      key.SetValue("Engine", (object) this._engineGuid);
    }

    public override void Unregister(RegistrationAttribute.RegistrationContext context)
    {
    }
  }
}
