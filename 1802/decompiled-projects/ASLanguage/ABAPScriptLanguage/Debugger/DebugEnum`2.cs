﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebugEnum`2
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class DebugEnum<T, I> where I : class
  {
    private readonly T[] m_data;
    private uint m_position;

    public DebugEnum(T[] data)
    {
      this.m_data = data;
      this.m_position = 0U;
    }

    public int Clone(out I ppEnum)
    {
      ppEnum = default (I);
      return -2147467263;
    }

    public int GetCount(out uint pcelt)
    {
      pcelt = (uint) this.m_data.Length;
      return 0;
    }

    public int Next(uint celt, T[] rgelt, out uint celtFetched)
    {
      return this.Move(celt, rgelt, out celtFetched);
    }

    public int Reset()
    {
      lock (this)
      {
        this.m_position = 0U;
        return 0;
      }
    }

    public int Skip(uint celt)
    {
      uint celtFetched;
      return this.Move(celt, (T[]) null, out celtFetched);
    }

    private int Move(uint celt, T[] rgelt, out uint celtFetched)
    {
      lock (this)
      {
        int num = 0;
        celtFetched = (uint) this.m_data.Length - this.m_position;
        if (celt > celtFetched)
          num = 1;
        else if (celt < celtFetched)
          celtFetched = celt;
        if (rgelt != null)
        {
          for (int index = 0; (long) index < (long) celtFetched; ++index)
            rgelt[index] = this.m_data[(long) this.m_position + (long) index];
        }
        this.m_position += celtFetched;
        return num;
      }
    }
  }
}
