﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ErrorBreakpointResolution
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class ErrorBreakpointResolution : IDebugErrorBreakpointResolution2
  {
    private DebugEngine engine;
    private DebugDocumentContext documentContext;
    private enum_BP_ERROR_TYPE errorType;
    private string errorMessage;
    private int lineNumber;
    private string filename;

    public ErrorBreakpointResolution(DebugEngine engine, DebugDocumentContext documentContext, enum_BP_ERROR_TYPE errorType, string errorMessage, string filename, int lineNumber)
    {
      this.engine = engine;
      this.documentContext = documentContext;
      this.errorType = errorType;
      this.errorMessage = errorMessage;
      this.lineNumber = lineNumber;
      this.filename = filename;
    }

    int IDebugErrorBreakpointResolution2.GetBreakpointType(enum_BP_TYPE[] pBPType)
    {
      pBPType[0] = enum_BP_TYPE.BPT_CODE;
      return 0;
    }

    int IDebugErrorBreakpointResolution2.GetResolutionInfo(enum_BPERESI_FIELDS dwFields, BP_ERROR_RESOLUTION_INFO[] pErrorResolutionInfo)
    {
      if ((dwFields & enum_BPERESI_FIELDS.BPERESI_BPRESLOCATION) != ~enum_BPERESI_FIELDS.BPERESI_ALLFIELDS)
      {
        BP_RESOLUTION_LOCATION resolutionLocation = new BP_RESOLUTION_LOCATION();
        resolutionLocation.bpType = 1U;
        DebugCodeContext debugCodeContext = new DebugCodeContext(this.engine, this.filename, this.lineNumber);
        debugCodeContext.SetDocumentContext((IDebugDocumentContext2) this.documentContext);
        resolutionLocation.unionmember1 = Marshal.GetComInterfaceForObject((object) debugCodeContext, typeof (IDebugCodeContext2));
        pErrorResolutionInfo[0].bpResLocation = resolutionLocation;
        pErrorResolutionInfo[0].dwFields |= enum_BPERESI_FIELDS.BPERESI_BPRESLOCATION;
      }
      if ((dwFields & enum_BPERESI_FIELDS.BPERESI_PROGRAM) != ~enum_BPERESI_FIELDS.BPERESI_ALLFIELDS)
      {
        pErrorResolutionInfo[0].pProgram = (IDebugProgram2) this.engine;
        pErrorResolutionInfo[0].dwFields |= enum_BPERESI_FIELDS.BPERESI_PROGRAM;
      }
      if ((dwFields & enum_BPERESI_FIELDS.BPERESI_TYPE) != ~enum_BPERESI_FIELDS.BPERESI_ALLFIELDS)
      {
        pErrorResolutionInfo[0].dwType = this.errorType;
        pErrorResolutionInfo[0].dwFields |= enum_BPERESI_FIELDS.BPERESI_TYPE;
      }
      if ((dwFields & enum_BPERESI_FIELDS.BPERESI_MESSAGE) != ~enum_BPERESI_FIELDS.BPERESI_ALLFIELDS)
      {
        pErrorResolutionInfo[0].bstrMessage = this.errorMessage;
        pErrorResolutionInfo[0].dwFields |= enum_BPERESI_FIELDS.BPERESI_MESSAGE;
      }
      return 0;
    }
  }
}
