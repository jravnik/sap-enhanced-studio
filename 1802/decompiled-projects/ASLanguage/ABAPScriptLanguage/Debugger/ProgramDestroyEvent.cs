﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ProgramDestroyEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class ProgramDestroyEvent : SynchronousEvent, IDebugProgramDestroyEvent2
  {
    public const string IID = "E147E9E3-6440-4073-A7B7-A65592C714B5";
    private readonly uint m_exitCode;

    public ProgramDestroyEvent(uint exitCode)
    {
      this.m_exitCode = exitCode;
    }

    public static void Send(DebugEngine engine, uint exitCode)
    {
      ProgramDestroyEvent programDestroyEvent = new ProgramDestroyEvent(exitCode);
      engine.Send((IDebugEvent2) programDestroyEvent, "E147E9E3-6440-4073-A7B7-A65592C714B5", (IDebugThread2) null);
    }

    int IDebugProgramDestroyEvent2.GetExitCode(out uint exitCode)
    {
      exitCode = this.m_exitCode;
      return 0;
    }
  }
}
