﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebugConstants
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  public static class DebugConstants
  {
    public static Guid DebugEngineGuid = new Guid("{A80A2A43-9839-46C5-8CFF-8A039F8F047B}");
    public static Guid LanguageGuid = new Guid("012D9923-96A3-4ee2-8CB7-670C478DC358");
    public static readonly Guid guidFilterLocals = new Guid("b200f725-e725-4c53-b36a-1ec27aef12ef");
    public static readonly Guid guidFilterAllLocals = new Guid("196db21f-5f22-45a9-b5a3-32cddb30db06");
    public static readonly Guid guidFilterLocalsPlusArgs = new Guid("e74721bb-10c0-40f5-807f-920d37f95419");
    public static readonly Guid guidFilterAllLocalsPlusArgs = new Guid("939729a8-4cb0-4647-9831-7ff465240d5f");
    public const int E_PROGRAM_DESTROY_PENDING = -2147218687;
    public const string DebugEngineGuidString = "{A80A2A43-9839-46C5-8CFF-8A039F8F047B}";
    public const string DebugEngineName = "ABSL Debugger";
    public const string LanguageName = "ABSL";
    public const int MaximumNumberOfParallelDebugSessions = 1;
    public const int DebuggerBackendBatchWaitTime = 50;
    public const string SentenceDelimiter = ".!?:;,";
  }
}
