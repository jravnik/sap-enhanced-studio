﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebugDocumentContext
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using System;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class DebugDocumentContext : IDebugDocumentContext2
  {
    private string m_fileName;
    private TEXT_POSITION m_begPos;
    private TEXT_POSITION m_endPos;
    private DebugCodeContext m_codeContext;

    public DebugDocumentContext(string fileName, TEXT_POSITION begPos, TEXT_POSITION endPos, DebugCodeContext codeContext)
    {
      this.m_fileName = fileName;
      this.m_begPos = begPos;
      this.m_endPos = endPos;
      this.m_codeContext = codeContext;
    }

    int IDebugDocumentContext2.Compare(enum_DOCCONTEXT_COMPARE Compare, IDebugDocumentContext2[] rgpDocContextSet, uint dwDocContextSetLen, out uint pdwDocContext)
    {
      dwDocContextSetLen = 0U;
      pdwDocContext = 0U;
      return -2147467263;
    }

    int IDebugDocumentContext2.EnumCodeContexts(out IEnumDebugCodeContexts2 ppEnumCodeCxts)
    {
      throw new NotImplementedException();
    }

    int IDebugDocumentContext2.GetDocument(out IDebugDocument2 ppDocument)
    {
      ppDocument = (IDebugDocument2) null;
      return -2147467259;
    }

    int IDebugDocumentContext2.GetLanguageInfo(ref string pbstrLanguage, ref Guid pguidLanguage)
    {
      pbstrLanguage = "ABSL";
      pguidLanguage = DebugConstants.LanguageGuid;
      return 0;
    }

    int IDebugDocumentContext2.GetName(enum_GETNAME_TYPE gnType, out string pbstrFileName)
    {
      pbstrFileName = this.m_fileName;
      return 0;
    }

    int IDebugDocumentContext2.GetSourceRange(TEXT_POSITION[] pBegPosition, TEXT_POSITION[] pEndPosition)
    {
      throw new NotImplementedException();
    }

    int IDebugDocumentContext2.GetStatementRange(TEXT_POSITION[] pBegPosition, TEXT_POSITION[] pEndPosition)
    {
      pBegPosition[0].dwColumn = this.m_begPos.dwColumn;
      pBegPosition[0].dwLine = this.m_begPos.dwLine;
      pEndPosition[0].dwColumn = this.m_endPos.dwColumn;
      pEndPosition[0].dwLine = this.m_endPos.dwLine;
      return 0;
    }

    int IDebugDocumentContext2.Seek(int nCount, out IDebugDocumentContext2 ppDocContext)
    {
      ppDocContext = (IDebugDocumentContext2) null;
      return -2147467263;
    }
  }
}
