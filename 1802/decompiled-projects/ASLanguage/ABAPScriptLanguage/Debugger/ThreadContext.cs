﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ThreadContext
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class ThreadContext
  {
    internal readonly string documentName;
    internal readonly TextSpan span;
    internal readonly FrameInfoEnum frameInfoEnum;

    internal ThreadContext(DebugEngine engine, DebugThread thread, string documentName, TextSpan span, PDI_ABSL_S_VARIABLE[] variables, PDI_ABSL_S_DBG_STACK_LEVEL[] callstack)
    {
      this.documentName = documentName;
      this.span = span;
      FRAMEINFO[] frameinfoArray;
      FRAMEINFO[] data = frameinfoArray = new FRAMEINFO[callstack.Length];
      for (int index = 0; index < callstack.Length; ++index)
      {
        StackFrame stackFrame = index != 0 ? new StackFrame(engine, thread, callstack[index]) : new StackFrame(engine, thread, callstack[index], variables);
        data[index] = stackFrame.FrameInfo;
      }
      this.frameInfoEnum = new FrameInfoEnum(data);
    }
  }
}
