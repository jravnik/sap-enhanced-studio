﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebugExpression
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Threading;
using System.Xml;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class DebugExpression : IDebugExpression2
  {
    private DebugProperty debugProperty;
    private string expression;
    private DebugEngine engine;
    private DebugThread thread;

    public DebugExpression(DebugEngine engine, DebugThread thread, string expression)
    {
      this.engine = engine;
      this.thread = thread;
      this.expression = expression;
    }

    public void AddInterpretedResult(PDI_ABSL_S_VARIABLE interpretedResult)
    {
      VariableInformation vi;
      try
      {
        vi = new VariableValueParser(interpretedResult).DeserializeValue();
      }
      catch (XmlException ex)
      {
        vi = new VariableInformation();
        vi.m_name = interpretedResult.ABSL_NAME;
        vi.m_typeName = interpretedResult.TYPE;
        vi.m_value = interpretedResult.VALUE;
      }
      this.debugProperty = new DebugProperty(vi);
    }

    int IDebugExpression2.Abort()
    {
      throw new NotImplementedException();
    }

    int IDebugExpression2.EvaluateAsync(enum_EVALFLAGS dwFlags, IDebugEventCallback2 pExprCallback)
    {
      throw new NotImplementedException();
    }

    int IDebugExpression2.EvaluateSync(enum_EVALFLAGS dwFlags, uint dwTimeout, IDebugEventCallback2 pExprCallback, out IDebugProperty2 ppResult)
    {
      if (this.debugProperty != null)
      {
        ppResult = (IDebugProperty2) this.debugProperty;
        return 0;
      }
      ManualResetEvent dataAvailableEvent = new ManualResetEvent(false);
      this.engine.ABAPSession.fifoExecution.QueueUserWorkItem((WaitCallback) (ignored => this.thread.Schedule_InterpretExpression(this.expression, this, dataAvailableEvent)), (object) null, false);
      if (dataAvailableEvent.WaitOne(5000) && this.debugProperty != null)
      {
        ppResult = (IDebugProperty2) this.debugProperty;
        return 0;
      }
      ppResult = (IDebugProperty2) null;
      return -2147467259;
    }
  }
}
