﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ThreadCreateEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class ThreadCreateEvent : AsynchronousEvent, IDebugThreadCreateEvent2
  {
    public const string IID = "2090CCFC-70C5-491D-A5E8-BAD2DD9EE3EA";

    public static void Send(DebugEngine engine, DebugThread debugThread)
    {
      ThreadCreateEvent threadCreateEvent = new ThreadCreateEvent();
      engine.Send((IDebugEvent2) threadCreateEvent, "2090CCFC-70C5-491D-A5E8-BAD2DD9EE3EA", (IDebugThread2) debugThread);
    }
  }
}
