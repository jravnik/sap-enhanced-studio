﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ABSLErrorBreakpoint
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class ABSLErrorBreakpoint : IDebugErrorBreakpoint2
  {
    private DebugEngine engine;
    private PendingBreakpoint pendingBreakpoint;
    private ErrorBreakpointResolution errorBreakpointResolution;

    public ABSLErrorBreakpoint(DebugEngine engine, PendingBreakpoint pendingBreakpoint, ErrorBreakpointResolution errorBreakpointResolution)
    {
      this.engine = engine;
      this.pendingBreakpoint = pendingBreakpoint;
      this.errorBreakpointResolution = errorBreakpointResolution;
    }

    public int GetBreakpointResolution(out IDebugErrorBreakpointResolution2 ppErrorResolution)
    {
      ppErrorResolution = (IDebugErrorBreakpointResolution2) this.errorBreakpointResolution;
      return 0;
    }

    public int GetPendingBreakpoint(out IDebugPendingBreakpoint2 ppPendingBreakpoint)
    {
      ppPendingBreakpoint = (IDebugPendingBreakpoint2) this.pendingBreakpoint;
      return 0;
    }
  }
}
