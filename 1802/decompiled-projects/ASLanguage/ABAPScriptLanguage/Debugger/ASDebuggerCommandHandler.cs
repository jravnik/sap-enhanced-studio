﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ASDebuggerCommandHandler
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis;
using SAP.Copernicus.ABAPScriptLanguage.Tracing;
using SAP.Copernicus.ASLanguage;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Tracing;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  public class ASDebuggerCommandHandler
  {
    private static bool firstDebuggerStart = true;
    private IServiceProvider serviceProvider;
    private Package package;
    private IVsDebugger3 ivsDbg3;
    private Debugger2 dteDbg2;
    private ITracingService tracingService;
    private DTE2 dte2;

    private IVsDebugger3 IVsDbg3
    {
      get
      {
        if (this.ivsDbg3 == null)
          this.ivsDbg3 = ASLanguagePackage.GetLanguageService().GetIVsDebugger() as IVsDebugger3;
        return this.ivsDbg3;
      }
    }

    private Debugger2 DTEDbg2
    {
      get
      {
        if (this.dteDbg2 == null)
          this.dteDbg2 = this.DTE2.Debugger as Debugger2;
        return this.dteDbg2;
      }
    }

    private ITracingService TracingService
    {
      get
      {
        if (this.tracingService == null)
          this.tracingService = this.serviceProvider.GetService(typeof (ITracingService)) as ITracingService;
        return this.tracingService;
      }
    }

    private DTE2 DTE2
    {
      get
      {
        if (this.dte2 == null)
          this.dte2 = (DTE2) this.serviceProvider.GetService(typeof (SDTE));
        return this.dte2;
      }
    }

    public ASDebuggerCommandHandler(Package package)
    {
      this.serviceProvider = (IServiceProvider) package;
      this.package = package;
    }

    private void OpenDebuggerWindows()
    {
      if (!ASDebuggerCommandHandler.firstDebuggerStart)
        return;
      ASDebuggerCommandHandler.firstDebuggerStart = false;
      this.DTE2.ExecuteCommand("View.SolutionExplorer", "");
      this.DTE2.ExecuteCommand("Debug.Output", "");
      this.DTE2.ExecuteCommand("Debug.Locals", "");
      this.DTE2.ExecuteCommand("Debug.CallStack", "");
      this.DTE2.ExecuteCommand("Debug.Breakpoints", "");
    }

    public void StartOrContinue(object sender, EventArgs e)
    {
      if (this.DTEDbg2.CurrentMode == dbgDebugMode.dbgDesignMode)
      {
        if (!this.SaveRefreshActivateSolution())
          return;
        VsDebugTargetInfo3 debugTargetInfo3 = new VsDebugTargetInfo3();
        debugTargetInfo3.guidLaunchDebugEngine = DebugConstants.DebugEngineGuid;
        debugTargetInfo3.bstrExe = "TODO.exe";
        debugTargetInfo3.dlo = 1U;
        debugTargetInfo3.LaunchFlags = 8U;
        VsDebugTargetProcessInfo[] pLaunchResults = new VsDebugTargetProcessInfo[1];
        if (this.IVsDbg3.LaunchDebugTargets3(1U, new VsDebugTargetInfo3[1]
        {
          debugTargetInfo3
        }, pLaunchResults) < 0)
          return;
        this.TracingService.ActivateTrace(true);
        DebugRealtimeTrace.Instance.Start();
        this.OpenDebuggerWindows();
      }
      else
      {
        if (this.DTEDbg2.CurrentMode != dbgDebugMode.dbgBreakMode)
          return;
        this.DTEDbg2.Go(false);
      }
    }

    public void StartOrContinue_BeforeQueryStatus(object sender, EventArgs e)
    {
      OleMenuCommand oleMenuCommand = sender as OleMenuCommand;
      Solution2 solution = this.DTE2.Solution as Solution2;
      bool flag = solution != null && solution.IsOpen;
      oleMenuCommand.Enabled = flag && (this.DTEDbg2.CurrentMode == dbgDebugMode.dbgDesignMode && !this.TracingService.IsActive || this.DTEDbg2.CurrentMode == dbgDebugMode.dbgBreakMode);
      if (this.DTEDbg2.CurrentMode == dbgDebugMode.dbgDesignMode)
        oleMenuCommand.Text = "&Start Debugging";
      else
        oleMenuCommand.Text = "&Continue";
    }

    public void StepOver(object sender, EventArgs e)
    {
      this.DTEDbg2.StepOver(false);
    }

    public void StepOver_BeforeQueryStatus(object sender, EventArgs e)
    {
      (sender as OleMenuCommand).Enabled = this.DTEDbg2.CurrentMode == dbgDebugMode.dbgBreakMode;
    }

    private bool SaveRefreshActivateSolution()
    {
      Connection instance1 = Connection.getInstance();
      string openSolutionStatus = XRepMapper.GetInstance().GetOpenSolutionStatus();
      XRepMapper.EditModes solutionEditStatus = XRepMapper.GetInstance().GetSolutionEditStatus();
      if (instance1.SystemMode == SystemMode.Productive)
      {
        Trace.WriteLine(string.Format("Debugger: productive system"));
        return true;
      }
      if ((instance1.UsageMode == UsageMode.OneOff || instance1.UsageMode == UsageMode.miltiCustomer) && openSolutionStatus != "In Development" || instance1.UsageMode == UsageMode.Scalable && solutionEditStatus == XRepMapper.EditModes.Locked)
      {
        Trace.WriteLine(string.Format("Debugger: solution not editable"));
        return true;
      }
      switch (DecisionDialogBox.CheckDecision(SAP.Copernicus.ABAPScriptLanguage.Resources.DebuggerSaveRefreshActivate, this.DTE2.Name, SAP.Copernicus.ABAPScriptLanguage.Resources.DebuggerDecisionAlwaySaveRefreshActivate, "ActivateBeforeDebugging"))
      {
        case DialogResult.Cancel:
          return false;
        case DialogResult.No:
          return true;
        default:
          if (this.DTE2 != null)
          {
            bool flag = false;
            foreach (Document document in this.DTE2.Documents)
            {
              if (!document.Saved)
              {
                flag = true;
                break;
              }
            }
            if (flag)
            {
              DateTime now = DateTime.Now;
              this.DTE2.Documents.SaveAll();
              Trace.WriteLine(string.Format("Debugger: Saving unsaved files before start took {0} ms.", (object) (DateTime.Now - now)));
            }
          }
          DateTime now1 = DateTime.Now;
          XRepMapper instance2 = XRepMapper.GetInstance();
          instance2.RefreshSolution(false);
          Trace.WriteLine(string.Format("Debugger: Refreshing solution before start took {0} ms.", (object) (DateTime.Now - now1)));
          string openSolutionName = ProjectUtil.GetOpenSolutionName();
          if (string.IsNullOrEmpty(openSolutionName))
            return true;
          DateTime now2 = DateTime.Now;
          bool flag1 = instance2.ActivateSolution(openSolutionName, true, char.MinValue, false);
          CopernicusFileNodeProperties.clearBuffer();
          if (this.DTE2 != null)
          {
            foreach (EnvDTE.Project project in this.DTE2.Solution.Projects)
            {
              CopernicusProjectNode copernicusProjectNode = project.Object as CopernicusProjectNode;
              if (copernicusProjectNode != null)
              {
                copernicusProjectNode.refreshProperties();
                copernicusProjectNode.RedrawIconsOfHierarchy();
              }
            }
          }
          Trace.WriteLine(string.Format("Debugger: Activating solution before start took {0} ms.", (object) (DateTime.Now - now2)));
          return flag1;
      }
    }

    public void DumpAnalysis(object sender, EventArgs e)
    {
      ToolWindowPane toolWindow = this.package.FindToolWindow(typeof (DumpAnalysisWindow), 0, true);
      if (toolWindow != null)
      {
        object frame = toolWindow.Frame;
      }
      ErrorHandler.ThrowOnFailure(((IVsWindowFrame) toolWindow.Frame).Show());
    }
  }
}
