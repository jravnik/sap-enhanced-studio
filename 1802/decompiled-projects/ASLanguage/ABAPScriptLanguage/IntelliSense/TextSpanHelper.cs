﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.TextSpanHelper
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.TextManager.Interop;
using System.Collections.Generic;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  internal class TextSpanHelper
  {
    private static IComparer<TextSpan> compareByStart;

    public static IComparer<TextSpan> CompareByStart
    {
      get
      {
        if (TextSpanHelper.compareByStart == null)
          TextSpanHelper.compareByStart = (IComparer<TextSpan>) new TextSpanHelper.TextSpanHelper_CompareByStart();
        return TextSpanHelper.compareByStart;
      }
    }

    public static bool Contains(TextSpan textSpan, int line, int index)
    {
      if (textSpan.iStartLine >= line && (textSpan.iStartLine != line || textSpan.iStartIndex > index))
        return false;
      if (line < textSpan.iEndLine)
        return true;
      if (line == textSpan.iEndLine)
        return index < textSpan.iEndIndex;
      return false;
    }

    private class TextSpanHelper_CompareByStart : IComparer<TextSpan>
    {
      public int Compare(TextSpan x, TextSpan y)
      {
        if (x.iStartLine < y.iStartLine)
          return -1;
        if (x.iStartLine > y.iStartLine)
          return 1;
        return x.iStartIndex - y.iStartIndex;
      }
    }
  }
}
