﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.ASMethods
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Package;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  public class ASMethods : Methods
  {
    public List<MethodDef> methods = new List<MethodDef>();

    public int CurrentParamIndex { set; get; }

    public int MethodTokenIndex { private set; get; }

    public ASMethods(int currentParamIndex, int methodTokenIndex)
    {
      this.CurrentParamIndex = currentParamIndex;
      this.MethodTokenIndex = methodTokenIndex;
    }

    public override int GetCount()
    {
      return this.methods.Count;
    }

    public override string GetName(int index)
    {
      if (index >= this.GetCount())
        throw new ArgumentException("Wrong index requested in Method.GetName.");
      return this.methods[index].Name;
    }

    public override string GetDescription(int index)
    {
      if (index >= this.GetCount())
        throw new ArgumentException("Wrong index requested in Method.GetDescription.");
      return this.methods[index].Description;
    }

    public override string GetType(int index)
    {
      if (index >= this.GetCount())
        throw new ArgumentException("Wrong index requested in Method.GetType.");
      return this.methods[index].ResultType;
    }

    public override int GetParameterCount(int index)
    {
      if (index >= this.GetCount())
        throw new ArgumentException("Wrong index requested in Method.GetParameterCount.");
      return this.methods[index].Params.Count;
    }

    public override void GetParameterInfo(int index, int parameter, out string name, out string display, out string description)
    {
      if (index >= this.GetCount())
        throw new ArgumentException("Wrong method index requested in Method.GetParameterInfo.");
      MethodDef method = this.methods[index];
      if (parameter < 0 || parameter >= method.Params.Count)
        throw new ArgumentException("Wrong method parameter index requested in Method.GetParameterInfo for method " + (object) index + ".");
      name = method.Params[parameter].Name;
      display = method.Params[parameter].DisplayName;
      description = method.Params[parameter].Description;
    }
  }
}
