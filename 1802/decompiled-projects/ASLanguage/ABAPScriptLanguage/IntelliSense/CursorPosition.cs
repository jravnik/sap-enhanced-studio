﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.CursorPosition
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Antlr.Runtime;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  public class CursorPosition
  {
    public int Line;
    public int Column;

    public CursorPosition(int line, int col)
    {
      this.Line = line;
      this.Column = col;
    }

    public RelativeCursorLocation DetermineLocationRelativeToToken(IToken token)
    {
      if (token == null)
        return RelativeCursorLocation.Unspecified;
      if (this.Line < token.Line || this.Line == token.Line && this.Column < token.CharPositionInLine + 1)
        return RelativeCursorLocation.Before;
      if (this.Line == token.Line && this.Column == token.CharPositionInLine + 1)
        return RelativeCursorLocation.RightBefore;
      if (this.Line == token.Line && this.Column > token.CharPositionInLine + 1 && this.Column < token.CharPositionInLine + token.Text.Length + 1)
        return RelativeCursorLocation.Within;
      if (this.Line == token.Line && this.Column == token.CharPositionInLine + token.Text.Length + 1)
        return RelativeCursorLocation.RightBehind;
      return this.Line == token.Line && this.Column == token.CharPositionInLine + token.Text.Length + 2 ? RelativeCursorLocation.OneCharBehind : RelativeCursorLocation.MoreThanOneCharBehind;
    }

    public bool IsWithinToken(IToken token, bool includingBehind = true)
    {
      RelativeCursorLocation locationRelativeToToken = this.DetermineLocationRelativeToToken(token);
      if (includingBehind && locationRelativeToToken == RelativeCursorLocation.RightBehind)
        return true;
      return locationRelativeToToken == RelativeCursorLocation.Within;
    }

    public string GetPartialToken(IToken token, bool withinCheck = false)
    {
      if (withinCheck && !this.IsWithinToken(token, true))
        return string.Empty;
      int length = this.Column - token.CharPositionInLine - 1;
      return token.Text.Substring(0, length);
    }
  }
}
