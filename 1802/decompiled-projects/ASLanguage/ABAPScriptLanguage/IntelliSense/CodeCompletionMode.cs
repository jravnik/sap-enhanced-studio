﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.CodeCompletionMode
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using SAP.Copernicus.ABAPScriptLanguage.Env;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  public enum CodeCompletionMode
  {
    [StringValue("N")] None,
    [StringValue("P")] Path,
    [StringValue("T")] TopLevel,
    [StringValue("I")] Import,
    [StringValue("M")] MethodTip,
  }
}
