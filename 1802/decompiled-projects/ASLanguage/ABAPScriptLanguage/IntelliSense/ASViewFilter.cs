﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.ASViewFilter
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.ASLanguage;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  internal class ASViewFilter : ViewFilter
  {
    private IVsTextView view;
    private IVsDebugger debugger;

    private IVsDebugger Debugger
    {
      get
      {
        if (this.debugger == null)
          this.debugger = ASLanguagePackage.GetLanguageService().GetIVsDebugger();
        return this.debugger;
      }
    }

    public ASViewFilter(CodeWindowManager mgr, IVsTextView view)
      : base(mgr, view)
    {
      this.view = view;
    }

    public override int GetFullDataTipText(string textValue, TextSpan ts, out string fullTipText)
    {
      fullTipText = textValue;
      try
      {
        IVsTextLines ppBuffer = (IVsTextLines) null;
        this.view.GetBuffer(out ppBuffer);
        if (ppBuffer != null)
        {
          if (this.Debugger != null)
          {
            DBGMODE[] pdbgmode = new DBGMODE[1];
            if (this.Debugger.GetMode(pdbgmode) == 0)
            {
              if (pdbgmode[0] == DBGMODE.DBGMODE_Break)
              {
                int dataTipValue = this.Debugger.GetDataTipValue(ppBuffer, new TextSpan[1]
                {
                  ts
                }, (string) null, out fullTipText);
                if (dataTipValue == 282625)
                  return dataTipValue;
              }
            }
          }
        }
      }
      catch (COMException ex)
      {
        Trace.WriteLine("COMException: GetDataTipValue, errorCode=" + (object) ex.ErrorCode);
      }
      return 0;
    }
  }
}
