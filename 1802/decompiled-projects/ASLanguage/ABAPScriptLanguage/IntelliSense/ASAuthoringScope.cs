﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.ASAuthoringScope
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.TextManager.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  public class ASAuthoringScope : AuthoringScope
  {
    private ASDeclarations declarationsList;
    private Methods Methods;
    private Source source;
    private TextSpan pathSpan;
    private TextSpan identifierSpan;
    private bool spansProvided;

    public ASAuthoringScope()
    {
    }

    public ASAuthoringScope(Source source)
    {
      this.source = source;
    }

    public void AddDataTipSpans(TextSpan pathSpan, TextSpan identifierSpan)
    {
      this.spansProvided = true;
      this.pathSpan = pathSpan;
      this.identifierSpan = identifierSpan;
    }

    public override string GetDataTipText(int line, int col, out TextSpan span)
    {
      if (this.spansProvided)
      {
        span = this.pathSpan;
        return this.source.GetText(this.pathSpan);
      }
      span = new TextSpan()
      {
        iStartLine = line,
        iStartIndex = col,
        iEndLine = line,
        iEndIndex = col
      };
      return "";
    }

    public void CreateDeclarationList()
    {
      if (this.declarationsList != null)
        return;
      this.declarationsList = new ASDeclarations();
    }

    public void AddDeclaration(ASDeclaration declaration)
    {
      if (declaration == null)
        return;
      if (this.declarationsList == null)
        this.declarationsList = new ASDeclarations();
      this.declarationsList.AddDeclaration(declaration);
    }

    public override Declarations GetDeclarations(IVsTextView view, int line, int col, TokenInfo info, ParseReason reason)
    {
      this.CreateDeclarationList();
      return (Declarations) this.declarationsList;
    }

    public void SetMethods(Methods methods)
    {
      this.Methods = methods;
    }

    public ASMethods GetMethods()
    {
      return this.Methods as ASMethods;
    }

    public override Methods GetMethods(int line, int col, string name)
    {
      return this.Methods;
    }

    public override string Goto(VSConstants.VSStd97CmdID cmd, IVsTextView textView, int line, int col, out TextSpan span)
    {
      span = new TextSpan();
      return (string) null;
    }
  }
}
