﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.ASSource
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.ABAPScriptLanguage.Formatter;
using SAP.Copernicus.ABAPScriptLanguage.IntelliSense;
using System;
using System.Diagnostics;
using System.Reflection;

namespace SAP.Copernicus.ABAPScriptLanguage
{
  public class ASSource : Source
  {
    private LanguagePreferences preferences;

    public string TranslatedABAPSource { get; set; }

    public ASSource(ASLanguageService languageService, IVsTextLines textLines, Colorizer colorizer)
      : base((LanguageService) languageService, textLines, colorizer)
    {
      this.preferences = languageService.Preferences;
    }

    public override void MethodTip(IVsTextView textView, int line, int index, TokenInfo info)
    {
      this.BeginParse(line, index, (TokenInfo) new ASTokenInfo(info)
      {
        IsCompletorActive = this.IsCompletorActive
      }, ParseReason.MethodTip, textView, new ParseResultHandler(this.HandleMethodTipResponse));
    }

    internal new void HandleMethodTipResponse(ParseRequest request)
    {
      try
      {
        FieldInfo field = this.GetType().BaseType.GetField("methodData", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        MethodInfo method = field.FieldType.GetMethod("Refresh");
        Methods methods = request.Scope.GetMethods(0, 0, (string) null);
        if (methods == null)
        {
          this.DismissCompletor();
        }
        else
        {
          int currentParamIndex = ((ASMethods) methods).CurrentParamIndex;
          object[] parameters = new object[4]
          {
            (object) request.View,
            (object) methods,
            (object) currentParamIndex,
            (object) new TextSpan()
          };
          method.Invoke(field.GetValue((object) this), parameters);
        }
      }
      catch (Exception ex)
      {
        Trace.TraceError("HandleMethodTipResponse: " + ex.ToString());
      }
    }

    public override TextSpan UncommentLines(TextSpan span, string lineComment)
    {
      int length = lineComment.Length;
      for (int iStartLine = span.iStartLine; iStartLine <= span.iEndLine; ++iStartLine)
      {
        int nonWhitespaceChar = this.ScanToNonWhitespaceChar(iStartLine);
        string line = this.GetLine(iStartLine);
        if (line.Length >= nonWhitespaceChar + length && line.Substring(nonWhitespaceChar, length) == lineComment)
        {
          this.SetText(iStartLine, nonWhitespaceChar, iStartLine, nonWhitespaceChar + length, "");
          if (iStartLine == span.iStartLine && span.iStartIndex != 0)
            span.iStartIndex = nonWhitespaceChar;
        }
      }
      span.iStartIndex = 0;
      return span;
    }

    public override void ReformatSpan(EditArray mgr, TextSpan span)
    {
      new SourceFormatter().Format(this.GetText(), mgr, span, this.preferences);
    }
  }
}
