﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Tracing.DebugRealtimeTrace
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.ABAPScriptLanguage.Debugger;
using SAP.Copernicus.Core.Protocol.JSON;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace SAP.Copernicus.ABAPScriptLanguage.Tracing
{
  internal class DebugRealtimeTrace
  {
    public static readonly DebugRealtimeTrace Instance = new DebugRealtimeTrace();
    private Dictionary<string, int> traceDict = new Dictionary<string, int>();
    private object lockToken = new object();
    private const int TIMERPERIOD = 5000;
    private string startDate;
    private string startTime;
    private string traceUser;
    private PDI_ABSL_DBG_TRC_LOG_S[] traceLog;
    private Timer timer;
    private IVsOutputWindowPane pane;

    private IVsOutputWindowPane Pane
    {
      get
      {
        if (this.pane == null)
        {
          IVsOutputWindow service = VSPackageUtil.ServiceProvider.GetService(typeof (SVsOutputWindow)) as IVsOutputWindow;
          Guid outWindowDebugPane = VSConstants.GUID_OutWindowDebugPane;
          service.GetPane(ref outWindowDebugPane, out this.pane);
        }
        return this.pane;
      }
    }

    private DebugRealtimeTrace()
    {
    }

    private void TimerCallback(object _automaticCall)
    {
      bool flag1 = (bool) _automaticCall;
      JSONClient jsonClient1 = Client.getInstance().getJSONClient(false);
      PDI_ABSL_DBG_TRACE pdiAbslDbgTrace1 = new PDI_ABSL_DBG_TRACE();
      pdiAbslDbgTrace1.Importing = new PDI_ABSL_DBG_TRACE.ImportingType();
      pdiAbslDbgTrace1.Importing.IV_UNAME = this.traceUser;
      pdiAbslDbgTrace1.Importing.IV_START_DATE = this.startDate;
      pdiAbslDbgTrace1.Importing.IV_START_TIME = this.startTime;
      pdiAbslDbgTrace1.Importing.IT_TRACE_LOG = this.traceLog;
      try
      {
        JSONClient jsonClient2 = jsonClient1;
        bool flag2 = true;
        PDI_ABSL_DBG_TRACE pdiAbslDbgTrace2 = pdiAbslDbgTrace1;
        int num1 = flag2 ? 1 : 0;
        int num2 = 0;
        int num3 = 0;
        jsonClient2.callFunctionModule((SAPFunctionModule) pdiAbslDbgTrace2, num1 != 0, num2 != 0, num3 != 0);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        pdiAbslDbgTrace1.Exporting = (PDI_ABSL_DBG_TRACE.ExportingType) null;
      }
      if (pdiAbslDbgTrace1.Exporting != null && pdiAbslDbgTrace1.Exporting.EV_SUCCESS != "")
      {
        this.traceLog = pdiAbslDbgTrace1.Exporting.ET_TRACE_LOG;
        foreach (PDI_ABSL_DBG_TRC_S pdiAbslDbgTrcS in pdiAbslDbgTrace1.Exporting.ET_TRACE)
        {
          int num;
          if (!this.traceDict.TryGetValue(pdiAbslDbgTrcS.ID, out num))
          {
            num = this.traceDict.Keys.Count + 1;
            this.traceDict.Add(pdiAbslDbgTrcS.ID, num);
          }
          this.WriteToDebugOutputWindow(string.Format("{4} [{0}:{1}] {2,-11} {3}\n", (object) num, (object) pdiAbslDbgTrcS.SEVERITY, (object) pdiAbslDbgTrcS.EVENT_TYPE, (object) pdiAbslDbgTrcS.DESCRIPTION, (object) DateTime.Now.ToLongTimeString()));
        }
        if (!flag1)
          return;
        this.Restart();
      }
      else
      {
        this.WriteToDebugOutputWindow(Resources.DebuggerTracingConnectionLostOutputWindow);
        lock (this.lockToken)
        {
          if (this.timer == null)
            return;
          this.timer.Dispose();
          this.timer = (Timer) null;
        }
      }
    }

    public void Start()
    {
      this.traceDict.Clear();
      this.startDate = ABAPDebugSession.InitInfo.TRACE_START_DATE;
      this.startTime = ABAPDebugSession.InitInfo.TRACE_START_TIME;
      this.traceUser = ABAPDebugSession.InitInfo.BREAKPOINT_USER;
      this.traceLog = new PDI_ABSL_DBG_TRC_LOG_S[0];
      lock (this.lockToken)
        this.timer = new Timer(new TimerCallback(this.TimerCallback), (object) true, 5000, -1);
    }

    public void Stop()
    {
      ManualResetEvent manualResetEvent;
      lock (this.lockToken)
      {
        if (this.timer == null)
          return;
        manualResetEvent = new ManualResetEvent(false);
        this.timer.Dispose((WaitHandle) manualResetEvent);
        this.timer = (Timer) null;
      }
      manualResetEvent.WaitOne();
      this.TimerCallback((object) false);
    }

    private void Restart()
    {
      lock (this.lockToken)
      {
        if (this.timer == null)
          return;
        this.timer.Change(5000, -1);
      }
    }

    private void WriteToDebugOutputWindow(string text)
    {
      if (this.Pane == null)
        return;
      this.Pane.OutputStringThreadSafe(text);
    }
  }
}
