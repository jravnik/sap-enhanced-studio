﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.ErrorList.Error
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.Diagnostics;

namespace SAP.Copernicus.ABAPScriptLanguage.ErrorList
{
  public class Error : IComparable<Error>
  {
    public string message;
    public int line;
    public int column;
    public int length;
    public Severity severity;

    internal Error(string message, int line, int col, int len)
    {
      this.message = message;
      this.line = line;
      this.column = col;
      this.length = len;
      this.severity = Severity.Error;
    }

    internal Error(string message, int line, int col, int len, Severity severity)
      : this(message, line, col, len)
    {
      this.severity = severity;
    }

    public bool IsWarning
    {
      get
      {
        return this.severity == Severity.Warning;
      }
    }

    int IComparable<Error>.CompareTo(Error other)
    {
      if (this.line < other.line)
        return -1;
      if (this.line > other.line)
        return 1;
      if (this.column < other.column)
        return -1;
      return this.column > other.column ? 1 : 0;
    }

    public bool Equals(Error other)
    {
      if (other != null && this.line == other.line)
        return this.column == other.column;
      return false;
    }

    public TextSpan TextSpan
    {
      get
      {
        return new TextSpan()
        {
          iStartLine = this.line,
          iEndLine = this.line,
          iStartIndex = this.column,
          iEndIndex = this.column + this.length
        };
      }
    }

    public void ToTrace()
    {
      Trace.TraceError(this.ToString());
    }

    public override string ToString()
    {
      return "ANTLR " + (object) this.severity + ": " + this.message + " line-" + (object) this.line + " col- " + (object) this.column + " len- " + (object) this.length;
    }
  }
}
