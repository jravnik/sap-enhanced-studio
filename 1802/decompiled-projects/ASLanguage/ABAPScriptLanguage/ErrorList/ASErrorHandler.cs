﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.ErrorList.ASErrorHandler
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Package;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SAP.Copernicus.ABAPScriptLanguage.ErrorList
{
  public class ASErrorHandler
  {
    private readonly List<Error> Items = new List<Error>();

    public IList<Error> getSortedItemList()
    {
      this.Items.Sort();
      return (IList<Error>) this.Items.AsReadOnly();
    }

    public int ItemCount
    {
      get
      {
        return this.Items.Count;
      }
    }

    public bool HasErrors
    {
      get
      {
        foreach (Error error in this.Items)
        {
          if (error.severity == Severity.Error)
            return true;
        }
        return false;
      }
    }

    public void AddError(Exception ex)
    {
      this.AddError(ex.Message, 0, 0, 0, Severity.Error);
    }

    public void AddError(string message)
    {
      this.AddError(message, 0, 0, 0);
    }

    public void AddError(string message, int line, int col, int len)
    {
      this.AddError(message, line, col, len, Severity.Error);
    }

    public void AddError(string message, int line, int col, int len, Severity severity)
    {
      this.Items.Add(new Error(message, line, col, len, severity));
    }

    public void ToTrace()
    {
      if (this.ItemCount <= 0)
        return;
      Trace.TraceError(this.ToString());
    }

    public override string ToString()
    {
      string str = "";
      foreach (Error error in this.Items)
        str = str + error.ToString() + "\n";
      return str;
    }
  }
}
