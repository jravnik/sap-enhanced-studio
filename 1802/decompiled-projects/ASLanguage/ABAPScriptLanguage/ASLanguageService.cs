﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.ASLanguageService
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Antlr.Runtime;
using Antlr.Runtime.Tree;
using EnvDTE;
using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.ABAPScriptLanguage.Classification;
using SAP.Copernicus.ABAPScriptLanguage.Env;
using SAP.Copernicus.ABAPScriptLanguage.ErrorList;
using SAP.Copernicus.ABAPScriptLanguage.IntelliSense;
using SAP.Copernicus.ABAPScriptLanguage.JSON;
using SAP.Copernicus.ASUtil;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.ResourceAccess.TestMocks;
using SAP.Copernicus.Core.Scripting;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace SAP.Copernicus.ABAPScriptLanguage
{
    public class ASLanguageService : LanguageService
    {
        private HelpContextProvider helpContextProvider = HelpContextProvider.Create();
        private LanguagePreferences preferences;
        private ASScanner scanner;
        private ASMethods MethodDefCache;

        public ASCompiler Compiler { get; private set; }

        public ASLanguageService()
        {
            this.Compiler = new ASCompiler();
        }

        public new void SetSite(object site)
        {
            if (site != null)
                base.SetSite(site);
            if (!(site is IServiceProvider))
            {
                string message = "ASLanguageService was sited with a non service procider object: " + site;
                Trace.TraceError(message);
                throw new Exception(message);
            }
        }

        public override string Name
        {
            get
            {
                return ScriptLanguageType.ByDScript.ToString();
            }
        }

        public override string GetFormatFilterList()
        {
            return ScriptLanguageType.ByDScript.ToString() + " File (*" + ScriptLanguageType.ByDScript.FileExtension + ")\n*" + ScriptLanguageType.ByDScript.FileExtension;
        }

        public override LanguagePreferences GetLanguagePreferences()
        {
            if (this.preferences == null)
            {
                this.preferences = new LanguagePreferences(this.Site, typeof(ASLanguageService).GUID, this.Name);
                this.preferences.Init();
            }
            return this.preferences;
        }

        public override Source CreateSource(IVsTextLines buffer)
        {
            return (Source)new ASSource(this, buffer, this.GetColorizer(buffer));
        }

        public override IScanner GetScanner(IVsTextLines buffer)
        {
            if (this.scanner == null)
                this.scanner = new ASScanner(buffer);
            return (IScanner)this.scanner;
        }

        public override void OnIdle(bool periodic)
        {
            Source source = this.GetSource(this.LastActiveTextView);
            if (source != null && source.LastParseTime >= 524287)
                source.LastParseTime = 0;
            base.OnIdle(periodic);
        }

        public override ImageList GetImageList()
        {
            ImageList imageList = new ImageList();
            imageList.Images.AddRange(EntityImageList.Images);
            return imageList;
        }

        public override void UpdateLanguageContext(LanguageContextHint hint, IVsTextLines buffer, TextSpan[] ptsSelection, IVsUserContext context)
        {
            if (hint != LanguageContextHint.LCH_F1_HELP)
                return;
            string helpId = this.helpContextProvider.GetHelpID(buffer, ptsSelection);
            context.RemoveAttribute("keyword", (string)null);
            context.AddAttribute(VSUSERCONTEXTATTRIBUTEUSAGE.VSUC_Usage_LookupF1, "keyword", helpId);
        }

        public override ViewFilter CreateViewFilter(CodeWindowManager mgr, IVsTextView newView)
        {
            return (ViewFilter)new ASViewFilter(mgr, newView);
        }

        private bool BreakpointAlreadyExists(IVsTextLines textLines, int line, int col)
        {
            int piLength;
            textLines.GetLengthOfLine(line, out piLength);
            IVsEnumLineMarkers ppEnum;
            textLines.EnumMarkers(line, col, line, piLength, 0, 2U, out ppEnum);
            if (ppEnum == null)
                return false;
            IVsTextLineMarker ppRetval;
            while (ppEnum.Next(out ppRetval) == 0)
            {
                int piMarkerType;
                if (ppRetval.GetType(out piMarkerType) == 0 && (piMarkerType == 62 || piMarkerType == 55 || piMarkerType == 58))
                    return true;
            }
            return false;
        }

        private bool BreakpointsAllowedInSnippetType(string filePath)
        {
            ScriptFileMetadata metadataForScriptFile = ScriptHandler.GetMetadataForScriptFile(filePath);
            if (metadataForScriptFile == null)
                return false;
            SnippetType? type = metadataForScriptFile.Type;
            if (type.GetValueOrDefault() == SnippetType.FMTExtension)
                return !type.HasValue;
            return true;
        }

        public override int ValidateBreakpointLocation(IVsTextBuffer buffer, int line, int col, TextSpan[] pCodeSpan)
        {
            if (pCodeSpan == null || buffer == null || !(buffer is IVsTextLines))
                return 1;
            Source source = this.GetSource(buffer as IVsTextLines);
            if (source == null || !this.BreakpointsAllowedInSnippetType(source.GetFilePath()))
                return 1;
            if (TaggingCache.Instance.GetBreakpointSpan(source.GetFilePath(), line + 1, ref pCodeSpan[0]))
            {
                --pCodeSpan[0].iStartLine;
                --pCodeSpan[0].iEndLine;
                return 0;
            }
            if (!this.BreakpointAlreadyExists((IVsTextLines)buffer, line, col))
                return 1;
            pCodeSpan[0].iStartLine = line;
            pCodeSpan[0].iStartIndex = col;
            pCodeSpan[0].iEndLine = line;
            pCodeSpan[0].iEndIndex = col;
            return 0;
        }

        public override int GetColorableItem(int index, out IVsColorableItem item)
        {
            item = (IVsColorableItem)null;
            return 0;
        }

        public override int GetItemCount(out int count)
        {
            count = 0;
            return 0;
        }

        public void OnScriptFileSave(string filePath, string[] dependentFilePaths, string content, ProjectProperties properties)
        {
            ASErrorHandler errorHandler = (ASErrorHandler)null;
            TimeSpan timeSpan = new TimeSpan();
            if (ScriptLanguageType.FromFileExtension(filePath) != ScriptLanguageType.ByDScript)
                return;
            DateTime now = DateTime.Now;
            ASCompiler compiler = this.Compiler;
            bool flag = true;
            string filePath1 = filePath;
            string asSource = content;
            int num1 = 0;
            int num2 = flag ? 1 : 0;
            compiler.Compile(filePath1, asSource, num1 != 0, out errorHandler, num2 != 0);
            Trace.TraceInformation("ByD Script checked on save in: " + (object)(DateTime.Now - now));
        }

        public void OnScriptFileDeletion(string filePath)
        {
            ErrorSink.Instance.ClearTasksFor(filePath, Origin.Unknown);
            try
            {
                ScriptFileMetadata metadataForScriptFile = ScriptHandler.GetMetadataForScriptFile(filePath);
                SnippetType? type1 = metadataForScriptFile.Type;
                if ((type1.GetValueOrDefault() != SnippetType.BODetermination ? 0 : (type1.HasValue ? 1 : 0)) == 0)
                {
                    SnippetType? type2 = metadataForScriptFile.Type;
                    if ((type2.GetValueOrDefault() != SnippetType.BOValidation ? 0 : (type2.HasValue ? 1 : 0)) == 0)
                    {
                        SnippetType? type3 = metadataForScriptFile.Type;
                        if ((type3.GetValueOrDefault() != SnippetType.BOAction ? 0 : (type3.HasValue ? 1 : 0)) == 0)
                            return;
                    }
                }
                ProjectItem projectItemForFile = ProjectUtil.GetProjectItemForFile(filePath);
                if (projectItemForFile == null)
                    return;
                HierarchyNode parent = projectItemForFile.Object as HierarchyNode;
                if (parent != null)
                    parent = parent.Parent;
                if (parent != null)
                    parent = parent.Parent;
                if (parent == null || !parent.Url.EndsWith(".bo") && !parent.Url.EndsWith(".xbo"))
                    return;
                XRepHandler.Instance.UpdateContentStatus(new string[1]
                {
          XRepMapper.GetInstance().GetXrepPathforLocalFile(parent.Url)
                }, "U");
            }
            catch (Exception ex)
            {
            }
        }

        public override AuthoringScope ParseSource(ParseRequest req)
        {
            AuthoringScope authoringScope = (AuthoringScope)new ASAuthoringScope();
            DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement(string.Format("Starting parse request '{0}' for: {1}", (object)req.Reason, (object)req.FileName));
            switch (req.Reason)
            {
                case ParseReason.MemberSelect:
                case ParseReason.MemberSelectAndHighlightBraces:
                case ParseReason.CompleteWord:
                case ParseReason.DisplayMemberList:
                case ParseReason.MethodTip:
                    authoringScope = this.BackendCodeCompletion(req);
                    break;
                case ParseReason.Check:
                    if (Connection.getInstance().SystemMode == SystemMode.Development)
                    {
                        ASErrorHandler errorHandler = (ASErrorHandler)null;
                        this.Compiler.Compile(req.FileName, req.Text, false, out errorHandler, false);
                        break;
                    }
                    break;
                case ParseReason.QuickInfo:
                    if (this.IsDebugging)
                    {
                        authoringScope = this.QuickInfo(req);
                        break;
                    }
                    break;
            }
            SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Parse request finished in: ");
            return authoringScope;
        }

        private void PrepareMethodTipCompletionContext(CodeCompletionContext completionContext, ParseRequest req, CursorPosition cursor)
        {
            completionContext.CCMode = CodeCompletionMode.MethodTip;
            IToken tokenBeforeCursor = TokenStreamHelper.GetLastTokenBeforeCursor(req.Text, cursor);
            if (tokenBeforeCursor != null)
            {
                if (tokenBeforeCursor.Type == 64)
                    completionContext.InsertIdentifier = true;
            }
            try
            {
                CommonTree tree = new ByDScriptingLanguageParser(new ByDScriptingLanguageLexer((ICharStream)new ANTLRStringStream(req.Text)), completionContext).program().Tree;
            }
            catch (CodeCompletionMatchException ex)
            {
            }
            catch (Exception ex)
            {
                Trace.TraceError("Frontend parser run for code completion failed. Reason: " + (object)ex);
            }
        }

        private AuthoringScope QuickInfo(ParseRequest req)
        {
            ASAuthoringScope asAuthoringScope = new ASAuthoringScope(this.GetSource(req.FileName));
            int outLine;
            int outCol;
            SourceHelper.RecalculateTextPosition(req.View, req.Line, req.Col, out outLine, out outCol);
            CodeCompletionContext ccCtx = new CodeCompletionContext(new CursorPosition(outLine, outCol), CodeCompletionContextMode.Tooltip);
            try
            {
                CommonTree tree = new ByDScriptingLanguageParser(new ByDScriptingLanguageLexer((ICharStream)new ANTLRStringStream(req.Text)), ccCtx).program().Tree;
            }
            catch (Exception ex)
            {
                Trace.TraceError("Frontend parser run for tooltip span determination failed. Reason: " + (object)ex);
                return (AuthoringScope)asAuthoringScope;
            }
            TextSpan pathSpan;
            TextSpan identifierSpan;
            if (ccCtx.GetTooltipSpans(out pathSpan, out identifierSpan))
                asAuthoringScope.AddDataTipSpans(pathSpan, identifierSpan);
            return (AuthoringScope)asAuthoringScope;
        }

        private AuthoringScope BackendCodeCompletion(ParseRequest req)
        {
            int outLine;
            int outCol;
            SourceHelper.RecalculateTextPosition(req.View, req.Line, req.Col, out outLine, out outCol);
            CursorPosition cursor = new CursorPosition(outLine + 1, outCol + 1);
            CodeCompletionContext completionContext = new CodeCompletionContext(cursor, CodeCompletionContextMode.CodeCompletion);
            ASAuthoringScope scope = new ASAuthoringScope();
            if (req.Reason == ParseReason.MethodTip)
            {
                if (!(req.TokenInfo as ASTokenInfo).IsCompletorActive)
                {
                    this.PrepareMethodTipCompletionContext(completionContext, req, cursor);
                }
                else
                {
                    if (this.MethodDefCache != null)
                    {
                        int relativeToTokenPos = TokenStreamHelper.GetParamIndexRelativeToTokenPos(this.MethodDefCache.MethodTokenIndex, req.Text, cursor);
                        if (relativeToTokenPos >= 0)
                        {
                            string message = "Using cached method data. ParamIndex: " + (object)relativeToTokenPos;
                            this.MethodDefCache.CurrentParamIndex = relativeToTokenPos;
                            Trace.WriteLine(message);
                            scope.SetMethods((Methods)this.MethodDefCache);
                            return (AuthoringScope)scope;
                        }
                    }
                    this.PrepareMethodTipCompletionContext(completionContext, req, cursor);
                }
            }
            else
            {
                this.DeriveCodeCompletionContextFromParser(completionContext, req.Text);
                if (TestController.TestMode)
                    TestController.TestResultStorage = req.FileName.Contains("Keyword") ? completionContext.ToTestResultString() : completionContext.ToString();
            }
            Trace.WriteLine("Invalidating cached method data.");
            scope.CreateDeclarationList();
            if (completionContext.CCMode != CodeCompletionMode.None)
            {
                string asSource = SourceHelper.AddDelimAtCursor(completionContext, req, false);
                new BackendCompilerHandler().CodeCompletion(req.FileName, asSource, scope, completionContext);
                if (req.Reason == ParseReason.MethodTip)
                    this.MethodDefCache = scope.GetMethods();
            }
            if (completionContext.AllowedTokenTypes.Length > 0)
            {
                foreach (ASDeclaration keywordDeclaration in TokenHelper.GetAllowedKeywordDeclarations(completionContext))
                    scope.AddDeclaration(keywordDeclaration);
            }
            return (AuthoringScope)scope;
        }

        private void DeriveCodeCompletionContextFromParser(CodeCompletionContext codeCompletionContext, string source)
        {
            try
            {
                CommonTree tree = new ByDScriptingLanguageParser(new ByDScriptingLanguageLexer((ICharStream)new ANTLRStringStream(source)), codeCompletionContext).program().Tree;
            }
            catch (CodeCompletionMatchException ex)
            {
            }
            catch (Exception ex)
            {
                Trace.TraceError("Frontend parser run for code completion failed. Reason: " + (object)ex);
            }
        }
    }
}
