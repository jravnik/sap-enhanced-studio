﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.ASCompiler
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using SAP.Copernicus.ABAPScriptLanguage.ErrorList;
using SAP.Copernicus.ABAPScriptLanguage.JSON;
using SAP.Copernicus.Core.ErrorList;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.ABAPScriptLanguage
{
  public class ASCompiler
  {
    private static BackendCompilerHandler BackendCompilerHandler = new BackendCompilerHandler();

    internal void Compile(string filePath, string asSource, bool compileToABAP, out ASErrorHandler errorHandler, bool updateUsageIndex = false)
    {
      ASCompiler.BackendCompilerHandler.Compile(filePath, asSource, compileToABAP, out errorHandler, updateUsageIndex);
      IEnumerable<Task> boCompilerErrors = this.ASErrorListToBOCompilerErrors(errorHandler, filePath);
      if (boCompilerErrors.Count<Task>() == 0)
        ErrorSink.Instance.ClearTasksFor(filePath, Origin.GenericEditorCheck);
      else
        ErrorSink.Instance.AddTasks(boCompilerErrors);
    }

    private IEnumerable<Task> ASErrorListToBOCompilerErrors(ASErrorHandler errorHandler, string filePath)
    {
      List<Task> taskList = new List<Task>();
      errorHandler.ToTrace();
      foreach (Error sortedItem in (IEnumerable<Error>) errorHandler.getSortedItemList())
        taskList.Add(new Task(sortedItem.message, filePath, sortedItem.line, sortedItem.column, sortedItem.length, Origin.GenericEditorCheck, sortedItem.severity == Microsoft.VisualStudio.Package.Severity.Error ? SAP.Copernicus.Core.ErrorList.Severity.Error : SAP.Copernicus.Core.ErrorList.Severity.Warning, (EventHandler) null));
      return (IEnumerable<Task>) taskList;
    }
  }
}
