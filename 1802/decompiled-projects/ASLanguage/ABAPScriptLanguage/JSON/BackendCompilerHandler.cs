﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.JSON.BackendCompilerHandler
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using Microsoft.VisualStudio.Package;
using SAP.Copernicus.ABAPScriptLanguage.Env;
using SAP.Copernicus.ABAPScriptLanguage.ErrorList;
using SAP.Copernicus.ABAPScriptLanguage.IntelliSense;
using SAP.Copernicus.ASUtil;
using SAP.Copernicus.Core.Protocol.JSON;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.ResourceAccess.TestMocks;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace SAP.Copernicus.ABAPScriptLanguage.JSON
{
  public class BackendCompilerHandler : JSONHandler
  {
    public void Compile(string filePath, string asSource, bool compileToABAP, out ASErrorHandler errorHandler, bool updateUsageIndex = false)
    {
      errorHandler = new ASErrorHandler();
      ScriptFileMetadata metadataForScriptFile = ScriptHandler.GetMetadataForScriptFile(filePath);
      List<SyntaxErrorInfo> syntaxErrorInfoList = new List<SyntaxErrorInfo>();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_ABSL_COMPILE pdiAbslCompile = new PDI_ABSL_COMPILE();
        pdiAbslCompile.Importing = new PDI_ABSL_COMPILE.ImportingType();
        pdiAbslCompile.Importing.IT_XREP_FILES = new string[1]
        {
          XRepMapper.GetInstance().GetXrepPathforLocalFile(filePath)
        };
        pdiAbslCompile.Importing.IV_COMPILE_TO_ABAP = compileToABAP ? "X" : " ";
        pdiAbslCompile.Importing.IV_MDRS_INACTIVE = "X";
        if (asSource != null)
          pdiAbslCompile.Importing.IV_ABSL_SOURCE = Convert.ToBase64String(Encoding.UTF8.GetBytes(asSource));
        if (string.IsNullOrEmpty(metadataForScriptFile.ProjectNamespace))
          pdiAbslCompile.Importing.IV_MDRS_NAMESPACE = metadataForScriptFile.Namespace;
        else
          pdiAbslCompile.Importing.IV_MDRS_NAMESPACE = metadataForScriptFile.ProjectNamespace;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiAbslCompile, false, false, false);
        foreach (PDI_RI_S_MESSAGE pdiRiSMessage in pdiAbslCompile.Exporting.ET_MESSAGES)
          errorHandler.AddError(pdiRiSMessage.TEXT);
        foreach (PDI_S_COMPILER_MESSAGE sCompilerMessage in pdiAbslCompile.Exporting.ET_COMPILER_MESSAGES)
          errorHandler.AddError(sCompilerMessage.TEXT, (int) Convert.ToInt16(sCompilerMessage.LINE_NO) - 1, (int) Convert.ToInt16(sCompilerMessage.COLUMN_NO) - 1, (int) Convert.ToInt16(sCompilerMessage.LENGTH), this.MapABAPToClientSeverity(sCompilerMessage.SEVERITY));
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
    }

    public void CodeCompletion(string filePath, string asSource, ASAuthoringScope scope, CodeCompletionContext completionContext)
    {
      Trace.WriteLine("CodeCompletion: " + ("Triggering backend code completion with " + (object) completionContext));
      if (TestController.TestMode)
        return;
      try
      {
        ScriptFileMetadata metadataForScriptFile = ScriptHandler.GetMetadataForScriptFile(filePath);
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_ABSL_CODE_COMPLETION abslCodeCompletion = new PDI_ABSL_CODE_COMPLETION();
        abslCodeCompletion.Importing = new PDI_ABSL_CODE_COMPLETION.ImportingType();
        abslCodeCompletion.Importing.IV_MDRS_INACTIVE = "X";
        if (asSource != null)
          abslCodeCompletion.Importing.IV_ABSL_SOURCE = Convert.ToBase64String(Encoding.UTF8.GetBytes(asSource));
        if (string.IsNullOrEmpty(metadataForScriptFile.ProjectNamespace))
          abslCodeCompletion.Importing.IV_MDRS_NAMESPACE = metadataForScriptFile.Namespace;
        else
          abslCodeCompletion.Importing.IV_MDRS_NAMESPACE = metadataForScriptFile.ProjectNamespace;
        abslCodeCompletion.Importing.IV_XREP_FILE_PATH = XRepMapper.GetInstance().GetXrepPathforLocalFile(filePath);
        abslCodeCompletion.Importing.IS_ABSL_CODECOMPL_PARAM = new PDI_S_ABSL_CODECOMPL_PARAMS();
        abslCodeCompletion.Importing.IS_ABSL_CODECOMPL_PARAM.CC_MODE = completionContext.CCMode.GetStringValue();
        abslCodeCompletion.Importing.IS_ABSL_CODECOMPL_PARAM.CURSOR_LINE = completionContext.Cursor.Line;
        abslCodeCompletion.Importing.IS_ABSL_CODECOMPL_PARAM.CURSOR_COLUMN = completionContext.Cursor.Column;
        abslCodeCompletion.Importing.IS_ABSL_CODECOMPL_PARAM.PREFIX = completionContext.PartialTokenText;
        bool flag = false;
        if (!flag)
        {
          jsonClient.callFunctionModule((SAPFunctionModule) abslCodeCompletion, false, false, false);
          this.reportServerSideProtocolException((SAPFunctionModule) abslCodeCompletion, false, false, false);
        }
        if (completionContext.CCMode == CodeCompletionMode.MethodTip)
        {
          PDI_S_ABSL_METH_TIP_DATA methodData = (PDI_S_ABSL_METH_TIP_DATA) null;
          if (flag)
            this.FakeMethodTipResult(ref methodData);
          else
            methodData = abslCodeCompletion.Exporting.ES_METHOD_TIP;
          if (methodData.METHODS == null || methodData.METHODS.Length <= 0)
            return;
          ASMethods asMethods = new ASMethods(methodData.PARAMETER_IDX, methodData.METHOD_TOKEN_IDX);
          foreach (PDI_S_ABSL_METH_TIP pdiSAbslMethTip in methodData.METHODS)
          {
            MethodDef methodDef = new MethodDef(pdiSAbslMethTip.METHOD_NAME, pdiSAbslMethTip.DESCRIPTION, pdiSAbslMethTip.RESULT_TYPE);
            if (pdiSAbslMethTip.PARAMETERS != null)
            {
              foreach (PDI_S_ABSL_METH_TIP_PARAM abslMethTipParam in pdiSAbslMethTip.PARAMETERS)
                methodDef.Params.Add(new MethodParamDef(abslMethTipParam.NAME, abslMethTipParam.DISPLAY_NAME, abslMethTipParam.DESCRIPTION));
            }
            asMethods.methods.Add(methodDef);
          }
          scope.SetMethods((Methods) asMethods);
        }
        else
        {
          foreach (PDI_S_ABSL_CODECOMPL_RESULT abslCodecomplResult in abslCodeCompletion.Exporting.ET_RESULT)
            scope.AddDeclaration(new ASDeclaration(abslCodecomplResult.NAME, abslCodecomplResult.DESCRIPTION, abslCodecomplResult.DISPLAY_NAME, abslCodecomplResult.ICON_INDEX));
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
    }

    private void FakeMethodTipResult(ref PDI_S_ABSL_METH_TIP_DATA methodData)
    {
      methodData = new PDI_S_ABSL_METH_TIP_DATA();
      methodData.PARAMETER_IDX = 0;
      methodData.METHOD_TOKEN_IDX = 33;
      methodData.METHODS = new PDI_S_ABSL_METH_TIP[3];
      methodData.METHODS[0] = new PDI_S_ABSL_METH_TIP();
      methodData.METHODS[0].METHOD_NAME = "TestMethod1";
      methodData.METHODS[0].RESULT_TYPE = "ResultType1";
      methodData.METHODS[0].DESCRIPTION = "MethodDescription1";
      methodData.METHODS[1] = new PDI_S_ABSL_METH_TIP();
      methodData.METHODS[1].METHOD_NAME = "TestMethod2";
      methodData.METHODS[1].RESULT_TYPE = "ResultType2";
      methodData.METHODS[1].DESCRIPTION = "MethodDescription2";
      methodData.METHODS[1].PARAMETERS = new PDI_S_ABSL_METH_TIP_PARAM[1];
      methodData.METHODS[1].PARAMETERS[0] = new PDI_S_ABSL_METH_TIP_PARAM();
      methodData.METHODS[1].PARAMETERS[0].NAME = "Param1";
      methodData.METHODS[1].PARAMETERS[0].DISPLAY_NAME = "ParamDisplayName1";
      methodData.METHODS[1].PARAMETERS[0].DESCRIPTION = "ParamDescription1";
      methodData.METHODS[2] = new PDI_S_ABSL_METH_TIP();
      methodData.METHODS[2].METHOD_NAME = "TestMethod2";
      methodData.METHODS[2].RESULT_TYPE = "ResultType3";
      methodData.METHODS[2].DESCRIPTION = "Overloaded Method";
      methodData.METHODS[2].PARAMETERS = new PDI_S_ABSL_METH_TIP_PARAM[2];
      methodData.METHODS[2].PARAMETERS[0] = new PDI_S_ABSL_METH_TIP_PARAM();
      methodData.METHODS[2].PARAMETERS[0].NAME = "Param1";
      methodData.METHODS[2].PARAMETERS[0].DISPLAY_NAME = "ParamDisplayName1";
      methodData.METHODS[2].PARAMETERS[0].DESCRIPTION = "ParamDescription1";
      methodData.METHODS[2].PARAMETERS[1] = new PDI_S_ABSL_METH_TIP_PARAM();
      methodData.METHODS[2].PARAMETERS[1].NAME = "Param2";
      methodData.METHODS[2].PARAMETERS[1].DISPLAY_NAME = "ParamDisplayName2";
      methodData.METHODS[2].PARAMETERS[1].DESCRIPTION = "ParamDescription2";
    }

    private Severity MapABAPToClientSeverity(string abapSeverity)
    {
      switch (abapSeverity)
      {
        case "I":
          return Severity.Hint;
        case "W":
          return Severity.Warning;
        case "E":
          return Severity.Error;
        case "A":
          return Severity.Fatal;
        default:
          return Severity.Error;
      }
    }
  }
}
