﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.VSPackage
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus.ABAPScriptLanguage
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class VSPackage
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    internal VSPackage()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) VSPackage.resourceMan, (object) null))
          VSPackage.resourceMan = new ResourceManager("SAP.Copernicus.ABAPScriptLanguage.VSPackage", typeof (VSPackage).Assembly);
        return VSPackage.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return VSPackage.resourceCulture;
      }
      set
      {
        VSPackage.resourceCulture = value;
      }
    }

    internal static string _106
    {
      get
      {
        return VSPackage.ResourceManager.GetString("106", VSPackage.resourceCulture);
      }
    }

    internal static string _110
    {
      get
      {
        return VSPackage.ResourceManager.GetString("110", VSPackage.resourceCulture);
      }
    }

    internal static string _112
    {
      get
      {
        return VSPackage.ResourceManager.GetString("112", VSPackage.resourceCulture);
      }
    }

    internal static string _113
    {
      get
      {
        return VSPackage.ResourceManager.GetString("113", VSPackage.resourceCulture);
      }
    }

    internal static Icon _400
    {
      get
      {
        return (Icon) VSPackage.ResourceManager.GetObject("400", VSPackage.resourceCulture);
      }
    }

    internal static string DumpAnalysisTitle
    {
      get
      {
        return VSPackage.ResourceManager.GetString(nameof (DumpAnalysisTitle), VSPackage.resourceCulture);
      }
    }
  }
}
