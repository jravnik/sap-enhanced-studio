﻿// Decompiled with JetBrains decompiler
// Type: ByDScriptingLanguageLexer
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Antlr.Runtime;
using System;
using System.CodeDom.Compiler;
using System.Diagnostics;

[CLSCompliant(false)]
[GeneratedCode("ANTLR", "3.4")]
public class ByDScriptingLanguageLexer : Lexer
{
  private static readonly bool[] decisionCanBacktrack = new bool[0];
  public const int EOF = -1;
  public const int ABSID = 4;
  public const int ABSLID = 5;
  public const int ACTION = 6;
  public const int ADD = 7;
  public const int AND = 8;
  public const int ANNOTATION = 9;
  public const int AS = 10;
  public const int ASSIGNMENT = 11;
  public const int ASSIGN_OPS = 12;
  public const int ASSOCIATION = 13;
  public const int BINARY = 14;
  public const int BONAME = 15;
  public const int BREAK = 16;
  public const int BROKEN_NUMLIT = 17;
  public const int BROKEN_STRLIT = 18;
  public const int BUSINESSOBJECT = 19;
  public const int CALL = 20;
  public const int CARDINALITY = 21;
  public const int CARD_N = 22;
  public const int CASE = 23;
  public const int CASECLAUSE = 24;
  public const int CATCH = 25;
  public const int COLLECTIONOF = 26;
  public const int COLLOF = 27;
  public const int COLON = 28;
  public const int COLON2 = 29;
  public const int COMMA = 30;
  public const int CONTINUE = 31;
  public const int DECLIT = 32;
  public const int DEFAULT = 33;
  public const int DEFAULTCLAUSE = 34;
  public const int DELIM = 35;
  public const int DIV = 36;
  public const int DOT = 37;
  public const int DoubleStringCharacter = 38;
  public const int ELEMENT = 39;
  public const int ELEMENTSOF = 40;
  public const int ELSE = 41;
  public const int ELSECLAUSE = 42;
  public const int ELSEIFCLAUSE = 43;
  public const int ELTSOF = 44;
  public const int EQ = 45;
  public const int EXEC = 46;
  public const int EscapeCharacter = 47;
  public const int ExponentPart = 48;
  public const int FOREACH = 49;
  public const int FOREACHDECL = 50;
  public const int GE = 51;
  public const int GT = 52;
  public const int IDX = 53;
  public const int IF = 54;
  public const int IFCLAUSE = 55;
  public const int ILLEGAL_OPERATOR = 56;
  public const int IMPORT = 57;
  public const int INN = 58;
  public const int LAMBDA = 59;
  public const int LAMBDA_OPS = 60;
  public const int LBRCKT = 61;
  public const int LCURLB = 62;
  public const int LE = 63;
  public const int LPAREN = 64;
  public const int LTT = 65;
  public const int MESSAGE = 66;
  public const int ML_COMMENT = 67;
  public const int MOD = 68;
  public const int MUL = 69;
  public const int NE = 70;
  public const int NODE = 71;
  public const int NONE = 72;
  public const int NOT = 73;
  public const int NSPACE = 74;
  public const int NUMLIT = 75;
  public const int OR = 76;
  public const int PATH = 77;
  public const int QUALID = 78;
  public const int RAISE = 79;
  public const int RAISES = 80;
  public const int RBRCKT = 81;
  public const int RCURLB = 82;
  public const int RETURN = 83;
  public const int RPAREN = 84;
  public const int SL_COMMENT = 85;
  public const int STRLIT = 86;
  public const int SUB = 87;
  public const int SWITCH = 88;
  public const int TEXTT = 89;
  public const int TO = 90;
  public const int TRY = 91;
  public const int TRYBLOCK = 92;
  public const int TYPEOF = 93;
  public const int TYPOF = 94;
  public const int UNARY = 95;
  public const int USING = 96;
  public const int VALUES = 97;
  public const int VAR = 98;
  public const int VARDECL = 99;
  public const int WHILE = 100;
  public const int WS = 101;
  private ByDScriptingLanguageLexer.DFA7 dfa7;
  private ByDScriptingLanguageLexer.DFA16 dfa16;

  public ByDScriptingLanguageLexer()
  {
  }

  public ByDScriptingLanguageLexer(ICharStream input)
    : this(input, new RecognizerSharedState())
  {
  }

  public ByDScriptingLanguageLexer(ICharStream input, RecognizerSharedState state)
    : base(input, state)
  {
  }

  public override string GrammarFileName
  {
    get
    {
      return "C:\\P4\\LeanStack\\BYDSTUDIO\\lsfrontend_LE_stream\\src\\_copernicus\\sln\\ASLanguage\\Grammar\\ByDScriptingLanguage.g";
    }
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void OnCreated()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule(string ruleName, int ruleIndex)
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule(string ruleName, int ruleIndex)
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_LAMBDA_OPS()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_LAMBDA_OPS()
  {
  }

  [GrammarRule("LAMBDA_OPS")]
  private void mLAMBDA_OPS()
  {
    int num1 = 60;
    int num2 = 0;
    this.Match("=>");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ASSIGN_OPS()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ASSIGN_OPS()
  {
  }

  [GrammarRule("ASSIGN_OPS")]
  private void mASSIGN_OPS()
  {
    int num1 = 12;
    int num2 = 0;
    this.Match(61);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_AND()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_AND()
  {
  }

  [GrammarRule("AND")]
  private void mAND()
  {
    int num1 = 8;
    int num2 = 0;
    this.Match("&&");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_OR()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_OR()
  {
  }

  [GrammarRule("OR")]
  private void mOR()
  {
    int num1 = 76;
    int num2 = 0;
    this.Match("||");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_NOT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_NOT()
  {
  }

  [GrammarRule("NOT")]
  private void mNOT()
  {
    int num1 = 73;
    int num2 = 0;
    this.Match(33);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_EQ()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_EQ()
  {
  }

  [GrammarRule("EQ")]
  private void mEQ()
  {
    int num1 = 45;
    int num2 = 0;
    this.Match("==");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_NE()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_NE()
  {
  }

  [GrammarRule("NE")]
  private void mNE()
  {
    int num1 = 70;
    int num2 = 0;
    this.Match("!=");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_GT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_GT()
  {
  }

  [GrammarRule("GT")]
  private void mGT()
  {
    int num1 = 52;
    int num2 = 0;
    this.Match(62);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_LTT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_LTT()
  {
  }

  [GrammarRule("LTT")]
  private void mLTT()
  {
    int num1 = 65;
    int num2 = 0;
    this.Match(60);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_GE()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_GE()
  {
  }

  [GrammarRule("GE")]
  private void mGE()
  {
    int num1 = 51;
    int num2 = 0;
    this.Match(">=");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_LE()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_LE()
  {
  }

  [GrammarRule("LE")]
  private void mLE()
  {
    int num1 = 63;
    int num2 = 0;
    this.Match("<=");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ADD()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ADD()
  {
  }

  [GrammarRule("ADD")]
  private void mADD()
  {
    int num1 = 7;
    int num2 = 0;
    this.Match(43);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_SUB()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_SUB()
  {
  }

  [GrammarRule("SUB")]
  private void mSUB()
  {
    int num1 = 87;
    int num2 = 0;
    this.Match(45);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_MUL()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_MUL()
  {
  }

  [GrammarRule("MUL")]
  private void mMUL()
  {
    int num1 = 69;
    int num2 = 0;
    this.Match(42);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_DIV()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_DIV()
  {
  }

  [GrammarRule("DIV")]
  private void mDIV()
  {
    int num1 = 36;
    int num2 = 0;
    this.Match(47);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_MOD()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_MOD()
  {
  }

  [GrammarRule("MOD")]
  private void mMOD()
  {
    int num1 = 68;
    int num2 = 0;
    this.Match(37);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_LPAREN()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_LPAREN()
  {
  }

  [GrammarRule("LPAREN")]
  private void mLPAREN()
  {
    int num1 = 64;
    int num2 = 0;
    this.Match(40);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_RPAREN()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_RPAREN()
  {
  }

  [GrammarRule("RPAREN")]
  private void mRPAREN()
  {
    int num1 = 84;
    int num2 = 0;
    this.Match(41);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_LCURLB()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_LCURLB()
  {
  }

  [GrammarRule("LCURLB")]
  private void mLCURLB()
  {
    int num1 = 62;
    int num2 = 0;
    this.Match(123);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_RCURLB()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_RCURLB()
  {
  }

  [GrammarRule("RCURLB")]
  private void mRCURLB()
  {
    int num1 = 82;
    int num2 = 0;
    this.Match(125);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_LBRCKT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_LBRCKT()
  {
  }

  [GrammarRule("LBRCKT")]
  private void mLBRCKT()
  {
    int num1 = 61;
    int num2 = 0;
    this.Match(91);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_RBRCKT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_RBRCKT()
  {
  }

  [GrammarRule("RBRCKT")]
  private void mRBRCKT()
  {
    int num1 = 81;
    int num2 = 0;
    this.Match(93);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_AS()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_AS()
  {
  }

  [GrammarRule("AS")]
  private void mAS()
  {
    int num1 = 10;
    int num2 = 0;
    this.Match("as");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_IMPORT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_IMPORT()
  {
  }

  [GrammarRule("IMPORT")]
  private void mIMPORT()
  {
    int num1 = 57;
    int num2 = 0;
    this.Match("import");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_IF()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_IF()
  {
  }

  [GrammarRule("IF")]
  private void mIF()
  {
    int num1 = 54;
    int num2 = 0;
    this.Match("if");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ELSE()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ELSE()
  {
  }

  [GrammarRule("ELSE")]
  private void mELSE()
  {
    int num1 = 41;
    int num2 = 0;
    this.Match("else");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_SWITCH()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_SWITCH()
  {
  }

  [GrammarRule("SWITCH")]
  private void mSWITCH()
  {
    int num1 = 88;
    int num2 = 0;
    this.Match("switch");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_CASE()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_CASE()
  {
  }

  [GrammarRule("CASE")]
  private void mCASE()
  {
    int num1 = 23;
    int num2 = 0;
    this.Match("case");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_DEFAULT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_DEFAULT()
  {
  }

  [GrammarRule("DEFAULT")]
  private void mDEFAULT()
  {
    int num1 = 33;
    int num2 = 0;
    this.Match("default");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_WHILE()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_WHILE()
  {
  }

  [GrammarRule("WHILE")]
  private void mWHILE()
  {
    int num1 = 100;
    int num2 = 0;
    this.Match("while");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_FOREACH()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_FOREACH()
  {
  }

  [GrammarRule("FOREACH")]
  private void mFOREACH()
  {
    int num1 = 49;
    int num2 = 0;
    this.Match("foreach");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_INN()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_INN()
  {
  }

  [GrammarRule("INN")]
  private void mINN()
  {
    int num1 = 58;
    int num2 = 0;
    this.Match("in");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_CONTINUE()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_CONTINUE()
  {
  }

  [GrammarRule("CONTINUE")]
  private void mCONTINUE()
  {
    int num1 = 31;
    int num2 = 0;
    this.Match("continue");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_BREAK()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_BREAK()
  {
  }

  [GrammarRule("BREAK")]
  private void mBREAK()
  {
    int num1 = 16;
    int num2 = 0;
    this.Match("break");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_RAISE()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_RAISE()
  {
  }

  [GrammarRule("RAISE")]
  private void mRAISE()
  {
    int num1 = 79;
    int num2 = 0;
    this.Match("raise");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_VAR()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_VAR()
  {
  }

  [GrammarRule("VAR")]
  private void mVAR()
  {
    int num1 = 98;
    int num2 = 0;
    this.Match("var");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_COLLOF()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_COLLOF()
  {
  }

  [GrammarRule("COLLOF")]
  private void mCOLLOF()
  {
    int num1 = 27;
    int num2 = 0;
    this.Match("collectionof");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ELTSOF()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ELTSOF()
  {
  }

  [GrammarRule("ELTSOF")]
  private void mELTSOF()
  {
    int num1 = 44;
    int num2 = 0;
    this.Match("elementsof");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_TYPOF()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_TYPOF()
  {
  }

  [GrammarRule("TYPOF")]
  private void mTYPOF()
  {
    int num1 = 94;
    int num2 = 0;
    this.Match("typeof");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_RETURN()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_RETURN()
  {
  }

  [GrammarRule("RETURN")]
  private void mRETURN()
  {
    int num1 = 83;
    int num2 = 0;
    this.Match("return");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_TRY()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_TRY()
  {
  }

  [GrammarRule("TRY")]
  private void mTRY()
  {
    int num1 = 91;
    int num2 = 0;
    this.Match("try");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_CATCH()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_CATCH()
  {
  }

  [GrammarRule("CATCH")]
  private void mCATCH()
  {
    int num1 = 25;
    int num2 = 0;
    this.Match("catch");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_BUSINESSOBJECT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_BUSINESSOBJECT()
  {
  }

  [GrammarRule("BUSINESSOBJECT")]
  private void mBUSINESSOBJECT()
  {
    int num1 = 19;
    int num2 = 0;
    this.Match("businessobject");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_MESSAGE()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_MESSAGE()
  {
  }

  [GrammarRule("MESSAGE")]
  private void mMESSAGE()
  {
    int num1 = 66;
    int num2 = 0;
    this.Match("message");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_NODE()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_NODE()
  {
  }

  [GrammarRule("NODE")]
  private void mNODE()
  {
    int num1 = 71;
    int num2 = 0;
    this.Match("node");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ELEMENT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ELEMENT()
  {
  }

  [GrammarRule("ELEMENT")]
  private void mELEMENT()
  {
    int num1 = 39;
    int num2 = 0;
    this.Match("element");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_RAISES()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_RAISES()
  {
  }

  [GrammarRule("RAISES")]
  private void mRAISES()
  {
    int num1 = 80;
    int num2 = 0;
    this.Match("raises");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ASSOCIATION()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ASSOCIATION()
  {
  }

  [GrammarRule("ASSOCIATION")]
  private void mASSOCIATION()
  {
    int num1 = 13;
    int num2 = 0;
    this.Match("association");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_TO()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_TO()
  {
  }

  [GrammarRule("TO")]
  private void mTO()
  {
    int num1 = 90;
    int num2 = 0;
    this.Match("to");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ACTION()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ACTION()
  {
  }

  [GrammarRule("ACTION")]
  private void mACTION()
  {
    int num1 = 6;
    int num2 = 0;
    this.Match("action");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_CARD_N()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_CARD_N()
  {
  }

  [GrammarRule("CARD_N")]
  private void mCARD_N()
  {
    int num1 = 22;
    int num2 = 0;
    this.Match(110);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_USING()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_USING()
  {
  }

  [GrammarRule("USING")]
  private void mUSING()
  {
    int num1 = 96;
    int num2 = 0;
    this.Match("using");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_TEXTT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_TEXTT()
  {
  }

  [GrammarRule("TEXTT")]
  private void mTEXTT()
  {
    int num1 = 89;
    int num2 = 0;
    this.Match("text");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_DOT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_DOT()
  {
  }

  [GrammarRule("DOT")]
  private void mDOT()
  {
    int num1 = 37;
    int num2 = 0;
    this.Match(46);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_COMMA()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_COMMA()
  {
  }

  [GrammarRule("COMMA")]
  private void mCOMMA()
  {
    int num1 = 30;
    int num2 = 0;
    this.Match(44);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_DELIM()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_DELIM()
  {
  }

  [GrammarRule("DELIM")]
  private void mDELIM()
  {
    int num1 = 35;
    int num2 = 0;
    this.Match(59);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_COLON()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_COLON()
  {
  }

  [GrammarRule("COLON")]
  private void mCOLON()
  {
    int num1 = 28;
    int num2 = 0;
    this.Match(58);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_COLON2()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_COLON2()
  {
  }

  [GrammarRule("COLON2")]
  private void mCOLON2()
  {
    int num1 = 29;
    int num2 = 0;
    this.Match("::");
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ABSLID()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ABSLID()
  {
  }

  [GrammarRule("ABSLID")]
  private void mABSLID()
  {
    int num1 = 5;
    int num2 = 0;
    if (this.input.LA(1) >= 65 && this.input.LA(1) <= 90 || this.input.LA(1) == 95 || this.input.LA(1) >= 97 && this.input.LA(1) <= 122)
    {
      this.input.Consume();
      while (true)
      {
        int num3 = 2;
        int num4 = this.input.LA(1);
        if (num4 >= 48 && num4 <= 57 || num4 >= 65 && num4 <= 90 || (num4 == 95 || num4 >= 97 && num4 <= 122))
          num3 = 1;
        if (num3 == 1)
          this.input.Consume();
        else
          break;
      }
      this.state.type = num1;
      this.state.channel = num2;
    }
    else
    {
      MismatchedSetException mismatchedSetException = new MismatchedSetException((BitSet) null, (IIntStream) this.input);
      this.Recover((RecognitionException) mismatchedSetException);
      throw mismatchedSetException;
    }
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_BROKEN_STRLIT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_BROKEN_STRLIT()
  {
  }

  [GrammarRule("BROKEN_STRLIT")]
  private void mBROKEN_STRLIT()
  {
    int num1 = 18;
    int num2 = 0;
    this.Match(34);
    while (true)
    {
      int num3 = 2;
      int num4 = this.input.LA(1);
      if (num4 >= 0 && num4 <= 9 || num4 >= 11 && num4 <= 12 || (num4 >= 14 && num4 <= 33 || num4 >= 35 && num4 <= (int) ushort.MaxValue))
        num3 = 1;
      if (num3 == 1)
        this.mDoubleStringCharacter();
      else
        break;
    }
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_STRLIT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_STRLIT()
  {
  }

  [GrammarRule("STRLIT")]
  private void mSTRLIT()
  {
    int num1 = 86;
    int num2 = 0;
    this.Match(34);
    while (true)
    {
      int num3 = 2;
      int num4 = this.input.LA(1);
      if (num4 >= 0 && num4 <= 9 || num4 >= 11 && num4 <= 12 || (num4 >= 14 && num4 <= 33 || num4 >= 35 && num4 <= (int) ushort.MaxValue))
        num3 = 1;
      if (num3 == 1)
        this.mDoubleStringCharacter();
      else
        break;
    }
    this.Match(34);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_DoubleStringCharacter()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_DoubleStringCharacter()
  {
  }

  [GrammarRule("DoubleStringCharacter")]
  private void mDoubleStringCharacter()
  {
    int num1 = this.input.LA(1);
    int num2;
    if (num1 >= 0 && num1 <= 9 || num1 >= 11 && num1 <= 12 || (num1 >= 14 && num1 <= 33 || num1 >= 35 && num1 <= 91) || num1 >= 93 && num1 <= (int) ushort.MaxValue)
    {
      num2 = 1;
    }
    else
    {
      if (num1 != 92)
        throw new NoViableAltException("", 4, 0, (IIntStream) this.input);
      num2 = 2;
    }
    switch (num2)
    {
      case 1:
        this.input.Consume();
        break;
      case 2:
        this.Match(92);
        this.mEscapeCharacter();
        break;
    }
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_EscapeCharacter()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_EscapeCharacter()
  {
  }

  [GrammarRule("EscapeCharacter")]
  private void mEscapeCharacter()
  {
    if (this.input.LA(1) == 34 || this.input.LA(1) == 39 || (this.input.LA(1) == 92 || this.input.LA(1) == 98) || (this.input.LA(1) == 102 || this.input.LA(1) == 110 || (this.input.LA(1) == 114 || this.input.LA(1) == 116)) || this.input.LA(1) == 118)
    {
      this.input.Consume();
    }
    else
    {
      MismatchedSetException mismatchedSetException = new MismatchedSetException((BitSet) null, (IIntStream) this.input);
      this.Recover((RecognitionException) mismatchedSetException);
      throw mismatchedSetException;
    }
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_DECLIT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_DECLIT()
  {
  }

  [GrammarRule("DECLIT")]
  private void mDECLIT()
  {
    int num1 = 32;
    int num2 = 0;
    int num3 = 0;
    while (true)
    {
      int num4 = 2;
      int num5 = this.input.LA(1);
      if (num5 >= 48 && num5 <= 57)
        num4 = 1;
      if (num4 == 1)
      {
        this.input.Consume();
        ++num3;
      }
      else
        break;
    }
    if (num3 < 1)
      throw new EarlyExitException(5, (IIntStream) this.input);
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_NUMLIT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_NUMLIT()
  {
  }

  [GrammarRule("NUMLIT")]
  private void mNUMLIT()
  {
    int num1 = 75;
    int num2 = 0;
    int num3;
    try
    {
      num3 = this.dfa7.Predict((IIntStream) this.input);
    }
    catch (NoViableAltException ex)
    {
      throw;
    }
    switch (num3)
    {
      case 1:
        this.mDECLIT();
        this.Match(46);
        this.mDECLIT();
        break;
      case 2:
        int num4 = 2;
        if (this.input.LA(1) == 46)
          num4 = 1;
        if (num4 == 1)
          this.Match(46);
        this.mDECLIT();
        break;
    }
    int num5 = 2;
    switch (this.input.LA(1))
    {
      case 69:
      case 101:
        num5 = 1;
        break;
    }
    if (num5 == 1)
      this.mExponentPart();
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ExponentPart()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ExponentPart()
  {
  }

  [GrammarRule("ExponentPart")]
  private void mExponentPart()
  {
    if (this.input.LA(1) == 69 || this.input.LA(1) == 101)
    {
      this.input.Consume();
      int num = 2;
      switch (this.input.LA(1))
      {
        case 43:
        case 45:
          num = 1;
          break;
      }
      if (num == 1)
        this.input.Consume();
      this.mDECLIT();
    }
    else
    {
      MismatchedSetException mismatchedSetException = new MismatchedSetException((BitSet) null, (IIntStream) this.input);
      this.Recover((RecognitionException) mismatchedSetException);
      throw mismatchedSetException;
    }
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_BROKEN_NUMLIT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_BROKEN_NUMLIT()
  {
  }

  [GrammarRule("BROKEN_NUMLIT")]
  private void mBROKEN_NUMLIT()
  {
    int num1 = 17;
    int num2 = 0;
    this.Match(46);
    this.MatchRange(48, 57);
    while (true)
    {
      int num3 = 2;
      int num4 = this.input.LA(1);
      if (num4 >= 0 && num4 <= 9 || num4 >= 11 && num4 <= 12 || (num4 >= 14 && num4 <= 43 || num4 >= 45 && num4 <= (int) ushort.MaxValue))
        num3 = 1;
      if (num3 == 1)
        this.input.Consume();
      else
        break;
    }
    this.state.type = num1;
    this.state.channel = num2;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_SL_COMMENT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_SL_COMMENT()
  {
  }

  [GrammarRule("SL_COMMENT")]
  private void mSL_COMMENT()
  {
    int num1 = 85;
    this.Match("//");
    while (true)
    {
      int num2 = 2;
      int num3 = this.input.LA(1);
      if (num3 >= 0 && num3 <= 9 || num3 >= 11 && num3 <= 12 || num3 >= 14 && num3 <= (int) ushort.MaxValue)
        num2 = 1;
      if (num2 == 1)
        this.input.Consume();
      else
        break;
    }
    int num4 = 2;
    if (this.input.LA(1) == 13)
      num4 = 1;
    if (num4 == 1)
      this.Match(13);
    int num5 = 2;
    if (this.input.LA(1) == 10)
      num5 = 1;
    if (num5 == 1)
      this.Match(10);
    int num6 = 99;
    this.state.type = num1;
    this.state.channel = num6;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ML_COMMENT()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ML_COMMENT()
  {
  }

  [GrammarRule("ML_COMMENT")]
  private void mML_COMMENT()
  {
    int num1 = 67;
    this.Match("/*");
    while (true)
    {
      int num2 = 2;
      int num3 = this.input.LA(1);
      if (num3 == 42)
      {
        int num4 = this.input.LA(2);
        if (num4 == 47)
          num2 = 2;
        else if (num4 >= 0 && num4 <= 46 || num4 >= 48 && num4 <= (int) ushort.MaxValue)
          num2 = 1;
      }
      else if (num3 >= 0 && num3 <= 41 || num3 >= 43 && num3 <= (int) ushort.MaxValue)
        num2 = 1;
      if (num2 == 1)
        this.MatchAny();
      else
        break;
    }
    this.Match("*/");
    int num5 = 99;
    this.state.type = num1;
    this.state.channel = num5;
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_WS()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_WS()
  {
  }

  [GrammarRule("WS")]
  private void mWS()
  {
    int num1 = 101;
    if (this.input.LA(1) >= 9 && this.input.LA(1) <= 10 || this.input.LA(1) >= 12 && this.input.LA(1) <= 13 || (this.input.LA(1) == 32 || this.input.LA(1) == 118 || this.input.LA(1) == 160))
    {
      this.input.Consume();
      int num2 = 99;
      this.state.type = num1;
      this.state.channel = num2;
    }
    else
    {
      MismatchedSetException mismatchedSetException = new MismatchedSetException((BitSet) null, (IIntStream) this.input);
      this.Recover((RecognitionException) mismatchedSetException);
      throw mismatchedSetException;
    }
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void EnterRule_ILLEGAL_OPERATOR()
  {
  }

  [Conditional("ANTLR_TRACE")]
  protected virtual void LeaveRule_ILLEGAL_OPERATOR()
  {
  }

  [GrammarRule("ILLEGAL_OPERATOR")]
  private void mILLEGAL_OPERATOR()
  {
    int num1 = 56;
    int num2 = 0;
    int num3 = 0;
    while (true)
    {
      int num4 = 2;
      int num5 = this.input.LA(1);
      if (num5 == 33 || num5 >= 37 && num5 <= 38 || (num5 >= 42 && num5 <= 43 || (num5 == 45 || num5 == 47)) || (num5 >= 60 && num5 <= 62 || num5 == 124))
        num4 = 1;
      if (num4 == 1)
      {
        this.input.Consume();
        ++num3;
      }
      else
        break;
    }
    if (num3 < 1)
      throw new EarlyExitException(15, (IIntStream) this.input);
    this.state.type = num1;
    this.state.channel = num2;
  }

  public override void mTokens()
  {
    int num;
    try
    {
      num = this.dfa16.Predict((IIntStream) this.input);
    }
    catch (NoViableAltException ex)
    {
      throw;
    }
    switch (num)
    {
      case 1:
        this.mLAMBDA_OPS();
        break;
      case 2:
        this.mASSIGN_OPS();
        break;
      case 3:
        this.mAND();
        break;
      case 4:
        this.mOR();
        break;
      case 5:
        this.mNOT();
        break;
      case 6:
        this.mEQ();
        break;
      case 7:
        this.mNE();
        break;
      case 8:
        this.mGT();
        break;
      case 9:
        this.mLTT();
        break;
      case 10:
        this.mGE();
        break;
      case 11:
        this.mLE();
        break;
      case 12:
        this.mADD();
        break;
      case 13:
        this.mSUB();
        break;
      case 14:
        this.mMUL();
        break;
      case 15:
        this.mDIV();
        break;
      case 16:
        this.mMOD();
        break;
      case 17:
        this.mLPAREN();
        break;
      case 18:
        this.mRPAREN();
        break;
      case 19:
        this.mLCURLB();
        break;
      case 20:
        this.mRCURLB();
        break;
      case 21:
        this.mLBRCKT();
        break;
      case 22:
        this.mRBRCKT();
        break;
      case 23:
        this.mAS();
        break;
      case 24:
        this.mIMPORT();
        break;
      case 25:
        this.mIF();
        break;
      case 26:
        this.mELSE();
        break;
      case 27:
        this.mSWITCH();
        break;
      case 28:
        this.mCASE();
        break;
      case 29:
        this.mDEFAULT();
        break;
      case 30:
        this.mWHILE();
        break;
      case 31:
        this.mFOREACH();
        break;
      case 32:
        this.mINN();
        break;
      case 33:
        this.mCONTINUE();
        break;
      case 34:
        this.mBREAK();
        break;
      case 35:
        this.mRAISE();
        break;
      case 36:
        this.mVAR();
        break;
      case 37:
        this.mCOLLOF();
        break;
      case 38:
        this.mELTSOF();
        break;
      case 39:
        this.mTYPOF();
        break;
      case 40:
        this.mRETURN();
        break;
      case 41:
        this.mTRY();
        break;
      case 42:
        this.mCATCH();
        break;
      case 43:
        this.mBUSINESSOBJECT();
        break;
      case 44:
        this.mMESSAGE();
        break;
      case 45:
        this.mNODE();
        break;
      case 46:
        this.mELEMENT();
        break;
      case 47:
        this.mRAISES();
        break;
      case 48:
        this.mASSOCIATION();
        break;
      case 49:
        this.mTO();
        break;
      case 50:
        this.mACTION();
        break;
      case 51:
        this.mCARD_N();
        break;
      case 52:
        this.mUSING();
        break;
      case 53:
        this.mTEXTT();
        break;
      case 54:
        this.mDOT();
        break;
      case 55:
        this.mCOMMA();
        break;
      case 56:
        this.mDELIM();
        break;
      case 57:
        this.mCOLON();
        break;
      case 58:
        this.mCOLON2();
        break;
      case 59:
        this.mABSLID();
        break;
      case 60:
        this.mBROKEN_STRLIT();
        break;
      case 61:
        this.mSTRLIT();
        break;
      case 62:
        this.mDECLIT();
        break;
      case 63:
        this.mNUMLIT();
        break;
      case 64:
        this.mBROKEN_NUMLIT();
        break;
      case 65:
        this.mSL_COMMENT();
        break;
      case 66:
        this.mML_COMMENT();
        break;
      case 67:
        this.mWS();
        break;
      case 68:
        this.mILLEGAL_OPERATOR();
        break;
    }
  }

  protected override void InitDFAs()
  {
    base.InitDFAs();
    this.dfa7 = new ByDScriptingLanguageLexer.DFA7((BaseRecognizer) this);
    this.dfa16 = new ByDScriptingLanguageLexer.DFA16((BaseRecognizer) this, new SpecialStateTransitionHandler(this.SpecialStateTransition16));
  }

  private int SpecialStateTransition16(DFA dfa, int s, IIntStream _input)
  {
    IIntStream input = _input;
    int stateNumber = s;
    switch (s)
    {
      case 0:
        int num1 = input.LA(1);
        s = -1;
        s = num1 != 47 ? (num1 != 42 ? (num1 == 33 || num1 >= 37 && num1 <= 38 || (num1 == 43 || num1 == 45 || num1 >= 60 && num1 <= 62) || num1 == 124 ? 105 : (num1 >= 0 && num1 <= 32 || num1 >= 34 && num1 <= 36 || (num1 >= 39 && num1 <= 41 || (num1 == 44 || num1 == 46)) || (num1 >= 48 && num1 <= 59 || num1 >= 63 && num1 <= 123 || num1 >= 125 && num1 <= (int) ushort.MaxValue) ? 106 : 44)) : 104) : 139;
        if (s >= 0)
          return s;
        break;
      case 1:
        int num2 = input.LA(1);
        s = -1;
        s = num2 == 69 || num2 == 101 ? 135 : (num2 < 48 || num2 > 57 ? (num2 >= 0 && num2 <= 9 || num2 >= 11 && num2 <= 12 || (num2 >= 14 && num2 <= 43 || num2 >= 45 && num2 <= 47) || (num2 >= 58 && num2 <= 68 || num2 >= 70 && num2 <= 100 || num2 >= 102 && num2 <= (int) ushort.MaxValue) ? 137 : 94) : 136);
        if (s >= 0)
          return s;
        break;
      case 2:
        int num3 = input.LA(1);
        s = -1;
        s = num3 != 42 ? (num3 == 33 || num3 >= 37 && num3 <= 38 || (num3 == 43 || num3 == 45 || num3 == 47) || (num3 >= 60 && num3 <= 62 || num3 == 124) ? 105 : (num3 >= 0 && num3 <= 32 || num3 >= 34 && num3 <= 36 || (num3 >= 39 && num3 <= 41 || (num3 == 44 || num3 == 46)) || (num3 >= 48 && num3 <= 59 || num3 >= 63 && num3 <= 123 || num3 >= 125 && num3 <= (int) ushort.MaxValue) ? 106 : 44)) : 104;
        if (s >= 0)
          return s;
        break;
      case 3:
        int num4 = input.LA(1);
        s = -1;
        s = num4 != 42 ? (num4 == 33 || num4 >= 37 && num4 <= 38 || (num4 == 43 || num4 == 45 || num4 == 47) || (num4 >= 60 && num4 <= 62 || num4 == 124) ? 105 : (num4 >= 0 && num4 <= 32 || num4 >= 34 && num4 <= 36 || (num4 >= 39 && num4 <= 41 || (num4 == 44 || num4 == 46)) || (num4 >= 48 && num4 <= 59 || num4 >= 63 && num4 <= 123 || num4 >= 125 && num4 <= (int) ushort.MaxValue) ? 106 : 44)) : 104;
        if (s >= 0)
          return s;
        break;
      case 4:
        int num5 = input.LA(1);
        s = -1;
        s = num5 >= 0 && num5 <= 9 || num5 >= 11 && num5 <= 12 || (num5 >= 14 && num5 <= 33 || num5 >= 35 && num5 <= 91) || num5 >= 93 && num5 <= (int) ushort.MaxValue ? 89 : (num5 != 92 ? (num5 != 34 ? 91 : 92) : 90);
        if (s >= 0)
          return s;
        break;
      case 5:
        int num6 = input.LA(1);
        s = -1;
        s = num6 >= 0 && num6 <= 9 || num6 >= 11 && num6 <= 12 || (num6 >= 14 && num6 <= 33 || num6 >= 35 && num6 <= 91) || num6 >= 93 && num6 <= (int) ushort.MaxValue ? 89 : (num6 != 92 ? (num6 != 34 ? 91 : 92) : 90);
        if (s >= 0)
          return s;
        break;
      case 6:
        int num7 = input.LA(1);
        s = -1;
        s = num7 >= 0 && num7 <= 9 || num7 >= 11 && num7 <= 12 || (num7 >= 14 && num7 <= 33 || num7 >= 35 && num7 <= 91) || num7 >= 93 && num7 <= (int) ushort.MaxValue ? 89 : (num7 != 92 ? (num7 != 34 ? 91 : 92) : 90);
        if (s >= 0)
          return s;
        break;
      case 7:
        int num8 = input.LA(1);
        s = -1;
        s = num8 == 69 || num8 == 101 ? 135 : (num8 < 48 || num8 > 57 ? (num8 >= 0 && num8 <= 9 || num8 >= 11 && num8 <= 12 || (num8 >= 14 && num8 <= 43 || num8 >= 45 && num8 <= 47) || (num8 >= 58 && num8 <= 68 || num8 >= 70 && num8 <= 100 || num8 >= 102 && num8 <= (int) ushort.MaxValue) ? 137 : 94) : 136);
        if (s >= 0)
          return s;
        break;
      case 8:
        int num9 = input.LA(1);
        s = -1;
        s = num9 < 48 || num9 > 57 ? (num9 >= 0 && num9 <= 9 || num9 >= 11 && num9 <= 12 || (num9 >= 14 && num9 <= 43 || num9 >= 45 && num9 <= 47) || num9 >= 58 && num9 <= (int) ushort.MaxValue ? 137 : 94) : 165;
        if (s >= 0)
          return s;
        break;
    }
    NoViableAltException nvae = new NoViableAltException(dfa.Description, 16, stateNumber, input);
    dfa.Error(nvae);
    throw nvae;
  }

  private class DFA7 : DFA
  {
    private static readonly string[] DFA7_transitionS = new string[4]
    {
      "\x0001\x0002\x0001\xFFFF\n\x0001",
      "\x0001\x0003\x0001\xFFFF\n\x0001",
      "",
      ""
    };
    private static readonly short[] DFA7_eot = DFA.UnpackEncodedString("\x0001\xFFFF\x0001\x0002\x0002\xFFFF");
    private static readonly short[] DFA7_eof = DFA.UnpackEncodedString("\x0004\xFFFF");
    private static readonly char[] DFA7_min = DFA.UnpackEncodedStringToUnsignedChars("\x0002.\x0002\xFFFF");
    private static readonly char[] DFA7_max = DFA.UnpackEncodedStringToUnsignedChars("\x00029\x0002\xFFFF");
    private static readonly short[] DFA7_accept = DFA.UnpackEncodedString("\x0002\xFFFF\x0001\x0002\x0001\x0001");
    private static readonly short[] DFA7_special = DFA.UnpackEncodedString("\x0004\xFFFF}>");
    private const string DFA7_eotS = "\x0001\xFFFF\x0001\x0002\x0002\xFFFF";
    private const string DFA7_eofS = "\x0004\xFFFF";
    private const string DFA7_minS = "\x0002.\x0002\xFFFF";
    private const string DFA7_maxS = "\x00029\x0002\xFFFF";
    private const string DFA7_acceptS = "\x0002\xFFFF\x0001\x0002\x0001\x0001";
    private const string DFA7_specialS = "\x0004\xFFFF}>";
    private static readonly short[][] DFA7_transition;

    static DFA7()
    {
      int length = ByDScriptingLanguageLexer.DFA7.DFA7_transitionS.Length;
      ByDScriptingLanguageLexer.DFA7.DFA7_transition = new short[length][];
      for (int index = 0; index < length; ++index)
        ByDScriptingLanguageLexer.DFA7.DFA7_transition[index] = DFA.UnpackEncodedString(ByDScriptingLanguageLexer.DFA7.DFA7_transitionS[index]);
    }

    public DFA7(BaseRecognizer recognizer)
    {
      this.recognizer = recognizer;
      this.decisionNumber = 7;
      this.eot = ByDScriptingLanguageLexer.DFA7.DFA7_eot;
      this.eof = ByDScriptingLanguageLexer.DFA7.DFA7_eof;
      this.min = ByDScriptingLanguageLexer.DFA7.DFA7_min;
      this.max = ByDScriptingLanguageLexer.DFA7.DFA7_max;
      this.accept = ByDScriptingLanguageLexer.DFA7.DFA7_accept;
      this.special = ByDScriptingLanguageLexer.DFA7.DFA7_special;
      this.transition = ByDScriptingLanguageLexer.DFA7.DFA7_transition;
    }

    public override string Description
    {
      get
      {
        return "710:9: ( DECLIT '.' DECLIT | ( '.' )? DECLIT )";
      }
    }

    public override void Error(NoViableAltException nvae)
    {
    }
  }

  private class DFA16 : DFA
  {
    private static readonly string[] DFA16_transitionS = new string[250]
    {
      "\x0002(\x0001\xFFFF\x0002(\x0012\xFFFF\x0001(\x0001\x0004\x0001&\x0002\xFFFF\x0001\v\x0001\x0002\x0001\xFFFF\x0001\f\x0001\r\x0001\t\x0001\a\x0001\"\x0001\b\x0001!\x0001\n\n'\x0001$\x0001#\x0001\x0006\x0001\x0001\x0001\x0005\x0002\xFFFF\x001A%\x0001\x0010\x0001\xFFFF\x0001\x0011\x0001\xFFFF\x0001%\x0001\xFFFF\x0001\x0012\x0001\x001A\x0001\x0016\x0001\x0017\x0001\x0014\x0001\x0019\x0002%\x0001\x0013\x0003%\x0001\x001E\x0001\x001F\x0003%\x0001\x001B\x0001\x0015\x0001\x001D\x0001 \x0001\x001C\x0001\x0018\x0003%\x0001\x000E\x0001\x0003\x0001\x000F\"\xFFFF\x0001(",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0001,\x0001*\x0001)=\xFFFF\x0001,",
      "\x0001-",
      "\x0001.",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0001,\x0001/\x0001,=\xFFFF\x0001,",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0001,\x00011\x0001,=\xFFFF\x0001,",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0001,\x00013\x0001,=\xFFFF\x0001,",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x00019\x0001,\x0001\xFFFF\x0001,\x0001\xFFFF\x00018\f\xFFFF\x0003,=\xFFFF\x0001,",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "",
      "",
      "",
      "",
      "",
      "",
      "\x0001=\x000F\xFFFF\x0001<",
      "\x0001?\x0006\xFFFF\x0001>\x0001@",
      "\x0001A",
      "\x0001B",
      "\x0001C\r\xFFFF\x0001D",
      "\x0001E",
      "\x0001F",
      "\x0001G",
      "\x0001H\x0002\xFFFF\x0001I",
      "\x0001J\x0003\xFFFF\x0001K",
      "\x0001L",
      "\x0001P\t\xFFFF\x0001O\x0002\xFFFF\x0001N\x0006\xFFFF\x0001M",
      "\x0001Q",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x000E%\x0001R\v%",
      "\x0001T",
      "\nU",
      "",
      "",
      "\x0001W",
      "",
      "\nY\x0001\xFFFF\x0002Y\x0001\xFFFF\x0014Y\x0001\\9Y\x0001ZﾣY",
      "\x0001^\x0001\xFFFF\n'\v\xFFFF\x0001^\x001F\xFFFF\x0001^",
      "",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "",
      "",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "",
      "\x0001,\x0003\xFFFF\x0002,\x0003\xFFFF\x0002,\x0001\xFFFF\x0001,\x0001\xFFFF\x0001,\f\xFFFF\x0003,=\xFFFF\x0001,",
      "",
      "",
      "",
      "",
      "\x0001f\x0003\xFFFF\x0002f\x0003\xFFFF\x0002f\x0001\xFFFF\x0001f\x0001\xFFFF\x0001f\f\xFFFF\x0003f=\xFFFF\x0001f",
      "!j\x0001i\x0003j\x0002i\x0003j\x0001h\x0001i\x0001j\x0001i\x0001j\x0001i\fj\x0003i=j\x0001iﾃj",
      "",
      "",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x0012%\x0001k\a%",
      "\x0001m",
      "\x0001n",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001r\r\xFFFF\x0001q",
      "\x0001s",
      "\x0001t\x0001u",
      "\x0001w\x0001\xFFFF\x0001v",
      "\x0001x",
      "\x0001y",
      "\x0001z",
      "\x0001{",
      "\x0001|",
      "\x0001}",
      "\x0001~",
      "\x0001\x007F",
      "\x0001\x0080",
      "\x0001\x0081",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001\x0083",
      "\x0001\x0084",
      "\x0001\x0085",
      "",
      "\x0001\x0086",
      "\n\x0089\x0001\xFFFF\x0002\x0089\x0001\xFFFF\x001E\x0089\x0001\xFFFF\x0003\x0089\n\x0088\v\x0089\x0001\x0087\x001F\x0089\x0001\x0087ﾚ\x0089",
      "",
      "",
      "",
      "\nY\x0001\xFFFF\x0002Y\x0001\xFFFF\x0014Y\x0001\\9Y\x0001ZﾣY",
      "\x0001\x008A\x0004\xFFFF\x0001\x008A4\xFFFF\x0001\x008A\x0005\xFFFF\x0001\x008A\x0003\xFFFF\x0001\x008A\a\xFFFF\x0001\x008A\x0003\xFFFF\x0001\x008A\x0001\xFFFF\x0001\x008A\x0001\xFFFF\x0001\x008A",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "\x0001f\x0003\xFFFF\x0002f\x0003\xFFFF\x0002f\x0001\xFFFF\x0001f\x0001\xFFFF\x0001f\f\xFFFF\x0003f=\xFFFF\x0001f",
      "",
      "!j\x0001i\x0003j\x0002i\x0003j\x0001h\x0001i\x0001j\x0001i\x0001j\x0001\x008B\fj\x0003i=j\x0001iﾃj",
      "!j\x0001i\x0003j\x0002i\x0003j\x0001h\x0001i\x0001j\x0001i\x0001j\x0001i\fj\x0003i=j\x0001iﾃj",
      "",
      "\x0001\x008C",
      "",
      "\x0001\x008D",
      "\x0001\x008E",
      "",
      "",
      "\x0001\x008F",
      "\x0001\x0090",
      "\x0001\x0091",
      "\x0001\x0092",
      "\x0001\x0093",
      "\x0001\x0094",
      "\x0001\x0095",
      "\x0001\x0096",
      "\x0001\x0097",
      "\x0001\x0098",
      "\x0001\x0099",
      "\x0001\x009A",
      "\x0001\x009B",
      "\x0001\x009C",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001\x009E",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "",
      "\x0001 ",
      "\x0001¡",
      "\x0001¢",
      "\x0001£",
      "\x0001¤\x0001\xFFFF\x0001¤\x0002\xFFFF\n¥",
      "\n\x0089\x0001\xFFFF\x0002\x0089\x0001\xFFFF\x001E\x0089\x0001\xFFFF\x0003\x0089\n\x0088\v\x0089\x0001\x0087\x001F\x0089\x0001\x0087ﾚ\x0089",
      "",
      "\nY\x0001\xFFFF\x0002Y\x0001\xFFFF\x0014Y\x0001\\9Y\x0001ZﾣY",
      "\x0001i\x0003\xFFFF\x0002i\x0003\xFFFF\x0001h\x0001i\x0001\xFFFF\x0001i\x0001\xFFFF\x0001i\f\xFFFF\x0003i=\xFFFF\x0001i",
      "\x0001¦",
      "\x0001§",
      "\x0001¨",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001ª",
      "\x0001«",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001\x00AD",
      "\x0001®",
      "\x0001¯",
      "\x0001°",
      "\x0001±",
      "\x0001\x00B2",
      "\x0001\x00B3",
      "\x0001´",
      "\x0001µ",
      "\x0001¶",
      "",
      "\x0001·",
      "",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001\x00B9",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001»",
      "\n¥",
      "\n\x0089\x0001\xFFFF\x0002\x0089\x0001\xFFFF\x001E\x0089\x0001\xFFFF\x0003\x0089\n¥ￆ\x0089",
      "\x0001\x00BC",
      "\x0001\x00BD",
      "\x0001\x00BE",
      "",
      "\x0001¿",
      "\x0001À",
      "",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001Â",
      "\x0001Ã",
      "\x0001Ä",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001Æ",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001È",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x0012%\x0001É\a%",
      "\x0001Ë",
      "\x0001Ì",
      "",
      "\x0001Í",
      "",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001Ï",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001Ò",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "",
      "\x0001Ô",
      "\x0001Õ",
      "\x0001Ö",
      "",
      "\x0001×",
      "",
      "\x0001Ø",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001Ü",
      "",
      "\x0001Ý",
      "",
      "",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x0012%\x0001Þ\a%",
      "",
      "\x0001à",
      "\x0001á",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001ä",
      "",
      "",
      "",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001æ",
      "\x0001ç",
      "",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001é",
      "",
      "",
      "\x0001ê",
      "",
      "\x0001ë",
      "\x0001ì",
      "",
      "\x0001í",
      "\x0001î",
      "\x0001ï",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001ñ",
      "\x0001ò",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "",
      "\x0001ô",
      "\x0001õ",
      "",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      "\x0001÷",
      "",
      "\x0001ø",
      "\n%\a\xFFFF\x001A%\x0004\xFFFF\x0001%\x0001\xFFFF\x001A%",
      ""
    };
    private static readonly short[] DFA16_eot = DFA.UnpackEncodedString("\x0001\xFFFF\x0001+\x0002,\x00010\x00012\x00014\x00015\x00016\x00017\x0001:\x0001;\x0006\xFFFF\r%\x0001S\x0001%\x0001V\x0002\xFFFF\x0001X\x0001\xFFFF\x0001[\x0001]\x0001\xFFFF\x0001_\x0001`\x0002\xFFFF\x0001a\x0001b\x0001c\x0001\xFFFF\x0001d\x0001\xFFFF\x0001e\x0004\xFFFF\x0001g\x0001,\x0002\xFFFF\x0001l\x0002%\x0001o\x0001p\x000E%\x0001\x0082\x0003%\x0001\xFFFF\x0001%\x0001^\x0003\xFFFF\x0001[\f\xFFFF\x0001g\x0001\xFFFF\x0002,\x0001\xFFFF\x0001%\x0001\xFFFF\x0002%\x0002\xFFFF\x000E%\x0001\x009D\x0001%\x0001\x009F\x0001\xFFFF\x0004%\x0001\x0089\x0001^\x0001\xFFFF\x0001[\x0001j\x0003%\x0001©\x0002%\x0001¬\n%\x0001\xFFFF\x0001%\x0001\xFFFF\x0001¸\x0001%\x0001º\x0001%\x0001\x0089\x0001^\x0003%\x0001\xFFFF\x0002%\x0001\xFFFF\x0001Á\x0003%\x0001Å\x0001%\x0001Ç\x0001%\x0001Ê\x0002%\x0001\xFFFF\x0001%\x0001\xFFFF\x0001Î\x0001%\x0001Ð\x0001Ñ\x0001%\x0001Ó\x0001\xFFFF\x0003%\x0001\xFFFF\x0001%\x0001\xFFFF\x0001%\x0001Ù\x0001\xFFFF\x0001Ú\x0001Û\x0001%\x0001\xFFFF\x0001%\x0002\xFFFF\x0001ß\x0001\xFFFF\x0002%\x0001â\x0001ã\x0001%\x0003\xFFFF\x0001å\x0002%\x0001\xFFFF\x0001è\x0001%\x0002\xFFFF\x0001%\x0001\xFFFF\x0002%\x0001\xFFFF\x0003%\x0001ð\x0002%\x0001ó\x0001\xFFFF\x0002%\x0001\xFFFF\x0001ö\x0001%\x0001\xFFFF\x0001%\x0001ù\x0001\xFFFF");
    private static readonly short[] DFA16_eof = DFA.UnpackEncodedString("ú\xFFFF");
    private static readonly char[] DFA16_min = DFA.UnpackEncodedStringToUnsignedChars("\x0001\t\x0001!\x0001&\x0001|\b!\x0006\xFFFF\x0001c\x0001f\x0001l\x0001w\x0001a\x0001e\x0001h\x0001o\x0001r\x0002a\x0002e\x00010\x0001s\x00010\x0002\xFFFF\x0001:\x0001\xFFFF\x0001\0\x0001.\x0001\xFFFF\x0002!\x0002\xFFFF\x0003!\x0001\xFFFF\x0001!\x0001\xFFFF\x0001!\x0004\xFFFF\x0001!\x0001\0\x0002\xFFFF\x00010\x0001t\x0001p\x00020\x0001e\x0001i\x0001s\x0001l\x0001f\x0001i\x0001r\x0001e\x0001s\x0001i\x0001t\x0001r\x0001p\x0001y\x00010\x0001x\x0001s\x0001d\x0001\xFFFF\x0001i\x0001\0\x0003\xFFFF\x0001\0\x0001\"\v\xFFFF\x0001!\x0001\xFFFF\x0002\0\x0001\xFFFF\x0001o\x0001\xFFFF\x0001i\x0001o\x0002\xFFFF\x0001e\x0001m\x0001t\x0001e\x0001c\x0001t\x0001l\x0001a\x0001l\x0001e\x0001a\x0001i\x0001s\x0001u\x00010\x0001e\x00010\x0001\xFFFF\x0001t\x0001s\x0001e\x0001n\x0001+\x0001\0\x0001\xFFFF\x0001\0\x0001!\x0001c\x0001o\x0001r\x00010\x0001e\x0001c\x00010\x0001h\x0001i\x0001e\x0001u\x0001e\x0001a\x0001k\x0001n\x0001e\x0001r\x0001\xFFFF\x0001o\x0001\xFFFF\x00010\x0001a\x00010\x0001g\x00010\x0001\0\x0001i\x0001n\x0001t\x0001\xFFFF\x0001n\x0001h\x0001\xFFFF\x00010\x0001n\x0001c\x0001l\x00010\x0001c\x00010\x0001e\x00010\x0001n\x0001f\x0001\xFFFF\x0001g\x0001\xFFFF\x00010\x0001a\x00020\x0001t\x00010\x0001\xFFFF\x0001u\x0002t\x0001\xFFFF\x0001h\x0001\xFFFF\x0001s\x00010\x0001\xFFFF\x00020\x0001e\x0001\xFFFF\x0001t\x0002\xFFFF\x00010\x0001\xFFFF\x0001e\x0001i\x00020\x0001s\x0003\xFFFF\x00010\x0001i\x0001o\x0001\xFFFF\x00010\x0001o\x0002\xFFFF\x0001o\x0001\xFFFF\x0001o\x0001f\x0001\xFFFF\x0001n\x0001b\x0001n\x00010\x0001o\x0001j\x00010\x0001\xFFFF\x0001f\x0001e\x0001\xFFFF\x00010\x0001c\x0001\xFFFF\x0001t\x00010\x0001\xFFFF");
    private static readonly char[] DFA16_max = DFA.UnpackEncodedStringToUnsignedChars("\x0001 \x0001|\x0001&\t|\x0006\xFFFF\x0001s\x0001n\x0001l\x0001w\x0001o\x0001e\x0001h\x0001o\x0001u\x0001e\x0001a\x0001y\x0001e\x0001z\x0001s\x00019\x0002\xFFFF\x0001:\x0001\xFFFF\x0001\xFFFF\x0001e\x0001\xFFFF\x0002|\x0002\xFFFF\x0003|\x0001\xFFFF\x0001|\x0001\xFFFF\x0001|\x0004\xFFFF\x0001|\x0001\xFFFF\x0002\xFFFF\x0001z\x0001t\x0001p\x0002z\x0001s\x0001i\x0001t\x0001n\x0001f\x0001i\x0001r\x0001e\x0001s\x0001i\x0001t\x0001r\x0001p\x0001y\x0001z\x0001x\x0001s\x0001d\x0001\xFFFF\x0001i\x0001\xFFFF\x0003\xFFFF\x0001\xFFFF\x0001v\v\xFFFF\x0001|\x0001\xFFFF\x0002\xFFFF\x0001\xFFFF\x0001o\x0001\xFFFF\x0001i\x0001o\x0002\xFFFF\x0001e\x0001m\x0001t\x0001e\x0001c\x0001t\x0001l\x0001a\x0001l\x0001e\x0001a\x0001i\x0001s\x0001u\x0001z\x0001e\x0001z\x0001\xFFFF\x0001t\x0001s\x0001e\x0001n\x00019\x0001\xFFFF\x0001\xFFFF\x0001\xFFFF\x0001|\x0001c\x0001o\x0001r\x0001z\x0001e\x0001c\x0001z\x0001h\x0001i\x0001e\x0001u\x0001e\x0001a\x0001k\x0001n\x0001e\x0001r\x0001\xFFFF\x0001o\x0001\xFFFF\x0001z\x0001a\x0001z\x0001g\x00019\x0001\xFFFF\x0001i\x0001n\x0001t\x0001\xFFFF\x0001n\x0001h\x0001\xFFFF\x0001z\x0001n\x0001c\x0001l\x0001z\x0001c\x0001z\x0001e\x0001z\x0001n\x0001f\x0001\xFFFF\x0001g\x0001\xFFFF\x0001z\x0001a\x0002z\x0001t\x0001z\x0001\xFFFF\x0001u\x0002t\x0001\xFFFF\x0001h\x0001\xFFFF\x0001s\x0001z\x0001\xFFFF\x0002z\x0001e\x0001\xFFFF\x0001t\x0002\xFFFF\x0001z\x0001\xFFFF\x0001e\x0001i\x0002z\x0001s\x0003\xFFFF\x0001z\x0001i\x0001o\x0001\xFFFF\x0001z\x0001o\x0002\xFFFF\x0001o\x0001\xFFFF\x0001o\x0001f\x0001\xFFFF\x0001n\x0001b\x0001n\x0001z\x0001o\x0001j\x0001z\x0001\xFFFF\x0001f\x0001e\x0001\xFFFF\x0001z\x0001c\x0001\xFFFF\x0001t\x0001z\x0001\xFFFF");
    private static readonly short[] DFA16_accept = DFA.UnpackEncodedString("\f\xFFFF\x0001\x0011\x0001\x0012\x0001\x0013\x0001\x0014\x0001\x0015\x0001\x0016\x0010\xFFFF\x00017\x00018\x0001\xFFFF\x0001;\x0002\xFFFF\x0001C\x0002\xFFFF\x0001\x0002\x0001D\x0003\xFFFF\x0001\x0005\x0001\xFFFF\x0001\b\x0001\xFFFF\x0001\t\x0001\f\x0001\r\x0001\x000E\x0002\xFFFF\x0001\x000F\x0001\x0010\x0017\xFFFF\x00013\x0002\xFFFF\x00016\x0001:\x00019\x0002\xFFFF\x0001<\x0001=\x0001>\x0001?\x0001\x0001\x0001\x0006\x0001\x0003\x0001\x0004\x0001\a\x0001\n\x0001\v\x0001\xFFFF\x0001A\x0002\xFFFF\x0001B\x0001\xFFFF\x0001\x0017\x0002\xFFFF\x0001\x0019\x0001 \x0011\xFFFF\x00011\x0006\xFFFF\x0001@\x0013\xFFFF\x0001$\x0001\xFFFF\x0001)\t\xFFFF\x0001\x001A\x0002\xFFFF\x0001\x001C\v\xFFFF\x00015\x0001\xFFFF\x0001-\x0006\xFFFF\x0001*\x0003\xFFFF\x0001\x001E\x0001\xFFFF\x0001\"\x0002\xFFFF\x0001#\x0003\xFFFF\x00014\x0001\xFFFF\x00012\x0001\x0018\x0001\xFFFF\x0001\x001B\x0005\xFFFF\x0001/\x0001(\x0001'\x0003\xFFFF\x0001.\x0002\xFFFF\x0001\x001D\x0001\x001F\x0001\xFFFF\x0001,\x0002\xFFFF\x0001!\a\xFFFF\x0001&\x0002\xFFFF\x00010\x0002\xFFFF\x0001%\x0002\xFFFF\x0001+");
    private static readonly short[] DFA16_special = DFA.UnpackEncodedString("&\xFFFF\x0001\x0006\x0012\xFFFF\x0001\x0002\x001B\xFFFF\x0001\x0001\x0003\xFFFF\x0001\x0004\x000E\xFFFF\x0001\0\x0001\x0003\x001E\xFFFF\x0001\a\x0001\xFFFF\x0001\x0005\x001A\xFFFF\x0001\bT\xFFFF}>");
    private const string DFA16_eotS = "\x0001\xFFFF\x0001+\x0002,\x00010\x00012\x00014\x00015\x00016\x00017\x0001:\x0001;\x0006\xFFFF\r%\x0001S\x0001%\x0001V\x0002\xFFFF\x0001X\x0001\xFFFF\x0001[\x0001]\x0001\xFFFF\x0001_\x0001`\x0002\xFFFF\x0001a\x0001b\x0001c\x0001\xFFFF\x0001d\x0001\xFFFF\x0001e\x0004\xFFFF\x0001g\x0001,\x0002\xFFFF\x0001l\x0002%\x0001o\x0001p\x000E%\x0001\x0082\x0003%\x0001\xFFFF\x0001%\x0001^\x0003\xFFFF\x0001[\f\xFFFF\x0001g\x0001\xFFFF\x0002,\x0001\xFFFF\x0001%\x0001\xFFFF\x0002%\x0002\xFFFF\x000E%\x0001\x009D\x0001%\x0001\x009F\x0001\xFFFF\x0004%\x0001\x0089\x0001^\x0001\xFFFF\x0001[\x0001j\x0003%\x0001©\x0002%\x0001¬\n%\x0001\xFFFF\x0001%\x0001\xFFFF\x0001¸\x0001%\x0001º\x0001%\x0001\x0089\x0001^\x0003%\x0001\xFFFF\x0002%\x0001\xFFFF\x0001Á\x0003%\x0001Å\x0001%\x0001Ç\x0001%\x0001Ê\x0002%\x0001\xFFFF\x0001%\x0001\xFFFF\x0001Î\x0001%\x0001Ð\x0001Ñ\x0001%\x0001Ó\x0001\xFFFF\x0003%\x0001\xFFFF\x0001%\x0001\xFFFF\x0001%\x0001Ù\x0001\xFFFF\x0001Ú\x0001Û\x0001%\x0001\xFFFF\x0001%\x0002\xFFFF\x0001ß\x0001\xFFFF\x0002%\x0001â\x0001ã\x0001%\x0003\xFFFF\x0001å\x0002%\x0001\xFFFF\x0001è\x0001%\x0002\xFFFF\x0001%\x0001\xFFFF\x0002%\x0001\xFFFF\x0003%\x0001ð\x0002%\x0001ó\x0001\xFFFF\x0002%\x0001\xFFFF\x0001ö\x0001%\x0001\xFFFF\x0001%\x0001ù\x0001\xFFFF";
    private const string DFA16_eofS = "ú\xFFFF";
    private const string DFA16_minS = "\x0001\t\x0001!\x0001&\x0001|\b!\x0006\xFFFF\x0001c\x0001f\x0001l\x0001w\x0001a\x0001e\x0001h\x0001o\x0001r\x0002a\x0002e\x00010\x0001s\x00010\x0002\xFFFF\x0001:\x0001\xFFFF\x0001\0\x0001.\x0001\xFFFF\x0002!\x0002\xFFFF\x0003!\x0001\xFFFF\x0001!\x0001\xFFFF\x0001!\x0004\xFFFF\x0001!\x0001\0\x0002\xFFFF\x00010\x0001t\x0001p\x00020\x0001e\x0001i\x0001s\x0001l\x0001f\x0001i\x0001r\x0001e\x0001s\x0001i\x0001t\x0001r\x0001p\x0001y\x00010\x0001x\x0001s\x0001d\x0001\xFFFF\x0001i\x0001\0\x0003\xFFFF\x0001\0\x0001\"\v\xFFFF\x0001!\x0001\xFFFF\x0002\0\x0001\xFFFF\x0001o\x0001\xFFFF\x0001i\x0001o\x0002\xFFFF\x0001e\x0001m\x0001t\x0001e\x0001c\x0001t\x0001l\x0001a\x0001l\x0001e\x0001a\x0001i\x0001s\x0001u\x00010\x0001e\x00010\x0001\xFFFF\x0001t\x0001s\x0001e\x0001n\x0001+\x0001\0\x0001\xFFFF\x0001\0\x0001!\x0001c\x0001o\x0001r\x00010\x0001e\x0001c\x00010\x0001h\x0001i\x0001e\x0001u\x0001e\x0001a\x0001k\x0001n\x0001e\x0001r\x0001\xFFFF\x0001o\x0001\xFFFF\x00010\x0001a\x00010\x0001g\x00010\x0001\0\x0001i\x0001n\x0001t\x0001\xFFFF\x0001n\x0001h\x0001\xFFFF\x00010\x0001n\x0001c\x0001l\x00010\x0001c\x00010\x0001e\x00010\x0001n\x0001f\x0001\xFFFF\x0001g\x0001\xFFFF\x00010\x0001a\x00020\x0001t\x00010\x0001\xFFFF\x0001u\x0002t\x0001\xFFFF\x0001h\x0001\xFFFF\x0001s\x00010\x0001\xFFFF\x00020\x0001e\x0001\xFFFF\x0001t\x0002\xFFFF\x00010\x0001\xFFFF\x0001e\x0001i\x00020\x0001s\x0003\xFFFF\x00010\x0001i\x0001o\x0001\xFFFF\x00010\x0001o\x0002\xFFFF\x0001o\x0001\xFFFF\x0001o\x0001f\x0001\xFFFF\x0001n\x0001b\x0001n\x00010\x0001o\x0001j\x00010\x0001\xFFFF\x0001f\x0001e\x0001\xFFFF\x00010\x0001c\x0001\xFFFF\x0001t\x00010\x0001\xFFFF";
    private const string DFA16_maxS = "\x0001 \x0001|\x0001&\t|\x0006\xFFFF\x0001s\x0001n\x0001l\x0001w\x0001o\x0001e\x0001h\x0001o\x0001u\x0001e\x0001a\x0001y\x0001e\x0001z\x0001s\x00019\x0002\xFFFF\x0001:\x0001\xFFFF\x0001\xFFFF\x0001e\x0001\xFFFF\x0002|\x0002\xFFFF\x0003|\x0001\xFFFF\x0001|\x0001\xFFFF\x0001|\x0004\xFFFF\x0001|\x0001\xFFFF\x0002\xFFFF\x0001z\x0001t\x0001p\x0002z\x0001s\x0001i\x0001t\x0001n\x0001f\x0001i\x0001r\x0001e\x0001s\x0001i\x0001t\x0001r\x0001p\x0001y\x0001z\x0001x\x0001s\x0001d\x0001\xFFFF\x0001i\x0001\xFFFF\x0003\xFFFF\x0001\xFFFF\x0001v\v\xFFFF\x0001|\x0001\xFFFF\x0002\xFFFF\x0001\xFFFF\x0001o\x0001\xFFFF\x0001i\x0001o\x0002\xFFFF\x0001e\x0001m\x0001t\x0001e\x0001c\x0001t\x0001l\x0001a\x0001l\x0001e\x0001a\x0001i\x0001s\x0001u\x0001z\x0001e\x0001z\x0001\xFFFF\x0001t\x0001s\x0001e\x0001n\x00019\x0001\xFFFF\x0001\xFFFF\x0001\xFFFF\x0001|\x0001c\x0001o\x0001r\x0001z\x0001e\x0001c\x0001z\x0001h\x0001i\x0001e\x0001u\x0001e\x0001a\x0001k\x0001n\x0001e\x0001r\x0001\xFFFF\x0001o\x0001\xFFFF\x0001z\x0001a\x0001z\x0001g\x00019\x0001\xFFFF\x0001i\x0001n\x0001t\x0001\xFFFF\x0001n\x0001h\x0001\xFFFF\x0001z\x0001n\x0001c\x0001l\x0001z\x0001c\x0001z\x0001e\x0001z\x0001n\x0001f\x0001\xFFFF\x0001g\x0001\xFFFF\x0001z\x0001a\x0002z\x0001t\x0001z\x0001\xFFFF\x0001u\x0002t\x0001\xFFFF\x0001h\x0001\xFFFF\x0001s\x0001z\x0001\xFFFF\x0002z\x0001e\x0001\xFFFF\x0001t\x0002\xFFFF\x0001z\x0001\xFFFF\x0001e\x0001i\x0002z\x0001s\x0003\xFFFF\x0001z\x0001i\x0001o\x0001\xFFFF\x0001z\x0001o\x0002\xFFFF\x0001o\x0001\xFFFF\x0001o\x0001f\x0001\xFFFF\x0001n\x0001b\x0001n\x0001z\x0001o\x0001j\x0001z\x0001\xFFFF\x0001f\x0001e\x0001\xFFFF\x0001z\x0001c\x0001\xFFFF\x0001t\x0001z\x0001\xFFFF";
    private const string DFA16_acceptS = "\f\xFFFF\x0001\x0011\x0001\x0012\x0001\x0013\x0001\x0014\x0001\x0015\x0001\x0016\x0010\xFFFF\x00017\x00018\x0001\xFFFF\x0001;\x0002\xFFFF\x0001C\x0002\xFFFF\x0001\x0002\x0001D\x0003\xFFFF\x0001\x0005\x0001\xFFFF\x0001\b\x0001\xFFFF\x0001\t\x0001\f\x0001\r\x0001\x000E\x0002\xFFFF\x0001\x000F\x0001\x0010\x0017\xFFFF\x00013\x0002\xFFFF\x00016\x0001:\x00019\x0002\xFFFF\x0001<\x0001=\x0001>\x0001?\x0001\x0001\x0001\x0006\x0001\x0003\x0001\x0004\x0001\a\x0001\n\x0001\v\x0001\xFFFF\x0001A\x0002\xFFFF\x0001B\x0001\xFFFF\x0001\x0017\x0002\xFFFF\x0001\x0019\x0001 \x0011\xFFFF\x00011\x0006\xFFFF\x0001@\x0013\xFFFF\x0001$\x0001\xFFFF\x0001)\t\xFFFF\x0001\x001A\x0002\xFFFF\x0001\x001C\v\xFFFF\x00015\x0001\xFFFF\x0001-\x0006\xFFFF\x0001*\x0003\xFFFF\x0001\x001E\x0001\xFFFF\x0001\"\x0002\xFFFF\x0001#\x0003\xFFFF\x00014\x0001\xFFFF\x00012\x0001\x0018\x0001\xFFFF\x0001\x001B\x0005\xFFFF\x0001/\x0001(\x0001'\x0003\xFFFF\x0001.\x0002\xFFFF\x0001\x001D\x0001\x001F\x0001\xFFFF\x0001,\x0002\xFFFF\x0001!\a\xFFFF\x0001&\x0002\xFFFF\x00010\x0002\xFFFF\x0001%\x0002\xFFFF\x0001+";
    private const string DFA16_specialS = "&\xFFFF\x0001\x0006\x0012\xFFFF\x0001\x0002\x001B\xFFFF\x0001\x0001\x0003\xFFFF\x0001\x0004\x000E\xFFFF\x0001\0\x0001\x0003\x001E\xFFFF\x0001\a\x0001\xFFFF\x0001\x0005\x001A\xFFFF\x0001\bT\xFFFF}>";
    private static readonly short[][] DFA16_transition;

    static DFA16()
    {
      int length = ByDScriptingLanguageLexer.DFA16.DFA16_transitionS.Length;
      ByDScriptingLanguageLexer.DFA16.DFA16_transition = new short[length][];
      for (int index = 0; index < length; ++index)
        ByDScriptingLanguageLexer.DFA16.DFA16_transition[index] = DFA.UnpackEncodedString(ByDScriptingLanguageLexer.DFA16.DFA16_transitionS[index]);
    }

    public DFA16(BaseRecognizer recognizer, SpecialStateTransitionHandler specialStateTransition)
      : base(specialStateTransition)
    {
      this.recognizer = recognizer;
      this.decisionNumber = 16;
      this.eot = ByDScriptingLanguageLexer.DFA16.DFA16_eot;
      this.eof = ByDScriptingLanguageLexer.DFA16.DFA16_eof;
      this.min = ByDScriptingLanguageLexer.DFA16.DFA16_min;
      this.max = ByDScriptingLanguageLexer.DFA16.DFA16_max;
      this.accept = ByDScriptingLanguageLexer.DFA16.DFA16_accept;
      this.special = ByDScriptingLanguageLexer.DFA16.DFA16_special;
      this.transition = ByDScriptingLanguageLexer.DFA16.DFA16_transition;
    }

    public override string Description
    {
      get
      {
        return "1:1: Tokens : ( LAMBDA_OPS | ASSIGN_OPS | AND | OR | NOT | EQ | NE | GT | LTT | GE | LE | ADD | SUB | MUL | DIV | MOD | LPAREN | RPAREN | LCURLB | RCURLB | LBRCKT | RBRCKT | AS | IMPORT | IF | ELSE | SWITCH | CASE | DEFAULT | WHILE | FOREACH | INN | CONTINUE | BREAK | RAISE | VAR | COLLOF | ELTSOF | TYPOF | RETURN | TRY | CATCH | BUSINESSOBJECT | MESSAGE | NODE | ELEMENT | RAISES | ASSOCIATION | TO | ACTION | CARD_N | USING | TEXTT | DOT | COMMA | DELIM | COLON | COLON2 | ABSLID | BROKEN_STRLIT | STRLIT | DECLIT | NUMLIT | BROKEN_NUMLIT | SL_COMMENT | ML_COMMENT | WS | ILLEGAL_OPERATOR );";
      }
    }

    public override void Error(NoViableAltException nvae)
    {
    }
  }
}
