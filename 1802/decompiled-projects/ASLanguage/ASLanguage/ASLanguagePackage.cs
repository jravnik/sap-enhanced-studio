﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ASLanguage.ASLanguagePackage
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Shell;
using SAP.Copernicus.ABAPScriptLanguage;
using SAP.Copernicus.ABAPScriptLanguage.Debugger;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Scripting;
using System;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.ASLanguage
{
  [Guid("e1916f8d-ac6d-499d-9efb-4317d7a43619")]
  [ProvideLanguageService(typeof (ASLanguageService), "ByD script", 106, CodeSense = true, CodeSenseDelay = 1500, EnableAsyncCompletion = true, EnableCommenting = true, EnableFormatSelection = true, EnableLineNumbers = true, MatchBraces = true, RequestStockColors = true, ShowCompletion = true)]
  [ProvideDebugLanguage("Test Language 8121", "{DACE1656-6C04-45E2-9D02-3C3E189ECA71}", "{A80A2A43-9839-46C5-8CFF-8A039F8F047B}")]
  [ProvideLoadKey("Standard", "1.0", "SAP.Copernicus.ABAPScriptLanguage", "SAP AG", 113)]
  [ProvideDebugEngine("ABSL Debugger", null, typeof (DebugEngine), "{A80A2A43-9839-46C5-8CFF-8A039F8F047B}")]
  [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
  [PackageRegistration(UseManagedResourcesOnly = true)]
  [ProvideService(typeof (ASLanguageService), ServiceName = "ByD script")]
  [ProvideLanguageExtension(typeof (ASLanguageService), ".absl")]
  public sealed class ASLanguagePackage : Package, IOleComponent
  {
    private uint m_componentID;
    private ASLanguageService languageService;

    public ASLanguagePackage()
    {
      Trace.WriteLine(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "Entering constructor for: {0}", new object[1]
      {
        (object) this.ToString()
      }));
    }

    protected override void Initialize()
    {
      Trace.WriteLine(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "Entering Initialize() of: {0}", new object[1]
      {
        (object) this.ToString()
      }));
      base.Initialize();
      this.languageService = new ASLanguageService();
      this.languageService.SetSite((object) this);
      ((IServiceContainer)this).AddService(typeof (ASLanguageService), (object) this.languageService, true);
      FileHandlerRegistry.INSTANCE.AddPreFileDeletionHandler(ScriptLanguageType.ByDScript.FileExtension.Replace(".", ""), new EventHandlerOnPreFileDeletion(this.languageService.OnScriptFileDeletion));
      FileHandlerRegistry.INSTANCE.AddHandler(ScriptLanguageType.ByDScript.FileExtension.Replace(".", ""), new EventHandlerOnFileSave(this.languageService.OnScriptFileSave));
      IOleComponentManager service = this.GetService(typeof (SOleComponentManager)) as IOleComponentManager;
      if ((int) this.m_componentID != 0 || service == null)
        return;
      OLECRINFO[] pcrinfo = new OLECRINFO[1];
      pcrinfo[0].cbSize = (uint) Marshal.SizeOf(typeof (OLECRINFO));
      pcrinfo[0].grfcrf = 3U;
      pcrinfo[0].grfcadvf = 7U;
      pcrinfo[0].uIdleTimeInterval = 1000U;
      service.FRegisterComponent((IOleComponent) this, pcrinfo, out this.m_componentID);
    }

    protected override void Dispose(bool disposing)
    {
      FileHandlerRegistry.INSTANCE.RemoveHandler(new EventHandlerOnFileSave(this.languageService.OnScriptFileSave));
      FileHandlerRegistry.INSTANCE.RemoveHandler(new EventHandlerOnFileDeletion(this.languageService.OnScriptFileDeletion));
      if ((int) this.m_componentID != 0)
      {
        IOleComponentManager service = this.GetService(typeof (SOleComponentManager)) as IOleComponentManager;
        if (service != null)
          service.FRevokeComponent(this.m_componentID);
        this.m_componentID = 0U;
      }
      base.Dispose(disposing);
    }

    public static ASLanguageService GetLanguageService()
    {
      return (ASLanguageService) Package.GetGlobalService(typeof (ASLanguageService));
    }

    public int FDoIdle(uint grfidlef)
    {
      if (this.languageService != null)
        this.languageService.OnIdle(((int) grfidlef & 1) != 0);
      return 0;
    }

    public int FContinueMessageLoop(uint uReason, IntPtr pvLoopData, MSG[] pMsgPeeked)
    {
      return 1;
    }

    public int FPreTranslateMessage(MSG[] pMsg)
    {
      return 0;
    }

    public int FQueryTerminate(int fPromptUser)
    {
      return 1;
    }

    public int FReserved1(uint dwReserved, uint message, IntPtr wParam, IntPtr lParam)
    {
      return 1;
    }

    public IntPtr HwndGetWindow(uint dwWhich, uint dwReserved)
    {
      return IntPtr.Zero;
    }

    public void OnActivationChange(IOleComponent pic, int fSameComponent, OLECRINFO[] pcrinfo, int fHostIsActivating, OLECHOSTINFO[] pchostinfo, uint dwReserved)
    {
    }

    public void OnAppActivate(int fActive, uint dwOtherThreadID)
    {
    }

    public void OnEnterState(uint uStateID, int fEnter)
    {
    }

    public void OnLoseActivation()
    {
    }

    public void Terminate()
    {
    }
  }
}
