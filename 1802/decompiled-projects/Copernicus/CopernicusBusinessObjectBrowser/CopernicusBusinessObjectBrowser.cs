﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Modeling;
using Microsoft.VisualStudio.Modeling.Validation;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Extension.UIDesignerHelper;
using SAP.Copernicus.Model;
using SAP.Copernicus.Model.BusinessObject;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

namespace CopernicusBusinessObjectBrowser
{
  public class CopernicusBusinessObjectBrowser : UserControl
  {
    private RepositoryDataSet repositoryDataSet = RepositoryDataCache.GetInstance().RepositoryDataSet;
    private TreeNode lastOpened = new TreeNode();
    private TreeNode lastOpenedISI = new TreeNode();
    private TreeNode forRightClickSelected = new TreeNode();
    private TreeNode[] openedBO = new TreeNode[CopernicusBusinessObjectBrowser.openCountBO];
    private TreeNode[] closedBO = new TreeNode[CopernicusBusinessObjectBrowser.closedCountBO];
    private TreeNode[] openSelectedDT = new TreeNode[CopernicusBusinessObjectBrowser.openCountDT];
    private TreeNode[] closeSelectedDT = new TreeNode[CopernicusBusinessObjectBrowser.closedCountDT];
    private TreeNode[] openSelectedEO = new TreeNode[CopernicusBusinessObjectBrowser.openCountEO];
    private TreeNode[] closeSelectedEO = new TreeNode[CopernicusBusinessObjectBrowser.closedCountEO];
    private TreeNode[] openedISI = new TreeNode[CopernicusBusinessObjectBrowser.openCountISI];
    private TreeNode[] closedISI = new TreeNode[CopernicusBusinessObjectBrowser.closedCountISI];
    private TreeNode lastSelected = new TreeNode();
    private List<PDI_REUSE_UI> listReuseUIs = new List<PDI_REUSE_UI>();
    private TreeNode reuseUIForRightClickSelected = new TreeNode();
    private const int boImageIndex = 0;
    private const int actionImageIndex = 1;
    private const int functionImageIndex = 2;
    private const int nodeElementImageIndex = 3;
    private const int nodeImageIndex = 4;
    private const int associationImageIndex = 5;
    private const int queryImageIndex = 6;
    private const int dataTypeImageIndex = 7;
    private const int enhancementOptionImageIndex = 8;
    private const int businessConfigurationObjectImageIndex = 9;
    private const int compositionAssociationImageIndex = 10;
    private const int compositionAssociationWithToNImageIndex = 11;
    private const int crossBOAssociationImageIndex = 12;
    private const int crossBOAssociationWithToNImageIndex = 13;
    private const int intraAssociationImageIndex = 14;
    private const int intraAssociationWithToNImageIndex = 15;
    private const int dependentObjectAssociationImageIndex = 16;
    private const int deprecatedOpenedEntityImageIndex = 17;
    private const int queryParameterImageIndex = 18;
    private const int webServiceImageIndex = 19;
    private const int DeprecatedAssociationImageIndex = 20;
    private const int DeprecatedBusinessObjectImageIndex = 21;
    private const int DeprecatedCompositionAssociationImageIndex = 22;
    private const int DeprecatedCrossBOAssociationImageIndex = 23;
    private const int DeprecatedCrossBOAssociationWithToNImageIndex = 24;
    private const int DeprecatedDependendObjectAssociationImageIndex = 25;
    private const int DeprecatedElementImageIndex = 26;
    private const int DeprecatedInternalAssociationImageIndex = 27;
    private const int DeprecatedInternalAssociationWithToNImageIndex = 28;
    private const int DeprecatedNodeImageIndex = 29;
    private const int ReadOnlyAssociationImageIndex = 30;
    private const int ReadOnlyBusinessObjectImageIndex = 31;
    private const int ReadOnlyCrossBoAssociationImageIndex = 32;
    private const int ReadOnlyCrossBoAssociationWithToNImageIndex = 33;
    private const int ReadOnlyDependendObjectAssociationImageIndex = 34;
    private const int ReadOnlyElementImageIndex = 35;
    private const int ReadOnlyInternalAssociationImageIndex = 36;
    private const int ReadOnlyInternalAssociationWithToNImageIndex = 37;
    private const int ReadOnlyNodeImageIndex = 38;
    private const int DeprecatedActionImageIndex = 39;
    private const int DeprecatedQueryImageIndex = 40;
    private const int ExtensibleNodeImageIndex = 41;
    private const int KeyElementImageIndex = 42;
    private const int InboundServiceInterfaceOperationImageIndex = 43;
    private const int actionParameterImageIndex = 44;
    private const int helpImageIndex = 45;
    private static int openCountBO;
    private static int closedCountBO;
    private static int openCountDT;
    private static int closedCountDT;
    private static int openCountEO;
    private static int closedCountEO;
    private static int openCountISI;
    private static int closedCountISI;
    private static int deprecationRebuildCount;
    private bool bCancel;
    private bool isDeprecationRebuild;
    private bool isNextRebuild;
    private bool isBackClicked;
    private bool isBackClickedDT;
    private bool isNextClickedDT;
    private bool isBackClickedEO;
    private bool isNextClickedEO;
    private bool isBackClickedISI;
    private bool isNextClickedISI;
    private int indexOfQuery;
    private int indexOfAssoc;
    private int DeprecationRebuildHelper;
    private int selectedBOElementTabIndex;
    private static CopernicusBusinessObjectBrowser instance;
    private TreeNode lastFoundReuseTreeNode;
    private bool hasFoundLastReuseTreeNode;
    private IContainer components;
    private ImageList imageList1;
    private ToolStrip miniToolStrip;
    private ContextMenuStrip contextMenuStripSUBelement;
    private ToolStripMenuItem copyElementNameToolStripMenuItem1;
    private ToolStripMenuItem copySelectedPathToolStripMenuItem;
    private ToolStripMenuItem copyImportNamespaceToolStripMenuItem1;
    private ContextMenuStrip contextMenuStripFirstLevel;
    private ToolStripMenuItem copyElementNameToolStripMenuItem2;
    private ToolStripMenuItem copyNamespaceToolStripMenuItem;
    private ContextMenuStrip contextMenuStripOP;
    private ToolStripMenuItem copyEntityNameToolStripMenuItem;
    private ContextMenuStrip contextMenuStripQuery;
    private ToolStripMenuItem toolStripMenuItem1;
    private ToolStripMenuItem toolStripMenuItem2;
    private ToolStripMenuItem toolStripMenuItem3;
    private ToolStripMenuItem executeQueryToolStripMenuItem;
    private TabPage tabReuseUIs;
    private SplitContainer splitContainer5;
    private RichTextBox richTextBoxReuseUIs;
    private TreeView treeViewReuseUIs;
    private TabControl tabControl2;
    private TabPage tabPage1;
    private WebBrowser webBrowserReuseUIs;
    private WebBrowser webBrowserEC;
    private ToolStrip toolStrip1;
    private ToolStripButton toolStripButton1;
    private ToolStripButton toolStripButton2;
    private ToolStripSeparator toolStripSeparator9;
    private ToolStripSeparator toolStripSeparator10;
    private ToolStripComboBox toolStripComboBoxSearchReuseUI;
    private ToolStripButton toolStripButtonSearchReuseUI;
    private TabPage tabPageInboundServiceInterfaces;
    private SplitContainer splitContainer4;
    private RichTextBox richTextBoxInfoBoxISI;
    private TreeView treeViewISI;
    private WebBrowser webBrowserISI;
    private ToolStrip toolStrip5ISI;
    private ToolStripButton toolStripButtonBackISI;
    private ToolStripButton toolStripButtonNextISI;
    private ToolStripSeparator toolStripSeparator7;
    private ToolStripButton toolStripButtonHorizontalISI;
    private ToolStripButton toolStripButtonVerticalISI;
    private ToolStripSeparator toolStripSeparator8;
    private ToolStripComboBox toolStripComboBoxSearchISI;
    private ToolStripButton toolStripButtonSearchISI;
    private ToolStripLabel toolStripLabelFilterISI;
    private ToolStripComboBox toolStripComboBoxNamespaceISI;
    private ToolStripButton toolStripButtonClearISI;
    private ToolStripButton toolStripButtonHelpISI;
    private TabPage tabPageEnhancementOptions;
    private SplitContainer splitContainer3;
    private RichTextBox richTextBoxInfoBoxEO;
    private TreeView treeViewEO;
    private WebBrowser webBrowserEO;
    private ToolStrip toolStrip4EO;
    private ToolStripButton toolStripButtonBackEO;
    private ToolStripButton toolStripButtonNextEO;
    private ToolStripSeparator toolStripSeparator5;
    private ToolStripButton toolStripButtonHorizontalWindowsEO;
    private ToolStripButton toolStripButtonVerticalWindowsEO;
    private ToolStripSeparator toolStripSeparator6;
    private ToolStripComboBox toolStripComboBoxSearchEO;
    private ToolStripButton toolStripButtonSearchEO;
    private ToolStripLabel toolStripLabelFilterEO;
    private ToolStripComboBox toolStripComboBoxNamespaceEO;
    private ToolStripButton toolStripButtonClearEO;
    private ToolStripButton toolStripButtonHelpEO;
    private TabPage tabPageDataTypes;
    private SplitContainer splitContainer2;
    private RichTextBox richTextBoxInfoBoxDT;
    private TreeView treeViewDT;
    private WebBrowser webBrowserDT;
    private ToolStrip toolStrip3DT;
    private ToolStripButton toolStripButtonBackDT;
    private ToolStripButton toolStripButtonNextDT;
    private ToolStripSeparator toolStripSeparator3;
    private ToolStripButton toolStripButtonHorizontalWindowsDT;
    private ToolStripButton toolStripButtonVerticalWindowsDT;
    private ToolStripSeparator toolStripSeparator4;
    private ToolStripComboBox toolStripComboBoxSearchDT;
    private ToolStripButton toolStripButtonSearchDT;
    private ToolStripLabel toolStripLabelFilterDT;
    private ToolStripComboBox toolStripComboBoxNamespaceDT;
    private ToolStripComboBox toolStripComboBoxUsageCat;
    private ToolStripButton toolStripButtonClearDT;
    private ToolStripButton toolStripButton1HelpDT;
    private TabPage tabPageBusinessObjectBrowser;
    private SplitContainer splitContainer1;
    private RichTextBox richTextBoxInfoBoxBO;
    private TreeView treeViewBO;
    private TabControl tabControlBO;
    private TabPage tabPageDescription;
    private WebBrowser webBrowserBO;
    private ToolStrip toolStrip2BO;
    private ToolStripComboBox toolStripComboBoxSearch;
    private ToolStripButton toolStripButtonSearch;
    private ToolStripLabel toolStripLabelFilterBO;
    private ToolStripComboBox toolStripComboBoxNamespace;
    private ToolStripComboBox toolStripComboBoxDepUnit;
    private ToolStripButton toolStripButtonClear;
    private ToolStripButton toolStripButtonHelpBO;
    private ToolStrip toolStrip1BO;
    private ToolStripButton toolStripButtonBack;
    private ToolStripButton toolStripButtonNext;
    private ToolStripSeparator toolStripSeparator2;
    private ToolStripButton toolStripButtonActions;
    private ToolStripButton toolStripButtonAssociations;
    private ToolStripButton toolStripButtonDeprecation;
    private ToolStripButton toolStripButtonNodes;
    private ToolStripButton toolStripButtonQueries;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripButton toolStripButtonHorizontalWindows;
    private ToolStripButton toolStripButtonVerticalWindows;
    private TabControl tabControl1;

    public CopernicusBusinessObjectBrowser()
    {
      Trace.WriteLine("CopernicusBusinessObjectBrowser constructor start");
      this.InitializeComponent();
      Trace.WriteLine("CopernicusBusinessObjectBrowser InitializeComponent done, now renderers");
      this.toolStrip1BO.Renderer = (ToolStripRenderer) new CopernicusBusinessObjectBrowser.NewRenderer();
      this.toolStrip3DT.Renderer = (ToolStripRenderer) new CopernicusBusinessObjectBrowser.NewRenderer();
      this.toolStrip4EO.Renderer = (ToolStripRenderer) new CopernicusBusinessObjectBrowser.NewRenderer();
      this.toolStrip5ISI.Renderer = (ToolStripRenderer) new CopernicusBusinessObjectBrowser.NewRenderer();
      RepositoryDataCache.RefreshEventHandlers += new EventHandlerOnRepositoryRefresh(this.OnRepDataCacheRefresh);
      CopernicusBusinessObjectBrowser.instance = this;
      this.Enabled = false;
      this.richTextBoxInfoBoxBO.Visible = true;
      Trace.WriteLine("CopernicusBusinessObjectBrowser constructor end");
    }

    public void OnRepDataCacheRefresh()
    {
      this.Init();
    }

    private void Init()
    {
      Trace.WriteLine("CopernicusBusinessObjectBrowser init () start");
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      service.SetText("Loading repository content...");
      RepositoryViewControl.GetInstance().LogOnConnection();
      this.initializeComboBoxes();
      this.Enabled = true;
      this.richTextBoxInfoBoxBO.Visible = false;
      this.richTextBoxInfoBoxDT.Visible = false;
      this.richTextBoxInfoBoxEO.Visible = false;
      this.richTextBoxInfoBoxISI.Visible = false;
      Trace.WriteLine("CopernicusBusinessObjectBrowser search and init");
      this.SearchAndInitTreeBO();
      this.SearchAndInitDT();
      service.SetText("Ready");
      Trace.WriteLine("CopernicusBusinessObjectBrowser init () end");
    }

    private void Init2()
    {
      Trace.WriteLine("CopernicusBusinessObjectBrowser init2 () start");
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      RepositoryViewControl.GetInstance().LogOnConnection();
      this.initializeComboBoxes();
      this.Enabled = true;
      this.richTextBoxInfoBoxBO.Visible = false;
      this.richTextBoxInfoBoxDT.Visible = false;
      this.richTextBoxInfoBoxEO.Visible = false;
      this.richTextBoxInfoBoxISI.Visible = false;
      Trace.WriteLine("CopernicusBusinessObjectBrowser search and init");
      this.SearchAndInitTreeBO();
      this.SearchAndInitDT();
      Trace.WriteLine("CopernicusBusinessObjectBrowser init2 () end");
    }

    public static CopernicusBusinessObjectBrowser GetInstance()
    {
      return CopernicusBusinessObjectBrowser.instance;
    }

    public void LoggedOff()
    {
      this.Enabled = false;
      this.richTextBoxInfoBoxBO.Visible = true;
      this.richTextBoxInfoBoxDT.Visible = true;
      this.richTextBoxInfoBoxEO.Visible = true;
      this.richTextBoxInfoBoxISI.Visible = true;
      this.toolStripComboBoxSearch.Text = "Search business objects";
      this.toolStripComboBoxSearchDT.Text = "Search data types";
      this.toolStripComboBoxSearchEO.Text = "Search enhancement options";
      this.toolStripComboBoxSearchISI.Text = "Search service interfaces";
      this.toolStripComboBoxDepUnit.Text = "Deployment Unit";
      this.toolStripComboBoxUsageCat.Text = "Usage Category";
      this.toolStripComboBoxNamespace.Text = "Namespace";
      this.toolStripComboBoxNamespaceDT.Text = "Namespace";
      this.toolStripComboBoxNamespaceEO.Text = "Namespace";
      this.toolStripComboBoxNamespaceISI.Text = "Namespace";
      CopernicusBusinessObjectBrowser.openCountBO = 0;
      this.openedBO = new TreeNode[CopernicusBusinessObjectBrowser.openCountBO];
      CopernicusBusinessObjectBrowser.closedCountBO = 0;
      this.closedBO = new TreeNode[CopernicusBusinessObjectBrowser.closedCountBO];
      CopernicusBusinessObjectBrowser.openCountDT = 0;
      this.openSelectedDT = new TreeNode[CopernicusBusinessObjectBrowser.openCountDT];
      CopernicusBusinessObjectBrowser.closedCountDT = 0;
      this.closeSelectedDT = new TreeNode[CopernicusBusinessObjectBrowser.closedCountDT];
      CopernicusBusinessObjectBrowser.openCountEO = 0;
      this.openSelectedEO = new TreeNode[CopernicusBusinessObjectBrowser.openCountEO];
      CopernicusBusinessObjectBrowser.closedCountEO = 0;
      this.closeSelectedEO = new TreeNode[CopernicusBusinessObjectBrowser.closedCountEO];
      CopernicusBusinessObjectBrowser.openCountISI = 0;
      this.openedISI = new TreeNode[CopernicusBusinessObjectBrowser.openCountISI];
      CopernicusBusinessObjectBrowser.closedCountISI = 0;
      this.closedISI = new TreeNode[CopernicusBusinessObjectBrowser.closedCountISI];
      this.toolStripButtonBack.Enabled = false;
      this.toolStripButtonNext.Enabled = false;
      this.toolStripButtonBackDT.Enabled = false;
      this.toolStripButtonNextDT.Enabled = false;
      this.toolStripButtonBackEO.Enabled = false;
      this.toolStripButtonNextEO.Enabled = false;
      this.toolStripButtonBackISI.Enabled = false;
      this.toolStripButtonNextISI.Enabled = false;
      this.toolStripComboBoxSearchISI.AutoCompleteCustomSource.Clear();
    }

    private void CopernicusBusinessObjectBrowser_Load(object sender, EventArgs e)
    {
      if (!RepositoryViewControl.GetInstance().isLoggedOnGet())
        return;
      this.Init2();
    }

    private bool isDeprecated(TreeNode trNode)
    {
      if (trNode.Tag.GetType() == typeof (SAP.Copernicus.Model.BusinessObject.BusinessObject))
      {
        SAP.Copernicus.Model.BusinessObject.BusinessObject tag = (SAP.Copernicus.Model.BusinessObject.BusinessObject) trNode.Tag;
        if (tag.PublicSolutionModel != null)
        {
          int releaseStatusCode = (int) tag.PublicSolutionModel.ReleaseStatusCode;
          if (tag.PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")
            return true;
        }
      }
      else if (trNode.Tag.GetType() == typeof (BusinessObject_QueryParameter))
      {
        BusinessObject_QueryParameter tag = (BusinessObject_QueryParameter) trNode.Tag;
        if (tag.PublicSolutionModel != null)
        {
          int releaseStatusCode = (int) tag.PublicSolutionModel.ReleaseStatusCode;
          if (tag.PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")
            return true;
        }
      }
      else if (trNode.ImageIndex == 39 || trNode.ImageIndex == 26 || (trNode.ImageIndex == 29 || trNode.ImageIndex == 40))
        return true;
      return false;
    }

    private bool ImageIndexIsAssoc(int ImageIndex)
    {
      return ImageIndex == 10 || ImageIndex == 11 || (ImageIndex == 12 || ImageIndex == 13) || (ImageIndex == 14 || ImageIndex == 15 || (ImageIndex == 16 || ImageIndex == 5)) || (ImageIndex == 20 || ImageIndex == 22 || (ImageIndex == 23 || ImageIndex == 24) || (ImageIndex == 25 || ImageIndex == 27 || (ImageIndex == 28 || ImageIndex == 30))) || (ImageIndex == 32 || ImageIndex == 33 || (ImageIndex == 34 || ImageIndex == 36) || ImageIndex == 37);
    }

    private bool ImageIndexIsNode(int ImageIndex)
    {
      return ImageIndex == 4 || ImageIndex == 38 || (ImageIndex == 29 || ImageIndex == 41);
    }

    private void toolStripButtonHorizontalWindows_Click(object sender, EventArgs e)
    {
      this.toolStripButtonHorizontalWindows.Checked = true;
      this.toolStripButtonVerticalWindows.Checked = false;
      this.splitContainer1.Orientation = Orientation.Horizontal;
    }

    private void toolStripButtonVerticalWindows_Click(object sender, EventArgs e)
    {
      this.toolStripButtonHorizontalWindows.Checked = false;
      this.toolStripButtonVerticalWindows.Checked = true;
      this.splitContainer1.Orientation = Orientation.Vertical;
    }

    private void toolStripButtonHorizontalWindowsDT_Click(object sender, EventArgs e)
    {
      this.toolStripButtonHorizontalWindowsDT.Checked = true;
      this.toolStripButtonVerticalWindowsDT.Checked = false;
      this.splitContainer2.Orientation = Orientation.Horizontal;
    }

    private void toolStripButtonVerticalWindowsDT_Click(object sender, EventArgs e)
    {
      this.toolStripButtonHorizontalWindowsDT.Checked = false;
      this.toolStripButtonVerticalWindowsDT.Checked = true;
      this.splitContainer2.Orientation = Orientation.Vertical;
    }

    private void toolStripButtonHorizontalWindowsEO_Click(object sender, EventArgs e)
    {
      this.toolStripButtonHorizontalWindowsEO.Checked = true;
      this.toolStripButtonVerticalWindowsEO.Checked = false;
      this.splitContainer3.Orientation = Orientation.Horizontal;
    }

    private void toolStripButtonVerticalWindowsEO_Click(object sender, EventArgs e)
    {
      this.toolStripButtonHorizontalWindowsEO.Checked = false;
      this.toolStripButtonVerticalWindowsEO.Checked = true;
      this.splitContainer3.Orientation = Orientation.Vertical;
    }

    private void toolStripButtonHorizontalISI_Click(object sender, EventArgs e)
    {
      this.toolStripButtonHorizontalISI.Checked = true;
      this.toolStripButtonVerticalISI.Checked = false;
      this.splitContainer4.Orientation = Orientation.Horizontal;
    }

    private void toolStripButtonVerticalISI_Click(object sender, EventArgs e)
    {
      this.toolStripButtonHorizontalISI.Checked = false;
      this.toolStripButtonVerticalISI.Checked = true;
      this.splitContainer4.Orientation = Orientation.Vertical;
    }

    private void ResizeDropDown(ToolStripComboBox box)
    {
      int num1 = box.DropDownWidth;
      Graphics graphics = box.ComboBox.CreateGraphics();
      Font font = box.Font;
      int num2 = box.Items.Count > box.MaxDropDownItems ? SystemInformation.VerticalScrollBarWidth : 0;
      foreach (string text in box.Items)
      {
        int num3 = (int) graphics.MeasureString(text, font).Width + num2;
        if (num1 < num3)
          num1 = num3;
      }
      box.DropDownWidth = num1;
    }

    private void toolStripComboBoxDepUnit_DropDown(object sender, EventArgs e)
    {
      this.ResizeDropDown(this.toolStripComboBoxDepUnit);
    }

    private void toolStripComboBoxNamespace_DropDown(object sender, EventArgs e)
    {
      this.ResizeDropDown(this.toolStripComboBoxNamespace);
    }

    private void toolStripComboBoxSearch_DropDown(object sender, EventArgs e)
    {
      this.ResizeDropDown(this.toolStripComboBoxSearch);
    }

    private void toolStripComboBoxSearchDT_DropDown(object sender, EventArgs e)
    {
      this.ResizeDropDown(this.toolStripComboBoxSearchDT);
    }

    private void toolStripComboBoxNamespaceDT_DropDown(object sender, EventArgs e)
    {
      this.ResizeDropDown(this.toolStripComboBoxNamespaceDT);
    }

    private void toolStripComboBoxSearchEO_DropDown(object sender, EventArgs e)
    {
      this.ResizeDropDown(this.toolStripComboBoxSearchEO);
    }

    private void toolStripComboBoxNamespaceEO_DropDown(object sender, EventArgs e)
    {
      this.ResizeDropDown(this.toolStripComboBoxNamespaceEO);
    }

    private void toolStripComboBoxUsageCat_DropDown(object sender, EventArgs e)
    {
      this.ResizeDropDown(this.toolStripComboBoxUsageCat);
    }

    private void toolStripComboBoxSearchISI_DropDown(object sender, EventArgs e)
    {
      this.ResizeDropDown(this.toolStripComboBoxSearchISI);
    }

    private void toolStripComboBoxNamespaceISI_DropDown(object sender, EventArgs e)
    {
      this.ResizeDropDown(this.toolStripComboBoxNamespaceISI);
    }

    protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
    {
      if (this.ActiveControl != this.toolStripComboBoxSearch.Control && this.ActiveControl != this.toolStripComboBoxNamespace.Control && (this.ActiveControl != this.toolStripComboBoxDepUnit.Control && this.ActiveControl != this.toolStripComboBoxSearchDT.Control) && (this.ActiveControl != this.toolStripComboBoxNamespaceDT.Control && this.ActiveControl != this.toolStripComboBoxUsageCat.Control && (this.ActiveControl != this.toolStripComboBoxSearchEO.Control && this.ActiveControl != this.toolStripComboBoxNamespaceEO.Control)) && (this.ActiveControl != this.toolStripComboBoxNamespaceISI.Control && this.ActiveControl != this.toolStripComboBoxSearchISI.Control) || keyData != Keys.Return)
        return base.ProcessCmdKey(ref msg, keyData);
      if (this.tabControl1.SelectedTab.Name == "tabPageBusinessObjectBrowser")
        this.SearchAndInitTreeBO();
      else if (this.tabControl1.SelectedTab.Name == "tabPageDataTypes")
        this.SearchAndInitDT();
      else if (this.tabControl1.SelectedTab.Name == "tabPageEnhancementOptions")
        this.SearchAndInitEO();
      else if (this.tabControl1.SelectedTab.Name == "tabPageInboundServiceInterfaces")
        this.SearchAndInitISI();
      return true;
    }

    private void toolStripButtonBack_Click(object sender, EventArgs e)
    {
      this.isBackClicked = true;
      if (this.openedBO.Length > 0)
      {
        int index = this.openedBO.Length - 1;
        ++CopernicusBusinessObjectBrowser.closedCountBO;
        TreeNode[] treeNodeArray = new TreeNode[CopernicusBusinessObjectBrowser.closedCountBO];
        Array.Copy((Array) this.closedBO, (Array) treeNodeArray, CopernicusBusinessObjectBrowser.closedCountBO - 1);
        treeNodeArray[CopernicusBusinessObjectBrowser.closedCountBO - 1] = this.openedBO[index];
        this.closedBO = (TreeNode[]) treeNodeArray.Clone();
        this.openedBO[index].Toggle();
        this.toolStripButtonNext.Enabled = true;
      }
      this.isBackClicked = false;
    }

    private void toolStripButtonNext_Click(object sender, EventArgs e)
    {
      this.toolStripButtonBack.Enabled = true;
      this.isNextRebuild = true;
      this.closedBO[this.closedBO.Length - 1].Toggle();
      ++CopernicusBusinessObjectBrowser.openCountBO;
      TreeNode[] treeNodeArray1 = new TreeNode[CopernicusBusinessObjectBrowser.openCountBO];
      Array.Copy((Array) this.openedBO, (Array) treeNodeArray1, CopernicusBusinessObjectBrowser.openCountBO - 1);
      treeNodeArray1[CopernicusBusinessObjectBrowser.openCountBO - 1] = this.closedBO[CopernicusBusinessObjectBrowser.closedCountBO - 1];
      this.openedBO = (TreeNode[]) treeNodeArray1.Clone();
      if (this.closedBO.Length == 1)
      {
        CopernicusBusinessObjectBrowser.closedCountBO = 0;
        this.closedBO = new TreeNode[0];
        this.toolStripButtonNext.Enabled = false;
      }
      else
      {
        --CopernicusBusinessObjectBrowser.closedCountBO;
        TreeNode[] treeNodeArray2 = new TreeNode[CopernicusBusinessObjectBrowser.closedCountBO];
        Array.Copy((Array) this.closedBO, (Array) treeNodeArray2, CopernicusBusinessObjectBrowser.closedCountBO);
        this.closedBO = (TreeNode[]) treeNodeArray2.Clone();
      }
      this.isNextRebuild = false;
    }

    private void toolStripButtonBackDT_Click(object sender, EventArgs e)
    {
      if (this.openSelectedDT.Length <= 1)
        return;
      this.isBackClickedDT = true;
      int index = this.openSelectedDT.Length - 1;
      ++CopernicusBusinessObjectBrowser.closedCountDT;
      TreeNode[] treeNodeArray1 = new TreeNode[CopernicusBusinessObjectBrowser.closedCountDT];
      Array.Copy((Array) this.closeSelectedDT, (Array) treeNodeArray1, CopernicusBusinessObjectBrowser.closedCountDT - 1);
      treeNodeArray1[CopernicusBusinessObjectBrowser.closedCountDT - 1] = this.openSelectedDT[index];
      this.closeSelectedDT = (TreeNode[]) treeNodeArray1.Clone();
      --CopernicusBusinessObjectBrowser.openCountDT;
      TreeNode[] treeNodeArray2 = new TreeNode[CopernicusBusinessObjectBrowser.openCountDT];
      Array.Copy((Array) this.openSelectedDT, (Array) treeNodeArray2, CopernicusBusinessObjectBrowser.openCountDT);
      this.openSelectedDT = (TreeNode[]) treeNodeArray2.Clone();
      this.treeViewDT.SelectedNode = this.openSelectedDT[index - 1];
      this.treeViewDT.SelectedNode.EnsureVisible();
      if (this.openSelectedDT.Length <= 1)
        this.toolStripButtonBackDT.Enabled = false;
      this.toolStripButtonNextDT.Enabled = true;
      this.isBackClickedDT = false;
    }

    private void toolStripButtonNextDT_Click(object sender, EventArgs e)
    {
      this.isNextClickedDT = true;
      this.toolStripButtonBackDT.Enabled = true;
      this.treeViewDT.SelectedNode = this.closeSelectedDT[this.closeSelectedDT.Length - 1];
      this.treeViewDT.SelectedNode.EnsureVisible();
      ++CopernicusBusinessObjectBrowser.openCountDT;
      TreeNode[] treeNodeArray1 = new TreeNode[CopernicusBusinessObjectBrowser.openCountDT];
      Array.Copy((Array) this.openSelectedDT, (Array) treeNodeArray1, CopernicusBusinessObjectBrowser.openCountDT - 1);
      treeNodeArray1[CopernicusBusinessObjectBrowser.openCountDT - 1] = this.closeSelectedDT[CopernicusBusinessObjectBrowser.closedCountDT - 1];
      this.openSelectedDT = (TreeNode[]) treeNodeArray1.Clone();
      if (this.closeSelectedDT.Length == 1)
      {
        CopernicusBusinessObjectBrowser.closedCountDT = 0;
        this.closeSelectedDT = new TreeNode[0];
        this.toolStripButtonNextDT.Enabled = false;
      }
      else
      {
        --CopernicusBusinessObjectBrowser.closedCountDT;
        TreeNode[] treeNodeArray2 = new TreeNode[CopernicusBusinessObjectBrowser.closedCountDT];
        Array.Copy((Array) this.closeSelectedDT, (Array) treeNodeArray2, CopernicusBusinessObjectBrowser.closedCountDT);
        this.closeSelectedDT = (TreeNode[]) treeNodeArray2.Clone();
      }
      this.isNextClickedDT = false;
    }

    private void toolStripButtonBackEO_Click(object sender, EventArgs e)
    {
      if (this.openSelectedEO.Length <= 1)
        return;
      this.isBackClickedEO = true;
      int index = this.openSelectedEO.Length - 1;
      ++CopernicusBusinessObjectBrowser.closedCountEO;
      TreeNode[] treeNodeArray1 = new TreeNode[CopernicusBusinessObjectBrowser.closedCountEO];
      Array.Copy((Array) this.closeSelectedEO, (Array) treeNodeArray1, CopernicusBusinessObjectBrowser.closedCountEO - 1);
      treeNodeArray1[CopernicusBusinessObjectBrowser.closedCountEO - 1] = this.openSelectedEO[index];
      this.closeSelectedEO = (TreeNode[]) treeNodeArray1.Clone();
      --CopernicusBusinessObjectBrowser.openCountEO;
      TreeNode[] treeNodeArray2 = new TreeNode[CopernicusBusinessObjectBrowser.openCountEO];
      Array.Copy((Array) this.openSelectedEO, (Array) treeNodeArray2, CopernicusBusinessObjectBrowser.openCountEO);
      this.openSelectedEO = (TreeNode[]) treeNodeArray2.Clone();
      this.treeViewEO.SelectedNode = this.openSelectedEO[index - 1];
      this.treeViewEO.SelectedNode.EnsureVisible();
      if (this.openSelectedEO.Length <= 1)
        this.toolStripButtonBackEO.Enabled = false;
      this.toolStripButtonNextEO.Enabled = true;
      this.isBackClickedEO = false;
    }

    private void toolStripButtonNextEO_Click(object sender, EventArgs e)
    {
      this.isNextClickedEO = true;
      this.toolStripButtonBackEO.Enabled = true;
      this.treeViewEO.SelectedNode = this.closeSelectedEO[this.closeSelectedEO.Length - 1];
      this.treeViewEO.SelectedNode.EnsureVisible();
      ++CopernicusBusinessObjectBrowser.openCountEO;
      TreeNode[] treeNodeArray1 = new TreeNode[CopernicusBusinessObjectBrowser.openCountEO];
      Array.Copy((Array) this.openSelectedEO, (Array) treeNodeArray1, CopernicusBusinessObjectBrowser.openCountEO - 1);
      treeNodeArray1[CopernicusBusinessObjectBrowser.openCountEO - 1] = this.closeSelectedEO[CopernicusBusinessObjectBrowser.closedCountEO - 1];
      this.openSelectedEO = (TreeNode[]) treeNodeArray1.Clone();
      if (this.closeSelectedEO.Length == 1)
      {
        CopernicusBusinessObjectBrowser.closedCountEO = 0;
        this.closeSelectedEO = new TreeNode[0];
        this.toolStripButtonNextEO.Enabled = false;
      }
      else
      {
        --CopernicusBusinessObjectBrowser.closedCountEO;
        TreeNode[] treeNodeArray2 = new TreeNode[CopernicusBusinessObjectBrowser.closedCountEO];
        Array.Copy((Array) this.closeSelectedEO, (Array) treeNodeArray2, CopernicusBusinessObjectBrowser.closedCountEO);
        this.closeSelectedEO = (TreeNode[]) treeNodeArray2.Clone();
      }
      this.isNextClickedEO = false;
    }

    private void toolStripButtonBackISI_Click(object sender, EventArgs e)
    {
      this.isBackClickedISI = true;
      if (this.openedISI.Length > 0)
      {
        int index = this.openedISI.Length - 1;
        ++CopernicusBusinessObjectBrowser.closedCountISI;
        TreeNode[] treeNodeArray1 = new TreeNode[CopernicusBusinessObjectBrowser.closedCountISI];
        Array.Copy((Array) this.closedISI, (Array) treeNodeArray1, CopernicusBusinessObjectBrowser.closedCountISI - 1);
        treeNodeArray1[CopernicusBusinessObjectBrowser.closedCountISI - 1] = this.openedISI[index];
        this.closedISI = (TreeNode[]) treeNodeArray1.Clone();
        this.openedISI[index].Toggle();
        --CopernicusBusinessObjectBrowser.openCountISI;
        TreeNode[] treeNodeArray2 = new TreeNode[CopernicusBusinessObjectBrowser.openCountISI];
        Array.Copy((Array) this.openedISI, (Array) treeNodeArray2, CopernicusBusinessObjectBrowser.openCountISI);
        this.openedISI = (TreeNode[]) treeNodeArray2.Clone();
        if (this.openedISI.Length == 0)
          this.toolStripButtonBackISI.Enabled = false;
        this.toolStripButtonNextISI.Enabled = true;
      }
      this.isBackClickedISI = false;
    }

    private void toolStripButtonNextISI_Click(object sender, EventArgs e)
    {
      this.toolStripButtonBackISI.Enabled = true;
      this.isNextClickedISI = true;
      this.closedISI[this.closedISI.Length - 1].Toggle();
      ++CopernicusBusinessObjectBrowser.openCountISI;
      TreeNode[] treeNodeArray1 = new TreeNode[CopernicusBusinessObjectBrowser.openCountISI];
      Array.Copy((Array) this.openedISI, (Array) treeNodeArray1, CopernicusBusinessObjectBrowser.openCountISI - 1);
      treeNodeArray1[CopernicusBusinessObjectBrowser.openCountISI - 1] = this.closedISI[CopernicusBusinessObjectBrowser.closedCountISI - 1];
      this.openedISI = (TreeNode[]) treeNodeArray1.Clone();
      if (this.closedISI.Length == 1)
      {
        CopernicusBusinessObjectBrowser.closedCountISI = 0;
        this.closedISI = new TreeNode[0];
        this.toolStripButtonNextISI.Enabled = false;
      }
      else
      {
        --CopernicusBusinessObjectBrowser.closedCountISI;
        TreeNode[] treeNodeArray2 = new TreeNode[CopernicusBusinessObjectBrowser.closedCountISI];
        Array.Copy((Array) this.closedISI, (Array) treeNodeArray2, CopernicusBusinessObjectBrowser.closedCountISI);
        this.closedISI = (TreeNode[]) treeNodeArray2.Clone();
      }
      this.isNextClickedISI = false;
    }

    private void toolStripButtonClear_Click(object sender, EventArgs e)
    {
      this.toolStripComboBoxDepUnit.Text = "Deployment Unit";
      this.toolStripComboBoxSearch.Text = "Search business objects";
      this.toolStripComboBoxNamespace.Text = "Namespace";
      CopernicusBusinessObjectBrowser.openCountBO = 0;
      this.openedBO = new TreeNode[CopernicusBusinessObjectBrowser.openCountBO];
      CopernicusBusinessObjectBrowser.closedCountBO = 0;
      this.closedBO = new TreeNode[CopernicusBusinessObjectBrowser.closedCountBO];
      this.toolStripButtonBack.Enabled = false;
      this.toolStripButtonNext.Enabled = false;
      this.SearchAndInitTreeBO();
    }

    private void toolStripButtonClearDT_Click(object sender, EventArgs e)
    {
      this.toolStripComboBoxSearchDT.Text = "Search data types";
      this.toolStripComboBoxNamespaceDT.Text = "Namespace";
      this.toolStripComboBoxUsageCat.Text = "Usage Category";
      CopernicusBusinessObjectBrowser.openCountDT = 0;
      this.openSelectedDT = new TreeNode[CopernicusBusinessObjectBrowser.openCountDT];
      CopernicusBusinessObjectBrowser.closedCountDT = 0;
      this.closeSelectedDT = new TreeNode[CopernicusBusinessObjectBrowser.closedCountDT];
      this.toolStripButtonBackDT.Enabled = false;
      this.toolStripButtonNextDT.Enabled = false;
      this.SearchAndInitDT();
    }

    private void toolStripButtonClearEO_Click(object sender, EventArgs e)
    {
      this.toolStripComboBoxSearchEO.Text = "Search enhancement options";
      this.toolStripComboBoxNamespaceEO.Text = "Namespace";
      CopernicusBusinessObjectBrowser.openCountEO = 0;
      this.openSelectedEO = new TreeNode[CopernicusBusinessObjectBrowser.openCountEO];
      CopernicusBusinessObjectBrowser.closedCountEO = 0;
      this.closeSelectedEO = new TreeNode[CopernicusBusinessObjectBrowser.closedCountEO];
      this.toolStripButtonBackEO.Enabled = false;
      this.toolStripButtonNextEO.Enabled = false;
      this.SearchAndInitEO();
    }

    private void toolStripButtonClearISI_Click(object sender, EventArgs e)
    {
      this.toolStripComboBoxSearchISI.Text = "Search service interfaces";
      this.toolStripComboBoxNamespaceISI.Text = "Namespace";
      CopernicusBusinessObjectBrowser.openCountISI = 0;
      this.openedISI = new TreeNode[CopernicusBusinessObjectBrowser.openCountISI];
      CopernicusBusinessObjectBrowser.closedCountISI = 0;
      this.closedISI = new TreeNode[CopernicusBusinessObjectBrowser.closedCountISI];
      this.toolStripButtonBackISI.Enabled = false;
      this.toolStripButtonNextISI.Enabled = false;
      this.SearchAndInitISI();
    }

    private void toolStripButtonHelpISI_Click(object sender, EventArgs e)
    {
      HelpUtil.DisplayF1Help(HELP_IDS.BDS_REPOSITORYEXPLORER_SELECTION);
    }

    private void toolStripButtonHelpEO_Click(object sender, EventArgs e)
    {
      HelpUtil.DisplayF1Help(HELP_IDS.BDS_REPOSITORYEXPLORER_SELECTION);
    }

    private void toolStripButton1HelpDT_Click(object sender, EventArgs e)
    {
      HelpUtil.DisplayF1Help(HELP_IDS.BDS_REPOSITORYEXPLORER_SELECTION);
    }

    private void toolStripButtonHelpBO_Click(object sender, EventArgs e)
    {
      HelpUtil.DisplayF1Help(HELP_IDS.BDS_REPOSITORYEXPLORER_SELECTION);
    }

    private void toolStripButtonActions_Click(object sender, EventArgs e)
    {
      for (int index1 = 0; index1 < this.closedBO.Length; ++index1)
      {
        if (this.closedBO[index1].Tag.GetType() != typeof (bostruct))
        {
          if (!(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)))
          {
            if (!(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)))
              continue;
          }
          try
          {
            BusinessObject_Node businessObjectNode = !(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)) ? (BusinessObject_Node) this.closedBO[index1].Tag : ((BusinessObject_Association) this.closedBO[index1].Tag).TargetNode;
            if (!this.toolStripButtonActions.Checked)
            {
              for (int index2 = 0; index2 < this.closedBO[index1].Nodes.Count; ++index2)
              {
                if (this.closedBO[index1].Nodes[index2].ImageIndex == 1 || this.closedBO[index1].Nodes[index2].ImageIndex == 39)
                {
                  this.closedBO[index1].Nodes[index2].Remove();
                  --index2;
                }
              }
            }
            else
            {
              TreeNode[] treeNodeArray = (TreeNode[]) this.createTreeElement("action", 1, businessObjectNode.Actions.Count, businessObjectNode).Clone();
              for (int index2 = 0; index2 < treeNodeArray.Length; ++index2)
                this.closedBO[index1].Nodes.Insert(index2, treeNodeArray[index2]);
            }
          }
          catch
          {
          }
        }
      }
      for (int index1 = 0; index1 < this.openedBO.Length; ++index1)
      {
        if (this.openedBO[index1].Tag.GetType() != typeof (bostruct))
        {
          if (!(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)))
          {
            if (!(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)))
              continue;
          }
          try
          {
            BusinessObject_Node businessObjectNode = !(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)) ? (BusinessObject_Node) this.openedBO[index1].Tag : ((BusinessObject_Association) this.openedBO[index1].Tag).TargetNode;
            if (!this.toolStripButtonActions.Checked)
            {
              for (int index2 = 0; index2 < this.openedBO[index1].Nodes.Count; ++index2)
              {
                if (this.openedBO[index1].Nodes[index2].ImageIndex == 1 || this.openedBO[index1].Nodes[index2].ImageIndex == 39)
                {
                  this.openedBO[index1].Nodes[index2].Remove();
                  --index2;
                }
              }
            }
            else
            {
              TreeNode[] treeNodeArray = (TreeNode[]) this.createTreeElement("action", 1, businessObjectNode.Actions.Count, businessObjectNode).Clone();
              for (int index2 = 0; index2 < treeNodeArray.Length; ++index2)
                this.openedBO[index1].Nodes.Insert(index2, treeNodeArray[index2]);
            }
          }
          catch
          {
          }
        }
      }
    }

    private void toolStripButtonQueries_Click(object sender, EventArgs e)
    {
      for (int index1 = 0; index1 < this.closedBO.Length; ++index1)
      {
        if (this.closedBO[index1].Tag.GetType() != typeof (bostruct))
        {
          if (!(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)))
          {
            if (!(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)))
              continue;
          }
          try
          {
            this.indexOfQuery = 0;
            BusinessObject_Node businessObjectNode = !(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)) ? ((BusinessObject_Association) this.closedBO[index1].Tag).TargetNode : (BusinessObject_Node) this.closedBO[index1].Tag;
            if (!this.toolStripButtonQueries.Checked)
            {
              for (int index2 = 0; index2 < this.closedBO[index1].Nodes.Count; ++index2)
              {
                if (this.closedBO[index1].Nodes[index2].ImageIndex == 6 || this.closedBO[index1].Nodes[index2].ImageIndex == 40)
                {
                  if (this.indexOfQuery != 0)
                    this.indexOfQuery = this.closedBO[index1].Nodes[index2].Index;
                  this.closedBO[index1].Nodes[index2].Remove();
                  --index2;
                }
              }
            }
            else
            {
              for (int index2 = 0; index2 < this.closedBO[index1].Nodes.Count; ++index2)
              {
                if (this.ImageIndexIsAssoc(this.closedBO[index1].Nodes[index2].ImageIndex))
                {
                  this.indexOfQuery = this.closedBO[index1].Nodes[index2].Index;
                  break;
                }
              }
              if (this.indexOfQuery == 0)
              {
                for (int index2 = 0; index2 < this.closedBO[index1].Nodes.Count; ++index2)
                {
                  if (this.ImageIndexIsNode(this.closedBO[index1].Nodes[index2].ImageIndex))
                  {
                    this.indexOfQuery = this.closedBO[index1].Nodes[index2].Index;
                    break;
                  }
                }
              }
              if (this.indexOfQuery == 0)
                this.indexOfQuery = this.closedBO[index1].Nodes.Count;
              TreeNode[] treeNodeArray = (TreeNode[]) this.createTreeElement("query", 6, businessObjectNode.Queries.Count, businessObjectNode).Clone();
              for (int index2 = 0; index2 < treeNodeArray.Length; ++index2)
                this.closedBO[index1].Nodes.Insert(this.indexOfQuery + index2, treeNodeArray[index2]);
            }
          }
          catch
          {
          }
        }
      }
      for (int index1 = 0; index1 < this.openedBO.Length; ++index1)
      {
        if (this.openedBO[index1].Tag.GetType() != typeof (bostruct))
        {
          if (!(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)))
          {
            if (!(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)))
              continue;
          }
          try
          {
            this.indexOfQuery = 0;
            BusinessObject_Node businessObjectNode = !(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)) ? ((BusinessObject_Association) this.openedBO[index1].Tag).TargetNode : (BusinessObject_Node) this.openedBO[index1].Tag;
            if (!this.toolStripButtonQueries.Checked)
            {
              for (int index2 = 0; index2 < this.openedBO[index1].Nodes.Count; ++index2)
              {
                if (this.openedBO[index1].Nodes[index2].ImageIndex == 6)
                {
                  if (this.indexOfQuery != 0)
                    this.indexOfQuery = this.openedBO[index1].Nodes[index2].Index;
                  this.openedBO[index1].Nodes[index2].Remove();
                  --index2;
                }
              }
            }
            else
            {
              for (int index2 = 0; index2 < this.openedBO[index1].Nodes.Count; ++index2)
              {
                if (this.ImageIndexIsAssoc(this.openedBO[index1].Nodes[index2].ImageIndex))
                {
                  this.indexOfQuery = this.openedBO[index1].Nodes[index2].Index;
                  break;
                }
              }
              if (this.indexOfQuery == 0)
              {
                for (int index2 = 0; index2 < this.openedBO[index1].Nodes.Count; ++index2)
                {
                  if (this.ImageIndexIsNode(this.openedBO[index1].Nodes[index2].ImageIndex))
                  {
                    this.indexOfQuery = this.openedBO[index1].Nodes[index2].Index;
                    break;
                  }
                }
              }
              if (this.indexOfQuery == 0)
                this.indexOfQuery = this.openedBO[index1].Nodes.Count;
              TreeNode[] treeNodeArray = (TreeNode[]) this.createTreeElement("query", 6, businessObjectNode.Queries.Count, businessObjectNode).Clone();
              for (int index2 = 0; index2 < treeNodeArray.Length; ++index2)
                this.openedBO[index1].Nodes.Insert(this.indexOfQuery + index2, treeNodeArray[index2]);
            }
          }
          catch
          {
          }
        }
      }
    }

    private void toolStripButtonAssociations_Click(object sender, EventArgs e)
    {
      for (int index1 = 0; index1 < this.closedBO.Length; ++index1)
      {
        if (this.closedBO[index1].Tag.GetType() != typeof (bostruct))
        {
          if (!(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)))
          {
            if (!(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)))
              continue;
          }
          try
          {
            this.indexOfAssoc = 0;
            BusinessObject_Node businessObjectNode = !(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)) ? ((BusinessObject_Association) this.closedBO[index1].Tag).TargetNode : (BusinessObject_Node) this.closedBO[index1].Tag;
            if (!this.toolStripButtonAssociations.Checked)
            {
              for (int index2 = 0; index2 < this.closedBO[index1].Nodes.Count; ++index2)
              {
                if (this.ImageIndexIsAssoc(this.closedBO[index1].Nodes[index2].ImageIndex))
                {
                  if (this.indexOfAssoc != 0)
                    this.indexOfAssoc = this.closedBO[index1].Nodes[index2].Index;
                  this.closedBO[index1].Nodes[index2].Remove();
                  --index2;
                }
              }
            }
            else
            {
              for (int index2 = 0; index2 < this.closedBO.Length; ++index2)
              {
                for (int index3 = 0; index3 < this.closedBO[index2].Nodes.Count; ++index3)
                {
                  if (this.ImageIndexIsNode(this.closedBO[index2].Nodes[index3].ImageIndex))
                  {
                    this.indexOfAssoc = this.closedBO[index2].Nodes[index3].Index;
                    break;
                  }
                }
                TreeNode[] treeNodeArray = (TreeNode[]) this.createTreeElement("association", 5, businessObjectNode.Associations.Count, businessObjectNode).Clone();
                for (int index3 = 0; index3 < treeNodeArray.Length; ++index3)
                {
                  if (this.indexOfAssoc != 0)
                  {
                    this.closedBO[index2].Nodes.Insert(this.indexOfAssoc + index3, treeNodeArray[index3]);
                  }
                  else
                  {
                    int count = this.closedBO[index2].Nodes.Count;
                    this.closedBO[index2].Nodes.Insert(count + index3, treeNodeArray[index3]);
                  }
                }
              }
            }
          }
          catch
          {
          }
        }
      }
      for (int index1 = 0; index1 < this.openedBO.Length; ++index1)
      {
        if (this.openedBO[index1].Tag.GetType() != typeof (bostruct))
        {
          if (!(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)))
          {
            if (!(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)))
              continue;
          }
          try
          {
            this.indexOfAssoc = 0;
            BusinessObject_Node businessObjectNode = !(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)) ? ((BusinessObject_Association) this.openedBO[index1].Tag).TargetNode : (BusinessObject_Node) this.openedBO[index1].Tag;
            if (!this.toolStripButtonAssociations.Checked)
            {
              for (int index2 = 0; index2 < this.openedBO[index1].Nodes.Count; ++index2)
              {
                if (this.ImageIndexIsAssoc(this.openedBO[index1].Nodes[index2].ImageIndex))
                {
                  if (this.indexOfAssoc != 0)
                    this.indexOfAssoc = this.openedBO[index1].Nodes[index2].Index;
                  this.openedBO[index1].Nodes[index2].Remove();
                  --index2;
                }
              }
            }
            else
            {
              for (int index2 = 0; index2 < this.openedBO[index1].Nodes.Count; ++index2)
              {
                if (this.ImageIndexIsNode(this.openedBO[index1].Nodes[index2].ImageIndex))
                {
                  this.indexOfAssoc = this.openedBO[index1].Nodes[index2].Index;
                  break;
                }
              }
              TreeNode[] treeNodeArray = (TreeNode[]) this.createTreeElement("association", 5, businessObjectNode.Associations.Count, businessObjectNode).Clone();
              for (int index2 = 0; index2 < treeNodeArray.Length; ++index2)
              {
                if (this.indexOfAssoc != 0)
                {
                  this.openedBO[index1].Nodes.Insert(this.indexOfAssoc + index2, treeNodeArray[index2]);
                }
                else
                {
                  int count = this.closedBO[index1].Nodes.Count;
                  this.openedBO[index1].Nodes.Insert(count + index2, treeNodeArray[index2]);
                }
              }
            }
          }
          catch
          {
          }
        }
      }
    }

    private void toolStripButtonNodes_Click(object sender, EventArgs e)
    {
      for (int index1 = 0; index1 < this.closedBO.Length; ++index1)
      {
        if (this.closedBO[index1].Tag.GetType() != typeof (bostruct))
        {
          if (!(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)))
          {
            if (!(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)))
              continue;
          }
          try
          {
            BusinessObject_Node businessObjectNode = !(this.closedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)) ? (BusinessObject_Node) this.closedBO[index1].Tag : ((BusinessObject_Association) this.closedBO[index1].Tag).TargetNode;
            if (!this.toolStripButtonNodes.Checked)
            {
              for (int index2 = 0; index2 < this.closedBO[index1].Nodes.Count; ++index2)
              {
                if (this.closedBO[index1].Nodes[index2].Text != "Root" && this.ImageIndexIsNode(this.closedBO[index1].Nodes[index2].ImageIndex))
                {
                  this.closedBO[index1].Nodes[index2].Remove();
                  --index2;
                }
              }
            }
            else
            {
              BusinessObject_Node[] array = new BusinessObject_Node[businessObjectNode.ChildNodes.Count];
              int index2 = 0;
              for (int index3 = 0; index3 < businessObjectNode.ChildNodes.Count; ++index3)
              {
                if (businessObjectNode.ChildNodes[index3].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(businessObjectNode.ChildNodes[index3].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(businessObjectNode.ChildNodes[index3].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
                {
                  array[index2] = businessObjectNode.ChildNodes[index3];
                  ++index2;
                }
              }
              Array.Sort<BusinessObject_Node>(array, (Comparison<BusinessObject_Node>) ((child1, child2) => child1.SemanticalName.CompareTo(child2.SemanticalName)));
              TreeNode[] treeNodeArray = new TreeNode[array.Length];
              for (int index3 = 0; index3 < array.Length; ++index3)
              {
                try
                {
                  TreeNode treeNode1 = new TreeNode();
                  TreeNode treeNode2 = new TreeNode();
                  if (array[index3].IsDOInclusionNode)
                  {
                    bool flag = false;
                    string text = array[index3].Name == "Root" || array[index3].Name == "ROOT" || array[index3].Name == "root" ? array[index3].SemanticalName.Replace(" ", "") : array[index3].Name;
                    for (int index4 = 0; index4 < businessObjectNode.Associations.Count; ++index4)
                    {
                      if (text == businessObjectNode.Associations[index4].Name)
                        flag = true;
                    }
                    if (flag)
                    {
                      TreeNode treeNode3 = new TreeNode(text);
                      treeNode3.Tag = (object) array[index3];
                      string str1 = "";
                      string str2 = "";
                      if (array[index3].PublicSolutionModel != null)
                      {
                        if (array[index3].PublicSolutionModel.ReleaseStatusCode.ToString() != null)
                          str2 = !(array[index3].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
                        int num = array[index3].PublicSolutionModel.WriteAccessPublishedIndicator ? 1 : 0;
                        str1 = !array[index3].PublicSolutionModel.WriteAccessPublishedIndicator ? "No" : "Yes";
                      }
                      string str3 = array[index3].FieldExtensibleIndicator ? "Yes" : "No";
                      string str4 = array[index3].CategoryCode.ToString();
                      string str5 = "Subelement Type: Node";
                      treeNode3.ToolTipText = str5 + "\nDeprecated: " + str2 + "\nPublic Write Access: " + str1 + "\nExtensible: " + str3 + "\nCategory: " + str4;
                      treeNode3.ContextMenuStrip = this.contextMenuStripSUBelement;
                      treeNode3.ImageIndex = !(str2 == "Yes") ? (array[index3].ReadOnlyOperationIndicator == null || !array[index3].ReadOnlyOperationIndicator.Indicator && !array[index3].ReadOnlyOperationIndicator.FinalIndicator ? (!(str3 == "Yes") ? 4 : 41) : 38) : 29;
                      treeNode3.SelectedImageIndex = treeNode3.ImageIndex;
                      treeNode3.Nodes.Add(new TreeNode("An error occured")
                      {
                        Tag = (object) "dummy"
                      });
                      treeNodeArray[index3] = treeNode3;
                      int count = this.closedBO[index1].Nodes.Count;
                      this.closedBO[index1].Nodes.Insert(count, treeNodeArray[index3]);
                    }
                  }
                  else
                  {
                    TreeNode treeNode3 = new TreeNode(array[index3].Name == "Root" || array[index3].Name == "ROOT" || array[index3].Name == "root" ? array[index3].SemanticalName.Replace(" ", "") : array[index3].Name);
                    treeNode3.Tag = (object) array[index3];
                    string str1 = "";
                    string str2 = "";
                    if (array[index3].PublicSolutionModel != null)
                    {
                      if (array[index3].PublicSolutionModel.ReleaseStatusCode.ToString() != null)
                        str2 = !(array[index3].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
                      int num = array[index3].PublicSolutionModel.WriteAccessPublishedIndicator ? 1 : 0;
                      str1 = !array[index3].PublicSolutionModel.WriteAccessPublishedIndicator ? "No" : "Yes";
                    }
                    string str3 = array[index3].FieldExtensibleIndicator ? "Yes" : "No";
                    string str4 = array[index3].CategoryCode.ToString();
                    string str5 = "Subelement Type: Node";
                    treeNode3.ToolTipText = str5 + "\nDeprecated: " + str2 + "\nPublic Write Access: " + str1 + "\nExtensible: " + str3 + "\nCategory: " + str4;
                    treeNode3.ContextMenuStrip = this.contextMenuStripSUBelement;
                    treeNode3.ImageIndex = !(str2 == "Yes") ? (array[index3].ReadOnlyOperationIndicator == null || !array[index3].ReadOnlyOperationIndicator.Indicator && !array[index3].ReadOnlyOperationIndicator.FinalIndicator ? (!(str3 == "Yes") ? 4 : 41) : 38) : 29;
                    treeNode3.SelectedImageIndex = treeNode3.ImageIndex;
                    treeNode3.Nodes.Add(new TreeNode("An error occured")
                    {
                      Tag = (object) "dummy"
                    });
                    treeNodeArray[index3] = treeNode3;
                    int count = this.closedBO[index1].Nodes.Count;
                    this.closedBO[index1].Nodes.Insert(count, treeNodeArray[index3]);
                  }
                }
                catch
                {
                }
              }
            }
          }
          catch
          {
          }
        }
      }
      for (int index1 = 0; index1 < this.openedBO.Length; ++index1)
      {
        if (this.openedBO[index1].Tag.GetType() != typeof (bostruct))
        {
          if (!(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)))
          {
            if (!(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Node)))
              continue;
          }
          try
          {
            BusinessObject_Node businessObjectNode = !(this.openedBO[index1].Tag.GetType() == typeof (BusinessObject_Association)) ? (BusinessObject_Node) this.openedBO[index1].Tag : ((BusinessObject_Association) this.openedBO[index1].Tag).TargetNode;
            if (!this.toolStripButtonNodes.Checked)
            {
              for (int index2 = 0; index2 < this.openedBO[index1].Nodes.Count; ++index2)
              {
                if (this.openedBO[index1].Nodes[index2].Text != "Root" && this.ImageIndexIsNode(this.openedBO[index1].Nodes[index2].ImageIndex))
                {
                  this.openedBO[index1].Nodes[index2].Remove();
                  --index2;
                }
              }
            }
            else
            {
              BusinessObject_Node[] array = new BusinessObject_Node[businessObjectNode.ChildNodes.Count];
              int index2 = 0;
              for (int index3 = 0; index3 < businessObjectNode.ChildNodes.Count; ++index3)
              {
                if (businessObjectNode.ChildNodes[index3].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(businessObjectNode.ChildNodes[index3].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(businessObjectNode.ChildNodes[index3].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
                {
                  array[index2] = businessObjectNode.ChildNodes[index3];
                  ++index2;
                }
              }
              Array.Sort<BusinessObject_Node>(array, (Comparison<BusinessObject_Node>) ((child1, child2) => child1.SemanticalName.CompareTo(child2.SemanticalName)));
              TreeNode[] treeNodeArray = new TreeNode[array.Length];
              for (int index3 = 0; index3 < array.Length; ++index3)
              {
                try
                {
                  TreeNode treeNode1 = new TreeNode();
                  TreeNode treeNode2 = new TreeNode();
                  if (array[index3].IsDOInclusionNode)
                  {
                    bool flag = false;
                    string text = array[index3].Name == "Root" || array[index3].Name == "ROOT" || array[index3].Name == "root" ? array[index3].SemanticalName.Replace(" ", "") : array[index3].Name;
                    for (int index4 = 0; index4 < businessObjectNode.Associations.Count; ++index4)
                    {
                      if (text == businessObjectNode.Associations[index4].Name)
                        flag = true;
                    }
                    if (flag)
                    {
                      TreeNode treeNode3 = new TreeNode(text);
                      treeNode3.Tag = (object) array[index3];
                      string str1 = "";
                      string str2 = "";
                      if (array[index3].PublicSolutionModel != null)
                      {
                        if (array[index3].PublicSolutionModel.ReleaseStatusCode.ToString() != null)
                          str2 = !(array[index3].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
                        int num = array[index3].PublicSolutionModel.WriteAccessPublishedIndicator ? 1 : 0;
                        str1 = !array[index3].PublicSolutionModel.WriteAccessPublishedIndicator ? "No" : "Yes";
                      }
                      string str3 = array[index3].FieldExtensibleIndicator ? "Yes" : "No";
                      string str4 = array[index3].CategoryCode.ToString();
                      string str5 = "Subelement Type: Node";
                      treeNode3.ToolTipText = str5 + "\nDeprecated: " + str2 + "\nPublic Write Access: " + str1 + "\nExtensible: " + str3 + "\nCategory: " + str4;
                      treeNode3.ContextMenuStrip = this.contextMenuStripSUBelement;
                      treeNode3.ImageIndex = !(str2 == "Yes") ? (array[index3].ReadOnlyOperationIndicator == null || !array[index3].ReadOnlyOperationIndicator.Indicator && !array[index3].ReadOnlyOperationIndicator.FinalIndicator ? (!(str3 == "Yes") ? 4 : 41) : 38) : 29;
                      treeNode3.SelectedImageIndex = treeNode3.ImageIndex;
                      treeNode3.Nodes.Add(new TreeNode("An error occured")
                      {
                        Tag = (object) "dummy"
                      });
                      treeNodeArray[index3] = treeNode3;
                      int count = this.openedBO[index1].Nodes.Count;
                      this.openedBO[index1].Nodes.Insert(count, treeNodeArray[index3]);
                    }
                  }
                  else
                  {
                    TreeNode treeNode3 = new TreeNode(array[index3].Name == "Root" || array[index3].Name == "ROOT" || array[index3].Name == "root" ? array[index3].SemanticalName.Replace(" ", "") : array[index3].Name);
                    treeNode3.Tag = (object) array[index3];
                    string str1 = "";
                    string str2 = "";
                    if (array[index3].PublicSolutionModel != null)
                    {
                      if (array[index3].PublicSolutionModel.ReleaseStatusCode.ToString() != null)
                        str2 = !(array[index3].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
                      int num = array[index3].PublicSolutionModel.WriteAccessPublishedIndicator ? 1 : 0;
                      str1 = !array[index3].PublicSolutionModel.WriteAccessPublishedIndicator ? "No" : "Yes";
                    }
                    string str3 = array[index3].FieldExtensibleIndicator ? "Yes" : "No";
                    string str4 = array[index3].CategoryCode.ToString();
                    string str5 = "Subelement Type: Node";
                    treeNode3.ToolTipText = str5 + "\nDeprecated: " + str2 + "\nPublic Write Access: " + str1 + "\nExtensible: " + str3 + "\nCategory: " + str4;
                    treeNode3.ContextMenuStrip = this.contextMenuStripSUBelement;
                    treeNode3.ImageIndex = !(str2 == "Yes") ? (array[index3].ReadOnlyOperationIndicator == null || !array[index3].ReadOnlyOperationIndicator.Indicator && !array[index3].ReadOnlyOperationIndicator.FinalIndicator ? (!(str3 == "Yes") ? 4 : 41) : 38) : 29;
                    treeNode3.SelectedImageIndex = treeNode3.ImageIndex;
                    treeNode3.Nodes.Add(new TreeNode("An error occured")
                    {
                      Tag = (object) "dummy"
                    });
                    treeNodeArray[index3] = treeNode3;
                    int count = this.openedBO[index1].Nodes.Count;
                    this.openedBO[index1].Nodes.Insert(count, treeNodeArray[index3]);
                  }
                }
                catch
                {
                }
              }
            }
          }
          catch
          {
          }
        }
      }
    }

    private void toolStripButtonDeprecation_Click(object sender, EventArgs e)
    {
      CopernicusBusinessObjectBrowser.deprecationRebuildCount = 0;
      if (this.openedBO.Length == 0)
        return;
      this.isDeprecationRebuild = true;
      if (!this.toolStripButtonDeprecation.Checked)
      {
        for (int index1 = 0; index1 < this.treeViewBO.Nodes.Count; ++index1)
        {
          bool flag = false;
          for (int index2 = 0; index2 < this.openedBO.Length; ++index2)
          {
            if (this.treeViewBO.Nodes[index1].Tag == this.openedBO[index2].Tag)
            {
              flag = true;
              break;
            }
          }
          if (!flag && ((bostruct) this.treeViewBO.Nodes[index1].Tag).deprecated)
            this.treeViewBO.Nodes[index1].Remove();
        }
        for (int index1 = 0; index1 < this.openedBO.Length; ++index1)
        {
          try
          {
            if (this.openedBO[index1].Tag.GetType() == typeof (bostruct) && ((bostruct) this.openedBO[index1].Tag).deprecated)
              this.openedBO[index1].Remove();
            for (int index2 = 0; index2 < this.openedBO[index1].Nodes.Count; ++index2)
            {
              if (this.isDeprecated(this.openedBO[index1].Nodes[index2]))
              {
                this.openedBO[index1].Nodes[index2].Remove();
                --index2;
              }
              this.toolStripButtonBack.Enabled = false;
              this.toolStripButtonNext.Enabled = false;
            }
          }
          catch
          {
          }
        }
      }
      else
      {
        for (int index = 0; index < this.openedBO.Length; ++index)
        {
          CopernicusBusinessObjectBrowser.deprecationRebuildCount = index;
          try
          {
            if (this.openedBO[index].Tag.GetType() != typeof (bostruct))
            {
              this.AfterCollapse(this.openedBO[index]);
              this.AfterExpand(this.openedBO[index]);
              this.DeprecationRebuildHelper = 0;
            }
          }
          catch
          {
          }
        }
      }
      this.isDeprecationRebuild = false;
    }

    private void SearchAndInitTreeBO()
    {
      string boNamespace = (string) null;
      string boDeploymentUnit = (string) null;
      string boName = (string) null;
      CopernicusBusinessObjectBrowser.openCountBO = 0;
      this.openedBO = new TreeNode[CopernicusBusinessObjectBrowser.openCountBO];
      CopernicusBusinessObjectBrowser.closedCountBO = 0;
      this.closedBO = new TreeNode[CopernicusBusinessObjectBrowser.closedCountBO];
      this.toolStripButtonNext.Enabled = false;
      this.toolStripButtonBack.Enabled = false;
      if (!(this.toolStripComboBoxNamespace.Text == "") && !(this.toolStripComboBoxNamespace.Text == "Namespace"))
        boNamespace = this.toolStripComboBoxNamespace.Text;
      else
        this.toolStripComboBoxNamespace.Text = "Namespace";
      if (!(this.toolStripComboBoxDepUnit.Text == "") && !(this.toolStripComboBoxDepUnit.Text == "Deployment Unit"))
        boDeploymentUnit = this.toolStripComboBoxDepUnit.Text;
      else
        this.toolStripComboBoxDepUnit.Text = "Deployment Unit";
      if (!(this.toolStripComboBoxSearch.Text == "") && !(this.toolStripComboBoxSearch.Text == "Search business objects"))
      {
        boName = this.toolStripComboBoxSearch.Text;
        bool flag = false;
        for (int index = 0; index < this.toolStripComboBoxSearch.Items.Count; ++index)
        {
          if (boName == this.toolStripComboBoxSearch.Items[index].ToString())
          {
            flag = true;
            break;
          }
        }
        if (!flag)
        {
          this.toolStripComboBoxSearch.Items.Add((object) boName);
          this.toolStripComboBoxSearch.AutoCompleteCustomSource.Add(boName);
        }
      }
      else
        this.toolStripComboBoxSearch.Text = "Search business objects";
      try
      {
        this.InitializeBOTreeView(boName, boNamespace, boDeploymentUnit);
      }
      catch
      {
        this.richTextBoxInfoBoxBO.Text = "\n Enter or select a valid deployment unit, for example, CustomerRelationshipManagement.";
        this.richTextBoxInfoBoxBO.Visible = true;
      }
    }

    private void SearchAndInitDT()
    {
      this.Cursor = Cursors.WaitCursor;
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      service.SetText("Loading data types...");
      string dtNamespaceSearch = (string) null;
      string dtNameSearch = (string) null;
      string dtUsageSearch = (string) null;
      CopernicusBusinessObjectBrowser.openCountDT = 0;
      this.openSelectedDT = new TreeNode[CopernicusBusinessObjectBrowser.openCountDT];
      CopernicusBusinessObjectBrowser.closedCountDT = 0;
      this.closeSelectedDT = new TreeNode[CopernicusBusinessObjectBrowser.closedCountDT];
      this.toolStripButtonBackDT.Enabled = false;
      this.toolStripButtonNextDT.Enabled = false;
      if (this.toolStripComboBoxUsageCat.Text != "Usage Category" && this.toolStripComboBoxUsageCat.Text != "")
        dtUsageSearch = this.toolStripComboBoxUsageCat.Text;
      else
        this.toolStripComboBoxUsageCat.Text = "Usage Category";
      if (this.toolStripComboBoxNamespaceDT.Text != "Namespace" && this.toolStripComboBoxNamespaceDT.Text != "")
        dtNamespaceSearch = this.toolStripComboBoxNamespaceDT.Text;
      else
        this.toolStripComboBoxNamespaceDT.Text = "Namespace";
      if (this.toolStripComboBoxSearchDT.Text != "Search data types" && this.toolStripComboBoxSearchDT.Text != "")
      {
        dtNameSearch = this.toolStripComboBoxSearchDT.Text;
        bool flag = false;
        for (int index = 0; index < this.toolStripComboBoxSearchDT.Items.Count; ++index)
        {
          if (dtNameSearch == this.toolStripComboBoxSearchDT.Items[index].ToString())
          {
            flag = true;
            break;
          }
        }
        if (!flag)
        {
          this.toolStripComboBoxSearchDT.Items.Add((object) dtNameSearch);
          this.toolStripComboBoxSearchDT.AutoCompleteCustomSource.Add(dtNameSearch);
        }
      }
      else
        this.toolStripComboBoxSearchDT.Text = "Search data types";
      try
      {
        this.InitializeDTTreeView(dtNamespaceSearch, dtNameSearch, dtUsageSearch);
      }
      catch
      {
        this.richTextBoxInfoBoxDT.Text = "\n An error occurred while loading the repository content. Try again. If the error occurs again, report an incident.";
        this.richTextBoxInfoBoxDT.Visible = true;
      }
      this.Cursor = Cursors.Default;
      service.SetText("Ready");
    }

    private void InitializeDTTreeView(string dtNamespaceSearch, string dtNameSearch, string dtUsageSearch)
    {
      this.richTextBoxInfoBoxDT.Visible = false;
      this.richTextBoxInfoBoxDT.Text = "";
      this.webBrowserDT.DocumentText = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>To view the documentation for an entity, click the entity in the tree view. You can search for an entity by typing in the search box above.You can show the documentation pane at the bottom by clicking Horizontal or on the right by clicking Vertical.</p></body></html>";
      this.treeViewDT.Nodes.Clear();
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      RepositoryDataSet.DataTypesRow[] array = RepositoryDataCache.GetInstance().QueryDataTypes((string) null, (string) null, (string) null, (string) null, (string) null, (string) null);
      Array.Sort<RepositoryDataSet.DataTypesRow>(array, (Comparison<RepositoryDataSet.DataTypesRow>) ((dtList1, dtList2) => dtList1.Name.CompareTo(dtList2.Name)));
      string str1;
      try
      {
        str1 = CopernicusProjectSystemUtil.GetNamespaceForSelectedProject();
      }
      catch
      {
        str1 = (string) null;
      }
      this.treeViewDT.BeginUpdate();
      foreach (RepositoryDataSet.DataTypesRow dataTypesRow in array)
      {
        if (!(str1 != null & dataTypesRow.NSName == str1) && (dtNameSearch == null || Regex.IsMatch(dataTypesRow.Name.ToUpper(), this.GetUpperRegexStringForName(dtNameSearch))) && (dtNamespaceSearch == null || dataTypesRow.NSName.Equals(dtNamespaceSearch, StringComparison.OrdinalIgnoreCase)))
        {
          dtstruct dtstruct = new dtstruct();
          switch (dataTypesRow.UsageCategory)
          {
            case "1":
              dtstruct.dtusage = "Unrestricted";
              break;
            case "2":
              dtstruct.dtusage = "Core";
              break;
            case "3":
              dtstruct.dtusage = "Key";
              break;
            case "4":
              dtstruct.dtusage = "Node";
              break;
            case "5":
              dtstruct.dtusage = "Action";
              break;
            case "6":
              dtstruct.dtusage = "Query";
              break;
            case "7":
              dtstruct.dtusage = "Message";
              break;
            case "8":
              dtstruct.dtusage = "Filter";
              break;
            case "9":
              dtstruct.dtusage = "Business Object Intermediate";
              break;
            case "10":
              dtstruct.dtusage = "Message Intermediate";
              break;
            case "11":
              dtstruct.dtusage = "Form Message Intermediate";
              break;
            case "12":
              dtstruct.dtusage = "Query Intermediate";
              break;
            case "13":
              dtstruct.dtusage = "Message Unrestricted";
              break;
            case "14":
              dtstruct.dtusage = "Code Data Type Context";
              break;
            case "15":
              dtstruct.dtusage = "Form Message";
              break;
            case "16":
              dtstruct.dtusage = "Enhancement Option";
              break;
            default:
              dtstruct.dtusage = dataTypesRow.UsageCategory;
              break;
          }
          if (dtUsageSearch == null || dtstruct.dtusage.Equals(dtUsageSearch, StringComparison.OrdinalIgnoreCase))
          {
            dtstruct.dtname = dataTypesRow.Name;
            dtstruct.dtnamespace = dataTypesRow.NSName;
            dtstruct.dtproxyname = dataTypesRow.ProxyName;
            TreeNode node = new TreeNode(dtstruct.dtname);
            this.treeViewDT.Nodes.Add(node);
            node.ImageIndex = 7;
            node.SelectedImageIndex = 7;
            node.Tag = (object) dtstruct;
            node.ContextMenuStrip = this.contextMenuStripFirstLevel;
            string str2 = "";
            if (dataTypesRow.Extensible)
            {
              str2 = "Yes";
            }
            else
            {
              string str3 = "No";
              node.ToolTipText = "Name: " + dtstruct.dtname + "\nNamespace: " + dtstruct.dtnamespace + "\nExtensible: " + str3 + "\nUsage Category: " + dtstruct.dtusage;
            }
          }
        }
      }
      this.treeViewDT.EndUpdate();
      if (this.treeViewDT.Nodes.Count != 0)
        return;
      this.richTextBoxInfoBoxDT.Text = "The system could not find any data types that match your search term. Try the following:\n    o Click the Clear All button to search in all namespaces and usage categories.\n    o Enter a different spelling or search term.";
      this.richTextBoxInfoBoxDT.Visible = true;
    }

    private void SearchAndInitEO()
    {
      string badiNamespace = (string) null;
      string badiName = (string) null;
      CopernicusBusinessObjectBrowser.openCountEO = 0;
      this.openSelectedEO = new TreeNode[CopernicusBusinessObjectBrowser.openCountEO];
      CopernicusBusinessObjectBrowser.closedCountEO = 0;
      this.closeSelectedEO = new TreeNode[CopernicusBusinessObjectBrowser.closedCountEO];
      this.toolStripButtonBackEO.Enabled = false;
      this.toolStripButtonNextEO.Enabled = false;
      if (this.toolStripComboBoxNamespaceEO.Text != "Namespace" && this.toolStripComboBoxNamespaceEO.Text != "")
        badiNamespace = this.toolStripComboBoxNamespaceEO.Text;
      else
        this.toolStripComboBoxNamespaceEO.Text = "Namespace";
      if (this.toolStripComboBoxSearchEO.Text != "Search enhancement options" && this.toolStripComboBoxSearchEO.Text != "")
      {
        badiName = this.toolStripComboBoxSearchEO.Text;
        bool flag = false;
        for (int index = 0; index < this.toolStripComboBoxSearchEO.Items.Count; ++index)
        {
          if (badiName == this.toolStripComboBoxSearchEO.Items[index].ToString())
          {
            flag = true;
            break;
          }
        }
        if (!flag)
        {
          this.toolStripComboBoxSearchEO.Items.Add((object) badiName);
          this.toolStripComboBoxSearchEO.AutoCompleteCustomSource.Add(badiName);
        }
      }
      else
        this.toolStripComboBoxSearchEO.Text = "Search enhancement options";
      try
      {
        this.InitializeEOTreeView(badiName, badiNamespace);
      }
      catch
      {
        this.richTextBoxInfoBoxEO.Text = "An error occurred while loading the repository content. Try again. If the error occurs again, report an incident.";
        this.richTextBoxInfoBoxEO.Visible = true;
      }
    }

    private void InitializeEOTreeView(string badiName, string badiNamespace)
    {
      this.richTextBoxInfoBoxEO.Visible = false;
      this.richTextBoxInfoBoxEO.Text = "";
      this.webBrowserEO.DocumentText = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>To view the documentation for an entity, click the entity in the tree view. You can search for an entity by typing in the search box above.You can show the documentation pane at the bottom by clicking Horizontal or on the right by clicking Vertical.</p></body></html>";
      this.treeViewEO.Nodes.Clear();
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      IEnumerable<DataRow> badInSandName = RepositoryDataCache.GetInstance().GetBAdINSandName((string) null, (string) null);
      string str1;
      try
      {
        str1 = CopernicusProjectSystemUtil.GetNamespaceForSelectedProject();
      }
      catch
      {
        str1 = (string) null;
      }
      this.treeViewEO.BeginUpdate();
      foreach (RepositoryDataSet.BAdIsRow badIsRow in badInSandName)
      {
        if ((str1 == null || !(badIsRow.NSName == str1)) && (badiName == null || Regex.IsMatch(badIsRow.Name.ToUpper(), this.GetUpperRegexStringForName(badiName))) && (badiNamespace == null || badIsRow.NSName.Equals(badiNamespace, StringComparison.OrdinalIgnoreCase)))
        {
          eostruct eostruct = new eostruct();
          eostruct.eoname = badIsRow.Name;
          eostruct.eonamespace = badIsRow.NSName;
          eostruct.eoproxyname = badIsRow.ProxyName;
          TreeNode node = new TreeNode(eostruct.eoname);
          this.treeViewEO.Nodes.Add(node);
          node.ImageIndex = 8;
          node.SelectedImageIndex = 8;
          node.Tag = (object) eostruct;
          string str2 = !badIsRow.IsSingleUsed ? "Multiple" : "Single";
          node.ToolTipText = "Usage: " + str2 + "\nBusiness Object: " + badIsRow.BusinessObject + "\nNamespace: " + badIsRow.NSName + "\nDescription: " + badIsRow.Description;
          node.ContextMenuStrip = this.contextMenuStripFirstLevel;
        }
      }
      this.treeViewEO.EndUpdate();
      if (this.treeViewEO.Nodes.Count != 0)
        return;
      this.richTextBoxInfoBoxEO.Text = "The system could not find any enhancement options that match your search term. Try the following:\n    o Click the Clear All button to search in all namespaces.\n    o Enter a different spelling or search term.";
      this.richTextBoxInfoBoxEO.Visible = true;
    }

    private void SearchAndInitISI()
    {
      this.Cursor = Cursors.WaitCursor;
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      service.SetText("Loading inbound service interfaces...");
      string isiNamespaceSearch = (string) null;
      string isiNameSearch = (string) null;
      if (this.toolStripComboBoxNamespaceISI.Text != "Namespace" && this.toolStripComboBoxNamespaceISI.Text != "")
        isiNamespaceSearch = this.toolStripComboBoxNamespaceISI.Text;
      else
        this.toolStripComboBoxNamespaceISI.Text = "Namespace";
      if (this.toolStripComboBoxSearchISI.Text != "Search service interfaces" && this.toolStripComboBoxSearchISI.Text != "")
      {
        isiNameSearch = this.toolStripComboBoxSearchISI.Text;
        bool flag = false;
        for (int index = 0; index < this.toolStripComboBoxSearchISI.Items.Count; ++index)
        {
          if (isiNameSearch == this.toolStripComboBoxSearchISI.Items[index].ToString())
          {
            flag = true;
            break;
          }
        }
        if (!flag)
        {
          this.toolStripComboBoxSearchISI.Items.Add((object) isiNameSearch);
          this.toolStripComboBoxSearchISI.AutoCompleteCustomSource.Add(isiNameSearch);
        }
      }
      else
        this.toolStripComboBoxSearchISI.Text = "Search service interfaces";
      try
      {
        this.InitializeISITreeView(isiNamespaceSearch, isiNameSearch);
      }
      catch
      {
        this.richTextBoxInfoBoxISI.Text = "\n An error occurred while loading the repository content. Try again. If the error occurs again, report an incident.";
        this.richTextBoxInfoBoxISI.Visible = true;
      }
      this.Cursor = Cursors.Default;
      service.SetText("Ready");
    }

    private void InitializeISITreeView(string isiNamespaceSearch, string isiNameSearch)
    {
      this.richTextBoxInfoBoxISI.Visible = false;
      this.richTextBoxInfoBoxISI.Text = "";
      this.webBrowserISI.DocumentText = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>To view the documentation for an entity, click the entity in the tree view. You can search for an entity by typing in the search box above.You can show the documentation pane at the bottom by clicking Horizontal or on the right by clicking Vertical.</p></body></html>";
      this.treeViewISI.Nodes.Clear();
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      PDI_RI_T_SI_AND_OPS[] siopInfo = new IntegrationScenarioHandler().getSIOPInfo((string) null, false);
      PDI_RI_T_SI_INFO[] siList = new BOXSHandler().getSIList((string) null);
      Array.Sort<PDI_RI_T_SI_AND_OPS>(siopInfo, (Comparison<PDI_RI_T_SI_AND_OPS>) ((siop1, siop2) => siop1.SI_NAME.CompareTo(siop2.SI_NAME)));
      bool flag = false;
      if (this.toolStripComboBoxSearchISI.AutoCompleteCustomSource.Count == 0)
        flag = true;
      this.treeViewISI.BeginUpdate();
      foreach (PDI_RI_T_SI_AND_OPS pdiRiTSiAndOps in siopInfo)
      {
        if (flag && pdiRiTSiAndOps.SI_NAME != null)
          this.toolStripComboBoxSearchISI.AutoCompleteCustomSource.Add(pdiRiTSiAndOps.SI_NAME);
        if ((isiNameSearch == null || Regex.IsMatch(pdiRiTSiAndOps.SI_NAME.ToUpper(), this.GetUpperRegexStringForName(isiNameSearch))) && (isiNamespaceSearch == null || pdiRiTSiAndOps.SI_NAMESPACE.Equals(isiNamespaceSearch, StringComparison.OrdinalIgnoreCase)) && !(pdiRiTSiAndOps.DIRECTION != "I"))
        {
          isistruct isistruct = new isistruct();
          foreach (PDI_RI_T_SI_INFO pdiRiTSiInfo in siList)
          {
            if (pdiRiTSiAndOps.SI_NAME == pdiRiTSiInfo.SI_ESR_NAME && pdiRiTSiAndOps.SI_NAMESPACE == pdiRiTSiInfo.SI_NAMESPACE)
            {
              isistruct.isiproxyname = pdiRiTSiInfo.SI_NAME;
              break;
            }
          }
          isistruct.isiname = pdiRiTSiAndOps.SI_NAME;
          isistruct.isidirection = pdiRiTSiAndOps.DIRECTION;
          isistruct.isinamespace = pdiRiTSiAndOps.SI_NAMESPACE;
          isistruct.isioperations = pdiRiTSiAndOps.OPERATIONS;
          TreeNode node1 = new TreeNode(isistruct.isiname);
          if (pdiRiTSiAndOps.OPERATIONS.Length > 0)
          {
            TreeNode node2 = new TreeNode("An error occured");
            node1.Nodes.Add(node2);
          }
          node1.ImageIndex = 19;
          node1.SelectedImageIndex = node1.ImageIndex;
          node1.Tag = (object) isistruct;
          node1.ToolTipText = "Name: " + isistruct.isiname + "\nNamespace: " + isistruct.isinamespace;
          node1.ContextMenuStrip = this.contextMenuStripFirstLevel;
          this.treeViewISI.Nodes.Add(node1);
        }
      }
      this.treeViewISI.EndUpdate();
      if (this.treeViewISI.Nodes.Count != 0)
        return;
      this.richTextBoxInfoBoxISI.Text = "The system could not find any inbound service interfaces that match your search term. Try the following:\n    o Click the Clear All button to search in all namespaces.\n    o Enter a different spelling or search term.";
      this.richTextBoxInfoBoxISI.Visible = true;
    }

    private void toolStripButtonSearch_Click(object sender, EventArgs e)
    {
      this.SearchAndInitTreeBO();
    }

    private void toolStripButtonSearchDT_Click(object sender, EventArgs e)
    {
      this.SearchAndInitDT();
    }

    private void toolStripButtonSearchEO_Click(object sender, EventArgs e)
    {
      this.SearchAndInitEO();
    }

    private void toolStripButtonSearchISI_Click(object sender, EventArgs e)
    {
      this.SearchAndInitISI();
    }

    private BusinessObject_Node getBONodefromModelAPI(string boproxy, string nodeproxy)
    {
      try
      {
        URLStore urlStore = new URLStore();
        string businessObjectNode1 = ModelHelper.GetResourceNameForBusinessObjectNode(boproxy, nodeproxy, "");
        BusinessObject_Node businessObjectNode2;
        using (Transaction transaction = urlStore.TransactionManager.BeginTransaction("Read " + nodeproxy))
        {
          businessObjectNode2 = MetamodelSerializationHelper.Instance.LoadModelFromUrl<BusinessObject_Node>(new SerializationResult(), (Store) urlStore, businessObjectNode1, (ISchemaResolver) null, (ValidationController) null);
          transaction.Commit();
        }
        return businessObjectNode2;
      }
      catch
      {
        return (BusinessObject_Node) null;
      }
    }

    private TreeNode[] createTreeElement(string treeElement, int imageindex, int count, BusinessObject_Node businessObjectNode)
    {
      string writeaccess = "";
      string deprecated = "";
      string type = "";
      string subelementtype = "";
      string multiplicity = "";
      string key = "";
      string cctstype = "";
      string availableoninstance = "";
      try
      {
        if (treeElement == "nodeelement")
          return this.CreateNodeTreeElement(imageindex, count, businessObjectNode, ref writeaccess, ref deprecated, ref type, ref subelementtype, ref key, ref cctstype);
        if (treeElement == "action")
          return this.CreateActionTreeElement(imageindex, count, businessObjectNode, ref deprecated, ref subelementtype, ref availableoninstance);
        if (treeElement == "query")
          return this.CreateQueryTreeElement(imageindex, count, businessObjectNode, ref deprecated, ref subelementtype);
        if (treeElement == "association")
          return this.CreateAssociationsElement(imageindex, count, businessObjectNode, ref writeaccess, ref deprecated, ref type, ref subelementtype, ref multiplicity);
        return new TreeNode[0];
      }
      catch
      {
        this.richTextBoxInfoBoxBO.Visible = true;
        this.richTextBoxInfoBoxBO.Text = "An error occurred while loading the repository content. Try again. If the error occurs again, report an incident.";
        return new TreeNode[0];
      }
    }

    private TreeNode[] CreateAssociationsElement(int imageindex, int count, BusinessObject_Node businessObjectNode, ref string writeaccess, ref string deprecated, ref string type, ref string subelementtype, ref string multiplicity)
    {
      int length = 0;
      int index1 = 0;
      for (int index2 = 0; index2 < count; ++index2)
      {
        bool flag = false;
        for (int index3 = 0; index3 < this.openedBO.Length; ++index3)
        {
          if (this.openedBO[index3].Tag == businessObjectNode.Associations[index2])
            flag = true;
        }
        if (!flag && (businessObjectNode.Associations[index2].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(businessObjectNode.Associations[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(businessObjectNode.Associations[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked")))
          ++length;
      }
      TreeNode[] array = new TreeNode[length];
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      TreeNode treeNode3 = new TreeNode();
      for (int index2 = 0; index2 < count; ++index2)
      {
        bool flag = false;
        for (int index3 = 0; index3 < this.openedBO.Length; ++index3)
        {
          if (this.openedBO[index3].Tag == businessObjectNode.Associations[index2])
            flag = true;
        }
        if (!flag && (businessObjectNode.Associations[index2].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(businessObjectNode.Associations[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(businessObjectNode.Associations[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked")))
        {
          TreeNode treeNode4 = new TreeNode(businessObjectNode.Associations[index2].Name);
          treeNode4.Tag = (object) businessObjectNode.Associations[index2];
          if (businessObjectNode.Associations[index2].PublicSolutionModel != null)
          {
            if (businessObjectNode.Associations[index2].PublicSolutionModel.ReleaseStatusCode.ToString() != null)
              deprecated = !(businessObjectNode.Associations[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
            int num = businessObjectNode.Associations[index2].PublicSolutionModel.WriteAccessPublishedIndicator ? 1 : 0;
            writeaccess = !businessObjectNode.Associations[index2].PublicSolutionModel.WriteAccessPublishedIndicator ? "No" : "Yes";
          }
          switch (businessObjectNode.Associations[index2].TypeCode.ToString())
          {
            case "IntraBusinessObject":
              type = "Intra Business Object";
              break;
            case "IntraBusinessObjectIntraInstance":
              type = "Intra Business Object / Intra Instance";
              break;
            case "IntraBusinessObjectCrossInstance":
              type = "Intra Business Object / Cross Instance";
              break;
            case "CrossBusinessObject":
              type = "Cross Business Object";
              break;
            case "CrossDeploymentUnit":
              type = "Cross Deployment Unit";
              break;
            default:
              type = businessObjectNode.Associations[index2].TypeCode.ToString();
              break;
          }
          subelementtype = "Subelement Type: Association";
          int multiplicityCode = (int) businessObjectNode.Associations[index2].ConsistencyTargetAssociationMultiplicityCode;
          multiplicity = businessObjectNode.Associations[index2].ConsistencyTargetAssociationMultiplicityCode.ToString();
          multiplicity = multiplicity.Replace("Value_", "");
          multiplicity = multiplicity.Replace("_", "..");
          multiplicity = multiplicity.Replace("N", "*");
          treeNode4.ToolTipText = subelementtype + "\nDeprecated: " + deprecated + "\nPublic Write Access: " + writeaccess + "\nType: " + type + "\nMultiplicity: " + multiplicity;
          treeNode4.ContextMenuStrip = this.contextMenuStripSUBelement;
          TreeNode node = new TreeNode("An error occured");
          treeNode4.Nodes.Add(node);
          int typeCode = (int) businessObjectNode.Associations[index2].TypeCode;
          if (businessObjectNode.Associations[index2].TypeCode.ToString() == "CrossBusinessObject")
            treeNode4.ImageIndex = businessObjectNode.Associations[index2].ConsistencyTargetAssociationMultiplicityCode.ToString() == "Value_0_1" || businessObjectNode.Associations[index2].ConsistencyTargetAssociationMultiplicityCode.ToString() == "Value_1" ? (!(deprecated == "Yes") ? 12 : 23) : (!(deprecated == "Yes") ? 13 : 24);
          else if (businessObjectNode.Associations[index2].TypeCode.ToString() == "IntraBusinessObject" || businessObjectNode.Associations[index2].TypeCode.ToString() == "IntraBusinessObjectIntraInstance" || businessObjectNode.Associations[index2].TypeCode.ToString() == "IntraBusinessObjectCrossInstance")
            treeNode4.ImageIndex = businessObjectNode.Associations[index2].ConsistencyTargetAssociationMultiplicityCode.ToString() == "Value_0_1" || businessObjectNode.Associations[index2].ConsistencyTargetAssociationMultiplicityCode.ToString() == "Value_1" ? (!(deprecated == "Yes") ? 14 : 27) : (!(deprecated == "Yes") ? 15 : 28);
          treeNode4.SelectedImageIndex = treeNode4.ImageIndex;
          array[index1] = treeNode4;
          ++index1;
        }
      }
      Array.Sort<TreeNode>(array, (Comparison<TreeNode>) ((element1, element2) => element1.Text.CompareTo(element2.Text)));
      return array;
    }

    private TreeNode[] CreateQueryTreeElement(int imageindex, int count, BusinessObject_Node businessObjectNode, ref string deprecated, ref string subelementtype)
    {
      int length = 0;
      int index1 = 0;
      for (int index2 = 0; index2 < count; ++index2)
      {
        bool flag = false;
        for (int index3 = 0; index3 < this.openedBO.Length; ++index3)
        {
          if (this.openedBO[index3].Tag == businessObjectNode.Queries[index2])
            flag = true;
        }
        if (!flag && (businessObjectNode.Queries[index2].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(businessObjectNode.Queries[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(businessObjectNode.Queries[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked")))
          ++length;
      }
      TreeNode[] array = new TreeNode[length];
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      TreeNode treeNode3 = new TreeNode();
      if (this.toolStripButtonQueries.Checked)
      {
        for (int index2 = 0; index2 < count; ++index2)
        {
          bool flag = false;
          for (int index3 = 0; index3 < this.openedBO.Length; ++index3)
          {
            if (this.openedBO[index3].Tag == businessObjectNode.Queries[index2])
              flag = true;
          }
          if (!flag && (businessObjectNode.Queries[index2].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(businessObjectNode.Queries[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(businessObjectNode.Queries[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked")))
          {
            TreeNode treeNode4 = new TreeNode(businessObjectNode.Queries[index2].Name);
            treeNode4.Tag = (object) businessObjectNode.Queries[index2];
            if (businessObjectNode.Queries[index2].PublicSolutionModel != null && businessObjectNode.Queries[index2].PublicSolutionModel.ReleaseStatusCode.ToString() != null)
              deprecated = !(businessObjectNode.Queries[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
            subelementtype = "Subelement Type: Query";
            treeNode4.ToolTipText = subelementtype + "\nDeprecated: " + deprecated;
            treeNode4.ContextMenuStrip = this.contextMenuStripSUBelement;
            if (businessObjectNode.Queries[index2].TopLevelQueryParameters.Count != 0)
            {
              TreeNode node = new TreeNode("An error occured");
              treeNode4.Nodes.Add(node);
            }
            treeNode4.ContextMenuStrip = this.contextMenuStripQuery;
            treeNode4.ImageIndex = !(deprecated == "Yes") ? imageindex : 40;
            treeNode4.SelectedImageIndex = treeNode4.ImageIndex;
            array[index1] = treeNode4;
            ++index1;
          }
        }
      }
      Array.Sort<TreeNode>(array, (Comparison<TreeNode>) ((element1, element2) => element1.Text.CompareTo(element2.Text)));
      return array;
    }

    private TreeNode[] CreateActionTreeElement(int imageindex, int count, BusinessObject_Node businessObjectNode, ref string deprecated, ref string subelementtype, ref string availableoninstance)
    {
      int length = 0;
      int index1 = 0;
      for (int index2 = 0; index2 < count; ++index2)
      {
        bool flag = false;
        for (int index3 = 0; index3 < this.openedBO.Length; ++index3)
        {
          if (this.openedBO[index3].Tag == businessObjectNode.Actions[index2])
            flag = true;
        }
        if (!flag && (businessObjectNode.Actions[index2].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(businessObjectNode.Actions[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(businessObjectNode.Actions[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked")))
          ++length;
      }
      TreeNode[] array = new TreeNode[length];
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      TreeNode treeNode3 = new TreeNode();
      if (this.toolStripButtonActions.Checked)
      {
        for (int index2 = 0; index2 < count; ++index2)
        {
          bool flag = false;
          for (int index3 = 0; index3 < this.openedBO.Length; ++index3)
          {
            if (this.openedBO[index3].Tag == businessObjectNode.Actions[index2])
              flag = true;
          }
          if (!flag && (businessObjectNode.Actions[index2].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(businessObjectNode.Actions[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(businessObjectNode.Actions[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked")))
          {
            TreeNode treeNode4 = new TreeNode(businessObjectNode.Actions[index2].Name);
            treeNode4.Tag = (object) businessObjectNode.Actions[index2];
            if (businessObjectNode.Actions[index2].PublicSolutionModel != null && businessObjectNode.Actions[index2].PublicSolutionModel.ReleaseStatusCode.ToString() != null)
              deprecated = !(businessObjectNode.Actions[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
            subelementtype = "Subelement Type: Action";
            int multiplicityCode = (int) businessObjectNode.Actions[index2].InstanceMultiplicityCode;
            availableoninstance = !(businessObjectNode.Actions[index2].InstanceMultiplicityCode.ToString() == "NoRowsAsInput") ? "Instance" : "Object";
            treeNode4.ToolTipText = subelementtype + "\nDeprecated: " + deprecated + "\nAvailable on: " + availableoninstance;
            treeNode4.ContextMenuStrip = this.contextMenuStripSUBelement;
            if (businessObjectNode.Actions[index2].TopLevelActionParameters.Count != 0)
            {
              TreeNode node = new TreeNode("An error occured");
              treeNode4.Nodes.Add(node);
            }
            treeNode4.ImageIndex = !(deprecated == "Yes") ? imageindex : 39;
            treeNode4.SelectedImageIndex = treeNode4.ImageIndex;
            array[index1] = treeNode4;
            ++index1;
          }
        }
      }
      Array.Sort<TreeNode>(array, (Comparison<TreeNode>) ((element1, element2) => element1.Text.CompareTo(element2.Text)));
      return array;
    }

    private TreeNode[] CreateNodeTreeElement(int imageindex, int count, BusinessObject_Node businessObjectNode, ref string writeaccess, ref string deprecated, ref string type, ref string subelementtype, ref string key, ref string cctstype)
    {
      int length = 0;
      for (int index1 = 0; index1 < count; ++index1)
      {
        BusinessObject_NodeElement nodeElement = businessObjectNode.NodeElements[index1];
        if (nodeElement.ParentBusinessObjectNodeElementPathProxyName == "")
        {
          bool flag = false;
          for (int index2 = 0; index2 < this.openedBO.Length; ++index2)
          {
            if (this.openedBO[index2].Tag == nodeElement)
              flag = true;
          }
          if (!flag && (nodeElement.PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(nodeElement.PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(nodeElement.PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked")) && nodeElement.PublicSolutionModel != null)
            ++length;
        }
      }
      TreeNode[] array = new TreeNode[length];
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      int index3 = 0;
      for (int index1 = 0; index1 < count; ++index1)
      {
        BusinessObject_NodeElement nodeElement = businessObjectNode.NodeElements[index1];
        if (nodeElement.ParentBusinessObjectNodeElementPathProxyName == "")
        {
          bool flag = false;
          int index2 = 0;
          if (index2 < this.openedBO.Length && this.openedBO[index2].Tag == nodeElement)
            flag = true;
          if (!flag && (nodeElement.PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(nodeElement.PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(nodeElement.PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked")) && nodeElement.PublicSolutionModel != null)
          {
            TreeNode treeNode3 = new TreeNode(nodeElement.Name);
            treeNode3.Tag = (object) nodeElement;
            if (nodeElement.PublicSolutionModel != null)
            {
              if (nodeElement.PublicSolutionModel.ReleaseStatusCode.ToString() != null)
                deprecated = !(nodeElement.PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
              int num = nodeElement.PublicSolutionModel.WriteAccessPublishedIndicator ? 1 : 0;
              writeaccess = !nodeElement.PublicSolutionModel.WriteAccessPublishedIndicator ? "No" : "Yes";
            }
            int num1 = nodeElement.AlternativeKeyIndicator ? 1 : 0;
            key = !nodeElement.AlternativeKeyIndicator ? "No" : "Yes";
            subelementtype = "Subelement Type: Node Element";
            treeNode3.ToolTipText = subelementtype + "\nDeprecated: " + deprecated + "\nPublic Write Access: " + writeaccess + "\nKey: " + key;
            treeNode3.ContextMenuStrip = this.contextMenuStripSUBelement;
            if (nodeElement.ChildNodeElements.Count != 0)
            {
              TreeNode node = new TreeNode("An error occured");
              treeNode3.Nodes.Add(node);
            }
            treeNode3.ImageIndex = !(deprecated == "Yes") ? (key == "true" || key == "True" ? 42 : (nodeElement.PublicSolutionModel == null || nodeElement.PublicSolutionModel.WriteAccessPublishedIndicator ? imageindex : 35)) : 26;
            treeNode3.SelectedImageIndex = treeNode3.ImageIndex;
            array[index3] = treeNode3;
            ++index3;
          }
        }
      }
      Array.Sort<TreeNode>(array, (Comparison<TreeNode>) ((element1, element2) => element1.Text.CompareTo(element2.Text)));
      return array;
    }

    private void createSubElements(TreeNode treeNode, BusinessObject_Node boNode)
    {
      TreeNode treeNode1 = new TreeNode();
      if (this.toolStripButtonActions.Checked)
      {
        DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Create actions started");
        TreeNode[] nodes = (TreeNode[]) this.createTreeElement("action", 1, boNode.Actions.Count, boNode).Clone();
        treeNode.Nodes.AddRange(nodes);
        SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Create actions finished in: ");
      }
      DateTime start1 = SAP.Copernicus.Core.Util.Util.startMeasurement("Create nodeelements started");
      TreeNode[] nodes1 = (TreeNode[]) this.createTreeElement("nodeelement", 3, boNode.NodeElements.Count, boNode).Clone();
      treeNode.Nodes.AddRange(nodes1);
      SAP.Copernicus.Core.Util.Util.endMeasurement(start1, "Create nodeelements finished in: ");
      if (this.toolStripButtonQueries.Checked)
      {
        DateTime start2 = SAP.Copernicus.Core.Util.Util.startMeasurement("Create queries started");
        TreeNode[] nodes2 = (TreeNode[]) this.createTreeElement("query", 6, boNode.Queries.Count, boNode).Clone();
        treeNode.Nodes.AddRange(nodes2);
        SAP.Copernicus.Core.Util.Util.endMeasurement(start2, "Create queries finished in: ");
      }
      if (!this.toolStripButtonAssociations.Checked)
        return;
      DateTime start3 = SAP.Copernicus.Core.Util.Util.startMeasurement("Create associations started");
      TreeNode[] nodes3 = (TreeNode[]) this.createTreeElement("association", 5, boNode.Associations.Count, boNode).Clone();
      treeNode.Nodes.AddRange(nodes3);
      SAP.Copernicus.Core.Util.Util.endMeasurement(start3, "Create associations finished in: ");
    }

    private void createQueryAndActionParaAndSubNodeElements(TreeNode TreeNodeToExpand, string element)
    {
      if (element == "queryparameter")
        this.createSubQueryParameters(TreeNodeToExpand);
      else if (element == "action")
        this.createActionParameters(TreeNodeToExpand);
      else if (element == "actionparameter")
        this.createSubActionParameters(TreeNodeToExpand);
      else if (element == "nodeelement")
      {
        this.createSubNodeElements(TreeNodeToExpand);
      }
      else
      {
        if (!(element == "query"))
          return;
        this.createQueryParameters(TreeNodeToExpand);
      }
    }

    private void createQueryParameters(TreeNode TreeNodeToExpand)
    {
      BusinessObject_Query tag = (BusinessObject_Query) TreeNodeToExpand.Tag;
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      string str1 = "";
      int length = 0;
      for (int index = 0; index < tag.TopLevelQueryParameters.Count; ++index)
      {
        if (tag.TopLevelQueryParameters[index].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(tag.TopLevelQueryParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(tag.TopLevelQueryParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
          ++length;
      }
      TreeNode[] treeNodeArray = new TreeNode[length];
      for (int index = 0; index < tag.TopLevelQueryParameters.Count; ++index)
      {
        if (tag.TopLevelQueryParameters[index].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(tag.TopLevelQueryParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(tag.TopLevelQueryParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
        {
          TreeNode treeNode3 = new TreeNode(tag.TopLevelQueryParameters[index].Name);
          treeNode3.ImageIndex = 18;
          treeNode3.SelectedImageIndex = 18;
          treeNode3.Tag = (object) tag.TopLevelQueryParameters[index];
          if (tag.TopLevelQueryParameters[index].PublicSolutionModel != null)
          {
            int releaseStatusCode = (int) tag.TopLevelQueryParameters[index].PublicSolutionModel.ReleaseStatusCode;
            str1 = !(tag.TopLevelQueryParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
          }
          string str2 = "Subelement Type: Query Parameter";
          treeNode3.ToolTipText = str2 + "\nDeprecated: " + str1;
          treeNode3.ContextMenuStrip = this.contextMenuStripSUBelement;
          treeNodeArray[index] = treeNode3;
          if (tag.TopLevelQueryParameters[index].ChildQueryParameters.Count != 0)
          {
            TreeNode node = new TreeNode("An error occured");
            treeNode3.Nodes.Add(node);
          }
        }
      }
      Array.Sort<TreeNode>(treeNodeArray, (Comparison<TreeNode>) ((element1, element2) => element1.Text.CompareTo(element2.Text)));
      TreeNodeToExpand.Nodes.AddRange(treeNodeArray);
    }

    private void createSubNodeElements(TreeNode TreeNodeToExpand)
    {
      BusinessObject_NodeElement tag = (BusinessObject_NodeElement) TreeNodeToExpand.Tag;
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      TreeNode[] treeNodeArray = new TreeNode[tag.ChildNodeElements.Count];
      string str1 = "";
      string str2 = "";
      for (int index = 0; index < tag.ChildNodeElements.Count; ++index)
      {
        if (tag.ChildNodeElements[index].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(tag.ChildNodeElements[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(tag.ChildNodeElements[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
        {
          TreeNode treeNode3 = new TreeNode(tag.ChildNodeElements[index].Name.Replace(tag.Name, "").Replace("/", ""));
          treeNode3.Tag = (object) tag.ChildNodeElements[index];
          if (tag.ChildNodeElements[index].PublicSolutionModel != null)
          {
            if (tag.ChildNodeElements[index].PublicSolutionModel.ReleaseStatusCode.ToString() != null)
              str2 = !(tag.ChildNodeElements[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
            int num = tag.ChildNodeElements[index].PublicSolutionModel.WriteAccessPublishedIndicator ? 1 : 0;
            str1 = !tag.ChildNodeElements[index].PublicSolutionModel.WriteAccessPublishedIndicator ? "No" : "Yes";
          }
          int num1 = tag.ChildNodeElements[index].AlternativeKeyIndicator ? 1 : 0;
          string str3 = !tag.ChildNodeElements[index].AlternativeKeyIndicator ? "No" : "Yes";
          string str4 = "Subelement Type: Node Element";
          treeNode3.ToolTipText = str4 + "\nDeprecated: " + str2 + "\nPublic Write Access: " + str1 + "\nKey: " + str3;
          treeNode3.ContextMenuStrip = this.contextMenuStripSUBelement;
          treeNode3.ImageIndex = !(str2 == "Yes") ? (!(str3 == "Yes") ? (tag.ChildNodeElements[index].PublicSolutionModel == null || tag.ChildNodeElements[index].PublicSolutionModel.WriteAccessPublishedIndicator ? 3 : 35) : 42) : 26;
          treeNode3.SelectedImageIndex = treeNode3.ImageIndex;
          if (tag.ChildNodeElements[index].ChildNodeElements.Count != 0)
          {
            TreeNode node = new TreeNode("An error occured");
            treeNode3.Nodes.Add(node);
          }
          treeNodeArray[index] = treeNode3;
        }
      }
      Array.Sort<TreeNode>(treeNodeArray, (Comparison<TreeNode>) ((element1, element2) => element1.Text.CompareTo(element2.Text)));
      TreeNodeToExpand.Nodes.AddRange(treeNodeArray);
    }

    private void createSubActionParameters(TreeNode TreeNodeToExpand)
    {
      BusinessObject_ActionParameter tag = (BusinessObject_ActionParameter) TreeNodeToExpand.Tag;
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      int length = 0;
      string str1 = "";
      for (int index = 0; index < tag.ChildActionParameters.Count; ++index)
      {
        if (tag.ChildActionParameters[index].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(tag.ChildActionParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(tag.ChildActionParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
          ++length;
      }
      BusinessObject_ActionParameter[] array = new BusinessObject_ActionParameter[length];
      for (int index = 0; index < tag.ChildActionParameters.Count; ++index)
      {
        if (tag.ChildActionParameters[index].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(tag.ChildActionParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(tag.ChildActionParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
          array[index] = tag.ChildActionParameters[index];
      }
      Array.Sort<BusinessObject_ActionParameter>(array, (Comparison<BusinessObject_ActionParameter>) ((element1, element2) => element1.OrdinalNumberValue.CompareTo(element2.OrdinalNumberValue)));
      TreeNode[] nodes = new TreeNode[array.Length];
      for (int index = 0; index < array.Length; ++index)
      {
        TreeNode treeNode3 = new TreeNode(array[index].Name.Replace(tag.Name, "").Replace("/", ""));
        treeNode3.ImageIndex = 44;
        treeNode3.SelectedImageIndex = treeNode3.SelectedImageIndex;
        treeNode3.Tag = (object) array[index];
        if (array[index].PublicSolutionModel != null)
        {
          int releaseStatusCode = (int) array[index].PublicSolutionModel.ReleaseStatusCode;
          str1 = !(array[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
        }
        string str2 = "Subelement Type: Action Parameter";
        treeNode3.ToolTipText = str2 + "\nDeprecated: " + str1;
        treeNode3.ContextMenuStrip = this.contextMenuStripSUBelement;
        if (array[index].ChildActionParameters.Count != 0)
        {
          TreeNode node = new TreeNode("An error occured");
          treeNode3.Nodes.Add(node);
        }
        nodes[index] = treeNode3;
      }
      TreeNodeToExpand.Nodes.AddRange(nodes);
    }

    private void createActionParameters(TreeNode TreeNodeToExpand)
    {
      BusinessObject_Action tag = (BusinessObject_Action) TreeNodeToExpand.Tag;
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      string str1 = "";
      int length = 0;
      for (int index = 0; index < tag.TopLevelActionParameters.Count; ++index)
      {
        if (tag.TopLevelActionParameters[index].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(tag.TopLevelActionParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(tag.TopLevelActionParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
          ++length;
      }
      BusinessObject_ActionParameter[] array = new BusinessObject_ActionParameter[length];
      for (int index = 0; index < tag.TopLevelActionParameters.Count; ++index)
      {
        if (tag.TopLevelActionParameters[index].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(tag.TopLevelActionParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(tag.TopLevelActionParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
          array[index] = tag.TopLevelActionParameters[index];
      }
      Array.Sort<BusinessObject_ActionParameter>(array, (Comparison<BusinessObject_ActionParameter>) ((element1, element2) => element1.OrdinalNumberValue.CompareTo(element2.OrdinalNumberValue)));
      TreeNode[] nodes = new TreeNode[array.Length];
      for (int index = 0; index < array.Length; ++index)
      {
        TreeNode treeNode3 = new TreeNode(array[index].Name);
        if (array[index].PublicSolutionModel != null)
        {
          int releaseStatusCode = (int) array[index].PublicSolutionModel.ReleaseStatusCode;
          str1 = !(array[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
        }
        string str2 = "Subelement Type: Action Parameter";
        treeNode3.ToolTipText = str2 + "\nDeprecated: " + str1;
        treeNode3.ContextMenuStrip = this.contextMenuStripSUBelement;
        treeNode3.ImageIndex = 44;
        treeNode3.SelectedImageIndex = treeNode3.ImageIndex;
        treeNode3.Tag = (object) array[index];
        if (array[index].ChildActionParameters.Count != 0)
        {
          TreeNode node = new TreeNode("An error occured");
          treeNode3.Nodes.Add(node);
        }
        nodes[index] = treeNode3;
      }
      TreeNodeToExpand.Nodes.AddRange(nodes);
    }

    private void createSubQueryParameters(TreeNode TreeNodeToExpand)
    {
      BusinessObject_QueryParameter tag = (BusinessObject_QueryParameter) TreeNodeToExpand.Tag;
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      int length = 0;
      string str1 = "";
      for (int index = 0; index < tag.ChildQueryParameters.Count; ++index)
      {
        if (tag.ChildQueryParameters[index].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(tag.ChildQueryParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(tag.ChildQueryParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
          ++length;
      }
      TreeNode[] treeNodeArray = new TreeNode[length];
      for (int index = 0; index < tag.ChildQueryParameters.Count; ++index)
      {
        if (tag.ChildQueryParameters[index].PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(tag.ChildQueryParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(tag.ChildQueryParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
        {
          TreeNode treeNode3 = new TreeNode(tag.ChildQueryParameters[index].Name.Replace(tag.Name, "").Replace("/", ""));
          treeNode3.ImageIndex = 18;
          treeNode3.SelectedImageIndex = 18;
          treeNode3.Tag = (object) tag.ChildQueryParameters[index];
          if (tag.ChildQueryParameters[index].PublicSolutionModel != null)
          {
            int releaseStatusCode = (int) tag.ChildQueryParameters[index].PublicSolutionModel.ReleaseStatusCode;
            str1 = !(tag.ChildQueryParameters[index].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
          }
          string str2 = "Subelement Type: Query Parameter";
          treeNode3.ToolTipText = str2 + "\nDeprecated: " + str1;
          treeNode3.ContextMenuStrip = this.contextMenuStripSUBelement;
          treeNodeArray[index] = treeNode3;
          if (tag.ChildQueryParameters[index].ChildQueryParameters.Count != 0)
          {
            TreeNode node = new TreeNode("An error occured");
            treeNode3.Nodes.Add(node);
          }
        }
      }
      Array.Sort<TreeNode>(treeNodeArray, (Comparison<TreeNode>) ((element1, element2) => element1.Text.CompareTo(element2.Text)));
      TreeNodeToExpand.Nodes.AddRange(treeNodeArray);
    }

    private void AfterExpand(TreeNode TreeNodeToExpand)
    {
      DateTime start1 = SAP.Copernicus.Core.Util.Util.startMeasurement("AfterExpand started");
      if (!this.isNextRebuild)
      {
        if (!this.isDeprecationRebuild)
        {
          ++CopernicusBusinessObjectBrowser.openCountBO;
          TreeNode[] treeNodeArray = new TreeNode[CopernicusBusinessObjectBrowser.openCountBO];
          Array.Copy((Array) this.openedBO, (Array) treeNodeArray, CopernicusBusinessObjectBrowser.openCountBO - 1);
          this.lastOpened = TreeNodeToExpand;
          treeNodeArray[CopernicusBusinessObjectBrowser.openCountBO - 1] = this.lastOpened;
          this.openedBO = (TreeNode[]) treeNodeArray.Clone();
          this.toolStripButtonBack.Enabled = true;
        }
        if (this.closedBO.Length != 0 && !this.isDeprecationRebuild)
        {
          bool flag = false;
          for (int index = 0; index < this.closedBO.Length; ++index)
          {
            if (this.closedBO[index].Tag == TreeNodeToExpand.Tag)
            {
              flag = true;
              break;
            }
          }
          if (!flag)
          {
            CopernicusBusinessObjectBrowser.closedCountBO = 0;
            this.closedBO = new TreeNode[0];
            this.toolStripButtonNext.Enabled = false;
          }
        }
        for (int index = 0; index < TreeNodeToExpand.Nodes.Count; ++index)
        {
          if (TreeNodeToExpand.Nodes[index].Text == "An error occured")
            TreeNodeToExpand.Nodes[index].Remove();
        }
        TreeNode treeNode1 = new TreeNode();
        TreeNode treeNode2 = new TreeNode();
        TreeNode treeNode3 = new TreeNode();
        if (TreeNodeToExpand.Tag.GetType() == typeof (BusinessObject_Query))
          this.createQueryAndActionParaAndSubNodeElements(TreeNodeToExpand, "query");
        else if (TreeNodeToExpand.Tag.GetType() == typeof (BusinessObject_QueryParameter))
          this.createQueryAndActionParaAndSubNodeElements(TreeNodeToExpand, "queryparameter");
        else if (TreeNodeToExpand.Tag.GetType() == typeof (BusinessObject_NodeElement))
          this.createQueryAndActionParaAndSubNodeElements(TreeNodeToExpand, "nodeelement");
        else if (TreeNodeToExpand.Tag.GetType() == typeof (BusinessObject_Action))
          this.createQueryAndActionParaAndSubNodeElements(TreeNodeToExpand, "action");
        else if (TreeNodeToExpand.Tag.GetType() == typeof (BusinessObject_ActionParameter))
          this.createQueryAndActionParaAndSubNodeElements(TreeNodeToExpand, "actionparameter");
        else if (TreeNodeToExpand.Parent == null)
        {
          if (TreeNodeToExpand.Tag != null && TreeNodeToExpand.Tag.GetType() == typeof (bostruct))
          {
            RepositoryDataSet.NodesRow[] array = (RepositoryDataSet.NodesRow[]) this.repositoryDataSet.Tables["Nodes"].Select("BOProxyName='" + ((bostruct) TreeNodeToExpand.Tag).proxynamebo + "' AND ParentNodeName=''");
            Array.Sort<RepositoryDataSet.NodesRow>(array, (Comparison<RepositoryDataSet.NodesRow>) ((bolist1, bolist2) => bolist1.Name.CompareTo(bolist2.Name)));
            for (int index = 0; index < array.Length; ++index)
            {
              nodestruct nodestruct = new nodestruct();
              nodestruct.boproxyname = array[index].BOProxyName;
              nodestruct.proxynamenode = array[index].ProxyName;
              nodestruct.displaynamenode = array[index].Name;
              TreeNode treeNode4 = new TreeNode(array[index].Name);
              DateTime start2 = SAP.Copernicus.Core.Util.Util.startMeasurement("LoadNode " + nodestruct.proxynamenode + " started");
              BusinessObject_Node nodefromModelApi = this.getBONodefromModelAPI(nodestruct.boproxyname, nodestruct.proxynamenode);
              SAP.Copernicus.Core.Util.Util.endMeasurement(start2, "LoadNode " + nodestruct.proxynamenode + " finished in: ");
              treeNode4.Tag = (object) nodefromModelApi;
              TreeNodeToExpand.Nodes.Add(treeNode4);
              treeNode4.Nodes.Add(new TreeNode("An error occured")
              {
                Tag = (object) "dummy"
              });
              string str1 = "";
              string str2 = "";
              if (nodefromModelApi.PublicSolutionModel == null || (this.toolStripButtonDeprecation.Checked || !(nodefromModelApi.PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")) && !(nodefromModelApi.PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked"))
              {
                if (nodefromModelApi.PublicSolutionModel != null)
                {
                  if (nodefromModelApi.PublicSolutionModel.ReleaseStatusCode.ToString() != null)
                    str1 = !(nodefromModelApi.PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
                  int num = nodefromModelApi.PublicSolutionModel.WriteAccessPublishedIndicator ? 1 : 0;
                  string str3 = !nodefromModelApi.PublicSolutionModel.WriteAccessPublishedIndicator ? "No" : "Yes";
                  str2 = nodefromModelApi.FieldExtensibleIndicator ? "Yes" : "No";
                  string str4 = nodefromModelApi.FieldExtensibleIndicator.ToString();
                  string str5 = nodefromModelApi.CategoryCode.ToString();
                  string str6 = "Subelement Type: Node";
                  treeNode4.ToolTipText = str6 + "\nDeprecated: " + str1 + "\nPublic Write Access: " + str3 + "\nExtensible: " + str4 + "\nCategory: " + str5;
                  treeNode4.ContextMenuStrip = this.contextMenuStripSUBelement;
                  treeNode4.ImageIndex = !(str1 == "Yes") ? (nodefromModelApi.ReadOnlyOperationIndicator == null || !nodefromModelApi.ReadOnlyOperationIndicator.Indicator && !nodefromModelApi.ReadOnlyOperationIndicator.FinalIndicator ? (!(str4 == "Yes") ? 4 : 41) : 38) : 29;
                  treeNode4.SelectedImageIndex = treeNode4.ImageIndex;
                }
                this.createSubElements(treeNode4, nodefromModelApi);
                if (!this.isDeprecationRebuild)
                {
                  treeNode4.Expand();
                  treeNode4.EnsureVisible();
                }
              }
            }
          }
        }
        else
        {
          BusinessObject_Node boNode = !(TreeNodeToExpand.Tag.GetType() == typeof (BusinessObject_Association)) ? (BusinessObject_Node) TreeNodeToExpand.Tag : ((BusinessObject_Association) TreeNodeToExpand.Tag).TargetNode;
          BusinessObject_Node[] array = new BusinessObject_Node[boNode.ChildNodes.Count];
          int index1 = 0;
          for (int index2 = 0; index2 < boNode.ChildNodes.Count; ++index2)
          {
            array[index1] = boNode.ChildNodes[index2];
            ++index1;
          }
          Array.Sort<BusinessObject_Node>(array, (Comparison<BusinessObject_Node>) ((child1, child2) => child1.SemanticalName.CompareTo(child2.SemanticalName)));
          if (TreeNodeToExpand.Nodes.Count == 0 || this.isDeprecationRebuild)
            this.createSubElements(TreeNodeToExpand, boNode);
          if (this.toolStripButtonNodes.Checked)
          {
            for (int index2 = 0; index2 < array.Length; ++index2)
            {
              try
              {
                if (array[index2].PublicSolutionModel != null)
                {
                  if (!this.toolStripButtonDeprecation.Checked)
                  {
                    if (array[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated")
                      continue;
                  }
                  if (array[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Revoked")
                    continue;
                }
                if (array[index2].IsDOInclusionNode)
                {
                  bool flag = false;
                  string text = array[index2].Name == "Root" || array[index2].Name == "ROOT" || array[index2].Name == "root" ? array[index2].SemanticalName.Replace(" ", "") : array[index2].Name;
                  for (int index3 = 0; index3 < boNode.Associations.Count; ++index3)
                  {
                    if (text == boNode.Associations[index3].Name)
                      flag = true;
                  }
                  if (flag)
                  {
                    TreeNode node = new TreeNode(text);
                    node.Tag = (object) array[index2];
                    string str1 = "";
                    string str2 = "";
                    if (array[index2].PublicSolutionModel != null)
                    {
                      if (array[index2].PublicSolutionModel.ReleaseStatusCode.ToString() != null)
                        str2 = !(array[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
                      int num = array[index2].PublicSolutionModel.WriteAccessPublishedIndicator ? 1 : 0;
                      str1 = !array[index2].PublicSolutionModel.WriteAccessPublishedIndicator ? "No" : "Yes";
                    }
                    string str3 = array[index2].FieldExtensibleIndicator ? "Yes" : "No";
                    string str4 = array[index2].CategoryCode.ToString();
                    string str5 = "Subelement Type: Node";
                    node.ToolTipText = str5 + "\nDeprecated: " + str2 + "\nPublic Write Access: " + str1 + "\nExtensible: " + str3 + "\nCategory: " + str4;
                    node.ContextMenuStrip = this.contextMenuStripSUBelement;
                    TreeNodeToExpand.Nodes.Add(node);
                    node.ImageIndex = !(str2 == "Yes") ? (array[index2].ReadOnlyOperationIndicator == null || !array[index2].ReadOnlyOperationIndicator.Indicator && !array[index2].ReadOnlyOperationIndicator.FinalIndicator ? (!(str3 == "Yes") ? 4 : 41) : 38) : 29;
                    node.SelectedImageIndex = node.ImageIndex;
                    node.Nodes.Add(new TreeNode("An error occured")
                    {
                      Tag = (object) "dummy"
                    });
                  }
                }
                else
                {
                  TreeNode node = new TreeNode(array[index2].Name == "Root" || array[index2].Name == "ROOT" || array[index2].Name == "root" ? array[index2].SemanticalName.Replace(" ", "") : array[index2].Name);
                  node.Tag = (object) array[index2];
                  string str1 = "";
                  string str2 = "";
                  if (array[index2].PublicSolutionModel != null)
                  {
                    if (array[index2].PublicSolutionModel.ReleaseStatusCode.ToString() != null)
                      str2 = !(array[index2].PublicSolutionModel.ReleaseStatusCode.ToString() == "Deprecated") ? "No" : "Yes";
                    int num = array[index2].PublicSolutionModel.WriteAccessPublishedIndicator ? 1 : 0;
                    str1 = !array[index2].PublicSolutionModel.WriteAccessPublishedIndicator ? "No" : "Yes";
                  }
                  string str3 = array[index2].FieldExtensibleIndicator ? "Yes" : "No";
                  string str4 = array[index2].CategoryCode.ToString();
                  string str5 = "Subelement Type: Node";
                  node.ToolTipText = str5 + "\nDeprecated: " + str2 + "\nPublic Write Access: " + str1 + "\nExtensible: " + str3 + "\nCategory: " + str4;
                  node.ContextMenuStrip = this.contextMenuStripSUBelement;
                  node.ImageIndex = !(str2 == "Yes") ? (array[index2].ReadOnlyOperationIndicator == null || !array[index2].ReadOnlyOperationIndicator.Indicator && !array[index2].ReadOnlyOperationIndicator.FinalIndicator ? (!(str3 == "Yes") ? 4 : 41) : 38) : 29;
                  node.SelectedImageIndex = node.ImageIndex;
                  node.Nodes.Add(new TreeNode("An error occured")
                  {
                    Tag = (object) "dummy"
                  });
                  TreeNodeToExpand.Nodes.Add(node);
                }
              }
              catch
              {
              }
            }
          }
        }
      }
      SAP.Copernicus.Core.Util.Util.endMeasurement(start1, "AfterExpand finished in: ");
    }

    private void treeViewBO_AfterExpand(object sender, TreeViewEventArgs e)
    {
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      this.Cursor = Cursors.WaitCursor;
      service.SetText("Loading content...");
      try
      {
        this.AfterExpand(e.Node);
        e.Node.EnsureVisible();
      }
      catch
      {
        this.richTextBoxInfoBoxBO.Text = "An error occurred while loading the repository content. Try again. If the error occurs again, report an incident.";
        this.richTextBoxInfoBoxBO.Visible = true;
      }
      this.Cursor = Cursors.Default;
      service.SetText("Ready");
    }

    private void treeViewBO_AfterSelect(object sender, TreeViewEventArgs e)
    {
      if (this.lastSelected != null && this.lastSelected == e.Node)
        return;
      this.Cursor = Cursors.WaitCursor;
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      service.SetText("Loading documentation...");
      this.lastSelected = e.Node;
      this.webBrowserBO.DocumentText = "\n Loading documentation. Please wait...";
      string str = (string) null;
      RepositoryQueryHandler repositoryQueryHandler = new RepositoryQueryHandler();
      foreach (TabPage tabPage in this.tabControlBO.TabPages)
      {
        if (tabPage.Text.Equals("Query Result"))
          this.tabControlBO.TabPages.Remove(tabPage);
        if (tabPage.Text.Equals("Data Type"))
        {
          this.selectedBOElementTabIndex = this.tabControlBO.SelectedIndex;
          this.tabControlBO.TabPages.Remove(tabPage);
        }
      }
      try
      {
        if (e.Node.Tag.GetType() == typeof (BusinessObject_Association))
        {
          BusinessObject_Association tag = (BusinessObject_Association) e.Node.Tag;
          str = tag.Name == "ToParent" || tag.Name == "ToRoot" ? "" : repositoryQueryHandler.Get_Docu_Service(tag.Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName, tag.Key.BusinessObjectNodeKey.ProxyName, tag.Key.ProxyName, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null);
        }
        else if (e.Node.Tag.GetType() == typeof (bostruct))
        {
          bostruct tag = (bostruct) e.Node.Tag;
          str = repositoryQueryHandler.Get_Docu_Service(tag.proxynamebo, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null);
        }
        else if (e.Node.Tag.GetType() == typeof (BusinessObject_Node))
        {
          BusinessObject_Node tag = (BusinessObject_Node) e.Node.Tag;
          str = repositoryQueryHandler.Get_Docu_Service(tag.Key.BusinessObjectProxyKey.ProxyName, tag.Key.ProxyName, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null);
        }
        else if (e.Node.Tag.GetType() == typeof (BusinessObject_Action))
        {
          BusinessObject_Action tag = (BusinessObject_Action) e.Node.Tag;
          str = repositoryQueryHandler.Get_Docu_Service(tag.Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName, tag.Key.BusinessObjectNodeKey.ProxyName, (string) null, tag.Key.ProxyName, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null);
        }
        else if (e.Node.Tag.GetType() == typeof (BusinessObject_Query))
        {
          BusinessObject_Query tag = (BusinessObject_Query) e.Node.Tag;
          str = repositoryQueryHandler.Get_Docu_Service(tag.Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName, tag.Key.BusinessObjectNodeKey.ProxyName, (string) null, (string) null, tag.Key.ProxyName, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null);
        }
        else if (!(e.Node.Tag.GetType() == typeof (BusinessObject_QueryParameter)))
        {
          if (!(e.Node.Tag.GetType() == typeof (BusinessObject_ActionParameter)))
          {
            if (e.Node.Tag.GetType() == typeof (BusinessObject_NodeElement))
            {
              BusinessObject_NodeElement tag = (BusinessObject_NodeElement) e.Node.Tag;
              str = repositoryQueryHandler.Get_Docu_Service(tag.Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName, tag.Key.BusinessObjectNodeKey.ProxyName, (string) null, (string) null, (string) null, tag.Key.DataTypeElementPathProxyName, (string) null, (string) null, (string) null, (string) null, (string) null);
              TabPage tabPage = new TabPage("Data Type");
              WebBrowser webBrowser = new WebBrowser();
              string datatypeName = "";
              XmlDocument xmlDocument = new XmlDocument();
              xmlDocument.InnerXml = str;
              XmlNodeList elementsByTagName = xmlDocument.GetElementsByTagName("td");
              for (int index = 0; index < elementsByTagName.Count; ++index)
              {
                if (elementsByTagName[index].InnerText == "Data Type")
                {
                  datatypeName = elementsByTagName[index + 1].InnerText;
                  break;
                }
              }
              string docuForDataType = this.getDocuForDataType(((IEnumerable<RepositoryDataSet.DataTypesRow>) RepositoryDataCache.GetInstance().QueryDataTypes((string) null, (string) null, (string) null, (string) null, (string) null, (string) null)).First<RepositoryDataSet.DataTypesRow>((Func<RepositoryDataSet.DataTypesRow, bool>) (item => item.Name == datatypeName)).ProxyName);
              webBrowser.ScriptErrorsSuppressed = true;
              webBrowser.Navigate("");
              while (webBrowser.Document == (HtmlDocument) null || webBrowser.Document.Body == (HtmlElement) null)
                Application.DoEvents();
              webBrowser.Document.Write(docuForDataType);
              webBrowser.Dock = DockStyle.Fill;
              tabPage.Controls.Add((Control) webBrowser);
              this.tabControlBO.TabPages.Add(tabPage);
              this.tabControlBO.SelectedIndex = this.selectedBOElementTabIndex;
            }
          }
        }
      }
      catch
      {
        str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the selected entity.</p></body></html>";
      }
      if (str == "" || str == null)
        str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the selected entity.</p></body></html>";
      this.webBrowserBO.DocumentText = str;
      this.Cursor = Cursors.Default;
      service.SetText("Ready");
    }

    private void AfterCollapse(TreeNode treenodeToCollapse)
    {
      TreeNode treeNode = new TreeNode();
      if (this.isDeprecationRebuild)
      {
        for (int index1 = 0; index1 < treenodeToCollapse.Nodes.Count; ++index1)
        {
          bool flag = false;
          for (int index2 = 0; index2 < this.openedBO.Length; ++index2)
          {
            if (treenodeToCollapse.Nodes[index1].Tag == this.openedBO[index2].Tag)
            {
              flag = true;
              break;
            }
          }
          if (!flag)
          {
            treenodeToCollapse.Nodes[index1].Remove();
            --index1;
          }
        }
      }
      else
      {
        int index1 = this.openedBO.Length - 1;
        if (treenodeToCollapse.Tag == this.openedBO[index1].Tag)
        {
          --CopernicusBusinessObjectBrowser.openCountBO;
          TreeNode[] treeNodeArray = new TreeNode[CopernicusBusinessObjectBrowser.openCountBO];
          Array.Copy((Array) this.openedBO, (Array) treeNodeArray, CopernicusBusinessObjectBrowser.openCountBO);
          this.openedBO = (TreeNode[]) treeNodeArray.Clone();
        }
        else
        {
          for (int index2 = index1; index2 >= 0; --index2)
          {
            if (treenodeToCollapse.Tag == this.openedBO[index2].Tag)
            {
              CopernicusBusinessObjectBrowser.openCountBO = index2;
              TreeNode[] treeNodeArray = new TreeNode[CopernicusBusinessObjectBrowser.openCountBO];
              Array.Copy((Array) this.openedBO, (Array) treeNodeArray, CopernicusBusinessObjectBrowser.openCountBO);
              this.openedBO = (TreeNode[]) treeNodeArray.Clone();
            }
          }
        }
        if (!this.isBackClicked)
        {
          treenodeToCollapse.Nodes.Clear();
          TreeNode node = new TreeNode("An error occured");
          treenodeToCollapse.Nodes.Add(node);
        }
        if (this.openedBO.Length != 0)
          return;
        this.toolStripButtonBack.Enabled = false;
      }
    }

    private void treeViewBO_AfterCollapse(object sender, TreeViewEventArgs e)
    {
      if (this.isDeprecationRebuild)
        ++this.DeprecationRebuildHelper;
      if (this.DeprecationRebuildHelper > 1)
        return;
      this.AfterCollapse(e.Node);
    }

    private void webBrowserBO_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      for (int index = 0; index < this.webBrowserBO.Document.Links.Count; ++index)
        this.webBrowserBO.Document.Links[index].Click += new HtmlElementEventHandler(this.LinkClick);
    }

    private void LinkClick(object sender, EventArgs e)
    {
      this.bCancel = true;
      int num = (int) MessageBox.Show("All links in the documentation are disabled.\nTo view the documentation for a specific entity, select the entity in the tree view.");
    }

    private void webBrowserBO_Navigating(object sender, WebBrowserNavigatingEventArgs e)
    {
      if (!this.bCancel)
        return;
      e.Cancel = true;
      this.bCancel = false;
    }

    private void webBrowserDT_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      for (int index = 0; index < this.webBrowserDT.Document.Links.Count; ++index)
        this.webBrowserDT.Document.Links[index].Click += new HtmlElementEventHandler(this.LinkClick);
    }

    private void webBrowserDT_Navigating(object sender, WebBrowserNavigatingEventArgs e)
    {
      if (!this.bCancel)
        return;
      e.Cancel = true;
      this.bCancel = false;
    }

    private void webBrowserEO_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      for (int index = 0; index < this.webBrowserEO.Document.Links.Count; ++index)
        this.webBrowserEO.Document.Links[index].Click += new HtmlElementEventHandler(this.LinkClick);
    }

    private void webBrowserEO_Navigating(object sender, WebBrowserNavigatingEventArgs e)
    {
      if (!this.bCancel)
        return;
      e.Cancel = true;
      this.bCancel = false;
    }

    private void webBrowserISI_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      for (int index = 0; index < this.webBrowserISI.Document.Links.Count; ++index)
        this.webBrowserISI.Document.Links[index].Click += new HtmlElementEventHandler(this.LinkClick);
    }

    private void webBrowserISI_Navigating(object sender, WebBrowserNavigatingEventArgs e)
    {
      if (!this.bCancel)
        return;
      e.Cancel = true;
      this.bCancel = false;
    }

    private void copyElementNameToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Clipboard.SetText(this.forRightClickSelected.Text);
    }

    private void copySelectedPathToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Clipboard.SetText(this.forRightClickSelected.FullPath.Replace("Root.", ""));
    }

    private void copyNamespaceToolStripMenuItem_Click(object sender, EventArgs e)
    {
      string boProxyName = "";
      string str = "";
      if (this.forRightClickSelected.Tag == null)
        return;
      if (this.forRightClickSelected.Tag.GetType() == typeof (bostruct))
        boProxyName = ((bostruct) this.forRightClickSelected.Tag).proxynamebo;
      else if (this.forRightClickSelected.Tag.GetType() == typeof (BusinessObject_Action))
        boProxyName = ((BusinessObject_Action) this.forRightClickSelected.Tag).Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName;
      else if (this.forRightClickSelected.Tag.GetType() == typeof (BusinessObject_ActionParameter))
        boProxyName = ((BusinessObject_ActionParameter) this.forRightClickSelected.Tag).Key.BusinessObjectNodeActionKey.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName;
      else if (this.forRightClickSelected.Tag.GetType() == typeof (BusinessObject_Association))
        boProxyName = ((BusinessObject_Association) this.forRightClickSelected.Tag).Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName;
      else if (this.forRightClickSelected.Tag.GetType() == typeof (BusinessObject_Node))
        boProxyName = ((BusinessObject_Node) this.forRightClickSelected.Tag).Key.BusinessObjectProxyKey.ProxyName;
      else if (this.forRightClickSelected.Tag.GetType() == typeof (BusinessObject_NodeElement))
        boProxyName = ((BusinessObject_NodeElement) this.forRightClickSelected.Tag).Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName;
      else if (this.forRightClickSelected.Tag.GetType() == typeof (BusinessObject_Query))
        boProxyName = ((BusinessObject_Query) this.forRightClickSelected.Tag).Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName;
      else if (this.forRightClickSelected.Tag.GetType() == typeof (BusinessObject_QueryParameter))
        boProxyName = ((BusinessObject_QueryParameter) this.forRightClickSelected.Tag).Key.BusinessObjectQueryKey.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName;
      else if (this.forRightClickSelected.Tag.GetType() == typeof (dtstruct))
        str = ((dtstruct) this.forRightClickSelected.Tag).dtnamespace;
      else if (this.forRightClickSelected.Tag.GetType() == typeof (eostruct))
        str = ((eostruct) this.forRightClickSelected.Tag).eonamespace;
      else if (this.forRightClickSelected.Tag.GetType() == typeof (isistruct))
        str = ((isistruct) this.forRightClickSelected.Tag).isinamespace;
      if (str == "")
        str = RepositoryDataCache.GetInstance().GetNamespaceForBOProxyName(boProxyName);
      if (str == null || !(str != ""))
        return;
      Clipboard.SetText(str.Replace("http://sap.com/xi/", "").Replace("/", "."));
    }

    private void treeViewBO_MouseUp(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Right)
        return;
      try
      {
        this.forRightClickSelected = this.treeViewBO.GetNodeAt(e.X, e.Y);
      }
      catch
      {
        this.forRightClickSelected = (TreeNode) null;
      }
    }

    private void treeViewDT_MouseUp(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Right)
        return;
      try
      {
        this.forRightClickSelected = this.treeViewDT.GetNodeAt(e.X, e.Y);
        this.forRightClickSelected.ContextMenuStrip.Show();
      }
      catch
      {
        this.forRightClickSelected = (TreeNode) null;
      }
    }

    private void treeViewEO_MouseUp(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Right)
        return;
      try
      {
        this.forRightClickSelected = this.treeViewEO.GetNodeAt(e.X, e.Y);
        this.forRightClickSelected.ContextMenuStrip.Show();
      }
      catch
      {
        this.forRightClickSelected = (TreeNode) null;
      }
    }

    private void treeViewISI_MouseUp(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Right)
        return;
      try
      {
        this.forRightClickSelected = this.treeViewISI.GetNodeAt(e.X, e.Y);
        this.forRightClickSelected.ContextMenuStrip.Show();
      }
      catch
      {
        this.forRightClickSelected = (TreeNode) null;
      }
    }

    private void treeViewDT_AfterSelect(object sender, TreeViewEventArgs e)
    {
      if (this.lastSelected != null && this.lastSelected == e.Node)
        return;
      this.Cursor = Cursors.WaitCursor;
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      service.SetText("Loading documentation...");
      this.lastSelected = e.Node;
      if (!this.isBackClickedDT && !this.isNextClickedDT)
      {
        ++CopernicusBusinessObjectBrowser.openCountDT;
        TreeNode[] treeNodeArray = new TreeNode[CopernicusBusinessObjectBrowser.openCountDT];
        Array.Copy((Array) this.openSelectedDT, (Array) treeNodeArray, CopernicusBusinessObjectBrowser.openCountDT - 1);
        this.lastOpened = e.Node;
        treeNodeArray[CopernicusBusinessObjectBrowser.openCountDT - 1] = this.lastOpened;
        this.openSelectedDT = (TreeNode[]) treeNodeArray.Clone();
        if (CopernicusBusinessObjectBrowser.openCountDT > 1)
          this.toolStripButtonBackDT.Enabled = true;
        if (this.closeSelectedDT.Length != 0)
        {
          bool flag = false;
          for (int index = 0; index < this.closeSelectedDT.Length; ++index)
          {
            if (this.closeSelectedDT[index].Tag == e.Node.Tag)
            {
              flag = true;
              break;
            }
          }
          if (!flag)
          {
            CopernicusBusinessObjectBrowser.closedCountDT = 0;
            this.closeSelectedDT = new TreeNode[0];
            this.toolStripButtonNextDT.Enabled = false;
          }
        }
      }
      this.webBrowserDT.DocumentText = this.getDocuForDataType(((dtstruct) e.Node.Tag).dtproxyname);
      this.Cursor = Cursors.Default;
      service.SetText("Ready");
    }

    private string getDocuForDataType(string dtProxyName)
    {
      RepositoryQueryHandler repositoryQueryHandler = new RepositoryQueryHandler();
      string str;
      try
      {
        str = repositoryQueryHandler.Get_Docu_Service((string) null, (string) null, (string) null, (string) null, (string) null, (string) null, dtProxyName, (string) null, (string) null, (string) null, (string) null);
      }
      catch
      {
        str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the selected entity.</p></body></html>";
      }
      if (str == "" || str == null)
        str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the selected entity.</p></body></html>";
      return str;
    }

    private void treeViewEO_AfterSelect(object sender, TreeViewEventArgs e)
    {
      if (this.lastSelected != null && this.lastSelected == e.Node)
        return;
      this.Cursor = Cursors.WaitCursor;
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      service.SetText("Loading documentation...");
      this.lastSelected = e.Node;
      if (!this.isBackClickedEO && !this.isNextClickedEO)
      {
        ++CopernicusBusinessObjectBrowser.openCountEO;
        TreeNode[] treeNodeArray = new TreeNode[CopernicusBusinessObjectBrowser.openCountEO];
        Array.Copy((Array) this.openSelectedEO, (Array) treeNodeArray, CopernicusBusinessObjectBrowser.openCountEO - 1);
        this.lastOpened = e.Node;
        treeNodeArray[CopernicusBusinessObjectBrowser.openCountEO - 1] = this.lastOpened;
        this.openSelectedEO = (TreeNode[]) treeNodeArray.Clone();
        if (CopernicusBusinessObjectBrowser.openCountEO > 1)
          this.toolStripButtonBackEO.Enabled = true;
        if (this.closeSelectedEO.Length != 0)
        {
          bool flag = false;
          for (int index = 0; index < this.closeSelectedEO.Length; ++index)
          {
            if (this.closeSelectedEO[index].Tag == e.Node.Tag)
            {
              flag = true;
              break;
            }
          }
          if (!flag)
          {
            CopernicusBusinessObjectBrowser.closedCountEO = 0;
            this.closeSelectedEO = new TreeNode[0];
            this.toolStripButtonNextEO.Enabled = false;
          }
        }
      }
      RepositoryQueryHandler repositoryQueryHandler = new RepositoryQueryHandler();
      string str;
      try
      {
        eostruct tag = (eostruct) e.Node.Tag;
        str = repositoryQueryHandler.Get_Docu_Service((string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, tag.eoproxyname, (string) null, (string) null, (string) null);
      }
      catch
      {
        str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the selected entity.</p></body></html>";
      }
      if (str == "" || str == null)
        str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the selected entity.</p></body></html>";
      this.webBrowserEO.DocumentText = str;
      this.Cursor = Cursors.Default;
      service.SetText("Ready");
    }

    private void treeViewISI_AfterSelect(object sender, TreeViewEventArgs e)
    {
      if (this.lastSelected != null && this.lastSelected == e.Node)
        return;
      this.Cursor = Cursors.WaitCursor;
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      service.SetText("Loading documentation...");
      this.lastSelected = e.Node;
      RepositoryQueryHandler repositoryQueryHandler = new RepositoryQueryHandler();
      string str;
      try
      {
        isistruct tag = (isistruct) e.Node.Tag;
        str = repositoryQueryHandler.Get_Docu_Service((string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, tag.isiproxyname, (string) null, (string) null);
      }
      catch
      {
        str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the selected entity.</p></body></html>";
      }
      if (str == "" || str == null)
        str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the selected entity.</p></body></html>";
      this.webBrowserISI.DocumentText = str;
      this.Cursor = Cursors.Default;
      service.SetText("Ready");
    }

    private void treeViewISI_AfterExpand(object sender, TreeViewEventArgs e)
    {
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      this.Cursor = Cursors.WaitCursor;
      service.SetText("Loading content...");
      try
      {
        if (this.isNextClickedISI || this.isBackClickedISI)
        {
          if (e.Node.FirstNode.Tag != (object) "dummy")
            goto label_22;
        }
        if (!this.isNextClickedISI && !this.isBackClickedISI)
        {
          ++CopernicusBusinessObjectBrowser.openCountISI;
          TreeNode[] treeNodeArray = new TreeNode[CopernicusBusinessObjectBrowser.openCountISI];
          Array.Copy((Array) this.openedISI, (Array) treeNodeArray, CopernicusBusinessObjectBrowser.openCountISI - 1);
          this.lastOpenedISI = e.Node;
          treeNodeArray[CopernicusBusinessObjectBrowser.openCountISI - 1] = this.lastOpenedISI;
          this.openedISI = (TreeNode[]) treeNodeArray.Clone();
          this.toolStripButtonBackISI.Enabled = true;
          if (this.closedISI.Length != 0)
          {
            bool flag = false;
            for (int index = 0; index < this.closedISI.Length; ++index)
            {
              if (this.closedISI[index].Tag == e.Node.Tag)
              {
                flag = true;
                break;
              }
            }
            if (!flag)
            {
              CopernicusBusinessObjectBrowser.closedCountISI = 0;
              this.closedISI = new TreeNode[0];
              this.toolStripButtonNextISI.Enabled = false;
            }
          }
        }
        TreeNode treeNode1 = new TreeNode();
        isistruct tag = (isistruct) e.Node.Tag;
        TreeNode[] treeNodeArray1 = new TreeNode[tag.isioperations.Length];
        for (int index = 0; index < e.Node.Nodes.Count; ++index)
        {
          if (e.Node.Nodes[index].Text == "An error occured")
            e.Node.Nodes[index].Remove();
        }
        for (int index = 0; index < tag.isioperations.Length; ++index)
        {
          operationstruct operationstruct = new operationstruct();
          operationstruct.operationname = tag.isioperations[index].OPERATION;
          TreeNode treeNode2 = new TreeNode(operationstruct.operationname);
          treeNode2.Tag = (object) operationstruct;
          treeNode2.ImageIndex = 43;
          treeNode2.SelectedImageIndex = treeNode2.ImageIndex;
          treeNode2.ToolTipText = "Name: " + operationstruct.operationname;
          treeNode2.ContextMenuStrip = this.contextMenuStripOP;
          treeNodeArray1[index] = treeNode2;
        }
        Array.Sort<TreeNode>(treeNodeArray1, (Comparison<TreeNode>) ((op1, op2) => op1.Text.CompareTo(op2.Text)));
        e.Node.Nodes.AddRange(treeNodeArray1);
        e.Node.EnsureVisible();
      }
      catch
      {
        this.richTextBoxInfoBoxBO.Text = "An error occurred while loading the repository content. Try again. If the error occurs again, report an incident.";
        this.richTextBoxInfoBoxBO.Visible = true;
      }
label_22:
      this.Cursor = Cursors.Default;
      service.SetText("Ready");
    }

    private void treeViewISI_AfterCollapse(object sender, TreeViewEventArgs e)
    {
      if (this.isBackClickedISI || this.isNextClickedISI)
        return;
      e.Node.Nodes.Clear();
      e.Node.Nodes.Add(new TreeNode("An error occured")
      {
        Tag = (object) "dummy"
      });
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);
      if (this.Enabled || !RepositoryViewControl.GetInstance().isLoggedOnGet())
        return;
      this.Init2();
    }

    private void tabSelected(object sender, TabControlEventArgs e)
    {
      if (e.TabPageIndex == 2)
        this.SearchAndInitEO();
      if (e.TabPageIndex == 3)
        this.SearchAndInitISI();
      if (e.TabPageIndex != 4)
        return;
      this.SearchAndInitReuseUIs();
    }

    private void SearchAndInitReuseUIs()
    {
      ContextMenu contextMenu = new ContextMenu();
      MenuItem menuItem = new MenuItem("Open in UI Designer");
      menuItem.Click += new EventHandler(this.openInUIDesigner_Click);
      contextMenu.MenuItems.Add(menuItem);
      this.richTextBoxReuseUIs.Visible = false;
      this.richTextBoxReuseUIs.Text = "";
      this.webBrowserReuseUIs.DocumentText = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>To view the documentation for an entity, click the entity in the tree view. You can search for an entity by typing in the search box above.You can show the documentation pane at the bottom by clicking Horizontal or on the right by clicking Vertical.</p></body></html>";
      if (this.listReuseUIs.Count == 0)
        this.listReuseUIs.AddRange((IEnumerable<PDI_REUSE_UI>) ((IEnumerable<PDI_REUSE_UI>) new RepositoryQueryHandler().get_Reuse_UIs()).OrderBy<PDI_REUSE_UI, string>((Func<PDI_REUSE_UI, string>) (item => item.TYPE)).ThenBy<PDI_REUSE_UI, string>((Func<PDI_REUSE_UI, string>) (item => ((IEnumerable<string>) item.XREP_FILE_PATH.Split('/')).Last<string>())).ToArray<PDI_REUSE_UI>());
      if (this.treeViewReuseUIs.Nodes.Count != 0)
        return;
      TreeNode treeNode1 = new TreeNode();
      this.treeViewReuseUIs.ImageList = this.imageList1;
      TreeNode treeNode2 = (TreeNode) null;
      ruistruct ruistruct = new ruistruct();
      foreach (PDI_REUSE_UI listReuseUi in this.listReuseUIs)
      {
        if (treeNode2 == null || treeNode2.Text != listReuseUi.TYPE)
        {
          TreeNode node = new TreeNode(listReuseUi.TYPE);
          this.treeViewReuseUIs.Nodes.Add(node);
          int nameFromImageList1 = this.getImagebyNameFromImageList1(listReuseUi.TYPE);
          node.ImageIndex = nameFromImageList1;
          node.SelectedImageIndex = nameFromImageList1;
          treeNode2 = node;
        }
        string text = ((IEnumerable<string>) ((IEnumerable<string>) listReuseUi.XREP_FILE_PATH.Split('/')).Last<string>().Split('.')).First<string>();
        ruistruct.filepath = listReuseUi.XREP_FILE_PATH;
        ruistruct.uiname = text;
        ruistruct.kt_document = listReuseUi.KT_DOC_NAME;
        this.toolStripComboBoxSearchReuseUI.AutoCompleteCustomSource.Add(text);
        TreeNode node1 = new TreeNode(text);
        int nameFromImageList1_1 = this.getImagebyNameFromImageList1(treeNode2.Text);
        node1.ImageIndex = nameFromImageList1_1;
        node1.SelectedImageIndex = nameFromImageList1_1;
        node1.ContextMenu = contextMenu;
        node1.Tag = (object) ruistruct;
        treeNode2.Nodes.Add(node1);
      }
      this.treeViewReuseUIs.NodeMouseClick += new TreeNodeMouseClickEventHandler(this.treeViewReuseUIs_NodeMouseClick);
    }

    private void openInUIDesigner_Click(object sender, EventArgs e)
    {
      if (this.reuseUIForRightClickSelected == null)
        return;
      ruistruct ruistruct = new ruistruct();
      string filePath = ((ruistruct) this.reuseUIForRightClickSelected.Tag).filepath;
      UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(this.listReuseUIs.Find((Predicate<PDI_REUSE_UI>) (item => item.XREP_FILE_PATH == filePath)).XREP_FILE_PATH, true);
    }

    private int getImagebyNameFromImageList1(string key)
    {
      key = key.Replace(" ", "") + ".png";
      switch (key)
      {
        case "ObjectValueSelector.png":
          key = "OVS.png";
          break;
        case "QuickActivityFloorplan.png":
          key = "QAF.png";
          break;
        case "ObjectInstanceeFloorplan.png":
          key = "QIF.png";
          break;
        case "ObjectWorkList.png":
          key = "OWL.png";
          break;
      }
      int num = this.imageList1.Images.IndexOfKey(key);
      if (num == -1)
        num = this.imageList1.Images.IndexOfKey("Floorplan.png");
      return num;
    }

    private void toolStripButtonSearchReuseUI_Click(object sender, EventArgs e)
    {
      this.searchReuseUI();
    }

    private void searchReuseUI()
    {
      string text = this.toolStripComboBoxSearchReuseUI.Text;
      TreeNode treeNode = (TreeNode) null;
      foreach (TreeNode node in this.treeViewReuseUIs.Nodes)
      {
        treeNode = this.findNodeInChilds(text, node);
        if (treeNode != null)
          break;
      }
      this.lastFoundReuseTreeNode = treeNode;
      if (treeNode != null)
      {
        this.treeViewReuseUIs.SelectedNode = treeNode;
        this.treeViewReuseUIs.Focus();
      }
      else
      {
        int num = (int) MessageBox.Show("No matching UIs found!");
      }
    }

    private TreeNode findNodeInChilds(string searchText, TreeNode parentNode)
    {
      TreeNode treeNode = (TreeNode) null;
      foreach (TreeNode node in parentNode.Nodes)
      {
        if (node.Equals((object) this.lastFoundReuseTreeNode) || this.lastFoundReuseTreeNode == null)
          this.hasFoundLastReuseTreeNode = true;
        if (node.Text.Contains(searchText) || node.Text.Contains(searchText.ToUpper()) && !node.Equals((object) this.lastFoundReuseTreeNode) && this.hasFoundLastReuseTreeNode)
          treeNode = node;
      }
      return treeNode;
    }

    private void treeViewReuseUIs_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
    {
      if (e.Node.Parent == null || this.lastSelected != null && this.lastSelected == e.Node)
        return;
      this.Cursor = Cursors.WaitCursor;
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      service.SetText("Loading documentation...");
      this.lastSelected = e.Node;
      RepositoryQueryHandler repositoryQueryHandler = new RepositoryQueryHandler();
      ruistruct tag = (ruistruct) e.Node.Tag;
      string str;
      if (tag.kt_document.Length == 0)
      {
        str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the Reuse UI of type " + e.Node.Parent.Text + "<br>Repository Path: " + tag.filepath + "</p></body></html>";
      }
      else
      {
        try
        {
          string docuService = repositoryQueryHandler.Get_Docu_Service((string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, tag.kt_document);
          str = "<html><body><p style=font-family: Arial, helvetica, sans-serif;><h3>" + e.Node.Parent.Text + ": " + e.Node.Text + "</h3><br><b>Repository Path</b>: " + tag.filepath + "</p></body></html>" + docuService;
        }
        catch
        {
          str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the Reuse UI of type " + e.Node.Parent.Text + "<br>Repository Path: " + tag.filepath + "</p></body></html>";
        }
        if (str == "" || str == null)
          str = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>No documentation available for the Reuse UI of type " + e.Node.Parent.Text + "<br>Repository Path: " + tag.filepath + "</p></body></html>";
      }
      this.webBrowserReuseUIs.DocumentText = str;
      this.Cursor = Cursors.Default;
      service.SetText("Ready");
    }

    private void executeQueryToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.Cursor = Cursors.WaitCursor;
      IVsStatusbar service = (IVsStatusbar) this.GetService(typeof (SVsStatusbar));
      service.SetText("Loading Query Result...");
      foreach (TabPage tabPage in this.tabControlBO.TabPages)
      {
        if (tabPage.Text.Equals("Query Result"))
          this.tabControlBO.TabPages.Remove(tabPage);
      }
      if (Connection.getInstance().SystemMode == SystemMode.Productive)
        return;
      BusinessObject_Query tag = (BusinessObject_Query) this.forRightClickSelected.Tag;
      QuerySelection1 querySelectionScreen = new QuerySelection1(tag);
      QueryResultView queryResultView = new QueryResultView(querySelectionScreen.ShowDialog(), tag, querySelectionScreen);
      queryResultView.Dock = DockStyle.Fill;
      TabPage tabPage1 = new TabPage("Query Result");
      tabPage1.Controls.Add((Control) queryResultView);
      this.tabControlBO.TabPages.Add(tabPage1);
      this.tabControlBO.SelectedIndex = 1;
      tabPage1.Show();
      this.Cursor = Cursors.Arrow;
      service.SetText("");
    }

    private void toolStripComboBoxSearchReuseUI_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode != Keys.Return)
        return;
      this.searchReuseUI();
    }

    private void treeViewReuseUIs_MouseUp(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Right)
        return;
      try
      {
        this.reuseUIForRightClickSelected = this.treeViewReuseUIs.GetNodeAt(e.X, e.Y);
      }
      catch
      {
        this.reuseUIForRightClickSelected = (TreeNode) null;
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private string GetUpperRegexStringForName(string name)
    {
      return "^" + name.Replace("(", "\\(").Replace(")", "\\)").Replace("^", "\\^").Replace("?", "\\?").Replace("+", "\\+").Replace("$", "\\$").Replace("|", "\\|").Replace(".", "\\.").ToUpper().Replace("*", ".*");
    }

    private void InitializeBOTreeView(string boName, string boNamespace, string boDeploymentUnit)
    {
      this.richTextBoxInfoBoxBO.Visible = false;
      this.richTextBoxInfoBoxBO.Text = "";
      this.webBrowserBO.DocumentText = "<html><body><p style=font-family:Arial;font-size:13px;font-style:normal;font-weight:normal;color:#000000;>To view the documentation for an entity, click the entity in the tree view. You can search for an entity by typing in the search box above.You can show the documentation pane at the bottom by clicking Horizontal or on the right by clicking Vertical.</p></body></html>";
      this.treeViewBO.Nodes.Clear();
      TreeNode treeNode1 = new TreeNode();
      TreeNode treeNode2 = new TreeNode();
      string deploymentUnit = (string) null;
      if (boDeploymentUnit != null)
        deploymentUnit = DeploymentUnit.DUProxyNameForName(boDeploymentUnit);
      RepositoryDataSet.BusinessObjectsRow[] array = RepositoryDataCache.GetInstance().QueryBOs((string) null, (string) null, (string) null, (string) null, (string) null, deploymentUnit, (string) null);
      Array.Sort<RepositoryDataSet.BusinessObjectsRow>(array, (Comparison<RepositoryDataSet.BusinessObjectsRow>) ((borow1, borow2) => borow1.Name.CompareTo(borow2.Name)));
      string str1;
      try
      {
        str1 = CopernicusProjectSystemUtil.GetNamespaceForSelectedProject();
      }
      catch
      {
        str1 = (string) null;
      }
      this.treeViewBO.BeginUpdate();
      foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in array)
      {
        if ((str1 == null || !(businessObjectsRow.NSName == str1)) && (boName == null || Regex.IsMatch(businessObjectsRow.Name.ToUpper(), this.GetUpperRegexStringForName(boName))) && (boNamespace == null || businessObjectsRow.NSName.Equals(boNamespace, StringComparison.OrdinalIgnoreCase)))
        {
          bostruct bostruct = new bostruct();
          bostruct.boname = businessObjectsRow.Name;
          bostruct.namespacebo = businessObjectsRow.NSName;
          bostruct.proxynamebo = businessObjectsRow.ProxyName;
          if (businessObjectsRow.Deprecated)
          {
            bostruct.deprecated = true;
            if (!this.toolStripButtonDeprecation.Checked)
              continue;
          }
          TreeNode node1 = new TreeNode(bostruct.boname);
          this.treeViewBO.Nodes.Add(node1);
          if (businessObjectsRow.ObjectCategory == "5")
          {
            node1.ImageIndex = 9;
            node1.SelectedImageIndex = 9;
          }
          else if (businessObjectsRow.Deprecated)
          {
            node1.ImageIndex = 21;
            node1.SelectedImageIndex = 21;
          }
          else
          {
            node1.ImageIndex = 0;
            node1.SelectedImageIndex = 0;
          }
          node1.Tag = (object) bostruct;
          string str2 = "";
          string str3 = "";
          string str4 = "";
          string str5 = "";
          if (businessObjectsRow.ObjectCategory != null)
          {
            switch (businessObjectsRow.ObjectCategory)
            {
              case "1":
                str2 = "Business Transaction Document";
                break;
              case "2":
                str2 = "Master Data Object";
                break;
              case "3":
                str2 = "Mass Data Run Object";
                break;
              case "4":
                str2 = "Technical Object";
                break;
              case "5":
                str2 = "Business Configuration Object";
                break;
              case "6":
                str2 = "Meta Object";
                break;
              case "7":
                str2 = "Analytics Enablement Object";
                break;
              case "8":
                str2 = "Key Figure Based Plan Object";
                break;
              case "9":
                str2 = "Business Administration Object";
                break;
              case "10":
                str2 = "UI related Object";
                break;
              case "11":
                str2 = "Suite HANA Business Object";
                break;
            }
          }
          if (businessObjectsRow.NSName != null)
            str3 = businessObjectsRow.NSName;
          if (businessObjectsRow.DeploymentUnit != null)
            str4 = businessObjectsRow.DeploymentUnit;
          if (businessObjectsRow.WriteAccess != null)
            str5 = !(businessObjectsRow.WriteAccess == "true") ? "No" : "Yes";
          int num = businessObjectsRow.Deprecated ? 1 : 0;
          string str6 = businessObjectsRow.Deprecated ? "Yes" : "No";
          string str7 = "Element Type: Business Object";
          node1.ToolTipText = "Object Category: " + str2 + "\nNamespace: " + str3 + "\nDeployment Unit: " + str4 + "\nPublic Write Access: " + str5 + "\nDeprecated: " + str6 + "\n" + str7;
          node1.ContextMenuStrip = this.contextMenuStripFirstLevel;
          TreeNode node2 = new TreeNode("An error occured");
          node1.Nodes.Add(node2);
        }
      }
      this.treeViewBO.EndUpdate();
      if (this.treeViewBO.Nodes.Count != 0)
        return;
      this.richTextBoxInfoBoxBO.Text = "The system could not find any business objects that match your search term. Try the following:\n    o Click the Clear All button to search in all namespaces and deployment units.\n    o Enter a different spelling or search term.";
      this.richTextBoxInfoBoxBO.Visible = true;
    }

    private void InitializeNamespacesComboBox()
    {
      this.toolStripComboBoxNamespace.Items.Clear();
      this.toolStripComboBoxNamespaceDT.Items.Clear();
      this.toolStripComboBoxNamespaceEO.Items.Clear();
      this.toolStripComboBoxNamespaceISI.Items.Clear();
      List<string> namespaces = RepositoryDataCache.GetInstance().GetNamespaces();
      this.toolStripComboBoxNamespace.Items.AddRange((object[]) namespaces.ToArray());
      this.toolStripComboBoxNamespaceDT.Items.AddRange((object[]) namespaces.ToArray());
      this.toolStripComboBoxNamespaceEO.Items.AddRange((object[]) namespaces.ToArray());
      this.toolStripComboBoxNamespaceISI.Items.AddRange((object[]) namespaces.ToArray());
    }

    private void InitializeBONames()
    {
      List<string> boNames = RepositoryDataCache.GetInstance().GetBONames();
      this.toolStripComboBoxSearch.AutoCompleteCustomSource.Clear();
      this.toolStripComboBoxSearch.AutoCompleteCustomSource.AddRange(boNames.ToArray<string>());
    }

    private void InitializeDTNames()
    {
      List<string> allDataTypes = RepositoryDataCache.GetInstance().GetAllDataTypes();
      this.toolStripComboBoxSearchDT.AutoCompleteCustomSource.Clear();
      this.toolStripComboBoxSearchDT.AutoCompleteCustomSource.AddRange(allDataTypes.ToArray<string>());
    }

    private void InitializeEONames()
    {
      RepositoryDataSet repositoryDataSet = RepositoryDataCache.GetInstance().RepositoryDataSet;
      IEnumerable<DataRow> badInSandName = RepositoryDataCache.GetInstance().GetBAdINSandName((string) null, (string) null);
      this.toolStripComboBoxSearchEO.AutoCompleteCustomSource.Clear();
      foreach (RepositoryDataSet.BAdIsRow badIsRow in badInSandName)
        this.toolStripComboBoxSearchEO.AutoCompleteCustomSource.Add(badIsRow.Name);
    }

    private void initializeComboBoxes()
    {
      this.InitializeBONames();
      this.InitializeDTNames();
      this.InitializeEONames();
      this.InitializeNamespacesComboBox();
      this.toolStripComboBoxDepUnit.Items.Clear();
      foreach (DeploymentUnit deploymentUnit in DeploymentUnit.Values)
        this.toolStripComboBoxDepUnit.Items.Add((object) deploymentUnit.GetDeploymentUnitName());
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (CopernicusBusinessObjectBrowser));
      this.imageList1 = new ImageList(this.components);
      this.miniToolStrip = new ToolStrip();
      this.contextMenuStripSUBelement = new ContextMenuStrip(this.components);
      this.copyElementNameToolStripMenuItem1 = new ToolStripMenuItem();
      this.copySelectedPathToolStripMenuItem = new ToolStripMenuItem();
      this.copyImportNamespaceToolStripMenuItem1 = new ToolStripMenuItem();
      this.contextMenuStripFirstLevel = new ContextMenuStrip(this.components);
      this.copyElementNameToolStripMenuItem2 = new ToolStripMenuItem();
      this.copyNamespaceToolStripMenuItem = new ToolStripMenuItem();
      this.contextMenuStripOP = new ContextMenuStrip(this.components);
      this.copyEntityNameToolStripMenuItem = new ToolStripMenuItem();
      this.contextMenuStripQuery = new ContextMenuStrip(this.components);
      this.executeQueryToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripMenuItem1 = new ToolStripMenuItem();
      this.toolStripMenuItem2 = new ToolStripMenuItem();
      this.toolStripMenuItem3 = new ToolStripMenuItem();
      this.tabReuseUIs = new TabPage();
      this.splitContainer5 = new SplitContainer();
      this.richTextBoxReuseUIs = new RichTextBox();
      this.treeViewReuseUIs = new TreeView();
      this.tabControl2 = new TabControl();
      this.tabPage1 = new TabPage();
      this.webBrowserReuseUIs = new WebBrowser();
      this.webBrowserEC = new WebBrowser();
      this.toolStrip1 = new ToolStrip();
      this.toolStripButton1 = new ToolStripButton();
      this.toolStripButton2 = new ToolStripButton();
      this.toolStripSeparator9 = new ToolStripSeparator();
      this.toolStripSeparator10 = new ToolStripSeparator();
      this.toolStripComboBoxSearchReuseUI = new ToolStripComboBox();
      this.toolStripButtonSearchReuseUI = new ToolStripButton();
      this.tabPageInboundServiceInterfaces = new TabPage();
      this.splitContainer4 = new SplitContainer();
      this.richTextBoxInfoBoxISI = new RichTextBox();
      this.treeViewISI = new TreeView();
      this.webBrowserISI = new WebBrowser();
      this.toolStrip5ISI = new ToolStrip();
      this.toolStripButtonBackISI = new ToolStripButton();
      this.toolStripButtonNextISI = new ToolStripButton();
      this.toolStripSeparator7 = new ToolStripSeparator();
      this.toolStripButtonHorizontalISI = new ToolStripButton();
      this.toolStripButtonVerticalISI = new ToolStripButton();
      this.toolStripSeparator8 = new ToolStripSeparator();
      this.toolStripComboBoxSearchISI = new ToolStripComboBox();
      this.toolStripButtonSearchISI = new ToolStripButton();
      this.toolStripLabelFilterISI = new ToolStripLabel();
      this.toolStripComboBoxNamespaceISI = new ToolStripComboBox();
      this.toolStripButtonClearISI = new ToolStripButton();
      this.toolStripButtonHelpISI = new ToolStripButton();
      this.tabPageEnhancementOptions = new TabPage();
      this.splitContainer3 = new SplitContainer();
      this.richTextBoxInfoBoxEO = new RichTextBox();
      this.treeViewEO = new TreeView();
      this.webBrowserEO = new WebBrowser();
      this.toolStrip4EO = new ToolStrip();
      this.toolStripButtonBackEO = new ToolStripButton();
      this.toolStripButtonNextEO = new ToolStripButton();
      this.toolStripSeparator5 = new ToolStripSeparator();
      this.toolStripButtonHorizontalWindowsEO = new ToolStripButton();
      this.toolStripButtonVerticalWindowsEO = new ToolStripButton();
      this.toolStripSeparator6 = new ToolStripSeparator();
      this.toolStripComboBoxSearchEO = new ToolStripComboBox();
      this.toolStripButtonSearchEO = new ToolStripButton();
      this.toolStripLabelFilterEO = new ToolStripLabel();
      this.toolStripComboBoxNamespaceEO = new ToolStripComboBox();
      this.toolStripButtonClearEO = new ToolStripButton();
      this.toolStripButtonHelpEO = new ToolStripButton();
      this.tabPageDataTypes = new TabPage();
      this.splitContainer2 = new SplitContainer();
      this.richTextBoxInfoBoxDT = new RichTextBox();
      this.treeViewDT = new TreeView();
      this.webBrowserDT = new WebBrowser();
      this.toolStrip3DT = new ToolStrip();
      this.toolStripButtonBackDT = new ToolStripButton();
      this.toolStripButtonNextDT = new ToolStripButton();
      this.toolStripSeparator3 = new ToolStripSeparator();
      this.toolStripButtonHorizontalWindowsDT = new ToolStripButton();
      this.toolStripButtonVerticalWindowsDT = new ToolStripButton();
      this.toolStripSeparator4 = new ToolStripSeparator();
      this.toolStripComboBoxSearchDT = new ToolStripComboBox();
      this.toolStripButtonSearchDT = new ToolStripButton();
      this.toolStripLabelFilterDT = new ToolStripLabel();
      this.toolStripComboBoxNamespaceDT = new ToolStripComboBox();
      this.toolStripComboBoxUsageCat = new ToolStripComboBox();
      this.toolStripButtonClearDT = new ToolStripButton();
      this.toolStripButton1HelpDT = new ToolStripButton();
      this.tabPageBusinessObjectBrowser = new TabPage();
      this.splitContainer1 = new SplitContainer();
      this.richTextBoxInfoBoxBO = new RichTextBox();
      this.treeViewBO = new TreeView();
      this.tabControlBO = new TabControl();
      this.tabPageDescription = new TabPage();
      this.webBrowserBO = new WebBrowser();
      this.toolStrip2BO = new ToolStrip();
      this.toolStripComboBoxSearch = new ToolStripComboBox();
      this.toolStripButtonSearch = new ToolStripButton();
      this.toolStripLabelFilterBO = new ToolStripLabel();
      this.toolStripComboBoxNamespace = new ToolStripComboBox();
      this.toolStripComboBoxDepUnit = new ToolStripComboBox();
      this.toolStripButtonClear = new ToolStripButton();
      this.toolStripButtonHelpBO = new ToolStripButton();
      this.toolStrip1BO = new ToolStrip();
      this.toolStripButtonBack = new ToolStripButton();
      this.toolStripButtonNext = new ToolStripButton();
      this.toolStripSeparator2 = new ToolStripSeparator();
      this.toolStripButtonActions = new ToolStripButton();
      this.toolStripButtonAssociations = new ToolStripButton();
      this.toolStripButtonDeprecation = new ToolStripButton();
      this.toolStripButtonNodes = new ToolStripButton();
      this.toolStripButtonQueries = new ToolStripButton();
      this.toolStripSeparator1 = new ToolStripSeparator();
      this.toolStripButtonHorizontalWindows = new ToolStripButton();
      this.toolStripButtonVerticalWindows = new ToolStripButton();
      this.tabControl1 = new TabControl();
      this.contextMenuStripSUBelement.SuspendLayout();
      this.contextMenuStripFirstLevel.SuspendLayout();
      this.contextMenuStripOP.SuspendLayout();
      this.contextMenuStripQuery.SuspendLayout();
      this.tabReuseUIs.SuspendLayout();
      this.splitContainer5.BeginInit();
      this.splitContainer5.Panel1.SuspendLayout();
      this.splitContainer5.Panel2.SuspendLayout();
      this.splitContainer5.SuspendLayout();
      this.tabControl2.SuspendLayout();
      this.tabPage1.SuspendLayout();
      this.toolStrip1.SuspendLayout();
      this.tabPageInboundServiceInterfaces.SuspendLayout();
      this.splitContainer4.BeginInit();
      this.splitContainer4.Panel1.SuspendLayout();
      this.splitContainer4.Panel2.SuspendLayout();
      this.splitContainer4.SuspendLayout();
      this.toolStrip5ISI.SuspendLayout();
      this.tabPageEnhancementOptions.SuspendLayout();
      this.splitContainer3.BeginInit();
      this.splitContainer3.Panel1.SuspendLayout();
      this.splitContainer3.Panel2.SuspendLayout();
      this.splitContainer3.SuspendLayout();
      this.toolStrip4EO.SuspendLayout();
      this.tabPageDataTypes.SuspendLayout();
      this.splitContainer2.BeginInit();
      this.splitContainer2.Panel1.SuspendLayout();
      this.splitContainer2.Panel2.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      this.toolStrip3DT.SuspendLayout();
      this.tabPageBusinessObjectBrowser.SuspendLayout();
      this.splitContainer1.BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.tabControlBO.SuspendLayout();
      this.tabPageDescription.SuspendLayout();
      this.toolStrip2BO.SuspendLayout();
      this.toolStrip1BO.SuspendLayout();
      this.tabControl1.SuspendLayout();
      this.SuspendLayout();
      this.imageList1.ImageStream = (ImageListStreamer) componentResourceManager.GetObject("imageList1.ImageStream");
      this.imageList1.TransparentColor = Color.Transparent;
      this.imageList1.Images.SetKeyName(0, "BusinessObject.png");
      this.imageList1.Images.SetKeyName(1, "Action.png");
      this.imageList1.Images.SetKeyName(2, "Method.png");
      this.imageList1.Images.SetKeyName(3, "Element.png");
      this.imageList1.Images.SetKeyName(4, "BusinessObjectNode.png");
      this.imageList1.Images.SetKeyName(5, "Association.png");
      this.imageList1.Images.SetKeyName(6, "Query.png");
      this.imageList1.Images.SetKeyName(7, "DataType.png");
      this.imageList1.Images.SetKeyName(8, "EnhancementOption.png");
      this.imageList1.Images.SetKeyName(9, "189BusinessConfigurationObject16x16.png");
      this.imageList1.Images.SetKeyName(10, "Composition_Association.png");
      this.imageList1.Images.SetKeyName(11, "Composition_Association_WithToN.png");
      this.imageList1.Images.SetKeyName(12, "Cross_BO_Association.png");
      this.imageList1.Images.SetKeyName(13, "Cross_BO_Association_WithToN.png");
      this.imageList1.Images.SetKeyName(14, "Internal_Association.png");
      this.imageList1.Images.SetKeyName(15, "Internal_Association_WithToN.png");
      this.imageList1.Images.SetKeyName(16, "DependentObject_Association.png");
      this.imageList1.Images.SetKeyName(17, "180Warning.png");
      this.imageList1.Images.SetKeyName(18, "Query_Parameter.png");
      this.imageList1.Images.SetKeyName(19, "Web_Services_16x16.png");
      this.imageList1.Images.SetKeyName(20, "232DeprecatedAssociation16x16.png");
      this.imageList1.Images.SetKeyName(21, "233DeprecatedBusinessObject16x16.png");
      this.imageList1.Images.SetKeyName(22, "235DeprecatedCompositionAssociationN16x16.png");
      this.imageList1.Images.SetKeyName(23, "236DeprecatedCrossBOAssociation16x16.png");
      this.imageList1.Images.SetKeyName(24, "237DeprecatedCrossBOAssociationN16x16.png");
      this.imageList1.Images.SetKeyName(25, "238DeprecatedDependendObjectAssociation16x16.png");
      this.imageList1.Images.SetKeyName(26, "239DeprecatedElement16x16.png");
      this.imageList1.Images.SetKeyName(27, "240DeprecatedInternalAssociation16x16.png");
      this.imageList1.Images.SetKeyName(28, "241DeprecatedInternalAssociationN16x16.png");
      this.imageList1.Images.SetKeyName(29, "242DeprecatedNode16x16.png");
      this.imageList1.Images.SetKeyName(30, "243ReadonlyAssociation16x16.png");
      this.imageList1.Images.SetKeyName(31, "244ReadonlyBusinessObject16x16.png");
      this.imageList1.Images.SetKeyName(32, "247ReadonlyCrossBOAssociation16x16.png");
      this.imageList1.Images.SetKeyName(33, "248ReadonlyCrossBOAssociationN16x16.png");
      this.imageList1.Images.SetKeyName(34, "249ReadonlyDependendObjectAssociation16x16.png");
      this.imageList1.Images.SetKeyName(35, "250ReadonlyElement16x16.png");
      this.imageList1.Images.SetKeyName(36, "251ReadonlyInternalAssociation16x16.png");
      this.imageList1.Images.SetKeyName(37, "252ReadonlyInternalAssociationN16x16.png");
      this.imageList1.Images.SetKeyName(38, "253ReadonlyNode16x16.png");
      this.imageList1.Images.SetKeyName(39, "261DeprecatedAction16x16.png");
      this.imageList1.Images.SetKeyName(40, "265DeprecatedQuery16x16.png");
      this.imageList1.Images.SetKeyName(41, "ExtensibleNode16x16.png");
      this.imageList1.Images.SetKeyName(42, "KeyElement16x16.png");
      this.imageList1.Images.SetKeyName(43, "WebServiceOperation16x16.png");
      this.imageList1.Images.SetKeyName(44, "Action_Parameter.png");
      this.imageList1.Images.SetKeyName(45, "271help16x16.png");
      this.imageList1.Images.SetKeyName(46, "EmbeddedComponent.png");
      this.imageList1.Images.SetKeyName(47, "OVS.png");
      this.imageList1.Images.SetKeyName(48, "Floorplan.png");
      this.imageList1.Images.SetKeyName(49, "ModalDialog.png");
      this.imageList1.Images.SetKeyName(50, "ThingType.png");
      this.imageList1.Images.SetKeyName(51, "QAF.png");
      this.imageList1.Images.SetKeyName(52, "OIF.png");
      this.imageList1.Images.SetKeyName(53, "OWL.png");
      this.imageList1.Images.SetKeyName(54, "ThingInspector.png");
      this.imageList1.Images.SetKeyName(55, "QuickCreate.png");
      this.imageList1.Images.SetKeyName(56, "QuickView.png");
      this.miniToolStrip.AutoSize = false;
      this.miniToolStrip.BackColor = Color.Transparent;
      this.miniToolStrip.CanOverflow = false;
      this.miniToolStrip.Dock = DockStyle.None;
      this.miniToolStrip.GripStyle = ToolStripGripStyle.Hidden;
      this.miniToolStrip.Location = new Point(479, 3);
      this.miniToolStrip.Name = "miniToolStrip";
      this.miniToolStrip.Size = new Size(713, 25);
      this.miniToolStrip.TabIndex = 2;
      this.contextMenuStripSUBelement.Items.AddRange(new ToolStripItem[3]
      {
        (ToolStripItem) this.copyElementNameToolStripMenuItem1,
        (ToolStripItem) this.copySelectedPathToolStripMenuItem,
        (ToolStripItem) this.copyImportNamespaceToolStripMenuItem1
      });
      this.contextMenuStripSUBelement.Name = "contextMenuStripSUBelement";
      this.contextMenuStripSUBelement.Size = new Size(177, 70);
      this.copyElementNameToolStripMenuItem1.Name = "copyElementNameToolStripMenuItem1";
      this.copyElementNameToolStripMenuItem1.Size = new Size(176, 22);
      this.copyElementNameToolStripMenuItem1.Text = "Copy Entity Name";
      this.copyElementNameToolStripMenuItem1.Click += new EventHandler(this.copyElementNameToolStripMenuItem_Click);
      this.copySelectedPathToolStripMenuItem.Name = "copySelectedPathToolStripMenuItem";
      this.copySelectedPathToolStripMenuItem.Size = new Size(176, 22);
      this.copySelectedPathToolStripMenuItem.Text = "Copy Selected Path";
      this.copySelectedPathToolStripMenuItem.Click += new EventHandler(this.copySelectedPathToolStripMenuItem_Click);
      this.copyImportNamespaceToolStripMenuItem1.Name = "copyImportNamespaceToolStripMenuItem1";
      this.copyImportNamespaceToolStripMenuItem1.Size = new Size(176, 22);
      this.copyImportNamespaceToolStripMenuItem1.Text = "Copy Namespace";
      this.copyImportNamespaceToolStripMenuItem1.Click += new EventHandler(this.copyNamespaceToolStripMenuItem_Click);
      this.contextMenuStripFirstLevel.Items.AddRange(new ToolStripItem[2]
      {
        (ToolStripItem) this.copyElementNameToolStripMenuItem2,
        (ToolStripItem) this.copyNamespaceToolStripMenuItem
      });
      this.contextMenuStripFirstLevel.Name = "contextMenuStripDT";
      this.contextMenuStripFirstLevel.Size = new Size(171, 48);
      this.copyElementNameToolStripMenuItem2.Name = "copyElementNameToolStripMenuItem2";
      this.copyElementNameToolStripMenuItem2.Size = new Size(170, 22);
      this.copyElementNameToolStripMenuItem2.Text = "Copy Entity Name";
      this.copyElementNameToolStripMenuItem2.Click += new EventHandler(this.copyElementNameToolStripMenuItem_Click);
      this.copyNamespaceToolStripMenuItem.Name = "copyNamespaceToolStripMenuItem";
      this.copyNamespaceToolStripMenuItem.Size = new Size(170, 22);
      this.copyNamespaceToolStripMenuItem.Text = "Copy Namespace";
      this.copyNamespaceToolStripMenuItem.Click += new EventHandler(this.copyNamespaceToolStripMenuItem_Click);
      this.contextMenuStripOP.Items.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) this.copyEntityNameToolStripMenuItem
      });
      this.contextMenuStripOP.Name = "contextMenuStripOP";
      this.contextMenuStripOP.Size = new Size(171, 26);
      this.copyEntityNameToolStripMenuItem.Name = "copyEntityNameToolStripMenuItem";
      this.copyEntityNameToolStripMenuItem.Size = new Size(170, 22);
      this.copyEntityNameToolStripMenuItem.Text = "Copy Entity Name";
      this.copyEntityNameToolStripMenuItem.Click += new EventHandler(this.copyElementNameToolStripMenuItem_Click);
      this.contextMenuStripQuery.Items.AddRange(new ToolStripItem[4]
      {
        (ToolStripItem) this.executeQueryToolStripMenuItem,
        (ToolStripItem) this.toolStripMenuItem1,
        (ToolStripItem) this.toolStripMenuItem2,
        (ToolStripItem) this.toolStripMenuItem3
      });
      this.contextMenuStripQuery.Name = "contextMenuStripSUBelement";
      this.contextMenuStripQuery.Size = new Size(177, 92);
      this.executeQueryToolStripMenuItem.Name = "executeQueryToolStripMenuItem";
      this.executeQueryToolStripMenuItem.Size = new Size(176, 22);
      this.executeQueryToolStripMenuItem.Text = "Execute Query";
      this.executeQueryToolStripMenuItem.Click += new EventHandler(this.executeQueryToolStripMenuItem_Click);
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new Size(176, 22);
      this.toolStripMenuItem1.Text = "Copy Entity Name";
      this.toolStripMenuItem1.Click += new EventHandler(this.copyElementNameToolStripMenuItem_Click);
      this.toolStripMenuItem2.Name = "toolStripMenuItem2";
      this.toolStripMenuItem2.Size = new Size(176, 22);
      this.toolStripMenuItem2.Text = "Copy Selected Path";
      this.toolStripMenuItem2.Click += new EventHandler(this.copySelectedPathToolStripMenuItem_Click);
      this.toolStripMenuItem3.Name = "toolStripMenuItem3";
      this.toolStripMenuItem3.Size = new Size(176, 22);
      this.toolStripMenuItem3.Text = "Copy Namespace";
      this.toolStripMenuItem3.Click += new EventHandler(this.copyNamespaceToolStripMenuItem_Click);
      this.tabReuseUIs.Controls.Add((Control) this.splitContainer5);
      this.tabReuseUIs.Controls.Add((Control) this.webBrowserEC);
      this.tabReuseUIs.Controls.Add((Control) this.toolStrip1);
      this.tabReuseUIs.ImageKey = "Floorplan.png";
      this.tabReuseUIs.Location = new Point(4, 23);
      this.tabReuseUIs.Name = "tabReuseUIs";
      this.tabReuseUIs.Size = new Size(784, 401);
      this.tabReuseUIs.TabIndex = 4;
      this.tabReuseUIs.Text = "Reuse UIs";
      this.tabReuseUIs.UseVisualStyleBackColor = true;
      this.splitContainer5.Dock = DockStyle.Fill;
      this.splitContainer5.Location = new Point(0, 25);
      this.splitContainer5.Name = "splitContainer5";
      this.splitContainer5.Panel1.Controls.Add((Control) this.richTextBoxReuseUIs);
      this.splitContainer5.Panel1.Controls.Add((Control) this.treeViewReuseUIs);
      this.splitContainer5.Panel2.Controls.Add((Control) this.tabControl2);
      this.splitContainer5.Size = new Size(784, 376);
      this.splitContainer5.SplitterDistance = 217;
      this.splitContainer5.TabIndex = 15;
      this.richTextBoxReuseUIs.BackColor = SystemColors.ButtonHighlight;
      this.richTextBoxReuseUIs.Dock = DockStyle.Fill;
      this.richTextBoxReuseUIs.Location = new Point(0, 0);
      this.richTextBoxReuseUIs.Name = "richTextBoxReuseUIs";
      this.richTextBoxReuseUIs.ReadOnly = true;
      this.richTextBoxReuseUIs.Size = new Size(217, 376);
      this.richTextBoxReuseUIs.TabIndex = 16;
      this.richTextBoxReuseUIs.Text = "";
      this.richTextBoxReuseUIs.Visible = false;
      this.treeViewReuseUIs.Dock = DockStyle.Fill;
      this.treeViewReuseUIs.ImageIndex = 0;
      this.treeViewReuseUIs.ImageList = this.imageList1;
      this.treeViewReuseUIs.Location = new Point(0, 0);
      this.treeViewReuseUIs.Name = "treeViewReuseUIs";
      this.treeViewReuseUIs.PathSeparator = ".";
      this.treeViewReuseUIs.SelectedImageIndex = 0;
      this.treeViewReuseUIs.ShowNodeToolTips = true;
      this.treeViewReuseUIs.Size = new Size(217, 376);
      this.treeViewReuseUIs.TabIndex = 14;
      this.treeViewReuseUIs.MouseUp += new MouseEventHandler(this.treeViewReuseUIs_MouseUp);
      this.tabControl2.Controls.Add((Control) this.tabPage1);
      this.tabControl2.Dock = DockStyle.Fill;
      this.tabControl2.Location = new Point(0, 0);
      this.tabControl2.Name = "tabControl2";
      this.tabControl2.SelectedIndex = 0;
      this.tabControl2.Size = new Size(563, 376);
      this.tabControl2.TabIndex = 14;
      this.tabPage1.Controls.Add((Control) this.webBrowserReuseUIs);
      this.tabPage1.Location = new Point(4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new Padding(3);
      this.tabPage1.Size = new Size(555, 350);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "Description";
      this.tabPage1.UseVisualStyleBackColor = true;
      this.webBrowserReuseUIs.Dock = DockStyle.Fill;
      this.webBrowserReuseUIs.Location = new Point(3, 3);
      this.webBrowserReuseUIs.MinimumSize = new Size(20, 20);
      this.webBrowserReuseUIs.Name = "webBrowserReuseUIs";
      this.webBrowserReuseUIs.ScriptErrorsSuppressed = true;
      this.webBrowserReuseUIs.Size = new Size(549, 344);
      this.webBrowserReuseUIs.TabIndex = 14;
      this.webBrowserEC.Dock = DockStyle.Fill;
      this.webBrowserEC.Location = new Point(0, 25);
      this.webBrowserEC.MinimumSize = new Size(20, 20);
      this.webBrowserEC.Name = "webBrowserEC";
      this.webBrowserEC.ScriptErrorsSuppressed = true;
      this.webBrowserEC.Size = new Size(784, 376);
      this.webBrowserEC.TabIndex = 14;
      this.toolStrip1.BackColor = Color.FromArgb(188, 199, 217);
      this.toolStrip1.Items.AddRange(new ToolStripItem[6]
      {
        (ToolStripItem) this.toolStripButton1,
        (ToolStripItem) this.toolStripButton2,
        (ToolStripItem) this.toolStripSeparator9,
        (ToolStripItem) this.toolStripSeparator10,
        (ToolStripItem) this.toolStripComboBoxSearchReuseUI,
        (ToolStripItem) this.toolStripButtonSearchReuseUI
      });
      this.toolStrip1.Location = new Point(0, 0);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new Size(784, 25);
      this.toolStrip1.Stretch = true;
      this.toolStrip1.TabIndex = 10;
      this.toolStrip1.Text = "toolStrip1";
      this.toolStripButton1.BackColor = Color.Transparent;
      this.toolStripButton1.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButton1.Enabled = false;
      this.toolStripButton1.ForeColor = SystemColors.ControlText;
      this.toolStripButton1.Image = (Image) CopernicusResources.GoToPreviousHS;
      this.toolStripButton1.ImageTransparentColor = Color.Black;
      this.toolStripButton1.Margin = new Padding(0, 1, 1, 2);
      this.toolStripButton1.Name = "toolStripButton1";
      this.toolStripButton1.Size = new Size(23, 22);
      this.toolStripButton1.Text = "Back";
      this.toolStripButton2.BackColor = Color.Transparent;
      this.toolStripButton2.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButton2.Enabled = false;
      this.toolStripButton2.Image = (Image) CopernicusResources.GoToNextHS;
      this.toolStripButton2.ImageTransparentColor = Color.Black;
      this.toolStripButton2.Margin = new Padding(1, 1, 1, 2);
      this.toolStripButton2.Name = "toolStripButton2";
      this.toolStripButton2.Size = new Size(23, 22);
      this.toolStripButton2.Text = "Forward";
      this.toolStripSeparator9.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator9.Name = "toolStripSeparator9";
      this.toolStripSeparator9.Size = new Size(6, 25);
      this.toolStripSeparator10.BackColor = Color.Black;
      this.toolStripSeparator10.ForeColor = SystemColors.Control;
      this.toolStripSeparator10.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator10.Name = "toolStripSeparator10";
      this.toolStripSeparator10.Size = new Size(6, 25);
      this.toolStripComboBoxSearchReuseUI.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxSearchReuseUI.AutoCompleteSource = AutoCompleteSource.CustomSource;
      this.toolStripComboBoxSearchReuseUI.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxSearchReuseUI.IntegralHeight = false;
      this.toolStripComboBoxSearchReuseUI.Margin = new Padding(1, 0, 4, 0);
      this.toolStripComboBoxSearchReuseUI.MaxDropDownItems = 12;
      this.toolStripComboBoxSearchReuseUI.Name = "toolStripComboBoxSearchReuseUI";
      this.toolStripComboBoxSearchReuseUI.Size = new Size(150, 25);
      this.toolStripComboBoxSearchReuseUI.Sorted = true;
      this.toolStripComboBoxSearchReuseUI.Text = "Search Reuse UI";
      this.toolStripComboBoxSearchReuseUI.KeyDown += new KeyEventHandler(this.toolStripComboBoxSearchReuseUI_KeyDown);
      this.toolStripButtonSearchReuseUI.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonSearchReuseUI.Image = (Image) CopernicusResources.View1;
      this.toolStripButtonSearchReuseUI.ImageTransparentColor = Color.Black;
      this.toolStripButtonSearchReuseUI.Margin = new Padding(4, 1, 2, 2);
      this.toolStripButtonSearchReuseUI.Name = "toolStripButtonSearchReuseUI";
      this.toolStripButtonSearchReuseUI.Size = new Size(23, 22);
      this.toolStripButtonSearchReuseUI.Text = "Search";
      this.toolStripButtonSearchReuseUI.Click += new EventHandler(this.toolStripButtonSearchReuseUI_Click);
      this.tabPageInboundServiceInterfaces.BackColor = Color.FromArgb(188, 199, 217);
      this.tabPageInboundServiceInterfaces.Controls.Add((Control) this.splitContainer4);
      this.tabPageInboundServiceInterfaces.Controls.Add((Control) this.toolStrip5ISI);
      this.tabPageInboundServiceInterfaces.ImageIndex = 19;
      this.tabPageInboundServiceInterfaces.Location = new Point(4, 23);
      this.tabPageInboundServiceInterfaces.Name = "tabPageInboundServiceInterfaces";
      this.tabPageInboundServiceInterfaces.Padding = new Padding(3);
      this.tabPageInboundServiceInterfaces.Size = new Size(784, 401);
      this.tabPageInboundServiceInterfaces.TabIndex = 3;
      this.tabPageInboundServiceInterfaces.Text = "Inbound Service Interfaces";
      this.splitContainer4.Dock = DockStyle.Fill;
      this.splitContainer4.Location = new Point(3, 28);
      this.splitContainer4.Name = "splitContainer4";
      this.splitContainer4.Panel1.Controls.Add((Control) this.richTextBoxInfoBoxISI);
      this.splitContainer4.Panel1.Controls.Add((Control) this.treeViewISI);
      this.splitContainer4.Panel2.Controls.Add((Control) this.webBrowserISI);
      this.splitContainer4.Size = new Size(778, 370);
      this.splitContainer4.SplitterDistance = 290;
      this.splitContainer4.TabIndex = 4;
      this.richTextBoxInfoBoxISI.BackColor = SystemColors.ButtonHighlight;
      this.richTextBoxInfoBoxISI.Dock = DockStyle.Fill;
      this.richTextBoxInfoBoxISI.Location = new Point(0, 0);
      this.richTextBoxInfoBoxISI.Name = "richTextBoxInfoBoxISI";
      this.richTextBoxInfoBoxISI.ReadOnly = true;
      this.richTextBoxInfoBoxISI.Size = new Size(290, 370);
      this.richTextBoxInfoBoxISI.TabIndex = 18;
      this.richTextBoxInfoBoxISI.Text = "";
      this.richTextBoxInfoBoxISI.Visible = false;
      this.treeViewISI.Dock = DockStyle.Fill;
      this.treeViewISI.ImageIndex = 0;
      this.treeViewISI.ImageList = this.imageList1;
      this.treeViewISI.Location = new Point(0, 0);
      this.treeViewISI.Name = "treeViewISI";
      this.treeViewISI.SelectedImageIndex = 0;
      this.treeViewISI.ShowNodeToolTips = true;
      this.treeViewISI.Size = new Size(290, 370);
      this.treeViewISI.TabIndex = 0;
      this.treeViewISI.AfterCollapse += new TreeViewEventHandler(this.treeViewISI_AfterCollapse);
      this.treeViewISI.AfterExpand += new TreeViewEventHandler(this.treeViewISI_AfterExpand);
      this.treeViewISI.AfterSelect += new TreeViewEventHandler(this.treeViewISI_AfterSelect);
      this.treeViewISI.MouseUp += new MouseEventHandler(this.treeViewISI_MouseUp);
      this.webBrowserISI.Dock = DockStyle.Fill;
      this.webBrowserISI.Location = new Point(0, 0);
      this.webBrowserISI.MinimumSize = new Size(20, 20);
      this.webBrowserISI.Name = "webBrowserISI";
      this.webBrowserISI.ScriptErrorsSuppressed = true;
      this.webBrowserISI.Size = new Size(484, 370);
      this.webBrowserISI.TabIndex = 14;
      this.webBrowserISI.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.webBrowserISI_DocumentCompleted);
      this.webBrowserISI.Navigating += new WebBrowserNavigatingEventHandler(this.webBrowserISI_Navigating);
      this.toolStrip5ISI.BackColor = Color.Transparent;
      this.toolStrip5ISI.Items.AddRange(new ToolStripItem[12]
      {
        (ToolStripItem) this.toolStripButtonBackISI,
        (ToolStripItem) this.toolStripButtonNextISI,
        (ToolStripItem) this.toolStripSeparator7,
        (ToolStripItem) this.toolStripButtonHorizontalISI,
        (ToolStripItem) this.toolStripButtonVerticalISI,
        (ToolStripItem) this.toolStripSeparator8,
        (ToolStripItem) this.toolStripComboBoxSearchISI,
        (ToolStripItem) this.toolStripButtonSearchISI,
        (ToolStripItem) this.toolStripLabelFilterISI,
        (ToolStripItem) this.toolStripComboBoxNamespaceISI,
        (ToolStripItem) this.toolStripButtonClearISI,
        (ToolStripItem) this.toolStripButtonHelpISI
      });
      this.toolStrip5ISI.Location = new Point(3, 3);
      this.toolStrip5ISI.Name = "toolStrip5ISI";
      this.toolStrip5ISI.Size = new Size(778, 25);
      this.toolStrip5ISI.TabIndex = 3;
      this.toolStrip5ISI.Text = "toolStrip5WS";
      this.toolStripButtonBackISI.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonBackISI.Enabled = false;
      this.toolStripButtonBackISI.Image = (Image) CopernicusResources.GoToPreviousHS;
      this.toolStripButtonBackISI.ImageTransparentColor = Color.Black;
      this.toolStripButtonBackISI.Margin = new Padding(0, 1, 1, 2);
      this.toolStripButtonBackISI.Name = "toolStripButtonBackISI";
      this.toolStripButtonBackISI.Size = new Size(23, 22);
      this.toolStripButtonBackISI.Text = "Back";
      this.toolStripButtonBackISI.Click += new EventHandler(this.toolStripButtonBackISI_Click);
      this.toolStripButtonNextISI.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonNextISI.Enabled = false;
      this.toolStripButtonNextISI.Image = (Image) CopernicusResources.GoToNextHS;
      this.toolStripButtonNextISI.ImageTransparentColor = Color.Black;
      this.toolStripButtonNextISI.Margin = new Padding(1, 1, 1, 2);
      this.toolStripButtonNextISI.Name = "toolStripButtonNextISI";
      this.toolStripButtonNextISI.Size = new Size(23, 22);
      this.toolStripButtonNextISI.Text = "Forward";
      this.toolStripButtonNextISI.Click += new EventHandler(this.toolStripButtonNextISI_Click);
      this.toolStripSeparator7.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator7.Name = "toolStripSeparator7";
      this.toolStripSeparator7.Size = new Size(6, 25);
      this.toolStripButtonHorizontalISI.CheckOnClick = true;
      this.toolStripButtonHorizontalISI.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonHorizontalISI.Image = (Image) CopernicusResources._254HorizontalWindows16x16;
      this.toolStripButtonHorizontalISI.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonHorizontalISI.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonHorizontalISI.Name = "toolStripButtonHorizontalISI";
      this.toolStripButtonHorizontalISI.Size = new Size(23, 22);
      this.toolStripButtonHorizontalISI.Text = "Horizontal";
      this.toolStripButtonHorizontalISI.Click += new EventHandler(this.toolStripButtonHorizontalISI_Click);
      this.toolStripButtonVerticalISI.Checked = true;
      this.toolStripButtonVerticalISI.CheckOnClick = true;
      this.toolStripButtonVerticalISI.CheckState = CheckState.Checked;
      this.toolStripButtonVerticalISI.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonVerticalISI.Image = (Image) CopernicusResources._255VerticalWindows16x16;
      this.toolStripButtonVerticalISI.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonVerticalISI.Margin = new Padding(2, 1, 1, 2);
      this.toolStripButtonVerticalISI.Name = "toolStripButtonVerticalISI";
      this.toolStripButtonVerticalISI.Size = new Size(23, 22);
      this.toolStripButtonVerticalISI.Text = "toolStripButton4";
      this.toolStripButtonVerticalISI.ToolTipText = "Vertical";
      this.toolStripButtonVerticalISI.Click += new EventHandler(this.toolStripButtonVerticalISI_Click);
      this.toolStripSeparator8.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator8.Name = "toolStripSeparator8";
      this.toolStripSeparator8.Size = new Size(6, 25);
      this.toolStripComboBoxSearchISI.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxSearchISI.AutoCompleteSource = AutoCompleteSource.CustomSource;
      this.toolStripComboBoxSearchISI.DropDownWidth = 140;
      this.toolStripComboBoxSearchISI.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxSearchISI.Margin = new Padding(4, 0, 4, 0);
      this.toolStripComboBoxSearchISI.MaxDropDownItems = 12;
      this.toolStripComboBoxSearchISI.Name = "toolStripComboBoxSearchISI";
      this.toolStripComboBoxSearchISI.Size = new Size(150, 25);
      this.toolStripComboBoxSearchISI.Sorted = true;
      this.toolStripComboBoxSearchISI.Text = "Search service interfaces";
      this.toolStripComboBoxSearchISI.DropDown += new EventHandler(this.toolStripComboBoxSearchISI_DropDown);
      this.toolStripButtonSearchISI.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonSearchISI.Image = (Image) CopernicusResources.View1;
      this.toolStripButtonSearchISI.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonSearchISI.Margin = new Padding(4, 1, 2, 2);
      this.toolStripButtonSearchISI.Name = "toolStripButtonSearchISI";
      this.toolStripButtonSearchISI.Size = new Size(23, 22);
      this.toolStripButtonSearchISI.Text = "Search";
      this.toolStripButtonSearchISI.Click += new EventHandler(this.toolStripButtonSearchISI_Click);
      this.toolStripLabelFilterISI.Margin = new Padding(2, 1, 2, 2);
      this.toolStripLabelFilterISI.Name = "toolStripLabelFilterISI";
      this.toolStripLabelFilterISI.Size = new Size(52, 22);
      this.toolStripLabelFilterISI.Text = "Filter by:";
      this.toolStripComboBoxNamespaceISI.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxNamespaceISI.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.toolStripComboBoxNamespaceISI.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxNamespaceISI.IntegralHeight = false;
      this.toolStripComboBoxNamespaceISI.Margin = new Padding(4, 0, 4, 0);
      this.toolStripComboBoxNamespaceISI.MaxDropDownItems = 15;
      this.toolStripComboBoxNamespaceISI.Name = "toolStripComboBoxNamespaceISI";
      this.toolStripComboBoxNamespaceISI.Size = new Size(140, 25);
      this.toolStripComboBoxNamespaceISI.Text = "Namespace";
      this.toolStripComboBoxNamespaceISI.DropDown += new EventHandler(this.toolStripComboBoxNamespaceISI_DropDown);
      this.toolStripButtonClearISI.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonClearISI.Image = (Image) CopernicusResources.ClearSearch16x16;
      this.toolStripButtonClearISI.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonClearISI.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonClearISI.Name = "toolStripButtonClearISI";
      this.toolStripButtonClearISI.Size = new Size(23, 22);
      this.toolStripButtonClearISI.Text = "Clear Search";
      this.toolStripButtonClearISI.ToolTipText = "Clear Search";
      this.toolStripButtonClearISI.Click += new EventHandler(this.toolStripButtonClearISI_Click);
      this.toolStripButtonHelpISI.Alignment = ToolStripItemAlignment.Right;
      this.toolStripButtonHelpISI.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonHelpISI.Image = (Image) CopernicusResources._271help16x16;
      this.toolStripButtonHelpISI.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonHelpISI.Name = "toolStripButtonHelpISI";
      this.toolStripButtonHelpISI.RightToLeft = RightToLeft.No;
      this.toolStripButtonHelpISI.Size = new Size(23, 22);
      this.toolStripButtonHelpISI.Text = "Help";
      this.toolStripButtonHelpISI.Click += new EventHandler(this.toolStripButtonHelpISI_Click);
      this.tabPageEnhancementOptions.BackColor = Color.FromArgb(188, 199, 217);
      this.tabPageEnhancementOptions.Controls.Add((Control) this.splitContainer3);
      this.tabPageEnhancementOptions.Controls.Add((Control) this.toolStrip4EO);
      this.tabPageEnhancementOptions.ImageIndex = 8;
      this.tabPageEnhancementOptions.Location = new Point(4, 23);
      this.tabPageEnhancementOptions.Name = "tabPageEnhancementOptions";
      this.tabPageEnhancementOptions.Padding = new Padding(3);
      this.tabPageEnhancementOptions.Size = new Size(784, 401);
      this.tabPageEnhancementOptions.TabIndex = 2;
      this.tabPageEnhancementOptions.Text = "Enhancement Options";
      this.splitContainer3.Dock = DockStyle.Fill;
      this.splitContainer3.Location = new Point(3, 28);
      this.splitContainer3.Name = "splitContainer3";
      this.splitContainer3.Panel1.Controls.Add((Control) this.richTextBoxInfoBoxEO);
      this.splitContainer3.Panel1.Controls.Add((Control) this.treeViewEO);
      this.splitContainer3.Panel2.Controls.Add((Control) this.webBrowserEO);
      this.splitContainer3.Size = new Size(778, 370);
      this.splitContainer3.SplitterDistance = 288;
      this.splitContainer3.TabIndex = 0;
      this.richTextBoxInfoBoxEO.BackColor = SystemColors.ButtonHighlight;
      this.richTextBoxInfoBoxEO.Dock = DockStyle.Fill;
      this.richTextBoxInfoBoxEO.Location = new Point(0, 0);
      this.richTextBoxInfoBoxEO.Name = "richTextBoxInfoBoxEO";
      this.richTextBoxInfoBoxEO.ReadOnly = true;
      this.richTextBoxInfoBoxEO.Size = new Size(288, 370);
      this.richTextBoxInfoBoxEO.TabIndex = 17;
      this.richTextBoxInfoBoxEO.Text = "";
      this.richTextBoxInfoBoxEO.Visible = false;
      this.treeViewEO.Dock = DockStyle.Fill;
      this.treeViewEO.ImageIndex = 8;
      this.treeViewEO.ImageList = this.imageList1;
      this.treeViewEO.Location = new Point(0, 0);
      this.treeViewEO.Name = "treeViewEO";
      this.treeViewEO.SelectedImageIndex = 8;
      this.treeViewEO.ShowNodeToolTips = true;
      this.treeViewEO.Size = new Size(288, 370);
      this.treeViewEO.TabIndex = 0;
      this.treeViewEO.AfterSelect += new TreeViewEventHandler(this.treeViewEO_AfterSelect);
      this.treeViewEO.MouseUp += new MouseEventHandler(this.treeViewEO_MouseUp);
      this.webBrowserEO.Dock = DockStyle.Fill;
      this.webBrowserEO.Location = new Point(0, 0);
      this.webBrowserEO.MinimumSize = new Size(20, 20);
      this.webBrowserEO.Name = "webBrowserEO";
      this.webBrowserEO.ScriptErrorsSuppressed = true;
      this.webBrowserEO.Size = new Size(486, 370);
      this.webBrowserEO.TabIndex = 14;
      this.webBrowserEO.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.webBrowserEO_DocumentCompleted);
      this.webBrowserEO.Navigating += new WebBrowserNavigatingEventHandler(this.webBrowserEO_Navigating);
      this.toolStrip4EO.BackColor = Color.Transparent;
      this.toolStrip4EO.Items.AddRange(new ToolStripItem[12]
      {
        (ToolStripItem) this.toolStripButtonBackEO,
        (ToolStripItem) this.toolStripButtonNextEO,
        (ToolStripItem) this.toolStripSeparator5,
        (ToolStripItem) this.toolStripButtonHorizontalWindowsEO,
        (ToolStripItem) this.toolStripButtonVerticalWindowsEO,
        (ToolStripItem) this.toolStripSeparator6,
        (ToolStripItem) this.toolStripComboBoxSearchEO,
        (ToolStripItem) this.toolStripButtonSearchEO,
        (ToolStripItem) this.toolStripLabelFilterEO,
        (ToolStripItem) this.toolStripComboBoxNamespaceEO,
        (ToolStripItem) this.toolStripButtonClearEO,
        (ToolStripItem) this.toolStripButtonHelpEO
      });
      this.toolStrip4EO.Location = new Point(3, 3);
      this.toolStrip4EO.Name = "toolStrip4EO";
      this.toolStrip4EO.Size = new Size(778, 25);
      this.toolStrip4EO.TabIndex = 2;
      this.toolStrip4EO.Text = "toolStrip3";
      this.toolStripButtonBackEO.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonBackEO.Enabled = false;
      this.toolStripButtonBackEO.Image = (Image) CopernicusResources.GoToPreviousHS;
      this.toolStripButtonBackEO.ImageTransparentColor = Color.Black;
      this.toolStripButtonBackEO.Margin = new Padding(0, 1, 1, 2);
      this.toolStripButtonBackEO.Name = "toolStripButtonBackEO";
      this.toolStripButtonBackEO.Size = new Size(23, 22);
      this.toolStripButtonBackEO.Text = "Back";
      this.toolStripButtonBackEO.Click += new EventHandler(this.toolStripButtonBackEO_Click);
      this.toolStripButtonNextEO.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonNextEO.Enabled = false;
      this.toolStripButtonNextEO.Image = (Image) CopernicusResources.GoToNextHS;
      this.toolStripButtonNextEO.ImageTransparentColor = Color.Black;
      this.toolStripButtonNextEO.Margin = new Padding(1, 1, 1, 2);
      this.toolStripButtonNextEO.Name = "toolStripButtonNextEO";
      this.toolStripButtonNextEO.Size = new Size(23, 22);
      this.toolStripButtonNextEO.Text = "Forward";
      this.toolStripButtonNextEO.Click += new EventHandler(this.toolStripButtonNextEO_Click);
      this.toolStripSeparator5.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator5.Name = "toolStripSeparator5";
      this.toolStripSeparator5.Size = new Size(6, 25);
      this.toolStripButtonHorizontalWindowsEO.CheckOnClick = true;
      this.toolStripButtonHorizontalWindowsEO.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonHorizontalWindowsEO.Image = (Image) CopernicusResources._254HorizontalWindows16x16;
      this.toolStripButtonHorizontalWindowsEO.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonHorizontalWindowsEO.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonHorizontalWindowsEO.Name = "toolStripButtonHorizontalWindowsEO";
      this.toolStripButtonHorizontalWindowsEO.Size = new Size(23, 22);
      this.toolStripButtonHorizontalWindowsEO.Text = "Horizontal";
      this.toolStripButtonHorizontalWindowsEO.Click += new EventHandler(this.toolStripButtonHorizontalWindowsEO_Click);
      this.toolStripButtonVerticalWindowsEO.Checked = true;
      this.toolStripButtonVerticalWindowsEO.CheckOnClick = true;
      this.toolStripButtonVerticalWindowsEO.CheckState = CheckState.Checked;
      this.toolStripButtonVerticalWindowsEO.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonVerticalWindowsEO.Image = (Image) CopernicusResources._255VerticalWindows16x16;
      this.toolStripButtonVerticalWindowsEO.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonVerticalWindowsEO.Margin = new Padding(2, 1, 1, 2);
      this.toolStripButtonVerticalWindowsEO.Name = "toolStripButtonVerticalWindowsEO";
      this.toolStripButtonVerticalWindowsEO.Size = new Size(23, 22);
      this.toolStripButtonVerticalWindowsEO.Text = "toolStripButton4";
      this.toolStripButtonVerticalWindowsEO.ToolTipText = "Vertical";
      this.toolStripButtonVerticalWindowsEO.Click += new EventHandler(this.toolStripButtonVerticalWindowsEO_Click);
      this.toolStripSeparator6.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator6.Name = "toolStripSeparator6";
      this.toolStripSeparator6.Size = new Size(6, 25);
      this.toolStripComboBoxSearchEO.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxSearchEO.AutoCompleteSource = AutoCompleteSource.CustomSource;
      this.toolStripComboBoxSearchEO.DropDownWidth = 140;
      this.toolStripComboBoxSearchEO.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxSearchEO.Margin = new Padding(4, 0, 4, 0);
      this.toolStripComboBoxSearchEO.MaxDropDownItems = 12;
      this.toolStripComboBoxSearchEO.Name = "toolStripComboBoxSearchEO";
      this.toolStripComboBoxSearchEO.Size = new Size(150, 25);
      this.toolStripComboBoxSearchEO.Sorted = true;
      this.toolStripComboBoxSearchEO.Text = "Search enhancement options";
      this.toolStripComboBoxSearchEO.DropDown += new EventHandler(this.toolStripComboBoxSearchEO_DropDown);
      this.toolStripButtonSearchEO.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonSearchEO.Image = (Image) CopernicusResources.View1;
      this.toolStripButtonSearchEO.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonSearchEO.Margin = new Padding(4, 1, 2, 2);
      this.toolStripButtonSearchEO.Name = "toolStripButtonSearchEO";
      this.toolStripButtonSearchEO.Size = new Size(23, 22);
      this.toolStripButtonSearchEO.Text = "Search";
      this.toolStripButtonSearchEO.Click += new EventHandler(this.toolStripButtonSearchEO_Click);
      this.toolStripLabelFilterEO.Margin = new Padding(2, 1, 2, 2);
      this.toolStripLabelFilterEO.Name = "toolStripLabelFilterEO";
      this.toolStripLabelFilterEO.Size = new Size(52, 22);
      this.toolStripLabelFilterEO.Text = "Filter by:";
      this.toolStripComboBoxNamespaceEO.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxNamespaceEO.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.toolStripComboBoxNamespaceEO.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxNamespaceEO.IntegralHeight = false;
      this.toolStripComboBoxNamespaceEO.Margin = new Padding(4, 0, 4, 0);
      this.toolStripComboBoxNamespaceEO.MaxDropDownItems = 15;
      this.toolStripComboBoxNamespaceEO.Name = "toolStripComboBoxNamespaceEO";
      this.toolStripComboBoxNamespaceEO.Size = new Size(140, 25);
      this.toolStripComboBoxNamespaceEO.Sorted = true;
      this.toolStripComboBoxNamespaceEO.Text = "Namespace";
      this.toolStripComboBoxNamespaceEO.DropDown += new EventHandler(this.toolStripComboBoxNamespaceEO_DropDown);
      this.toolStripButtonClearEO.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonClearEO.Image = (Image) CopernicusResources.ClearSearch16x16;
      this.toolStripButtonClearEO.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonClearEO.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonClearEO.Name = "toolStripButtonClearEO";
      this.toolStripButtonClearEO.Size = new Size(23, 22);
      this.toolStripButtonClearEO.Text = "Clear Search";
      this.toolStripButtonClearEO.ToolTipText = "Clear Search";
      this.toolStripButtonClearEO.Click += new EventHandler(this.toolStripButtonClearEO_Click);
      this.toolStripButtonHelpEO.Alignment = ToolStripItemAlignment.Right;
      this.toolStripButtonHelpEO.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonHelpEO.Image = (Image) CopernicusResources._271help16x16;
      this.toolStripButtonHelpEO.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonHelpEO.Name = "toolStripButtonHelpEO";
      this.toolStripButtonHelpEO.RightToLeft = RightToLeft.No;
      this.toolStripButtonHelpEO.Size = new Size(23, 22);
      this.toolStripButtonHelpEO.Text = "Help";
      this.toolStripButtonHelpEO.Click += new EventHandler(this.toolStripButtonHelpEO_Click);
      this.tabPageDataTypes.BackColor = Color.FromArgb(188, 199, 217);
      this.tabPageDataTypes.Controls.Add((Control) this.splitContainer2);
      this.tabPageDataTypes.Controls.Add((Control) this.toolStrip3DT);
      this.tabPageDataTypes.ImageIndex = 7;
      this.tabPageDataTypes.Location = new Point(4, 23);
      this.tabPageDataTypes.Name = "tabPageDataTypes";
      this.tabPageDataTypes.Padding = new Padding(3);
      this.tabPageDataTypes.Size = new Size(784, 401);
      this.tabPageDataTypes.TabIndex = 1;
      this.tabPageDataTypes.Text = "Data Types";
      this.splitContainer2.Dock = DockStyle.Fill;
      this.splitContainer2.Location = new Point(3, 28);
      this.splitContainer2.Name = "splitContainer2";
      this.splitContainer2.Panel1.Controls.Add((Control) this.richTextBoxInfoBoxDT);
      this.splitContainer2.Panel1.Controls.Add((Control) this.treeViewDT);
      this.splitContainer2.Panel2.Controls.Add((Control) this.webBrowserDT);
      this.splitContainer2.Size = new Size(778, 370);
      this.splitContainer2.SplitterDistance = 363;
      this.splitContainer2.TabIndex = 1;
      this.richTextBoxInfoBoxDT.BackColor = SystemColors.ButtonHighlight;
      this.richTextBoxInfoBoxDT.Dock = DockStyle.Fill;
      this.richTextBoxInfoBoxDT.Location = new Point(0, 0);
      this.richTextBoxInfoBoxDT.Name = "richTextBoxInfoBoxDT";
      this.richTextBoxInfoBoxDT.ReadOnly = true;
      this.richTextBoxInfoBoxDT.Size = new Size(363, 370);
      this.richTextBoxInfoBoxDT.TabIndex = 17;
      this.richTextBoxInfoBoxDT.Text = "";
      this.richTextBoxInfoBoxDT.Visible = false;
      this.treeViewDT.Dock = DockStyle.Fill;
      this.treeViewDT.ImageIndex = 7;
      this.treeViewDT.ImageList = this.imageList1;
      this.treeViewDT.Location = new Point(0, 0);
      this.treeViewDT.Name = "treeViewDT";
      this.treeViewDT.SelectedImageIndex = 7;
      this.treeViewDT.ShowNodeToolTips = true;
      this.treeViewDT.Size = new Size(363, 370);
      this.treeViewDT.TabIndex = 0;
      this.treeViewDT.AfterSelect += new TreeViewEventHandler(this.treeViewDT_AfterSelect);
      this.treeViewDT.MouseUp += new MouseEventHandler(this.treeViewDT_MouseUp);
      this.webBrowserDT.Dock = DockStyle.Fill;
      this.webBrowserDT.Location = new Point(0, 0);
      this.webBrowserDT.MinimumSize = new Size(20, 20);
      this.webBrowserDT.Name = "webBrowserDT";
      this.webBrowserDT.ScriptErrorsSuppressed = true;
      this.webBrowserDT.Size = new Size(411, 370);
      this.webBrowserDT.TabIndex = 14;
      this.webBrowserDT.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.webBrowserDT_DocumentCompleted);
      this.webBrowserDT.Navigating += new WebBrowserNavigatingEventHandler(this.webBrowserDT_Navigating);
      this.toolStrip3DT.BackColor = Color.Transparent;
      this.toolStrip3DT.Items.AddRange(new ToolStripItem[13]
      {
        (ToolStripItem) this.toolStripButtonBackDT,
        (ToolStripItem) this.toolStripButtonNextDT,
        (ToolStripItem) this.toolStripSeparator3,
        (ToolStripItem) this.toolStripButtonHorizontalWindowsDT,
        (ToolStripItem) this.toolStripButtonVerticalWindowsDT,
        (ToolStripItem) this.toolStripSeparator4,
        (ToolStripItem) this.toolStripComboBoxSearchDT,
        (ToolStripItem) this.toolStripButtonSearchDT,
        (ToolStripItem) this.toolStripLabelFilterDT,
        (ToolStripItem) this.toolStripComboBoxNamespaceDT,
        (ToolStripItem) this.toolStripComboBoxUsageCat,
        (ToolStripItem) this.toolStripButtonClearDT,
        (ToolStripItem) this.toolStripButton1HelpDT
      });
      this.toolStrip3DT.Location = new Point(3, 3);
      this.toolStrip3DT.Name = "toolStrip3DT";
      this.toolStrip3DT.Size = new Size(778, 25);
      this.toolStrip3DT.TabIndex = 0;
      this.toolStrip3DT.Text = "toolStrip3";
      this.toolStripButtonBackDT.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonBackDT.Enabled = false;
      this.toolStripButtonBackDT.Image = (Image) CopernicusResources.GoToPreviousHS;
      this.toolStripButtonBackDT.ImageTransparentColor = Color.Black;
      this.toolStripButtonBackDT.Margin = new Padding(0, 1, 1, 2);
      this.toolStripButtonBackDT.Name = "toolStripButtonBackDT";
      this.toolStripButtonBackDT.Size = new Size(23, 22);
      this.toolStripButtonBackDT.Text = "Back";
      this.toolStripButtonBackDT.Click += new EventHandler(this.toolStripButtonBackDT_Click);
      this.toolStripButtonNextDT.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonNextDT.Enabled = false;
      this.toolStripButtonNextDT.Image = (Image) CopernicusResources.GoToNextHS;
      this.toolStripButtonNextDT.ImageTransparentColor = Color.Black;
      this.toolStripButtonNextDT.Margin = new Padding(1, 1, 1, 2);
      this.toolStripButtonNextDT.Name = "toolStripButtonNextDT";
      this.toolStripButtonNextDT.Size = new Size(23, 22);
      this.toolStripButtonNextDT.Text = "Forward";
      this.toolStripButtonNextDT.Click += new EventHandler(this.toolStripButtonNextDT_Click);
      this.toolStripSeparator3.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new Size(6, 25);
      this.toolStripButtonHorizontalWindowsDT.CheckOnClick = true;
      this.toolStripButtonHorizontalWindowsDT.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonHorizontalWindowsDT.Image = (Image) CopernicusResources._254HorizontalWindows16x16;
      this.toolStripButtonHorizontalWindowsDT.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonHorizontalWindowsDT.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonHorizontalWindowsDT.Name = "toolStripButtonHorizontalWindowsDT";
      this.toolStripButtonHorizontalWindowsDT.Size = new Size(23, 22);
      this.toolStripButtonHorizontalWindowsDT.Text = "toolStripButton3";
      this.toolStripButtonHorizontalWindowsDT.ToolTipText = "Horizontal";
      this.toolStripButtonHorizontalWindowsDT.Click += new EventHandler(this.toolStripButtonHorizontalWindowsDT_Click);
      this.toolStripButtonVerticalWindowsDT.Checked = true;
      this.toolStripButtonVerticalWindowsDT.CheckOnClick = true;
      this.toolStripButtonVerticalWindowsDT.CheckState = CheckState.Checked;
      this.toolStripButtonVerticalWindowsDT.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonVerticalWindowsDT.Image = (Image) CopernicusResources._255VerticalWindows16x16;
      this.toolStripButtonVerticalWindowsDT.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonVerticalWindowsDT.Margin = new Padding(2, 1, 1, 2);
      this.toolStripButtonVerticalWindowsDT.Name = "toolStripButtonVerticalWindowsDT";
      this.toolStripButtonVerticalWindowsDT.Size = new Size(23, 22);
      this.toolStripButtonVerticalWindowsDT.Text = "toolStripButton4";
      this.toolStripButtonVerticalWindowsDT.ToolTipText = "Vertical";
      this.toolStripButtonVerticalWindowsDT.Click += new EventHandler(this.toolStripButtonVerticalWindowsDT_Click);
      this.toolStripSeparator4.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      this.toolStripSeparator4.Size = new Size(6, 25);
      this.toolStripComboBoxSearchDT.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxSearchDT.AutoCompleteSource = AutoCompleteSource.CustomSource;
      this.toolStripComboBoxSearchDT.DropDownWidth = 140;
      this.toolStripComboBoxSearchDT.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxSearchDT.Margin = new Padding(4, 0, 4, 0);
      this.toolStripComboBoxSearchDT.MaxDropDownItems = 12;
      this.toolStripComboBoxSearchDT.Name = "toolStripComboBoxSearchDT";
      this.toolStripComboBoxSearchDT.Size = new Size(150, 25);
      this.toolStripComboBoxSearchDT.Sorted = true;
      this.toolStripComboBoxSearchDT.Text = "Search data types";
      this.toolStripComboBoxSearchDT.DropDown += new EventHandler(this.toolStripComboBoxSearchDT_DropDown);
      this.toolStripButtonSearchDT.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonSearchDT.Image = (Image) CopernicusResources.View1;
      this.toolStripButtonSearchDT.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonSearchDT.Margin = new Padding(4, 1, 2, 2);
      this.toolStripButtonSearchDT.Name = "toolStripButtonSearchDT";
      this.toolStripButtonSearchDT.Size = new Size(23, 22);
      this.toolStripButtonSearchDT.Text = "Search";
      this.toolStripButtonSearchDT.Click += new EventHandler(this.toolStripButtonSearchDT_Click);
      this.toolStripLabelFilterDT.Margin = new Padding(2, 1, 2, 2);
      this.toolStripLabelFilterDT.Name = "toolStripLabelFilterDT";
      this.toolStripLabelFilterDT.Size = new Size(52, 22);
      this.toolStripLabelFilterDT.Text = "Filter by:";
      this.toolStripComboBoxNamespaceDT.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxNamespaceDT.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.toolStripComboBoxNamespaceDT.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxNamespaceDT.IntegralHeight = false;
      this.toolStripComboBoxNamespaceDT.Margin = new Padding(4, 0, 4, 0);
      this.toolStripComboBoxNamespaceDT.MaxDropDownItems = 15;
      this.toolStripComboBoxNamespaceDT.Name = "toolStripComboBoxNamespaceDT";
      this.toolStripComboBoxNamespaceDT.Size = new Size(140, 25);
      this.toolStripComboBoxNamespaceDT.Sorted = true;
      this.toolStripComboBoxNamespaceDT.Text = "Namespace";
      this.toolStripComboBoxNamespaceDT.DropDown += new EventHandler(this.toolStripComboBoxNamespaceDT_DropDown);
      this.toolStripComboBoxUsageCat.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxUsageCat.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.toolStripComboBoxUsageCat.DropDownWidth = 140;
      this.toolStripComboBoxUsageCat.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxUsageCat.Items.AddRange(new object[12]
      {
        (object) "Action",
        (object) "Code Data Type Context",
        (object) "Core",
        (object) "Enhancement Option",
        (object) "Filter",
        (object) "Form Message",
        (object) "Form Message Intermediate",
        (object) "Key",
        (object) "Message",
        (object) "Message Intermediate",
        (object) "Message Unrestricted",
        (object) "Unrestricted"
      });
      this.toolStripComboBoxUsageCat.Margin = new Padding(4, 0, 4, 0);
      this.toolStripComboBoxUsageCat.MaxDropDownItems = 12;
      this.toolStripComboBoxUsageCat.Name = "toolStripComboBoxUsageCat";
      this.toolStripComboBoxUsageCat.Size = new Size(140, 25);
      this.toolStripComboBoxUsageCat.Sorted = true;
      this.toolStripComboBoxUsageCat.Text = "Usage Category";
      this.toolStripComboBoxUsageCat.DropDown += new EventHandler(this.toolStripComboBoxUsageCat_DropDown);
      this.toolStripButtonClearDT.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonClearDT.Image = (Image) CopernicusResources.ClearSearch16x16;
      this.toolStripButtonClearDT.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonClearDT.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonClearDT.Name = "toolStripButtonClearDT";
      this.toolStripButtonClearDT.Size = new Size(23, 22);
      this.toolStripButtonClearDT.Text = "Clear Search";
      this.toolStripButtonClearDT.ToolTipText = "Clear Search";
      this.toolStripButtonClearDT.Click += new EventHandler(this.toolStripButtonClearDT_Click);
      this.toolStripButton1HelpDT.Alignment = ToolStripItemAlignment.Right;
      this.toolStripButton1HelpDT.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButton1HelpDT.Image = (Image) CopernicusResources._271help16x16;
      this.toolStripButton1HelpDT.ImageTransparentColor = Color.Magenta;
      this.toolStripButton1HelpDT.Name = "toolStripButton1HelpDT";
      this.toolStripButton1HelpDT.RightToLeft = RightToLeft.No;
      this.toolStripButton1HelpDT.Size = new Size(23, 22);
      this.toolStripButton1HelpDT.Text = "Help";
      this.toolStripButton1HelpDT.Click += new EventHandler(this.toolStripButton1HelpDT_Click);
      this.tabPageBusinessObjectBrowser.BackColor = Color.FromArgb(188, 199, 217);
      this.tabPageBusinessObjectBrowser.Controls.Add((Control) this.splitContainer1);
      this.tabPageBusinessObjectBrowser.Controls.Add((Control) this.toolStrip2BO);
      this.tabPageBusinessObjectBrowser.Controls.Add((Control) this.toolStrip1BO);
      this.tabPageBusinessObjectBrowser.ImageIndex = 0;
      this.tabPageBusinessObjectBrowser.Location = new Point(4, 23);
      this.tabPageBusinessObjectBrowser.Name = "tabPageBusinessObjectBrowser";
      this.tabPageBusinessObjectBrowser.Padding = new Padding(3);
      this.tabPageBusinessObjectBrowser.Size = new Size(784, 401);
      this.tabPageBusinessObjectBrowser.TabIndex = 0;
      this.tabPageBusinessObjectBrowser.Text = "Business Objects";
      this.splitContainer1.Dock = DockStyle.Fill;
      this.splitContainer1.Location = new Point(3, 53);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Panel1.Controls.Add((Control) this.richTextBoxInfoBoxBO);
      this.splitContainer1.Panel1.Controls.Add((Control) this.treeViewBO);
      this.splitContainer1.Panel2.Controls.Add((Control) this.tabControlBO);
      this.splitContainer1.Size = new Size(778, 345);
      this.splitContainer1.SplitterDistance = 216;
      this.splitContainer1.TabIndex = 14;
      this.richTextBoxInfoBoxBO.BackColor = SystemColors.ButtonHighlight;
      this.richTextBoxInfoBoxBO.Dock = DockStyle.Fill;
      this.richTextBoxInfoBoxBO.Location = new Point(0, 0);
      this.richTextBoxInfoBoxBO.Name = "richTextBoxInfoBoxBO";
      this.richTextBoxInfoBoxBO.ReadOnly = true;
      this.richTextBoxInfoBoxBO.Size = new Size(216, 345);
      this.richTextBoxInfoBoxBO.TabIndex = 16;
      this.richTextBoxInfoBoxBO.Text = "";
      this.richTextBoxInfoBoxBO.Visible = false;
      this.treeViewBO.Dock = DockStyle.Fill;
      this.treeViewBO.ImageIndex = 0;
      this.treeViewBO.ImageList = this.imageList1;
      this.treeViewBO.Location = new Point(0, 0);
      this.treeViewBO.Name = "treeViewBO";
      this.treeViewBO.PathSeparator = ".";
      this.treeViewBO.SelectedImageIndex = 0;
      this.treeViewBO.ShowNodeToolTips = true;
      this.treeViewBO.Size = new Size(216, 345);
      this.treeViewBO.TabIndex = 14;
      this.treeViewBO.AfterCollapse += new TreeViewEventHandler(this.treeViewBO_AfterCollapse);
      this.treeViewBO.AfterExpand += new TreeViewEventHandler(this.treeViewBO_AfterExpand);
      this.treeViewBO.AfterSelect += new TreeViewEventHandler(this.treeViewBO_AfterSelect);
      this.treeViewBO.MouseUp += new MouseEventHandler(this.treeViewBO_MouseUp);
      this.tabControlBO.Controls.Add((Control) this.tabPageDescription);
      this.tabControlBO.Dock = DockStyle.Fill;
      this.tabControlBO.Location = new Point(0, 0);
      this.tabControlBO.Name = "tabControlBO";
      this.tabControlBO.SelectedIndex = 0;
      this.tabControlBO.Size = new Size(558, 345);
      this.tabControlBO.TabIndex = 14;
      this.tabPageDescription.Controls.Add((Control) this.webBrowserBO);
      this.tabPageDescription.Location = new Point(4, 22);
      this.tabPageDescription.Name = "tabPageDescription";
      this.tabPageDescription.Padding = new Padding(3);
      this.tabPageDescription.Size = new Size(550, 319);
      this.tabPageDescription.TabIndex = 0;
      this.tabPageDescription.Text = "Description";
      this.tabPageDescription.UseVisualStyleBackColor = true;
      this.webBrowserBO.Dock = DockStyle.Fill;
      this.webBrowserBO.Location = new Point(3, 3);
      this.webBrowserBO.MinimumSize = new Size(20, 20);
      this.webBrowserBO.Name = "webBrowserBO";
      this.webBrowserBO.ScriptErrorsSuppressed = true;
      this.webBrowserBO.Size = new Size(544, 313);
      this.webBrowserBO.TabIndex = 14;
      this.toolStrip2BO.AllowItemReorder = true;
      this.toolStrip2BO.BackColor = Color.FromArgb(188, 199, 217);
      this.toolStrip2BO.Items.AddRange(new ToolStripItem[7]
      {
        (ToolStripItem) this.toolStripComboBoxSearch,
        (ToolStripItem) this.toolStripButtonSearch,
        (ToolStripItem) this.toolStripLabelFilterBO,
        (ToolStripItem) this.toolStripComboBoxNamespace,
        (ToolStripItem) this.toolStripComboBoxDepUnit,
        (ToolStripItem) this.toolStripButtonClear,
        (ToolStripItem) this.toolStripButtonHelpBO
      });
      this.toolStrip2BO.Location = new Point(3, 28);
      this.toolStrip2BO.Name = "toolStrip2BO";
      this.toolStrip2BO.Size = new Size(778, 25);
      this.toolStrip2BO.TabIndex = 10;
      this.toolStrip2BO.Text = "toolStrip2";
      this.toolStripComboBoxSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
      this.toolStripComboBoxSearch.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxSearch.IntegralHeight = false;
      this.toolStripComboBoxSearch.Margin = new Padding(1, 0, 4, 0);
      this.toolStripComboBoxSearch.MaxDropDownItems = 12;
      this.toolStripComboBoxSearch.Name = "toolStripComboBoxSearch";
      this.toolStripComboBoxSearch.Size = new Size(150, 25);
      this.toolStripComboBoxSearch.Sorted = true;
      this.toolStripComboBoxSearch.Text = "Search business objects";
      this.toolStripButtonSearch.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonSearch.Image = (Image) CopernicusResources.View1;
      this.toolStripButtonSearch.ImageTransparentColor = Color.Black;
      this.toolStripButtonSearch.Margin = new Padding(4, 1, 2, 2);
      this.toolStripButtonSearch.Name = "toolStripButtonSearch";
      this.toolStripButtonSearch.Size = new Size(23, 22);
      this.toolStripButtonSearch.Text = "Search";
      this.toolStripButtonSearch.Click += new EventHandler(this.toolStripButtonSearch_Click);
      this.toolStripLabelFilterBO.Margin = new Padding(2, 1, 2, 2);
      this.toolStripLabelFilterBO.Name = "toolStripLabelFilterBO";
      this.toolStripLabelFilterBO.Size = new Size(52, 22);
      this.toolStripLabelFilterBO.Text = "Filter by:";
      this.toolStripComboBoxNamespace.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxNamespace.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.toolStripComboBoxNamespace.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxNamespace.IntegralHeight = false;
      this.toolStripComboBoxNamespace.Margin = new Padding(4, 0, 4, 0);
      this.toolStripComboBoxNamespace.MaxDropDownItems = 15;
      this.toolStripComboBoxNamespace.Name = "toolStripComboBoxNamespace";
      this.toolStripComboBoxNamespace.Size = new Size(140, 25);
      this.toolStripComboBoxNamespace.Sorted = true;
      this.toolStripComboBoxNamespace.Text = "Namespace";
      this.toolStripComboBoxNamespace.DropDown += new EventHandler(this.toolStripComboBoxNamespace_DropDown);
      this.toolStripComboBoxDepUnit.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.toolStripComboBoxDepUnit.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.toolStripComboBoxDepUnit.DropDownWidth = 140;
      this.toolStripComboBoxDepUnit.FlatStyle = FlatStyle.Standard;
      this.toolStripComboBoxDepUnit.IntegralHeight = false;
      this.toolStripComboBoxDepUnit.Margin = new Padding(4, 0, 4, 0);
      this.toolStripComboBoxDepUnit.MaxDropDownItems = 12;
      this.toolStripComboBoxDepUnit.Name = "toolStripComboBoxDepUnit";
      this.toolStripComboBoxDepUnit.Size = new Size(140, 25);
      this.toolStripComboBoxDepUnit.Sorted = true;
      this.toolStripComboBoxDepUnit.Text = "Deployment Unit";
      this.toolStripComboBoxDepUnit.DropDown += new EventHandler(this.toolStripComboBoxDepUnit_DropDown);
      this.toolStripButtonClear.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonClear.Image = (Image) CopernicusResources.ClearSearch16x16;
      this.toolStripButtonClear.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonClear.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonClear.Name = "toolStripButtonClear";
      this.toolStripButtonClear.Size = new Size(23, 22);
      this.toolStripButtonClear.Text = "Clear Search";
      this.toolStripButtonClear.ToolTipText = "Clear Search";
      this.toolStripButtonClear.Click += new EventHandler(this.toolStripButtonClear_Click);
      this.toolStripButtonHelpBO.Alignment = ToolStripItemAlignment.Right;
      this.toolStripButtonHelpBO.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonHelpBO.Image = (Image) CopernicusResources._271help16x16;
      this.toolStripButtonHelpBO.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonHelpBO.Name = "toolStripButtonHelpBO";
      this.toolStripButtonHelpBO.RightToLeft = RightToLeft.No;
      this.toolStripButtonHelpBO.Size = new Size(23, 22);
      this.toolStripButtonHelpBO.Text = "Help";
      this.toolStripButtonHelpBO.Click += new EventHandler(this.toolStripButtonHelpBO_Click);
      this.toolStrip1BO.BackColor = Color.FromArgb(188, 199, 217);
      this.toolStrip1BO.Items.AddRange(new ToolStripItem[11]
      {
        (ToolStripItem) this.toolStripButtonBack,
        (ToolStripItem) this.toolStripButtonNext,
        (ToolStripItem) this.toolStripSeparator2,
        (ToolStripItem) this.toolStripButtonActions,
        (ToolStripItem) this.toolStripButtonAssociations,
        (ToolStripItem) this.toolStripButtonDeprecation,
        (ToolStripItem) this.toolStripButtonNodes,
        (ToolStripItem) this.toolStripButtonQueries,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.toolStripButtonHorizontalWindows,
        (ToolStripItem) this.toolStripButtonVerticalWindows
      });
      this.toolStrip1BO.Location = new Point(3, 3);
      this.toolStrip1BO.Name = "toolStrip1BO";
      this.toolStrip1BO.Size = new Size(778, 25);
      this.toolStrip1BO.Stretch = true;
      this.toolStrip1BO.TabIndex = 9;
      this.toolStrip1BO.Text = "toolStrip1";
      this.toolStripButtonBack.BackColor = Color.Transparent;
      this.toolStripButtonBack.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonBack.Enabled = false;
      this.toolStripButtonBack.ForeColor = SystemColors.ControlText;
      this.toolStripButtonBack.Image = (Image) CopernicusResources.GoToPreviousHS;
      this.toolStripButtonBack.ImageTransparentColor = Color.Black;
      this.toolStripButtonBack.Margin = new Padding(0, 1, 1, 2);
      this.toolStripButtonBack.Name = "toolStripButtonBack";
      this.toolStripButtonBack.Size = new Size(23, 22);
      this.toolStripButtonBack.Text = "Back";
      this.toolStripButtonBack.Click += new EventHandler(this.toolStripButtonBack_Click);
      this.toolStripButtonNext.BackColor = Color.Transparent;
      this.toolStripButtonNext.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonNext.Enabled = false;
      this.toolStripButtonNext.Image = (Image) CopernicusResources.GoToNextHS;
      this.toolStripButtonNext.ImageTransparentColor = Color.Black;
      this.toolStripButtonNext.Margin = new Padding(1, 1, 1, 2);
      this.toolStripButtonNext.Name = "toolStripButtonNext";
      this.toolStripButtonNext.Size = new Size(23, 22);
      this.toolStripButtonNext.Text = "Forward";
      this.toolStripButtonNext.Click += new EventHandler(this.toolStripButtonNext_Click);
      this.toolStripSeparator2.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new Size(6, 25);
      this.toolStripButtonActions.BackColor = Color.Transparent;
      this.toolStripButtonActions.Checked = true;
      this.toolStripButtonActions.CheckOnClick = true;
      this.toolStripButtonActions.CheckState = CheckState.Checked;
      this.toolStripButtonActions.Image = (Image) CopernicusResources.Action;
      this.toolStripButtonActions.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonActions.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonActions.Name = "toolStripButtonActions";
      this.toolStripButtonActions.Size = new Size(67, 22);
      this.toolStripButtonActions.Text = "Actions";
      this.toolStripButtonActions.ToolTipText = "Show/Hide Actions";
      this.toolStripButtonActions.Click += new EventHandler(this.toolStripButtonActions_Click);
      this.toolStripButtonAssociations.Checked = true;
      this.toolStripButtonAssociations.CheckOnClick = true;
      this.toolStripButtonAssociations.CheckState = CheckState.Checked;
      this.toolStripButtonAssociations.Image = (Image) CopernicusResources._22Association16x16;
      this.toolStripButtonAssociations.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonAssociations.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonAssociations.Name = "toolStripButtonAssociations";
      this.toolStripButtonAssociations.Size = new Size(93, 22);
      this.toolStripButtonAssociations.Text = "Associations";
      this.toolStripButtonAssociations.ToolTipText = "Show/Hide Associations";
      this.toolStripButtonAssociations.Click += new EventHandler(this.toolStripButtonAssociations_Click);
      this.toolStripButtonDeprecation.Checked = true;
      this.toolStripButtonDeprecation.CheckOnClick = true;
      this.toolStripButtonDeprecation.CheckState = CheckState.Checked;
      this.toolStripButtonDeprecation.Image = (Image) CopernicusResources._180Warning;
      this.toolStripButtonDeprecation.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonDeprecation.Margin = new Padding(2, 1, 1, 2);
      this.toolStripButtonDeprecation.Name = "toolStripButtonDeprecation";
      this.toolStripButtonDeprecation.Size = new Size(91, 22);
      this.toolStripButtonDeprecation.Text = "Deprecation";
      this.toolStripButtonDeprecation.ToolTipText = "Show/Hide Deprecated Entities";
      this.toolStripButtonDeprecation.Visible = false;
      this.toolStripButtonDeprecation.Click += new EventHandler(this.toolStripButtonDeprecation_Click);
      this.toolStripButtonNodes.Checked = true;
      this.toolStripButtonNodes.CheckOnClick = true;
      this.toolStripButtonNodes.CheckState = CheckState.Checked;
      this.toolStripButtonNodes.Image = (Image) CopernicusResources._21BONode_16x16;
      this.toolStripButtonNodes.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonNodes.Margin = new Padding(2, 1, 1, 2);
      this.toolStripButtonNodes.Name = "toolStripButtonNodes";
      this.toolStripButtonNodes.Size = new Size(61, 22);
      this.toolStripButtonNodes.Text = "Nodes";
      this.toolStripButtonNodes.ToolTipText = "Show/Hide Nodes";
      this.toolStripButtonNodes.Click += new EventHandler(this.toolStripButtonNodes_Click);
      this.toolStripButtonQueries.BackColor = Color.Transparent;
      this.toolStripButtonQueries.Checked = true;
      this.toolStripButtonQueries.CheckOnClick = true;
      this.toolStripButtonQueries.CheckState = CheckState.Checked;
      this.toolStripButtonQueries.Image = (Image) CopernicusResources.Query;
      this.toolStripButtonQueries.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonQueries.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonQueries.Name = "toolStripButtonQueries";
      this.toolStripButtonQueries.Size = new Size(67, 22);
      this.toolStripButtonQueries.Text = "Queries";
      this.toolStripButtonQueries.ToolTipText = "Show/Hide Queries";
      this.toolStripButtonQueries.Click += new EventHandler(this.toolStripButtonQueries_Click);
      this.toolStripSeparator1.BackColor = Color.Black;
      this.toolStripSeparator1.ForeColor = SystemColors.Control;
      this.toolStripSeparator1.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new Size(6, 25);
      this.toolStripButtonHorizontalWindows.BackColor = Color.Transparent;
      this.toolStripButtonHorizontalWindows.CheckOnClick = true;
      this.toolStripButtonHorizontalWindows.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonHorizontalWindows.Image = (Image) CopernicusResources._254HorizontalWindows16x16;
      this.toolStripButtonHorizontalWindows.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonHorizontalWindows.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonHorizontalWindows.Name = "toolStripButtonHorizontalWindows";
      this.toolStripButtonHorizontalWindows.Size = new Size(23, 22);
      this.toolStripButtonHorizontalWindows.Text = "Horizontal Windows";
      this.toolStripButtonHorizontalWindows.ToolTipText = "Horizontal";
      this.toolStripButtonHorizontalWindows.Click += new EventHandler(this.toolStripButtonHorizontalWindows_Click);
      this.toolStripButtonVerticalWindows.BackColor = Color.Transparent;
      this.toolStripButtonVerticalWindows.Checked = true;
      this.toolStripButtonVerticalWindows.CheckOnClick = true;
      this.toolStripButtonVerticalWindows.CheckState = CheckState.Checked;
      this.toolStripButtonVerticalWindows.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonVerticalWindows.Image = (Image) CopernicusResources._255VerticalWindows16x16;
      this.toolStripButtonVerticalWindows.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonVerticalWindows.Margin = new Padding(2, 1, 2, 2);
      this.toolStripButtonVerticalWindows.Name = "toolStripButtonVerticalWindows";
      this.toolStripButtonVerticalWindows.Size = new Size(23, 22);
      this.toolStripButtonVerticalWindows.Text = "Vertical Windows";
      this.toolStripButtonVerticalWindows.ToolTipText = "Vertical";
      this.toolStripButtonVerticalWindows.Click += new EventHandler(this.toolStripButtonVerticalWindows_Click);
      this.tabControl1.Controls.Add((Control) this.tabPageBusinessObjectBrowser);
      this.tabControl1.Controls.Add((Control) this.tabPageDataTypes);
      this.tabControl1.Controls.Add((Control) this.tabPageEnhancementOptions);
      this.tabControl1.Controls.Add((Control) this.tabPageInboundServiceInterfaces);
      this.tabControl1.Controls.Add((Control) this.tabReuseUIs);
      this.tabControl1.Dock = DockStyle.Fill;
      this.tabControl1.ImageList = this.imageList1;
      this.tabControl1.Location = new Point(0, 0);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new Size(792, 428);
      this.tabControl1.TabIndex = 15;
      this.tabControl1.Selected += new TabControlEventHandler(this.tabSelected);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = Color.FromArgb(188, 199, 217);
      this.BackgroundImageLayout = ImageLayout.None;
      this.Controls.Add((Control) this.tabControl1);
      this.Name = nameof (CopernicusBusinessObjectBrowser);
      this.Size = new Size(792, 428);
      this.Load += new EventHandler(this.copyNamespaceToolStripMenuItem_Click);
      this.contextMenuStripSUBelement.ResumeLayout(false);
      this.contextMenuStripFirstLevel.ResumeLayout(false);
      this.contextMenuStripOP.ResumeLayout(false);
      this.contextMenuStripQuery.ResumeLayout(false);
      this.tabReuseUIs.ResumeLayout(false);
      this.tabReuseUIs.PerformLayout();
      this.splitContainer5.Panel1.ResumeLayout(false);
      this.splitContainer5.Panel2.ResumeLayout(false);
      this.splitContainer5.EndInit();
      this.splitContainer5.ResumeLayout(false);
      this.tabControl2.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.tabPageInboundServiceInterfaces.ResumeLayout(false);
      this.tabPageInboundServiceInterfaces.PerformLayout();
      this.splitContainer4.Panel1.ResumeLayout(false);
      this.splitContainer4.Panel2.ResumeLayout(false);
      this.splitContainer4.EndInit();
      this.splitContainer4.ResumeLayout(false);
      this.toolStrip5ISI.ResumeLayout(false);
      this.toolStrip5ISI.PerformLayout();
      this.tabPageEnhancementOptions.ResumeLayout(false);
      this.tabPageEnhancementOptions.PerformLayout();
      this.splitContainer3.Panel1.ResumeLayout(false);
      this.splitContainer3.Panel2.ResumeLayout(false);
      this.splitContainer3.EndInit();
      this.splitContainer3.ResumeLayout(false);
      this.toolStrip4EO.ResumeLayout(false);
      this.toolStrip4EO.PerformLayout();
      this.tabPageDataTypes.ResumeLayout(false);
      this.tabPageDataTypes.PerformLayout();
      this.splitContainer2.Panel1.ResumeLayout(false);
      this.splitContainer2.Panel2.ResumeLayout(false);
      this.splitContainer2.EndInit();
      this.splitContainer2.ResumeLayout(false);
      this.toolStrip3DT.ResumeLayout(false);
      this.toolStrip3DT.PerformLayout();
      this.tabPageBusinessObjectBrowser.ResumeLayout(false);
      this.tabPageBusinessObjectBrowser.PerformLayout();
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.tabControlBO.ResumeLayout(false);
      this.tabPageDescription.ResumeLayout(false);
      this.toolStrip2BO.ResumeLayout(false);
      this.toolStrip2BO.PerformLayout();
      this.toolStrip1BO.ResumeLayout(false);
      this.toolStrip1BO.PerformLayout();
      this.tabControl1.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    private class NewRenderer : ToolStripProfessionalRenderer
    {
      protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e)
      {
        SolidBrush solidBrush1 = new SolidBrush(ColorTranslator.FromHtml("#ffefbb"));
        SolidBrush solidBrush2 = new SolidBrush(ColorTranslator.FromHtml("#e5c365"));
        ToolStripButton toolStripButton = e.Item as ToolStripButton;
        if (toolStripButton != null && toolStripButton.CheckOnClick && toolStripButton.Checked)
        {
          Size size = new Size(e.Item.Size.Width - 2, e.Item.Size.Height - 2);
          Point location = new Point(1, 1);
          Rectangle rect1 = new Rectangle(Point.Empty, e.Item.Size);
          e.Graphics.FillRectangle((Brush) solidBrush2, rect1);
          Rectangle rect2 = new Rectangle(location, size);
          e.Graphics.FillRectangle((Brush) solidBrush1, rect2);
        }
        else
          base.OnRenderButtonBackground(e);
      }
    }
  }
}
