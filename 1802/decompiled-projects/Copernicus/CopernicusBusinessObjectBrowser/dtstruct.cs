﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.dtstruct
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

namespace CopernicusBusinessObjectBrowser
{
  public struct dtstruct
  {
    public string dtproxyname;
    public string dtname;
    public string dtnamespace;
    public string dtusage;
  }
}
