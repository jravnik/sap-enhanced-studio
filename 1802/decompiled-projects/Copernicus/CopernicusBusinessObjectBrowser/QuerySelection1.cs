﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.QuerySelection1
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Modeling;
using SAP.Copernicus.Core;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.MassDataRun;
using SAP.Copernicus.Model;
using SAP.Copernicus.Model.BusinessObject;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace CopernicusBusinessObjectBrowser
{
  public class QuerySelection1 : BaseForm
  {
    public static char isSensitive = ' ';
    public static string[] sensitiveElements = (string[]) null;
    private List<string[]> queryResult = new List<string[]>();
    private List<ValueHelp> valueHelpBuffer = new List<ValueHelp>();
    public List<SESF_SELECTION_PARAMETER> selectionParametersForABSL = new List<SESF_SELECTION_PARAMETER>();
    private new IContainer components;
    private DataGridView dataGridView1;
    private SplitContainer splitContainer1;
    private SplitContainer splitContainer2;
    private Button buttonCount;
    private Label label1;
    private TextBox textBoxMaxRows;
    private Button button_pasteValues;
    private Button button_remove;
    private Button button_add;
    private Label label_numberOfRecords;
    private BusinessObject_QueryParameter[] parameters;
    public BusinessObject_Query query;
    public bool cancelled;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.dataGridView1 = new DataGridView();
      this.splitContainer1 = new SplitContainer();
      this.label_numberOfRecords = new Label();
      this.button_pasteValues = new Button();
      this.button_remove = new Button();
      this.button_add = new Button();
      this.label1 = new Label();
      this.textBoxMaxRows = new TextBox();
      this.buttonCount = new Button();
      this.splitContainer2 = new SplitContainer();
      ((ISupportInitialize) this.dataGridView1).BeginInit();
      this.splitContainer1.BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.splitContainer2.BeginInit();
      this.splitContainer2.Panel1.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      this.SuspendLayout();
      this.buttonCancel.Location = new Point(875, 500);
      this.buttonOk.Location = new Point(690, 500);
      this.dataGridView1.Dock = DockStyle.Fill;
      this.dataGridView1.Location = new Point(0, 0);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.Size = new Size(962, 353);
      this.dataGridView1.TabIndex = 0;
      this.dataGridView1.CellEnter += new DataGridViewCellEventHandler(this.dataGridView1_CellEnter);
      this.dataGridView1.CellLeave += new DataGridViewCellEventHandler(this.dataGridView1_CellLeave);
      this.dataGridView1.CellValidating += new DataGridViewCellValidatingEventHandler(this.dataGridView1_CellValidating);
      this.dataGridView1.DataError += new DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
      this.dataGridView1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
      this.dataGridView1.SelectionChanged += new EventHandler(this.dataGridView1_SelectionChanged);
      this.splitContainer1.BackColor = Color.Transparent;
      this.splitContainer1.Dock = DockStyle.Fill;
      this.splitContainer1.Location = new Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = Orientation.Horizontal;
      this.splitContainer1.Panel1.Controls.Add((Control) this.label_numberOfRecords);
      this.splitContainer1.Panel1.Controls.Add((Control) this.button_pasteValues);
      this.splitContainer1.Panel1.Controls.Add((Control) this.button_remove);
      this.splitContainer1.Panel1.Controls.Add((Control) this.button_add);
      this.splitContainer1.Panel1.Controls.Add((Control) this.label1);
      this.splitContainer1.Panel1.Controls.Add((Control) this.textBoxMaxRows);
      this.splitContainer1.Panel1.Controls.Add((Control) this.buttonCount);
      this.splitContainer1.Panel2.Controls.Add((Control) this.splitContainer2);
      this.splitContainer1.Size = new Size(962, 535);
      this.splitContainer1.SplitterDistance = 97;
      this.splitContainer1.TabIndex = 4;
      this.label_numberOfRecords.AutoSize = true;
      this.label_numberOfRecords.Location = new Point(891, 77);
      this.label_numberOfRecords.Name = "label_numberOfRecords";
      this.label_numberOfRecords.Size = new Size(16, 13);
      this.label_numberOfRecords.TabIndex = 11;
      this.label_numberOfRecords.Text = "...";
      this.button_pasteValues.Enabled = false;
      this.button_pasteValues.Location = new Point(125, 68);
      this.button_pasteValues.Name = "button_pasteValues";
      this.button_pasteValues.Size = new Size(149, 23);
      this.button_pasteValues.TabIndex = 10;
      this.button_pasteValues.Text = "Paste Values from Clipboard";
      this.button_pasteValues.UseVisualStyleBackColor = true;
      this.button_pasteValues.Click += new EventHandler(this.button_pasteValues_Click);
      this.button_remove.Location = new Point(62, 68);
      this.button_remove.Name = "button_remove";
      this.button_remove.Size = new Size(57, 23);
      this.button_remove.TabIndex = 9;
      this.button_remove.Text = "Remove";
      this.button_remove.UseVisualStyleBackColor = true;
      this.button_remove.Click += new EventHandler(this.button_remove_Click);
      this.button_add.Location = new Point(3, 68);
      this.button_add.Name = "button_add";
      this.button_add.Size = new Size(53, 23);
      this.button_add.TabIndex = 8;
      this.button_add.Text = "Add";
      this.button_add.UseVisualStyleBackColor = true;
      this.button_add.Click += new EventHandler(this.button_add_Click);
      this.label1.AutoSize = true;
      this.label1.Location = new Point(427, 76);
      this.label1.Name = "label1";
      this.label1.Size = new Size(149, 13);
      this.label1.TabIndex = 7;
      this.label1.Text = "Maximum Number of Records:";
      this.textBoxMaxRows.Location = new Point(582, 75);
      this.textBoxMaxRows.Name = "textBoxMaxRows";
      this.textBoxMaxRows.Size = new Size(59, 20);
      this.textBoxMaxRows.TabIndex = 6;
      this.textBoxMaxRows.Text = "20";
      this.buttonCount.Location = new Point(666, 72);
      this.buttonCount.Name = "buttonCount";
      this.buttonCount.Size = new Size(219, 23);
      this.buttonCount.TabIndex = 5;
      this.buttonCount.Text = "Available Number of Records for Selection";
      this.buttonCount.UseVisualStyleBackColor = true;
      this.buttonCount.Click += new EventHandler(this.buttonCount_Click);
      this.splitContainer2.Dock = DockStyle.Fill;
      this.splitContainer2.Location = new Point(0, 0);
      this.splitContainer2.Name = "splitContainer2";
      this.splitContainer2.Orientation = Orientation.Horizontal;
      this.splitContainer2.Panel1.Controls.Add((Control) this.dataGridView1);
      this.splitContainer2.Size = new Size(962, 434);
      this.splitContainer2.SplitterDistance = 353;
      this.splitContainer2.TabIndex = 1;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BannerVisible = true;
      this.ClientSize = new Size(962, 535);
      this.Controls.Add((Control) this.splitContainer1);
      this.HelpButton = true;
      this.Name = nameof (QuerySelection1);
      this.Subtitle = "Enter selection parameters for the query execution.";
      this.Text = "Query Parameter Selection";
      this.KeyUp += new KeyEventHandler(this.QuerySelection1_KeyUp);
      this.Controls.SetChildIndex((Control) this.splitContainer1, 0);
      this.Controls.SetChildIndex((Control) this.buttonOk, 0);
      this.Controls.SetChildIndex((Control) this.buttonCancel, 0);
      ((ISupportInitialize) this.dataGridView1).EndInit();
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel1.PerformLayout();
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.splitContainer2.Panel1.ResumeLayout(false);
      this.splitContainer2.EndInit();
      this.splitContainer2.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public QuerySelection1(BusinessObject_Query query)
      : base(HELP_IDS.BDS_QUERY_EXECUTE)
    {
      this.getQueryParameters(query);
      this.InitializeComponent();
      this.query = query;
      if (this.parameters != null)
      {
        this.sortQueryParameters();
        this.setupDataGridView();
      }
      this.splitContainer1.SendToBack();
      this.HelpID = HELP_IDS.BDS_QUERY_EXECUTE;
      this.buttonOk.Location = new Point(this.buttonOk.Location.X + 80, this.buttonOk.Location.Y);
    }

    public QuerySelection1(BusinessObject_Query query, DataGridView preSelectionDatagridView)
      : base(HELP_IDS.BDS_QUERY_EXECUTE)
    {
      this.getQueryParameters(query);
      this.InitializeComponent();
      this.query = query;
      this.sortQueryParameters();
      this.splitContainer1.SendToBack();
      this.HelpID = HELP_IDS.BDS_QUERY_EXECUTE;
      this.buttonOk.Location = new Point(this.buttonOk.Location.X + 80, this.buttonOk.Location.Y);
      this.dataGridView1 = preSelectionDatagridView;
    }

    private void sortQueryParameters()
    {
      ((IEnumerable<BusinessObject_QueryParameter>) this.parameters).OrderBy<BusinessObject_QueryParameter, int>((Func<BusinessObject_QueryParameter, int>) (item => item.OrdinalNumberValue));
    }

    public List<string[]> ShowDialog()
    {
      if (this.parameters == null)
      {
        this.textBoxMaxRows.Text = "";
        this.queryResult = this.executeQuery(false);
        this.Close();
      }
      else
      {
        int num = (int) base.ShowDialog();
      }
      return this.queryResult;
    }

    private void setupDataGridView()
    {
      this.dataGridView1.EnableHeadersVisualStyles = false;
      this.dataGridView1.ColumnHeadersHeight = 30;
      string[] array = ((IEnumerable<BusinessObject_QueryParameter>) this.parameters).Select<BusinessObject_QueryParameter, string>((Func<BusinessObject_QueryParameter, string>) (item => item.Name)).ToArray<string>();
      DataGridViewComboBoxColumn viewComboBoxColumn1 = new DataGridViewComboBoxColumn();
      viewComboBoxColumn1.HeaderText = "Parameter";
      viewComboBoxColumn1.Items.AddRange((object[]) array);
      viewComboBoxColumn1.Width = 350;
      viewComboBoxColumn1.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
      viewComboBoxColumn1.Resizable = DataGridViewTriState.True;
      this.dataGridView1.Columns.Add((DataGridViewColumn) viewComboBoxColumn1);
      DataGridViewComboBoxColumn viewComboBoxColumn2 = new DataGridViewComboBoxColumn();
      viewComboBoxColumn2.HeaderText = "Include/\nExlude ";
      viewComboBoxColumn2.MaxDropDownItems = 2;
      viewComboBoxColumn2.Items.Add((object) "Include");
      viewComboBoxColumn2.Items.Add((object) "Exclude");
      viewComboBoxColumn2.Resizable = DataGridViewTriState.True;
      viewComboBoxColumn2.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
      this.dataGridView1.Columns.Add((DataGridViewColumn) viewComboBoxColumn2);
      DataGridViewComboBoxColumn viewComboBoxColumn3 = new DataGridViewComboBoxColumn();
      viewComboBoxColumn3.HeaderText = "Option";
      viewComboBoxColumn3.MaxDropDownItems = CompareOperands.getAll().Length;
      viewComboBoxColumn3.Items.AddRange((object[]) CompareOperands.getAll());
      viewComboBoxColumn3.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
      viewComboBoxColumn3.Resizable = DataGridViewTriState.True;
      this.dataGridView1.Columns.Add((DataGridViewColumn) viewComboBoxColumn3);
      DataGridViewColumn dataGridViewColumn1 = new DataGridViewColumn();
      dataGridViewColumn1.HeaderText = "Lower Boundary Value";
      dataGridViewColumn1.MinimumWidth = 150;
      dataGridViewColumn1.CellTemplate = (DataGridViewCell) new DataGridViewTextBoxCell();
      dataGridViewColumn1.Resizable = DataGridViewTriState.True;
      this.dataGridView1.Columns.Add(dataGridViewColumn1);
      DataGridViewColumn dataGridViewColumn2 = new DataGridViewColumn();
      dataGridViewColumn2.HeaderText = "Upper Boundary Value";
      dataGridViewColumn2.MinimumWidth = 150;
      dataGridViewColumn2.CellTemplate = (DataGridViewCell) new DataGridViewTextBoxCell();
      DataGridViewCellStyle gridViewCellStyle = new DataGridViewCellStyle();
      gridViewCellStyle.BackColor = Color.LightGray;
      dataGridViewColumn2.ReadOnly = true;
      dataGridViewColumn2.DefaultCellStyle = gridViewCellStyle;
      dataGridViewColumn2.Resizable = DataGridViewTriState.True;
      this.dataGridView1.Columns.Add(dataGridViewColumn2);
      foreach (BusinessObject_QueryParameter parameter1 in this.parameters)
      {
        BusinessObject_QueryParameter parameter = parameter1;
        BusinessObject_NodeElement objectNodeElement = parameter.ToParent.ToParent.NodeElements.Find((Predicate<BusinessObject_NodeElement>) (item => item.Key.DataTypeElementPathProxyName == parameter.Key.DataTypeElementPathProxyName));
        if (objectNodeElement != null)
        {
          bool alternativeKeyIndicator = objectNodeElement.AlternativeKeyIndicator;
          if (!alternativeKeyIndicator && objectNodeElement.ParentNodeElement != null)
            alternativeKeyIndicator = objectNodeElement.ParentNodeElement.AlternativeKeyIndicator;
          if (alternativeKeyIndicator && !parameter.Name.Contains("UUID"))
          {
            this.dataGridView1.Rows.Add((object[]) new string[5]
            {
              parameter.Name,
              "Include",
              "Equal to",
              "",
              ""
            });
            return;
          }
        }
      }
      this.dataGridView1.Rows.Add((object[]) new string[5]
      {
        this.parameters[0].Name,
        "Include",
        "Equal to",
        "",
        ""
      });
    }

    private void dataGridView1_CellLeave(object sender, DataGridViewCellEventArgs e)
    {
      if (e.ColumnIndex != 0)
        return;
      this.dataGridView1.Rows[e.RowIndex].Cells[3] = (DataGridViewCell) new DataGridViewTextBoxCell();
      this.dataGridView1.Rows[e.RowIndex].Cells[4] = (DataGridViewCell) new DataGridViewTextBoxCell();
      this.enableCell(this.dataGridView1.Rows[e.RowIndex].Cells[4], false);
    }

    private void getQueryParameters(BusinessObject_Query boQuery)
    {
      if (boQuery.QueryParameters.Count == 0)
        return;
      List<BusinessObject_QueryParameter> parameter = new List<BusinessObject_QueryParameter>();
      for (int index = 0; index < boQuery.QueryParameters.Count; ++index)
      {
        if (boQuery.QueryParameters[index].ChildQueryParameters.Count != 0)
          this.addParameters(parameter, this.getChilds(boQuery.QueryParameters[index]));
        else
          this.addParameters(parameter, new BusinessObject_QueryParameter[1]
          {
            boQuery.QueryParameters[index]
          });
      }
      for (int i = 0; i < parameter.Count; ++i)
      {
        if (parameter.FindAll((Predicate<BusinessObject_QueryParameter>) (s => s.Equals((object) parameter[i]))).Count > 1)
          parameter.RemoveAt(i);
      }
      this.parameters = parameter.ToArray();
      for (int index1 = 0; index1 < this.parameters.Length; ++index1)
      {
        for (int index2 = index1 + 1; index2 < this.parameters.Length; ++index2)
        {
          if (this.parameters[index2].Name.CompareTo(this.parameters[index1].Name) < 0)
          {
            BusinessObject_QueryParameter parameter1 = this.parameters[index1];
            this.parameters[index1] = this.parameters[index2];
            this.parameters[index2] = parameter1;
          }
        }
      }
    }

    private BusinessObject_QueryParameter[] getChilds(BusinessObject_QueryParameter boQuery)
    {
      List<BusinessObject_QueryParameter> objectQueryParameterList = new List<BusinessObject_QueryParameter>();
      foreach (BusinessObject_QueryParameter childQueryParameter in (ReadOnlyLinkedElementCollection<BusinessObject_QueryParameter>) boQuery.ChildQueryParameters)
      {
        if (childQueryParameter.ChildQueryParameters.Count != 0)
          objectQueryParameterList.AddRange((IEnumerable<BusinessObject_QueryParameter>) this.getChilds(childQueryParameter));
        else
          objectQueryParameterList.Add(childQueryParameter);
      }
      return objectQueryParameterList.ToArray();
    }

    private void addParameters(List<BusinessObject_QueryParameter> parameters, BusinessObject_QueryParameter[] newParameters)
    {
      foreach (BusinessObject_QueryParameter newParameter in newParameters)
      {
        bool flag = false;
        foreach (BusinessObject_QueryParameter parameter in parameters)
        {
          if (parameter.Name.Equals(newParameter.Name))
            flag = true;
        }
        if (!flag)
          parameters.Add(newParameter);
      }
    }

    private void buttonCount_Click(object sender, EventArgs e)
    {
      int num = this.countQuery();
      if (num == 1)
        this.label_numberOfRecords.Text = num.ToString() + " Entry";
      else
        this.label_numberOfRecords.Text = num.ToString() + " Entries";
    }

    private void buttonClear_Click(object sender, EventArgs e)
    {
      this.dataGridView1.Rows.Clear();
    }

    protected override void buttonOk_Click(object sender, EventArgs e)
    {
      this.queryResult = this.executeQuery(false);
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    public static List<string[]> parseQueryResult(string resultBase64, BusinessObject_Node node, bool intern)
    {
      string str = "";
      List<string> stringList1 = (List<string>) null;
      if (QuerySelection1.sensitiveElements != null)
        stringList1 = ((IEnumerable<string>) QuerySelection1.sensitiveElements).ToList<string>();
      try
      {
        if (resultBase64 != null)
          str = Encoding.UTF8.GetString(Convert.FromBase64String(resultBase64));
      }
      catch (SystemException ex)
      {
        int num = (int) CopernicusMessageBox.Show("Fehler bei JSON");
      }
      List<BusinessObject_NodeElement> source = new List<BusinessObject_NodeElement>();
      List<BusinessObject_NodeElement> list = QuerySelection1.getLeafNodeElements(node).OrderBy<BusinessObject_NodeElement, string>((Func<BusinessObject_NodeElement, string>) (item => item.Name)).ToList<BusinessObject_NodeElement>();
      XmlDocument doc = new XmlDocument();
      doc.InnerXml = str;
      if ((int) QuerySelection1.isSensitive == 88 && stringList1 != null)
      {
        for (int index = 0; index < list.Count; ++index)
        {
          if (!stringList1.Contains(list[index].Key.DataTypeElementPathProxyName))
            source.Add(list[index]);
        }
      }
      else
        source = list;
      List<string[]> strArrayList = new List<string[]>();
      int index1 = 0;
      int num1 = 0;
      List<string> stringList2 = new List<string>();
      stringList2.Add("NODE_ID");
      stringList2.AddRange(source.Select<BusinessObject_NodeElement, string>((Func<BusinessObject_NodeElement, string>) (item => item.Key.DataTypeElementPathProxyName)));
      string[] strArray = new string[stringList2.Count];
      strArray[0] = "NODE_ID";
      List<PDI_S_BC_VALUE_DESCRIPTION[]> valueDescriptionArrayList = new List<PDI_S_BC_VALUE_DESCRIPTION[]>();
      valueDescriptionArrayList.Add((PDI_S_BC_VALUE_DESCRIPTION[]) null);
      for (int index2 = 0; index2 < source.Count; ++index2)
      {
        string proxyofNodeElement = QuerySelection1.getDatatypeProxyofNodeElement(source[index2]);
        DataTypeRepresentationTermCodeEnum representationTerm = QuerySelection1.getRepresentationTerm(proxyofNodeElement);
        if (!intern && representationTerm == DataTypeRepresentationTermCodeEnum.Code)
        {
          PDI_S_BC_VALUE_DESCRIPTION[] codeValues = new BusinessConfigurationHandler().getCodeValues(source[index2].ToRoot.NameKey.Namespace, proxyofNodeElement);
          valueDescriptionArrayList.Add(codeValues);
        }
        else
          valueDescriptionArrayList.Add((PDI_S_BC_VALUE_DESCRIPTION[]) null);
        strArray[index2 + 1] = source[index2].Name;
      }
      strArrayList.Add(strArray);
      foreach (string element in stringList2)
      {
        List<XmlNode> nodeListByTag = QuerySelection1.getNodeListByTag(doc, element);
        if (nodeListByTag.Count != 0)
        {
          if (num1 == 0)
          {
            for (int index2 = 0; index2 < nodeListByTag.Count; ++index2)
              strArrayList.Add(new string[stringList2.Count]);
          }
          for (int index2 = 0; index2 < nodeListByTag.Count; ++index2)
          {
            string innerText = nodeListByTag[index2].InnerText;
            if (valueDescriptionArrayList[index1] != null && ((IEnumerable<PDI_S_BC_VALUE_DESCRIPTION>) valueDescriptionArrayList[index1]).Count<PDI_S_BC_VALUE_DESCRIPTION>() != 0)
            {
              if (innerText != "")
              {
                try
                {
                  innerText = ((IEnumerable<PDI_S_BC_VALUE_DESCRIPTION>) valueDescriptionArrayList[index1]).First<PDI_S_BC_VALUE_DESCRIPTION>((Func<PDI_S_BC_VALUE_DESCRIPTION, bool>) (item => item.VALUE == innerText)).DESCRIPTION;
                }
                catch
                {
                }
              }
            }
            strArrayList[index2 + 1][index1] = innerText;
          }
          ++num1;
        }
        ++index1;
      }
      return strArrayList;
    }

    private static List<XmlNode> getNodeListByTag(XmlDocument doc, string element)
    {
      string str1 = element;
      List<XmlNode> xmlNodeList = new List<XmlNode>();
      if (str1.Contains("-"))
      {
        string str2 = str1.Replace('-', '/');
        foreach (XmlNode selectNode in doc.SelectNodes("//item/" + str2))
          xmlNodeList.Add(selectNode);
      }
      else
      {
        foreach (XmlNode xmlNode in doc.GetElementsByTagName(element))
        {
          if (xmlNode.ParentNode.Name == "item" || xmlNode.ParentNode.Name == "EXT")
            xmlNodeList.Add(xmlNode);
        }
      }
      return xmlNodeList;
    }

    private static List<BusinessObject_NodeElement> getLeafNodeElements(BusinessObject_Node node)
    {
      List<BusinessObject_NodeElement> objectNodeElementList = new List<BusinessObject_NodeElement>();
      foreach (BusinessObject_NodeElement nodeElement in (ReadOnlyLinkedElementCollection<BusinessObject_NodeElement>) node.NodeElements)
      {
        if (nodeElement.ChildNodeElements.Count == 0)
          objectNodeElementList.Add(nodeElement);
      }
      return objectNodeElementList;
    }

    public List<string[]> executeQuery(bool intern)
    {
      RepositoryQueryHandler repositoryQueryHandler = new RepositoryQueryHandler();
      SESF_SELECTION_PARAMETER[] selectionParameter = new SESF_SELECTION_PARAMETER[0];
      if (selectionParameter != null)
        selectionParameter = this.getSelection();
      int maxRows = !(this.textBoxMaxRows.Text == "") ? Convert.ToInt32(this.textBoxMaxRows.Text) : -1;
      int count;
      string resultBase64 = !intern ? repositoryQueryHandler.execute_Query(this.query.Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName, this.query.Key.BusinessObjectNodeKey.ProxyName, this.query.Key.ProxyName, maxRows, selectionParameter, ' ', ' ', out count) : repositoryQueryHandler.execute_Query(this.query.Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName, this.query.Key.BusinessObjectNodeKey.ProxyName, this.query.Key.ProxyName, maxRows, selectionParameter, ' ', 'X', out count);
      repositoryQueryHandler.getSensitiveQueryInfo(this.query.Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName, this.query.Key.BusinessObjectNodeKey.ProxyName, out QuerySelection1.isSensitive, out QuerySelection1.sensitiveElements);
      return QuerySelection1.parseQueryResult(resultBase64, this.query.ToParent, intern);
    }

    private int countQuery()
    {
      RepositoryQueryHandler repositoryQueryHandler = new RepositoryQueryHandler();
      SESF_SELECTION_PARAMETER[] selection = this.getSelection();
      int maxRows = -1;
      int count;
      repositoryQueryHandler.execute_Query(this.query.Key.BusinessObjectNodeKey.BusinessObjectProxyKey.ProxyName, this.query.Key.BusinessObjectNodeKey.ProxyName, this.query.Key.ProxyName, maxRows, selection, 'X', ' ', out count);
      return count;
    }

    private SESF_SELECTION_PARAMETER convertSelectionParameter(SESF_SELECTION_PARAMETER parameter)
    {
      BusinessObject_QueryParameter queryParameter = ((IEnumerable<BusinessObject_QueryParameter>) this.parameters).Single<BusinessObject_QueryParameter>((Func<BusinessObject_QueryParameter, bool>) (item => item.Key.DataTypeElementPathProxyName == parameter.ATTRIBUTE_NAME));
      DataTypeRepresentationTermCodeEnum representationTerm = QuerySelection1.getRepresentationTerm(queryParameter);
      if (representationTerm == DataTypeRepresentationTermCodeEnum.Indicator)
      {
        this.convertIndicator(parameter.LOW);
        if (parameter.HIGH != "")
          this.convertIndicator(parameter.HIGH);
      }
      if (representationTerm == DataTypeRepresentationTermCodeEnum.Code && this.valueHelpBuffer.Count != 0)
      {
        ValueHelp valueHelp = this.valueHelpBuffer.Single<ValueHelp>((Func<ValueHelp, bool>) (item => item.queryParameterProxyName == QuerySelection1.getProxyofQueryParameter(queryParameter)));
        parameter.LOW = QuerySelection1.convertCode(parameter.LOW, valueHelp);
        if (parameter.HIGH != "")
          parameter.HIGH = QuerySelection1.convertCode(parameter.HIGH, valueHelp);
      }
      return parameter;
    }

    private string convertIndicator(string value)
    {
      value = value.Replace("true", "X");
      value = value.Replace("false", " ");
      return value;
    }

    private static string convertCode(string value, ValueHelp valueHelp)
    {
      PDI_S_BC_VALUE_DESCRIPTION valueDescription = ((IEnumerable<PDI_S_BC_VALUE_DESCRIPTION>) valueHelp.values).Single<PDI_S_BC_VALUE_DESCRIPTION>((Func<PDI_S_BC_VALUE_DESCRIPTION, bool>) (item => item.DESCRIPTION.ToUpper() == value));
      if (value == null)
        return "";
      value = valueDescription.VALUE;
      return value;
    }

    private bool checkSelectionParameterIsValid(SESF_SELECTION_PARAMETER parameter, string queryParameterPrxName)
    {
      BusinessObject_QueryParameter queryParameter1 = ((IEnumerable<BusinessObject_QueryParameter>) this.parameters).Single<BusinessObject_QueryParameter>((Func<BusinessObject_QueryParameter, bool>) (item => item.Name == queryParameterPrxName));
      QueryParameterType queryParameter2 = new QueryParameterType();
      queryParameter2.QueryPrxParameterName = parameter.ATTRIBUTE_NAME;
      queryParameter2.QueryParameterValueList = new QueryParameterValueType[1];
      queryParameter2.QueryParameterValueList[0] = new QueryParameterValueType();
      queryParameter2.QueryParameterValueList[0].InclusionExclusionCode = parameter.SIGN;
      queryParameter2.QueryParameterValueList[0].IntervalBoundaryTypeCode = parameter.OPTION;
      queryParameter2.QueryParameterValueList[0].LowerBoundaryValue = parameter.LOW;
      queryParameter2.QueryParameterValueList[0].UpperBoundaryValue = parameter.HIGH;
      InputValueCheck inputValueCheck = new InputValueCheck();
      queryParameter2.QueryPrxParameterDataType = QuerySelection1.getProxyofQueryParameter(queryParameter1);
      DataTypeRepresentationTermCodeEnum representationTerm = QuerySelection1.getRepresentationTerm(queryParameter1);
      List<string> messages;
      if (inputValueCheck.IsInputValid(queryParameter2, representationTerm, out messages))
        return true;
      int num = (int) CopernicusMessageBox.Show(messages[0]);
      return false;
    }

    private SESF_SELECTION_PARAMETER[] getSelection()
    {
      this.selectionParametersForABSL.Clear();
      List<SESF_SELECTION_PARAMETER> selectionParameterList = new List<SESF_SELECTION_PARAMETER>();
      if (this.dataGridView1.RowCount == 0)
        return selectionParameterList.ToArray();
      foreach (DataGridViewRow row in (IEnumerable) this.dataGridView1.Rows)
      {
        SESF_SELECTION_PARAMETER parameter1 = new SESF_SELECTION_PARAMETER();
        DataGridViewCell attribut = row.Cells[0];
        DataGridViewCell cell1 = row.Cells[1];
        DataGridViewCell cell2 = row.Cells[2];
        DataGridViewCell cell3 = row.Cells[3];
        DataGridViewCell cell4 = row.Cells[4];
        if (cell2.Value != null && cell3.Value != null && cell3.Value.ToString() != "")
        {
          BusinessObject_QueryParameter queryParameter = ((IEnumerable<BusinessObject_QueryParameter>) this.parameters).Single<BusinessObject_QueryParameter>((Func<BusinessObject_QueryParameter, bool>) (item => item.Name == attribut.Value.ToString()));
          parameter1.ATTRIBUTE_NAME = queryParameter.Key.DataTypeElementPathProxyName;
          parameter1.SIGN = cell1.Value.ToString();
          parameter1.OPTION = CompareOperands.getAcronym(cell2.Value.ToString()).ToUpper();
          parameter1.LOW = cell3.Value.ToString().ToUpper();
          parameter1.HIGH = cell4.Value != null ? cell4.Value.ToString().ToUpper() : "";
          SESF_SELECTION_PARAMETER parameter2 = this.convertSelectionParameter(parameter1);
          if (!this.checkSelectionParameterIsValid(parameter2, attribut.Value.ToString()))
            return (SESF_SELECTION_PARAMETER[]) null;
          if (QuerySelection1.getRepresentationTerm(queryParameter) == DataTypeRepresentationTermCodeEnum.Date)
          {
            parameter2.LOW = parameter2.LOW.Replace("-", "");
            parameter2.HIGH = parameter2.HIGH.Replace("-", "");
          }
          SESF_SELECTION_PARAMETER selectionParameter = new SESF_SELECTION_PARAMETER();
          selectionParameter.ATTRIBUTE_NAME = queryParameter.Name.Replace("/", ".");
          selectionParameter.SIGN = string.Concat((object) parameter2.SIGN[0]);
          selectionParameter.OPTION = parameter2.OPTION;
          selectionParameter.LOW = parameter2.LOW;
          selectionParameter.HIGH = parameter2.HIGH;
          selectionParameterList.Add(parameter2);
          this.selectionParametersForABSL.Add(selectionParameter);
        }
      }
      return selectionParameterList.ToArray();
    }

    private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
    {
      this.button_pasteValues.Enabled = false;
      if (e.ColumnIndex != 3 && e.ColumnIndex != 4 || this.dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex] is DataGridViewComboBoxCell)
        return;
      if (this.dataGridView1.Rows[e.RowIndex].Cells[0].Value == null)
        return;
      string queryParameterName = this.dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
      if (queryParameterName == "")
        return;
      BusinessObject_QueryParameter queryParameter = ((IEnumerable<BusinessObject_QueryParameter>) this.parameters).First<BusinessObject_QueryParameter>((Func<BusinessObject_QueryParameter, bool>) (item => item.Name == queryParameterName));
      if (queryParameter == null)
        return;
      if (e.ColumnIndex == 3)
        this.button_pasteValues.Enabled = true;
      DataTypeRepresentationTermCodeEnum representationTerm = QuerySelection1.getRepresentationTerm(queryParameter);
      if (representationTerm == DataTypeRepresentationTermCodeEnum.Code)
      {
        string proxyofQueryParameter = QuerySelection1.getProxyofQueryParameter(queryParameter);
        PDI_S_BC_VALUE_DESCRIPTION[] codeValues = new BusinessConfigurationHandler().getCodeValues(queryParameter.ToRoot.NameKey.Namespace, proxyofQueryParameter);
        if (codeValues != null && codeValues.Length != 0)
        {
          DataGridViewComboBoxCell viewComboBoxCell = new DataGridViewComboBoxCell();
          viewComboBoxCell.Items.AddRange((object[]) ((IEnumerable<PDI_S_BC_VALUE_DESCRIPTION>) codeValues).Select<PDI_S_BC_VALUE_DESCRIPTION, string>((Func<PDI_S_BC_VALUE_DESCRIPTION, string>) (item => item.DESCRIPTION)).ToArray<string>());
          this.dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex] = (DataGridViewCell) viewComboBoxCell;
          viewComboBoxCell.Value = (object) codeValues[0].DESCRIPTION;
          this.valueHelpBuffer.Add(new ValueHelp()
          {
            queryParameterProxyName = proxyofQueryParameter,
            values = codeValues
          });
        }
      }
      if (representationTerm != DataTypeRepresentationTermCodeEnum.Indicator)
        return;
      DataGridViewComboBoxCell viewComboBoxCell1 = new DataGridViewComboBoxCell();
      viewComboBoxCell1.Items.AddRange((object[]) new string[2]
      {
        "true",
        "false"
      });
      this.dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex] = (DataGridViewCell) viewComboBoxCell1;
      viewComboBoxCell1.Value = (object) "true";
    }

    private static DataTypeRepresentationTermCodeEnum getRepresentationTerm(BusinessObject_QueryParameter queryParameter)
    {
      return (DataTypeRepresentationTermCodeEnum) Enum.Parse(typeof (DataTypeRepresentationTermCodeEnum), RepositoryDataCache.GetInstance().GetDataTypeByProxyName(QuerySelection1.getProxyofQueryParameter(queryParameter)).RepresentationTerm);
    }

    private static DataTypeRepresentationTermCodeEnum getRepresentationTerm(string dataTypeProxyName)
    {
      RepositoryDataSet.DataTypesRow dataTypeByProxyName = RepositoryDataCache.GetInstance().GetDataTypeByProxyName(dataTypeProxyName);
      if (dataTypeByProxyName == null)
        return DataTypeRepresentationTermCodeEnum.Text;
      return (DataTypeRepresentationTermCodeEnum) Enum.Parse(typeof (DataTypeRepresentationTermCodeEnum), dataTypeByProxyName.RepresentationTerm);
    }

    private static string getProxyofQueryParameter(BusinessObject_QueryParameter queryParameter)
    {
      return queryParameter.TypingDataTypeKey != null ? queryParameter.TypingDataTypeKey.ProxyName : queryParameter.ReferencingDataTypeElementKey.DataTypeKey.ProxyName;
    }

    private static string getDatatypeProxyofNodeElement(BusinessObject_NodeElement nodeElement)
    {
      return nodeElement.TypingDataTypeKey != null ? nodeElement.TypingDataTypeKey.ProxyName : nodeElement.ReferencingDataTypeElementKey.DataTypeKey.ProxyName;
    }

    private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
    {
      try
      {
        DataGridViewComboBoxEditingControl control = (DataGridViewComboBoxEditingControl) e.Control;
        control.DropDownStyle = ComboBoxStyle.DropDown;
        control.AutoCompleteMode = AutoCompleteMode.Suggest;
        this.resizeDropdown(control);
        control.DropDown -= new EventHandler(this.ec_DropDown);
        control.DropDownClosed -= new EventHandler(this.ec_DropDownClosed);
        control.SelectionChangeCommitted -= new EventHandler(this.ec_SelectionChangeCommitted);
        control.SelectionChangeCommitted += new EventHandler(this.ec_SelectionChangeCommitted);
        control.DropDown += new EventHandler(this.ec_DropDown);
        control.DropDownClosed += new EventHandler(this.ec_DropDownClosed);
        control.HelpRequested -= new HelpEventHandler(this.ec_HelpRequested);
        control.HelpRequested += new HelpEventHandler(this.ec_HelpRequested);
      }
      catch (Exception ex)
      {
        e.Control.TextChanged += new EventHandler(this.Control_TextChanged);
      }
    }

    private void ec_HelpRequested(object sender, HelpEventArgs hlpevent)
    {
      DataGridViewComboBoxEditingControl boxEditingControl = (DataGridViewComboBoxEditingControl) sender;
      boxEditingControl.DropDownWidth = this.getOptimalComboBoxDropDownWidth((ComboBox) boxEditingControl);
    }

    private void resizeDropdown(DataGridViewComboBoxEditingControl ec)
    {
    }

    public int getOptimalComboBoxDropDownWidth(ComboBox cbEdit)
    {
      int num1 = cbEdit.DropDownWidth;
      Graphics graphics = cbEdit.CreateGraphics();
      Font font = cbEdit.Font;
      int num2 = cbEdit.Items.Count > cbEdit.MaxDropDownItems ? SystemInformation.VerticalScrollBarWidth : 0;
      foreach (object obj in cbEdit.Items)
      {
        int num3 = (int) graphics.MeasureString(obj.ToString(), font).Width + num2;
        if (num1 < num3)
          num1 = num3;
      }
      return num1;
    }

    private void ec_SelectionChangeCommitted(object sender, EventArgs e)
    {
      if (this.dataGridView1.CurrentCell.ColumnIndex != 2)
        return;
      DataGridViewComboBoxEditingControl boxEditingControl = (DataGridViewComboBoxEditingControl) sender;
      DataGridViewCell dc = this.dataGridView1[4, this.dataGridView1.CurrentCell.RowIndex];
      if (boxEditingControl.SelectedItem.ToString() == "Between")
        this.enableCell(dc, true);
      else
        this.enableCell(dc, false);
    }

    private void ec_DropDownClosed(object sender, EventArgs e)
    {
      ((ComboBox) sender).AutoCompleteMode = AutoCompleteMode.Suggest;
    }

    private void ec_DropDown(object sender, EventArgs e)
    {
      ComboBox cbEdit = (ComboBox) sender;
      cbEdit.BackColor = Color.White;
      cbEdit.AutoCompleteMode = AutoCompleteMode.None;
      cbEdit.DropDownWidth = this.getOptimalComboBoxDropDownWidth(cbEdit);
    }

    private void Control_TextChanged(object sender, EventArgs e)
    {
      DataGridViewTextBoxEditingControl boxEditingControl = (DataGridViewTextBoxEditingControl) sender;
      if (!boxEditingControl.Text.Contains("*"))
        return;
      this.dataGridView1.Rows[boxEditingControl.EditingControlRowIndex].Cells[2].Value = (object) "Contains Pattern";
    }

    private void enableCell(DataGridViewCell dc, bool enabled)
    {
      dc.ReadOnly = !enabled;
      if (enabled)
      {
        dc.Style.BackColor = Color.White;
        dc.Style.ForeColor = dc.OwningColumn.DefaultCellStyle.ForeColor;
      }
      else
      {
        dc.Style.BackColor = Color.LightGray;
        dc.Style.ForeColor = Color.DarkGray;
      }
    }

    private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
    {
      string str = e.FormattedValue.ToString();
      this.dataGridView1[e.ColumnIndex, e.RowIndex].Value = (object) str;
    }

    private void dataGridView1_SelectionChanged(object sender, EventArgs e)
    {
    }

    private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
    {
      this.dataGridView1[e.ColumnIndex, e.RowIndex].Value = (object) this.parameters[0].Name;
    }

    private void button_pasteValues_Click(object sender, EventArgs e)
    {
      this.pasteFromClipboard();
    }

    private void pasteFromClipboard()
    {
      Clipboard.GetData(DataFormats.Text);
      string[] strArray = Clipboard.GetText().Split(new string[1]
      {
        "\r\n"
      }, StringSplitOptions.RemoveEmptyEntries);
      int index1 = this.dataGridView1.CurrentRow.Index;
      DataGridViewRow currentRow = this.dataGridView1.CurrentRow;
      if (strArray.Length > 1)
        this.dataGridView1.Rows.Insert(this.dataGridView1.CurrentRow.Index, strArray.Length - 1);
      for (int index2 = 0; index2 < strArray.Length; ++index2)
      {
        this.dataGridView1.Rows[index1 + index2].Cells[0].Value = currentRow.Cells[0].Value;
        this.dataGridView1.Rows[index1 + index2].Cells[1].Value = currentRow.Cells[1].Value;
        this.dataGridView1.Rows[index1 + index2].Cells[2].Value = currentRow.Cells[2].Value;
        this.dataGridView1.Rows[index1 + index2].Cells[3].Value = (object) strArray[index2];
        this.dataGridView1.Rows[index1 + index2].Cells[4].Value = currentRow.Cells[4].Value;
      }
      this.dataGridView1.Refresh();
    }

    private void button_add_Click(object sender, EventArgs e)
    {
      this.dataGridView1.Rows.Add();
    }

    private void button_remove_Click(object sender, EventArgs e)
    {
      if (this.dataGridView1.CurrentRow == null)
      {
        int num = (int) MessageBox.Show("Please select a Row");
      }
      else
      {
        if (this.dataGridView1.Rows.Count == 1)
          return;
        this.dataGridView1.Rows.Remove(this.dataGridView1.CurrentRow);
      }
    }

    protected override void buttonCancel_Click(object sender, EventArgs e)
    {
      this.cancelled = true;
      this.DialogResult = DialogResult.Cancel;
      this.Close();
    }

    private void QuerySelection1_KeyUp(object sender, KeyEventArgs e)
    {
      if (this.dataGridView1 == null || this.dataGridView1.CurrentRow == null)
        return;
      this.pasteFromClipboard();
    }
  }
}
