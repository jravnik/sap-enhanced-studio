﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.RepositoryViewControl
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.BOCompiler.Common;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.PDI;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.RepositoryView;
using SAP.Copernicus.Core.RepositoryView.TreeModel;
using SAP.Copernicus.Core.Util;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SAP.Copernicus
{
  public class RepositoryViewControl : UserControl
  {
    private RepositoryViewWindow parentWindow;
    private RepositoryTreeBuilder treeBuilder;
    private bool logonInitialOpened;
    private string lastSearchString;
    private int lastSearchIndex;
    private int currentSearchIndex;
    private string regexPatternUpper;
    private bool isLoggedOn;
    private static RepositoryViewControl instance;
    private IContainer components;
    private ToolStrip toolStripRepView;
    private ToolStripButton toolStripButtonLogon;
    private ToolStripButton toolStripButtonLogoff;
    private ToolStripButton toolStripButtonRefresh;
    private TreeView treeViewRepository;
    private ImageList repositoryTreeViewImages;
    private RichTextBox richTextBoxLogon;
    private RichTextBox richTextBoxLoading;
    private ToolStripTextBox toolStripTextBoxSearch;
    private ToolStripButton toolStripSplitButtonSearch;
    private ToolStripButton toolStripButtonCreateSol;
    private ToolStripButton toolStripButtonHelp;

    public static RepositoryViewControl GetInstance()
    {
      return RepositoryViewControl.instance;
    }

    public RepositoryViewControl(RepositoryViewWindow parentWindow)
    {
      this.InitializeComponent();
      this.parentWindow = parentWindow;
      this.treeBuilder = new RepositoryTreeBuilder(RepositoryDataCache.GetInstance().RepositoryDataSet);
      this.treeViewRepository.Sort();
      this.toolStripButtonLogon.Text = CopernicusResources.Logon;
      this.toolStripButtonLogoff.Text = CopernicusResources.Logoff;
      this.toolStripButtonRefresh.Text = CopernicusResources.RefreshText;
      this.richTextBoxLogon.SelectionIndent = 10;
      this.richTextBoxLoading.SelectionIndent = 10;
      this.richTextBoxLogon.Text = "\n" + CopernicusResources.RepositoryLogon;
      this.richTextBoxLoading.Text = "\n" + CopernicusResources.RepositoryLoading;
      RepositoryViewControl.instance = this;
      RepositoryDataCache.RefreshEventHandlers += new EventHandlerOnRepositoryRefresh(this.OnRepDataCacheRefresh);
    }

    public bool isLoggedOnGet()
    {
      return this.isLoggedOn;
    }

    public void OnRepDataCacheRefresh()
    {
      DateTime start1 = SAP.Copernicus.Core.Util.Util.startMeasurement("Refreshing Repository TreeView");
      DateTime start2 = SAP.Copernicus.Core.Util.Util.startMeasurement("Building RepositoryTree.");
      this.buildTreeViewFromDataSet();
      SAP.Copernicus.Core.Util.Util.endMeasurement(start2, "Tree build in: ");
      SAP.Copernicus.Core.Util.Util.endMeasurement(start1, "Total Time: ");
      Trace.WriteLine("----------");
      this.richTextBoxLoading.Hide();
      this.treeViewRepository.Show();
      this.Cursor = Cursors.Default;
      this.lastSearchIndex = -1;
      this.Refresh();
    }

    [UIPermission(SecurityAction.LinkDemand, Window = UIPermissionWindow.AllWindows)]
    protected override bool ProcessDialogChar(char charCode)
    {
      if ((int) charCode != 32 && this.ProcessMnemonic(charCode))
        return true;
      return base.ProcessDialogChar(charCode);
    }

    protected override bool CanEnableIme
    {
      get
      {
        return true;
      }
    }

    public RepositoryTreeBuilder getTreeBuilder()
    {
      return this.treeBuilder;
    }

    public ToolStrip getToolStrip()
    {
      return this.toolStripRepView;
    }

    public void disableToolStripRefresh()
    {
      this.toolStripRepView.Items["toolStripButtonRefresh"].Enabled = false;
      this.toolStripRepView.Refresh();
    }

    public void enableToolStripRefresh()
    {
      this.toolStripRepView.Items["toolStripButtonRefresh"].Enabled = true;
      this.toolStripRepView.Refresh();
    }

    public void LogOnConnection()
    {
      if (this.isLoggedOn)
        return;
      this.isLoggedOn = true;
      this.toolStripRepView.Items["toolStripButtonLogon"].Enabled = false;
      this.toolStripRepView.Items["toolStripButtonLogoff"].Enabled = true;
      this.toolStripRepView.Items["toolStripButtonRefresh"].Enabled = true;
      this.toolStripRepView.Items["toolStripButtonCreateSol"].Enabled = Connection.getInstance().UsageMode != UsageMode.miltiCustomer && Connection.getInstance().SystemMode != SystemMode.Productive;
      this.toolStripSplitButtonSearch.Enabled = true;
      this.toolStripTextBoxSearch.Enabled = true;
      this.toolStripRepView.Refresh();
      this.updateRepositoryViewCaption();
      this.disableToolStripRefresh();
      this.refreshRepositoryTreeViewRoot();
      this.enableToolStripRefresh();
      CompilerEventRegistry.INSTANCE.RaiseClearErrorListEvent("");
    }

    public void LogOn()
    {
      IVsWindowFrame ppWindowFrame = (IVsWindowFrame) null;
      IVsUIShell globalService = Package.GetGlobalService(typeof (SVsUIShell)) as IVsUIShell;
      Guid rguidPersistenceSlot = new Guid("{3AE79031-E1BC-11D0-8F78-00A0C9110057}");
      globalService.FindToolWindow(524288U, ref rguidPersistenceSlot, out ppWindowFrame);
      if (ppWindowFrame != null)
      {
        ppWindowFrame.Show();
        string str = "Solution Explorer";
        ppWindowFrame.SetProperty(-3004, (object) str);
      }
      this.logonInitialOpened = true;
      try
      {
        if (!CopernicusProjectSystemUtil.CloseCurrentSolution(true))
          return;
        Connection instance = Connection.getInstance();
        if (!instance.connect())
          return;
        instance.disconnectEvent += new onDisconnect(this.EventHandler_onDisconnect);
        this.LogOnConnection();
      }
      catch (Exception ex)
      {
        Trace.TraceError(ex.ToString());
        ErrorSink.Instance.AddTask("Severe error while connecting to repository system: " + ex.ToString(), Origin.Unknown, Severity.Error, (EventHandler) null);
      }
    }

    private void EventHandler_onDisconnect()
    {
      this.Logoff();
    }

    public void CreateSolution()
    {
      try
      {
        CreateSolutionWizard createSolutionWizard = new CreateSolutionWizard();
        int num = (int) createSolutionWizard.ShowDialog();
        if (createSolutionWizard == null || !this.SolutionNameIsValid(createSolutionWizard.SolutionName) || (string.IsNullOrEmpty(createSolutionWizard.SolutionDescription) || string.IsNullOrEmpty(createSolutionWizard.SolutionType)))
          return;
        CopernicusProjectSystemUtil.CloseCurrentSolution(true);
        XRepMapper.GetInstance().CreateSolution(createSolutionWizard.SolutionName, createSolutionWizard.SolutionDescription, createSolutionWizard.SolutionDetailedDescription, createSolutionWizard.Keywords, createSolutionWizard.SolutionType, createSolutionWizard.DeploymentUnit, createSolutionWizard.IsLocalSolution, createSolutionWizard.UseBBC, createSolutionWizard.DevPartner, createSolutionWizard.ContactPerson, createSolutionWizard.EMail);
        ExtensionDataCache.GetInstance().Reset();
        this.refreshSolutions();
      }
      catch (ProtocolException ex)
      {
        ex.GetType();
      }
      catch (Exception ex)
      {
        ErrorSink.Instance.AddTask("Severe error in solution creation: " + ex.ToString(), Origin.Unknown, Severity.Error, (EventHandler) null);
      }
    }

    private void refreshRepositoryTreeViewRoot()
    {
      this.richTextBoxLogon.Hide();
      this.treeViewRepository.Hide();
      this.richTextBoxLoading.Show();
      this.Cursor = Cursors.AppStarting;
      this.Refresh();
      this.treeViewRepository.Nodes.Clear();
      RepositoryDataCache.GetInstance().RefreshRoot();
    }

    private void buildTreeViewFromDataSet()
    {
      this.treeBuilder.BuildFromRoot(this.treeViewRepository);
    }

    private void toolStripButtonRefresh_Click(object sender, EventArgs e)
    {
      this.disableToolStripRefresh();
      this.refreshSolutions();
      this.enableToolStripRefresh();
    }

    private void toolStripButtonLogon_Click(object sender, EventArgs e)
    {
      this.LogOn();
    }

    private void toolStripButtonLogoff_Click(object sender, EventArgs e)
    {
      this.Logoff();
    }

    public void Logoff()
    {
      this.isLoggedOn = false;
      if (!CopernicusProjectSystemUtil.CloseCurrentSolution(true))
        return;
      Connection instance1 = Connection.getInstance();
      instance1.disconnect();
      if (!instance1.isConnected())
      {
        CompilerEventRegistry.INSTANCE.RaiseClearErrorListEvent((string) null);
        this.treeViewRepository.Nodes.Clear();
        RepositoryDataCache.GetInstance().Reset();
        ExtensionDataCache.GetInstance().Reset();
        this.treeViewRepository.Hide();
        this.richTextBoxLogon.Show();
        this.Refresh();
        this.toolStripRepView.Items["toolStripButtonLogon"].Enabled = true;
        this.toolStripRepView.Items["toolStripButtonLogoff"].Enabled = false;
        this.toolStripRepView.Items["toolStripButtonRefresh"].Enabled = false;
        this.toolStripRepView.Items["toolStripButtonCreateSol"].Enabled = false;
        this.toolStripSplitButtonSearch.Enabled = false;
        this.toolStripTextBoxSearch.Enabled = false;
        this.toolStripRepView.Refresh();
        this.updateRepositoryViewCaption();
      }
      CopernicusBusinessObjectBrowser.CopernicusBusinessObjectBrowser instance2 = CopernicusBusinessObjectBrowser.CopernicusBusinessObjectBrowser.GetInstance();
      if (instance2 == null)
        return;
      instance2.LoggedOff();
    }

    private void updateRepositoryViewCaption()
    {
      string connectionName = Connection.getInstance().GetConnectionName();
      if (connectionName != null)
      {
        if (Connection.getInstance().TenantRoleCode == TenantRoleCode.Production)
        {
          if (Connection.getInstance().SystemMode == SystemMode.Development)
          {
            this.parentWindow.Caption = string.Format("{0}({1},{2})", (object) CopernicusResources.ToolWindowTitle, (object) connectionName, (object) "Preproduction");
          }
          else
          {
            if (Connection.getInstance().SystemMode != SystemMode.Productive)
              return;
            this.parentWindow.Caption = string.Format("{0}({1},{2})", (object) CopernicusResources.ToolWindowTitle, (object) connectionName, (object) "Production");
          }
        }
        else
          this.parentWindow.Caption = string.Format("{0}({1},{2})", (object) CopernicusResources.ToolWindowTitle, (object) connectionName, (object) Connection.getInstance().TenantRoleCode.ToString());
      }
      else
        this.parentWindow.Caption = CopernicusResources.ToolWindowTitle;
    }

    public void searchInTreeView(string searchString)
    {
      searchString = searchString.Replace("(", "\\(");
      searchString = searchString.Replace(")", "\\)");
      searchString = searchString.Replace("^", "\\^");
      searchString = searchString.Replace("?", "\\?");
      searchString = searchString.Replace("+", "\\+");
      searchString = searchString.Replace("$", "\\$");
      searchString = searchString.Replace("|", "\\|");
      searchString = searchString.Replace(".", "\\.");
      if (!searchString.Equals(this.lastSearchString))
      {
        this.lastSearchIndex = -1;
        this.lastSearchString = searchString;
        this.regexPatternUpper = "^.*" + searchString.ToUpper().Replace("*", ".*");
      }
      this.currentSearchIndex = -1;
      for (int index = 0; index < this.treeViewRepository.Nodes.Count; ++index)
      {
        if (this.searchRecursive(this.treeViewRepository.Nodes[index]))
          return;
      }
      int num = (int) MessageBox.Show("Search reached end of list", "Search", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
      this.lastSearchIndex = -1;
    }

    private bool searchRecursive(TreeNode node)
    {
      ++this.currentSearchIndex;
      if (this.currentSearchIndex > this.lastSearchIndex && this.checkNodeSearchMatch(node))
      {
        this.treeViewRepository.SelectedNode = node;
        this.treeViewRepository.TopNode = node;
        return true;
      }
      for (int index = 0; index < node.Nodes.Count; ++index)
      {
        if (this.searchRecursive(node.Nodes[index]))
          return true;
      }
      return false;
    }

    private bool checkNodeSearchMatch(TreeNode node)
    {
      if (!Regex.IsMatch(node.Text.ToUpper(), this.regexPatternUpper))
        return false;
      this.lastSearchIndex = this.currentSearchIndex;
      return true;
    }

    private void searchBoxKeyPress(object sender, KeyPressEventArgs e)
    {
      if (!this.toolStripTextBoxSearch.Focused || (int) e.KeyChar != 13)
        return;
      this.searchInTreeView(this.toolStripTextBoxSearch.Text);
      e.Handled = true;
    }

    private void toolStripSplitButton1_Click(object sender, EventArgs e)
    {
      this.searchInTreeView(this.toolStripTextBoxSearch.Text);
    }

    private bool SolutionNameIsValid(string name)
    {
      if (Connection.getInstance().UsageMode != UsageMode.OneOff && Connection.getInstance().UsageMode != UsageMode.miltiCustomer)
        return !string.IsNullOrEmpty(name);
      return true;
    }

    private void toolStripButtonCreateSol_Click(object sender, EventArgs e)
    {
      if (Connection.getInstance().UsageMode != UsageMode.OneOff && Connection.getInstance().UsageMode != UsageMode.miltiCustomer && Environment.GetEnvironmentVariable("_copernicus_trace_file") != "AllowScalable")
      {
        int num = (int) CopernicusMessageBox.Show(Resource.MsgScalableNotAllowed, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
      }
      else
        this.CreateSolution();
    }

    private void refreshSolutions()
    {
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Refreshing Solutions");
      RepositoryDataCache.GetInstance().RefreshSolutions();
      this.getTreeBuilder().refreshMySolutionsNode((BaseNode) this.getTreeBuilder().mySolutionsNode);
      this.treeViewRepository.Refresh();
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Refreshed in: ");
    }

    public TreeNode CurrentSelectedNode()
    {
      return this.treeViewRepository.SelectedNode;
    }

    private void toolStripButtonHelpClick(object sender, EventArgs e)
    {
      HelpUtil.DisplayF1Help(HELP_IDS.BDS_MYSOLUTIONS_SELECTION);
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      if (this.treeViewRepository.ImageList == null)
      {
        if (this.repositoryTreeViewImages.Images.Count == 0)
          this.repositoryTreeViewImages = EntityImageList.ImageList;
        this.treeViewRepository.ImageList = this.repositoryTreeViewImages;
      }
      base.OnPaint(e);
      if (this.logonInitialOpened || VSPackageUtil.VsTestHostInstalled)
        return;
      this.LogOn();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (RepositoryViewControl));
      this.toolStripRepView = new ToolStrip();
      this.toolStripButtonLogon = new ToolStripButton();
      this.toolStripButtonLogoff = new ToolStripButton();
      this.toolStripButtonCreateSol = new ToolStripButton();
      this.toolStripButtonRefresh = new ToolStripButton();
      this.toolStripTextBoxSearch = new ToolStripTextBox();
      this.toolStripSplitButtonSearch = new ToolStripButton();
      this.toolStripButtonHelp = new ToolStripButton();
      this.treeViewRepository = new TreeView();
      this.repositoryTreeViewImages = EntityImageList.ImageList;
      this.richTextBoxLogon = new RichTextBox();
      this.richTextBoxLoading = new RichTextBox();
      this.toolStripRepView.SuspendLayout();
      this.SuspendLayout();
      this.toolStripRepView.BackColor = SystemColors.InactiveCaption;
      this.toolStripRepView.GripStyle = ToolStripGripStyle.Hidden;
      this.toolStripRepView.Items.AddRange(new ToolStripItem[7]
      {
        (ToolStripItem) this.toolStripButtonLogon,
        (ToolStripItem) this.toolStripButtonLogoff,
        (ToolStripItem) this.toolStripButtonCreateSol,
        (ToolStripItem) this.toolStripButtonRefresh,
        (ToolStripItem) this.toolStripTextBoxSearch,
        (ToolStripItem) this.toolStripSplitButtonSearch,
        (ToolStripItem) this.toolStripButtonHelp
      });
      this.toolStripRepView.Location = new Point(0, 0);
      this.toolStripRepView.Name = "toolStripRepView";
      this.toolStripRepView.RenderMode = ToolStripRenderMode.Professional;
      this.toolStripRepView.Size = new Size(249, 26);
      this.toolStripRepView.TabIndex = 0;
      this.toolStripRepView.Text = "toolStrip1";
      this.toolStripButtonLogon.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonLogon.Image = (Image) componentResourceManager.GetObject("toolStripButtonLogon.Image");
      this.toolStripButtonLogon.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonLogon.Name = "toolStripButtonLogon";
      this.toolStripButtonLogon.Size = new Size(23, 23);
      this.toolStripButtonLogon.Click += new EventHandler(this.toolStripButtonLogon_Click);
      this.toolStripButtonLogoff.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonLogoff.Enabled = false;
      this.toolStripButtonLogoff.Image = (Image) componentResourceManager.GetObject("toolStripButtonLogoff.Image");
      this.toolStripButtonLogoff.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonLogoff.Name = "toolStripButtonLogoff";
      this.toolStripButtonLogoff.Size = new Size(23, 23);
      this.toolStripButtonLogoff.Click += new EventHandler(this.toolStripButtonLogoff_Click);
      this.toolStripButtonCreateSol.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonCreateSol.Enabled = false;
      this.toolStripButtonCreateSol.Image = (Image) CopernicusResources.CreateSolution;
      this.toolStripButtonCreateSol.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonCreateSol.Name = "toolStripButtonCreateSol";
      this.toolStripButtonCreateSol.Size = new Size(23, 23);
      this.toolStripButtonCreateSol.ToolTipText = "Create Solution";
      this.toolStripButtonCreateSol.Click += new EventHandler(this.toolStripButtonCreateSol_Click);
      this.toolStripButtonRefresh.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonRefresh.Enabled = false;
      this.toolStripButtonRefresh.Image = (Image) componentResourceManager.GetObject("toolStripButtonRefresh.Image");
      this.toolStripButtonRefresh.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonRefresh.Name = "toolStripButtonRefresh";
      this.toolStripButtonRefresh.Size = new Size(23, 23);
      this.toolStripButtonRefresh.Click += new EventHandler(this.toolStripButtonRefresh_Click);
      this.toolStripTextBoxSearch.AcceptsReturn = true;
      this.toolStripTextBoxSearch.Enabled = false;
      this.toolStripTextBoxSearch.Margin = new Padding(1, 1, 1, 2);
      this.toolStripTextBoxSearch.Name = "toolStripTextBoxSearch";
      this.toolStripTextBoxSearch.Size = new Size(100, 23);
      this.toolStripTextBoxSearch.KeyPress += new KeyPressEventHandler(this.searchBoxKeyPress);
      this.toolStripSplitButtonSearch.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripSplitButtonSearch.Enabled = false;
      this.toolStripSplitButtonSearch.Image = (Image) CopernicusResources.View;
      this.toolStripSplitButtonSearch.ImageScaling = ToolStripItemImageScaling.None;
      this.toolStripSplitButtonSearch.ImageTransparentColor = Color.Magenta;
      this.toolStripSplitButtonSearch.Name = "toolStripSplitButtonSearch";
      this.toolStripSplitButtonSearch.Size = new Size(23, 23);
      this.toolStripSplitButtonSearch.Click += new EventHandler(this.toolStripSplitButton1_Click);
      this.toolStripButtonHelp.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonHelp.Image = (Image) CopernicusResources.Help;
      this.toolStripButtonHelp.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonHelp.Name = "toolStripButtonHelp";
      this.toolStripButtonHelp.Size = new Size(23, 23);
      this.toolStripButtonHelp.Click += new EventHandler(this.toolStripButtonHelpClick);
      this.treeViewRepository.Dock = DockStyle.Fill;
      this.treeViewRepository.HideSelection = false;
      this.treeViewRepository.ImageIndex = 0;
      this.treeViewRepository.ImageList = this.repositoryTreeViewImages;
      this.treeViewRepository.Location = new Point(0, 26);
      this.treeViewRepository.Name = "treeViewRepository";
      this.treeViewRepository.SelectedImageIndex = 0;
      this.treeViewRepository.ShowNodeToolTips = true;
      this.treeViewRepository.Size = new Size(249, 124);
      this.treeViewRepository.TabIndex = 1;
      this.treeViewRepository.Visible = false;
      this.richTextBoxLogon.BackColor = SystemColors.Window;
      this.richTextBoxLogon.Dock = DockStyle.Fill;
      this.richTextBoxLogon.Location = new Point(0, 26);
      this.richTextBoxLogon.Name = "richTextBoxLogon";
      this.richTextBoxLogon.ReadOnly = true;
      this.richTextBoxLogon.Size = new Size(249, 124);
      this.richTextBoxLogon.TabIndex = 2;
      this.richTextBoxLogon.Text = "";
      this.richTextBoxLoading.BackColor = SystemColors.Window;
      this.richTextBoxLoading.Dock = DockStyle.Fill;
      this.richTextBoxLoading.Location = new Point(0, 26);
      this.richTextBoxLoading.Margin = new Padding(3, 3, 3, 6);
      this.richTextBoxLoading.Name = "richTextBoxLoading";
      this.richTextBoxLoading.ReadOnly = true;
      this.richTextBoxLoading.ShowSelectionMargin = true;
      this.richTextBoxLoading.Size = new Size(249, 124);
      this.richTextBoxLoading.TabIndex = 3;
      this.richTextBoxLoading.Text = "";
      this.richTextBoxLoading.Visible = false;
      this.BackColor = SystemColors.Window;
      this.Controls.Add((Control) this.richTextBoxLoading);
      this.Controls.Add((Control) this.richTextBoxLogon);
      this.Controls.Add((Control) this.treeViewRepository);
      this.Controls.Add((Control) this.toolStripRepView);
      this.Name = nameof (RepositoryViewControl);
      this.Size = new Size(249, 150);
      this.toolStripRepView.ResumeLayout(false);
      this.toolStripRepView.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
