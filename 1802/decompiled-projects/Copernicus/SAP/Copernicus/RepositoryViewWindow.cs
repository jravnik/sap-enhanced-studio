﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.RepositoryViewWindow
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAP.Copernicus
{
  [Guid("0128a988-1402-4c49-a09b-df226597dc6c")]
  public class RepositoryViewWindow : CopernicusToolWindowPane
  {
    private RepositoryViewControl control;

    public RepositoryViewWindow()
      : base(HELP_IDS.BDS_MYSOLUTIONS_SELECTION)
    {
      this.Caption = CopernicusResources.ToolWindowTitle;
      this.BitmapResourceID = 302;
      this.BitmapIndex = 0;
      this.control = new RepositoryViewControl(this);
    }

    public override IWin32Window Window
    {
      get
      {
        return (IWin32Window) this.control;
      }
    }
  }
}
