﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusBusinessObjectBrowserWindow
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAP.Copernicus
{
  [Guid("FACD957C-A1CA-4FFA-B71D-3AF395F6A604")]
  public class CopernicusBusinessObjectBrowserWindow : CopernicusToolWindowPane
  {
    private CopernicusBusinessObjectBrowser.CopernicusBusinessObjectBrowser control;

    public CopernicusBusinessObjectBrowserWindow()
      : base(HELP_IDS.BDS_REPOSITORYEXPLORER_SELECTION)
    {
      this.Caption = "Repository Explorer";
      this.BitmapResourceID = 302;
      this.BitmapIndex = 0;
      this.control = new CopernicusBusinessObjectBrowser.CopernicusBusinessObjectBrowser();
    }

    public override IWin32Window Window
    {
      get
      {
        return (IWin32Window) this.control;
      }
    }
  }
}
