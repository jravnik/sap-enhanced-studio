﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusOptionPage
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.ToolOptions;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAP.Copernicus
{
  [Guid("1A0791D6-3987-4f49-97B6-3AC92E59D9D6")]
  [CLSCompliant(false)]
  [ComVisible(true)]
  [ClassInterface(ClassInterfaceType.AutoDual)]
  public class CopernicusOptionPage : OptionPageFields
  {
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    protected override IWin32Window Window
    {
      get
      {
        CopernicusOptionDialog copernicusOptionDialog = new CopernicusOptionDialog(this);
        copernicusOptionDialog.Initialize();
        return (IWin32Window) copernicusOptionDialog;
      }
    }
  }
}
