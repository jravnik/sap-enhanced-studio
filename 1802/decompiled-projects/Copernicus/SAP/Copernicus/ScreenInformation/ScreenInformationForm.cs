﻿using SAP.Copernicus.Core;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Extension.UIDesignerHelper;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SAP.Copernicus.ScreenInformation
{
    class ScreenInformationForm : BaseForm
    {
        private GroupBox groupBox;
        private Label lblUrl;
        private Label lblDecoded;
        private Label lblScreenComponent;
        private TextBox txtUrl;
        private TextBox txtDecoded;
        private TextBox txtScreenComponent;
        private Button btnGetInfo;
        private Button btnOpenInUIDesigner;
        private string system;

        public ScreenInformationForm()
          : base()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.lblUrl = new Label();
            this.lblDecoded = new Label();
            this.lblScreenComponent = new Label();
            this.groupBox = new GroupBox();
            this.btnGetInfo = new Button();
            this.btnOpenInUIDesigner = new Button();
            this.txtUrl = new TextBox();
            this.txtDecoded = new TextBox();
            this.txtScreenComponent = new TextBox();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            this.lblUrl.AutoSize = true;
            this.lblUrl.Location = new Point(7, 30);
            this.lblUrl.Name = "lblUrl";
            this.lblUrl.TabIndex = 1;
            this.lblUrl.Text = "URL:";
            this.lblDecoded.AutoSize = true;
            this.lblDecoded.Location = new Point(7, 60);
            this.lblDecoded.Name = "lblDecoded";
            this.lblDecoded.TabIndex = 1;
            this.lblDecoded.Text = "Decoded:";
            this.lblScreenComponent.AutoSize = true;
            this.lblScreenComponent.Location = new Point(7, 200);
            this.lblScreenComponent.Name = "lblScreenComponent";
            this.lblScreenComponent.TabIndex = 1;
            this.lblScreenComponent.Text = "Target Component:";
            this.groupBox.Controls.Add((Control)this.lblUrl);
            this.groupBox.Controls.Add((Control)this.lblDecoded);
            this.groupBox.Controls.Add((Control)this.lblScreenComponent);
            this.groupBox.Controls.Add((Control)this.txtUrl);
            this.groupBox.Controls.Add((Control)this.txtDecoded);
            this.groupBox.Controls.Add((Control)this.txtScreenComponent);
            this.groupBox.Controls.Add((Control)this.btnGetInfo);
            this.groupBox.Controls.Add((Control)this.btnOpenInUIDesigner);
            this.groupBox.Location = new Point(12, 75);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new Size(718, 247);
            this.groupBox.TabIndex = 2;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Decode an URL";
            this.btnGetInfo.Location = new Point(595, 26);
            this.btnGetInfo.Name = "btnGetInfo";
            this.btnGetInfo.Size = new Size(115, 22);
            this.btnGetInfo.TabIndex = 1;
            this.btnGetInfo.Text = "Decode URL";
            this.btnGetInfo.UseVisualStyleBackColor = true;
            this.btnGetInfo.Click += new EventHandler(this.buttonGetInfo_Click);
            this.btnOpenInUIDesigner.Location = new Point(595, 196);
            this.btnOpenInUIDesigner.Name = "btnOpenInUIDesigner";
            this.btnOpenInUIDesigner.Size = new Size(115, 22);
            this.btnOpenInUIDesigner.TabIndex = 1;
            this.btnOpenInUIDesigner.Text = "Open in UI Designer";
            this.btnOpenInUIDesigner.UseVisualStyleBackColor = true;
            this.btnOpenInUIDesigner.Click += new EventHandler(this.buttonOpenInUIDesigner_Click);
            this.txtUrl.Location = new Point(109, 27);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new Size(475, 20);
            this.txtUrl.TabIndex = 0;
            this.txtDecoded.Location = new Point(109, 57);
            this.txtDecoded.Name = "txtDecoded";
            this.txtDecoded.Size = new Size(475, 130);
            this.txtDecoded.TabIndex = 0;
            this.txtDecoded.Multiline = true;
            this.txtDecoded.ReadOnly = true;
            this.txtScreenComponent.Location = new Point(109, 197);
            this.txtScreenComponent.Name = "txtScreenComponent";
            this.txtScreenComponent.Size = new Size(475, 20);
            this.txtScreenComponent.TabIndex = 0;
            this.txtScreenComponent.ReadOnly = true;
            this.Controls.Add((Control)this.groupBox);
            this.buttonOk.Visible = false;
            this.buttonCancel.Location = new Point(655, 329);
            this.MinimumSize = new Size(760, 400);
            this.Size = new Size(760, 400);
            this.Name = nameof(ScreenInformationForm);
            this.StartPosition = FormStartPosition.CenterParent;
            this.Subtitle = "You can get more information on an UI component or screen from an URL.";
            this.Text = "Screen Information";
            this.Load += new EventHandler(this.ScreenInformation_Load);
            this.Controls.SetChildIndex((Control)this.groupBox, 0);
            this.Controls.SetChildIndex((Control)this.buttonCancel, 0);
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.ResumeLayout(false);
        }

        private void ScreenInformation_Load(object sender, EventArgs e)
        {
            this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;

            this.btnOpenInUIDesigner.Enabled = (this.txtScreenComponent.Text.Length > 0 && isInSolution() && isFloorPlanEditable());

            string clipboard = Clipboard.GetText();

            Uri url;
            bool isUrl = Uri.TryCreate(clipboard, UriKind.Absolute, out url) && (url.Scheme == Uri.UriSchemeHttp || url.Scheme == Uri.UriSchemeHttps);

            if (isUrl)
            {
                this.txtUrl.Text = clipboard;
            }
        }

        private bool isFloorPlanEditable()
        {
            return (!(XRepMapper.GetInstance().GetOpenSolutionStatus() != "In Development") || !(XRepMapper.GetInstance().GetOpenSolutionStatus() != "In Maintenance")) && SAP.Copernicus.Core.Protocol.Connection.getInstance().SystemMode != SystemMode.Productive;
        }

        private bool isInSolution()
        {
            if (CopernicusProjectSystemUtil.GetCurrentSolutionName() != null)
            {
                return XRepMapper.GetInstance().IsSolutionEditable();
            }

            return false;
        }

        private void buttonOpenInUIDesigner_Click(object sender, EventArgs e)
        {
            string screenPath = txtScreenComponent.Text;

            if (screenPath.Length > 0 && isInSolution() && isFloorPlanEditable())
            {
                if (Connection.getInstance() == null || Connection.getInstance().getConnectedSystem() == null)
                {
                    return;
                }

                if (Connection.getInstance().getConnectedSystem().Host.ToUpper().Contains("CRM"))
                {
                    if (!this.system.StartsWith("c4c"))
                    {
                        CopernicusMessageBox.Show("Cannot open an UI component of ByD in a C4C solution", CopernicusResources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        return;
                    }
                }
                else
                {
                    if (!this.system.StartsWith("byd"))
                    { 
                        CopernicusMessageBox.Show("Cannot open an UI component of C4C in a ByD solution", CopernicusResources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        return;
                    }
                }

                if (XRepMapper.GetInstance() == null)
                {
                    return;
                }

                if (Regex.IsMatch(screenPath, "^\\/Y[A-Z0-9]*_MAIN\\/"))
                {
                    string xRepPath = XRepMapper.GetInstance().GetProjectFolderXRepPath();

                    if (xRepPath != null && xRepPath.Length > 0 && !screenPath.StartsWith(xRepPath))
                    {
                        string currentSolution = xRepPath.Replace("MAIN/SRC", "").Replace("/", "");
                        CopernicusMessageBox.Show("UI component is not contained in currently open solution (" + currentSolution + ")", CopernicusResources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        return;
                    }
                }

                if (screenPath.EndsWith(".xuicomponent"))
                {
                    IDictionary<string, string> attribs = (IDictionary<string, string>)null;
                    string content = "";

                    XRepHandler xrepHandler = new XRepHandler();
                    xrepHandler.Read(screenPath, out content, out attribs);

                    string strXRepPathforUIComponent;

                    if (attribs.TryGetValue("XUIComponent", out strXRepPathforUIComponent))
                    {
                        UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(strXRepPathforUIComponent, false);
                    }
                }
                else
                {
                    UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(screenPath, false);
                }
            }
        }

        private void buttonGetInfo_Click(object sender, EventArgs e)
        {
            string text = this.txtUrl.Text;

            Uri url;
            bool isUrl = Uri.TryCreate(text, UriKind.Absolute, out url) && (url.Scheme == Uri.UriSchemeHttp || url.Scheme == Uri.UriSchemeHttps);

            if (isUrl)
            {
                string base64 = "";
                string fragment = Uri.UnescapeDataString(url.Fragment);

                if (url.Host.ToUpper().Contains("CRM"))
                {
                    this.system = "c4c_";

                    if (url.AbsolutePath.ToUpper().Contains("SAP/PUBLIC/AP/UI/REPOSITORY/SAP_UI/RUNTIME/STARTPAGE.HTML"))
                    {
                        this.system = this.system + "silverlight";
                    }
                    else if (url.Query.ToUpper().Contains("CLIENT_TYPE=HTML"))
                    {
                        this.system = this.system + "html5";
                    }
                    else
                    {
                        this.system = this.system + "fiori";
                    }

                    if (fragment.Length > 7)
                    {
                        base64 = fragment.Remove(0, 7);
                        base64 = Uri.UnescapeDataString(base64);
                    }
                }
                else
                {
                    this.system = "byd_";

                    if (url.AbsolutePath.ToUpper().Contains("HTMLOBERON5"))
                    {
                        this.system = this.system + "html5";
                        if (fragment.Length > 3)
                        {
                            base64 = fragment.Remove(0, 3);
                        }
                    }
                    else
                    {
                        this.system = this.system + "silverlight";
                        if (fragment.Length > 7)
                        {
                            base64 = fragment.Remove(0, 7);
                            base64 = Uri.UnescapeDataString(base64);
                        }
                    }
                }

                if (base64.Length > 0)
                {
                    try
                    {
                        byte[] data = Convert.FromBase64String(base64);
                        string decodedString = Encoding.UTF8.GetString(data);

                        this.txtDecoded.Text = decodedString;

                        if (decodedString.Length > 0)
                        {
                            string uiComponent = "";
                            int start = -1, end = -1;

                            switch (this.system)
                            {
                                case "c4c_silverlight":
                                case "byd_silverlight":
                                    uiComponent = decodedString.Remove(0, 1);

                                    end = uiComponent.IndexOf(".uicomponent");

                                    if (end > -1)
                                    {
                                        uiComponent = uiComponent.Substring(0, end) + ".uicomponent";
                                    }

                                    break;
                                case "c4c_html5":
                                case "c4c_fiori":
                                case "byd_html5":
                                    start = decodedString.IndexOf("\"target\":\"");

                                    if (start > -1)
                                    {
                                        end = decodedString.IndexOf("\"", start + 10);

                                        if (end > -1)
                                        {
                                            uiComponent = decodedString.Substring((start + 10), end - (start + 10));
                                        }
                                    }

                                    break;
                            }

                            this.txtScreenComponent.Text = uiComponent;
                            this.btnOpenInUIDesigner.Enabled = (this.txtScreenComponent.Text.Length > 0 && isInSolution() && isFloorPlanEditable());
                        }
                    }
                    catch
                    {
                        CopernicusMessageBox.Show("Entered text is not an URL with a valid fragment", CopernicusResources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }
            }
            else
            {
                CopernicusMessageBox.Show("Entered text is not a valid URL", CopernicusResources.BYD_STUDIO, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        protected override void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
