﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BusinessObjectNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BusinessObjectNode : BaseNode
  {
    private string boName;

    public BusinessObjectNode(RepositoryDataSet.BusinessObjectsRow boRow)
      : base((DataRow) boRow)
    {
      this.boName = boRow.Name;
      this.BOProxyName = boRow.ProxyName;
      this.Text = this.boName;
      this.ImageIndex = 6;
      this.SelectedImageIndex = 6;
    }

    public override string BOName
    {
      get
      {
        return this.boName;
      }
    }

    public string BOProxyName { get; set; }

    public RepositoryDataSet.BusinessObjectsRow getData()
    {
      return (RepositoryDataSet.BusinessObjectsRow) this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.BusinessObject;
    }
  }
}
