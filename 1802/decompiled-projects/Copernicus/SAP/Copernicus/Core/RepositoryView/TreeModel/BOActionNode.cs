﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BOActionNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BOActionNode : BaseNode
  {
    public string boActionName;
    public string boActionProxyName;

    public BOActionNode(RepositoryDataSet.ActionsRow actionRow)
      : base((DataRow) actionRow)
    {
      this.boActionName = actionRow.Name;
      this.boActionProxyName = actionRow.ProxyName;
      this.Text = this.boActionName;
      this.ImageIndex = 5;
      this.SelectedImageIndex = 5;
    }

    public RepositoryDataSet.ActionsRow getData()
    {
      return (RepositoryDataSet.ActionsRow) this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.BOAction;
    }
  }
}
