﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.MySolutionsNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;
using System;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class MySolutionsNode : BaseNode
  {
    private string stName;

    public MySolutionsNode()
      : base((DataRow) null)
    {
      this.stName = "My Solutions (" + Resource.CustomerID + ": " + Connection.getInstance().PartnerID + ")";
      this.Text = this.stName;
      this.ToolTipText = Resource.Solution + ":\t" + Connection.getInstance().OfficialName + "\n" + Resource.Version + ":\t\t" + Connection.getInstance().ReleaseName + "\n" + Resource.CustomerID + ":\t" + Connection.getInstance().PartnerID + "\n" + Resource.CustomerName + ":\t" + Connection.getInstance().PartnerName;
      this.ImageIndex = 179;
      this.SelectedImageIndex = 179;
    }

    public override NodeType getNodeType()
    {
      return NodeType.MySolutions;
    }

    public override void refresh()
    {
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Refreshing " + (object) this.getNodeType() + " (" + this.stName + ") ...");
      RepositoryViewControl repositoryViewControl = this.getRepositoryViewControl();
      RepositoryDataCache.GetInstance().RefreshSolutions();
      repositoryViewControl.getTreeBuilder().refreshMySolutionsNode((BaseNode) this);
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Refreshed in: ");
    }
  }
}
