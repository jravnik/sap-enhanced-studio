﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BODeterminationNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BODeterminationNode : BaseNode
  {
    public string boDeterminationName;
    public char boDeterminationABAPcode;

    public BODeterminationNode(string boDeterminationName, char boDeterminationABAPcode)
      : base((DataRow) null)
    {
      this.boDeterminationABAPcode = boDeterminationABAPcode;
      this.boDeterminationName = boDeterminationName;
      this.Text = boDeterminationName;
      this.ImageIndex = 6;
      this.SelectedImageIndex = 6;
    }

    public override NodeType getNodeType()
    {
      return NodeType.BODetermination;
    }
  }
}
