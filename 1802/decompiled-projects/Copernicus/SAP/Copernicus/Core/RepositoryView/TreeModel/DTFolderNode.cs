﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.DTFolderNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository;
using System;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class DTFolderNode : BaseNode
  {
    public DTFolderNode()
      : this((DataRow) null)
    {
    }

    public DTFolderNode(DataRow dataRow)
      : base(dataRow)
    {
      this.Text = CopernicusResources.TreeNodeTextDT;
      this.ImageIndex = 9;
      this.SelectedImageIndex = 9;
    }

    public override NodeType getNodeType()
    {
      return NodeType.DTFolder;
    }

    public override void refresh()
    {
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Refreshing " + (object) this.getNodeType() + " (" + this.Namespace + ") ...");
      string nsName = this.Namespace;
      RepositoryDataCache.GetInstance().RefreshDTFolderNode(nsName);
      this.getRepositoryViewControl().getTreeBuilder().refreshDTFolder((BaseNode) this, nsName);
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Refreshed in: ");
    }
  }
}
