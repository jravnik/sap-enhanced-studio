﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.DataTypeNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class DataTypeNode : BaseNode
  {
    private string dtName;

    public DataTypeNode(RepositoryDataSet.DataTypesRow dtRow)
      : base((DataRow) dtRow)
    {
      this.dtName = dtRow.Name;
      this.Text = this.dtName;
      this.ImageIndex = 3;
      this.SelectedImageIndex = 3;
    }

    public RepositoryDataSet.DataTypesRow getData()
    {
      return (RepositoryDataSet.DataTypesRow) this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.DataType;
    }
  }
}
