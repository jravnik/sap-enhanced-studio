﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BaseNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public abstract class BaseNode : TreeNode
  {
    protected DataRow content;

    protected BaseNode(DataRow dataRow)
    {
      this.content = dataRow;
      this.ContextMenuStrip = new ContextMenuStrip();
    }

    public bool isNodeType(NodeType type)
    {
      return this.getNodeType() == type;
    }

    public DataRow getData()
    {
      return this.content;
    }

    public BaseNode getParent()
    {
      return this.Parent as BaseNode;
    }

    public RepositoryViewControl getRepositoryViewControl()
    {
      return (RepositoryViewControl) this.TreeView.Parent;
    }

    public abstract NodeType getNodeType();

    public virtual void refresh()
    {
      Trace.TraceWarning("Refresh not supported for " + (object) this.getNodeType());
    }

    public virtual string Namespace
    {
      get
      {
        BaseNode parent = this.getParent();
        if (parent == null)
          return (string) null;
        return parent.Namespace;
      }
    }

    public virtual string BOName
    {
      get
      {
        BaseNode parent = this.getParent();
        if (parent == null)
          return (string) null;
        return parent.BOName;
      }
    }

    public virtual string NodeName
    {
      get
      {
        BaseNode parent = this.getParent();
        if (parent == null)
          return (string) null;
        return parent.NodeName;
      }
    }
  }
}
