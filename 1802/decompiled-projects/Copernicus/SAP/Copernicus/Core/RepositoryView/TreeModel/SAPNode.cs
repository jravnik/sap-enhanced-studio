﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.SAPNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class SAPNode : BaseNode
  {
    private string stName;

    public SAPNode()
      : base((DataRow) null)
    {
      this.stName = "SAP Content";
      this.Text = this.stName;
      this.ImageIndex = 9;
      this.SelectedImageIndex = 9;
    }

    public override NodeType getNodeType()
    {
      return NodeType.SAPContent;
    }
  }
}
