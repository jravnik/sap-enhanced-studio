﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ErrorList.ErrorListPresenter
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Adornments;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.Core.ErrorList
{
  internal class ErrorListPresenter
  {
    private IWpfTextView textView;
    private string fileName;
    private CopernicusErrorListProvider errorListProvider;
    private SimpleTagger<ErrorTag> squiggleTagger;
    private List<TrackingTagSpan<ErrorTag>> previousSquiggles;
    private List<CopernicusErrorTask> previousErrors;

    public ErrorListPresenter(IWpfTextView textView, IErrorProviderFactory squiggleProviderFactory, IServiceProvider serviceProvider)
    {
      this.errorListProvider = CopernicusErrorListProvider.GetInstance(serviceProvider);
      this.textView = textView;
      this.fileName = textView.TextBuffer.Properties.GetProperty<ITextDocument>((object) typeof (ITextDocument)).FilePath;
      this.errorListProvider.AddHandler(this.fileName, new ErrorListUpdated(this.OnErrorListUpdated));
      this.textView.Closed += new EventHandler(this.OnTextViewClosed);
      this.squiggleTagger = squiggleProviderFactory.GetErrorTagger(textView.TextBuffer);
      this.previousErrors = new List<CopernicusErrorTask>();
      this.previousSquiggles = new List<TrackingTagSpan<ErrorTag>>();
      this.CreateErrors();
    }

    private void OnTextViewClosed(object sender, EventArgs e)
    {
      this.ClearErrors();
      this.errorListProvider.RemoveHandler(this.fileName, new ErrorListUpdated(this.OnErrorListUpdated));
    }

    private void OnErrorListUpdated()
    {
      this.CreateErrors();
    }

    private void ClearErrors()
    {
      this.previousSquiggles.ForEach((Action<TrackingTagSpan<ErrorTag>>) (tag => this.squiggleTagger.RemoveTagSpan(tag)));
      this.previousSquiggles.Clear();
      this.previousErrors.Clear();
    }

    private void CreateErrors()
    {
      IEnumerable<CopernicusErrorTask> tasksForDocument = this.errorListProvider.GetErrorTasksForDocument(this.fileName);
      if (tasksForDocument.Count<CopernicusErrorTask>() == this.previousErrors.Count)
        return;
      this.ClearErrors();
      foreach (CopernicusErrorTask copernicusErrorTask in tasksForDocument)
      {
        this.previousErrors.Add(copernicusErrorTask);
        TextSpan textSpan = copernicusErrorTask.TextSpan;
        if (copernicusErrorTask.Line < this.textView.TextSnapshot.LineCount && copernicusErrorTask.EndLine < this.textView.TextSnapshot.LineCount)
        {
          int start = this.textView.TextSnapshot.GetLineFromLineNumber(copernicusErrorTask.Line).Start.Position + copernicusErrorTask.Column;
          int length = Math.Min(this.textView.TextSnapshot.GetLineFromLineNumber(copernicusErrorTask.EndLine).Start.Position + copernicusErrorTask.EndIndex - start, Math.Max(this.textView.TextSnapshot.Length - start, 0));
          if (start >= 0 && length >= 0 && start + length <= this.textView.TextSnapshot.Length)
          {
            ITrackingSpan trackingSpan = this.textView.TextSnapshot.CreateTrackingSpan(new Span(start, length), SpanTrackingMode.EdgeNegative);
            string errorType;
            switch (copernicusErrorTask.ErrorCategory)
            {
              case TaskErrorCategory.Error:
                errorType = "syntax error";
                break;
              case TaskErrorCategory.Warning:
                errorType = "compiler warning";
                break;
              case TaskErrorCategory.Message:
                errorType = "compiler warning";
                break;
              default:
                errorType = "other error";
                break;
            }
            ErrorTag tag = new ErrorTag(errorType, (object) copernicusErrorTask.Text);
            this.previousSquiggles.Add(this.squiggleTagger.CreateTagSpan(trackingSpan, tag));
          }
        }
      }
    }
  }
}
