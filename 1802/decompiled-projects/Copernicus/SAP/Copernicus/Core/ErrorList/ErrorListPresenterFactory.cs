﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ErrorList.ErrorListPresenterFactory
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text.Adornments;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;
using System;
using System.ComponentModel.Composition;

namespace SAP.Copernicus.Core.ErrorList
{
  [Export(typeof (IWpfTextViewCreationListener))]
  [ContentType("any")]
  [TextViewRole("DOCUMENT")]
  internal class ErrorListPresenterFactory : IWpfTextViewCreationListener
  {
    [Import]
    private IErrorProviderFactory SquiggleProviderFactory { get; set; }

    [Import(typeof (SVsServiceProvider))]
    private IServiceProvider ServiceProvider { get; set; }

    public void TextViewCreated(IWpfTextView textView)
    {
      textView.Properties.GetOrCreateSingletonProperty<ErrorListPresenter>((Func<ErrorListPresenter>) (() => new ErrorListPresenter(textView, this.SquiggleProviderFactory, this.ServiceProvider)));
    }
  }
}
