﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.DevTrace
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using System;
using System.Diagnostics;
using System.IO;

namespace SAP.Copernicus.Core
{
  internal class DevTrace : TraceListener
  {
    private static readonly char[] WHITE_SPACES = new char[2]
    {
      ' ',
      '\t'
    };
    private static readonly char[] TRIM_CHARS = new char[1]
    {
      '"'
    };
    public const string TRACE_FILE_PARAMETER = "trace_file";
    private readonly StreamWriter writer;

    internal static DevTrace Start()
    {
      string[] commandLineArgs = Environment.GetCommandLineArgs();
      string str = (string) null;
      for (int index = 0; index < commandLineArgs.Length; ++index)
      {
        if (commandLineArgs[index].Equals(47.ToString() + "trace_file", StringComparison.InvariantCultureIgnoreCase) && index + 1 < commandLineArgs.Length)
        {
          str = commandLineArgs[index + 1];
          break;
        }
      }
      if (string.IsNullOrEmpty(str))
        str = Environment.GetEnvironmentVariable("_copernicus_trace_file");
      if (!string.IsNullOrEmpty(str))
        str = str.Trim(DevTrace.TRIM_CHARS);
      return new DevTrace(str + "CopernicusTrace");
    }

    private DevTrace(string traceFileName)
    {
      if (string.IsNullOrEmpty(traceFileName))
        return;
      try
      {
        string directoryName = Path.GetDirectoryName(traceFileName);
        if (!string.IsNullOrEmpty(directoryName) && !Directory.Exists(directoryName))
          Directory.CreateDirectory(directoryName);
        this.writer = File.CreateText(traceFileName);
        this.writer.AutoFlush = true;
        Trace.Listeners.Add((TraceListener) this);
      }
      catch (Exception ex)
      {
        Console.WriteLine("Unable to create trace file {0}, reason: {1}", (object) traceFileName, (object) ex.Message);
      }
    }

    protected override void Dispose(bool disposing)
    {
      this.Stop();
    }

    public override void Write(string message)
    {
      this.writer.Write(message);
    }

    public override void WriteLine(string message)
    {
      this.writer.WriteLine(message);
    }

    internal void Stop()
    {
      if (Trace.Listeners.Contains((TraceListener) this))
        Trace.Listeners.Remove((TraceListener) this);
      if (this.writer == null)
        return;
      this.writer.WriteLine("stopped at {0}", (object) DateTime.Now);
      this.writer.Close();
    }
  }
}
