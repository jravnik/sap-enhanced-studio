﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.SelectionEvents
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.CopernicusProjectView;
using System;

namespace SAP.Copernicus
{
  public class SelectionEvents : IVsSelectionEvents
  {
    private static uint ContextCookie = SelectionEvents.RegisterContext();
    private static IVsMonitorSelection SelectionService;

    private static uint RegisterContext()
    {
      SelectionEvents.SelectionService = (IVsMonitorSelection) Package.GetGlobalService(typeof (SVsShellMonitorSelection));
      Guid uicontextBoFileSelected = GuidList.UICONTEXT_boFileSelected;
      uint pdwCmdUICookie;
      SelectionEvents.SelectionService.GetCmdUIContextCookie(ref uicontextBoFileSelected, out pdwCmdUICookie);
      return pdwCmdUICookie;
    }

    int IVsSelectionEvents.OnCmdUIContextChanged(uint dwCmdUICookie, int fActive)
    {
      return 0;
    }

    int IVsSelectionEvents.OnElementValueChanged(uint elementid, object varValueOld, object varValueNew)
    {
      return 0;
    }

    int IVsSelectionEvents.OnSelectionChanged(IVsHierarchy pHierOld, uint itemidOld, IVsMultiItemSelect pMISOld, ISelectionContainer pSCOld, IVsHierarchy pHierNew, uint itemidNew, IVsMultiItemSelect pMISNew, ISelectionContainer pSCNew)
    {
      if (pHierNew != null)
      {
        HierarchyNode selectedNode = CopernicusProjectSystemUtil.getSelectedNode();
        if (selectedNode != null)
          selectedNode.ReDraw(UIHierarchyElement.Icon);
        object pvar;
        pHierNew.GetProperty(itemidNew, -2012, out pvar);
        if (pvar != null && pvar.ToString().EndsWith(".bo"))
        {
          SelectionEvents.SelectionService.SetCmdUIContext(SelectionEvents.ContextCookie, 1);
          return 0;
        }
      }
      SelectionEvents.SelectionService.SetCmdUIContext(SelectionEvents.ContextCookie, 0);
      return 0;
    }
  }
}
