﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ToolOptions.SolutionContentImport
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using EnvDTE;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Util;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace SAP.Copernicus.ToolOptions
{
  public class SolutionContentImport : Form
  {
    private static string MainFolder = "Main";
    private static string BCFolder = "BC";
    private static string AttrExt = ".attr";
    private IContainer components;
    private FolderBrowserDialog folderBrowserDialog;
    private Label label1;
    private Label label2;
    private TextBox textBoxSourceDir;
    private TextBox textBoxImportLog;
    private Button buttonSelSource;
    private Label label3;
    private Button buttonExit;
    private Button buttonImport;
    private Label importSolution;
    private CopernicusProjectNode project;
    private XRepMapper xRepMapper;
    private string sourceProjectName;
    private string sourceNamespace;
    private string sourceNamespacePrefix;
    private string sourceRepositoryRootFolder;
    private string sourceDefaultPC;
    private string sourceDevPackage;
    private string sourceXRepSolution;
    private string sourceBCSourceFolder;
    private string sourceProjectSourceFolder;
    private string projectProjectName;
    private string projectNamespace;
    private string projectNamespacePrefix;
    private string projectRepositoryRootFolder;
    private string projectDefaultPC;
    private string projectDevPackage;
    private string projectXRepSolution;
    private string projectBCSourceFolder;
    private string projectProjectSourceFolder;
    private List<SolutionContentImport.Replacement> replacements;
    private List<string> failedBOs;
    private string sourceDir;
    private string bcDir;
    private string targetDir;
    private string targetBCDir;
    private Stopwatch stopwatch;
    private bool stopwatchStarted;
    private XmlNodeList itemGroups;
    private Guid projectGuid;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.folderBrowserDialog = new FolderBrowserDialog();
      this.label1 = new Label();
      this.label2 = new Label();
      this.textBoxSourceDir = new TextBox();
      this.textBoxImportLog = new TextBox();
      this.buttonSelSource = new Button();
      this.label3 = new Label();
      this.buttonExit = new Button();
      this.buttonImport = new Button();
      this.importSolution = new Label();
      this.SuspendLayout();
      this.label1.AutoSize = true;
      this.label1.Location = new Point(8, 38);
      this.label1.Name = "label1";
      this.label1.Size = new Size(76, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Source Folder:";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(8, 9);
      this.label2.Name = "label2";
      this.label2.Size = new Size(80, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Import Solution:";
      this.textBoxSourceDir.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.textBoxSourceDir.BackColor = SystemColors.Window;
      this.textBoxSourceDir.Location = new Point(97, 35);
      this.textBoxSourceDir.Name = "textBoxSourceDir";
      this.textBoxSourceDir.Size = new Size(435, 20);
      this.textBoxSourceDir.TabIndex = 1;
      this.textBoxSourceDir.Text = "C:\\SolutionContent";
      this.textBoxImportLog.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.textBoxImportLog.BackColor = SystemColors.Window;
      this.textBoxImportLog.Location = new Point(12, 93);
      this.textBoxImportLog.Multiline = true;
      this.textBoxImportLog.Name = "textBoxImportLog";
      this.textBoxImportLog.ReadOnly = true;
      this.textBoxImportLog.ScrollBars = ScrollBars.Both;
      this.textBoxImportLog.Size = new Size(560, 218);
      this.textBoxImportLog.TabIndex = 0;
      this.textBoxImportLog.WordWrap = false;
      this.buttonSelSource.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.buttonSelSource.Location = new Point(538, 33);
      this.buttonSelSource.Name = "buttonSelSource";
      this.buttonSelSource.Size = new Size(34, 23);
      this.buttonSelSource.TabIndex = 2;
      this.buttonSelSource.Text = "...";
      this.buttonSelSource.UseVisualStyleBackColor = true;
      this.buttonSelSource.Click += new EventHandler(this.buttonSelSource_Click);
      this.label3.AutoSize = true;
      this.label3.Location = new Point(9, 77);
      this.label3.Name = "label3";
      this.label3.Size = new Size(60, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "Import Log:";
      this.buttonExit.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonExit.Location = new Point(497, 327);
      this.buttonExit.Name = "buttonExit";
      this.buttonExit.Size = new Size(75, 23);
      this.buttonExit.TabIndex = 6;
      this.buttonExit.Text = "Exit";
      this.buttonExit.UseVisualStyleBackColor = true;
      this.buttonExit.Click += new EventHandler(this.buttonExit_Click);
      this.buttonImport.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonImport.Location = new Point(425, 327);
      this.buttonImport.Name = "buttonImport";
      this.buttonImport.Size = new Size(66, 23);
      this.buttonImport.TabIndex = 5;
      this.buttonImport.Text = "Import";
      this.buttonImport.UseVisualStyleBackColor = true;
      this.buttonImport.Click += new EventHandler(this.buttonImport_Click);
      this.importSolution.AutoSize = true;
      this.importSolution.Location = new Point(94, 9);
      this.importSolution.Name = "importSolution";
      this.importSolution.Size = new Size(16, 13);
      this.importSolution.TabIndex = 7;
      this.importSolution.Text = "---";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(584, 362);
      this.Controls.Add((Control) this.importSolution);
      this.Controls.Add((Control) this.buttonImport);
      this.Controls.Add((Control) this.buttonExit);
      this.Controls.Add((Control) this.buttonSelSource);
      this.Controls.Add((Control) this.textBoxImportLog);
      this.Controls.Add((Control) this.textBoxSourceDir);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.label1);
      this.Name = nameof (SolutionContentImport);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Import Solution Content";
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public SolutionContentImport()
    {
      this.InitializeComponent();
      this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;
      this.folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;
      if (!string.IsNullOrEmpty(this.textBoxSourceDir.Text))
        this.folderBrowserDialog.SelectedPath = this.textBoxSourceDir.Text;
      this.Activated += new EventHandler(this.ActivatedEvent);
    }

    public bool IsSolutionOpen()
    {
      try
      {
        Solution solution = DTEUtil.GetDTE().Solution;
        foreach (Project project in solution.Projects)
        {
          this.project = project.Object as CopernicusProjectNode;
          if (this.project != null)
          {
            this.importSolution.Text = Path.GetFileNameWithoutExtension(solution.FullName) + " - " + this.project.Caption;
            this.projectNamespace = this.project.RepositoryNamespace;
            this.selectProject();
            return true;
          }
        }
      }
      catch (NullReferenceException ex)
      {
        int num = (int) CopernicusMessageBox.Show("Open the solution from which the content should be exported!");
      }
      return false;
    }

    private void selectProject()
    {
      UIHierarchyItems uiHierarchyItems1 = DTEUtil.GetDTE().ToolWindows.SolutionExplorer.UIHierarchyItems;
      if (uiHierarchyItems1.Count != 1)
        return;
      UIHierarchyItems uiHierarchyItems2 = uiHierarchyItems1.Item((object) 1).UIHierarchyItems;
      if (uiHierarchyItems2.Count != 1)
        return;
      uiHierarchyItems2.Item((object) 1).Select(vsUISelectionType.vsUISelectionTypeSelect);
    }

    private void ActivatedEvent(object sender, EventArgs e)
    {
      this.buttonExit.Focus();
    }

    private void buttonExit_Click(object sender, EventArgs e)
    {
      this.Close();
      this.Dispose();
    }

    private void buttonImport_Click(object sender, EventArgs e)
    {
      List<string> logLines = new List<string>();
      this.textBoxImportLog.Lines = new string[0];
      this.Refresh();
      this.xRepMapper = XRepMapper.GetInstance();
      this.sourceDir = this.textBoxSourceDir.Text + (object) Path.DirectorySeparatorChar + SolutionContentImport.MainFolder;
      this.bcDir = this.textBoxSourceDir.Text + (object) Path.DirectorySeparatorChar + SolutionContentImport.BCFolder;
      this.targetDir = this.project.BaseURI.Directory;
      this.targetBCDir = XRepMapper.GetInstance().GetBCFolderPath();
      if (!string.IsNullOrEmpty(this.sourceDir) && Directory.Exists(this.sourceDir) && (!string.IsNullOrEmpty(this.targetDir) && Directory.Exists(this.targetDir)))
      {
        this.unloadProject();
        this.stopwatch = new Stopwatch();
        this.stopwatch.Start();
        this.stopwatchStarted = true;
        int count = 0;
        bool flag = false;
        logLines.Add("Importing content into solution " + this.importSolution.Text + " ...");
        using (List<string>.Enumerator enumerator = FileUtil.GetFilesFromDir(this.sourceDir, "*.myproj").GetEnumerator())
        {
          if (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            try
            {
              XmlDocument xmlDocument = new XmlDocument();
              xmlDocument.LoadXml(this.ImportSource(current, false));
              XmlElement documentElement = xmlDocument.DocumentElement;
              XmlElement xmlElement = documentElement["PropertyGroup"];
              this.sourceProjectName = xmlElement["Name"].InnerText;
              this.sourceNamespace = xmlElement["RepositoryNamespace"].InnerText;
              this.sourceNamespacePrefix = xmlElement["RuntimeNamespacePrefix"].InnerText;
              this.sourceRepositoryRootFolder = xmlElement["RepositoryRootFolder"].InnerText;
              this.sourceDefaultPC = xmlElement["DefaultProcessComponent"].InnerText;
              this.sourceDevPackage = xmlElement["DevelopmentPackage"].InnerText;
              this.sourceXRepSolution = xmlElement["XRepSolution"].InnerText;
              this.sourceBCSourceFolder = xmlElement["BCSourceFolderInXRep"].InnerText;
              this.sourceProjectSourceFolder = xmlElement["ProjectSourceFolderinXRep"].InnerText;
              this.itemGroups = documentElement.GetElementsByTagName("ItemGroup");
            }
            catch (Exception ex)
            {
              flag = true;
              logLines.Add("Error: " + ex.Message);
            }
          }
        }
        if (string.IsNullOrEmpty(this.sourceProjectName) || string.IsNullOrEmpty(this.sourceNamespace) || string.IsNullOrEmpty(this.sourceNamespacePrefix))
        {
          flag = true;
          logLines.Add("Error: Project file could not be found in source folder or project file is corrupt!");
        }
        using (List<string>.Enumerator enumerator = FileUtil.GetFilesFromDir(this.targetDir, "*.myproj").GetEnumerator())
        {
          if (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            try
            {
              XmlDocument xmlDocument = new XmlDocument();
              xmlDocument.LoadXml(this.ImportSource(current, false));
              XmlElement documentElement = xmlDocument.DocumentElement;
              XmlElement xmlElement = documentElement["PropertyGroup"];
              this.projectProjectName = xmlElement["Name"].InnerText;
              this.projectNamespace = xmlElement["RepositoryNamespace"].InnerText;
              this.projectNamespacePrefix = xmlElement["RuntimeNamespacePrefix"].InnerText;
              this.projectRepositoryRootFolder = xmlElement["RepositoryRootFolder"].InnerText;
              this.projectDefaultPC = xmlElement["DefaultProcessComponent"].InnerText;
              this.projectDevPackage = xmlElement["DevelopmentPackage"].InnerText;
              this.projectXRepSolution = xmlElement["XRepSolution"].InnerText;
              this.projectBCSourceFolder = xmlElement["BCSourceFolderInXRep"].InnerText;
              this.projectProjectSourceFolder = xmlElement["ProjectSourceFolderinXRep"].InnerText;
              this.prepareReplacements();
              if (!flag)
              {
                if (!string.IsNullOrEmpty(this.projectProjectName))
                {
                  if (!string.IsNullOrEmpty(this.projectNamespacePrefix))
                  {
                    if (this.projectNamespacePrefix.StartsWith("/") && !this.sourceNamespacePrefix.StartsWith("/"))
                    {
                      logLines.Add("Warning: Importing local solution content into a global solution can cause inconsistencies (e.g. in SADLs, BC XML, etc....)!");
                      if (CopernicusMessageBox.Show("Importing local solution content into a global solution can cause inconsistencies (e.g. in SADLs, BC XML, etc....)!", "Continue solution content import?", MessageBoxButtons.YesNo).Equals((object) DialogResult.No))
                      {
                        flag = true;
                        logLines.Add("Info: Import canceled!");
                      }
                    }
                    else if (!this.projectNamespacePrefix.StartsWith("/") && this.sourceNamespacePrefix.StartsWith("/"))
                    {
                      logLines.Add("Warning: Importing global solution content into a local solution can cause inconsistencies (e.g. in SADLs, BC XML, etc....)!");
                      if (CopernicusMessageBox.Show("Importing global solution content into a local solution can cause inconsistencies (e.g. in SADLs, BC XML, etc....)!", "Continue solution content import?", MessageBoxButtons.YesNo).Equals((object) DialogResult.No))
                      {
                        flag = true;
                        logLines.Add("Info: Import canceled!");
                      }
                    }
                    if (!flag)
                    {
                      XmlNodeList elementsByTagName = documentElement.GetElementsByTagName("ItemGroup");
                      if (elementsByTagName != null)
                      {
                        for (int index = elementsByTagName.Count - 1; index >= 0; --index)
                          documentElement.RemoveChild(elementsByTagName[index]);
                      }
                      foreach (XmlNode itemGroup in this.itemGroups)
                      {
                        XmlNode newChild = xmlDocument.ImportNode(itemGroup, true);
                        documentElement.AppendChild(newChild);
                      }
                      FileInfo fileInfo = new FileInfo(current);
                      fileInfo.IsReadOnly = false;
                      MemoryStream memoryStream = new MemoryStream();
                      xmlDocument.Save((TextWriter) new StreamWriter((Stream) memoryStream));
                      memoryStream.Position = 0L;
                      string str = this.replaceContent(new StreamReader((Stream) memoryStream).ReadToEnd().Replace(this.projectNamespace, this.sourceNamespace), current);
                      File.WriteAllText(current, str);
                      fileInfo.IsReadOnly = true;
                      this.xRepMapper.SaveAndActivateLocalFileInXRep(current, str, (IDictionary<string, string>) null, false);
                      logLines.Add("Project file adjusted...");
                    }
                  }
                }
              }
            }
            catch (Exception ex)
            {
              flag = true;
              logLines.Add("Error: " + ex.Message);
            }
          }
        }
        if (string.IsNullOrEmpty(this.projectProjectName) || string.IsNullOrEmpty(this.projectNamespacePrefix))
        {
          flag = true;
          logLines.Add("Error: Project file could not be found in target folder or project file is corrupt!");
        }
        if (!flag)
        {
          if (this.sourceDir != null && Directory.Exists(this.sourceDir))
          {
            logLines.Add("Importing content into solution...");
            List<string> filesFromDir = FileUtil.GetFilesFromDir(this.sourceDir, "*.*");
            foreach (string currentFile in filesFromDir)
            {
              ++count;
              this.importFile(currentFile, count, filesFromDir.Count, ref logLines, true);
            }
          }
          else
            logLines.Add("Error: Source content folder empty or does not exist.");
          if (this.bcDir != null && Directory.Exists(this.bcDir))
          {
            logLines.Add("----");
            logLines.Add("Importing business configuration into solution...");
            count = 0;
            List<string> filesFromDir = FileUtil.GetFilesFromDir(this.bcDir, "*.*");
            foreach (string currentFile in filesFromDir)
            {
              ++count;
              this.importFile(currentFile, count, filesFromDir.Count, ref logLines, false);
            }
          }
          else
            logLines.Add("Warning: Source BC folder empty or does not exist.");
          this.loadProject();
        }
        this.stopwatch.Stop();
        this.stopwatchStarted = false;
        logLines.Add("--------------------");
        logLines.Add("Import finished in " + (object) this.stopwatch.Elapsed + " (" + (object) count + " files).");
        this.textBoxImportLog.Lines = logLines.ToArray();
        this.textBoxImportLog.SelectionStart = this.textBoxImportLog.TextLength;
        this.textBoxImportLog.ScrollToCaret();
        this.Refresh();
      }
      else
      {
        if (string.IsNullOrEmpty(this.sourceDir) || !Directory.Exists(this.sourceDir))
          logLines.Add("Source folder does not exist!");
        if (string.IsNullOrEmpty(this.targetDir) || !Directory.Exists(this.targetDir))
          logLines.Add("Target folder does not exist!");
        this.textBoxImportLog.Lines = logLines.ToArray();
        this.textBoxImportLog.SelectionStart = this.textBoxImportLog.TextLength;
        this.textBoxImportLog.ScrollToCaret();
        this.Refresh();
      }
    }

    private bool importFile(string currentFile, int count, int fileCount, ref List<string> logLines, bool main)
    {
      bool flag = false;
      logLines.Add("Importing (" + string.Concat((object) count).PadLeft(3, '0') + "/" + string.Concat((object) fileCount).PadLeft(3, '0') + "): " + Path.GetFileName(currentFile));
      string lower = Path.GetExtension(currentFile).ToLower();
      if (!lower.EndsWith(".attr") && !lower.EndsWith(".myproj") && (!lower.EndsWith(".xrep") && !lower.EndsWith(".sln")) && !lower.EndsWith(".suo"))
      {
        if (!lower.EndsWith(".bocd"))
        {
          try
          {
            string str = this.ImportSource(currentFile, true);
            IDictionary<string, string> fileAttribsToBesaved = this.ImportAttributes(currentFile);
            string targetFilename = this.GetTargetFilename(currentFile, main);
            Directory.CreateDirectory(Path.GetDirectoryName(targetFilename));
            if (File.Exists(targetFilename))
              new FileInfo(targetFilename).IsReadOnly = false;
            File.WriteAllText(targetFilename, str);
            new FileInfo(targetFilename).IsReadOnly = true;
            this.xRepMapper.SaveAndActivateLocalFileInXRep(targetFilename, str, fileAttribsToBesaved, false);
            goto label_9;
          }
          catch (Exception ex)
          {
            logLines.Add("Error: " + ex.Message);
            flag = true;
            goto label_9;
          }
        }
      }
      if (lower.EndsWith(".attr"))
        logLines.Add("-> File processed for updating attributes...");
      else
        logLines.Add("-> File skipped...");
label_9:
      this.textBoxImportLog.Lines = logLines.ToArray();
      this.textBoxImportLog.SelectionStart = this.textBoxImportLog.TextLength;
      this.textBoxImportLog.ScrollToCaret();
      this.Refresh();
      return flag;
    }

    public void unloadProject()
    {
      IVsSolution2 globalService = (IVsSolution2) Package.GetGlobalService(typeof (SVsSolution));
      IEnumerator enumerator = DTEUtil.GetDTE().Solution.Projects.GetEnumerator();
      try
      {
        if (!enumerator.MoveNext())
          return;
        IVsHierarchy vsHierarchy = ((Project) enumerator.Current).Object as IVsHierarchy;
        ErrorHandler.ThrowOnFailure(globalService.GetGuidOfProject(vsHierarchy, out this.projectGuid));
        ErrorHandler.ThrowOnFailure(globalService.CloseSolutionElement(65536U, vsHierarchy, 0U));
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
    }

    public void loadProject()
    {
      IVsSolution4 globalService = (IVsSolution2) Package.GetGlobalService(typeof (SVsSolution)) as IVsSolution4;
      Guid projectGuid = this.projectGuid;
      ErrorHandler.ThrowOnFailure(globalService.ReloadProject(ref projectGuid));
      this.IsSolutionOpen();
    }

    private string GetTargetFilename(string filename, bool main)
    {
      if (main)
      {
        int length = this.sourceDir.Length;
        return this.targetDir + filename.Substring(length);
      }
      int length1 = this.bcDir.Length;
      return this.targetBCDir + filename.Substring(length1);
    }

    private void prepareReplacements()
    {
      this.replacements = new List<SolutionContentImport.Replacement>();
      this.replacements.Add(new SolutionContentImport.Replacement(this.sourceNamespace, this.projectNamespace));
      this.replacements.Add(new SolutionContentImport.Replacement(this.sourceProjectSourceFolder, this.projectProjectSourceFolder));
      this.replacements.Add(new SolutionContentImport.Replacement(this.sourceBCSourceFolder, this.projectBCSourceFolder));
      this.replacements.Add(new SolutionContentImport.Replacement(this.sourceRepositoryRootFolder, this.projectRepositoryRootFolder));
      this.replacements.Add(new SolutionContentImport.Replacement(this.sourceXRepSolution, this.projectXRepSolution));
      this.replacements.Add(new SolutionContentImport.Replacement(this.sourceDefaultPC, this.projectDefaultPC));
      this.replacements.Add(new SolutionContentImport.Replacement(this.sourceDevPackage, this.projectDevPackage));
      this.replacements.Add(new SolutionContentImport.Replacement(this.sourceNamespacePrefix, this.projectNamespacePrefix));
      if (!this.projectNamespacePrefix.StartsWith("/") && this.sourceNamespacePrefix.StartsWith("/"))
        this.replacements.Add(new SolutionContentImport.Replacement(this.sourceNamespacePrefix.Replace("/", "\\/"), this.projectNamespacePrefix, ".sadl"));
      this.replacements.Add(new SolutionContentImport.Replacement(this.sourceProjectName, this.projectProjectName));
      if (!this.projectNamespacePrefix.StartsWith("/") || this.sourceNamespacePrefix.StartsWith("/"))
        return;
      this.replacements.Add(new SolutionContentImport.Replacement("binding=\"" + this.projectNamespacePrefix, "binding=\"" + this.projectNamespacePrefix.Replace("/", "\\/"), ".sadl"));
      this.replacements.Add(new SolutionContentImport.Replacement("name=\"" + this.projectNamespacePrefix, "name=\"" + this.projectProjectName + "_", ".sadl"));
    }

    private string replaceContent(string content, string filename)
    {
      foreach (SolutionContentImport.Replacement replacement in this.replacements)
        content = replacement.Replace(content, filename);
      return content;
    }

    private string ImportSource(string filename, bool replace)
    {
      string content = File.ReadAllText(filename);
      if (replace)
        content = this.replaceContent(content, filename);
      return content;
    }

    private IDictionary<string, string> ImportAttributes(string filename)
    {
      IDictionary<string, string> dictionary = (IDictionary<string, string>) new Dictionary<string, string>();
      string path = filename + SolutionContentImport.AttrExt;
      if (File.Exists(path))
      {
        foreach (string readAllLine in File.ReadAllLines(path, Encoding.UTF8))
        {
          char[] chArray = new char[1]{ '=' };
          string[] strArray = readAllLine.Split(chArray);
          if (strArray.Length == 2)
          {
            string key = strArray[0];
            string str = this.replaceContent(strArray[1], filename);
            dictionary.Add(key, str);
          }
        }
      }
      return dictionary;
    }

    private void buttonSelSource_Click(object sender, EventArgs e)
    {
      int num = (int) this.folderBrowserDialog.ShowDialog();
      this.textBoxSourceDir.Text = this.folderBrowserDialog.SelectedPath;
    }

    private class Replacement
    {
      private string from;
      private string to;
      private string extension;

      public Replacement(string from, string to)
      {
        this.from = from;
        this.to = to;
        this.extension = (string) null;
      }

      public Replacement(string from, string to, string extension)
        : this(from, to)
      {
        this.extension = extension;
      }

      public string Replace(string content, string filename)
      {
        if (this.extension == null || Path.GetExtension(filename).EndsWith(this.extension))
          content = content.Replace(this.from, this.to);
        return content;
      }
    }
  }
}
