﻿using SAP.Copernicus.Core.Util;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyFileVersion("142.0.3319.0231")]
[assembly: AssemblyCSN("AP-RC-BDS")]
[assembly: InternalsVisibleTo("CopernicusTest")]
[assembly: SuppressMessage("Microsoft.Design", "CA1017:MarkAssembliesWithComVisible")]
[assembly: AssemblyTitle("Copernicus")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SAP AG")]
[assembly: AssemblyProduct("SAP® Business ByDesign™")]
[assembly: AssemblyCopyright("© 2011 SAP AG. All rights reserved.")]
[assembly: AssemblyTrademark("SAP and Business ByDesign are trademark(s) or registered trademark(s) of SAP AG in Germany and in several other countries.")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyVersion("142.0.3319.231")]
