﻿// Decompiled with JetBrains decompiler
// Type: com.sap.JSONConnector.JSONClient
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace com.sap.JSONConnector
{
  public class JSONClient
  {
    private static CookieContainer sharedCookieContainer = new CookieContainer();
    private CookieContainer statefulCookieContainer = new CookieContainer();
    internal const string TRACE_LOCATION = "JSONClient";
    private bool protocolExceptionShown;
    private Connection connection;
    private bool debug;

    internal static void ResetCookies()
    {
      JSONClient.sharedCookieContainer = new CookieContainer();
    }

    internal static void SetCookies(CookieContainer cookies)
    {
      JSONClient.sharedCookieContainer = cookies;
    }

    public static JSONClient getInstance(Connection connection)
    {
      return new JSONClient() { connection = connection };
    }

    private CookieContainer CookieContainer
    {
      get
      {
        if (this.connection.stateful)
          return this.statefulCookieContainer;
        return JSONClient.sharedCookieContainer;
      }
    }

    [Conditional("DEBUG")]
    private void EnableDebug()
    {
      this.debug = true;
    }

    public string resultURL(bool logoffSession = false)
    {
      int num = !this.connection.stateful ? 0 : 1;
      if (logoffSession)
        num = 2;
      string str = this.connection.url + "?stateful=" + (object) num;
      if (this.connection.client != null)
        str = str + "&sap-client=" + this.connection.client;
      if (this.connection.stateful)
        str += "&saml2=disabled";
      return str;
    }

    private string GetNameForTrace(SAPFunctionModule functionModule)
    {
      if (functionModule == null)
        return (string) null;
      if (this.debug)
        return functionModule.GetType().Name;
      return functionModule.getName();
    }

    [STAThread]
    public void callFunctionModule(SAPFunctionModule functionModule, bool no_popup = false, bool logoffSession = false, bool retry = false)
    {
      SAP.Copernicus.Core.Protocol.Connection.getInstance().CheckConnectionChanged();
      Cursor.Current = Cursors.WaitCursor;
      HttpWebResponse httpWebResponse = (HttpWebResponse) null;
      try
      {
        string requestUriString = this.resultURL(logoffSession) + "&fm=" + functionModule.getName();
        string nameForTrace = this.GetNameForTrace(functionModule);
        HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(requestUriString);
        httpWebRequest.Proxy = (IWebProxy) WebUtil.GetProxy();
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        if (this.connection.timeout != 0)
          httpWebRequest.Timeout = this.connection.timeout;
        httpWebRequest.CookieContainer = this.CookieContainer;
        httpWebRequest.Method = "POST";
        httpWebRequest.ServicePoint.Expect100Continue = false;
        httpWebRequest.ContentType = "text/json";
        if (PropertyAccess.GeneralProps.ZipJson)
        {
          httpWebRequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
          httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
        }
        if (this.CookieContainer.Count == 0 && this.connection.user != null && this.connection.pass != null)
        {
          httpWebRequest.PreAuthenticate = true;
          httpWebRequest.Credentials = (ICredentials) new NetworkCredential(this.connection.user, SecureStringUtil.secureStringToString(this.connection.pass));
        }
        Type type = functionModule.GetType();
        object graph = type.GetProperty("Importing").GetValue((object) functionModule, (object[]) null);
        MemoryStream memoryStream1 = new MemoryStream();
        if (graph != null)
        {
          new DataContractJsonSerializer(graph.GetType()).WriteObject((Stream) memoryStream1, graph);
          memoryStream1.Position = 0L;
        }
        string s = "{\"IMPORTING\":" + new StreamReader((Stream) memoryStream1).ReadToEnd() + "}";
        byte[] bytes = Encoding.UTF8.GetBytes(s);
        string str1 = string.Empty;
        if (s != null)
          str1 = s.Substring(0, Math.Min(s.Length, 120));
        if (TraceUtil.Instance.IsOn)
        {
          using (StreamWriter streamWriter = new StreamWriter(TraceUtil.Instance.GetNextFile("JSON", 40.ToString() + nameForTrace + (object) ')' + (object) '_' + functionModule.getName() + ".json")))
            streamWriter.Write(s);
        }
        Trace.WriteLine(string.Format("calling (FM = {0} {1}) ...", (object) nameForTrace, (object) str1), nameof (JSONClient));
        bool flag = false;
        WebException webException = (WebException) null;
        try
        {
          httpWebRequest.ContentLength = (long) bytes.Length;
          Stream requestStream = httpWebRequest.GetRequestStream();
          requestStream.Write(bytes, 0, bytes.Length);
          requestStream.Close();
          httpWebResponse = (HttpWebResponse) httpWebRequest.GetResponse();
        }
        catch (WebException ex)
        {
          flag = ex.Status == WebExceptionStatus.Timeout;
          httpWebResponse = ex.Response as HttpWebResponse;
          webException = !flag ? ex : new WebException(!(functionModule.getName() == "00163E0115B01DDFB194EC88B8EE4C9B") ? Resource.MessageTimeOut : Resource.MessageTimeOutActivate, (Exception) ex);
        }
        if (httpWebResponse == null && webException != null)
        {
          Trace.WriteLine(string.Format("Error: call failed: {2} (FM = {0} {1}) ", (object) nameForTrace, (object) str1, (object) webException.Message), nameof (JSONClient));
          throw new ProtocolException(ProtocolException.ErrorArea.CLIENT, new List<SAP.Copernicus.Core.Protocol.JSON.Message>() { new SAP.Copernicus.Core.Protocol.JSON.Message(SAP.Copernicus.Core.Protocol.JSON.Message.MessageSeverity.Error, webException.Message) });
        }
        if (flag || this.connection.timeout != 0 && stopwatch.ElapsedMilliseconds > (long) this.connection.timeout)
        {
          Trace.WriteLine(string.Format("Error: call timed out after: {2} (FM = {0} {1})", (object) nameForTrace, (object) str1, (object) stopwatch.Elapsed), nameof (JSONClient));
          throw new ProtocolException(ProtocolException.ErrorArea.CLIENT, string.Format("Backend call timed out after {0}. Please retry the last operation.", (object) stopwatch.Elapsed));
        }
        if (httpWebResponse.StatusCode != HttpStatusCode.OK)
        {
          ProtocolException.ErrorArea area = ProtocolException.ErrorArea.CLIENT;
          string message;
          if (httpWebResponse.StatusCode == HttpStatusCode.Unauthorized)
          {
            if (!retry)
            {
              if (httpWebResponse != null)
                httpWebResponse.Close();
              JSONClient.ResetCookies();
              this.callFunctionModule(functionModule, no_popup, logoffSession, true);
              Cursor.Current = Cursors.Default;
              return;
            }
            message = "Logon failed because of wrong user or password or because your user is locked. Try again. If you still cannot log on, contact an administrator.";
            area = ProtocolException.ErrorArea.NONE;
          }
          else if (httpWebResponse.StatusCode == HttpStatusCode.InternalServerError)
          {
            message = "HTTP 500 (InternalServerError) - Dump or unhandled exception in backend.";
            area = ProtocolException.ErrorArea.NONE;
          }
          else if (httpWebResponse.StatusCode == HttpStatusCode.BadRequest)
          {
            message = Resource.MsgMissingAuthorization;
            area = ProtocolException.ErrorArea.NONE;
          }
          else
          {
            message = "JSON communiation error. HTTP response code: " + (object) httpWebResponse.StatusCode;
            if (httpWebResponse.StatusDescription != null)
              message = message + " (" + httpWebResponse.StatusDescription + ")";
          }
          Trace.WriteLine(string.Format("Error: call failed: {2} (FM = {0} {1})", (object) nameForTrace, (object) str1, (object) message), nameof (JSONClient));
          throw new ProtocolException(area, message);
        }
        DataContractJsonSerializer contractJsonSerializer = new DataContractJsonSerializer(functionModule.GetType());
        string str2 = new StreamReader(httpWebResponse.GetResponseStream()).ReadToEnd().Replace("\r", "").Replace("\n", "").Replace("\t", "");
        MemoryStream memoryStream2 = new MemoryStream();
        StreamWriter streamWriter1 = new StreamWriter((Stream) memoryStream2);
        streamWriter1.Write(str2);
        streamWriter1.Flush();
        memoryStream2.Position = 0L;
        SAPFunctionModule sapFunctionModule = (SAPFunctionModule) contractJsonSerializer.ReadObject((Stream) memoryStream2);
        string str3 = string.Empty;
        if (str2 != null)
          str3 = str2.Substring(0, Math.Min(str2.Length, 120));
        object obj1 = type.GetProperty("Exporting").GetValue((object) sapFunctionModule, (object[]) null);
        type.GetProperty("Exporting").SetValue((object) functionModule, obj1, (object[]) null);
        object obj2 = type.GetProperty("Tables").GetValue((object) sapFunctionModule, (object[]) null);
        type.GetProperty("Tables").SetValue((object) functionModule, obj2, (object[]) null);
        object obj3 = type.GetProperty("Changing").GetValue((object) sapFunctionModule, (object[]) null);
        type.GetProperty("Changing").SetValue((object) functionModule, obj3, (object[]) null);
        httpWebResponse.Close();
        stopwatch.Stop();
        Trace.WriteLine(string.Format("call took: {3} (FM = {0} {1} {2}) ", (object) nameForTrace, (object) str1, (object) str3, (object) stopwatch.Elapsed), nameof (JSONClient));
        this.protocolExceptionShown = false;
      }
      catch (ThreadInterruptedException ex)
      {
        Trace.WriteLine(string.Format("thread '{0}' has been interrupted", (object) Thread.CurrentThread.Name));
        Trace.WriteLine((object) ex);
        throw ex;
      }
      catch (ThreadAbortException ex)
      {
        Trace.WriteLine(string.Format("thread '{0}' has been aborted", (object) Thread.CurrentThread.Name));
        Trace.WriteLine((object) ex);
        throw ex;
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException ?? new ProtocolException(ProtocolException.ErrorArea.CLIENT, string.Format("Severe error in JSONClient: {0}", (object) ex.Message), ex);
        if (no_popup || this.protocolExceptionShown)
          return;
        this.protocolExceptionShown = true;
        protocolException.showPopup();
      }
      finally
      {
        if (httpWebResponse != null)
          httpWebResponse.Close();
        Cursor.Current = Cursors.Default;
      }
    }
  }
}
