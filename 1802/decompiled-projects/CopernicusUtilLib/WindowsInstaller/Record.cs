﻿// Decompiled with JetBrains decompiler
// Type: WindowsInstaller.Record
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace WindowsInstaller
{
  [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
  [CompilerGenerated]
  [TypeIdentifier]
  [Guid("000C1093-0000-0000-C000-000000000046")]
  [ComImport]
  public interface Record
  {
    [DispId(1)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.BStr)]
    string get_StringData([In] int Field);

    [DispId(1)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    void set_StringData([In] int Field, [MarshalAs(UnmanagedType.BStr), In] string value);
  }
}
