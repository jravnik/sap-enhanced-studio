﻿// Decompiled with JetBrains decompiler
// Type: AdvancedDataGridView.ExpandingEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;

namespace AdvancedDataGridView
{
  public class ExpandingEventArgs : CancelEventArgs
  {
    private TreeGridNode _node;

    private ExpandingEventArgs()
    {
    }

    public ExpandingEventArgs(TreeGridNode node)
    {
      this._node = node;
    }

    public TreeGridNode Node
    {
      get
      {
        return this._node;
      }
    }
  }
}
