﻿// Decompiled with JetBrains decompiler
// Type: Microsoft.VisualStudio.VSHelp80.Help2
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.VSHelp;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Microsoft.VisualStudio.VSHelp80
{
  [TypeIdentifier]
  [CompilerGenerated]
  [Guid("78413D2D-0492-4A9B-AB25-730633679977")]
  [ComImport]
  public interface Help2 : Help
  {
    [SpecialName]
    void _VtblGap1_9();

    [DispId(10)]
    void DisplayTopicFromF1Keyword([MarshalAs(UnmanagedType.BStr), In] string pszKeyword);
  }
}
