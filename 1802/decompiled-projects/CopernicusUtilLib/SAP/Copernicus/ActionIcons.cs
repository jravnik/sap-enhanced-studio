﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ActionIcons
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus
{
    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [DebuggerNonUserCode]
    [CompilerGenerated]
    public class ActionIcons
    {
        private static ResourceManager resourceMan;
        private static CultureInfo resourceCulture;

        internal ActionIcons()
        {
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public static ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals((object)ActionIcons.resourceMan, (object)null))
                    ActionIcons.resourceMan = new ResourceManager("SAP.Copernicus.ActionIcons", typeof(ActionIcons).Assembly);
                return ActionIcons.resourceMan;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public static CultureInfo Culture
        {
            get
            {
                return ActionIcons.resourceCulture;
            }
            set
            {
                ActionIcons.resourceCulture = value;
            }
        }

        public static Bitmap _170DeployBusinessConfiguration16x16
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(_170DeployBusinessConfiguration16x16), ActionIcons.resourceCulture);
            }
        }

        public static Bitmap Activate
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(Activate), ActionIcons.resourceCulture);
            }
        }

        public static Bitmap ActivateAll
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(ActivateAll), ActionIcons.resourceCulture);
            }
        }

        public static Bitmap ActivateChanged
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(ActivateChanged), ActionIcons.resourceCulture);
            }
        }

        public static Bitmap CheckConsistency
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(CheckConsistency), ActionIcons.resourceCulture);
            }
        }

        public static Bitmap CheckRuntime
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(CheckRuntime), ActionIcons.resourceCulture);
            }
        }

        public static Bitmap Clean
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(Clean), ActionIcons.resourceCulture);
            }
        }

        public static Bitmap Delete
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(Delete), ActionIcons.resourceCulture);
            }
        }

        public static Icon Error
        {
            get
            {
                return (Icon)ActionIcons.ResourceManager.GetObject(nameof(Error), ActionIcons.resourceCulture);
            }
        }

        public static Bitmap Help
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(Help), ActionIcons.resourceCulture);
            }
        }

        public static Icon OK
        {
            get
            {
                return (Icon)ActionIcons.ResourceManager.GetObject(nameof(OK), ActionIcons.resourceCulture);
            }
        }

        public static Icon YellowOK
        {
            get
            {
                return (Icon) ActionIcons.ResourceManager.GetObject(nameof(YellowOK), ActionIcons.resourceCulture);
            }
        }

public static Bitmap OpenInUIDesigner
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(OpenInUIDesigner), ActionIcons.resourceCulture);
            }
        }

        public static Bitmap Refresh
        {
            get
            {
                return (Bitmap)ActionIcons.ResourceManager.GetObject(nameof(Refresh), ActionIcons.resourceCulture);
            }
        }
    }
}
