﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Resource
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  public class Resource
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resource()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) Resource.resourceMan, (object) null))
          Resource.resourceMan = new ResourceManager("SAP.Copernicus.Resource", typeof (Resource).Assembly);
        return Resource.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return Resource.resourceCulture;
      }
      set
      {
        Resource.resourceCulture = value;
      }
    }

    public static string BackgroundCheckDisabled
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (BackgroundCheckDisabled), Resource.resourceCulture);
      }
    }

    public static string BackgroundCheckEnabled
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (BackgroundCheckEnabled), Resource.resourceCulture);
      }
    }

    public static string BackgroundCheckfailed
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (BackgroundCheckfailed), Resource.resourceCulture);
      }
    }

    public static string BCDeploymentFailed
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (BCDeploymentFailed), Resource.resourceCulture);
      }
    }

    public static string BCDeploymentFinished
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (BCDeploymentFinished), Resource.resourceCulture);
      }
    }

    public static string BCDeploymentStarted
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (BCDeploymentStarted), Resource.resourceCulture);
      }
    }

    public static string BOCheckTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (BOCheckTitle), Resource.resourceCulture);
      }
    }

    public static string BOSaveMissing
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (BOSaveMissing), Resource.resourceCulture);
      }
    }

    public static string CanNotCreateWindow
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (CanNotCreateWindow), Resource.resourceCulture);
      }
    }

    public static string CheckTriggeredInBackground
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (CheckTriggeredInBackground), Resource.resourceCulture);
      }
    }

    public static string CloseCorrection
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (CloseCorrection), Resource.resourceCulture);
      }
    }

    public static string CloseCorrectionFailed
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (CloseCorrectionFailed), Resource.resourceCulture);
      }
    }

    public static string ContactPersonEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ContactPersonEmpty), Resource.resourceCulture);
      }
    }

    public static string CopernicusAppExitFilterValueF4Tip
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (CopernicusAppExitFilterValueF4Tip), Resource.resourceCulture);
      }
    }

    public static string CreateScreenShortIdTextBoxWarningLabel
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (CreateScreenShortIdTextBoxWarningLabel), Resource.resourceCulture);
      }
    }

    public static string CustomerID
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (CustomerID), Resource.resourceCulture);
      }
    }

    public static string CustomerName
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (CustomerName), Resource.resourceCulture);
      }
    }

    public static string DeploymentUnitEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (DeploymentUnitEmpty), Resource.resourceCulture);
      }
    }

    public static string Deprecated
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (Deprecated), Resource.resourceCulture);
      }
    }

    public static string DevPartnerEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (DevPartnerEmpty), Resource.resourceCulture);
      }
    }

    public static string Dist_Notpossible
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (Dist_Notpossible), Resource.resourceCulture);
      }
    }

    public static string Dist_Success
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (Dist_Success), Resource.resourceCulture);
      }
    }

    public static string DumpAnalysisLogonRequired
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (DumpAnalysisLogonRequired), Resource.resourceCulture);
      }
    }

    public static string EmailEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (EmailEmpty), Resource.resourceCulture);
      }
    }

    public static string EmailValidationFailed
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (EmailValidationFailed), Resource.resourceCulture);
      }
    }

    public static string InvalidProjectsLocation_0_reason_1
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (InvalidProjectsLocation_0_reason_1), Resource.resourceCulture);
      }
    }

    public static string InvalidToolsOptions
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (InvalidToolsOptions), Resource.resourceCulture);
      }
    }

    public static string KEY_USER_SOLUTION_DISCLAIMER_MSG
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (KEY_USER_SOLUTION_DISCLAIMER_MSG), Resource.resourceCulture);
      }
    }

    public static string LogonDialog_Error_Auth
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonDialog_Error_Auth), Resource.resourceCulture);
      }
    }

    public static string LogonDialog_Error_Caption
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonDialog_Error_Caption), Resource.resourceCulture);
      }
    }

    public static string LogonDialog_Error_Fatal
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonDialog_Error_Fatal), Resource.resourceCulture);
      }
    }

    public static string LogonDialog_Error_Other
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonDialog_Error_Other), Resource.resourceCulture);
      }
    }

    public static string LogonForm_Btn_Cancel
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonForm_Btn_Cancel), Resource.resourceCulture);
      }
    }

    public static string LogonForm_Btn_Ok
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonForm_Btn_Ok), Resource.resourceCulture);
      }
    }

    public static string LogonForm_Header
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonForm_Header), Resource.resourceCulture);
      }
    }

    public static string LogonForm_Label_Client
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonForm_Label_Client), Resource.resourceCulture);
      }
    }

    public static string LogonForm_Label_Pwd
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonForm_Label_Pwd), Resource.resourceCulture);
      }
    }

    public static string LogonForm_Label_System
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonForm_Label_System), Resource.resourceCulture);
      }
    }

    public static string LogonForm_Label_User
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonForm_Label_User), Resource.resourceCulture);
      }
    }

    public static string LogonForm_Title
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (LogonForm_Title), Resource.resourceCulture);
      }
    }

    public static string MCSDeployToolTip
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MCSDeployToolTip), Resource.resourceCulture);
      }
    }

    public static string MCSLoadToolTip
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MCSLoadToolTip), Resource.resourceCulture);
      }
    }

    public static string MessagePrefixActivate
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MessagePrefixActivate), Resource.resourceCulture);
      }
    }

    public static string MessagePrefixCheck
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MessagePrefixCheck), Resource.resourceCulture);
      }
    }

    public static string MessagePrefixClean
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MessagePrefixClean), Resource.resourceCulture);
      }
    }

    public static string MessagePrefixDelete
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MessagePrefixDelete), Resource.resourceCulture);
      }
    }

    public static string MessageTimeOut
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MessageTimeOut), Resource.resourceCulture);
      }
    }

    public static string MessageTimeOutActivate
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MessageTimeOutActivate), Resource.resourceCulture);
      }
    }

    public static string MsgAllreadyLockedOn
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgAllreadyLockedOn), Resource.resourceCulture);
      }
    }

    public static string MsgBrowserLogonToolTip
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgBrowserLogonToolTip), Resource.resourceCulture);
      }
    }

    public static string MsgBYD
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgBYD), Resource.resourceCulture);
      }
    }

    public static string MsgCancel
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgCancel), Resource.resourceCulture);
      }
    }

    public static string MsgCancelSolutionDeletion
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgCancelSolutionDeletion), Resource.resourceCulture);
      }
    }

    public static string MsgChangeInvalidatesEntries
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgChangeInvalidatesEntries), Resource.resourceCulture);
      }
    }

    public static string MsgConenctionHost
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgConenctionHost), Resource.resourceCulture);
      }
    }

    public static string MsgConnectionName
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgConnectionName), Resource.resourceCulture);
      }
    }

    public static string MsgConnectionNameNotUnique
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgConnectionNameNotUnique), Resource.resourceCulture);
      }
    }

    public static string MsgDisableKeyUser
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgDisableKeyUser), Resource.resourceCulture);
      }
    }

    public static string MsgEnableKeyUser
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgEnableKeyUser), Resource.resourceCulture);
      }
    }

    public static string MsgFailedBCNameBO
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgFailedBCNameBO), Resource.resourceCulture);
      }
    }

    public static string MsgInvalidBCSetAssignmentEpilog
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgInvalidBCSetAssignmentEpilog), Resource.resourceCulture);
      }
    }

    public static string MsgInvalidBCSetAssignmentProlog
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgInvalidBCSetAssignmentProlog), Resource.resourceCulture);
      }
    }

    public static string MsgInvalidBCSetAssignmentTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgInvalidBCSetAssignmentTitle), Resource.resourceCulture);
      }
    }

    public static string MsgInvalidConnectionName
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgInvalidConnectionName), Resource.resourceCulture);
      }
    }

    public static string MsgInvalidNameChars
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgInvalidNameChars), Resource.resourceCulture);
      }
    }

    public static string MsgInvalidNameCharsUpper
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgInvalidNameCharsUpper), Resource.resourceCulture);
      }
    }

    public static string MsgMissingAuthorization
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgMissingAuthorization), Resource.resourceCulture);
      }
    }

    public static string MsgNameFirstChar
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgNameFirstChar), Resource.resourceCulture);
      }
    }

    public static string MsgPasswordToolTip
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgPasswordToolTip), Resource.resourceCulture);
      }
    }

    public static string MsgScalableNotAllowed
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgScalableNotAllowed), Resource.resourceCulture);
      }
    }

    public static string MsgSystemToolTip
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgSystemToolTip), Resource.resourceCulture);
      }
    }

    public static string MsgWebException
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (MsgWebException), Resource.resourceCulture);
      }
    }

    public static string NumberTextBox_Validation_Caption
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (NumberTextBox_Validation_Caption), Resource.resourceCulture);
      }
    }

    public static string NumberTextBox_Validation_Error
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (NumberTextBox_Validation_Error), Resource.resourceCulture);
      }
    }

    public static string OpenCorrection
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (OpenCorrection), Resource.resourceCulture);
      }
    }

    public static string OpenCorrectionFailed
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (OpenCorrectionFailed), Resource.resourceCulture);
      }
    }

    public static string OutputWindowPaneTitleSolution
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (OutputWindowPaneTitleSolution), Resource.resourceCulture);
      }
    }

    public static string QBODLMigrationCannotRevert
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (QBODLMigrationCannotRevert), Resource.resourceCulture);
      }
    }

    public static Icon SAPBusinessByDesignStudioIcon
    {
      get
      {
        return (Icon) Resource.ResourceManager.GetObject(nameof (SAPBusinessByDesignStudioIcon), Resource.resourceCulture);
      }
    }

    public static string ScriptFileReverFailed
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ScriptFileReverFailed), Resource.resourceCulture);
      }
    }

    public static string ScriptFileRevertSuccessful
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ScriptFileRevertSuccessful), Resource.resourceCulture);
      }
    }

    public static string Solution
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (Solution), Resource.resourceCulture);
      }
    }

    public static string SolutioNameEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (SolutioNameEmpty), Resource.resourceCulture);
      }
    }

    public static string SolutionCtxMenuOpen
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (SolutionCtxMenuOpen), Resource.resourceCulture);
      }
    }

    public static string SolutionCtxMenuSolutionProperties
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (SolutionCtxMenuSolutionProperties), Resource.resourceCulture);
      }
    }

    public static string SolutionDescriptionEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (SolutionDescriptionEmpty), Resource.resourceCulture);
      }
    }

    public static string SolutionDetailedDescriptionEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (SolutionDetailedDescriptionEmpty), Resource.resourceCulture);
      }
    }

    public static string SolutionNameInvalid
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (SolutionNameInvalid), Resource.resourceCulture);
      }
    }

    public static string SolutionPropertyTitle
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (SolutionPropertyTitle), Resource.resourceCulture);
      }
    }

    public static string SolutionTypeEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (SolutionTypeEmpty), Resource.resourceCulture);
      }
    }

    public static string TTDelPending
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (TTDelPending), Resource.resourceCulture);
      }
    }

    public static string TTGlobalSol
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (TTGlobalSol), Resource.resourceCulture);
      }
    }

    public static string TTPatchSolution
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (TTPatchSolution), Resource.resourceCulture);
      }
    }

    public static string TTSandboxSol
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (TTSandboxSol), Resource.resourceCulture);
      }
    }

    public static string TTTemplateSolution
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (TTTemplateSolution), Resource.resourceCulture);
      }
    }

    public static string ValueExceedLimit
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (ValueExceedLimit), Resource.resourceCulture);
      }
    }

    public static string Version
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (Version), Resource.resourceCulture);
      }
    }

    public static string WaitCaption
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (WaitCaption), Resource.resourceCulture);
      }
    }

    public static string WaitDeleteSolution
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (WaitDeleteSolution), Resource.resourceCulture);
      }
    }

    public static string WaitScriptCreate
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (WaitScriptCreate), Resource.resourceCulture);
      }
    }

    public static string WaitScriptCreateCanceled
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (WaitScriptCreateCanceled), Resource.resourceCulture);
      }
    }

    public static string WaitScriptCreateFailed
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (WaitScriptCreateFailed), Resource.resourceCulture);
      }
    }

    public static string WaitScriptCreateSuccess
    {
      get
      {
        return Resource.ResourceManager.GetString(nameof (WaitScriptCreateSuccess), Resource.resourceCulture);
      }
    }

    public static Bitmap Warning32x32
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject(nameof (Warning32x32), Resource.resourceCulture);
      }
    }
  }
}
