﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.ModelEntity
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [XmlInclude(typeof (TextPoolEntryType))]
  [XmlType(Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
  [XmlInclude(typeof (NamedModelEntity))]
  [XmlInclude(typeof (PropertyType))]
  [XmlInclude(typeof (PropertyBagType))]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class ModelEntity
  {
    private PropertyBagType propertyBagField;
    private string idField;

    public PropertyBagType PropertyBag
    {
      get
      {
        return this.propertyBagField;
      }
      set
      {
        this.propertyBagField = value;
      }
    }

    [XmlAttribute]
    public string id
    {
      get
      {
        return this.idField;
      }
      set
      {
        this.idField = value;
      }
    }
  }
}
