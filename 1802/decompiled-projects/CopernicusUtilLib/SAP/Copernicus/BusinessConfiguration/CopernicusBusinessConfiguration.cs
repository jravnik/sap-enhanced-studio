﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.CopernicusBusinessConfiguration
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.BusinessConfiguration.Model;
using SAP.Copernicus.BusinessConfiguration.Model.BAC;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration
{
  public class CopernicusBusinessConfiguration
  {
    public static event CopernicusBusinessConfiguration.PostImplProjectTemplateActivateCleanHandler pimpl;

    public static void bccLifeCycleUpdate(string fpath)
    {
      string namespaceForFile = ProjectUtil.GetProjectNamespaceForFile(fpath);
      RepositoryDataCache.GetInstance().RefreshDTFolderNode(namespaceForFile);
      RepositoryDataCache.GetInstance().RefreshBOFolderNode(namespaceForFile);
    }

    public static void postImplProjectTemplateClean(string fpath)
    {
      if (CopernicusBusinessConfiguration.pimpl == null)
        return;
      CopernicusBusinessConfiguration.pimpl(CopernicusBusinessConfiguration.ImplProjStateChange.Clean, fpath);
    }

    public static void postImplProjectTemplateActivate(string fpath)
    {
      if (CopernicusBusinessConfiguration.pimpl == null)
        return;
      CopernicusBusinessConfiguration.pimpl(CopernicusBusinessConfiguration.ImplProjStateChange.Activate, fpath);
    }

    public static void checkAndRemoveObsoleteAssignments(string xrepProjectPath, string xrepProjectBCPath, string xrepBACFilePath, string localBACFilePath, string projectRepNameSpace, string solStatus)
    {
      if (solStatus != "In Development")
        return;
      BusinessConfigurationType serializableBC = CopernicusBusinessConfiguration.InitializeSolutionFromXrep(xrepBACFilePath, false, solStatus);
      if (serializableBC == null)
        return;
      List<ContentType> contentTypeList = new List<ContentType>();
      if (serializableBC.Content != null)
        contentTypeList.AddRange((IEnumerable<ContentType>) serializableBC.Content);
      CopernicusBusinessConfiguration.InitContentOfProject(contentTypeList, false, xrepProjectPath, xrepProjectBCPath, projectRepNameSpace);
      List<ContentType> all = contentTypeList.FindAll((Predicate<ContentType>) (bcs => bcs.ElementID == null));
      if (all != null && all.Count > 0)
      {
        List<BacType> bacTypeList = new List<BacType>();
        foreach (BacType bacType in serializableBC.Bac)
        {
          if (bacType.Type.Equals("Business Option"))
            bacTypeList.Add(bacType);
        }
        if (bacTypeList.Count == 1)
        {
          foreach (ContentType contentType in all)
            contentType.ElementID = bacTypeList[0].ElementID;
        }
      }
      if (serializableBC.Content != null && serializableBC.Content.Length != contentTypeList.Count || all.Count > 0)
      {
        serializableBC.Content = contentTypeList.ToArray<ContentType>();
        CopernicusBusinessConfiguration.saveSolution(serializableBC, localBACFilePath, xrepBACFilePath);
      }
      else
        new XRepHandler().Activate(xrepBACFilePath, false, (string) null);
    }

    public static BCSetType deserializeBCSet(string xmlString)
    {
      XmlSerializer xmlSerializer = new XmlSerializer(typeof (BCSetType));
      MemoryStream memoryStream = new MemoryStream(new UTF8Encoding().GetBytes(xmlString));
      XmlTextWriter xmlTextWriter = new XmlTextWriter((Stream) memoryStream, Encoding.UTF8);
      if (xmlString != null)
      {
        if (xmlString.Length > 0)
        {
          try
          {
            return (BCSetType) xmlSerializer.Deserialize((Stream) memoryStream);
          }
          catch (Exception ex)
          {
            StringReader stringReader = new StringReader(xmlString);
            return (BCSetType) xmlSerializer.Deserialize((TextReader) stringReader);
          }
        }
      }
      return (BCSetType) null;
    }

    public static BusinessConfigurationType DeserializeBAC(string content)
    {
      XmlSerializer xmlSerializer = new XmlSerializer(typeof (BusinessConfigurationType));
      new XmlSerializerNamespaces().Add("bcd", "http://byd.sap.com/PDI/BusinessConfigurationDefinition");
      MemoryStream memoryStream = new MemoryStream(new UTF8Encoding().GetBytes(content));
      XmlTextWriter xmlTextWriter = new XmlTextWriter((Stream) memoryStream, Encoding.UTF8);
      return (BusinessConfigurationType) xmlSerializer.Deserialize((Stream) memoryStream);
    }

    public static BusinessConfigurationType InitializeSolutionFromXrep(string xrepPath, bool shipped, string solutionStatus)
    {
      string content = (string) null;
      IDictionary<string, string> attribs;
      new XRepHandler().Read(xrepPath, shipped, out content, out attribs);
      if (content != null && !content.Equals(string.Empty))
        return CopernicusBusinessConfiguration.DeserializeBAC(content);
      return (BusinessConfigurationType) null;
    }

    public static void InitContentOfProject(List<ContentType> BCSets, bool confirmDialog, string projectXrepPath, string xrepProjectBCPath, string projectRepNameSpace)
    {
      List<ContentType> contentTypeList = new List<ContentType>();
      XRepHandler xrepHandler1 = new XRepHandler();
      IDictionary<string, IDictionary<string, string>> attributesofAllFiles1;
      List<string> pathList1;
      xrepHandler1.ListContentwithAttributes(projectXrepPath, false, out attributesofAllFiles1, out pathList1, (OSLS_XREP_ATTR_FILTER[]) null);
      IDictionary<string, IDictionary<string, string>> attributesofAllFiles2;
      List<string> pathList2;
      xrepHandler1.ListContentwithAttributes(xrepProjectBCPath, false, out attributesofAllFiles2, out pathList2, (OSLS_XREP_ATTR_FILTER[]) null);
      pathList1.AddRange((IEnumerable<string>) pathList2);
      IDictionary<string, IDictionary<string, string>> dictionary = (IDictionary<string, IDictionary<string, string>>) attributesofAllFiles1.Concat<KeyValuePair<string, IDictionary<string, string>>>((IEnumerable<KeyValuePair<string, IDictionary<string, string>>>) attributesofAllFiles2).ToDictionary<KeyValuePair<string, IDictionary<string, string>>, string, IDictionary<string, string>>((Func<KeyValuePair<string, IDictionary<string, string>>, string>) (x => x.Key), (Func<KeyValuePair<string, IDictionary<string, string>>, IDictionary<string, string>>) (x => x.Value));
      if (pathList1.Count <= 0)
        return;
      XmlSerializer xmlSerializer = new XmlSerializer(typeof (BCSetType));
      XRepHandler xrepHandler2 = new XRepHandler();
      ArrayList arrayList = new ArrayList();
      foreach (string key in pathList1)
      {
        IDictionary<string, string> attributes;
        if (dictionary.TryGetValue(key, out attributes))
        {
          string str1 = CopernicusBusinessConfiguration.langIndepGet(attributes, "BCSET_ID");
          if (str1 != null)
          {
            BCSetType bcSetType = new BCSetType();
            bcSetType.Head = new SAP.Copernicus.BusinessConfiguration.Model.HeadType();
            bcSetType.Head.ID = str1;
            arrayList.Add((object) str1);
            bcSetType.Head.Description = CopernicusBusinessConfiguration.langIndepGet(attributes, "BCSET_DESCRIPTION");
            bcSetType.Head.Type = CopernicusBusinessConfiguration.langIndepGet(attributes, "BCSET_TYPE");
            bcSetType.Head.SubType = CopernicusBusinessConfiguration.langIndepGet(attributes, "BCSET_SUBTYPE");
            bool flag = false;
            for (int index = 0; index < BCSets.Count; ++index)
            {
              ContentType bcSet = BCSets[index];
              string str2;
              if (bcSet.Type.Equals(CopernicusBusinessConfigurationConstants.BCContentType.MDRO.ToString()))
              {
                str2 = RepositoryDataCache.GetInstance().GetBONameFromProxyName(bcSet.ID);
                if (str2 == null)
                {
                  RepositoryDataCache.GetInstance().RefreshBOFolderNode(projectRepNameSpace);
                  str2 = RepositoryDataCache.GetInstance().GetBONameFromProxyName(bcSet.ID);
                  if (str2 == null)
                  {
                    string proxyNameFromBo = RepositoryDataCache.GetInstance().GetProxyNameFromBO(projectRepNameSpace, bcSetType.Head.ID);
                    if (proxyNameFromBo != null && proxyNameFromBo.Equals(bcSet.ID))
                    {
                      flag = true;
                      break;
                    }
                  }
                }
              }
              else
                str2 = bcSet.ID;
              if (str2 != null && str2.Equals(bcSetType.Head.ID))
                flag = true;
            }
            if (!flag)
            {
              ContentType contentType = new ContentType();
              contentType.Description = bcSetType.Head.Description;
              contentType.Type = bcSetType.Head.Type;
              contentType.ID = !contentType.Type.Equals(CopernicusBusinessConfigurationConstants.BCContentType.MDRO.ToString()) ? bcSetType.Head.ID : RepositoryDataCache.GetInstance().GetProxyNameFromBO(projectRepNameSpace, bcSetType.Head.ID);
              if (contentType.ID != null)
                BCSets.Add(contentType);
            }
          }
        }
      }
      for (int index = 0; index < BCSets.Count; ++index)
      {
        ContentType bcSet = BCSets[index];
        string str;
        if (bcSet.Type.Equals(CopernicusBusinessConfigurationConstants.BCContentType.MDRO.ToString()))
        {
          str = RepositoryDataCache.GetInstance().GetBONameFromProxyName(bcSet.ID);
          if (str == null)
            contentTypeList.Add(bcSet);
        }
        else
          str = bcSet.ID;
        if (!arrayList.Contains((object) str) && !bcSet.Internal && !contentTypeList.Contains(bcSet))
          contentTypeList.Add(bcSet);
      }
      if (contentTypeList.Count <= 0)
        return;
      string str3 = Resource.MsgInvalidBCSetAssignmentProlog + "\n";
      for (int index = 0; index < contentTypeList.Count; ++index)
        str3 = str3 + contentTypeList[index].ID + "\n";
      string text = str3 + Resource.MsgInvalidBCSetAssignmentEpilog;
      if (confirmDialog && DialogResult.Yes != MessageBox.Show(text, Resource.MsgInvalidBCSetAssignmentTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk))
        return;
      for (int index = 0; index < contentTypeList.Count; ++index)
        BCSets.Remove(contentTypeList[index]);
    }

    public static string langIndepGet(IDictionary<string, string> attributes, string attrib)
    {
      string str = (string) null;
      string[] array = attributes.Keys.ToArray<string>();
      for (int index = 0; index < array.Length; ++index)
      {
        if (array[index].Contains(attrib))
        {
          attributes.TryGetValue(array[index], out str);
          return str;
        }
      }
      return str;
    }

    public static string serializeBAC(BusinessConfigurationType serializableBC)
    {
      XmlSerializer xmlSerializer = new XmlSerializer(serializableBC.GetType());
      XmlTextWriter xmlTextWriter = new XmlTextWriter((Stream) new MemoryStream(), Encoding.UTF8);
      xmlSerializer.Serialize((XmlWriter) xmlTextWriter, (object) serializableBC);
      return new UTF8Encoding().GetString(((MemoryStream) xmlTextWriter.BaseStream).ToArray());
    }

    public static void saveSolution(BusinessConfigurationType serializableBC, string localBACPath, string xrepBacPath)
    {
      string str = CopernicusBusinessConfiguration.serializeBAC(serializableBC);
      LocalFileHandler.CreateFileOnDisk(str, localBACPath, true);
      new XRepHandler().CreateOrUpdate(xrepBacPath, str, true, (IDictionary<string, string>) null, false, (string) null);
    }

    public enum ImplProjStateChange
    {
      Activate,
      Clean,
    }

    public delegate void PostImplProjectTemplateActivateCleanHandler(CopernicusBusinessConfiguration.ImplProjStateChange changeType, string fpath);
  }
}
