﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.BCSetParam
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.BusinessConfiguration.Model;
using System.Collections;
using System.Collections.Generic;

namespace SAP.Copernicus.BusinessConfiguration
{
  public class BCSetParam : ParamType
  {
    public bool usedOnlyOnce;
    public List<string> BCSetID;
    public Hashtable ValueBCsets;

    public BCSetParam(ParamType superParam)
    {
      this.BCSetID = new List<string>();
      this.FineTuningAdd = superParam.FineTuningAdd;
      this.FineTuningDel = superParam.FineTuningDel;
      this.ParameterDescription = superParam.ParameterDescription;
      this.ParameterID = superParam.ParameterID;
      this.ParameterType = superParam.ParameterType;
      this.ParameterValue = superParam.ParameterValue;
    }

    public BCSetParam()
    {
      this.BCSetID = new List<string>();
    }

    public override string ToString()
    {
      return this.ParameterID;
    }
  }
}
