﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BCO.BCONodeElementType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BCO
{
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCODefinition")]
  [DesignerCategory("code")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [Serializable]
  public class BCONodeElementType
  {
    private string fieldNameField;
    private string fieldDescriptionField;
    private FieldTypeType fieldTypeField;
    private DataTypeType dataTypeField;
    private string dataSubTypeField;
    private string proxyNameField;
    private string namespaceField;
    private int lengthField;
    private int decimalsField;
    private bool isMandatoryField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string FieldName
    {
      get
      {
        return this.fieldNameField;
      }
      set
      {
        this.fieldNameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string FieldDescription
    {
      get
      {
        return this.fieldDescriptionField;
      }
      set
      {
        this.fieldDescriptionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public FieldTypeType FieldType
    {
      get
      {
        return this.fieldTypeField;
      }
      set
      {
        this.fieldTypeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public DataTypeType DataType
    {
      get
      {
        return this.dataTypeField;
      }
      set
      {
        this.dataTypeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string DataSubType
    {
      get
      {
        return this.dataSubTypeField;
      }
      set
      {
        this.dataSubTypeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ProxyName
    {
      get
      {
        return this.proxyNameField;
      }
      set
      {
        this.proxyNameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Namespace
    {
      get
      {
        return this.namespaceField;
      }
      set
      {
        this.namespaceField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public int Length
    {
      get
      {
        return this.lengthField;
      }
      set
      {
        this.lengthField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public int Decimals
    {
      get
      {
        return this.decimalsField;
      }
      set
      {
        this.decimalsField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public bool IsMandatory
    {
      get
      {
        return this.isMandatoryField;
      }
      set
      {
        this.isMandatoryField = value;
      }
    }
  }
}
