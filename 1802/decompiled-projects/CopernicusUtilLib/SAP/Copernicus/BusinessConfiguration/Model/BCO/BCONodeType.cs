﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BCO.BCONodeType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BCO
{
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCODefinition")]
  [DesignerCategory("code")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [Serializable]
  public class BCONodeType
  {
    private BCONodeHeadType headField;
    private BCONodeElementType[] elementsField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public BCONodeHeadType Head
    {
      get
      {
        return this.headField;
      }
      set
      {
        this.headField = value;
      }
    }

    [XmlElement("Elements", Form = XmlSchemaForm.Unqualified)]
    public BCONodeElementType[] Elements
    {
      get
      {
        return this.elementsField;
      }
      set
      {
        this.elementsField = value;
      }
    }
  }
}
