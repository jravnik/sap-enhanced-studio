﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BCO.DataTypeType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BCO
{
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCODefinition")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [Serializable]
  public enum DataTypeType
  {
    [XmlEnum("1")] Code,
    [XmlEnum("2")] Indicator,
    [XmlEnum("5")] Integer,
    [XmlEnum("6")] Date,
    [XmlEnum("7")] Time,
    [XmlEnum("8")] Percentage,
    [XmlEnum("9")] Text,
  }
}
