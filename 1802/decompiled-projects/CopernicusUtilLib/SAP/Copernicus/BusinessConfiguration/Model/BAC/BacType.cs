﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BAC.BacType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BAC
{
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BusinessConfigurationDefinition")]
  [Serializable]
  public class BacType
  {
    private string elementIDField;
    private string parentIDField;
    private string visibleScopingField;
    private string visibleFineTuningField;
    private string descriptionField;
    private string documentationField;
    private string scopingQuestionField;
    private string typeField;
    private string subTypeField;
    private string goLiveCheckField;
    private string goLivePhaseField;
    private string goLiveActivityGroupField;
    private string goLiveActivityTypeField;
    private string goLiveActivityDescriptionField;
    private string countryField;
    private string industryField;
    private string implFocusField;
    private string constraintField;
    private string optionGroupField;
    private string optionExclusiveField;
    private string optionMandatoryField;
    private string kTOverviewField;
    private string kTRelevanceField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ElementID
    {
      get
      {
        return this.elementIDField;
      }
      set
      {
        this.elementIDField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ParentID
    {
      get
      {
        return this.parentIDField;
      }
      set
      {
        this.parentIDField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string VisibleScoping
    {
      get
      {
        return this.visibleScopingField;
      }
      set
      {
        this.visibleScopingField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string VisibleFineTuning
    {
      get
      {
        return this.visibleFineTuningField;
      }
      set
      {
        this.visibleFineTuningField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Description
    {
      get
      {
        return this.descriptionField;
      }
      set
      {
        this.descriptionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Documentation
    {
      get
      {
        return this.documentationField;
      }
      set
      {
        this.documentationField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ScopingQuestion
    {
      get
      {
        return this.scopingQuestionField;
      }
      set
      {
        this.scopingQuestionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Type
    {
      get
      {
        return this.typeField;
      }
      set
      {
        this.typeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string SubType
    {
      get
      {
        return this.subTypeField;
      }
      set
      {
        this.subTypeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string GoLiveCheck
    {
      get
      {
        return this.goLiveCheckField;
      }
      set
      {
        this.goLiveCheckField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string GoLivePhase
    {
      get
      {
        return this.goLivePhaseField;
      }
      set
      {
        this.goLivePhaseField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string GoLiveActivityGroup
    {
      get
      {
        return this.goLiveActivityGroupField;
      }
      set
      {
        this.goLiveActivityGroupField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string GoLiveActivityType
    {
      get
      {
        return this.goLiveActivityTypeField;
      }
      set
      {
        this.goLiveActivityTypeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string GoLiveActivityDescription
    {
      get
      {
        return this.goLiveActivityDescriptionField;
      }
      set
      {
        this.goLiveActivityDescriptionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Country
    {
      get
      {
        return this.countryField;
      }
      set
      {
        this.countryField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Industry
    {
      get
      {
        return this.industryField;
      }
      set
      {
        this.industryField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ImplFocus
    {
      get
      {
        return this.implFocusField;
      }
      set
      {
        this.implFocusField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Constraint
    {
      get
      {
        return this.constraintField;
      }
      set
      {
        this.constraintField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string OptionGroup
    {
      get
      {
        return this.optionGroupField;
      }
      set
      {
        this.optionGroupField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string OptionExclusive
    {
      get
      {
        return this.optionExclusiveField;
      }
      set
      {
        this.optionExclusiveField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string OptionMandatory
    {
      get
      {
        return this.optionMandatoryField;
      }
      set
      {
        this.optionMandatoryField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string KTOverview
    {
      get
      {
        return this.kTOverviewField;
      }
      set
      {
        this.kTOverviewField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string KTRelevance
    {
      get
      {
        return this.kTRelevanceField;
      }
      set
      {
        this.kTRelevanceField = value;
      }
    }
  }
}
