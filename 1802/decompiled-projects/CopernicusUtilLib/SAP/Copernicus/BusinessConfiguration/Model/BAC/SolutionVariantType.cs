﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BAC.SolutionVariantType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BAC
{
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BusinessConfigurationDefinition")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class SolutionVariantType
  {
    private string solutionVariantIDField;
    private string descriptionField;
    private string[] elementIDField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string SolutionVariantID
    {
      get
      {
        return this.solutionVariantIDField;
      }
      set
      {
        this.solutionVariantIDField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Description
    {
      get
      {
        return this.descriptionField;
      }
      set
      {
        this.descriptionField = value;
      }
    }

    [XmlElement("ElementID", Form = XmlSchemaForm.Unqualified)]
    public string[] ElementID
    {
      get
      {
        return this.elementIDField;
      }
      set
      {
        this.elementIDField = value;
      }
    }
  }
}
