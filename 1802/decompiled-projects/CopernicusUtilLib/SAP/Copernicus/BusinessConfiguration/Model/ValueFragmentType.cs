﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.ValueFragmentType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCSetDefinition")]
  [Serializable]
  public class ValueFragmentType
  {
    private string headerField;
    private string valueField;
    private string descriptionField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Header
    {
      get
      {
        return this.headerField;
      }
      set
      {
        this.headerField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Value
    {
      get
      {
        return this.valueField;
      }
      set
      {
        this.valueField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Description
    {
      get
      {
        return this.descriptionField;
      }
      set
      {
        this.descriptionField = value;
      }
    }
  }
}
