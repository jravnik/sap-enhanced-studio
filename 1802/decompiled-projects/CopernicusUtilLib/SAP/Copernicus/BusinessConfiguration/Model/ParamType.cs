﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.ParamType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model
{
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCSetDefinition")]
  [DebuggerStepThrough]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [Serializable]
  public class ParamType
  {
    private string parameterIDField;
    private string parameterDescriptionField;
    private string parameterTypeField;
    private ValueStructureType[] parameterValueField;
    private bool fineTuningAddField;
    private bool fineTuningDelField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ParameterID
    {
      get
      {
        return this.parameterIDField;
      }
      set
      {
        this.parameterIDField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ParameterDescription
    {
      get
      {
        return this.parameterDescriptionField;
      }
      set
      {
        this.parameterDescriptionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ParameterType
    {
      get
      {
        return this.parameterTypeField;
      }
      set
      {
        this.parameterTypeField = value;
      }
    }

    [XmlElement("ParameterValue", Form = XmlSchemaForm.Unqualified)]
    public ValueStructureType[] ParameterValue
    {
      get
      {
        return this.parameterValueField;
      }
      set
      {
        this.parameterValueField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public bool FineTuningAdd
    {
      get
      {
        return this.fineTuningAddField;
      }
      set
      {
        this.fineTuningAddField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public bool FineTuningDel
    {
      get
      {
        return this.fineTuningDelField;
      }
      set
      {
        this.fineTuningDelField = value;
      }
    }
  }
}
