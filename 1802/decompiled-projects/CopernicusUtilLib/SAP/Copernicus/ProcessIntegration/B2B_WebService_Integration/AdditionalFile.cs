﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.AdditionalFile
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration
{
  public class AdditionalFile
  {
    public string FileName { get; set; }

    public string FilePath { get; set; }

    public AdditionalFile()
    {
    }

    public AdditionalFile(string pstrFileName, string pstrFilePath)
    {
      this.FileName = pstrFileName;
      this.FilePath = pstrFilePath;
    }
  }
}
