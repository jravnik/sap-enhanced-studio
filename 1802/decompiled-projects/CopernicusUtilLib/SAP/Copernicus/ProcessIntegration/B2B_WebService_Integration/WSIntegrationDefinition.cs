﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.WSIntegrationDefinition
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;
using System.Xml.Serialization;

namespace SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration
{
  public class WSIntegrationDefinition
  {
    [XmlElement(typeof (SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.SoapOperationBinding))]
    public List<SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.SoapOperationBinding> SoapOperationBinding { get; set; }

    public string WsdlLocation { get; set; }

    [XmlElement(typeof (SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.AdditionalFile))]
    public List<SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.AdditionalFile> AdditionalFile { get; set; }

    public string SelectedPort { get; set; }

    [XmlElement(typeof (SelectedPortType))]
    public SelectedPortType SelectedPortType { get; set; }

    public string IsActivated { get; set; }

    public bool IsStateful { get; set; }

    [XmlElement(typeof (WebServiceTypeCode))]
    public WebServiceTypeCode WebServiceType { get; set; }

    public string Url { get; set; }
  }
}
