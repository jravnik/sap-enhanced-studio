﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.WebReferenceVersion.ZgetVersion
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.WebReferenceVersion
{
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [GeneratedCode("System.Xml", "4.0.30319.17929")]
  [XmlType(AnonymousType = true, Namespace = "urn:sap-com:document:sap:soap:functions:mc-style")]
  [Serializable]
  public class ZgetVersion
  {
    private string ivPpmsTechnicalNameField;
    private string ivPpmsTechnicalReleaseField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string IvPpmsTechnicalName
    {
      get
      {
        return this.ivPpmsTechnicalNameField;
      }
      set
      {
        this.ivPpmsTechnicalNameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string IvPpmsTechnicalRelease
    {
      get
      {
        return this.ivPpmsTechnicalReleaseField;
      }
      set
      {
        this.ivPpmsTechnicalReleaseField = value;
      }
    }
  }
}
