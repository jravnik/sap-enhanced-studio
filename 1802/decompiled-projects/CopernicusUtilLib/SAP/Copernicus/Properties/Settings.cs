﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Properties.Settings
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }

    [ApplicationScopedSetting]
    [DebuggerNonUserCode]
    [SpecialSetting(SpecialSetting.WebServiceUrl)]
    [DefaultSettingValue("https://atf-cust810.dev.sapbydesign.com/sap/bc/srt/rfc/sap/zget_version_service/zsn_version/zbn_version?sap-vhost=atf-cust810.dev.sapbydesign.com")]
    public string CopernicusUtilLib_WebReferenceVersion_ZSN_VERSION
    {
      get
      {
        return (string) this[nameof (CopernicusUtilLib_WebReferenceVersion_ZSN_VERSION)];
      }
    }
  }
}
