﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Automation.HelpUtil
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.VSHelp80;

namespace SAP.Copernicus.Core.Automation
{
  public class HelpUtil
  {
    public static void DisplayF1Help(string keyword)
    {
      // ISSUE: variable of a compiler-generated type
      Help2 help2 = (Help2) DTEUtil.GetDTE().GetObject("Help2");
      // ISSUE: reference to a compiler-generated method
      help2.DisplayTopicFromF1Keyword(keyword);
    }
  }
}
