﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Automation.DTEUtil
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using EnvDTE80;
using System;

namespace SAP.Copernicus.Core.Automation
{
  public class DTEUtil
  {
    private static DTEUtil.GetDteDelegate theDelegate;

    public static void Initialize(DTEUtil.GetDteDelegate theDelegate)
    {
      DTEUtil.theDelegate = theDelegate;
    }

    public static DTE2 GetDTE()
    {
      if (DTEUtil.theDelegate == null)
        throw new NotSupportedException("DTEUtil not yet initialized");
      return DTEUtil.theDelegate();
    }

    public static bool IsInitialized
    {
      get
      {
        return DTEUtil.theDelegate != null;
      }
    }

    public delegate DTE2 GetDteDelegate();
  }
}
