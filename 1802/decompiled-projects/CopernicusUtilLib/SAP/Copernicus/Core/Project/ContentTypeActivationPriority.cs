﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Project.ContentTypeActivationPriority
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Project
{
  public enum ContentTypeActivationPriority
  {
    BO_THING_TYPE = 0,
    MDRS_GENERIC_CONTENT = 0,
    MDRS_NAMESPACE = 0,
    XREP_CONTENT = 0,
    XREP_ENTITY = 0,
    BCO = 1,
    CUSTOMER_OBJECT_REFERENCES = 1,
    BCSET = 2,
    BUSINESS_OBJECT = 3,
    EXTENSION_ENTITY = 4,
    FORM_EXTENSION_ENTITY = 5,
    PROCESS_COMPONENT = 7,
    BUSINESSCONFIGURATION = 8,
    BCSET_CODELIST = 10,
    CODE_DATATYPE = 10,
    FORMS = 10,
    SOLUTION = 12,
    MDRO = 14,
    PROCESS_INTEGRATION = 20,
    SERVICE_ADAPT_DEF_LANG = 21,
    BC_WS_TEMPLATE = 30,
    APPLICATION_EXIT = 40,
    APPLICATION_EXIT_FILTER = 41,
    QUERYDEF = 50,
    DATASOURCE = 51,
    A2X = 55,
    UICOMPONENT = 55,
    XSODATA = 55,
    WSAUTH = 56,
    WSID = 57,
    CSD = 58,
    APPROVALSTEP = 60,
    ABSL = 65,
    USG = 66,
    WSDL = 67,
    NOTIFICATION_RULE = 80,
    XWEBSERVICE = 91,
    PDD = 101,
    PERSONALDATASETTING = 102,
    MSAPPROVALSTEP = 110,
    CUSTOM_REUSE_LIBRARY = 120,
  }
}
