﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Settings.SettingsAccess
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.Settings;
using Microsoft.VisualStudio.Shell.Settings;
using SAP.Copernicus.Core.Util;

namespace SAP.Copernicus.Core.Settings
{
  public class SettingsAccess
  {
    private const string collectionPath = "SAP.Copernicus.CopernicusSettings";
    private static SettingsAccess instance;
    private WritableSettingsStore settingsStore;
    private bool? showABSLSignatureAdornment;

    public static SettingsAccess Instance
    {
      get
      {
        if (SettingsAccess.instance == null)
          SettingsAccess.instance = new SettingsAccess();
        return SettingsAccess.instance;
      }
    }

    private SettingsAccess()
    {
      this.settingsStore = new ShellSettingsManager(VSPackageUtil.ServiceProvider).GetWritableSettingsStore(SettingsScope.UserSettings);
      if (this.settingsStore.CollectionExists("SAP.Copernicus.CopernicusSettings"))
        return;
      this.settingsStore.CreateCollection("SAP.Copernicus.CopernicusSettings");
    }

    public bool ShowABSLSignatureAdornment
    {
      set
      {
        try
        {
          this.settingsStore.SetBoolean("SAP.Copernicus.CopernicusSettings", nameof (ShowABSLSignatureAdornment), value);
          this.showABSLSignatureAdornment = new bool?(value);
        }
        catch
        {
        }
      }
      get
      {
        if (!this.showABSLSignatureAdornment.HasValue)
          this.showABSLSignatureAdornment = new bool?(this.settingsStore.GetBoolean("SAP.Copernicus.CopernicusSettings", nameof (ShowABSLSignatureAdornment), true));
        return this.showABSLSignatureAdornment.Value;
      }
    }

    public string ContactPerson
    {
      set
      {
        try
        {
          this.settingsStore.SetString("SAP.Copernicus.CopernicusSettings", nameof (ContactPerson), value);
        }
        catch
        {
        }
      }
      get
      {
        try
        {
          return this.settingsStore.GetString("SAP.Copernicus.CopernicusSettings", nameof (ContactPerson));
        }
        catch
        {
          return "";
        }
      }
    }

    public string SAPDevelopmentPartner
    {
      set
      {
        try
        {
          this.settingsStore.SetString("SAP.Copernicus.CopernicusSettings", nameof (SAPDevelopmentPartner), value);
        }
        catch
        {
        }
      }
      get
      {
        try
        {
          return this.settingsStore.GetString("SAP.Copernicus.CopernicusSettings", nameof (SAPDevelopmentPartner));
        }
        catch
        {
          return "";
        }
      }
    }

    public string EMail
    {
      set
      {
        try
        {
          this.settingsStore.SetString("SAP.Copernicus.CopernicusSettings", nameof (EMail), value);
        }
        catch
        {
        }
      }
      get
      {
        try
        {
          return this.settingsStore.GetString("SAP.Copernicus.CopernicusSettings", nameof (EMail));
        }
        catch
        {
          return "";
        }
      }
    }
  }
}
