﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.BO.BOHelper
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Repository;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.BO
{
  public class BOHelper
  {
    private BOHelper()
    {
    }

    public static bool BOWasSaved(string boNS, string boName)
    {
      if (!string.IsNullOrEmpty(RepositoryDataCache.GetInstance().GetProxyNameFromBO(boNS, boName)))
        return true;
      int num = (int) CopernicusMessageBox.Show(string.Format(Resource.BOSaveMissing, (object) boName), Resource.BOCheckTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      return false;
    }
  }
}
