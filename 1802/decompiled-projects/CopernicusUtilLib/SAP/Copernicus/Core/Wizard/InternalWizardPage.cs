﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Wizard.InternalWizardPage
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Automation;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Wizard
{
  public class InternalWizardPage : CopernicusWizardPage
  {
    public WizardBanner Banner;
    private IContainer components;
    private string HelpID;

    public InternalWizardPage()
    {
      this.InitializeComponent();
      this.HelpID = HELP_IDS.MSDNSTART;
      this.Banner.HelpRequested += new WizardBanner.HelpRequestedEventHandler(this.HelpRequested);
    }

    public InternalWizardPage(string helpID)
    {
      this.InitializeComponent();
      this.HelpID = helpID;
      this.Banner.HelpRequested += new WizardBanner.HelpRequestedEventHandler(this.HelpRequested);
    }

    private void HelpRequested(object sender)
    {
      HelpUtil.DisplayF1Help(this.HelpID);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.Banner = new WizardBanner();
      this.SuspendLayout();
      this.Banner.BackColor = SystemColors.Window;
      this.Banner.Dock = DockStyle.Top;
      this.Banner.Location = new Point(0, 0);
      this.Banner.Name = "Banner";
      this.Banner.Size = new Size(853, 64);
      this.Banner.Subtitle = "Subtitle";
      this.Banner.TabIndex = 0;
      this.Banner.Title = "Title";
      this.AutoSize = true;
      this.Controls.Add((Control) this.Banner);
      this.Name = nameof (InternalWizardPage);
      this.Size = new Size(853, 845);
      this.ResumeLayout(false);
    }
  }
}
