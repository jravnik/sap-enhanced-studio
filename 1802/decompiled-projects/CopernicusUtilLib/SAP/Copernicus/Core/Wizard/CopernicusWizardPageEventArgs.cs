﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Wizard.CopernicusWizardPageEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;

namespace SAP.Copernicus.Core.Wizard
{
  public class CopernicusWizardPageEventArgs : CancelEventArgs
  {
    private string _newPage;

    public string NewPage
    {
      get
      {
        return this._newPage;
      }
      set
      {
        this._newPage = value;
      }
    }
  }
}
