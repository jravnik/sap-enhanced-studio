﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.HierarchicalTreeView.DataGridViewDisengageableButtonCell
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.HierarchicalTreeView
{
  public class DataGridViewDisengageableButtonCell : DataGridViewButtonCell, DataGridViewDisengageableCell
  {
    private bool enabled;

    public bool Enabled
    {
      set
      {
        this.enabled = value;
      }
      get
      {
        return this.enabled;
      }
    }

    public DataGridViewDisengageableButtonCell()
    {
      this.enabled = true;
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
      if (!this.enabled)
      {
        if ((cellState & DataGridViewElementStates.Selected) != DataGridViewElementStates.None)
        {
          graphics.FillRectangle((Brush) new SolidBrush(cellStyle.SelectionBackColor), cellBounds);
          graphics.DrawString((string) formattedValue, cellStyle.Font, (Brush) new SolidBrush(cellStyle.SelectionForeColor), (RectangleF) cellBounds, StringFormat.GenericDefault);
        }
        else
        {
          graphics.FillRectangle((Brush) new SolidBrush(cellStyle.BackColor), cellBounds);
          graphics.DrawString((string) formattedValue, cellStyle.Font, (Brush) new SolidBrush(cellStyle.ForeColor), (RectangleF) cellBounds, StringFormat.GenericDefault);
        }
        this.PaintBorder(graphics, clipBounds, cellBounds, cellStyle, advancedBorderStyle);
      }
      else
        base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
    }
  }
}
