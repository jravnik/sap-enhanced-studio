﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CopernicusStatusBar
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using EnvDTE;
using EnvDTE80;

namespace SAP.Copernicus.Core.GUI
{
  public class CopernicusStatusBar : IStatus
  {
    private DTE2 dte;

    public static IStatus Instance { get; set; }

    public CopernicusStatusBar(DTE2 dte)
    {
      this.dte = dte;
    }

    public void ShowMessage(string message)
    {
      this.dte.StatusBar.Progress(true, message, 0, 100);
    }

    public void StartProcess(string message)
    {
      this.dte.StatusBar.Animate(true, (object) vsStatusAnimation.vsStatusAnimationGeneral);
      this.dte.StatusBar.Progress(true, message, 0, 100);
    }

    public void EndProcess(string message)
    {
      this.dte.StatusBar.Progress(true, message, 0, 100);
      this.dte.StatusBar.Animate(false, (object) vsStatusAnimation.vsStatusAnimationGeneral);
    }
  }
}
