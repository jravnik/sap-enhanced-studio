﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckVariableDataGridCell.CopernicusDataGridViewCheckVariableEditingControl
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.CheckVariableDataGridCell
{
  public class CopernicusDataGridViewCheckVariableEditingControl : UserControlCheckVariable, IDataGridViewEditingControl
  {
    private DataGridView dataGridView;
    private bool valueChanged;
    private int rowIndex;

    [DllImport("USER32.DLL", CharSet = CharSet.Auto)]
    private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

    public CopernicusDataGridViewCheckVariableEditingControl()
    {
      this.TabStop = false;
    }

    public virtual DataGridView EditingControlDataGridView
    {
      get
      {
        return this.dataGridView;
      }
      set
      {
        this.dataGridView = value;
      }
    }

    public virtual object EditingControlFormattedValue
    {
      get
      {
        return this.GetEditingControlFormattedValue(DataGridViewDataErrorContexts.Formatting);
      }
      set
      {
        this.Value = (string) value;
      }
    }

    public virtual int EditingControlRowIndex
    {
      get
      {
        return this.rowIndex;
      }
      set
      {
        this.rowIndex = value;
      }
    }

    public virtual bool EditingControlValueChanged
    {
      get
      {
        return this.valueChanged;
      }
      set
      {
        this.valueChanged = value;
      }
    }

    public virtual Cursor EditingPanelCursor
    {
      get
      {
        return Cursors.Default;
      }
    }

    public virtual bool RepositionEditingControlOnValueChange
    {
      get
      {
        return false;
      }
    }

    public virtual void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
    {
      this.Font = dataGridViewCellStyle.Font;
      if ((int) dataGridViewCellStyle.BackColor.A < (int) byte.MaxValue)
      {
        Color color = Color.FromArgb((int) byte.MaxValue, dataGridViewCellStyle.BackColor);
        this.BackColor = color;
        this.dataGridView.EditingPanel.BackColor = color;
      }
      else
        this.BackColor = dataGridViewCellStyle.BackColor;
      this.ForeColor = dataGridViewCellStyle.ForeColor;
      this.TextAlign = CopernicusDataGridViewCheckVariableCell.TranslateAlignment(dataGridViewCellStyle.Alignment);
    }

    public virtual bool EditingControlWantsInputKey(Keys keyData, bool dataGridViewWantsInputKey)
    {
      bool isProcessedInside = false;
      switch (keyData & Keys.KeyCode)
      {
        case Keys.Return:
          TextBox control = this.Controls[0] as TextBox;
          if (control != null && control.Text.Length > 0)
          {
            this.NotifyDataGridViewOfValueChange();
            return true;
          }
          break;
        case Keys.End:
        case Keys.Home:
          this.ProcessKey(Keys.End, out isProcessedInside);
          if (isProcessedInside)
            return true;
          break;
        case Keys.Left:
          this.ProcessKey(Keys.Left, out isProcessedInside);
          if (isProcessedInside)
            return true;
          break;
        case Keys.Up:
          if (this.Value.Length > 0)
            return true;
          break;
        case Keys.Right:
          this.ProcessKey(Keys.Right, out isProcessedInside);
          if (isProcessedInside)
            return true;
          break;
        case Keys.Down:
          if (this.Value.Length > 0)
            return true;
          break;
        case Keys.Delete:
          this.ProcessKey(Keys.Delete, out isProcessedInside);
          if (isProcessedInside)
            return true;
          break;
        case Keys.F4:
          if (!this.IsFilterValueSelectable)
            return false;
          DialogResult result;
          string str = this.PerformPopupPlugin(out result);
          if (result == DialogResult.OK && !this.Value.Equals(str))
          {
            this.Value = str;
            this.NotifyDataGridViewOfValueChange();
            return true;
          }
          break;
      }
      return !dataGridViewWantsInputKey;
    }

    public virtual object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
    {
      bool userEdit = this.UserEdit;
      try
      {
        this.UserEdit = (context & DataGridViewDataErrorContexts.Display) == (DataGridViewDataErrorContexts) 0;
        return this.Value == null ? (object) string.Empty : (object) this.Value.ToString();
      }
      finally
      {
        this.UserEdit = userEdit;
      }
    }

    public virtual void PrepareEditingControlForEdit(bool selectAll)
    {
    }

    private void NotifyDataGridViewOfValueChange()
    {
      if (this.valueChanged)
        return;
      this.valueChanged = true;
      this.dataGridView.NotifyCurrentCellDirty(true);
    }

    protected override void OnKeyPress(KeyPressEventArgs e)
    {
      base.OnKeyPress(e);
      bool flag = false;
      if (char.IsDigit(e.KeyChar))
      {
        flag = true;
      }
      else
      {
        NumberFormatInfo numberFormat = CultureInfo.CurrentCulture.NumberFormat;
        string decimalSeparator = numberFormat.NumberDecimalSeparator;
        string numberGroupSeparator = numberFormat.NumberGroupSeparator;
        string negativeSign = numberFormat.NegativeSign;
        if (!string.IsNullOrEmpty(decimalSeparator) && decimalSeparator.Length == 1)
          flag = (int) decimalSeparator[0] == (int) e.KeyChar;
        if (!flag && !string.IsNullOrEmpty(numberGroupSeparator) && numberGroupSeparator.Length == 1)
          flag = (int) numberGroupSeparator[0] == (int) e.KeyChar;
        if (!flag && !string.IsNullOrEmpty(negativeSign) && negativeSign.Length == 1)
          flag = (int) negativeSign[0] == (int) e.KeyChar;
      }
      if (!flag)
        return;
      this.NotifyDataGridViewOfValueChange();
    }

    protected override void OnValueChanged(EventArgs e)
    {
      base.OnValueChanged(e);
      this.valueChanged = true;
      this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
      this.NotifyDataGridViewOfValueChange();
    }

    protected override bool ProcessKeyEventArgs(ref Message m)
    {
      TextBox control = this.Controls[0] as TextBox;
      if (control == null)
        return base.ProcessKeyEventArgs(ref m);
      CopernicusDataGridViewCheckVariableEditingControl.SendMessage(control.Handle, m.Msg, m.WParam, m.LParam);
      return true;
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);
    }

    private void InitializeComponent()
    {
      this.SuspendLayout();
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.Name = nameof (CopernicusDataGridViewCheckVariableEditingControl);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
