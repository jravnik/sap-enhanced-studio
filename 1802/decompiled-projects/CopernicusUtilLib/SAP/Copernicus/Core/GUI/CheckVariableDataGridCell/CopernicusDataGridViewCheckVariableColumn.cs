﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckVariableDataGridCell.CopernicusDataGridViewCheckVariableColumn
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.CheckVariableDataGridCell
{
  public class CopernicusDataGridViewCheckVariableColumn : DataGridViewColumn
  {
    private static string strCellTemplateError = "Value provided for CellTemplate must be of type SAP.Copernicus.Core.GUI.CheckVariableDataGridCell.CopernicusDataGridViewCheckVariableCell or derive from it.";

    public CopernicusDataGridViewCheckVariableColumn()
      : base((DataGridViewCell) new CopernicusDataGridViewCheckVariableCell())
    {
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    public override DataGridViewCell CellTemplate
    {
      get
      {
        return base.CellTemplate;
      }
      set
      {
        CopernicusDataGridViewCheckVariableCell checkVariableCell = value as CopernicusDataGridViewCheckVariableCell;
        if (value != null && checkVariableCell == null)
          throw new InvalidCastException(CopernicusDataGridViewCheckVariableColumn.strCellTemplateError);
        base.CellTemplate = value;
      }
    }

    [Category("Behavior")]
    [Description("Callback Form With SAP.Copernicus.Core.GUI.CustomizedDataGridCell.CallbackFormInterface.")]
    [DefaultValue(false)]
    public bool IsRequiredCellEnable
    {
      get
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusCheckVariableCellTemplate.IsEnableRequiredFilterValue;
      }
      set
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusCheckVariableCellTemplate.IsEnableRequiredFilterValue = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewCheckVariableCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewCheckVariableCell;
          if (cell != null)
            cell.SetIsEnableRequiredValue(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    [DefaultValue(false)]
    [Description("Enable Filter Value Selection by Default Value for Column.")]
    [Category("Behavior")]
    public bool IsFilterValueSelectableForCell
    {
      get
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusCheckVariableCellTemplate.IsEnableRequiredFilterValue;
      }
      set
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusCheckVariableCellTemplate.IsFilterValueSelectableForCell = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewCheckVariableCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewCheckVariableCell;
          if (cell != null)
            cell.SetCallbackFormIsFilterValueSelectableForCell(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    [DefaultValue("")]
    [Description("Callback Form With SAP.Copernicus.Core.GUI.CheckVariableDataGridCell.CopernicusFilterPopupForm.")]
    [Category("Behavior")]
    public string CallbackFormHandler
    {
      get
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusCheckVariableCellTemplate.strCallbackFormHandler;
      }
      set
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusCheckVariableCellTemplate.strCallbackFormHandler = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewCheckVariableCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewCheckVariableCell;
          if (cell != null)
            cell.SetCallbackFormHandler(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    [Category("Behavior")]
    [Description("Callback Form Assembly Name.")]
    [DefaultValue("")]
    public string CallbackFormPackageName
    {
      get
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusCheckVariableCellTemplate.CallbackFormPackageName;
      }
      set
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusCheckVariableCellTemplate.CallbackFormPackageName = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewCheckVariableCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewCheckVariableCell;
          if (cell != null)
            cell.SetCallbackFormPackageName(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    public CopernicusPopupClassMetaData FilterClassMetaData
    {
      get
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusCheckVariableCellTemplate.FilterClassMetaDataForCell;
      }
      set
      {
        if (this.CopernicusCheckVariableCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusCheckVariableCellTemplate.FilterClassMetaDataForCell = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewCheckVariableCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewCheckVariableCell;
          if (cell != null)
            cell.SetFilterClassMetaDataForCell(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    private CopernicusDataGridViewCheckVariableCell CopernicusCheckVariableCellTemplate
    {
      get
      {
        return (CopernicusDataGridViewCheckVariableCell) this.CellTemplate;
      }
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder(100);
      stringBuilder.Append("CopernicusDataGridViewCheckVariableColumn { Name=");
      stringBuilder.Append(this.Name);
      stringBuilder.Append(", Index=");
      stringBuilder.Append(this.Index.ToString((IFormatProvider) CultureInfo.CurrentCulture));
      stringBuilder.Append(" }");
      return stringBuilder.ToString();
    }
  }
}
