﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckVariableDataGridCell.UserControlCheckVariable
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.CheckVariableDataGridCell
{
  public class UserControlCheckVariable : UserControl
  {
    private static string strTooltipTempateRequired = "Required Filter Value from Backend";
    private static string strTooltipTempateOptional = "Optional Filter Value for Backend";
    private ToolTip filterToolTip = new ToolTip();
    private string strCallbackFormClass = string.Empty;
    private string strCallbackFormPackageName = string.Empty;
    public List<CopernicusDataGridViewCheckVariableCell> listMonitorInstances = new List<CopernicusDataGridViewCheckVariableCell>();
    private const string strToolTipForF4 = "Press F4 Key to retrieve Value list from backend";
    public Action<EventArgs> OnFilterValueCallback;
    private bool isUserEdit;
    private bool isFilterBackendMandatory;
    private Action<bool> invokeAction;
    private bool isDisposedInstance;
    private bool isFilterValueSelectable;
    private int irowIndex;
    private int icolumnIndex;
    private bool isCellChecked;
    private bool isEnterForCurrentControl;
    private bool isF4buttonProcessed;
    private CopernicusPopupClassMetaData filterClassMetaData;
    private IContainer components;
    private SplitContainer splitContainer;
    private CheckBox checkBoxRequired;
    private TextBox textBoxFilterValue;

    public UserControlCheckVariable()
    {
      this.InitializeComponent();
      this.filterToolTip.AutoPopDelay = 6000;
      this.filterToolTip.InitialDelay = 500;
      this.filterToolTip.ReshowDelay = 500;
      this.filterToolTip.ShowAlways = true;
      this.filterToolTip.UseAnimation = true;
      this.filterToolTip.IsBalloon = true;
      this.filterToolTip.UseFading = true;
      this.OnFilterValueCallback += new Action<EventArgs>(this.OnValueChanged);
    }

    public string Value
    {
      get
      {
        return this.textBoxFilterValue.Text;
      }
      set
      {
        if (value == null || value.Equals(this.textBoxFilterValue.Text))
          return;
        this.textBoxFilterValue.Text = value;
      }
    }

    public bool UserEdit
    {
      get
      {
        return this.isUserEdit;
      }
      set
      {
        if (value == this.isUserEdit)
          return;
        this.isUserEdit = value;
      }
    }

    public bool IsFilterBackendMandatory
    {
      get
      {
        return this.isFilterBackendMandatory;
      }
      set
      {
        if (value == this.isFilterBackendMandatory)
          return;
        this.isFilterBackendMandatory = value;
      }
    }

    public bool IsUserChecked
    {
      get
      {
        return this.checkBoxRequired.Checked;
      }
      set
      {
        this.UpdateUIStatus(value);
      }
    }

    private void checkBoxRequired_CheckedChanged(object sender, EventArgs e)
    {
      this.FireTagChangeEvent(this.checkBoxRequired.Checked, this.irowIndex, this.icolumnIndex);
      if (this.isFilterBackendMandatory || !this.checkBoxRequired.Enabled)
        return;
      if (!this.checkBoxRequired.Checked)
      {
        this.textBoxFilterValue.Clear();
        this.textBoxFilterValue.Enabled = false;
      }
      else
        this.textBoxFilterValue.Enabled = true;
    }

    public HorizontalAlignment TextAlign
    {
      get
      {
        return this.textBoxFilterValue.TextAlign;
      }
      set
      {
        if (value == this.textBoxFilterValue.TextAlign)
          return;
        this.textBoxFilterValue.TextAlign = value;
      }
    }

    private void UpdateUIStatus(bool isEnableUIStatus)
    {
      if (this.InvokeRequired)
      {
        if (this.invokeAction == null)
          this.invokeAction = new Action<bool>(this.UpdateEnableUIStatus);
        this.BeginInvoke((Delegate) this.invokeAction, (object) isEnableUIStatus, null, null);
      }
      else
        this.UpdateEnableUIStatus(isEnableUIStatus);
    }

    private void UpdateEnableUIStatus(bool isEnableUICheckedStatus)
    {
      if (this.isFilterBackendMandatory)
      {
        if (!this.checkBoxRequired.Checked)
        {
          this.checkBoxRequired.Checked = true;
          this.textBoxFilterValue.Enabled = true;
        }
        if (this.checkBoxRequired.Enabled)
          this.checkBoxRequired.Enabled = false;
        this.FireTagChangeEvent(this.checkBoxRequired.Checked, this.irowIndex, this.icolumnIndex);
      }
      else
      {
        if (!this.checkBoxRequired.Enabled)
          this.checkBoxRequired.Enabled = true;
        if (isEnableUICheckedStatus != this.checkBoxRequired.Checked)
          this.checkBoxRequired.Checked = isEnableUICheckedStatus;
        this.textBoxFilterValue.Enabled = this.checkBoxRequired.Checked;
      }
      if (this.isFilterBackendMandatory)
        this.UpdateTooltipStatus(UserControlCheckVariable.strTooltipTempateRequired);
      else
        this.UpdateTooltipStatus(UserControlCheckVariable.strTooltipTempateOptional);
    }

    private void UpdateTooltipStatus(string strTooltipTipTitle)
    {
      this.filterToolTip.ToolTipTitle = strTooltipTipTitle;
      this.filterToolTip.GetToolTip((Control) this.textBoxFilterValue);
      this.filterToolTip.SetToolTip((Control) this.splitContainer, string.Empty);
    }

    protected virtual void OnValueChanged(EventArgs e)
    {
    }

    private void textBoxFilterValue_TextChanged(object sender, EventArgs e)
    {
      this.OnFilterValueCallback(e);
    }

    public bool IsDisposedAlready
    {
      get
      {
        return this.isDisposedInstance;
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
      this.isDisposedInstance = disposing;
    }

    public string CallbackModelFormClass
    {
      get
      {
        return this.strCallbackFormClass;
      }
      set
      {
        if (value == null || value.Equals(this.strCallbackFormClass))
          return;
        this.strCallbackFormClass = value;
      }
    }

    public string CallbackModelFormPackageName
    {
      get
      {
        return this.strCallbackFormPackageName;
      }
      set
      {
        if (value == null || value.Equals(this.strCallbackFormPackageName))
          return;
        this.strCallbackFormPackageName = value;
      }
    }

    public bool IsFilterValueSelectable
    {
      get
      {
        return this.isFilterValueSelectable;
      }
      set
      {
        if (value == this.isFilterValueSelectable)
          return;
        this.isFilterValueSelectable = value;
      }
    }

    public string PerformPopupPlugin(out DialogResult result)
    {
      if (this.CallbackModelFormPackageName == null || this.CallbackModelFormPackageName.Length == 0)
        throw new InvalidOperationException("CallbackModalForm Interface hasn't been implemented - CallbackModelFromPackageName is null");
      Type type = Assembly.Load(this.CallbackModelFormPackageName).GetType(this.CallbackModelFormClass);
      if (this.CallbackModelFormClass == null || this.CallbackModelFormClass.Length == 0)
        throw new InvalidOperationException("CallbackModalForm Interface hasn't been implemented - CallbackModelFormClass is null");
      if ((Type) null == type)
        throw new InvalidOperationException("CallbackModalForm Interface is incorrect Property");
      return (Activator.CreateInstance(type) as CopernicusFilterPopupForm).ShowModalDialog(this.Value, this.filterClassMetaData, out result);
    }

    public event Action<bool, int, int> OnTagIsCellCheckedCallback;

    public int IRowIndex
    {
      get
      {
        return this.irowIndex;
      }
      set
      {
        this.irowIndex = value;
      }
    }

    public int IColumnIndex
    {
      get
      {
        return this.icolumnIndex;
      }
      set
      {
        this.icolumnIndex = value;
      }
    }

    public bool IsCellChecked
    {
      get
      {
        return this.isCellChecked;
      }
      set
      {
        if (value == this.isCellChecked)
          return;
        this.isCellChecked = value;
      }
    }

    public void AddTagChangeMonitor(Action<bool, int, int> callback, CopernicusDataGridViewCheckVariableCell _monitorInstance)
    {
      if (this.listMonitorInstances.Contains(_monitorInstance))
        return;
      this.OnTagIsCellCheckedCallback += callback;
    }

    private void textBoxFilterValue_Enter(object sender, EventArgs e)
    {
      this.isEnterForCurrentControl = true;
    }

    private void textBoxFilterValue_Leave(object sender, EventArgs e)
    {
      this.isEnterForCurrentControl = false;
    }

    public void ReceiveWindowPaneMessageInUserControl(Keys keyCode, int iRowIndex, int iColumnInde)
    {
      if (keyCode != Keys.F4 || iRowIndex != this.irowIndex || (iColumnInde != this.icolumnIndex || !this.isEnterForCurrentControl) || !this.textBoxFilterValue.Enabled)
        return;
      this.FilterValuePopupHandler();
    }

    private void FireTagChangeEvent(bool isFilterValueChecked, int iRowIndex, int iColumnIndex)
    {
      if (this.OnTagIsCellCheckedCallback == null)
        return;
      Action<bool, int, int> action = Interlocked.CompareExchange<Action<bool, int, int>>(ref this.OnTagIsCellCheckedCallback, (Action<bool, int, int>) null, (Action<bool, int, int>) null);
      if (action == null)
        return;
      action(isFilterValueChecked, iRowIndex, iColumnIndex);
    }

    private void FilterValuePopupHandler()
    {
      if (!this.IsFilterValueSelectable)
        return;
      DialogResult result;
      string str = this.PerformPopupPlugin(out result);
      if (result != DialogResult.OK || this.Value.Equals(str))
        return;
      this.Value = str;
    }

    public CopernicusPopupClassMetaData FilterClassMetaData
    {
      get
      {
        return this.filterClassMetaData;
      }
      set
      {
        if (value == null || value.Equals((object) this.filterClassMetaData))
          return;
        this.filterClassMetaData = value;
      }
    }

    protected void ProcessKey(Keys key, out bool isProcessedInside)
    {
      isProcessedInside = false;
      if (key == Keys.Left)
      {
        if (this.textBoxFilterValue.Focused && (this.RightToLeft == RightToLeft.No && (this.textBoxFilterValue.SelectionLength != 0 || this.textBoxFilterValue.SelectionStart != 0) || this.RightToLeft == RightToLeft.Yes && (this.textBoxFilterValue.SelectionLength != 0 || this.textBoxFilterValue.SelectionStart != this.textBoxFilterValue.Text.Length)))
          isProcessedInside = true;
        if (!this.checkBoxRequired.Focused)
          return;
        isProcessedInside = false;
      }
      else if (key == Keys.Right)
      {
        if (this.textBoxFilterValue.Focused && (this.RightToLeft == RightToLeft.No && (this.textBoxFilterValue.SelectionLength != 0 || this.textBoxFilterValue.SelectionStart != this.textBoxFilterValue.Text.Length) || this.RightToLeft == RightToLeft.Yes && (this.textBoxFilterValue.SelectionLength != 0 || this.textBoxFilterValue.SelectionStart != 0)))
          isProcessedInside = true;
        if (!this.checkBoxRequired.Focused)
          return;
        this.textBoxFilterValue.Focus();
        isProcessedInside = false;
      }
      else if (key == Keys.End)
      {
        if (!this.textBoxFilterValue.Focused || this.textBoxFilterValue.SelectionLength == this.textBoxFilterValue.Text.Length)
          return;
        isProcessedInside = true;
      }
      else
      {
        if (key != Keys.Delete || this.textBoxFilterValue.SelectionLength <= 0 && this.textBoxFilterValue.SelectionStart >= this.textBoxFilterValue.Text.Length)
          return;
        isProcessedInside = true;
      }
    }

    private void InitializeComponent()
    {
      this.splitContainer = new SplitContainer();
      this.checkBoxRequired = new CheckBox();
      this.textBoxFilterValue = new TextBox();
      this.splitContainer.BeginInit();
      this.splitContainer.Panel1.SuspendLayout();
      this.splitContainer.Panel2.SuspendLayout();
      this.splitContainer.SuspendLayout();
      this.SuspendLayout();
      this.splitContainer.Dock = DockStyle.Fill;
      this.splitContainer.Location = new Point(0, 0);
      this.splitContainer.Name = "splitContainer";
      this.splitContainer.Panel1.Controls.Add((Control) this.checkBoxRequired);
      this.splitContainer.Panel2.Controls.Add((Control) this.textBoxFilterValue);
      this.splitContainer.Size = new Size(276, 23);
      this.splitContainer.SplitterDistance = 25;
      this.splitContainer.SplitterWidth = 1;
      this.splitContainer.TabIndex = 0;
      this.checkBoxRequired.AutoSize = true;
      this.checkBoxRequired.Dock = DockStyle.Fill;
      this.checkBoxRequired.Location = new Point(0, 0);
      this.checkBoxRequired.Name = "checkBoxRequired";
      this.checkBoxRequired.Size = new Size(25, 23);
      this.checkBoxRequired.TabIndex = 0;
      this.checkBoxRequired.UseVisualStyleBackColor = true;
      this.checkBoxRequired.CheckedChanged += new EventHandler(this.checkBoxRequired_CheckedChanged);
      this.textBoxFilterValue.Dock = DockStyle.Fill;
      this.textBoxFilterValue.Location = new Point(0, 0);
      this.textBoxFilterValue.Name = "textBoxFilterValue";
      this.textBoxFilterValue.Size = new Size(250, 20);
      this.textBoxFilterValue.TabIndex = 0;
      this.textBoxFilterValue.TextChanged += new EventHandler(this.textBoxFilterValue_TextChanged);
      this.textBoxFilterValue.Enter += new EventHandler(this.textBoxFilterValue_Enter);
      this.textBoxFilterValue.Leave += new EventHandler(this.textBoxFilterValue_Leave);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.splitContainer);
      this.Name = nameof (UserControlCheckVariable);
      this.Size = new Size(276, 23);
      this.splitContainer.Panel1.ResumeLayout(false);
      this.splitContainer.Panel1.PerformLayout();
      this.splitContainer.Panel2.ResumeLayout(false);
      this.splitContainer.Panel2.PerformLayout();
      this.splitContainer.EndInit();
      this.splitContainer.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
