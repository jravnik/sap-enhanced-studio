﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.ConnectionModel.ConnectionDataSet
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Protocol.ConnectionModel
{
    [HelpKeyword("vs.data.DataSet")]
    [XmlSchemaProvider("GetTypedDataSetSchema")]
    [DesignerCategory("code")]
    [XmlRoot("ConnectionDataSet")]
    [ToolboxItem(true)]
    [Serializable]
    public class ConnectionDataSet : DataSet
    {
        private SchemaSerializationMode _schemaSerializationMode = SchemaSerializationMode.IncludeSchema;
        private ConnectionDataSet.SystemDataDataTable tableSystemData;

        [DebuggerNonUserCode]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        public ConnectionDataSet()
        {
            this.BeginInit();
            this.InitClass();
            CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
            base.Tables.CollectionChanged += changeEventHandler;
            base.Relations.CollectionChanged += changeEventHandler;
            this.EndInit();
        }

        [DebuggerNonUserCode]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        protected ConnectionDataSet(SerializationInfo info, StreamingContext context)
          : base(info, context, false)
        {
            if (this.IsBinarySerialized(info, context))
            {
                this.InitVars(false);
                CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
                this.Tables.CollectionChanged += changeEventHandler;
                this.Relations.CollectionChanged += changeEventHandler;
            }
            else
            {
                string s = (string)info.GetValue("XmlSchema", typeof(string));
                if (this.DetermineSchemaSerializationMode(info, context) == SchemaSerializationMode.IncludeSchema)
                {
                    DataSet dataSet = new DataSet();
                    dataSet.ReadXmlSchema((XmlReader)new XmlTextReader((TextReader)new StringReader(s)));
                    if (dataSet.Tables[nameof(SystemData)] != null)
                        base.Tables.Add((DataTable)new ConnectionDataSet.SystemDataDataTable(dataSet.Tables[nameof(SystemData)]));
                    this.DataSetName = dataSet.DataSetName;
                    this.Prefix = dataSet.Prefix;
                    this.Namespace = dataSet.Namespace;
                    this.Locale = dataSet.Locale;
                    this.CaseSensitive = dataSet.CaseSensitive;
                    this.EnforceConstraints = dataSet.EnforceConstraints;
                    this.Merge(dataSet, false, MissingSchemaAction.Add);
                    this.InitVars();
                }
                else
                    this.ReadXmlSchema((XmlReader)new XmlTextReader((TextReader)new StringReader(s)));
                this.GetSerializationData(info, context);
                CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
                base.Tables.CollectionChanged += changeEventHandler;
                this.Relations.CollectionChanged += changeEventHandler;
            }
        }

        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [DebuggerNonUserCode]
        [Browsable(false)]
        public ConnectionDataSet.SystemDataDataTable SystemData
        {
            get
            {
                return this.tableSystemData;
            }
        }

        [DebuggerNonUserCode]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        public override SchemaSerializationMode SchemaSerializationMode
        {
            get
            {
                return this._schemaSerializationMode;
            }
            set
            {
                this._schemaSerializationMode = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        [DebuggerNonUserCode]
        public new DataTableCollection Tables
        {
            get
            {
                return base.Tables;
            }
        }

        [DebuggerNonUserCode]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        public new DataRelationCollection Relations
        {
            get
            {
                return base.Relations;
            }
        }

        [DebuggerNonUserCode]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        protected override void InitializeDerivedDataSet()
        {
            this.BeginInit();
            this.InitClass();
            this.EndInit();
        }

        [DebuggerNonUserCode]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        public override DataSet Clone()
        {
            ConnectionDataSet connectionDataSet = (ConnectionDataSet)base.Clone();
            connectionDataSet.InitVars();
            connectionDataSet.SchemaSerializationMode = this.SchemaSerializationMode;
            return (DataSet)connectionDataSet;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        protected override bool ShouldSerializeTables()
        {
            return false;
        }

        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        [DebuggerNonUserCode]
        protected override bool ShouldSerializeRelations()
        {
            return false;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        protected override void ReadXmlSerializable(XmlReader reader)
        {
            if (this.DetermineSchemaSerializationMode(reader) == SchemaSerializationMode.IncludeSchema)
            {
                this.Reset();
                DataSet dataSet = new DataSet();
                int num = (int)dataSet.ReadXml(reader);
                if (dataSet.Tables["SystemData"] != null)
                    base.Tables.Add((DataTable)new ConnectionDataSet.SystemDataDataTable(dataSet.Tables["SystemData"]));
                this.DataSetName = dataSet.DataSetName;
                this.Prefix = dataSet.Prefix;
                this.Namespace = dataSet.Namespace;
                this.Locale = dataSet.Locale;
                this.CaseSensitive = dataSet.CaseSensitive;
                this.EnforceConstraints = dataSet.EnforceConstraints;
                this.Merge(dataSet, false, MissingSchemaAction.Add);
                this.InitVars();
            }
            else
            {
                int num = (int)this.ReadXml(reader);
                this.InitVars();
            }
        }

        [DebuggerNonUserCode]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        protected override XmlSchema GetSchemaSerializable()
        {
            MemoryStream memoryStream = new MemoryStream();
            this.WriteXmlSchema((XmlWriter)new XmlTextWriter((Stream)memoryStream, (Encoding)null));
            memoryStream.Position = 0L;
            return XmlSchema.Read((XmlReader)new XmlTextReader((Stream)memoryStream), (ValidationEventHandler)null);
        }

        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        [DebuggerNonUserCode]
        internal void InitVars()
        {
            this.InitVars(true);
        }

        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        [DebuggerNonUserCode]
        internal void InitVars(bool initTable)
        {
            this.tableSystemData = (ConnectionDataSet.SystemDataDataTable)base.Tables["SystemData"];
            if (!initTable || this.tableSystemData == null)
                return;
            this.tableSystemData.InitVars();
        }

        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        [DebuggerNonUserCode]
        private void InitClass()
        {
            this.DataSetName = nameof(ConnectionDataSet);
            this.Prefix = "";
            this.Namespace = "http://sap.com/copernicus/ConnectionDataSet.xsd";
            this.EnforceConstraints = true;
            this.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;
            this.tableSystemData = new ConnectionDataSet.SystemDataDataTable();
            base.Tables.Add((DataTable)this.tableSystemData);
        }

        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        [DebuggerNonUserCode]
        private bool ShouldSerializeSystemData()
        {
            return false;
        }

        [DebuggerNonUserCode]
        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        private void SchemaChanged(object sender, CollectionChangeEventArgs e)
        {
            if (e.Action != CollectionChangeAction.Remove)
                return;
            this.InitVars();
        }

        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        [DebuggerNonUserCode]
        public static XmlSchemaComplexType GetTypedDataSetSchema(XmlSchemaSet xs)
        {
            ConnectionDataSet connectionDataSet = new ConnectionDataSet();
            XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
            XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
            xmlSchemaSequence.Items.Add((XmlSchemaObject)new XmlSchemaAny()
            {
                Namespace = connectionDataSet.Namespace
            });
            schemaComplexType.Particle = (XmlSchemaParticle)xmlSchemaSequence;
            XmlSchema schemaSerializable = connectionDataSet.GetSchemaSerializable();
            if (xs.Contains(schemaSerializable.TargetNamespace))
            {
                MemoryStream memoryStream1 = new MemoryStream();
                MemoryStream memoryStream2 = new MemoryStream();
                try
                {
                    schemaSerializable.Write((Stream)memoryStream1);
                    foreach (XmlSchema schema in (IEnumerable)xs.Schemas(schemaSerializable.TargetNamespace))
                    {
                        memoryStream2.SetLength(0L);
                        schema.Write((Stream)memoryStream2);
                        if (memoryStream1.Length == memoryStream2.Length)
                        {
                            memoryStream1.Position = 0L;
                            memoryStream2.Position = 0L;
                            do
                                ;
                            while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                            if (memoryStream1.Position == memoryStream1.Length)
                                return schemaComplexType;
                        }
                    }
                }
                finally
                {
                    if (memoryStream1 != null)
                        memoryStream1.Close();
                    if (memoryStream2 != null)
                        memoryStream2.Close();
                }
            }
            xs.Add(schemaSerializable);
            return schemaComplexType;
        }

        public class SystemDataRow : DataRow
        {
            private SecureString password;
            private ConnectionDataSet.SystemDataDataTable tableSystemData;

            public string User { get; set; }

            public string UserAlias { get; set; }

            public SecureString SecurePassword
            {
                get
                {
                    return this.password;
                }
                set
                {
                    this.password = value;
                }
            }

            public string MCSKey { get; set; }

            public string MCSMode { get; set; }

            public SapSolutionCode SapSolution { get; set; }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            internal SystemDataRow(DataRowBuilder rb)
              : base(rb)
            {
                this.tableSystemData = (ConnectionDataSet.SystemDataDataTable)this.Table;
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public string Name
            {
                get
                {
                    try
                    {
                        return (string)this[this.tableSystemData.NameColumn];
                    }
                    catch (InvalidCastException ex)
                    {
                        throw new StrongTypingException("The value for column 'Name' in table 'SystemData' is DBNull.", (Exception)ex);
                    }
                }
                set
                {
                    this[this.tableSystemData.NameColumn] = (object)value;
                }
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public string Host
            {
                get
                {
                    return (string)this[this.tableSystemData.HostColumn];
                }
                set
                {
                    this[this.tableSystemData.HostColumn] = (object)value;
                }
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public int HttpPort
            {
                get
                {
                    return (int)this[this.tableSystemData.HttpPortColumn];
                }
                set
                {
                    this[this.tableSystemData.HttpPortColumn] = (object)value;
                }
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public int SSLPort
            {
                get
                {
                    try
                    {
                        return (int)this[this.tableSystemData.SSLPortColumn];
                    }
                    catch (InvalidCastException ex)
                    {
                        throw new StrongTypingException("The value for column 'SSLPort' in table 'SystemData' is DBNull.", (Exception)ex);
                    }
                }
                set
                {
                    this[this.tableSystemData.SSLPortColumn] = (object)value;
                }
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public int SysNo
            {
                get
                {
                    return (int)this[this.tableSystemData.SysNoColumn];
                }
                set
                {
                    this[this.tableSystemData.SysNoColumn] = (object)value;
                }
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public int Client
            {
                get
                {
                    try
                    {
                        return (int)this[this.tableSystemData.ClientColumn];
                    }
                    catch (InvalidCastException ex)
                    {
                        throw new StrongTypingException("The value for column 'Client' in table 'SystemData' is DBNull.", (Exception)ex);
                    }
                }
                set
                {
                    this[this.tableSystemData.ClientColumn] = (object)value;
                }
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public string Language
            {
                get
                {
                    try
                    {
                        return (string)this[this.tableSystemData.LanguageColumn];
                    }
                    catch (InvalidCastException ex)
                    {
                        throw new StrongTypingException("The value for column 'Language' in table 'SystemData' is DBNull.", (Exception)ex);
                    }
                }
                set
                {
                    this[this.tableSystemData.LanguageColumn] = (object)value;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public string Group
            {
                get
                {
                    return (string)this[this.tableSystemData.GroupColumn];
                }
                set
                {
                    this[this.tableSystemData.GroupColumn] = (object)value;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public string DefaultUser
            {
                get
                {
                    return (string)this[this.tableSystemData.DefaultUserColumn];
                }
                set
                {
                    this[this.tableSystemData.DefaultUserColumn] = (object)value;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public bool Favourite
            {
                get
                {
                    try
                    {
                        return (bool)this[this.tableSystemData.FavouriteColumn];
                    }
                    catch (InvalidCastException ex)
                    {
                        throw new StrongTypingException("The value for column 'Favourite' in table 'SystemData' is DBNull.", (Exception)ex);
                    }
                }
                set
                {
                    this[this.tableSystemData.FavouriteColumn] = (object)value;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public bool IsNameNull()
            {
                return this.IsNull(this.tableSystemData.NameColumn);
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public void SetNameNull()
            {
                this[this.tableSystemData.NameColumn] = Convert.DBNull;
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public bool IsSSLPortNull()
            {
                return this.IsNull(this.tableSystemData.SSLPortColumn);
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public void SetSSLPortNull()
            {
                this[this.tableSystemData.SSLPortColumn] = Convert.DBNull;
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public bool IsClientNull()
            {
                return this.IsNull(this.tableSystemData.ClientColumn);
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public void SetClientNull()
            {
                this[this.tableSystemData.ClientColumn] = Convert.DBNull;
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public bool IsLanguageNull()
            {
                return this.IsNull(this.tableSystemData.LanguageColumn);
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public void SetLanguageNull()
            {
                this[this.tableSystemData.LanguageColumn] = Convert.DBNull;
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public bool IsGroupNull()
            {
                return this.IsNull(this.tableSystemData.GroupColumn);
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public void SetGroupNull()
            {
                this[this.tableSystemData.GroupColumn] = Convert.DBNull;
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public bool IsDefaultUserNull()
            {
                return this.IsNull(this.tableSystemData.DefaultUserColumn);
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public void SetDefaultUserNull()
            {
                this[this.tableSystemData.DefaultUserColumn] = Convert.DBNull;
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public bool IsFavouriteNull()
            {
                return this.IsNull(this.tableSystemData.FavouriteColumn);
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public void SetFavouriteNull()
            {
                this[this.tableSystemData.FavouriteColumn] = Convert.DBNull;
            }
        }

        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        public delegate void SystemDataRowChangeEventHandler(object sender, ConnectionDataSet.SystemDataRowChangeEvent e);

        [XmlSchemaProvider("GetTypedTableSchema")]
        [Serializable]
        public class SystemDataDataTable : TypedTableBase<ConnectionDataSet.SystemDataRow>
        {
            private DataColumn columnName;
            private DataColumn columnHost;
            private DataColumn columnHttpPort;
            private DataColumn columnSSLPort;
            private DataColumn columnSysNo;
            private DataColumn columnClient;
            private DataColumn columnLanguage;
            private DataColumn columnGroup;
            private DataColumn columnDefaultUser;
            private DataColumn columnFavourite;

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public SystemDataDataTable()
            {
                this.TableName = "SystemData";
                this.BeginInit();
                this.InitClass();
                this.EndInit();
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            internal SystemDataDataTable(DataTable table)
            {
                this.TableName = table.TableName;
                if (table.CaseSensitive != table.DataSet.CaseSensitive)
                    this.CaseSensitive = table.CaseSensitive;
                if (table.Locale.ToString() != table.DataSet.Locale.ToString())
                    this.Locale = table.Locale;
                if (table.Namespace != table.DataSet.Namespace)
                    this.Namespace = table.Namespace;
                this.Prefix = table.Prefix;
                this.MinimumCapacity = table.MinimumCapacity;
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            protected SystemDataDataTable(SerializationInfo info, StreamingContext context)
              : base(info, context)
            {
                this.InitVars();
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public DataColumn NameColumn
            {
                get
                {
                    return this.columnName;
                }
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public DataColumn HostColumn
            {
                get
                {
                    return this.columnHost;
                }
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public DataColumn HttpPortColumn
            {
                get
                {
                    return this.columnHttpPort;
                }
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public DataColumn SSLPortColumn
            {
                get
                {
                    return this.columnSSLPort;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public DataColumn SysNoColumn
            {
                get
                {
                    return this.columnSysNo;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public DataColumn ClientColumn
            {
                get
                {
                    return this.columnClient;
                }
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public DataColumn LanguageColumn
            {
                get
                {
                    return this.columnLanguage;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public DataColumn GroupColumn
            {
                get
                {
                    return this.columnGroup;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public DataColumn DefaultUserColumn
            {
                get
                {
                    return this.columnDefaultUser;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public DataColumn FavouriteColumn
            {
                get
                {
                    return this.columnFavourite;
                }
            }

            [DebuggerNonUserCode]
            [Browsable(false)]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public int Count
            {
                get
                {
                    return this.Rows.Count;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public ConnectionDataSet.SystemDataRow this[int index]
            {
                get
                {
                    return (ConnectionDataSet.SystemDataRow)this.Rows[index];
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public event ConnectionDataSet.SystemDataRowChangeEventHandler SystemDataRowChanging;

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public event ConnectionDataSet.SystemDataRowChangeEventHandler SystemDataRowChanged;

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public event ConnectionDataSet.SystemDataRowChangeEventHandler SystemDataRowDeleting;

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public event ConnectionDataSet.SystemDataRowChangeEventHandler SystemDataRowDeleted;

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public void AddSystemDataRow(ConnectionDataSet.SystemDataRow row)
            {
                this.Rows.Add((DataRow)row);
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public ConnectionDataSet.SystemDataRow AddSystemDataRow(string Name, string Host, int HttpPort, int SSLPort, int SysNo, int Client, string Language, string Group, string DefaultUser, bool Favourite)
            {
                ConnectionDataSet.SystemDataRow systemDataRow = (ConnectionDataSet.SystemDataRow)this.NewRow();
                object[] objArray = new object[10] { (object)Name, (object)Host, (object)HttpPort, (object)SSLPort, (object)SysNo, (object)Client, (object)Language, (object)Group, (object)DefaultUser, (object)Favourite };
                systemDataRow.ItemArray = objArray;
                this.Rows.Add((DataRow)systemDataRow);
                return systemDataRow;
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public override DataTable Clone()
            {
                ConnectionDataSet.SystemDataDataTable systemDataDataTable = (ConnectionDataSet.SystemDataDataTable)base.Clone();
                systemDataDataTable.InitVars();
                return (DataTable)systemDataDataTable;
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            protected override DataTable CreateInstance()
            {
                return (DataTable)new ConnectionDataSet.SystemDataDataTable();
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            internal void InitVars()
            {
                this.columnName = this.Columns["Name"];
                this.columnHost = this.Columns["Host"];
                this.columnHttpPort = this.Columns["HttpPort"];
                this.columnSSLPort = this.Columns["SSLPort"];
                this.columnSysNo = this.Columns["SysNo"];
                this.columnClient = this.Columns["Client"];
                this.columnLanguage = this.Columns["Language"];
                this.columnGroup = this.Columns["Group"];
                this.columnDefaultUser = this.Columns["DefaultUser"];
                this.columnFavourite = this.Columns["Favourite"];
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            private void InitClass()
            {
                this.columnName = new DataColumn("Name", typeof(string), (string)null, MappingType.Element);
                this.Columns.Add(this.columnName);
                this.columnHost = new DataColumn("Host", typeof(string), (string)null, MappingType.Element);
                this.Columns.Add(this.columnHost);
                this.columnHttpPort = new DataColumn("HttpPort", typeof(int), (string)null, MappingType.Element);
                this.Columns.Add(this.columnHttpPort);
                this.columnSSLPort = new DataColumn("SSLPort", typeof(int), (string)null, MappingType.Element);
                this.Columns.Add(this.columnSSLPort);
                this.columnSysNo = new DataColumn("SysNo", typeof(int), (string)null, MappingType.Element);
                this.Columns.Add(this.columnSysNo);
                this.columnClient = new DataColumn("Client", typeof(int), (string)null, MappingType.Element);
                this.Columns.Add(this.columnClient);
                this.columnLanguage = new DataColumn("Language", typeof(string), (string)null, MappingType.Element);
                this.Columns.Add(this.columnLanguage);
                this.columnGroup = new DataColumn("Group", typeof(string), (string)null, MappingType.Element);
                this.Columns.Add(this.columnGroup);
                this.columnDefaultUser = new DataColumn("DefaultUser", typeof(string), (string)null, MappingType.Element);
                this.Columns.Add(this.columnDefaultUser);
                this.columnFavourite = new DataColumn("Favourite", typeof(bool), (string)null, MappingType.Element);
                this.Columns.Add(this.columnFavourite);
                this.Constraints.Add((Constraint)new UniqueConstraint("SystemDataKey1", new DataColumn[1]
                {
          this.columnName
                }, false));
                this.columnName.Unique = true;
                this.columnHost.AllowDBNull = false;
                this.columnHttpPort.AutoIncrementSeed = -1L;
                this.columnHttpPort.AutoIncrementStep = -1L;
                this.columnHttpPort.AllowDBNull = false;
                this.columnHttpPort.DefaultValue = (object)50000;
                this.columnSSLPort.DefaultValue = (object)443;
                this.columnSysNo.AllowDBNull = false;
                this.columnLanguage.DefaultValue = (object)"EN";
                this.columnGroup.DefaultValue = (object)"Other";
                this.columnGroup.AllowDBNull = true;
                this.columnDefaultUser.AllowDBNull = true;
                this.columnFavourite.DefaultValue = (object)false;
                this.columnFavourite.AllowDBNull = false;
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public ConnectionDataSet.SystemDataRow NewSystemDataRow()
            {
                return (ConnectionDataSet.SystemDataRow)this.NewRow();
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
            {
                return (DataRow)new ConnectionDataSet.SystemDataRow(builder);
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            protected override Type GetRowType()
            {
                return typeof(ConnectionDataSet.SystemDataRow);
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            protected override void OnRowChanged(DataRowChangeEventArgs e)
            {
                base.OnRowChanged(e);
                if (this.SystemDataRowChanged == null)
                    return;
                this.SystemDataRowChanged((object)this, new ConnectionDataSet.SystemDataRowChangeEvent((ConnectionDataSet.SystemDataRow)e.Row, e.Action));
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            protected override void OnRowChanging(DataRowChangeEventArgs e)
            {
                base.OnRowChanging(e);
                if (this.SystemDataRowChanging == null)
                    return;
                this.SystemDataRowChanging((object)this, new ConnectionDataSet.SystemDataRowChangeEvent((ConnectionDataSet.SystemDataRow)e.Row, e.Action));
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            protected override void OnRowDeleted(DataRowChangeEventArgs e)
            {
                base.OnRowDeleted(e);
                if (this.SystemDataRowDeleted == null)
                    return;
                this.SystemDataRowDeleted((object)this, new ConnectionDataSet.SystemDataRowChangeEvent((ConnectionDataSet.SystemDataRow)e.Row, e.Action));
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            protected override void OnRowDeleting(DataRowChangeEventArgs e)
            {
                base.OnRowDeleting(e);
                if (this.SystemDataRowDeleting == null)
                    return;
                this.SystemDataRowDeleting((object)this, new ConnectionDataSet.SystemDataRowChangeEvent((ConnectionDataSet.SystemDataRow)e.Row, e.Action));
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public void RemoveSystemDataRow(ConnectionDataSet.SystemDataRow row)
            {
                this.Rows.Remove((DataRow)row);
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
            {
                XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
                XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
                ConnectionDataSet connectionDataSet = new ConnectionDataSet();
                XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
                xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
                xmlSchemaAny1.MinOccurs = new Decimal(0);
                xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte)0);
                xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
                xmlSchemaSequence.Items.Add((XmlSchemaObject)xmlSchemaAny1);
                XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
                xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
                xmlSchemaAny2.MinOccurs = new Decimal(1);
                xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
                xmlSchemaSequence.Items.Add((XmlSchemaObject)xmlSchemaAny2);
                schemaComplexType.Attributes.Add((XmlSchemaObject)new XmlSchemaAttribute()
                {
                    Name = "namespace",
                    FixedValue = connectionDataSet.Namespace
                });
                schemaComplexType.Attributes.Add((XmlSchemaObject)new XmlSchemaAttribute()
                {
                    Name = "tableTypeName",
                    FixedValue = nameof(SystemDataDataTable)
                });
                schemaComplexType.Particle = (XmlSchemaParticle)xmlSchemaSequence;
                XmlSchema schemaSerializable = connectionDataSet.GetSchemaSerializable();
                if (xs.Contains(schemaSerializable.TargetNamespace))
                {
                    MemoryStream memoryStream1 = new MemoryStream();
                    MemoryStream memoryStream2 = new MemoryStream();
                    try
                    {
                        schemaSerializable.Write((Stream)memoryStream1);
                        foreach (XmlSchema schema in (IEnumerable)xs.Schemas(schemaSerializable.TargetNamespace))
                        {
                            memoryStream2.SetLength(0L);
                            schema.Write((Stream)memoryStream2);
                            if (memoryStream1.Length == memoryStream2.Length)
                            {
                                memoryStream1.Position = 0L;
                                memoryStream2.Position = 0L;
                                do
                                    ;
                                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                                if (memoryStream1.Position == memoryStream1.Length)
                                    return schemaComplexType;
                            }
                        }
                    }
                    finally
                    {
                        if (memoryStream1 != null)
                            memoryStream1.Close();
                        if (memoryStream2 != null)
                            memoryStream2.Close();
                    }
                }
                xs.Add(schemaSerializable);
                return schemaComplexType;
            }
        }

        [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
        public class SystemDataRowChangeEvent : EventArgs
        {
            private ConnectionDataSet.SystemDataRow eventRow;
            private DataRowAction eventAction;

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public SystemDataRowChangeEvent(ConnectionDataSet.SystemDataRow row, DataRowAction action)
            {
                this.eventRow = row;
                this.eventAction = action;
            }

            [DebuggerNonUserCode]
            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            public ConnectionDataSet.SystemDataRow Row
            {
                get
                {
                    return this.eventRow;
                }
            }

            [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [DebuggerNonUserCode]
            public DataRowAction Action
            {
                get
                {
                    return this.eventAction;
                }
            }
        }
    }
}
