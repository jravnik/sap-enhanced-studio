﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_ABSL_METH_TIP
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_ABSL_METH_TIP
  {
    [DataMember]
    public string METHOD_NAME;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string RESULT_TYPE;
    [DataMember]
    public PDI_S_ABSL_METH_TIP_PARAM[] PARAMETERS;
  }
}
