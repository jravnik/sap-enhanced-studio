﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_RETURN_MSG
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses
{
  [DataContract]
  public class OSLS_XREP_RETURN_MSG
  {
    [DataMember]
    public string TYPE;
    [DataMember]
    public string ID;
    [DataMember]
    public string NUMBER;
    [DataMember]
    public string MESSAGE;
    [DataMember]
    public string LOG_NO;
    [DataMember]
    public string LOG_MSG_NO;
    [DataMember]
    public string MESSAGE_V1;
    [DataMember]
    public string MESSAGE_V2;
    [DataMember]
    public string MESSAGE_V3;
    [DataMember]
    public string MESSAGE_V4;
    [DataMember]
    public string PARAMETER;
    [DataMember]
    public string ROW;
    [DataMember]
    public string FIELD;
    [DataMember]
    public string SYSTEM;
    [DataMember]
    public string EXCEPTION_CLASS;
  }
}
