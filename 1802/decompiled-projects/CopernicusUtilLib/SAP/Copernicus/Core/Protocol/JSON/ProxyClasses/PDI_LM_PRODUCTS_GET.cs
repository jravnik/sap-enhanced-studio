﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRODUCTS_GET
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_PRODUCTS_GET : AbstractRemoteFunction<PDI_LM_PRODUCTS_GET.ImportingType, PDI_LM_PRODUCTS_GET.ExportingType, PDI_LM_PRODUCTS_GET.ChangingType, PDI_LM_PRODUCTS_GET.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB7204C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_USER;
      [DataMember]
      public string IV_GET;
      [DataMember]
      public string IV_SOLUTION_TYPE;
      [DataMember]
      public string IV_PRODUCT_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_S_PRODUCT_VERS_AND_COMPS[] ET_PRODUCTS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
