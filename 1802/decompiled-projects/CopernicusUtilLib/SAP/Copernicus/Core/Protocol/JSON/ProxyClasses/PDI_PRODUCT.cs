﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PRODUCT
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PRODUCT
  {
    [DataMember]
    public string PRODUCT;
    [DataMember]
    public string MASTER_LANGUAGE;
    [DataMember]
    public string PRODUCT_TYPE;
    [DataMember]
    public string PPMS_NR;
    [DataMember]
    public string PARTNER;
    [DataMember]
    public string PARTNER_ID;
    [DataMember]
    public string IS_DELETED;
    [DataMember]
    public string DEV_PARTNER;
    [DataMember]
    public string CONTACT_PERSON;
    [DataMember]
    public string EMAIL;
  }
}
