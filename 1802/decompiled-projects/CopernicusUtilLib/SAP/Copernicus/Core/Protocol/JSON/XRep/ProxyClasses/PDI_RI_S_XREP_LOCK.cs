﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_XREP_LOCK
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses
{
  [DataContract]
  public class PDI_RI_S_XREP_LOCK
  {
    [DataMember]
    public string CONT_ID;
    [DataMember]
    public string TROBJTYPE;
    [DataMember]
    public string SOLUTION;
    [DataMember]
    public string BRANCH;
    [DataMember]
    public string FULLPATH_HASH;
    [DataMember]
    public string FILEPATH;
    [DataMember]
    public string IN_CHECKOUT;
    [DataMember]
    public string OPERATION_TYPE;
    [DataMember]
    public string ACTIVATION_GROUP;
    [DataMember]
    public string PRIMARY_FLAG;
    [DataMember]
    public string CHECKOUT_BY;
    [DataMember]
    public string CHECKOUT_ON;
    [DataMember]
    public string IN_MODIFICATION;
    [DataMember]
    public string EDIT_BY;
    [DataMember]
    public string EDIT_ON;
    [DataMember]
    public string SID;
    [DataMember]
    public string LAYERST;
    [DataMember]
    public string LAYERST_VERSION;
  }
}
