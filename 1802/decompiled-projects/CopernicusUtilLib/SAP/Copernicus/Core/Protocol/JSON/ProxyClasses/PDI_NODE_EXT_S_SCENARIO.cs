﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_NODE_EXT_S_SCENARIO
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_NODE_EXT_S_SCENARIO
  {
    [DataMember]
    public string SCEN_NAME_PRX;
    [DataMember]
    public string SCEN_DESCRIPTION;
    [DataMember]
    public string FLOW_DESCRIPTION;
    [DataMember]
    public string SOURCE_BO_PRX;
    [DataMember]
    public string SOURCE_ND_PRX;
    [DataMember]
    public string TARGET_BO_PRX;
    [DataMember]
    public string TARGET_ND_PRX;
    [DataMember]
    public string REF_FIELD_BUNDLE_KEY;
    [DataMember]
    public string REF_FIELD_NAME;
  }
}
