﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_SPLIT_DOWNLOAD
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_SPLIT_DOWNLOAD : AbstractRemoteFunction<PDI_SPLIT_DOWNLOAD.ImportingType, PDI_SPLIT_DOWNLOAD.ExportingType, PDI_SPLIT_DOWNLOAD.ChangingType, PDI_SPLIT_DOWNLOAD.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0975CB1ED4B79AD6AC1C161314";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PROJECT_NAME;
      [DataMember]
      public string IV_PROJECT_TYPE;
      [DataMember]
      public string IV_MINOR_VERSION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SOLUTION_ASSEMBLE;
      [DataMember]
      public string EV_JOBNAME;
      [DataMember]
      public string EV_JOBCNT;
      [DataMember]
      public string EV_APPLOG_ID;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
