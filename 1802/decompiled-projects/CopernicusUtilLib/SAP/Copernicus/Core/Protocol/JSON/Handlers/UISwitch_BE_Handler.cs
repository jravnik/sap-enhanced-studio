﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.UISwitch_BE_Handler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class UISwitch_BE_Handler : JSONHandler
  {
    public void getUISwitchUsages(string UISwitchID, out string[] UIChangeTransactionXrepPaths, out string[] UIXrepPaths)
    {
      UIChangeTransactionXrepPaths = (string[]) null;
      UIXrepPaths = (string[]) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_GET_UI_SWITCH_USAGES getUiSwitchUsages = new PDI_GET_UI_SWITCH_USAGES();
      getUiSwitchUsages.Importing = new PDI_GET_UI_SWITCH_USAGES.ImportingType();
      getUiSwitchUsages.Importing.IV_UI_SWITCH_ID = UISwitchID;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) getUiSwitchUsages, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (getUiSwitchUsages.Exporting == null)
        return;
      if (getUiSwitchUsages.Exporting.EV_SUCCESS == "X")
      {
        UIChangeTransactionXrepPaths = getUiSwitchUsages.Exporting.ET_UICHANGE_TR_XREP_PATHS;
        UIXrepPaths = getUiSwitchUsages.Exporting.ET_UI_XREP_PATHS;
      }
      else
        this.reportServerSideProtocolException((SAPFunctionModule) getUiSwitchUsages, false, false, false);
    }
  }
}
