﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.JSONHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;

namespace SAP.Copernicus.Core.Protocol.JSON
{
  public class JSONHandler
  {
    protected void reportClientSideProtocolException(Exception ex)
    {
      if (ex is ProtocolException)
        throw ex;
      Trace.WriteLine(string.Format("Error: {0}", (object) ex.Message), "JSONClient");
      Trace.WriteLine((object) ex);
      if (ex is ThreadAbortException || ex is ThreadInterruptedException)
        throw ex;
      string str = "JSON communiation error. Reason: " + ex.Message;
      ProtocolException protocolException = new ProtocolException(ProtocolException.ErrorArea.CLIENT, ex);
      protocolException.showPopup();
      throw protocolException;
    }

    protected void reportServerSideProtocolException(SAPFunctionModule proxy, bool dontThrowException = false, bool prodfixWCassigned = false, bool showMsgsOnSuccess = false)
    {
      if (proxy == null)
      {
        string message = "JSON communiation failure. Client proxy is null.";
        Trace.TraceError(message);
        ProtocolException protocolException = new ProtocolException(ProtocolException.ErrorArea.CLIENT, message);
        protocolException.showPopup();
        if (!dontThrowException)
          throw protocolException;
      }
      object exportingValue = proxy.GetType().GetProperty("Exporting").GetValue((object) proxy, (object[]) null);
      if (exportingValue == null)
      {
        string message = "JSON communiation failure. Severe server side error. Exporting parameter is null.";
        Trace.TraceError(message);
        ProtocolException protocolException = new ProtocolException(ProtocolException.ErrorArea.SERVER, message);
        protocolException.showPopup();
        if (!dontThrowException)
          throw protocolException;
      }
      this.HandleMessages(exportingValue, dontThrowException, prodfixWCassigned, showMsgsOnSuccess);
    }

    protected void HandleMessages(object exportingValue, bool dontThrowException = false, bool prodfixWCassigned = false, bool showMsgsOnSuccess = false)
    {
      Type type = exportingValue.GetType();
      FieldInfo field1 = type.GetField("EV_SUCCESS");
      FieldInfo field2 = type.GetField("ET_MESSAGES");
      if (field1 == (FieldInfo) null || field2 == (FieldInfo) null)
      {
        Trace.TraceInformation("RFC has no EV_SUCCESS or ET_MESSAGES exporting parameter defined.");
      }
      else
      {
        object obj1 = field1.GetValue(exportingValue);
        object obj2 = field2.GetValue(exportingValue);
        string str = obj1 as string;
        PDI_RI_S_MESSAGE[] pdiRiSMessageArray = obj2 as PDI_RI_S_MESSAGE[];
        if (str == null || pdiRiSMessageArray == null)
        {
          string message = "Severe server side error: ES_ERROR_INFO and ET_MESSAGES exporting parameters are defined, but not provided by the backened.";
          Trace.TraceError(message);
          ProtocolException protocolException = new ProtocolException(ProtocolException.ErrorArea.SERVER, message);
          protocolException.showPopup();
          if (!dontThrowException)
            throw protocolException;
        }
        else if (str != "X")
        {
          List<Message> msgs = new List<Message>();
          foreach (PDI_RI_S_MESSAGE pdiRiSMessage in pdiRiSMessageArray)
          {
            Message.MessageSeverity severity = Message.MessageSeverity.FromABAPCode(pdiRiSMessage.SEVERITY);
            msgs.Add(new Message(severity, pdiRiSMessage.TEXT));
          }
          if (msgs.Count == 0)
            return;
          ProtocolException protocolException = new ProtocolException(ProtocolException.ErrorArea.SERVER, msgs);
          Trace.TraceError("Backend failure: " + protocolException.ToString());
          protocolException.showPopup();
          if (!dontThrowException)
            throw protocolException;
        }
        else
        {
          if (prodfixWCassigned.Equals(true))
          {
            List<Message> msgs = new List<Message>();
            foreach (PDI_RI_S_MESSAGE pdiRiSMessage in pdiRiSMessageArray)
            {
              Message.MessageSeverity severity = Message.MessageSeverity.FromABAPCode(pdiRiSMessage.SEVERITY);
              msgs.Add(new Message(severity, pdiRiSMessage.TEXT));
            }
            if (msgs.Count != 0)
            {
              ProtocolException protocolException = new ProtocolException(ProtocolException.ErrorArea.SERVER, msgs);
              Trace.TraceError("Backend failure: " + protocolException.ToString());
              protocolException.showPopup();
            }
          }
          if (!showMsgsOnSuccess.Equals(true))
            return;
          List<Message> msgs1 = new List<Message>();
          foreach (PDI_RI_S_MESSAGE pdiRiSMessage in pdiRiSMessageArray)
          {
            Message.MessageSeverity severity = Message.MessageSeverity.FromABAPCode(pdiRiSMessage.SEVERITY);
            msgs1.Add(new Message(severity, pdiRiSMessage.TEXT));
          }
          if (msgs1.Count == 0)
            return;
          ProtocolException protocolException1 = new ProtocolException(ProtocolException.ErrorArea.SERVER, msgs1);
          Trace.TraceError(protocolException1.ToString());
          protocolException1.showPopup();
        }
      }
    }
  }
}
