﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_AE_GET_VALUE_HELP
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_AE_GET_VALUE_HELP : AbstractRemoteFunction<PDI_AE_GET_VALUE_HELP.ImportingType, PDI_AE_GET_VALUE_HELP.ExportingType, PDI_AE_GET_VALUE_HELP.ChangingType, PDI_AE_GET_VALUE_HELP.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0231F61EE192DF976CCE4796A6";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_NAMESPACE;
      [DataMember]
      public string IV_SPOT_NAME;
      [DataMember]
      public string IV_BADI_DEF_NAME;
    }

    [DataContract]
    public class PDI_AE_S_CODE_VALUE_W_DESC
    {
      [DataMember]
      public string CONTENT;
      [DataMember]
      public string DESCRIPTION;

      public string GetContent
      {
        get
        {
          return this.CONTENT;
        }
        set
        {
          this.CONTENT = value;
        }
      }

      public string GetDescription
      {
        get
        {
          return this.DESCRIPTION;
        }
        set
        {
          this.DESCRIPTION = value;
        }
      }

      public string GetCodeAndDescription
      {
        get
        {
          return this.CONTENT.Trim() + "-" + this.DESCRIPTION.Trim();
        }
      }
    }

    [DataContract]
    public class PDI_AE_S_CLS_CODE_LIST
    {
      [DataMember]
      public string FILTER_NAME;
      [DataMember]
      public PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC[] VALUE_LIST;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_AE_GET_VALUE_HELP.PDI_AE_S_CLS_CODE_LIST[] ET_VALUE_LIST;
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
