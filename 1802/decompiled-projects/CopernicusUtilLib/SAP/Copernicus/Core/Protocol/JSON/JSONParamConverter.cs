﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.JSONParamConverter
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.JSON
{
  public static class JSONParamConverter
  {
    public static List<SyntaxErrorInfo> ET_MESSAGES2SyntaxErrorInfoList(PDI_SFW_S_MESSAGE[] etMessages)
    {
      List<SyntaxErrorInfo> syntaxErrorInfoList = new List<SyntaxErrorInfo>();
      foreach (PDI_SFW_S_MESSAGE etMessage in etMessages)
      {
        SyntaxErrorInfo syntaxErrorInfo = new SyntaxErrorInfo(etMessage.TYPE, etMessage.METHOD, etMessage.MESSAGE);
        syntaxErrorInfoList.Add(syntaxErrorInfo);
      }
      return syntaxErrorInfoList;
    }
  }
}
