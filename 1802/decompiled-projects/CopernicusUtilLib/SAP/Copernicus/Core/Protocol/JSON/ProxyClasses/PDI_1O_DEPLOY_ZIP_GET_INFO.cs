﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_DEPLOY_ZIP_GET_INFO
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_DEPLOY_ZIP_GET_INFO : AbstractRemoteFunction<PDI_1O_DEPLOY_ZIP_GET_INFO.ImportingType, PDI_1O_DEPLOY_ZIP_GET_INFO.ExportingType, PDI_1O_DEPLOY_ZIP_GET_INFO.ChangingType, PDI_1O_DEPLOY_ZIP_GET_INFO.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0135861ED0B6BED978E85FC547";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION_ASSEMBLE;
      [DataMember]
      public string IV_FILE_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SOLUTION_NAME;
      [DataMember]
      public string EV_PATCH_VERSION;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_TOBE_ALIASED;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_MAJOR_VERSION;
      [DataMember]
      public string EV_CORRECTION_SOLUTION;
      [DataMember]
      public string EV_DESCRIPTION;
      [DataMember]
      public PDI_1O_S_SDI_FILE[] ET_WEB_SERVICE;
      [DataMember]
      public string EV_WRITE_ACCESS_TO_SAP_BO;
      [DataMember]
      public string EV_PROJECT_TYPE;
      [DataMember]
      public string EV_SAP_ENTITIES_USED;
      [DataMember]
      public string EV_SAP_ENTITY_ACCESS;
      [DataMember]
      public string EV_USAGE_CATEGORY_OUTBOUND;
      [DataMember]
      public string EV_COMPILER_MIG_NEEDED;
      [DataMember]
      public string EV_REQ_MULTICUST_INST_KEY;
      [DataMember]
      public string EV_EXT_SOLUTION_NAME;
      [DataMember]
      public string EV_CAN_IMPORT_ORIGINAL;
      [DataMember]
      public string EV_PATCH_SOLUTION_EXISTS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }

    public class SAP_ENTITY_ACCESS
    {
      public const string Write = "W";
      public const string Read = "R";
      public const string No_Access = "N";
      public const string Old_Solution = "";
    }
  }
}
