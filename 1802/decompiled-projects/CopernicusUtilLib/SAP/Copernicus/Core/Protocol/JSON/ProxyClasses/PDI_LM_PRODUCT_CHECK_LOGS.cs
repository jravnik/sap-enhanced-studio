﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRODUCT_CHECK_LOGS
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_PRODUCT_CHECK_LOGS : AbstractRemoteFunction<PDI_LM_PRODUCT_CHECK_LOGS.ImportingType, PDI_LM_PRODUCT_CHECK_LOGS.ExportingType, PDI_LM_PRODUCT_CHECK_LOGS.ChangingType, PDI_LM_PRODUCT_CHECK_LOGS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F51EE5B6DCCA64FA5A552C";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_PASS_CHK_IND;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_T_APPLOG_MSG[] ET_APPLOG;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
