﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRODUCT_UI_GET_STATUS
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_PRODUCT_UI_GET_STATUS : AbstractRemoteFunction<PDI_LM_PRODUCT_UI_GET_STATUS.ImportingType, PDI_LM_PRODUCT_UI_GET_STATUS.ExportingType, PDI_LM_PRODUCT_UI_GET_STATUS.ChangingType, PDI_LM_PRODUCT_UI_GET_STATUS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01151C1ED08DA162C695CA15A1";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_OVERALL_STATUS;
      [DataMember]
      public string EV_CERTIFICATION_STATUS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_EDIT_STATUS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
