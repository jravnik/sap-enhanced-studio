﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.MessageDialog
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.JSON
{
    public class MessageDialog : Form
    {
        private IContainer components;
        private Button buttonOk;
        private DataGridView dataGridViewMessages;
        private ImageList imageList;
        private DataGridViewImageColumn Severity;
        private DataGridViewTextBoxColumn Message;
        private DataGridViewImageColumn dataGridViewImageColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private bool blockForm;

        public MessageDialog(List<SAP.Copernicus.Core.Protocol.JSON.Message> messages)
        {
            blockForm = false;

            if (messages.Count == 1)
            {
                if (messages[0].Text == "Production fix not allowed as solution not deployed")
                {
                    blockForm = true;
                }
                else if (messages[0].Text == "Production fix not allowed as solution deployment is not finished")
                {
                    blockForm = true;
                }
            }

            this.InitializeComponent();
            this.Icon = Resource.SAPBusinessByDesignStudioIcon;
            this.dataGridViewMessages.Rows.Add(messages.Count);
            for (int index = 0; index < messages.Count; ++index)
            {
                Image image = this.imageList.Images[messages[index].Severity.IconIndex];
                this.dataGridViewMessages.Rows[index].Cells[0].Value = (object)image;
                this.dataGridViewMessages.Rows[index].Cells[1].Value = (object)messages[index].Text;
            }
        }

        public bool BlockForm
        {
            get
            {
                return blockForm;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
            DataGridViewCellStyle gridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle gridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle gridViewCellStyle3 = new DataGridViewCellStyle();
            DataGridViewCellStyle gridViewCellStyle4 = new DataGridViewCellStyle();
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(MessageDialog));
            this.buttonOk = new Button();
            this.dataGridViewMessages = new DataGridView();
            this.imageList = new ImageList(this.components);
            this.dataGridViewImageColumn1 = new DataGridViewImageColumn();
            this.Severity = new DataGridViewImageColumn();
            this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.Message = new DataGridViewTextBoxColumn();
            ((ISupportInitialize)this.dataGridViewMessages).BeginInit();
            this.SuspendLayout();
            this.buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            this.buttonOk.Location = new Point(407, 137);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new Size(75, 23);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new EventHandler(this.buttonOk_Click);
            this.dataGridViewMessages.AllowUserToAddRows = false;
            this.dataGridViewMessages.AllowUserToDeleteRows = false;
            this.dataGridViewMessages.AllowUserToResizeColumns = false;
            this.dataGridViewMessages.AllowUserToResizeRows = false;
            this.dataGridViewMessages.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            this.dataGridViewMessages.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewMessages.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewMessages.BackgroundColor = SystemColors.Window;
            gridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleLeft;
            gridViewCellStyle1.BackColor = SystemColors.Control;
            gridViewCellStyle1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte)0);
            gridViewCellStyle1.ForeColor = SystemColors.WindowText;
            gridViewCellStyle1.SelectionBackColor = SystemColors.Highlight;
            gridViewCellStyle1.SelectionForeColor = SystemColors.HighlightText;
            gridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            this.dataGridViewMessages.ColumnHeadersDefaultCellStyle = gridViewCellStyle1;
            this.dataGridViewMessages.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMessages.Columns.AddRange((DataGridViewColumn)this.Severity, (DataGridViewColumn)this.Message);
            gridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            gridViewCellStyle2.BackColor = SystemColors.Window;
            gridViewCellStyle2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte)0);
            gridViewCellStyle2.ForeColor = SystemColors.ControlText;
            gridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
            gridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
            gridViewCellStyle2.WrapMode = DataGridViewTriState.False;
            this.dataGridViewMessages.DefaultCellStyle = gridViewCellStyle2;
            this.dataGridViewMessages.Location = new Point(12, 12);
            this.dataGridViewMessages.Name = "dataGridViewMessages";
            this.dataGridViewMessages.ReadOnly = true;
            gridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
            gridViewCellStyle3.BackColor = SystemColors.Control;
            gridViewCellStyle3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte)0);
            gridViewCellStyle3.ForeColor = SystemColors.WindowText;
            gridViewCellStyle3.SelectionBackColor = SystemColors.Highlight;
            gridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
            gridViewCellStyle3.WrapMode = DataGridViewTriState.True;
            this.dataGridViewMessages.RowHeadersDefaultCellStyle = gridViewCellStyle3;
            this.dataGridViewMessages.RowHeadersVisible = false;
            gridViewCellStyle4.WrapMode = DataGridViewTriState.True;
            this.dataGridViewMessages.RowsDefaultCellStyle = gridViewCellStyle4;
            this.dataGridViewMessages.ScrollBars = ScrollBars.Vertical;
            this.dataGridViewMessages.Size = new Size(470, 119);
            this.dataGridViewMessages.TabIndex = 1;
            this.imageList.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("imageList.ImageStream");
            this.imageList.TransparentColor = Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Error.png");
            this.imageList.Images.SetKeyName(1, "Information.png");
            this.imageList.Images.SetKeyName(2, "Success.png");
            this.imageList.Images.SetKeyName(3, "Warning.png");
            this.dataGridViewImageColumn1.Frozen = true;
            this.dataGridViewImageColumn1.HeaderText = "Severity";
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 51;
            this.Severity.Frozen = true;
            this.Severity.HeaderText = "Severity";
            this.Severity.Name = "Severity";
            this.Severity.ReadOnly = true;
            this.Severity.Width = 51;
            this.dataGridViewTextBoxColumn1.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Message";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.Message.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.Message.HeaderText = "Message";
            this.Message.Name = "Message";
            this.Message.ReadOnly = true;
            this.AccessibleName = nameof(MessageDialog);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(494, 172);
            this.Controls.Add((Control)this.dataGridViewMessages);
            this.Controls.Add((Control)this.buttonOk);
            this.Name = nameof(MessageDialog);
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "Server Messages";
            this.TopMost = true;
            ((ISupportInitialize)this.dataGridViewMessages).EndInit();
            this.ResumeLayout(false);
        }
    }
}
