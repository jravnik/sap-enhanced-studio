﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_A2X_GET_REUSE_INFO
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_A2X_GET_REUSE_INFO : AbstractRemoteFunction<PDI_RI_A2X_GET_REUSE_INFO.ImportingType, PDI_RI_A2X_GET_REUSE_INFO.ExportingType, PDI_RI_A2X_GET_REUSE_INFO.ChangingType, PDI_RI_A2X_GET_REUSE_INFO.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194F015C5248C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_TRG_DO_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public TY_REUSE_INFO[] ET_REUSE_TYPES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
