﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.DecisionTreeHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class DecisionTreeHandler : JSONHandler
  {
    public void getTaxContent(ref List<object> Tables, string langu, string country)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_GET_TAX_CONTENT pdiBcGetTaxContent = new PDI_BC_GET_TAX_CONTENT();
      pdiBcGetTaxContent.Importing = new PDI_BC_GET_TAX_CONTENT.ImportingType();
      pdiBcGetTaxContent.Importing.IV_LANGU = langu;
      pdiBcGetTaxContent.Importing.IV_COUNTRY = country;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) pdiBcGetTaxContent, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return;
      }
      if (pdiBcGetTaxContent.Exporting == null)
        return;
      if (pdiBcGetTaxContent.Exporting.EV_SUCCESS == "" && pdiBcGetTaxContent.Exporting.ET_MESSAGES != null && pdiBcGetTaxContent.Exporting.ET_MESSAGES.Length > 0)
        this.reportServerSideProtocolException((SAPFunctionModule) pdiBcGetTaxContent, false, false, false);
      if (pdiBcGetTaxContent.Exporting.ET_TAX_EVENTS != null)
        Tables.Add((object) pdiBcGetTaxContent.Exporting.ET_TAX_EVENTS);
      if (pdiBcGetTaxContent.Exporting.ET_TAX_TYPES != null)
        Tables.Add((object) pdiBcGetTaxContent.Exporting.ET_TAX_TYPES);
      if (pdiBcGetTaxContent.Exporting.ET_TAX_EXEM_RSNS == null)
        return;
      Tables.Add((object) pdiBcGetTaxContent.Exporting.ET_TAX_EXEM_RSNS);
    }

    public string gettreeContent(ref List<object> Tables, string treeid, string treedesc, string country, string xmlstring, bool whtTaxCase)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BC_ACTIVATE_DT_CONTENT activateDtContent = new PDI_BC_ACTIVATE_DT_CONTENT();
      activateDtContent.Importing = new PDI_BC_ACTIVATE_DT_CONTENT.ImportingType();
      activateDtContent.Importing.IV_COUNTRY = country;
      activateDtContent.Importing.IV_PATH = xmlstring;
      activateDtContent.Importing.IV_TREE_DESC = treedesc;
      activateDtContent.Importing.IV_TREE_ID = treeid;
      activateDtContent.Importing.IV_WHT = whtTaxCase;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) activateDtContent, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return (string) null;
      }
      if (activateDtContent.Exporting != null)
      {
        if (activateDtContent.Exporting.EV_SUCCESS == "" && activateDtContent.Exporting.ET_MESSAGES != null && activateDtContent.Exporting.ET_MESSAGES.Length > 0)
          this.reportServerSideProtocolException((SAPFunctionModule) activateDtContent, false, false, false);
        if (activateDtContent.Exporting.E_DT != null)
          Tables.Add((object) activateDtContent.Exporting.E_DT);
        if (activateDtContent.Exporting.E_DT_T != null)
          Tables.Add((object) activateDtContent.Exporting.E_DT_T);
        if (activateDtContent.Exporting.E_DT_DATA != null)
          Tables.Add((object) activateDtContent.Exporting.E_DT_DATA);
        if (activateDtContent.Exporting.E_DT_TEXTS != null)
          Tables.Add((object) activateDtContent.Exporting.E_DT_TEXTS);
        if (activateDtContent.Exporting.E_DT_TGTCTX != null)
          Tables.Add((object) activateDtContent.Exporting.E_DT_TGTCTX);
        if (activateDtContent.Exporting.E_DT_DECTR_DET != null)
          Tables.Add((object) activateDtContent.Exporting.E_DT_DECTR_DET);
      }
      return activateDtContent.Exporting.E_STRING;
    }

    public void getTreeContentFromXML(ref List<object> Tables, string treeid, string treetype, string version, string mode, string xmldoc, TTEC_DT_TEXTS[] texts)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      TTE_DT_TREE_GET_PDI tteDtTreeGetPdi = new TTE_DT_TREE_GET_PDI();
      tteDtTreeGetPdi.Importing = new TTE_DT_TREE_GET_PDI.ImportingType();
      tteDtTreeGetPdi.Importing.I_TREEID = treeid;
      tteDtTreeGetPdi.Importing.I_TREETYPE = treetype;
      tteDtTreeGetPdi.Importing.I_VERSION = version;
      tteDtTreeGetPdi.Importing.I_MODE = mode;
      tteDtTreeGetPdi.Importing.IV_XMLDOC = xmldoc;
      tteDtTreeGetPdi.Importing.I_DT_TEXTS = texts;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) tteDtTreeGetPdi, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return;
      }
      if (tteDtTreeGetPdi.Exporting == null)
        return;
      if (tteDtTreeGetPdi.Exporting.ET_TREE_NODES != null)
        Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_TREE_NODES);
      if (tteDtTreeGetPdi.Exporting.ET_HDR_TARGET != null)
        Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_HDR_TARGET);
      if (tteDtTreeGetPdi.Exporting.ET_TARGET_CTX != null)
        Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_TARGET_CTX);
      if (tteDtTreeGetPdi.Exporting.ET_TEST_FIELDS != null)
        Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_TEST_FIELDS);
      if (tteDtTreeGetPdi.Exporting.ET_TEST_VALUES != null)
        Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_TEST_VALUES);
      if (tteDtTreeGetPdi.Exporting.ET_TEST_CTX != null)
        Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_TEST_CTX);
      if (tteDtTreeGetPdi.Exporting.ET_TEST_CMP != null)
        Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_TEST_CMP);
      if (tteDtTreeGetPdi.Exporting.ET_RESULT_NODES != null)
        Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_RESULT_NODES);
      if (tteDtTreeGetPdi.Exporting.ET_RESULT_CTX != null)
        Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_RESULT_CTX);
      if (tteDtTreeGetPdi.Exporting.ET_ERROR_NODES != null)
        Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_ERROR_NODES);
      if (tteDtTreeGetPdi.Exporting.ET_NOTES == null)
        return;
      Tables.Add((object) tteDtTreeGetPdi.Exporting.ET_NOTES);
    }
  }
}
