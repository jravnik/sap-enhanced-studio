﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_SENSITIVE_QUERY_INFO
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_GET_SENSITIVE_QUERY_INFO : AbstractRemoteFunction<PDI_GET_SENSITIVE_QUERY_INFO.ImportingType, PDI_GET_SENSITIVE_QUERY_INFO.ExportingType, PDI_GET_SENSITIVE_QUERY_INFO.ChangingType, PDI_GET_SENSITIVE_QUERY_INFO.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F51EE7B5CAC947CAEB4814";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BO_NAME;
      [DataMember]
      public string IV_NODE_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_IS_SENSITIVE;
      [DataMember]
      public string[] ET_SENSITIVE_QUERY_NEW_FIELDS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
