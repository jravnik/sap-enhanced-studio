﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_SYSTEM_LOGON
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_SYSTEM_LOGON : AbstractRemoteFunction<PDI_LM_SYSTEM_LOGON.ImportingType, PDI_LM_SYSTEM_LOGON.ExportingType, PDI_LM_SYSTEM_LOGON.ChangingType, PDI_LM_SYSTEM_LOGON.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB7206C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SID;
      [DataMember]
      public string IV_DESCRIPTION;
      [DataMember]
      public int IV_TIMEOUT;
      [DataMember]
      public PDI_S_LOGON_COMPONENT IS_LOGON_COMPONENT;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SID;
      [DataMember]
      public PDI_PARTNER EV_PARTNER;
      [DataMember]
      public string EV_USER_IS_ADMIN;
      [DataMember]
      public PDI_S_LOGON_RETURN_CODE ES_LOGON_COMPONENT_RC;
      [DataMember]
      public PDI_S_LOGON_DETAILS ES_LOGON_DETAILS;
      [DataMember]
      public string EV_CLIENT;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
