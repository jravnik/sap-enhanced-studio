﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_T_APPLOG_MSG
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_T_APPLOG_MSG
  {
    [DataMember]
    public string MANDT;
    [DataMember]
    public string LOG_ID;
    [DataMember]
    public string LOG_TIMESTAMP;
    [DataMember]
    public string LOG_MSG;
    [DataMember]
    public string LOG_SEVERITY;
    [DataMember]
    public string LOG_LEVEL;
    [DataMember]
    public string LOG_DETAIL;
  }
}
