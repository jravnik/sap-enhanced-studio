﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_S_SPAN
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.TextManager.Interop;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_S_SPAN
  {
    [DataMember]
    public int START_LINE;
    [DataMember]
    public int START_COL;
    [DataMember]
    public int END_LINE;
    [DataMember]
    public int END_COL;
    private TextSpan? _textspan;

    public TextSpan textspan
    {
      get
      {
        if (!this._textspan.HasValue)
          this._textspan = new TextSpan?(new TextSpan()
          {
            iStartLine = this.START_LINE,
            iStartIndex = this.START_COL,
            iEndLine = this.END_LINE,
            iEndIndex = this.END_COL
          });
        return this._textspan.Value;
      }
      private set
      {
      }
    }
  }
}
