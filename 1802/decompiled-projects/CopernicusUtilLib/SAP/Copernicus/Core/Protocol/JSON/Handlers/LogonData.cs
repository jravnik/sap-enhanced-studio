﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.LogonData
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class LogonData
  {
    public bool UsePSM = true;
    public string backend_user;
    public string ValidatedXRepSessionID;
    public bool IsAdminUser;
    public string PartnerDomain;
    public string PartnerID;
    public string PartnerName;
    public string client;
    public UsageMode UsageMode;
    public SystemMode SystemMode;
    public RestrictionMode RestrictionMode;
    public TenantRoleCode TenantRoleCode;
    public SapSolutionCode SapSolutionCode;
    public string officialName;
    public string releaseName;
    public bool IsMultiCustomerEnabled;
  }
}
