﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_USG_DATA
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_USG_DATA
  {
    [DataMember]
    public string SOURCE_FILE_NAME;
    [DataMember]
    public string TARGET_ENTITY_NAME;
    [DataMember]
    public int USAGE_TYPE;
    [DataMember]
    public char CROSS_DU;
    [DataMember]
    public char WRITE_ACCESS;
    [DataMember]
    public char DEFINES;
    [DataMember]
    public string PROCESS_COMPONENT;
  }
}
