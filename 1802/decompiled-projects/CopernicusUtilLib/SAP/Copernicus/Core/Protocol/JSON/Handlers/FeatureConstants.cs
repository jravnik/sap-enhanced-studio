﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.FeatureConstants
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class FeatureConstants
  {
    public const string queryFeed = "QRY_FEED";
    public const string queryTTnav = "QRY_ES_TT_NAV";
    public const string firstImplProjTempl = "IMPL_PROJ_TMPL";
    public const string fineTuneImplProjTempl = "FT_IMPL_PROJ_TMPL";
  }
}
