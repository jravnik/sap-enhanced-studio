﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PI_CHECK_WSDL
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PI_CHECK_WSDL : AbstractRemoteFunction<PDI_PI_CHECK_WSDL.ImportingType, PDI_PI_CHECK_WSDL.ExportingType, PDI_PI_CHECK_WSDL.ChangingType, PDI_PI_CHECK_WSDL.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB720AC9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_URI;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_MESSAGE;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
