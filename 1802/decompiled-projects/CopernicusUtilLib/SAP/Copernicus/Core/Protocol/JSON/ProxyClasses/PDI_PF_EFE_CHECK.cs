﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PF_EFE_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PF_EFE_CHECK : AbstractRemoteFunction<PDI_PF_EFE_CHECK.ImportingType, PDI_PF_EFE_CHECK.ExportingType, PDI_PF_EFE_CHECK.ChangingType, PDI_PF_EFE_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return nameof (PDI_PF_EFE_CHECK);
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string[] IT_XREP_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
