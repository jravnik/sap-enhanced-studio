﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.XBOHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class XBOHandler : JSONHandler
  {
    public bool MigrateXBO(string solution, string nodeName)
    {
      try
      {
        PDI_EXT_ACT_BO_MIGRATION extActBoMigration = new PDI_EXT_ACT_BO_MIGRATION();
        extActBoMigration.Importing = new PDI_EXT_ACT_BO_MIGRATION.ImportingType();
        extActBoMigration.Importing.IV_SOLUTION = solution;
        extActBoMigration.Importing.IV_NODE_NAME = nodeName;
        Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) extActBoMigration, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) extActBoMigration, true, false, false);
        if (extActBoMigration.Exporting != null)
          return extActBoMigration.Exporting.EV_SUCCESS.Equals("X");
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }
  }
}
