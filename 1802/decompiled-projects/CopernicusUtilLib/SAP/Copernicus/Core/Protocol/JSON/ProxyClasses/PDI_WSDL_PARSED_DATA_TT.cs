﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_WSDL_PARSED_DATA_TT
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_WSDL_PARSED_DATA_TT
  {
    [DataMember]
    public string OPERATION_NAME;
    [DataMember]
    public string MESSAGE_TYPE_PRX_NAME;
    [DataMember]
    public PDI_WSDL_PRX_COMP_TT[] PARSED_OUT_DATA;
    [DataMember]
    public char MESSAGE_TYPE;
  }
}
