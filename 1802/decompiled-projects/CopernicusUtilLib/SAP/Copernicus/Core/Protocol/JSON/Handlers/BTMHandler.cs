﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.BTMHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class BTMHandler : JSONHandler
  {
    public bool UpdateTask(string XrepPath)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_BTM_TASK_UPDATE pdiRiBtmTaskUpdate = new PDI_RI_BTM_TASK_UPDATE();
        pdiRiBtmTaskUpdate.Importing = new PDI_RI_BTM_TASK_UPDATE.ImportingType();
        pdiRiBtmTaskUpdate.Importing.IV_XREP_PATH = XrepPath;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiBtmTaskUpdate, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiRiBtmTaskUpdate, false, false, false);
        if (pdiRiBtmTaskUpdate.Exporting != null)
          return pdiRiBtmTaskUpdate.Exporting.EV_SUCCESS.Equals("X");
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }

    public bool checkIfApprovalIdExist(string ProcessId, string xrepPath)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_CHECK_APPROVAL_ID riCheckApprovalId = new PDI_RI_CHECK_APPROVAL_ID();
        riCheckApprovalId.Importing = new PDI_RI_CHECK_APPROVAL_ID.ImportingType();
        riCheckApprovalId.Importing.IV_ID = ProcessId;
        riCheckApprovalId.Importing.IV_XREP_PATH = xrepPath;
        jsonClient.callFunctionModule((SAPFunctionModule) riCheckApprovalId, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) riCheckApprovalId, false, false, false);
        if (riCheckApprovalId.Exporting != null)
          return riCheckApprovalId.Exporting.EV_ID_FOUND.Equals("X");
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }
  }
}
