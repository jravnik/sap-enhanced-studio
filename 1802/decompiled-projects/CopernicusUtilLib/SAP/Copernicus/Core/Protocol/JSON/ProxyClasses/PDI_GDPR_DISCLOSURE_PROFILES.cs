﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GDPR_DISCLOSURE_PROFILES
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_GDPR_DISCLOSURE_PROFILES : AbstractRemoteFunction<PDI_GDPR_DISCLOSURE_PROFILES.ImportingType, PDI_GDPR_DISCLOSURE_PROFILES.ExportingType, PDI_GDPR_DISCLOSURE_PROFILES.ChangingType, PDI_GDPR_DISCLOSURE_PROFILES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F91ED79E810E43BE8D8374";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public ET_PROFILE[] ET_PROFILE;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
