﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_SOLUTION_VERSION
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_1O_SOLUTION_VERSION
  {
    [DataMember]
    public string VERSION_ID;
    [DataMember]
    public string VERSION_STATUS;
    [DataMember]
    public string VERSION_STATUS_TEXT;
    [DataMember]
    public string CHANGE_DATETIME;
    [DataMember]
    public string PHASE;
    [DataMember]
    public string PHASE_STATUS;
    [DataMember]
    public string APPL_COMPONENT;
    [DataMember]
    public PDI_1O_SOLUTION_VERS_STATUS[] SOLUTION_VERS_STATUS_LIST;
  }
}
