﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PF_CODE_GENERATE
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PF_CODE_GENERATE : AbstractRemoteFunction<PDI_PF_CODE_GENERATE.ImportingType, PDI_PF_CODE_GENERATE.ExportingType, PDI_PF_CODE_GENERATE.ChangingType, PDI_PF_CODE_GENERATE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DEFB6E9B2B46AD979FD";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ENTITY;
      [DataMember]
      public string IV_NAMESPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_GENERATED_CODE;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
