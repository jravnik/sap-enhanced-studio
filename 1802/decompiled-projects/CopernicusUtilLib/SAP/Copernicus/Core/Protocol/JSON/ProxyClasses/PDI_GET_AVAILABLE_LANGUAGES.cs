﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_AVAILABLE_LANGUAGES
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_GET_AVAILABLE_LANGUAGES : AbstractRemoteFunction<PDI_GET_AVAILABLE_LANGUAGES.ImportingType, PDI_GET_AVAILABLE_LANGUAGES.ExportingType, PDI_GET_AVAILABLE_LANGUAGES.ChangingType, PDI_GET_AVAILABLE_LANGUAGES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01138A1EE0AFEA2765D152DC26";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public ZLANG_STRUC[] E_SUPP_LANG;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string[] ET_CHECK_INFO;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
