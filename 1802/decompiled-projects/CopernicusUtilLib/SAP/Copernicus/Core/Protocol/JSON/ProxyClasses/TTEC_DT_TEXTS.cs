﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.TTEC_DT_TEXTS
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class TTEC_DT_TEXTS
  {
    [DataMember]
    public string CLIENT;
    [DataMember]
    public string LANGUAGE;
    [DataMember]
    public string TREEID;
    [DataMember]
    public string TREETYPE;
    [DataMember]
    public string VERSION;
    [DataMember]
    public string TEXTID;
    [DataMember]
    public string TEXT;
  }
}
