﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_HASH_VALUE_GET
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_HASH_VALUE_GET : AbstractRemoteFunction<PDI_HASH_VALUE_GET.ImportingType, PDI_HASH_VALUE_GET.ExportingType, PDI_HASH_VALUE_GET.ChangingType, PDI_HASH_VALUE_GET.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DEFB1AAB03055DB188E";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_TEXT;
      [DataMember]
      public int IV_TEXT_LEN;
      [DataMember]
      public int IV_HASH_LEN;
      [DataMember]
      public string IV_FLG_UPPER_CASE;
      [DataMember]
      public string IV_FLG_NO_SPECIALS;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_HASH;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
