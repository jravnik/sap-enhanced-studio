﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_AE_QUERY_BADI
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_AE_QUERY_BADI : AbstractRemoteFunction<PDI_AE_QUERY_BADI.ImportingType, PDI_AE_QUERY_BADI.ExportingType, PDI_AE_QUERY_BADI.ChangingType, PDI_AE_QUERY_BADI.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0231F61EE192DF97F320CB16A6";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_RESULT_SET;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
