﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.__EXT__S_PDI_REF_UI_CUSTOM_FIELD
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class __EXT__S_PDI_REF_UI_CUSTOM_FIELD
  {
    [DataMember]
    public string SELECTED;
    [DataMember]
    public string BO_NAME;
    [DataMember]
    public string BO_NAMESPACE;
    [DataMember]
    public string BO_NODE_NAME;
    [DataMember]
    public string BO_NODE_NAMESPACE;
    [DataMember]
    public string FIELD_NAME;
    [DataMember]
    public string FIELD_TYPE;
    [DataMember]
    public string FIELD_LENGTH;
    [DataMember]
    public string FIELD_DECIMALS;
    [DataMember]
    public string FIELD_TYPE_DESCRIPTION;
    [DataMember]
    public string FIELD_LABEL;
    [DataMember]
    public string ERROR_OCCURRED;
    [DataMember]
    public string MESSAGE_TEXT;
  }
}
