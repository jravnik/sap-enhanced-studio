﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_NAVIGATE_BY_ASSOCIATION
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_NAVIGATE_BY_ASSOCIATION : AbstractRemoteFunction<PDI_NAVIGATE_BY_ASSOCIATION.ImportingType, PDI_NAVIGATE_BY_ASSOCIATION.ExportingType, PDI_NAVIGATE_BY_ASSOCIATION.ChangingType, PDI_NAVIGATE_BY_ASSOCIATION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E03836E1ED2B799F760AA8DD1D7";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BONAME;
      [DataMember]
      public string IV_BONODENAME;
      [DataMember]
      public string IV_ASSOCIATION_NAME;
      [DataMember]
      public string[] IT_NODE_IDS;
      [DataMember]
      public char IV_INTERN;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_OUT_DATA;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
