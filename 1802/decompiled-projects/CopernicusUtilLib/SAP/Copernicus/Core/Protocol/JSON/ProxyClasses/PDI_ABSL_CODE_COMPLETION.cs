﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_CODE_COMPLETION
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_CODE_COMPLETION : AbstractRemoteFunction<PDI_ABSL_CODE_COMPLETION.ImportingType, PDI_ABSL_CODE_COMPLETION.ExportingType, PDI_ABSL_CODE_COMPLETION.ChangingType, PDI_ABSL_CODE_COMPLETION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01602A1EE189CB84EC68B95D9F";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_MDRS_INACTIVE;
      [DataMember]
      public string IV_MDRS_NAMESPACE;
      [DataMember]
      public string IV_XREP_FILE_PATH;
      [DataMember]
      public string IV_ABSL_SOURCE;
      [DataMember]
      public PDI_S_ABSL_CODECOMPL_PARAMS IS_ABSL_CODECOMPL_PARAM;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_S_ABSL_CODECOMPL_RESULT[] ET_RESULT;
      [DataMember]
      public PDI_S_ABSL_METH_TIP_DATA ES_METHOD_TIP;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
