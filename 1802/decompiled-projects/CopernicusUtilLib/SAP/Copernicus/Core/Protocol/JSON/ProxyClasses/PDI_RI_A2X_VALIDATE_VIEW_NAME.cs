﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_A2X_VALIDATE_VIEW_NAME
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_A2X_VALIDATE_VIEW_NAME : AbstractRemoteFunction<PDI_RI_A2X_VALIDATE_VIEW_NAME.ImportingType, PDI_RI_A2X_VALIDATE_VIEW_NAME.ExportingType, PDI_RI_A2X_VALIDATE_VIEW_NAME.ChangingType, PDI_RI_A2X_VALIDATE_VIEW_NAME.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194F015C523EC9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BO_NAME;
      [DataMember]
      public string IV_BOV_NAME;
      [DataMember]
      public string IV_NAME_SPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public ViewNameDetails[] ET_BOV_NAME;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
