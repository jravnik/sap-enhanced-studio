﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_DS_GET_BO_NAV_INFOS
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_DS_GET_BO_NAV_INFOS : AbstractRemoteFunction<PDI_DS_GET_BO_NAV_INFOS.ImportingType, PDI_DS_GET_BO_NAV_INFOS.ExportingType, PDI_DS_GET_BO_NAV_INFOS.ChangingType, PDI_DS_GET_BO_NAV_INFOS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115011DDFAEE8D186A8898648";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BO_ID;
      [DataMember]
      public string IV_NODE_ID;
      [DataMember]
      public string IV_BO_NS_ID;
      [DataMember]
      public string IV_DT_MODE;
      [DataMember]
      public string IV_DT_SOLUTION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_DS_GET_BO_NAV_INFOS.NAV_INFO[] ET_NAV_INFOS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }

    [DataContract]
    public class NAV_INFO
    {
      [DataMember]
      public PDI_DS_GET_BO_NAV_INFOS.OPERATION OPERATION;
      [DataMember]
      public PDI_DS_GET_BO_NAV_INFOS.NAV_ATTRIBUTE NAV_ATTRIBUTE;
      [DataMember]
      public PDI_DS_GET_BO_NAV_INFOS.PORT_TYPE PORT_TYPE;
      [DataMember]
      public string TARGET_COMP_ID;
      [DataMember]
      public string INPORT_NAME;
    }

    [DataContract]
    public class OPERATION
    {
      [DataMember]
      public string OP_ID;
      [DataMember]
      public string OP_DNAME;
    }

    [DataContract]
    public class NAV_ATTRIBUTE
    {
      [DataMember]
      public string ATTR_ID;
      [DataMember]
      public string ATTR_DNAME;
    }

    [DataContract]
    public class PORT_TYPE
    {
      [DataMember]
      public string PORT_TYPE_PKG_ID;
      [DataMember]
      public string PORT_TYPE_ID;
    }
  }
}
