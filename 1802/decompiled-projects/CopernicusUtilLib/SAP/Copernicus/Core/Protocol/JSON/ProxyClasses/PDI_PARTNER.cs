﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PARTNER
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PARTNER
  {
    [DataMember]
    public string MANDT;
    [DataMember]
    public string PARTNER;
    [DataMember]
    public string PARTNER_ID;
    [DataMember]
    public string APPLICATION_COMP;
    [DataMember]
    public string PARTNERDOMAIN;
    [DataMember]
    public string PARTNER_CLASSISC;
    [DataMember]
    public string PARTNER_RESP_ORG;
    [DataMember]
    public string PSM_RELEVANT_IND;
    [DataMember]
    public string IS_MULTI_CUSTOMER_ENABLED;
  }
}
