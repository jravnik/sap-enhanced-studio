﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_BCO_VALUE_HELP
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_BCO_VALUE_HELP : AbstractRemoteFunction<PDI_BC_BCO_VALUE_HELP.ImportingType, PDI_BC_BCO_VALUE_HELP.ExportingType, PDI_BC_BCO_VALUE_HELP.ChangingType, PDI_BC_BCO_VALUE_HELP.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0122C41ED0988BBE46096AD709";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_NODE_ID;
      [DataMember]
      public string IV_FIELD_NAME;
      [DataMember]
      public string IV_NAMESPACE;
      [DataMember]
      public string IV_BCO_PROXYNAME;
      [DataMember]
      public BCT_S_FIELD_VALUE_RANGE[] IT_VALUE_FILTER;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_S_BC_VALUE_DESCRIPTION[] ET_VALUE_DESCRIPTION;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
