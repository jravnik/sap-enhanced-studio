﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_CRL_S_PARAM_TYPE_HELP
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_CRL_S_PARAM_TYPE_HELP
  {
    [DataMember]
    public PDI_CRL_PARAM_DATA_TYPE DATA_TYPE;
    [DataMember]
    public PDI_CRL_PARAM_NODE_TYPE NODE_TYPE;
    [DataMember]
    public string TOOLTIP_NAME;
    [DataMember]
    public string IS_DATA_TYPE;
    [DataMember]
    public string IS_NODE_TYPE;
    [DataMember]
    public string IS_DEPRECATED;
  }
}
