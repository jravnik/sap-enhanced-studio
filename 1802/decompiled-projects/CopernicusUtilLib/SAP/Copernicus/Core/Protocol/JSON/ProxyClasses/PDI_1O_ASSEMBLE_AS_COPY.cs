﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_ASSEMBLE_AS_COPY
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_1O_ASSEMBLE_AS_COPY : AbstractRemoteFunction<PDI_1O_ASSEMBLE_AS_COPY.ImportingType, PDI_1O_ASSEMBLE_AS_COPY.ExportingType, PDI_1O_ASSEMBLE_AS_COPY.ChangingType, PDI_1O_ASSEMBLE_AS_COPY.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19CD5103F63539ACD";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SESSION_ID;
      [DataMember]
      public string IV_SOLUTION_NAME;
      [DataMember]
      public string IV_COPY_SOLUTION_DESCRIPTION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SOLUTION_ASSEMBLE;
      [DataMember]
      public string EV_COPY_SOLUTION_NAME;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
