﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_BCO_INFORMATION
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_BCO_INFORMATION : AbstractRemoteFunction<PDI_BC_BCO_INFORMATION.ImportingType, PDI_BC_BCO_INFORMATION.ExportingType, PDI_BC_BCO_INFORMATION.ChangingType, PDI_BC_BCO_INFORMATION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01151F1ED082A3200298BACB41";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PROXY_NAME;
      [DataMember]
      public string IV_NAMESPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_S_BCO_SCHEMA_NODE[] ET_BCO_INFO;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_BCO_DESCRIPTION;
      [DataMember]
      public string EV_FT_VIEW_EXIST;
      [DataMember]
      public string EV_BCO_PSM_STATUS;
      [DataMember]
      public string EV_BCO_PSM_READ_ONLY;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_S_APPLICATION_F4[] ET_APPLICATION_F4;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
