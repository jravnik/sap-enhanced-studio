﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_S_DBG_STACK_LEVEL
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_S_DBG_STACK_LEVEL
  {
    [DataMember]
    public int STACK_POSITION;
    [DataMember]
    public string XREP_FILE_PATH;
    [DataMember]
    public PDI_ABSL_S_SPAN SPAN;
  }
}
