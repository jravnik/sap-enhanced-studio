﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_DBG
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_DBG : AbstractRemoteFunction<PDI_ABSL_DBG.ImportingType, PDI_ABSL_DBG.ExportingType, PDI_ABSL_DBG.ChangingType, PDI_ABSL_DBG.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E028DA21ED1B09FB03B20D80CC7";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember(Order = 1)]
      public int IV_OPERATION;
      [DataMember(Order = 2)]
      public string IV_MDRS_NAMESPACE;
      [DataMember(Order = 3)]
      public string IV_BREAKPOINT_USER;
      [DataMember(Order = 4)]
      public string IV_BREAKPOINT_USER_ALIAS;
      [DataMember(Order = 5)]
      public PDI_ABSL_S_BREAKPOINT[] IT_BREAKPOINT;
      [DataMember(Order = 6)]
      public PDI_ABSL_S_DEBUGGEE[] IT_WAITING_DEBUGGEE;
      [DataMember(Order = 7)]
      public int IV_STACK_POSITION;
      [DataMember(Order = 8)]
      public string IV_EXPRESSION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public int EV_STATUS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_ABSL_S_DEBUGGEE[] ET_WAITING_DEBUGGEE;
      [DataMember]
      public PDI_ABSL_S_DEBUGGEE ES_ATTACHED_DEBUGGEE;
      [DataMember]
      public string EV_XREP_FILE_PATH;
      [DataMember]
      public string EV_SOURCE_HASH;
      [DataMember]
      public string EV_SESSION_ENDED;
      [DataMember]
      public PDI_ABSL_S_SPAN ES_BREAKPOINT_SPAN;
      [DataMember]
      public PDI_ABSL_S_VARIABLE[] ET_VARIABLE;
      [DataMember]
      public PDI_ABSL_S_BREAKPOINT[] ET_BREAKPOINT;
      [DataMember]
      public PDI_ABSL_S_DBG_INIT_INFO ES_INIT_INFO;
      [DataMember]
      public PDI_ABSL_S_DBG_STACK_LEVEL[] ET_CALLSTACK;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
