﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.XRepHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class XRepHandler : JSONHandler
  {
    public bool binaryMode;
    private static XRepHandler instance;

    public static XRepHandler Instance
    {
      get
      {
        if (XRepHandler.instance == null)
          XRepHandler.instance = new XRepHandler();
        return XRepHandler.instance;
      }
      protected set
      {
        XRepHandler.instance = value;
      }
    }

    private void ReportError(SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE[] retMsg)
    {
      string str = "XRepository Error";
      if (retMsg != null && retMsg.Length > 0)
      {
        str += ":\n";
        foreach (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE pdiRiSMessage in retMsg)
          str = str + pdiRiSMessage.TEXT + "\n";
      }
      Trace.TraceError(str);
      int num = (int) CopernicusMessageBox.Show(str, "XRepository Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      throw new ProtocolException(ProtocolException.ErrorArea.SERVER, str);
    }

    protected bool CheckAndRetrieveResponseCode(SAPFunctionModule proxy)
    {
      bool flag = false;
      if (proxy == null)
      {
        string message = "JSON communication failure. Client proxy is null.";
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message).showPopup();
        return flag;
      }
      object obj = proxy.GetType().GetProperty("Exporting").GetValue((object) proxy, (object[]) null);
      if (obj == null)
      {
        string message = "JSON communication failure. Severe server side error. Exporting parameter is null.";
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.SERVER, message).showPopup();
        return flag;
      }
      FieldInfo field = obj.GetType().GetField("EV_SUCCESS");
      if (field == (FieldInfo) null)
      {
        Trace.TraceInformation("The called XRep RFC (" + proxy.getName() + ") has no EV_SUCCESS exporting parameter defined.");
        flag = false;
      }
      else
      {
        string str = field.GetValue(obj) as string;
        if (str == null)
        {
          string message = "JSON communication failure. Severe server side error. Exporting parameter 'EV_SUCCESS' is defined, but not provided by the backened.";
          Trace.TraceError(message);
          new ProtocolException(ProtocolException.ErrorArea.SERVER, message).showPopup();
        }
        else if (str == "X")
          flag = true;
        else if (str == "")
        {
          flag = false;
        }
        else
        {
          string message = "JSON communication failure. Severe server side error. Exporting parameter value of 'EV_SUCCESS' is not Correctly set: " + str;
          Trace.TraceError(message);
          new ProtocolException(ProtocolException.ErrorArea.SERVER, message).showPopup();
        }
      }
      return flag;
    }

    public bool DeleteContent(string contentType, string xrepPath)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_CONTENT_DELETE pdiRiContentDelete = new PDI_RI_CONTENT_DELETE();
      pdiRiContentDelete.Importing = new PDI_RI_CONTENT_DELETE.ImportingType();
      pdiRiContentDelete.Importing.IV_CONTENT_TYPE = contentType;
      pdiRiContentDelete.Importing.IV_XREP_PATH = xrepPath;
      pdiRiContentDelete.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiContentDelete, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
      if (pdiRiContentDelete.Exporting == null)
        return false;
      ErrorSink.Instance.ClearTasksFor(xrepPath, Origin.GenericSolutionAction);
      ErrorSink.Instance.AddMessages(pdiRiContentDelete.Exporting.ET_MSG_LIST, Resource.MessagePrefixDelete, Origin.GenericSolutionAction);
      if (pdiRiContentDelete.Exporting.EV_SUCCESS == "")
      {
        if (pdiRiContentDelete.Exporting.ET_MESSAGES != null && pdiRiContentDelete.Exporting.ET_MESSAGES.Length > 0)
          this.reportServerSideProtocolException((SAPFunctionModule) pdiRiContentDelete, false, false, false);
        return false;
      }
      ErrorSink.Instance.ClearTasksFor(xrepPath, Origin.All);
      return true;
    }

    public bool ActivateContent(string contentType, string xrepPath)
    {
      return this.ActivateContent(contentType, xrepPath, (string[]) null);
    }

    public bool ActivateContent(string contentType, string[] xrepPath)
    {
      return this.ActivateContent(contentType, (string) null, xrepPath);
    }

    public bool ActivateContent(string contentType, string xrepPath, string[] xrepPaths)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_CONTENT_ACTIVATE riContentActivate = new PDI_RI_CONTENT_ACTIVATE();
      riContentActivate.Importing = new PDI_RI_CONTENT_ACTIVATE.ImportingType();
      riContentActivate.Importing.IV_CONTENT_TYPE = contentType;
      riContentActivate.Importing.IV_XREP_PATH = xrepPath;
      riContentActivate.Importing.IT_XREP_PATH = xrepPaths;
      riContentActivate.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) riContentActivate, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
      if (riContentActivate.Exporting == null)
        return false;
      if (xrepPath != null)
        ErrorSink.Instance.ClearTasksFor(xrepPath, Origin.GenericSolutionAction);
      if (xrepPaths != null)
      {
        foreach (string xrepPath1 in xrepPaths)
        {
          ErrorSink.Instance.ClearTasksFor(xrepPath1, Origin.GenericSolutionAction);
          ErrorSink.Instance.ClearTasksFor(xrepPath1, Origin.BOCompilerLevel3);
        }
      }
      ErrorSink.Instance.AddMessages(riContentActivate.Exporting.ET_MSG_LIST, Resource.MessagePrefixActivate, Origin.GenericSolutionAction);
      if (riContentActivate.Exporting.EV_SUCCESS == "")
      {
        if (riContentActivate.Exporting.ET_MESSAGES != null && riContentActivate.Exporting.ET_MESSAGES.Length > 0)
          this.reportServerSideProtocolException((SAPFunctionModule) riContentActivate, false, false, false);
        return false;
      }
      if (contentType == "NOTIFICATION_RULE" || contentType == "PSD")
        this.reportServerSideProtocolException((SAPFunctionModule) riContentActivate, false, false, true);
      return true;
    }

    public bool CleanContent(string contentType, string xrepPath)
    {
      return this.CleanContent(contentType, xrepPath, (string[]) null);
    }

    public bool CleanContent(string contentType, string[] xrepPath)
    {
      return this.CleanContent(contentType, (string) null, xrepPath);
    }

    public bool CleanContent(string contentType, string xrepPath, string[] xrepPaths)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_CONTENT_CLEANUP riContentCleanup = new PDI_RI_CONTENT_CLEANUP();
      riContentCleanup.Importing = new PDI_RI_CONTENT_CLEANUP.ImportingType();
      riContentCleanup.Importing.IV_CONTENT_TYPE = contentType;
      riContentCleanup.Importing.IV_XREP_PATH = xrepPath;
      riContentCleanup.Importing.IT_XREP_PATH = xrepPaths;
      riContentCleanup.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) riContentCleanup, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
      if (riContentCleanup.Exporting == null)
        return false;
      if (xrepPath != null)
        ErrorSink.Instance.ClearTasksFor(xrepPath, Origin.GenericSolutionAction);
      if (xrepPaths != null)
      {
        foreach (string xrepPath1 in xrepPaths)
          ErrorSink.Instance.ClearTasksFor(xrepPath1, Origin.GenericSolutionAction);
      }
      ErrorSink.Instance.AddMessages(riContentCleanup.Exporting.ET_MSG_LIST, Resource.MessagePrefixClean, Origin.GenericSolutionAction);
      if (!(riContentCleanup.Exporting.EV_SUCCESS == ""))
        return true;
      if (riContentCleanup.Exporting.ET_MESSAGES != null && riContentCleanup.Exporting.ET_MESSAGES.Length > 0)
        this.reportServerSideProtocolException((SAPFunctionModule) riContentCleanup, false, false, false);
      return false;
    }

    public bool CheckContent(string contentType, string xrepPath, out IDictionary<string, List<string>> usageErrorList)
    {
      return this.CheckContent(contentType, xrepPath, out usageErrorList, (string[]) null);
    }

    public bool CheckContent(string contentType, out IDictionary<string, List<string>> usageErrorList, string[] xrepPath)
    {
      return this.CheckContent(contentType, (string) null, out usageErrorList, xrepPath);
    }

    public bool CheckContent(string contentType, string xrepPath, out IDictionary<string, List<string>> usageErrorList, string[] xrepPaths)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      IDictionary<string, List<string>> dictionary = (IDictionary<string, List<string>>) new Dictionary<string, List<string>>();
      PDI_RI_CONTENT_CHECK pdiRiContentCheck = new PDI_RI_CONTENT_CHECK();
      pdiRiContentCheck.Importing = new PDI_RI_CONTENT_CHECK.ImportingType();
      pdiRiContentCheck.Importing.IV_CONTENT_TYPE = contentType;
      pdiRiContentCheck.Importing.IV_XREP_PATH = xrepPath;
      pdiRiContentCheck.Importing.IT_XREP_PATH = xrepPaths;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiContentCheck, false, false, false);
        if (xrepPath != null)
          ErrorSink.Instance.ClearTasksFor(xrepPath, Origin.GenericSolutionAction);
        if (xrepPaths != null)
        {
          foreach (string xrepPath1 in xrepPaths)
            ErrorSink.Instance.ClearTasksFor(xrepPath1, Origin.GenericSolutionAction);
        }
        ErrorSink.Instance.AddMessages(pdiRiContentCheck.Exporting.ET_MSG_LIST, Resource.MessagePrefixCheck, Origin.GenericSolutionAction);
        List<string> stringList1 = new List<string>();
        string empty = string.Empty;
        foreach (SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MESSAGE pdiRiSMessage in pdiRiContentCheck.Exporting.ET_MESSAGES)
        {
          if (pdiRiSMessage.SEVERITY == "W")
          {
            stringList1.Add(pdiRiSMessage.TEXT);
            if (((IEnumerable<string>) pdiRiSMessage.TEXT.Split(' ')).Count<string>() > 2)
              empty = pdiRiSMessage.TEXT.Split(' ')[2];
          }
        }
        if (stringList1.Count > 0)
          dictionary.Add(empty, stringList1);
        List<string> stringList2 = new List<string>();
        foreach (PDI_S_USG_INCONSISTENCY usgInconsistency in pdiRiContentCheck.Exporting.ET_INCONSISTENCIES)
        {
          if (usgInconsistency.MESSAGES != null)
          {
            foreach (SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MESSAGE pdiRiSMessage in usgInconsistency.MESSAGES)
              stringList2.Add(pdiRiSMessage.TEXT);
          }
          if (usgInconsistency.STATUS != "C")
            dictionary.Add(usgInconsistency.ENTITY_PATH, stringList2);
        }
        usageErrorList = dictionary;
        if (!(pdiRiContentCheck.Exporting.EV_SUCCESS == ""))
          return true;
        if (pdiRiContentCheck.Exporting.ET_MESSAGES != null && pdiRiContentCheck.Exporting.ET_MESSAGES.Length > 0)
          this.reportServerSideProtocolException((SAPFunctionModule) pdiRiContentCheck, false, false, false);
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        usageErrorList = dictionary;
        return false;
      }
    }

    public bool CheckContent(string contentType, string xrepPath)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_CONTENT_CHECK pdiRiContentCheck = new PDI_RI_CONTENT_CHECK();
      pdiRiContentCheck.Importing = new PDI_RI_CONTENT_CHECK.ImportingType();
      pdiRiContentCheck.Importing.IV_CONTENT_TYPE = contentType;
      pdiRiContentCheck.Importing.IV_XREP_PATH = xrepPath;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiContentCheck, false, false, false);
        ErrorSink.Instance.ClearTasksFor(xrepPath, Origin.GenericSolutionAction);
        ErrorSink.Instance.AddMessages(pdiRiContentCheck.Exporting.ET_MSG_LIST, Resource.MessagePrefixCheck, Origin.GenericSolutionAction);
        if (!(pdiRiContentCheck.Exporting.EV_SUCCESS == ""))
          return true;
        if (pdiRiContentCheck.Exporting.ET_MESSAGES != null && pdiRiContentCheck.Exporting.ET_MESSAGES.Length > 0)
          this.reportServerSideProtocolException((SAPFunctionModule) pdiRiContentCheck, false, false, false);
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public void ListContent(string fullPath, out List<string> pathList)
    {
      this.ListContent(fullPath, out pathList, (Dictionary<string, string>) null, false, false);
    }

    public void ListContentwithAttributes(string folderPath, bool lastShippedVersion, out IDictionary<string, IDictionary<string, string>> attributesofAllFiles, out List<string> pathList, out List<string> subDirectories)
    {
      attributesofAllFiles = (IDictionary<string, IDictionary<string, string>>) new Dictionary<string, IDictionary<string, string>>();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      pathList = new List<string>();
      subDirectories = new List<string>();
      PDI_RI_XREP_LIST pdiRiXrepList = new PDI_RI_XREP_LIST();
      pdiRiXrepList.Importing = new PDI_RI_XREP_LIST.ImportingType();
      pdiRiXrepList.Importing.IV_PATH = folderPath;
      pdiRiXrepList.Importing.IV_VIRTUAL_VIEW = "X";
      pdiRiXrepList.Importing.IV_RECURSIVELY = "X";
      pdiRiXrepList.Importing.IV_WITH_ATTRIBUTE = "X";
      if (lastShippedVersion)
        pdiRiXrepList.Importing.IV_LAST_SHIPPED_VERSION = "X";
      else
        pdiRiXrepList.Importing.IV_LAST_SHIPPED_VERSION = " ";
      OSLS_XREP_ATTR_FILTER[] oslsXrepAttrFilterArray = new OSLS_XREP_ATTR_FILTER[0];
      pdiRiXrepList.Importing.IT_FILTER = oslsXrepAttrFilterArray;
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepList, false, false, false);
      if (!this.CheckAndRetrieveResponseCode((SAPFunctionModule) pdiRiXrepList))
        this.ReportError(pdiRiXrepList.Exporting != null ? pdiRiXrepList.Exporting.ET_MESSAGES : (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE[]) null);
      foreach (OSLS_XREP_CONTENT_INFO oslsXrepContentInfo in pdiRiXrepList.Exporting.ET_CONT_INFO)
      {
        if (!"X".Equals(oslsXrepContentInfo.IS_DIRECTORY, StringComparison.OrdinalIgnoreCase))
          pathList.Add(oslsXrepContentInfo.FULL_PATH);
        else
          subDirectories.Add(oslsXrepContentInfo.FULL_PATH);
        SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR[] fileAttrs = oslsXrepContentInfo.FILE_ATTRS;
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        foreach (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR attr in fileAttrs)
          this.AddAttribute((IDictionary<string, string>) dictionary, attr);
        if (dictionary.Count > 0)
        {
          if (attributesofAllFiles.ContainsKey(oslsXrepContentInfo.FULL_PATH))
            attributesofAllFiles[oslsXrepContentInfo.FULL_PATH] = (IDictionary<string, string>) dictionary;
          else
            attributesofAllFiles.Add(oslsXrepContentInfo.FULL_PATH, (IDictionary<string, string>) dictionary);
        }
      }
    }

    public void ListContentwithAttributes(string folderPath, bool lastShippedVersion, out IDictionary<string, IDictionary<string, string>> attributesofAllFiles, out List<string> pathList, OSLS_XREP_ATTR_FILTER[] filters = null)
    {
      attributesofAllFiles = (IDictionary<string, IDictionary<string, string>>) new Dictionary<string, IDictionary<string, string>>();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      pathList = new List<string>();
      PDI_RI_XREP_LIST pdiRiXrepList = new PDI_RI_XREP_LIST();
      pdiRiXrepList.Importing = new PDI_RI_XREP_LIST.ImportingType();
      pdiRiXrepList.Importing.IV_PATH = folderPath;
      pdiRiXrepList.Importing.IV_VIRTUAL_VIEW = "X";
      pdiRiXrepList.Importing.IV_RECURSIVELY = "X";
      pdiRiXrepList.Importing.IV_WITH_ATTRIBUTE = "X";
      if (lastShippedVersion)
        pdiRiXrepList.Importing.IV_LAST_SHIPPED_VERSION = "X";
      else
        pdiRiXrepList.Importing.IV_LAST_SHIPPED_VERSION = " ";
      OSLS_XREP_ATTR_FILTER[] oslsXrepAttrFilterArray = filters != null ? filters : new OSLS_XREP_ATTR_FILTER[0];
      pdiRiXrepList.Importing.IT_FILTER = oslsXrepAttrFilterArray;
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepList, false, false, false);
      if (!this.CheckAndRetrieveResponseCode((SAPFunctionModule) pdiRiXrepList))
        this.ReportError(pdiRiXrepList.Exporting != null ? pdiRiXrepList.Exporting.ET_MESSAGES : (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE[]) null);
      foreach (OSLS_XREP_CONTENT_INFO oslsXrepContentInfo in pdiRiXrepList.Exporting.ET_CONT_INFO)
      {
        if (!"X".Equals(oslsXrepContentInfo.IS_DIRECTORY, StringComparison.OrdinalIgnoreCase))
          pathList.Add(oslsXrepContentInfo.FULL_PATH);
        SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR[] fileAttrs = oslsXrepContentInfo.FILE_ATTRS;
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        foreach (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR attr in fileAttrs)
          this.AddAttribute((IDictionary<string, string>) dictionary, attr);
        if (dictionary.Count > 0)
          attributesofAllFiles.Add(oslsXrepContentInfo.FULL_PATH, (IDictionary<string, string>) dictionary);
      }
    }

    private void AddAttribute(IDictionary<string, string> dict, SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR attr)
    {
      if (!dict.ContainsKey(attr.NAME))
      {
        dict.Add(attr.NAME, attr.VALUE);
      }
      else
      {
        dict.Remove(attr.NAME);
        dict.Add(attr.NAME, attr.VALUE);
      }
    }

    public void ListContent(string fullPath, out List<string> pathList, Dictionary<string, string> attribs, bool recursive, bool suppressErrors = false)
    {
      Trace.TraceInformation("Trying to read directory file list for: " + fullPath);
      pathList = new List<string>();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_LIST pdiRiXrepList = new PDI_RI_XREP_LIST();
      pdiRiXrepList.Importing = new PDI_RI_XREP_LIST.ImportingType();
      pdiRiXrepList.Importing.IV_PATH = fullPath;
      pdiRiXrepList.Importing.IV_VIRTUAL_VIEW = "X";
      pdiRiXrepList.Importing.IV_RECURSIVELY = recursive ? "X" : "";
      pdiRiXrepList.Importing.IV_WITH_ATTRIBUTE = "";
      OSLS_XREP_ATTR_FILTER[] oslsXrepAttrFilterArray = new OSLS_XREP_ATTR_FILTER[attribs == null ? 0 : attribs.Count];
      if (attribs != null)
      {
        pdiRiXrepList.Importing.IV_WITH_ATTRIBUTE = "X";
        int num = 0;
        foreach (KeyValuePair<string, string> keyValuePair in attribs.AsEnumerable<KeyValuePair<string, string>>())
          oslsXrepAttrFilterArray[num++] = new OSLS_XREP_ATTR_FILTER()
          {
            ATTRIBUTE_NAME = keyValuePair.Key,
            SIGN = "I",
            OPTION = "EQ",
            LOW = keyValuePair.Value
          };
      }
      pdiRiXrepList.Importing.IT_FILTER = oslsXrepAttrFilterArray;
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepList, false, false, false);
      if (!this.CheckAndRetrieveResponseCode((SAPFunctionModule) pdiRiXrepList))
      {
        if (suppressErrors)
          return;
        this.ReportError(pdiRiXrepList.Exporting != null ? pdiRiXrepList.Exporting.ET_MESSAGES : (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE[]) null);
      }
      else
      {
        foreach (OSLS_XREP_CONTENT_INFO oslsXrepContentInfo in pdiRiXrepList.Exporting.ET_CONT_INFO)
        {
          if (!"X".Equals(oslsXrepContentInfo.IS_DIRECTORY, StringComparison.OrdinalIgnoreCase))
            pathList.Add(oslsXrepContentInfo.FULL_PATH);
        }
      }
    }

    public IDictionary<string, string> Activate(string fullPath, bool retainLockPostActivate = false, string solution = null)
    {
      Trace.TraceInformation("Trying to activate file: " + fullPath);
      IDictionary<string, string> dict = (IDictionary<string, string>) new Dictionary<string, string>();
      this.getOpenWorkList(solution);
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_ACTIVATE pdiRiXrepActivate = new PDI_RI_XREP_ACTIVATE();
      pdiRiXrepActivate.Importing = new PDI_RI_XREP_ACTIVATE.ImportingType();
      pdiRiXrepActivate.Importing.IT_PATH = new string[1]
      {
        fullPath
      };
      pdiRiXrepActivate.Importing.IV_LANGU = "";
      pdiRiXrepActivate.Importing.IV_SKIP_ACTIVE_FILES = "X";
      pdiRiXrepActivate.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      if (!retainLockPostActivate)
        pdiRiXrepActivate.Importing.IV_FORCE_UNLOCK = "X";
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepActivate, false, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) pdiRiXrepActivate, false, false, false);
      PDI_RI_S_XREP_FILE_ATTR[] etAttr = pdiRiXrepActivate.Exporting.ET_ATTR;
      if (etAttr == null || ((IEnumerable<PDI_RI_S_XREP_FILE_ATTR>) etAttr).Count<PDI_RI_S_XREP_FILE_ATTR>() == 0)
        return dict;
      SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR[] attributes = etAttr[0].ATTRIBUTES;
      if (attributes == null)
        return dict;
      foreach (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR attr in attributes)
        this.AddAttribute(dict, attr);
      return dict;
    }

    public List<string> getOpenWorkList(string solution = null)
    {
      List<string> stringList = new List<string>();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_GET_WORKLIST riXrepGetWorklist = new PDI_RI_XREP_GET_WORKLIST();
      riXrepGetWorklist.Importing = new PDI_RI_XREP_GET_WORKLIST.ImportingType();
      if (solution != null)
        riXrepGetWorklist.Importing.IV_SOLUTION_NAME = solution;
      jsonClient.callFunctionModule((SAPFunctionModule) riXrepGetWorklist, false, false, false);
      if (!this.CheckAndRetrieveResponseCode((SAPFunctionModule) riXrepGetWorklist))
      {
        this.ReportError(riXrepGetWorklist.Exporting != null ? riXrepGetWorklist.Exporting.ET_MESSAGES : (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE[]) null);
      }
      else
      {
        foreach (OSLS_XREP_WORKLIST_ITEM xrepWorklistItem in riXrepGetWorklist.Exporting.ET_WORKLIST)
          stringList.Add(xrepWorklistItem.FILE_PATH);
      }
      return stringList;
    }

    public void Lock(string fullPath, bool isSplitActivate = false)
    {
      Trace.TraceInformation("Locking file: " + fullPath);
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_LOCK pdiRiXrepLock = new PDI_RI_XREP_LOCK();
      pdiRiXrepLock.Importing = new PDI_RI_XREP_LOCK.ImportingType();
      pdiRiXrepLock.Importing.IV_PATH = fullPath;
      pdiRiXrepLock.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      if (isSplitActivate)
        pdiRiXrepLock.Importing.IV_SPLIT_ASM = "X";
      else
        pdiRiXrepLock.Importing.IV_SPLIT_ASM = " ";
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepLock, false, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) pdiRiXrepLock, false, false, false);
    }

    public void Save(string fullPath, string content, IDictionary<string, string> attribs)
    {
      Trace.TraceInformation("Trying to save file: " + fullPath);
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_SAVE pdiRiXrepSave = new PDI_RI_XREP_SAVE();
      pdiRiXrepSave.Importing = new PDI_RI_XREP_SAVE.ImportingType();
      pdiRiXrepSave.Importing.IV_PATH = fullPath;
      pdiRiXrepSave.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      if (content != null)
        content = Convert.ToBase64String(Encoding.UTF8.GetBytes(content));
      pdiRiXrepSave.Importing.IV_CONTENT = content;
      SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR[] oslsXrepFileAttrArray = new SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR[attribs == null ? 0 : attribs.Count];
      if (attribs != null)
      {
        int num = 0;
        foreach (KeyValuePair<string, string> keyValuePair in attribs.AsEnumerable<KeyValuePair<string, string>>())
          oslsXrepFileAttrArray[num++] = new SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR()
          {
            NAME = keyValuePair.Key,
            VALUE = keyValuePair.Value,
            SEQUENCE_NUMBER = "0",
            SPRAS = ""
          };
      }
      pdiRiXrepSave.Importing.IT_ATTR = oslsXrepFileAttrArray;
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepSave, false, false, false);
      if (this.CheckAndRetrieveResponseCode((SAPFunctionModule) pdiRiXrepSave))
        return;
      this.ReportError(pdiRiXrepSave.Exporting != null ? pdiRiXrepSave.Exporting.ET_MESSAGES : (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE[]) null);
    }

    public virtual bool CleanUpFolder(string xrepPath)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_DELETE pdiRiXrepDelete = new PDI_RI_XREP_DELETE();
      pdiRiXrepDelete.Importing = new PDI_RI_XREP_DELETE.ImportingType();
      pdiRiXrepDelete.Importing.IV_PATH = xrepPath;
      pdiRiXrepDelete.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      pdiRiXrepDelete.Importing.IV_CLEANUP = "X";
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepDelete, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
      if (pdiRiXrepDelete.Exporting != null)
      {
        if (!(pdiRiXrepDelete.Exporting.EV_SUCCESS == "") || pdiRiXrepDelete.Exporting.ET_MESSAGES == null || pdiRiXrepDelete.Exporting.ET_MESSAGES.Length < 0)
          return true;
        this.reportServerSideProtocolException((SAPFunctionModule) pdiRiXrepDelete, false, false, false);
      }
      return false;
    }

    public bool Delete(string fullPath)
    {
      Trace.TraceInformation("Trying to delete file: " + fullPath);
      return this.DeleteContent(ContentTypeMapping.fromExtn(fullPath.Substring(fullPath.LastIndexOf(".") + 1)).ToString(), fullPath);
    }

    public bool Read(string fullPath, out string content, out IDictionary<string, string> attribs)
    {
      return this.Read(fullPath, false, out content, out attribs);
    }

    public bool Read(string fullPath, bool lastShippedVersion, out string content, out IDictionary<string, string> attribs)
    {
      content = (string) null;
      attribs = (IDictionary<string, string>) new Dictionary<string, string>();
      Trace.TraceInformation("Trying to read file: " + fullPath);
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_READ pdiRiXrepRead = new PDI_RI_XREP_READ();
      pdiRiXrepRead.Importing = new PDI_RI_XREP_READ.ImportingType();
      pdiRiXrepRead.Importing.IV_PATH = fullPath;
      pdiRiXrepRead.Importing.IV_VIRTUAL_VIEW = "X";
      pdiRiXrepRead.Importing.IV_WITH_CONTENT = "X";
      if (lastShippedVersion)
        pdiRiXrepRead.Importing.IV_LAST_SHIPPED_VERSION = "X";
      else
        pdiRiXrepRead.Importing.IV_LAST_SHIPPED_VERSION = " ";
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepRead, false, false, false);
      bool flag = this.CheckAndRetrieveResponseCode((SAPFunctionModule) pdiRiXrepRead);
      if (flag)
      {
        content = pdiRiXrepRead.Exporting.EV_CONTENT;
        foreach (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR attr in pdiRiXrepRead.Exporting.ET_ATTR)
          this.AddAttribute(attribs, attr);
        if (content != null)
        {
          byte[] bytes = Convert.FromBase64String(content);
          content = Encoding.UTF8.GetString(bytes);
        }
      }
      return flag;
    }

    public List<string> GetListOfLocksForLoggedInUser(string[] folderPathsForLockCheck)
    {
      List<string> stringList = new List<string>();
      string upper = SAP.Copernicus.Core.Protocol.Connection.getInstance().getLoggedInUser().ToUpper();
      string xrepSessionId = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_LIST_LOCKS pdiRiXrepListLocks = new PDI_RI_XREP_LIST_LOCKS();
      pdiRiXrepListLocks.Importing = new PDI_RI_XREP_LIST_LOCKS.ImportingType();
      pdiRiXrepListLocks.Importing.IT_PATH = folderPathsForLockCheck;
      pdiRiXrepListLocks.Importing.IV_USER = upper;
      pdiRiXrepListLocks.Importing.IV_SESSION_ID = xrepSessionId;
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepListLocks, false, false, false);
      if (!string.IsNullOrEmpty(pdiRiXrepListLocks.Exporting.EV_SUCCESS))
      {
        foreach (PDI_RI_S_XREP_LOCK pdiRiSXrepLock in pdiRiXrepListLocks.Exporting.ET_LOCK)
        {
          stringList.Add(pdiRiSXrepLock.FILEPATH);
          if (pdiRiSXrepLock.EDIT_BY.ToUpperInvariant() == upper && pdiRiSXrepLock.SID == xrepSessionId)
            stringList.Add(pdiRiSXrepLock.FILEPATH);
        }
        return stringList;
      }
      SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE[] etMessages = pdiRiXrepListLocks.Exporting.ET_MESSAGES;
      string str = "XRepository Error";
      if (etMessages != null && etMessages.Length > 0)
      {
        str += ":\n";
        foreach (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE pdiRiSMessage in etMessages)
          str = str + pdiRiSMessage.TEXT + "\n";
      }
      Trace.TraceError(str);
      int num = (int) CopernicusMessageBox.Show(str, "XRepository Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      throw new ProtocolException(ProtocolException.ErrorArea.SERVER, str);
    }

    public List<string> GetListOfLocksForLoggedInUser(string folderPathForLockCheck)
    {
      List<string> stringList = new List<string>();
      string upper = SAP.Copernicus.Core.Protocol.Connection.getInstance().getLoggedInUser().ToUpper();
      string xrepSessionId = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_LIST_LOCKS pdiRiXrepListLocks = new PDI_RI_XREP_LIST_LOCKS();
      pdiRiXrepListLocks.Importing = new PDI_RI_XREP_LIST_LOCKS.ImportingType();
      pdiRiXrepListLocks.Importing.IV_PATH = folderPathForLockCheck;
      pdiRiXrepListLocks.Importing.IV_USER = upper;
      pdiRiXrepListLocks.Importing.IV_SESSION_ID = xrepSessionId;
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepListLocks, false, false, false);
      if (!string.IsNullOrEmpty(pdiRiXrepListLocks.Exporting.EV_SUCCESS))
      {
        foreach (PDI_RI_S_XREP_LOCK pdiRiSXrepLock in pdiRiXrepListLocks.Exporting.ET_LOCK)
        {
          stringList.Add(pdiRiSXrepLock.FILEPATH);
          if (pdiRiSXrepLock.EDIT_BY.ToUpperInvariant() == upper && pdiRiSXrepLock.SID == xrepSessionId)
            stringList.Add(pdiRiSXrepLock.FILEPATH);
        }
        return stringList;
      }
      SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE[] etMessages = pdiRiXrepListLocks.Exporting.ET_MESSAGES;
      string str = "XRepository Error";
      if (etMessages != null && etMessages.Length > 0)
      {
        str += ":\n";
        foreach (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE pdiRiSMessage in etMessages)
          str = str + pdiRiSMessage.TEXT + "\n";
      }
      Trace.TraceError(str);
      int num = (int) CopernicusMessageBox.Show(str, "XRepository Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      throw new ProtocolException(ProtocolException.ErrorArea.SERVER, str);
    }

    public PDI_RI_S_XREP_LOCK[] GetListOfLocks(string[] folderPathsForLockCheck)
    {
      if (folderPathsForLockCheck.Length <= 0)
        return (PDI_RI_S_XREP_LOCK[]) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_LIST_LOCKS pdiRiXrepListLocks = new PDI_RI_XREP_LIST_LOCKS();
      pdiRiXrepListLocks.Importing = new PDI_RI_XREP_LIST_LOCKS.ImportingType();
      pdiRiXrepListLocks.Importing.IV_USER = (string) null;
      pdiRiXrepListLocks.Importing.IV_SESSION_ID = (string) null;
      pdiRiXrepListLocks.Importing.IT_PATH = new string[folderPathsForLockCheck.Length];
      for (int index = 0; index < folderPathsForLockCheck.Length; ++index)
        pdiRiXrepListLocks.Importing.IT_PATH[index] = folderPathsForLockCheck[index];
      string xrepSessionId = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      string upper = SAP.Copernicus.Core.Protocol.Connection.getInstance().getLoggedInUser().ToUpper();
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiXrepListLocks, false, false, false);
      if (!string.IsNullOrEmpty(pdiRiXrepListLocks.Exporting.EV_SUCCESS))
      {
        PDI_RI_S_XREP_LOCK[] etLock = pdiRiXrepListLocks.Exporting.ET_LOCK;
        for (int index = 0; index < etLock.Length; ++index)
        {
          PDI_RI_S_XREP_LOCK pdiRiSXrepLock = etLock[index];
          string upperInvariant = pdiRiSXrepLock.EDIT_BY.ToUpperInvariant();
          if (upperInvariant == upper && pdiRiSXrepLock.SID != xrepSessionId)
            pdiRiSXrepLock.EDIT_BY = upperInvariant + " " + pdiRiSXrepLock.SID;
        }
        return etLock;
      }
      SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE[] etMessages = pdiRiXrepListLocks.Exporting.ET_MESSAGES;
      string str = "XRepository Error";
      if (etMessages != null && etMessages.Length > 0)
      {
        str += ":\n";
        foreach (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.PDI_RI_S_MESSAGE pdiRiSMessage in etMessages)
          str = str + pdiRiSMessage.TEXT + "\n";
      }
      Trace.TraceError(str);
      int num = (int) CopernicusMessageBox.Show(str, "XRepository Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      throw new ProtocolException(ProtocolException.ErrorArea.SERVER, str);
    }

    public IDictionary<string, List<string>> GetUsageInConsistency(string pXrepPath, string component = "")
    {
      IDictionary<string, List<string>> dictionary = (IDictionary<string, List<string>>) new Dictionary<string, List<string>>();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_USAGES_CHECK pdiRiUsagesCheck = new PDI_RI_USAGES_CHECK();
      pdiRiUsagesCheck.Importing = new PDI_RI_USAGES_CHECK.ImportingType();
      if (component == string.Empty)
        pdiRiUsagesCheck.Importing.IV_XREP_PATH = pXrepPath;
      else
        pdiRiUsagesCheck.Importing.IV_COMPONENT = component;
      jsonClient.callFunctionModule((SAPFunctionModule) pdiRiUsagesCheck, false, false, false);
      if (!string.IsNullOrEmpty(pdiRiUsagesCheck.Exporting.EV_SUCCESS))
      {
        foreach (PDI_S_USG_INCONSISTENCY usgInconsistency in pdiRiUsagesCheck.Exporting.ET_INCONSISTENCIES)
        {
          List<string> stringList = new List<string>();
          if (usgInconsistency.MESSAGES != null)
          {
            foreach (SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MESSAGE pdiRiSMessage in usgInconsistency.MESSAGES)
              stringList.Add(pdiRiSMessage.TEXT);
          }
          if (usgInconsistency.STATUS != "C")
            dictionary.Add(usgInconsistency.ENTITY_PATH, stringList);
        }
        return dictionary;
      }
      SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MESSAGE[] etMessages = pdiRiUsagesCheck.Exporting.ET_MESSAGES;
      string str = "Check Error (Usage Dependency)";
      if (etMessages != null && etMessages.Length > 0)
      {
        str += ":\n";
        foreach (SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MESSAGE pdiRiSMessage in etMessages)
          str = str + pdiRiSMessage.TEXT + "\n";
      }
      Trace.TraceError(str);
      int num = (int) CopernicusMessageBox.Show(str, "Check Error (Usage Dependency)", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      throw new ProtocolException(ProtocolException.ErrorArea.SERVER, str);
    }

    public bool UpdateUsageIndex(List<PDI_S_USG_DATA> lt_usg_data, string SolutionName, bool forceUpdate, out SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MESSAGE[] messages)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_USAGE_UPDATE pdiRiUsageUpdate = new PDI_RI_USAGE_UPDATE();
      pdiRiUsageUpdate.Importing = new PDI_RI_USAGE_UPDATE.ImportingType();
      pdiRiUsageUpdate.Importing.IT_FILE_ATTR = lt_usg_data;
      pdiRiUsageUpdate.Importing.IV_SOLUTION_NAME = SolutionName;
      if (forceUpdate)
        pdiRiUsageUpdate.Importing.IV_FORCE_UPLOAD = "X";
      messages = new SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MESSAGE[0];
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiRiUsageUpdate, false, false, false);
        messages = pdiRiUsageUpdate.Exporting.ET_MESSAGES;
        if (pdiRiUsageUpdate.Exporting.EV_SUCCESS == "X")
          return true;
      }
      catch (ProtocolException ex)
      {
      }
      return false;
    }

    public bool CheckUsageUpdateStatus(string SolutionName, out bool isUpdated)
    {
      isUpdated = false;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_USAGE_UPDATE_CHECK usageUpdateCheck = new PDI_RI_USAGE_UPDATE_CHECK();
      usageUpdateCheck.Importing = new PDI_RI_USAGE_UPDATE_CHECK.ImportingType();
      usageUpdateCheck.Importing.IV_SOLUTION_NAME = SolutionName;
      jsonClient.callFunctionModule((SAPFunctionModule) usageUpdateCheck, false, false, false);
      if (!(usageUpdateCheck.Exporting.EV_SUCCESS == "X"))
        return false;
      if (usageUpdateCheck.Exporting.EV_ALREADY_UPDATED == "X")
        isUpdated = true;
      return true;
    }

    public void CheckHTML5(List<string> listUIComponentFiles)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      FM_XREP_UXCOMP_JSCRIPT_CHECK uxcompJscriptCheck = new FM_XREP_UXCOMP_JSCRIPT_CHECK();
      uxcompJscriptCheck.Importing = new FM_XREP_UXCOMP_JSCRIPT_CHECK.ImportingType();
      uxcompJscriptCheck.Importing.IT_XREP_FILE_PATH = listUIComponentFiles.ToArray();
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) uxcompJscriptCheck, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) uxcompJscriptCheck, true, false, false);
        if (uxcompJscriptCheck.Exporting == null)
          return;
        SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MESSAGE[] etMessages = uxcompJscriptCheck.Exporting.ET_MESSAGES;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
    }

    public PDI_S_APPS_CONTENT_STATUS[] ContentGetStatus(string xrepPath, string[] xrepPaths, string solution)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_CONTENT_GET_STATUS contentGetStatus = new PDI_RI_CONTENT_GET_STATUS();
      contentGetStatus.Importing = new PDI_RI_CONTENT_GET_STATUS.ImportingType();
      contentGetStatus.Importing.IV_XREP_PATH = xrepPath;
      contentGetStatus.Importing.IT_XREP_PATH = xrepPaths;
      contentGetStatus.Importing.IV_SOLUTION_PREFIX = solution;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) contentGetStatus, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return (PDI_S_APPS_CONTENT_STATUS[]) null;
      }
      if (contentGetStatus.Exporting == null)
        return (PDI_S_APPS_CONTENT_STATUS[]) null;
      if (!(contentGetStatus.Exporting.EV_SUCCESS == ""))
        return contentGetStatus.Exporting.ET_CONTENT_STATUS;
      if (contentGetStatus.Exporting.ET_MESSAGES != null && contentGetStatus.Exporting.ET_MESSAGES.Length > 0)
        this.reportServerSideProtocolException((SAPFunctionModule) contentGetStatus, false, false, false);
      return (PDI_S_APPS_CONTENT_STATUS[]) null;
    }

    public void UpdateContentStatus(string[] xrepPaths, string newStatus = "U")
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_CONTENT_UPDATE_STATUS contentUpdateStatus = new PDI_RI_CONTENT_UPDATE_STATUS();
      contentUpdateStatus.Importing = new PDI_RI_CONTENT_UPDATE_STATUS.ImportingType();
      contentUpdateStatus.Importing.IT_XREP_PATH = xrepPaths;
      contentUpdateStatus.Importing.IV_UPDATE_TYPE = newStatus;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) contentUpdateStatus, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return;
      }
      this.reportServerSideProtocolException((SAPFunctionModule) contentUpdateStatus, false, false, false);
    }

    public virtual IDictionary<string, string> CreateOrUpdate(string fullPath, string content, bool doActivate, IDictionary<string, string> attribs, bool retainLockAfterActivate = false, string shortId = null)
    {
      if (doActivate)
        Trace.TraceInformation("Trying to save and activate file: " + fullPath);
      else
        Trace.TraceInformation("Trying to save file: " + fullPath);
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_RI_XREP_CREATE_OR_UPDATE xrepCreateOrUpdate = new PDI_RI_XREP_CREATE_OR_UPDATE();
      xrepCreateOrUpdate.Importing = new PDI_RI_XREP_CREATE_OR_UPDATE.ImportingType();
      xrepCreateOrUpdate.Importing.IV_PATH = fullPath;
      xrepCreateOrUpdate.Importing.IV_OVERWRITE = "X";
      xrepCreateOrUpdate.Importing.IV_ACTIVATE = doActivate ? "X" : "";
      xrepCreateOrUpdate.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      xrepCreateOrUpdate.Importing.IV_SHORT_NAME = shortId;
      if (!retainLockAfterActivate)
        xrepCreateOrUpdate.Importing.IV_FORCE_UNLOCK = "X";
      if (content != null && !this.binaryMode)
        content = Convert.ToBase64String(Encoding.UTF8.GetBytes(content));
      xrepCreateOrUpdate.Importing.IV_CONTENT = content;
      SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR[] oslsXrepFileAttrArray = new SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR[attribs == null ? 0 : attribs.Count];
      if (attribs != null)
      {
        int num = 0;
        foreach (KeyValuePair<string, string> keyValuePair in attribs.AsEnumerable<KeyValuePair<string, string>>())
          oslsXrepFileAttrArray[num++] = new SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR()
          {
            NAME = keyValuePair.Key,
            VALUE = keyValuePair.Value,
            SEQUENCE_NUMBER = "0",
            SPRAS = ""
          };
      }
      xrepCreateOrUpdate.Importing.IT_ATTR = oslsXrepFileAttrArray;
      jsonClient.callFunctionModule((SAPFunctionModule) xrepCreateOrUpdate, false, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) xrepCreateOrUpdate, false, false, false);
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      foreach (SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_FILE_ATTR attr in xrepCreateOrUpdate.Exporting.ET_ATTR)
        this.AddAttribute((IDictionary<string, string>) dictionary, attr);
      return (IDictionary<string, string>) dictionary;
    }

    public IDictionary<string, string> CreateOrUpdate(string fullPath, byte[] content, bool doActivate, IDictionary<string, string> attribs, bool retainLockAfterActivate = false)
    {
      this.binaryMode = true;
      string base64String = Convert.ToBase64String(content);
      IDictionary<string, string> dictionary = (IDictionary<string, string>) new Dictionary<string, string>();
      IDictionary<string, string> orUpdate = this.CreateOrUpdate(fullPath, base64String, doActivate, attribs, retainLockAfterActivate, (string) null);
      this.binaryMode = false;
      return orUpdate;
    }
  }
}
