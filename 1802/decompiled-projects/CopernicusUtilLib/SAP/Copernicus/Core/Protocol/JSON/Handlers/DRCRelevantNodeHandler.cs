﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.DRCRelevantNodeHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class DRCRelevantNodeHandler : JSONHandler
  {
    public void getDRCNodes(string stdBONameUpper, string extBoNameSpace, bool isXBOCustomNode, bool p, string[] boNodeNames, string solutionName, string boXrepPath, out List<string> drcNodeName, out string[] ProjectionBO)
    {
      PDI_1O_GET_DRC_BO_NODES pdi1OGetDrcBoNodes = new PDI_1O_GET_DRC_BO_NODES();
      drcNodeName = (List<string>) null;
      ProjectionBO = (string[]) null;
      pdi1OGetDrcBoNodes.Importing = new PDI_1O_GET_DRC_BO_NODES.ImportingType();
      pdi1OGetDrcBoNodes.Importing.IV_XBO_NAME = stdBONameUpper;
      pdi1OGetDrcBoNodes.Importing.IV_XBO_NAMESPACE = extBoNameSpace;
      pdi1OGetDrcBoNodes.Importing.IS_XBO_CUSTOM_NODE = isXBOCustomNode;
      pdi1OGetDrcBoNodes.Importing.IS_XBO = p;
      pdi1OGetDrcBoNodes.Importing.IT_BO_NODE_NAMES = boNodeNames;
      pdi1OGetDrcBoNodes.Importing.IV_SOLUTION = solutionName;
      pdi1OGetDrcBoNodes.Importing.IV_XREP_FILE_PATH = boXrepPath;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdi1OGetDrcBoNodes, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (pdi1OGetDrcBoNodes.Exporting == null)
        return;
      drcNodeName = new List<string>((IEnumerable<string>) pdi1OGetDrcBoNodes.Exporting.ET_BO_NODE_NAMES);
      ProjectionBO = pdi1OGetDrcBoNodes.Exporting.ET_BO_PROJECTIONS;
    }
  }
}
