﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_GET_TAX_CONTENT
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_GET_TAX_CONTENT : AbstractRemoteFunction<PDI_BC_GET_TAX_CONTENT.ImportingType, PDI_BC_GET_TAX_CONTENT.ExportingType, PDI_BC_GET_TAX_CONTENT.ChangingType, PDI_BC_GET_TAX_CONTENT.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E028DAE1EE1BE833682B133C2B8";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_LANGU;
      [DataMember]
      public string IV_COUNTRY;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_BC_TAX_EVENTS[] ET_TAX_EVENTS;
      [DataMember]
      public PDI_BC_TAX_TYPE[] ET_TAX_TYPES;
      [DataMember]
      public PDI_BC_TAX_EXEM_RSN[] ET_TAX_EXEM_RSNS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
