﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_CHECK_APPROVAL_ID
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_CHECK_APPROVAL_ID : AbstractRemoteFunction<PDI_RI_CHECK_APPROVAL_ID.ImportingType, PDI_RI_CHECK_APPROVAL_ID.ExportingType, PDI_RI_CHECK_APPROVAL_ID.ChangingType, PDI_RI_CHECK_APPROVAL_ID.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01EE082FDB2CA6FEF48E0";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ID;
      [DataMember]
      public string IV_XREP_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_ID_FOUND;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
