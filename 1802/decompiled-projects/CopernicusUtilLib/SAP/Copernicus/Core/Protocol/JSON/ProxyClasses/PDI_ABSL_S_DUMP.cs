﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_S_DUMP
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_S_DUMP
  {
    [DataMember]
    public string DATUM;
    [DataMember]
    public string UZEIT;
    [DataMember]
    public string USERALIAS;
    [DataMember]
    public string ERRID;
    [DataMember]
    public string ERRTEXT;
    [DataMember]
    public string SOLUTION;
    [DataMember]
    public string PATH;
    [DataMember]
    public int LINE;
    [DataMember]
    public int COL;
    [DataMember]
    public string STATUS;
    [DataMember]
    public string TRANSID;
  }
}
