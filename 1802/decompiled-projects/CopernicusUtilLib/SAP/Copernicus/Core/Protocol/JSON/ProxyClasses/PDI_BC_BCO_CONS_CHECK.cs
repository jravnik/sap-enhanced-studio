﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_BCO_CONS_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_BCO_CONS_CHECK : AbstractRemoteFunction<PDI_BC_BCO_CONS_CHECK.ImportingType, PDI_BC_BCO_CONS_CHECK.ExportingType, PDI_BC_BCO_CONS_CHECK.ChangingType, PDI_BC_BCO_CONS_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01151F1EE09AB60F89AF18C307";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_CONTENT;
      [DataMember]
      public PDI_S_CC_BCNODE_LINE[] IT_SCHEMA_IDS;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public BCE_S_DTE_CHECK_ERROR_OCC[] ET_OCCURRENCES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
