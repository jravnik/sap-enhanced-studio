﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.BCT_S_TEMPLATE_CONTEXT
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class BCT_S_TEMPLATE_CONTEXT
  {
    [DataMember]
    public string ELEMENT_ID;
    [DataMember]
    public string CONTENT_ID;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string BO_NAMESPACE;
    [DataMember]
    public string BO_NAME;
    [DataMember]
    public string NODE;
    [DataMember]
    public string OPERATION;
    [DataMember]
    public string IS_WORKITEM;
    [DataMember]
    public string IS_INSCOPE;
  }
}
