﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXECUTE_QUERY
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_EXECUTE_QUERY : AbstractRemoteFunction<PDI_EXECUTE_QUERY.ImportingType, PDI_EXECUTE_QUERY.ExportingType, PDI_EXECUTE_QUERY.ChangingType, PDI_EXECUTE_QUERY.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E03836E1ED2B3BA79790254C2D3";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BONAME;
      [DataMember]
      public string IV_BONODENAME;
      [DataMember]
      public string IV_QUERYNAME;
      [DataMember]
      public SESF_SELECTION_PARAMETER[] IT_SELECTIONPARAMS;
      [DataMember]
      public int IV_MAXROWS;
      [DataMember]
      public char IV_ONLYCOUNT;
      [DataMember]
      public char IV_INTERN;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_OUT_DATA;
      [DataMember]
      public int EV_COUNT;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
