﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_PROD_FIX_REVERT
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_1O_PROD_FIX_REVERT : AbstractRemoteFunction<PDI_1O_PROD_FIX_REVERT.ImportingType, PDI_1O_PROD_FIX_REVERT.ExportingType, PDI_1O_PROD_FIX_REVERT.ChangingType, PDI_1O_PROD_FIX_REVERT.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0975CB1EE4AEFEE4CF760DD2A9";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PATH;
      [DataMember]
      public string IV_SESSION_ID;
      [DataMember]
      public string IV_MINORVERSION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
