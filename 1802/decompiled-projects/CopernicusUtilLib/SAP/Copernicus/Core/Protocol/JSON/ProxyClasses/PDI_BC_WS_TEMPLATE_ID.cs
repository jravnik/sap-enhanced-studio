﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_WS_TEMPLATE_ID
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_WS_TEMPLATE_ID : AbstractRemoteFunction<PDI_BC_WS_TEMPLATE_ID.ImportingType, PDI_BC_WS_TEMPLATE_ID.ExportingType, PDI_BC_WS_TEMPLATE_ID.ChangingType, PDI_BC_WS_TEMPLATE_ID.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01151C1EE096BEAEE6AC7A5BA6";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_NAMESPACE;
      [DataMember]
      public string IV_TEMPLATE_ID;
      [DataMember]
      public string IV_TEMPLATE_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_WS_ID;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
