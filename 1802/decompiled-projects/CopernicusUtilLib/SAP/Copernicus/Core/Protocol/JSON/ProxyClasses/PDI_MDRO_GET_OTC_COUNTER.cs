﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_MDRO_GET_OTC_COUNTER
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_MDRO_GET_OTC_COUNTER : AbstractRemoteFunction<PDI_MDRO_GET_OTC_COUNTER.ImportingType, PDI_MDRO_GET_OTC_COUNTER.ExportingType, PDI_MDRO_GET_OTC_COUNTER.ChangingType, PDI_MDRO_GET_OTC_COUNTER.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E03836E1EE2A4DB059D5503C966";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_XREP_FILE_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public int EV_OTC_COUNTER;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
