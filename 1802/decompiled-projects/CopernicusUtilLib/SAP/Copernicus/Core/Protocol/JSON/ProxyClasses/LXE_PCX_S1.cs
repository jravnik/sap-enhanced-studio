﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.LXE_PCX_S1
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class LXE_PCX_S1
  {
    [DataMember]
    public string TEXTKEY;
    [DataMember]
    public string S_TEXT;
    [DataMember]
    public string T_TEXT;
    [DataMember]
    public string UNITMLT;
    [DataMember]
    public string UPPCASE;
    [DataMember]
    public string TEXTTYPE;
  }
}
