﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.UIGenerationHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class UIGenerationHandler : JSONHandler
  {
    public string UpdateScoping(string workcenterName, string workcenterViewName, out string returnValue)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_UI_UPDATE_SCOPING pdiUiUpdateScoping = new PDI_UI_UPDATE_SCOPING();
        pdiUiUpdateScoping.Importing = new PDI_UI_UPDATE_SCOPING.ImportingType();
        string[] strArray1 = new string[1]{ workcenterName };
        pdiUiUpdateScoping.Importing.IT_WORKCENTERS = strArray1;
        string[] strArray2 = new string[1]{ workcenterViewName };
        pdiUiUpdateScoping.Importing.IT_WOCVIEWS = strArray2;
        pdiUiUpdateScoping.Importing.IV_TRIGGER_RBAM_GENERATION = true;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiUiUpdateScoping, false, false, false);
        if (pdiUiUpdateScoping.Exporting != null)
        {
          returnValue = pdiUiUpdateScoping.Exporting.EV_RC;
          return returnValue;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      returnValue = "empty";
      return returnValue;
    }
  }
}
