﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PROJECT_ITEM
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PROJECT_ITEM
  {
    [DataMember]
    public string PROJECT_ITEM_KEY;
    [DataMember]
    public string PROJ_ITEM_TYPE;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string LM_CONTENT_TYPE;
    [DataMember]
    public string KEY_USER_REQUIRED;
    [DataMember]
    public string ADD_ITEM_RELEVANT;
    [DataMember]
    public string DEL_ALLOWED_IN_MAINT;
  }
}
