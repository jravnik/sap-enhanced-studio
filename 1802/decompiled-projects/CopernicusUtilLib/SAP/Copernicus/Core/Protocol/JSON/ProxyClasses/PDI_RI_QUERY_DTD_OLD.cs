﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_QUERY_DTD_OLD
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_QUERY_DTD_OLD : AbstractRemoteFunction<PDI_RI_QUERY_DTD_OLD.ImportingType, PDI_RI_QUERY_DTD_OLD.ExportingType, PDI_RI_QUERY_DTD_OLD.ChangingType, PDI_RI_QUERY_DTD_OLD.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E04DC271ED385ADF8AEC2FF8FC2";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public SESF_QUERY_OPTIONS IS_QUERY_OPTION;
      [DataMember]
      public string IV_WITH_PSM_FILTER;
      [DataMember]
      public SESF_SELECTION_PARAMETER[] IT_SELECTION_PARAMETER;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_RESULT_SET;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
