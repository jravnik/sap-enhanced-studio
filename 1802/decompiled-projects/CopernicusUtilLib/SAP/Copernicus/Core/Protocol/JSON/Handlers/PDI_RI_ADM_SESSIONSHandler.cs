﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.PDI_RI_ADM_SESSIONSHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class PDI_RI_ADM_SESSIONSHandler : JSONHandler
  {
    public void ReadUsers(out PDI_RI_S_ADM_SESSION_USER[] ET_USER_IDENTITY, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_RI_ADM_SESSION_READ_USERS sessionReadUsers = new PDI_RI_ADM_SESSION_READ_USERS();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      sessionReadUsers.Importing = new PDI_RI_ADM_SESSION_READ_USERS.ImportingType();
      jsonClient.callFunctionModule((SAPFunctionModule) sessionReadUsers, false, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) sessionReadUsers, false, false, false);
      ET_USER_IDENTITY = sessionReadUsers.Exporting.ET_USER_IDENTITY;
      ET_MESSAGES = sessionReadUsers.Exporting.ET_MESSAGES;
    }

    public void DeleteUserSessions(PDI_RI_S_ADM_SESSION_USER[] IT_USER, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_RI_ADM_SESSION_DELETE admSessionDelete = new PDI_RI_ADM_SESSION_DELETE();
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      admSessionDelete.Importing = new PDI_RI_ADM_SESSION_DELETE.ImportingType();
      admSessionDelete.Importing.IT_USER = IT_USER;
      jsonClient.callFunctionModule((SAPFunctionModule) admSessionDelete, false, false, false);
      this.reportServerSideProtocolException((SAPFunctionModule) admSessionDelete, false, false, false);
      ET_MESSAGES = admSessionDelete.Exporting.ET_MESSAGES;
    }
  }
}
