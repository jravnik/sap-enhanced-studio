﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.PDI_TRC_JSONHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Diagnostics;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class PDI_TRC_JSONHandler : JSONHandler
  {
    public void GetTrace(string IV_USERNAME, out string EV_TRACE_LIST, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS)
    {
      PDI_TRC_QUERY_TRACE_BY_USER queryTraceByUser = new PDI_TRC_QUERY_TRACE_BY_USER();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        queryTraceByUser.Importing = new PDI_TRC_QUERY_TRACE_BY_USER.ImportingType();
        queryTraceByUser.Importing.IV_USER_NAME = IV_USERNAME;
        jsonClient.callFunctionModule((SAPFunctionModule) queryTraceByUser, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) queryTraceByUser, false, false, false);
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      ET_MESSAGES = queryTraceByUser.Exporting.ET_MESSAGES;
      EV_TRACE_LIST = queryTraceByUser.Exporting.EV_TRACE_LIST;
      EV_SUCCESS = queryTraceByUser.Exporting.EV_SUCCESS;
    }

    public void GetIncidentTrace(string IV_SYNCH, out string EV_TRACE_LIST, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_TRC_QUERY_INCIDENT_TRACE queryIncidentTrace = new PDI_TRC_QUERY_INCIDENT_TRACE();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        queryIncidentTrace.Importing = new PDI_TRC_QUERY_INCIDENT_TRACE.ImportingType();
        queryIncidentTrace.Importing.IV_SYNCH = IV_SYNCH;
        jsonClient.callFunctionModule((SAPFunctionModule) queryIncidentTrace, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) queryIncidentTrace, false, false, false);
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      ET_MESSAGES = queryIncidentTrace.Exporting.ET_MESSAGES;
      EV_TRACE_LIST = queryIncidentTrace.Exporting.EV_TRACE_LIST;
      EV_SUCCESS = queryIncidentTrace.Exporting.EV_SUCCESS;
    }

    public void GetAllTraces(string IV_SYNCH, string IV_USER_NAME, out string EV_LOCAL_TRACE_LIST, out string EV_INCIDENT_TRACE_LIST, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_TRC_QUERY_ALL_TRACES trcQueryAllTraces = new PDI_TRC_QUERY_ALL_TRACES();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        trcQueryAllTraces.Importing = new PDI_TRC_QUERY_ALL_TRACES.ImportingType();
        trcQueryAllTraces.Importing.IV_SYNCH = IV_SYNCH;
        trcQueryAllTraces.Importing.IV_USER_NAME = IV_USER_NAME;
        jsonClient.callFunctionModule((SAPFunctionModule) trcQueryAllTraces, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) trcQueryAllTraces, false, false, false);
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Severe error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      ET_MESSAGES = trcQueryAllTraces.Exporting.ET_MESSAGES;
      EV_LOCAL_TRACE_LIST = trcQueryAllTraces.Exporting.EV_LOCAL_TRACE_LIST;
      EV_INCIDENT_TRACE_LIST = trcQueryAllTraces.Exporting.EV_INCIDENT_TRACE_LIST;
      EV_SUCCESS = trcQueryAllTraces.Exporting.EV_SUCCESS;
    }

    public void ActivateTrace(string user, bool checkBusinessUser, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out PDI_RI_S_MESSAGE[] ET_MESSAGES_POPUP, out string EV_SUCCESS, out string EV_ERROR_TYPE)
    {
      PDI_TRC_ACTIVATE_TRACE trcActivateTrace = new PDI_TRC_ACTIVATE_TRACE();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        trcActivateTrace.Importing = new PDI_TRC_ACTIVATE_TRACE.ImportingType();
        trcActivateTrace.Importing.IV_ALIAS = user;
        trcActivateTrace.Importing.IV_USE_ALIAS = "X";
        if (checkBusinessUser)
          trcActivateTrace.Importing.IV_CHECK_BUSINESS_USER = "X";
        else
          trcActivateTrace.Importing.IV_CHECK_BUSINESS_USER = "";
        jsonClient.callFunctionModule((SAPFunctionModule) trcActivateTrace, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.TraceError("Severe error in JSONClient: " + ex.Message);
        PDI_RI_S_MESSAGE pdiRiSMessage = new PDI_RI_S_MESSAGE();
        pdiRiSMessage.TEXT = ex.Message;
        ET_MESSAGES = new PDI_RI_S_MESSAGE[1];
        ET_MESSAGES[0] = pdiRiSMessage;
        EV_SUCCESS = "";
        EV_ERROR_TYPE = "JsonFailed";
        ET_MESSAGES_POPUP = (PDI_RI_S_MESSAGE[]) null;
        return;
      }
      ET_MESSAGES = trcActivateTrace.Exporting.ET_MESSAGES;
      EV_SUCCESS = trcActivateTrace.Exporting.EV_SUCCESS;
      EV_ERROR_TYPE = trcActivateTrace.Exporting.EV_ERROR_TYPE;
      ET_MESSAGES_POPUP = trcActivateTrace.Exporting.ET_MESSAGES_POPUP;
    }

    public void DeactivateTrace(string user, bool checkBusinessUser, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS, out string EV_ERROR_TYPE)
    {
      PDI_TRC_DEACTIVATE_TRACE trcDeactivateTrace = new PDI_TRC_DEACTIVATE_TRACE();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        trcDeactivateTrace.Importing = new PDI_TRC_DEACTIVATE_TRACE.ImportingType();
        trcDeactivateTrace.Importing.IV_ALIAS = user;
        trcDeactivateTrace.Importing.IV_USE_ALIAS = "X";
        if (checkBusinessUser)
          trcDeactivateTrace.Importing.IV_CHECK_BUSINESS_USER = "X";
        else
          trcDeactivateTrace.Importing.IV_CHECK_BUSINESS_USER = "";
        jsonClient.callFunctionModule((SAPFunctionModule) trcDeactivateTrace, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.TraceError("Severe error in JSONClient: " + ex.Message);
        PDI_RI_S_MESSAGE pdiRiSMessage = new PDI_RI_S_MESSAGE();
        pdiRiSMessage.TEXT = ex.Message;
        ET_MESSAGES = new PDI_RI_S_MESSAGE[1];
        ET_MESSAGES[0] = pdiRiSMessage;
        EV_SUCCESS = "";
        EV_ERROR_TYPE = "JsonFailed";
        return;
      }
      ET_MESSAGES = trcDeactivateTrace.Exporting.ET_MESSAGES;
      EV_SUCCESS = trcDeactivateTrace.Exporting.EV_SUCCESS;
      EV_ERROR_TYPE = trcDeactivateTrace.Exporting.EV_ERROR_TYPE;
    }

    public void GetTraceContent(string IV_TRACE_RUN_ID, string[] IT_PROVIDER_ALIAS, out PDI_TRC_S_OVERVIEW[] ET_TRACE_CONTENT, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out PDI_RI_S_MESSAGE[] ET_MESSAGES_POPUP, out string EV_ERROR_TYPE)
    {
      PDI_TRC_READ_TRACE pdiTrcReadTrace = new PDI_TRC_READ_TRACE();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiTrcReadTrace.Importing = new PDI_TRC_READ_TRACE.ImportingType();
        pdiTrcReadTrace.Importing.IV_TRACE_RUN_ID = IV_TRACE_RUN_ID;
        pdiTrcReadTrace.Importing.IT_PROVIDER_ALIAS = IT_PROVIDER_ALIAS;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiTrcReadTrace, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.TraceError("Severe error in JSONClient: " + ex.Message);
        PDI_RI_S_MESSAGE pdiRiSMessage = new PDI_RI_S_MESSAGE();
        pdiRiSMessage.TEXT = ex.Message;
        ET_MESSAGES = new PDI_RI_S_MESSAGE[1];
        ET_MESSAGES[0] = pdiRiSMessage;
        ET_TRACE_CONTENT = new PDI_TRC_S_OVERVIEW[1];
        EV_SUCCESS = "";
        EV_ERROR_TYPE = "";
        ET_MESSAGES_POPUP = (PDI_RI_S_MESSAGE[]) null;
        return;
      }
      ET_MESSAGES = pdiTrcReadTrace.Exporting.ET_MESSAGES;
      EV_SUCCESS = pdiTrcReadTrace.Exporting.EV_SUCCESS;
      ET_TRACE_CONTENT = pdiTrcReadTrace.Exporting.ET_TRACE_CONTENT;
      EV_ERROR_TYPE = pdiTrcReadTrace.Exporting.EV_ERROR_TYPE;
      ET_MESSAGES_POPUP = pdiTrcReadTrace.Exporting.ET_MESSAGES_POPUP;
    }

    public void DeleteTrace(string IV_TRACE_RUN_ID, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS)
    {
      PDI_TRC_DELETE_TRACE pdiTrcDeleteTrace = new PDI_TRC_DELETE_TRACE();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiTrcDeleteTrace.Importing = new PDI_TRC_DELETE_TRACE.ImportingType();
        pdiTrcDeleteTrace.Importing.IV_TRACE_RUN_ID = IV_TRACE_RUN_ID;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiTrcDeleteTrace, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.TraceError("Severe error in JSONClient: " + ex.Message);
        PDI_RI_S_MESSAGE pdiRiSMessage = new PDI_RI_S_MESSAGE();
        pdiRiSMessage.TEXT = ex.Message;
        ET_MESSAGES = new PDI_RI_S_MESSAGE[1];
        ET_MESSAGES[0] = pdiRiSMessage;
        EV_SUCCESS = "";
        return;
      }
      ET_MESSAGES = pdiTrcDeleteTrace.Exporting.ET_MESSAGES;
      EV_SUCCESS = pdiTrcDeleteTrace.Exporting.EV_SUCCESS;
    }
  }
}
