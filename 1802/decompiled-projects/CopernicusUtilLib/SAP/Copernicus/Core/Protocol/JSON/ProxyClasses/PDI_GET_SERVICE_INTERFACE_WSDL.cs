﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_SERVICE_INTERFACE_WSDL
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_GET_SERVICE_INTERFACE_WSDL : AbstractRemoteFunction<PDI_GET_SERVICE_INTERFACE_WSDL.ImportingType, PDI_GET_SERVICE_INTERFACE_WSDL.ExportingType, PDI_GET_SERVICE_INTERFACE_WSDL.ChangingType, PDI_GET_SERVICE_INTERFACE_WSDL.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0520391ED389883E64A3D3844C";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SERVICE_INTERFACE_PRX_NAME;
      [DataMember]
      public string IV_SERVICE_INTERFACE_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_WSDL_URI;
      [DataMember]
      public string EV_WSDL_CONTENT;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
