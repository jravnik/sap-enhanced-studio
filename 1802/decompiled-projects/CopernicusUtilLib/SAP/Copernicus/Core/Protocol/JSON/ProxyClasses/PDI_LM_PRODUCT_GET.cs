﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRODUCT_GET
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_PRODUCT_GET : AbstractRemoteFunction<PDI_LM_PRODUCT_GET.ImportingType, PDI_LM_PRODUCT_GET.ExportingType, PDI_LM_PRODUCT_GET.ChangingType, PDI_LM_PRODUCT_GET.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB7202C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_MODE;
      [DataMember]
      public string IV_SOLUTION_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_S_PRODUCT_VERS_AND_COMPS ES_PRODUCT;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }

    public class SOLUTION_TYPE
    {
      public const string Mashup = "001";
      public const string Integrated = "002";
      public const string Light_Weight = "003";
      public const string Add_On = "004";
      public const string Micro_Vertical = "005";
      public const string Customer_Specific = "006";
      public const string Solution_Template = "007";
    }
  }
}
