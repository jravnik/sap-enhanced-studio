﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_S_EXT_FORM_FIELDS
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_S_EXT_FORM_FIELDS
  {
    [DataMember]
    public string ESR_BO_NAME;
    [DataMember]
    public string ESR_BO_NAMESPACE;
    [DataMember]
    public string ESR_BO_NODE_NAME;
    [DataMember]
    public string ESR_FIELD_NAME;
    [DataMember]
    public string ESR_FIELD_NAMESPACE;
    [DataMember]
    public string UI_TEXT_BO_NODE;
    [DataMember]
    public string UI_TEXT_FIELD_NAME;
    [DataMember]
    public string FIELD_HAS_REFERENCE_BO_NODE;
    [DataMember]
    public string MAX_BO_ESR_NAME;
    [DataMember]
    public string MAX_BO_ESR_NAMESPACE;
  }
}
