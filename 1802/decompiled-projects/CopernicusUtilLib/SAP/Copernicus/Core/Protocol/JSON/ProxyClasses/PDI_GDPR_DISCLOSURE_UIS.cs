﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GDPR_DISCLOSURE_UIS
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_GDPR_DISCLOSURE_UIS : AbstractRemoteFunction<PDI_GDPR_DISCLOSURE_UIS.ImportingType, PDI_GDPR_DISCLOSURE_UIS.ExportingType, PDI_GDPR_DISCLOSURE_UIS.ChangingType, PDI_GDPR_DISCLOSURE_UIS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F51ED79E9DB61D3CB15FBE";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION;
      [DataMember]
      public string IV_PROFILE_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public ET_DISCLOSURE_UIS[] ET_DISCLOSURE_UIS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
