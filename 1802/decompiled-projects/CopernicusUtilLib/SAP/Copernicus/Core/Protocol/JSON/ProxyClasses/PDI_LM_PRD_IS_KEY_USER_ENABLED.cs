﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRD_IS_KEY_USER_ENABLED
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_PRD_IS_KEY_USER_ENABLED : AbstractRemoteFunction<PDI_LM_PRD_IS_KEY_USER_ENABLED.ImportingType, PDI_LM_PRD_IS_KEY_USER_ENABLED.ExportingType, PDI_LM_PRD_IS_KEY_USER_ENABLED.ChangingType, PDI_LM_PRD_IS_KEY_USER_ENABLED.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194EC88B8EF6C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT;
      [DataMember]
      public string IV_USER;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_ENABLED;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
