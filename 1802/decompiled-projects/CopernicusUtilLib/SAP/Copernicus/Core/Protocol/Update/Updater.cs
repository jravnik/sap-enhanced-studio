﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.Updater
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using WindowsInstaller;

namespace SAP.Copernicus.Core.Protocol.Update
{
  public class Updater
  {
    private string appDataPath = UpdateHandler.AppDataPath;
    private string programPath = Application.ExecutablePath;

    public void update()
    {
      Process process = new Process();
      process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
      string msiFile = this.appDataPath + "\\CopernicusIsolatedShell.msi";
      string path = this.appDataPath + "\\update.bat";
      string msiProductCode = Updater.getMsiProductCode(msiFile);
      msiProductCode.Replace("\"", "");
      this.programPath.Replace("\"", "");
      if (Environment.OSVersion.Version.Major >= 6)
        process.StartInfo.Verb = "runas";
      string[] strArray = new string[10]{ "@echo off", "msiexec /x " + msiProductCode + " /passive", "cd \"" + this.appDataPath + "\"", "IF EXIST \"CopernicusIsolatedShell.msi\" (", "     msiexec /i \"CopernicusIsolatedShell.msi\" /passive", "     ECHO Starting program...", "     START \"\" \"" + this.programPath + "\"", "     PING -n 12 127.0.0.1>nul ", ")", "exit" };
      StringBuilder stringBuilder = new StringBuilder();
      for (int index = 0; index < strArray.Length; ++index)
        stringBuilder.AppendLine(strArray[index]);
      File.WriteAllText(path, stringBuilder.ToString());
      process.StartInfo.FileName = path;
      try
      {
        process.Start();
      }
      catch
      {
      }
    }

    private static string getMsiProductCode(string msiFile)
    {
      string str1 = "";
      string str2 = "ProductCode";
      // ISSUE: variable of a compiler-generated type
      Installer instance = Activator.CreateInstance(Type.GetTypeFromProgID("WindowsInstaller.Installer")) as Installer;
      // ISSUE: reference to a compiler-generated method
      // ISSUE: variable of a compiler-generated type
      Database database = instance.OpenDatabase(msiFile, (object) 0);
      string Sql = string.Format("SELECT Value FROM Property WHERE Property='{0}'", (object) str2);
      // ISSUE: reference to a compiler-generated method
      // ISSUE: variable of a compiler-generated type
      WindowsInstaller.View view = database.OpenView(Sql);
      // ISSUE: reference to a compiler-generated method
      view.Execute((Record) null);
      // ISSUE: reference to a compiler-generated method
      // ISSUE: variable of a compiler-generated type
      Record record = view.Fetch();
      if (record != null)
      {
        // ISSUE: reference to a compiler-generated method
        str1 = record.get_StringData(1);
      }
      return str1;
    }
  }
}
