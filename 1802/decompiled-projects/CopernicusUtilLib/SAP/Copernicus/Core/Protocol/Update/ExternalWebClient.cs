﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.ExternalWebClient
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace SAP.Copernicus.Core.Protocol.Update
{
  internal class ExternalWebClient : WebClient
  {
    private X509Certificate cert;

    public ExternalWebClient(X509Certificate cert)
    {
      this.cert = cert;
    }

    protected override WebRequest GetWebRequest(Uri address)
    {
      HttpWebRequest webRequest = (HttpWebRequest) base.GetWebRequest(address);
      webRequest.ClientCertificates.Add(this.cert);
      return (WebRequest) webRequest;
    }
  }
}
