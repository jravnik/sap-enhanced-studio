﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.FormBuildSelection
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.Update
{
  public class FormBuildSelection : Form
  {
    private IContainer components;
    private Button button1;
    private Button buttonInternal;
    private Label label_info;
    private Button buttonCancel;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.button1 = new Button();
      this.buttonInternal = new Button();
      this.label_info = new Label();
      this.buttonCancel = new Button();
      this.SuspendLayout();
      this.button1.DialogResult = DialogResult.No;
      this.button1.Location = new Point(150, 43);
      this.button1.Name = "button1";
      this.button1.Size = new Size(92, 23);
      this.button1.TabIndex = 18;
      this.button1.Text = "External (SMP)";
      this.button1.UseVisualStyleBackColor = true;
      this.buttonInternal.DialogResult = DialogResult.Yes;
      this.buttonInternal.Location = new Point(18, 43);
      this.buttonInternal.Name = "buttonInternal";
      this.buttonInternal.Size = new Size(102, 23);
      this.buttonInternal.TabIndex = 17;
      this.buttonInternal.Text = "Internal (Japro)";
      this.buttonInternal.UseVisualStyleBackColor = true;
      this.label_info.AutoSize = true;
      this.label_info.Location = new Point(15, 10);
      this.label_info.Name = "label_info";
      this.label_info.Size = new Size(266, 13);
      this.label_info.TabIndex = 16;
      this.label_info.Text = "Please choose which build you would like to update to:";
      this.buttonCancel.DialogResult = DialogResult.Cancel;
      this.buttonCancel.Location = new Point(274, 43);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new Size(78, 23);
      this.buttonCancel.TabIndex = 15;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(367, 77);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.buttonInternal);
      this.Controls.Add((Control) this.label_info);
      this.Controls.Add((Control) this.buttonCancel);
      this.Name = nameof (FormBuildSelection);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Update";
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public FormBuildSelection()
    {
      this.InitializeComponent();
    }
  }
}
