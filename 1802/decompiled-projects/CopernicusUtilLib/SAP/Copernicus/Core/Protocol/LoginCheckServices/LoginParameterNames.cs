﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginParameterNames
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public static class LoginParameterNames
  {
    public const string DELETE_SESSION = "delete_session";
    public const string SAP_ALIAS = "sap-alias";
    public const string SAP_USER = "sap-user";
    public const string SAP_PASSWORD = "sap-password";
    public const string SYS_LOGIN_PASSWORD = "sap-system-login-password";
    public const string SYS_LOGIN_PASSWORD_NEW = "sap-system-login-passwordnew";
    public const string SYS_LOGIN_PASSWORD_REPEAT = "sap-system-login-passwordrepeat";
    public const string SYS_LOGIN_INPUT_PROCESSING = "sap-system-login-oninputprocessing";
  }
}
