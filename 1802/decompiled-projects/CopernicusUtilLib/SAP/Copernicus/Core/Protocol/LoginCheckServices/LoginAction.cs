﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginAction
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Text;

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  internal sealed class LoginAction : ILoginAction
  {
    private readonly string _key;
    private readonly Uri _uri;
    private readonly LoginActionType _type;
    private volatile string _toString;

    internal LoginAction(LoginActionType type, string key)
      : this(type, key, (Uri) null)
    {
    }

    internal LoginAction(LoginActionType type, Uri uri)
      : this(type, (string) null, uri)
    {
    }

    private LoginAction(LoginActionType type, string key, Uri uri)
    {
      this._type = type;
      this._key = key;
      this._uri = uri;
    }

    public LoginActionType Type
    {
      get
      {
        return this._type;
      }
    }

    public string Key
    {
      get
      {
        return this._key;
      }
    }

    public override string ToString()
    {
      if (this._toString == null)
      {
        StringBuilder stringBuilder = new StringBuilder(128);
        stringBuilder.Append(this._type.ToString()).Append(", ").Append(this._key);
        if (this._uri == (Uri) null)
          stringBuilder.Append(", ").Append((object) this._uri);
        this._toString = stringBuilder.ToString();
      }
      return this._toString;
    }
  }
}
