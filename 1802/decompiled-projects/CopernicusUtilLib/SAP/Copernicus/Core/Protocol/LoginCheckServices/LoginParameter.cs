﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginParameter
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  internal sealed class LoginParameter : ILoginParameter
  {
    private readonly string _name;
    private string _value;
    private string _toString;

    internal LoginParameter(string name, string value)
    {
      this._name = name;
      this._value = value;
    }

    public string Name
    {
      get
      {
        return this._name;
      }
    }

    public string Value
    {
      get
      {
        return this._value;
      }
      set
      {
        this._value = value;
      }
    }

    public override string ToString()
    {
      if (this._toString == null)
        this._toString = this._name + "=" + this._value;
      return this._toString;
    }
  }
}
