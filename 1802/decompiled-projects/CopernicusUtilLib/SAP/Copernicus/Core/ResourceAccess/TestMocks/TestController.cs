﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.TestController
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks
{
  public static class TestController
  {
    private static string testInputRootPath;

    public static event TestController.EventHandlerOnPathUpdate PathUpdateEventRegistration;

    public static bool TestMode { set; get; }

    public static string TestResultStorage { set; get; }

    public static string TestInputRootPath
    {
      set
      {
        TestController.testInputRootPath = value;
        TestController.PathUpdateEventRegistration(TestController.testInputRootPath);
      }
      get
      {
        return TestController.testInputRootPath;
      }
    }

    public delegate void EventHandlerOnPathUpdate(string newFilePath);
  }
}
