﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.Core.UnitTestConnection
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks.Core
{
  public class UnitTestConnection : Connection
  {
    public UnitTestConnection()
    {
      PropertyAccess.GeneralProps.ProxySupport = true;
      Connection.instance = (Connection) this;
    }
  }
}
