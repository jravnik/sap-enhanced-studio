﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.MockResourceAccess
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.ResourceAccess.TestMocks.Core;

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks
{
  internal class MockResourceAccess : SAP.Copernicus.Core.ResourceAccess.ResourceAccess
  {
    public void Initialize(bool usePSM, bool mockMDRS = true)
    {
      this.Connection = (Connection) new MockConnection(usePSM);
      if (!mockMDRS)
        return;
      this.RepositoryDataCache = (RepositoryDataCache) new MockRepositoryDataCache();
    }
  }
}
