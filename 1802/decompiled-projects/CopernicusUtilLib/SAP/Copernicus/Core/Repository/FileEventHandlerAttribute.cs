﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.FileEventHandlerAttribute
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel.Composition;

namespace SAP.Copernicus.Core.Repository
{
  [MetadataAttribute]
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
  public class FileEventHandlerAttribute : ExportAttribute
  {
    public FileEventHandlerAttribute(FileEventType eventTypeMask, int priority = 100)
      : base(typeof (GenericFileEventHandler))
    {
      this.EventTypeMask = eventTypeMask;
      this.Priority = priority;
    }

    public FileEventType EventTypeMask { get; set; }

    public int Priority { get; set; }
  }
}
