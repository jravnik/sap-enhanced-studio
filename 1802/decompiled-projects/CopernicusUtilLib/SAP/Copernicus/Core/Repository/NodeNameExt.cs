﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeNameExt
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class NodeNameExt
  {
    public string Name { get; set; }

    public string ProxyName { get; set; }

    public string ParentProxyName { get; set; }

    public string ReleasedNode { get; set; }

    public string DisabledNode { get; set; }

    public string BoProxyName { get; set; }

    public string OffEnabled { get; set; }

    public NodeNameExt(string name, string proxyName, string parentProxyName, string boProxyName, string nodeReleased, string nodeDisabled, string offEnabled)
    {
      this.Name = name;
      this.ProxyName = proxyName;
      this.ParentProxyName = parentProxyName;
      this.ReleasedNode = nodeReleased;
      this.DisabledNode = nodeDisabled;
      this.BoProxyName = boProxyName;
      this.OffEnabled = offEnabled;
    }
  }
}
