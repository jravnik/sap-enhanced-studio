﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.DataModel.QueryResultSetParser
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;

namespace SAP.Copernicus.Core.Repository.DataModel
{
  public class QueryResultSetParser
  {
    private RepositoryDataSet repositoryDataSet;
    private RepositoryQueryHandler queryHandler;

    public QueryResultSetParser(RepositoryDataSet repositoryDataSet, RepositoryQueryHandler queryHandler)
    {
      this.repositoryDataSet = repositoryDataSet;
      this.queryHandler = queryHandler;
    }

    public void ParseBOsIntoDataSet(string queryResult, Dictionary<string, LastChangedItem> boLastChangedInfo)
    {
      lock (this.queryHandler.lockObject)
      {
        if (queryResult == null || queryResult.Length == 0)
          return;
        foreach (XElement element1 in XElement.Parse(queryResult).Descendants(XName.Get("RESULT_SET")).Elements<XElement>((XName) "item"))
        {
          string str1 = element1.Element((XName) "NAME").Value;
          string str2 = element1.Element((XName) "NAMESPACE").Value;
          string str3 = element1.Element((XName) "PROXYNAME").Value;
          string str4 = element1.Element((XName) "UUID").Value;
          string s = element1.Element((XName) "LAST_CHANGED_DATE_TIME").Value;
          string str5 = element1.Element((XName) "OBJECT_CATEGORY").Value;
          string str6 = (string) null;
          string str7 = (string) null;
          bool flag = false;
          if (element1.Element((XName) "TECH_CATEGORY") != null)
            str6 = element1.Element((XName) "TECH_CATEGORY").Value;
          if (element1.Element((XName) "LIFE_CYCLE_STAT") != null)
            str7 = element1.Element((XName) "LIFE_CYCLE_STAT").Value;
          if (element1.Element((XName) "DEPRECATED") != null && element1.Element((XName) "DEPRECATED").Value.Equals("X"))
            flag = true;
          string str8 = (string) null;
          string str9 = (string) null;
          string str10 = (string) null;
          if (element1.Element((XName) "DU_NAME") != null && element1.Element((XName) "DU_NAME").Value.Length > 0)
            str8 = element1.Element((XName) "DU_NAME").Value;
          if (element1.Element((XName) "WRITE_ACCESS") != null)
            str9 = element1.Element((XName) "WRITE_ACCESS").Value;
          if (element1.Element((XName) "TRANSITIVE_HASH") != null)
            str10 = element1.Element((XName) "TRANSITIVE_HASH").Value;
          DateTime result;
          if (!DateTime.TryParse(s, out result))
            throw new ProtocolException(ProtocolException.ErrorArea.CLIENT, string.Format("unable to parse timestamp {0} for business object {1} in namespace {2}", (object) s, (object) str1, (object) str2));
          if (str2 != null && str1 != null && (str3 != null && str4 != null) && (str2.Length > 0 && str1.Length > 0 && str3.Length > 0))
          {
            if (str4.Length > 0)
            {
              try
              {
                if (!this.repositoryDataSet.Namespaces.Rows.Contains(new object[1]{ (object) str2 }))
                  this.repositoryDataSet.Namespaces.Rows.Add(new object[1]
                  {
                    (object) str2
                  });
                RepositoryDataSet.BusinessObjectsRow businessObjectsRow = (RepositoryDataSet.BusinessObjectsRow) this.repositoryDataSet.BusinessObjects.Rows.Find(new object[2]{ (object) str2, (object) str1 });
                if (businessObjectsRow != null)
                {
                  businessObjectsRow.BO_UUID = str4;
                  businessObjectsRow.LastChangeAt = result;
                  businessObjectsRow.ObjectCategory = str5;
                  businessObjectsRow.TechCategory = str6;
                  businessObjectsRow.LifecycleStat = str7;
                  businessObjectsRow.DeploymentUnit = str8;
                  businessObjectsRow.WriteAccess = str9;
                  businessObjectsRow.TransitiveHash = str10;
                  businessObjectsRow.Deprecated = flag;
                }
                else
                  this.repositoryDataSet.BusinessObjects.Rows.Add((object) str2, (object) str1, (object) str3, (object) str4, (object) result, (object) str5, (object) str6, (object) str7, (object) str8, (object) str9, (object) str10);
                LastChangedItem lastChangedItem;
                if (boLastChangedInfo.TryGetValue(str3, out lastChangedItem))
                {
                  if (str10 != null && !str10.Equals(lastChangedItem.TransitiveHash) && RepositoryDataCache.ModelChangeEventHandlerRegistry != null)
                    RepositoryDataCache.ModelChangeEventHandlerRegistry(RepositoryDataCache.MetaObjectTypes.BusinessObject, str3, false);
                  lastChangedItem.Handled = true;
                }
                foreach (XElement element2 in element1.Element((XName) "NODE").Elements((XName) "item"))
                {
                  string str11 = element2.Element((XName) "NAME").Value;
                  string str12 = element2.Element((XName) "PROXYNAME").Value;
                  string str13 = (string) null;
                  if (element2.Element((XName) "PARENT_NODE_NAME") != null)
                    str13 = element2.Element((XName) "PARENT_NODE_NAME").Value;
                  string str14 = (string) null;
                  if (element2.Element((XName) "WRITE_ACCESS") != null)
                    str14 = element2.Element((XName) "WRITE_ACCESS").Value.ToUpperInvariant();
                  string str15 = (string) null;
                  if (element2.Element((XName) "CATEGORY") != null)
                    str15 = element2.Element((XName) "CATEGORY").Value.ToUpperInvariant();
                  if (str3 != null && str11 != null && (str12 != null && str3.Length > 0) && str11.Length > 0)
                  {
                    if (str12.Length > 0)
                    {
                      try
                      {
                        if (!this.repositoryDataSet.Nodes.Rows.Contains(new object[2]{ (object) str3, (object) str11 }))
                          this.repositoryDataSet.Nodes.Rows.Add((object) str3, (object) str11, (object) str12, (object) str13, (object) str14, (object) str15);
                      }
                      catch (ConstraintException ex)
                      {
                        Console.WriteLine(ex.Message);
                      }
                    }
                  }
                }
              }
              catch (ConstraintException ex)
              {
                Console.WriteLine(ex.Message);
              }
            }
          }
        }
        this.repositoryDataSet.AcceptChanges();
        if (RepositoryDataCache.ModelChangeEventHandlerRegistry == null)
          return;
        foreach (LastChangedItem lastChangedItem in boLastChangedInfo.Values.Where<LastChangedItem>((Func<LastChangedItem, bool>) (n => !n.Handled)))
          RepositoryDataCache.GetInstance().DeleteBusinessObject((string) null, lastChangedItem.ProxyName);
      }
    }

    public void PutItemsAndFeaturesInDataSet(PDI_PROJECT_ITEM_SOLUTION[] projectItemSolution, PDI_PROJECT_ITEM[] projectItem, PDI_FEATURE[] features)
    {
      lock (this.queryHandler.lockObject)
      {
        this.repositoryDataSet.ProjectItemSolution.Clear();
        this.repositoryDataSet.ProjectItem.Clear();
        this.repositoryDataSet.Feature.Clear();
        foreach (PDI_PROJECT_ITEM_SOLUTION projectItemSolution1 in projectItemSolution)
        {
          if (projectItemSolution1.SAP_SOLUTION_TYPE == null)
            projectItemSolution1.SAP_SOLUTION_TYPE = "";
          if (projectItemSolution1.PDI_SOLUTION_SUBTYPE == null)
            projectItemSolution1.PDI_SOLUTION_SUBTYPE = "";
          this.repositoryDataSet.ProjectItemSolution.Rows.Add((object) projectItemSolution1.PROJECT_ITEM_SOLUTION_KEY, (object) projectItemSolution1.PROJ_ITEM_TYPE, (object) projectItemSolution1.SAP_SOLUTION_TYPE, (object) projectItemSolution1.PDI_SOLUTION_SUBTYPE);
        }
        foreach (PDI_PROJECT_ITEM pdiProjectItem in projectItem)
          this.repositoryDataSet.ProjectItem.Rows.Add((object) pdiProjectItem.PROJECT_ITEM_KEY, (object) pdiProjectItem.PROJ_ITEM_TYPE, (object) pdiProjectItem.DESCRIPTION, (object) pdiProjectItem.LM_CONTENT_TYPE, (object) pdiProjectItem.KEY_USER_REQUIRED, (object) pdiProjectItem.ADD_ITEM_RELEVANT, (object) pdiProjectItem.DEL_ALLOWED_IN_MAINT);
        foreach (PDI_FEATURE feature in features)
        {
          if (feature.SAP_SOLUTION_TYPE == null)
            feature.SAP_SOLUTION_TYPE = "";
          if (feature.PDI_SOLUTION_SUBTYPE == null)
            feature.PDI_SOLUTION_SUBTYPE = "";
          this.repositoryDataSet.Feature.Rows.Add((object) feature.FEATURE_KEY, (object) feature.FEATURE, (object) feature.DESCRIPTION, (object) feature.SAP_SOLUTION_TYPE, (object) feature.PDI_SOLUTION_SUBTYPE);
        }
      }
    }

    public void ParseBAdIIntoDataset(string queryResult, Dictionary<string, LastChangedItem> dtLastChangedInfo)
    {
      lock (this.queryHandler.lockObject)
      {
        if (string.IsNullOrEmpty(queryResult))
          return;
        foreach (XElement element1 in XElement.Parse(queryResult).Descendants(XName.Get("RESULT_SET")).Elements<XElement>((XName) "item"))
        {
          string str1 = element1.Element((XName) "NAMESPACE").Value;
          foreach (XElement element2 in element1.Element((XName) "BADI_DEFS").Elements((XName) "item"))
          {
            string str2 = element2.Element((XName) "PROXY_NAME").Value;
            string str3 = element2.Element((XName) "ES_NAME").Value;
            string str4 = element2.Element((XName) "SPOT_NAME").Value;
            string str5 = element2.Element((XName) "DESCRIPTION").Value;
            string str6 = element2.Element((XName) "RELATED_BO").Value;
            string str7 = element2.Element((XName) "DEPLOYMENT_UNIT").Value;
            bool flag1 = element2.Element((XName) "SINGLE_USE").Value == "X";
            if (!string.IsNullOrEmpty(str1) && !string.IsNullOrEmpty(str2))
            {
              if (!string.IsNullOrEmpty(str3))
              {
                try
                {
                  if (!this.repositoryDataSet.Namespaces.Rows.Contains(new object[1]{ (object) str1 }))
                    this.repositoryDataSet.Namespaces.Rows.Add(new object[1]
                    {
                      (object) str1
                    });
                  RepositoryDataSet.BAdIsRow badIsRow = (RepositoryDataSet.BAdIsRow) this.repositoryDataSet.BAdIs.Rows.Find(new object[1]{ (object) str2 });
                  if (badIsRow != null)
                  {
                    badIsRow.ProxyName = str2;
                    badIsRow.Name = str3;
                    badIsRow.Description = str5;
                    badIsRow.BusinessObject = str6;
                    badIsRow.SpotName = str4;
                    badIsRow.DeploymentUnit = str7;
                    badIsRow.IsSingleUsed = flag1;
                  }
                  else
                    this.repositoryDataSet.BAdIs.Rows.Add((object) str1, (object) str3, (object) str2, (object) str5, (object) str6, (object) str4, (object) str7, (object) flag1);
                }
                catch (ConstraintException ex)
                {
                  Console.WriteLine(ex.Message);
                }
                foreach (XElement element3 in element2.Element((XName) "FILTERS").Elements((XName) "item"))
                {
                  string str8 = element3.Element((XName) "NAME").Value;
                  string str9 = element3.Element((XName) "TYPE").Value;
                  string str10 = element3.Element((XName) "DESCRIPTION").Value;
                  bool flag2 = element3.Element((XName) "MANDATORY").Value == "X";
                  if (!string.IsNullOrEmpty(str8) && !string.IsNullOrEmpty(str9))
                  {
                    RepositoryDataSet.BAdIFiltersRow badIfiltersRow = (RepositoryDataSet.BAdIFiltersRow) this.repositoryDataSet.BAdIFilters.Rows.Find(new object[2]{ (object) str8, (object) str2 });
                    if (badIfiltersRow != null)
                    {
                      badIfiltersRow.Name = str8;
                      badIfiltersRow.BAdIProxyName = str2;
                      badIfiltersRow.Type = str9;
                      badIfiltersRow.Mandatory = flag2;
                    }
                    else
                      this.repositoryDataSet.BAdIFilters.Rows.Add((object) str8, (object) str9, (object) str10, (object) str2, (object) flag2);
                  }
                }
              }
            }
          }
        }
      }
    }

    public void ParseDTsIntoDataSet(string queryResult, Dictionary<string, LastChangedItem> dtLastChangedInfo)
    {
      lock (this.queryHandler.lockObject)
      {
        if (queryResult == null)
          return;
        XElement xelement1 = XElement.Parse(queryResult);
        IEnumerable<XElement> source = xelement1.Descendants(XName.Get("RESULT_SET"));
        IEnumerable<XElement> xelements1 = source.Elements<XElement>((XName) "RELEASE");
        int num = 0;
        foreach (XElement xelement2 in xelements1)
          num = Convert.ToInt32(xelement2.Value);
        if (num >= 30)
        {
          foreach (XElement element1 in source.Descendants<XElement>(XName.Get("DATATYPESTYPE")).Elements<XElement>((XName) "item"))
          {
            IEnumerable<XElement> xelements2 = element1.Descendants(XName.Get("NAMESPACE"));
            string nsName = (string) null;
            foreach (XElement xelement2 in xelements2)
              nsName = xelement2.Value;
            foreach (XContainer descendant in element1.Descendants(XName.Get("DATATYPES")))
            {
              foreach (XElement element2 in descendant.Elements((XName) "item"))
                this.parseDTXelement35(element2, nsName, dtLastChangedInfo);
            }
          }
        }
        else
        {
          foreach (XElement element in xelement1.Descendants(XName.Get("RESULT_SET")).Elements<XElement>((XName) "item"))
            this.parseDTXelement(element, dtLastChangedInfo);
        }
        this.repositoryDataSet.AcceptChanges();
        if (RepositoryDataCache.ModelChangeEventHandlerRegistry == null)
          return;
        foreach (LastChangedItem lastChangedItem in dtLastChangedInfo.Values.Where<LastChangedItem>((Func<LastChangedItem, bool>) (n => !n.Handled)))
          RepositoryDataCache.ModelChangeEventHandlerRegistry(RepositoryDataCache.MetaObjectTypes.DataType, lastChangedItem.ProxyName, true);
      }
    }

    private void parseDTXelement35(XElement item, string nsName, Dictionary<string, LastChangedItem> dtLastChangedInfo)
    {
      string str1 = item.Element((XName) "NAME").Value;
      string str2 = item.Element((XName) "PROXYNAME").Value;
      string s = item.Element((XName) "LAST_CHANGED_DATE_TIME").Value;
      string str3 = (string) null;
      if (item.Element((XName) "BASE_DT_KEY_NAME") != null)
        str3 = item.Element((XName) "BASE_DT_KEY_NAME").Value;
      string str4 = (string) null;
      if (item.Element((XName) "USAGE_CATEGORY") != null)
        str4 = item.Element((XName) "USAGE_CATEGORY").Value;
      bool flag = false;
      if (item.Element((XName) "EXTENSIBLE") != null)
        flag = item.Element((XName) "EXTENSIBLE").Equals((object) "X");
      string str5 = (string) null;
      if (item.Element((XName) "REP_TERM") != null)
        str5 = item.Element((XName) "REP_TERM").Value;
      string str6 = (string) null;
      if (item.Element((XName) "TRANSITIVE_HASH") != null)
        str6 = item.Element((XName) "TRANSITIVE_HASH").Value;
      DateTime result;
      if (!DateTime.TryParse(s, out result))
        throw new ProtocolException(ProtocolException.ErrorArea.CLIENT, string.Format("unable to parse timestamp {0} for data type {1} in namespace {2}", (object) s, (object) str1, (object) nsName));
      if (nsName == null || str1 == null || (str2 == null || nsName.Length <= 0) || str1.Length <= 0)
        return;
      if (str2.Length <= 0)
        return;
      try
      {
        if (!this.repositoryDataSet.Namespaces.Rows.Contains(new object[1]{ (object) nsName }))
          this.repositoryDataSet.Namespaces.Rows.Add(new object[1]
          {
            (object) nsName
          });
        RepositoryDataSet.DataTypesRow dataTypesRow = (RepositoryDataSet.DataTypesRow) this.repositoryDataSet.DataTypes.Rows.Find(new object[2]{ (object) nsName, (object) str1 });
        if (dataTypesRow != null)
        {
          dataTypesRow.DT_UUID = "";
          dataTypesRow.LastChangeAt = result;
          dataTypesRow.BaseDTKeyName = str3;
          dataTypesRow.UsageCategory = str4;
          dataTypesRow.Extensible = flag;
          dataTypesRow.TransitiveHash = str6;
          dataTypesRow.RepresentationTerm = str5;
        }
        else
          this.repositoryDataSet.DataTypes.Rows.Add((object) nsName, (object) str1, (object) str2, (object) "", (object) result, (object) str3, (object) str4, (object) flag, (object) str6, (object) str5);
        LastChangedItem lastChangedItem;
        if (!dtLastChangedInfo.TryGetValue(str2, out lastChangedItem))
          return;
        if (str6 != null && !str6.Equals(lastChangedItem.TransitiveHash) && RepositoryDataCache.ModelChangeEventHandlerRegistry != null)
          RepositoryDataCache.ModelChangeEventHandlerRegistry(RepositoryDataCache.MetaObjectTypes.DataType, str2, false);
        lastChangedItem.Handled = true;
      }
      catch (ConstraintException ex)
      {
        Console.WriteLine(ex.Message);
      }
    }

    private void parseDTXelement(XElement item, Dictionary<string, LastChangedItem> dtLastChangedInfo)
    {
      string str1 = item.Element((XName) "NAME").Value;
      string str2 = item.Element((XName) "NAMESPACE").Value;
      string str3 = item.Element((XName) "PROXYNAME").Value;
      string str4 = item.Element((XName) "UUID").Value;
      string s = item.Element((XName) "LAST_CHANGED_DATE_TIME").Value;
      string str5 = (string) null;
      if (item.Element((XName) "BASE_DT_KEY_NAME") != null)
        str5 = item.Element((XName) "BASE_DT_KEY_NAME").Value;
      string str6 = (string) null;
      if (item.Element((XName) "USAGE_CATEGORY") != null)
        str6 = item.Element((XName) "USAGE_CATEGORY").Value;
      bool flag = false;
      if (item.Element((XName) "EXTENSIBLE") != null)
        flag = item.Element((XName) "EXTENSIBLE").Equals((object) "X");
      string str7 = (string) null;
      if (item.Element((XName) "TRANSITIVE_HASH") != null)
        str7 = item.Element((XName) "TRANSITIVE_HASH").Value;
      DateTime result;
      if (!DateTime.TryParse(s, out result))
        throw new ProtocolException(ProtocolException.ErrorArea.CLIENT, string.Format("unable to parse timestamp {0} for data type {1} in namespace {2}", (object) s, (object) str1, (object) str2));
      if (str2 == null || str1 == null || (str3 == null || str4 == null) || (str2.Length <= 0 || str1.Length <= 0 || str3.Length <= 0))
        return;
      if (str4.Length <= 0)
        return;
      try
      {
        if (!this.repositoryDataSet.Namespaces.Rows.Contains(new object[1]{ (object) str2 }))
          this.repositoryDataSet.Namespaces.Rows.Add(new object[1]
          {
            (object) str2
          });
        RepositoryDataSet.DataTypesRow dataTypesRow = (RepositoryDataSet.DataTypesRow) this.repositoryDataSet.DataTypes.Rows.Find(new object[2]{ (object) str2, (object) str1 });
        if (dataTypesRow != null)
        {
          dataTypesRow.DT_UUID = str4;
          dataTypesRow.LastChangeAt = result;
          dataTypesRow.BaseDTKeyName = str5;
          dataTypesRow.UsageCategory = str6;
          dataTypesRow.Extensible = flag;
          dataTypesRow.TransitiveHash = str7;
        }
        else
          this.repositoryDataSet.DataTypes.Rows.Add((object) str2, (object) str1, (object) str3, (object) str4, (object) result, (object) str5, (object) str6, (object) flag, (object) str7);
        LastChangedItem lastChangedItem;
        if (!dtLastChangedInfo.TryGetValue(str3, out lastChangedItem))
          return;
        if (str7 != null && !str7.Equals(lastChangedItem.TransitiveHash) && RepositoryDataCache.ModelChangeEventHandlerRegistry != null)
          RepositoryDataCache.ModelChangeEventHandlerRegistry(RepositoryDataCache.MetaObjectTypes.DataType, str3, false);
        lastChangedItem.Handled = true;
      }
      catch (ConstraintException ex)
      {
        Console.WriteLine(ex.Message);
      }
    }

    public void ParseLibrariesIntoDataSet(string queryResult, Dictionary<string, LastChangedItem> LibrariesLastChangedInfo)
    {
      lock (this.queryHandler.lockObject)
      {
        if (queryResult == null)
          return;
        foreach (XElement element in XElement.Parse(queryResult).Descendants(XName.Get("RESULT_SET")).Elements<XElement>((XName) "item"))
        {
          string str1 = element.Element((XName) "PROXYNAME").Value;
          string str2 = element.Element((XName) "NAME").Value;
          string import = element.Element((XName) "NAMESPACE").Value;
          string s = element.Element((XName) "LAST_CHANGED_DATE_TIME").Value;
          string str3 = element.Element((XName) "TYPE_CODE").Value;
          string str4 = element.Element((XName) "CALL_TYPE").Value;
          string str5 = (string) null;
          if (element.Element((XName) "TRANSITIVE_HASH") != null)
            str5 = element.Element((XName) "TRANSITIVE_HASH").Value;
          DateTime result;
          if (!DateTime.TryParse(s, out result))
            throw new ProtocolException(ProtocolException.ErrorArea.CLIENT, string.Format("unable to parse timestamp {0} for library {1} in namespace {2}", (object) s, (object) str2, (object) import));
          string str6 = ImportPathUtil.ConvertImportFromESRToInternal(import, "AP/PDI/") + '.'.ToString() + str2.Replace('_', '.');
          string str7 = ImportPathUtil.ConvertImportFromESRToInternal(import);
          if (str7 == "AP.PDI.ABSL")
            str7 = "ABSL";
          RepositoryDataSet.LibrariesDataTable libraries = this.repositoryDataSet.Libraries;
          if (!libraries.Rows.Contains((object) str1))
          {
            libraries.Rows.Add((object) str1, (object) str2, (object) str7, (object) result, (object) str3, (object) str4, (object) str5);
            if (RepositoryDataCache.ModelChangeEventHandlerRegistry != null)
              RepositoryDataCache.ModelChangeEventHandlerRegistry(RepositoryDataCache.MetaObjectTypes.Library, str1, false);
          }
          else
          {
            libraries.Rows.Remove(libraries.Rows.Find(new object[1]
            {
              (object) str1
            }));
            libraries.Rows.Add((object) str1, (object) str2, (object) str7, (object) result, (object) str3, (object) str4, (object) str5);
            LastChangedItem lastChangedItem;
            if (LibrariesLastChangedInfo.TryGetValue(str1, out lastChangedItem) && str5 != null && (!str5.Equals(lastChangedItem.TransitiveHash) && RepositoryDataCache.ModelChangeEventHandlerRegistry != null))
              RepositoryDataCache.ModelChangeEventHandlerRegistry(RepositoryDataCache.MetaObjectTypes.Library, str1, false);
          }
        }
        this.repositoryDataSet.AcceptChanges();
      }
    }

    public void ParseMTsIntoDataSet(string queryResult, Dictionary<string, LastChangedItem> mtLastChangedInfo)
    {
      lock (this.queryHandler.lockObject)
      {
        if (queryResult == null)
          return;
        foreach (XElement element in XElement.Parse(queryResult).Descendants(XName.Get("RESULT_SET")).Elements<XElement>((XName) "item"))
        {
          string str1 = element.Element((XName) "NAME").Value;
          string str2 = ImportPathUtil.ConvertImportFromESRToInternal(element.Element((XName) "NAMESPACE").Value);
          string str3 = element.Element((XName) "PROXYNAME").Value;
          string str4 = element.Element((XName) "UUID").Value;
          string s = element.Element((XName) "LAST_CHANGED_DATE_TIME").Value;
          string str5 = (string) null;
          if (element.Element((XName) "DATA_TYPE_KEY_NAME") != null)
            str5 = element.Element((XName) "DATA_TYPE_KEY_NAME").Value;
          string str6 = (string) null;
          if (element.Element((XName) "LEADING_BO_NAME") != null)
            str6 = element.Element((XName) "LEADING_BO_NAME").Value;
          string str7 = (string) null;
          if (element.Element((XName) "MSG_DATA_TYPE_FIELDNAME") != null)
            str7 = element.Element((XName) "MSG_DATA_TYPE_FIELDNAME").Value;
          string str8 = (string) null;
          if (element.Element((XName) "TRANSITIVE_HASH") != null)
            str8 = element.Element((XName) "TRANSITIVE_HASH").Value;
          DateTime result;
          if (!DateTime.TryParse(s, out result))
            throw new ProtocolException(ProtocolException.ErrorArea.CLIENT, string.Format("unable to parse timestamp {0} for message type {1} in namespace {2}", (object) s, (object) str1, (object) str2));
          if (str2 != null && str1 != null && (str3 != null && str4 != null) && (str2.Length > 0 && str1.Length > 0 && str3.Length > 0))
          {
            if (str4.Length > 0)
            {
              try
              {
                RepositoryDataSet.MessageTypesRow messageTypesRow = (RepositoryDataSet.MessageTypesRow) this.repositoryDataSet.MessageTypes.Rows.Find(new object[2]{ (object) str2, (object) str1 });
                if (messageTypesRow != null)
                {
                  messageTypesRow.DataTypeName = str5;
                  messageTypesRow.LeadingBOName = str6;
                  messageTypesRow.DataTypeFieldName = str7;
                  messageTypesRow.LastChangedAt = result;
                  messageTypesRow.TrasitiveHash = str8;
                }
                else
                  this.repositoryDataSet.MessageTypes.Rows.Add((object) str2, (object) str1, (object) str3, (object) str5, (object) str6, (object) str7, (object) result, (object) str8);
                LastChangedItem lastChangedItem;
                if (mtLastChangedInfo.TryGetValue(str3, out lastChangedItem))
                {
                  if (str8 != null && !str8.Equals(lastChangedItem.TransitiveHash) && RepositoryDataCache.ModelChangeEventHandlerRegistry != null)
                    RepositoryDataCache.ModelChangeEventHandlerRegistry(RepositoryDataCache.MetaObjectTypes.MessageType, str3, false);
                  lastChangedItem.Handled = true;
                }
              }
              catch (ConstraintException ex)
              {
                Console.WriteLine(ex.Message);
              }
            }
          }
        }
        this.repositoryDataSet.AcceptChanges();
        if (RepositoryDataCache.ModelChangeEventHandlerRegistry == null)
          return;
        foreach (LastChangedItem lastChangedItem in mtLastChangedInfo.Values.Where<LastChangedItem>((Func<LastChangedItem, bool>) (n => !n.Handled)))
          RepositoryDataCache.ModelChangeEventHandlerRegistry(RepositoryDataCache.MetaObjectTypes.MessageType, lastChangedItem.ProxyName, true);
      }
    }

    public void RefreshBOsForNSInDataSet(string nsName, string bqlQueryResult)
    {
      lock (this.queryHandler.lockObject)
      {
        Dictionary<string, LastChangedItem> boLastChangedInfo = new Dictionary<string, LastChangedItem>();
        foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.BusinessObjects.Select("NSName='" + nsName + "'"))
        {
          boLastChangedInfo.Add(businessObjectsRow.ProxyName, new LastChangedItem(businessObjectsRow.ProxyName, businessObjectsRow.LastChangeAt, businessObjectsRow.TransitiveHash));
          foreach (DataRow row in this.repositoryDataSet.Nodes.Select("BOProxyName='" + businessObjectsRow.ProxyName + "'"))
            this.repositoryDataSet.Nodes.Rows.Remove(row);
          this.repositoryDataSet.BusinessObjects.Rows.Remove((DataRow) businessObjectsRow);
        }
        this.repositoryDataSet.AcceptChanges();
        this.ParseBOsIntoDataSet(bqlQueryResult, boLastChangedInfo);
      }
    }

    public void RefreshBO(string nsName, string boName, string bqlQueryResult)
    {
      RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.BusinessObjects.Select("NSName='" + nsName + "' AND Name='" + boName + "'");
      Dictionary<string, LastChangedItem> boLastChangedInfo = new Dictionary<string, LastChangedItem>();
      foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in businessObjectsRowArray)
      {
        boLastChangedInfo.Add(businessObjectsRow.ProxyName, new LastChangedItem(businessObjectsRow.ProxyName, businessObjectsRow.LastChangeAt, businessObjectsRow.TransitiveHash));
        foreach (DataRow row in this.repositoryDataSet.Nodes.Select("BOProxyName='" + businessObjectsRow.ProxyName + "'"))
          this.repositoryDataSet.Nodes.Rows.Remove(row);
        this.repositoryDataSet.BusinessObjects.Rows.Remove((DataRow) businessObjectsRow);
      }
      this.repositoryDataSet.AcceptChanges();
      this.ParseBOsIntoDataSet(bqlQueryResult, boLastChangedInfo);
    }

    public void RefreshBOs(string queryResult)
    {
      if (queryResult == null || queryResult.Length == 0)
        return;
      IEnumerable<XElement> xelements = XElement.Parse(queryResult).Descendants(XName.Get("RESULT_SET")).Elements<XElement>((XName) "item");
      Dictionary<string, LastChangedItem> boLastChangedInfo = new Dictionary<string, LastChangedItem>();
      foreach (XElement xelement in xelements)
      {
        string str1 = xelement.Element((XName) "NAME").Value;
        string str2 = xelement.Element((XName) "NAMESPACE").Value;
        RepositoryDataSet.BusinessObjectsDataTable businessObjects = this.repositoryDataSet.BusinessObjects;
        string filterExpression = "NSName='" + str2 + "' AND Name='" + str1 + "'";
        foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in (RepositoryDataSet.BusinessObjectsRow[]) businessObjects.Select(filterExpression))
        {
          boLastChangedInfo.Add(businessObjectsRow.ProxyName, new LastChangedItem(businessObjectsRow.ProxyName, businessObjectsRow.LastChangeAt, businessObjectsRow.TransitiveHash));
          foreach (DataRow row in this.repositoryDataSet.Nodes.Select("BOProxyName='" + businessObjectsRow.ProxyName + "'"))
            this.repositoryDataSet.Nodes.Rows.Remove(row);
          this.repositoryDataSet.BusinessObjects.Rows.Remove((DataRow) businessObjectsRow);
        }
      }
      this.ParseBOsIntoDataSet(queryResult, boLastChangedInfo);
    }

    public void RefreshDTsForNSInDataSet(string nsName, string bqlQueryResult)
    {
      lock (this.queryHandler.lockObject)
      {
        Dictionary<string, LastChangedItem> dtLastChangedInfo = new Dictionary<string, LastChangedItem>();
        foreach (RepositoryDataSet.DataTypesRow dataTypesRow in (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.DataTypes.Select("NSName='" + nsName + "'"))
        {
          dtLastChangedInfo.Add(dataTypesRow.ProxyName, new LastChangedItem(dataTypesRow.ProxyName, dataTypesRow.LastChangeAt, dataTypesRow.TransitiveHash));
          this.repositoryDataSet.DataTypes.Rows.Remove((DataRow) dataTypesRow);
        }
        this.repositoryDataSet.AcceptChanges();
        this.ParseDTsIntoDataSet(bqlQueryResult, dtLastChangedInfo);
      }
    }

    public void RefreshEnhancementOptionsForNSInDataSet(string nsName, string bqlQueryResult)
    {
      lock (this.queryHandler.lockObject)
      {
        Dictionary<string, LastChangedItem> dtLastChangedInfo = new Dictionary<string, LastChangedItem>();
        RepositoryDataSet.BAdIsRow[] badIsRowArray = (RepositoryDataSet.BAdIsRow[]) this.repositoryDataSet.BAdIs.Select("NSName='" + nsName + "'");
        DateTime lastChangedAt = new DateTime();
        foreach (RepositoryDataSet.BAdIsRow badIsRow in badIsRowArray)
        {
          dtLastChangedInfo.Add(badIsRow.ProxyName, new LastChangedItem(badIsRow.ProxyName, lastChangedAt));
          this.repositoryDataSet.BAdIs.Rows.Remove((DataRow) badIsRow);
        }
        this.repositoryDataSet.AcceptChanges();
        this.ParseBAdIIntoDataset(bqlQueryResult, dtLastChangedInfo);
      }
    }

    public void RefreshMTsForNSInDataSet(string nsName, string bqlQueryResult)
    {
      lock (this.queryHandler.lockObject)
      {
        Dictionary<string, LastChangedItem> mtLastChangedInfo = new Dictionary<string, LastChangedItem>();
        foreach (RepositoryDataSet.MessageTypesRow messageTypesRow in (RepositoryDataSet.MessageTypesRow[]) this.repositoryDataSet.DataTypes.Select("NSName='" + nsName + "'"))
        {
          mtLastChangedInfo.Add(messageTypesRow.ProxyName, new LastChangedItem(messageTypesRow.ProxyName, messageTypesRow.LastChangedAt, messageTypesRow.TrasitiveHash));
          this.repositoryDataSet.MessageTypes.Rows.Remove((DataRow) messageTypesRow);
        }
        this.repositoryDataSet.AcceptChanges();
        this.ParseMTsIntoDataSet(bqlQueryResult, mtLastChangedInfo);
      }
    }
  }
}
