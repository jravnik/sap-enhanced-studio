﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.DataModel.LastChangedItem
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;

namespace SAP.Copernicus.Core.Repository.DataModel
{
  public class LastChangedItem
  {
    public string TransitiveHash;

    public LastChangedItem(string proxyName, DateTime lastChangedAt)
    {
      this.ProxyName = proxyName;
      this.LastChangedAt = lastChangedAt;
    }

    public LastChangedItem(string proxyName, DateTime lastChangedAt, string transitiveHash)
    {
      this.ProxyName = proxyName;
      this.LastChangedAt = lastChangedAt;
      this.TransitiveHash = transitiveHash;
    }

    public string ProxyName { private set; get; }

    public DateTime LastChangedAt { private set; get; }

    public bool Handled { internal set; get; }
  }
}
