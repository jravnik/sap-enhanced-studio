﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtScenDataCache
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository.ExtScenDataModel;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Repository
{
  public class ExtScenDataCache
  {
    protected static ExtScenDataCache instance = new ExtScenDataCache();
    private ExtensionHandler handler = new ExtensionHandler();
    private List<RegistrationFieldScenario> fieldScenList = new List<RegistrationFieldScenario>();
    private List<FlowNodeElement> scenarioNodeList;
    private List<FlowRefElement> scenarioRefList;

    public static ExtScenDataCache GetInstance()
    {
      return ExtScenDataCache.instance;
    }

    public void Reset()
    {
      this.ResetScenarios();
      this.fieldScenList = new List<RegistrationFieldScenario>();
    }

    private void ResetScenarios()
    {
      this.scenarioNodeList = (List<FlowNodeElement>) null;
    }

    public bool addField(string boNamespace, string boName, string fieldName, string type, string node, List<string> scenarios, string solution, string length, string decimals, string defaultValue, string defaultCode, PDI_EXT_S_ID_TARGET_ATTRIBUTE relation, out string error, out string errorScenario, out string usedBoNamespace, out string usedBoName, out string usedNodeName, out string defValError)
    {
      return this.addField(boNamespace, boName, fieldName, "", "", type, node, scenarios, solution, length, decimals, defaultValue, defaultCode, relation, out error, out errorScenario, out usedBoNamespace, out usedBoName, out usedNodeName, out defValError);
    }

    public bool addField(string boNamespace, string boName, string fieldName, string label, string tooltip, string type, string node, List<string> scenarios, string solution, string length, string decimals, string defaultValue, string defaultCode, PDI_EXT_S_ID_TARGET_ATTRIBUTE relation, out string error, out string errorScenario, out string usedBoNamespace, out string usedBoName, out string usedNodeName, out string defValError)
    {
      if (solution.Equals(""))
        solution = ProjectUtil.GetOpenSolutionName();
      error = "";
      errorScenario = "";
      usedBoName = (string) null;
      usedNodeName = (string) null;
      usedBoNamespace = (string) null;
      defValError = (string) null;
      List<string> scenarioNames = this.getScenarioNames(solution);
      foreach (string scenario in scenarios)
      {
        if (!scenarioNames.Contains(scenario))
        {
          error = "scenario";
          errorScenario = scenario;
          return false;
        }
        if (!this.getScenarioNames(boNamespace, boName, node, solution).Contains(scenario))
        {
          error = nameof (node);
          errorScenario = scenario;
          return false;
        }
        bool kut = false;
        bool flag = false;
        bool inStandard;
        string usedFieldType;
        bool usedIsReference;
        string boNameRef;
        string nodeNameRef;
        bool defExists;
        string boNameDef;
        string nodeNameDef;
        if (ExtensionDataCache.GetInstance().checkFieldExists(boNamespace, boName, node, fieldName, out inStandard, out usedBoNamespace, out usedBoName, out usedNodeName, out usedFieldType, out usedIsReference, out boNameRef, out nodeNameRef, out defExists, out boNameDef, out nodeNameDef, out kut))
        {
          if (inStandard)
          {
            error = "field";
            return false;
          }
          if (kut)
          {
            error = "kut";
            return false;
          }
          if (defExists)
          {
            flag = defExists && boName.Equals(boNameDef);
            if (!flag)
            {
              error = "fieldexists";
              return false;
            }
          }
          if (!flag)
          {
            if (this.isFieldActiveInXBO(fieldName, usedBoNamespace, usedBoName, usedNodeName) && !usedIsReference)
            {
              error = "fieldexists";
              return false;
            }
            List<string> otherScenarios;
            if (!this.isPartOfScenario(usedBoNamespace, usedBoName, usedNodeName, solution, scenario, out otherScenarios) && (otherScenarios.Count <= 0 || !usedIsReference))
            {
              error = "field";
              errorScenario = scenario;
              return false;
            }
          }
        }
      }
      string BOName = (string) null;
      string BONode = (string) null;
      bool flag1 = ExtensionDataCache.GetInstance().addField(boNamespace, boName, fieldName, type, node, label, tooltip, length, decimals, false, defaultValue, defaultCode, relation, out BOName, out BONode, out defValError);
      this.addGenerationData(boNamespace, boName, fieldName, node, scenarios);
      return flag1;
    }

    private bool addGenerationData(string boNamespace, string boName, string fieldName, string node, List<string> scenarios)
    {
      foreach (string scenario in scenarios)
        this.fieldScenList.Add(new RegistrationFieldScenario(fieldName, scenario));
      return true;
    }

    public bool addReference(string boNamespace, string boName, string fieldName, string node, List<string> scenarios, string solution, out string error, out string errorScenario)
    {
      if (solution.Equals(""))
        solution = ProjectUtil.GetOpenSolutionName();
      List<string> scenarioNames = this.getScenarioNames(boNamespace, boName, node, solution);
      foreach (string scenario in scenarios)
      {
        if (!scenarioNames.Contains(scenario))
        {
          error = "scenario";
          errorScenario = scenario;
          return false;
        }
      }
      error = "";
      errorScenario = "";
      bool inStandard;
      string usedBoNamespace;
      string usedBoName;
      string usedNodeName;
      string usedFieldType;
      bool usedIsReference;
      string boNameRef;
      string nodeNameRef;
      bool defExists;
      string boNameDef;
      string nodeNameDef;
      bool kut;
      if (ExtensionDataCache.GetInstance().checkFieldExists(boNamespace, boName, node, fieldName, out inStandard, out usedBoNamespace, out usedBoName, out usedNodeName, out usedFieldType, out usedIsReference, out boNameRef, out nodeNameRef, out defExists, out boNameDef, out nodeNameDef, out kut))
      {
        if (!inStandard)
        {
          if (kut)
          {
            error = "kut";
            return false;
          }
          string errboName = (string) null;
          string errboNode = (string) null;
          string defValError = (string) null;
          PDI_EXT_S_ID_TARGET_ATTRIBUTE relation = new PDI_EXT_S_ID_TARGET_ATTRIBUTE();
          if (!ExtensionDataCache.GetInstance().addField(boNamespace, boName, fieldName, "", node, "", "", string.Empty, string.Empty, relation, out errboName, out errboNode, out defValError))
          {
            error = "field";
            errorScenario = "";
            return false;
          }
          if (this.addGenerationData(boNamespace, boName, fieldName, node, scenarios))
            return true;
          error = "field";
          errorScenario = "";
          return false;
        }
        error = "standardfield";
        errorScenario = "";
        return false;
      }
      error = "field";
      errorScenario = "";
      return false;
    }

    public bool addReference(string boNamespace, string boName, string fieldName, string node, string solution, out string error)
    {
      if (solution.Equals(""))
        solution = ProjectUtil.GetOpenSolutionName();
      error = "";
      bool inStandard;
      string usedBoNamespace;
      string usedBoName;
      string usedNodeName;
      string usedFieldType;
      bool usedIsReference;
      string boNameRef;
      string nodeNameRef;
      bool defExists;
      string boNameDef;
      string nodeNameDef;
      bool kut;
      if (ExtensionDataCache.GetInstance().checkFieldExists(boNamespace, boName, node, fieldName, out inStandard, out usedBoNamespace, out usedBoName, out usedNodeName, out usedFieldType, out usedIsReference, out boNameRef, out nodeNameRef, out defExists, out boNameDef, out nodeNameDef, out kut))
      {
        if (!inStandard)
        {
          if (kut)
          {
            error = "kut";
            return false;
          }
          string errboName = (string) null;
          string errboNode = (string) null;
          string defValError = (string) null;
          PDI_EXT_S_ID_TARGET_ATTRIBUTE relation = new PDI_EXT_S_ID_TARGET_ATTRIBUTE();
          if (ExtensionDataCache.GetInstance().addField(boNamespace, boName, fieldName, "", node, "", "", string.Empty, string.Empty, relation, out errboName, out errboNode, out defValError))
            return true;
          error = "field";
          return false;
        }
        error = "standardfield";
        return false;
      }
      error = "field";
      return false;
    }

    public List<string> getScenarioNames(string namespaceName, string boName, string nodeName, string solution)
    {
      if (solution.Equals(""))
        solution = ProjectUtil.GetOpenSolutionName();
      string str1 = "";
      string str2 = "";
      string str3 = "";
      foreach (PDI_EXT_S_EXT_NODE pdiExtSExtNode in ExtensionDataCache.GetInstance().GetBONodesData(namespaceName, boName, solution))
      {
        if (!pdiExtSExtNode.MAX_BO_PRX_NAME.Equals(""))
          str3 = pdiExtSExtNode.MAX_BO_PRX_NAME;
        if (pdiExtSExtNode.ND_NAME.Equals(nodeName))
        {
          str1 = pdiExtSExtNode.PRX_BO_NAME;
          str2 = pdiExtSExtNode.PRX_ND_NAME;
        }
      }
      if (!str3.Equals(""))
        str1 = str3;
      List<string> stringList = new List<string>();
      if (this.scenarioNodeList == null || this.scenarioNodeList.Count == 0)
        this.loadScenarios(solution);
      foreach (FlowNodeElement scenarioNode in this.scenarioNodeList)
      {
        if (scenarioNode.BoName.Equals(str1) && scenarioNode.NodeName.Equals(str2) && !stringList.Contains(scenarioNode.ScenarioName))
          stringList.Add(scenarioNode.ScenarioName);
      }
      return stringList;
    }

    public List<string> getScenarioNames(string solution)
    {
      if (solution.Equals(""))
        solution = ProjectUtil.GetOpenSolutionName();
      List<string> stringList = new List<string>();
      if (this.scenarioNodeList == null || this.scenarioNodeList.Count == 0)
        this.loadScenarios(solution);
      foreach (FlowNodeElement scenarioNode in this.scenarioNodeList)
      {
        if (!stringList.Contains(scenarioNode.ScenarioName))
          stringList.Add(scenarioNode.ScenarioName);
      }
      return stringList;
    }

    private void loadScenarios(string solution)
    {
      PDI_EXT_S_XFP_FILE_DETAILS[] ET_XFP_FILE_DETAILS = (PDI_EXT_S_XFP_FILE_DETAILS[]) null;
      OSLS_XREP_MASS_VERS_CONTENT[] ET_CONTENT_XS_FILES = (OSLS_XREP_MASS_VERS_CONTENT[]) null;
      OSLS_XREP_MASS_VERS_CONTENT[] ET_CONTENT_XBO_FILES = (OSLS_XREP_MASS_VERS_CONTENT[]) null;
      this.scenarioNodeList = new List<FlowNodeElement>();
      this.scenarioRefList = new List<FlowRefElement>();
      bool IV_XS_FILES = true;
      this.handler.GetXrepFiles(solution, IV_XS_FILES, false, false, out ET_CONTENT_XS_FILES, out ET_CONTENT_XBO_FILES, out ET_XFP_FILE_DETAILS);
      foreach (OSLS_XREP_MASS_VERS_CONTENT xrepMassVersContent in ET_CONTENT_XS_FILES)
      {
        ProcessExtensionScenarioModel extensionScenarioModel = new ProcessExtensionScenarioModel();
        extensionScenarioModel.loadModelByString(xrepMassVersContent.FILE_CONTENT);
        if (extensionScenarioModel.xmlModel.ExtensionScenarioList != null)
        {
          foreach (ExtensionScenarioType extensionScenario in extensionScenarioModel.xmlModel.ExtensionScenarioList)
          {
            if (extensionScenario.is_selected && extensionScenario.bo_connections != null)
            {
              foreach (FlowType boConnection in extensionScenario.bo_connections)
              {
                this.scenarioNodeList.Add(new FlowNodeElement(extensionScenario.scenario_name, boConnection.source_bo_name, boConnection.source_bo_node_name, extensionScenarioModel.xmlModel.Name));
                this.scenarioNodeList.Add(new FlowNodeElement(extensionScenario.scenario_name, boConnection.target_bo_name, boConnection.target_bo_node_name, extensionScenarioModel.xmlModel.Name));
                ReferenceFieldType[] referenceFieldKeys = boConnection.reference_field_keys;
                if (referenceFieldKeys != null)
                {
                  int length = referenceFieldKeys.Length;
                  for (int index = 0; index < length; ++index)
                  {
                    ReferenceFieldType referenceFieldType = referenceFieldKeys[index];
                    this.scenarioRefList.Add(new FlowRefElement(extensionScenario.scenario_name, referenceFieldType.reference_field_bundle_key, referenceFieldType.reference_field_name, extensionScenarioModel.xmlModel.Name));
                  }
                }
              }
            }
          }
        }
      }
    }

    public PDI_EXT_S_EXT_FIELD_SCENARIOS[] getFieldScenarios()
    {
      PDI_EXT_S_EXT_FIELD_SCENARIOS[] extFieldScenariosArray = new PDI_EXT_S_EXT_FIELD_SCENARIOS[this.fieldScenList.Count];
      int index = 0;
      foreach (RegistrationFieldScenario fieldScen in this.fieldScenList)
      {
        extFieldScenariosArray[index] = new PDI_EXT_S_EXT_FIELD_SCENARIOS();
        extFieldScenariosArray[index].FIELD_NAME = fieldScen.Field;
        extFieldScenariosArray[index].PX_SCENARIO_NAME = fieldScen.Scenario;
        ++index;
      }
      return extFieldScenariosArray;
    }

    public bool isScenarioInUse(string scenario, out string xbo, out string node)
    {
      OSLS_XREP_MASS_VERS_CONTENT[] ET_CONTENT_XS_FILES = (OSLS_XREP_MASS_VERS_CONTENT[]) null;
      OSLS_XREP_MASS_VERS_CONTENT[] ET_CONTENT_XBO_FILES = (OSLS_XREP_MASS_VERS_CONTENT[]) null;
      PDI_EXT_S_XFP_FILE_DETAILS[] ET_XFP_FILE_DETAILS = (PDI_EXT_S_XFP_FILE_DETAILS[]) null;
      this.handler.GetXrepFiles(ProjectUtil.GetOpenSolutionName(), false, false, true, out ET_CONTENT_XS_FILES, out ET_CONTENT_XBO_FILES, out ET_XFP_FILE_DETAILS);
      node = "";
      xbo = "";
      foreach (PDI_EXT_S_XFP_FILE_DETAILS extSXfpFileDetails in ET_XFP_FILE_DETAILS)
      {
        if (extSXfpFileDetails.PX_SCENARIO_NAME.Equals(scenario))
        {
          node = extSXfpFileDetails.ESR_ND_NAME;
          xbo = extSXfpFileDetails.XBO_NAME;
          return true;
        }
      }
      return false;
    }

    public bool isNodeExtScenarioInUse(string scenario, string bo_namespace, string bo_name, string bo_node, string extended_node)
    {
      if (bo_namespace != null && bo_name != null && (bo_node != null && extended_node != null))
        return this.handler.GetNodeExtXrepFiles(ProjectUtil.GetOpenSolutionName(), bo_namespace, bo_name, bo_node, extended_node, scenario);
      return false;
    }

    private bool isPartOfScenario(string namespaceName, string boName, string nodeName, string solution, string scenario, out List<string> otherScenarios)
    {
      if (solution.Equals(""))
        solution = ProjectUtil.GetOpenSolutionName();
      string str1 = "";
      string str2 = "";
      foreach (PDI_EXT_S_EXT_NODE pdiExtSExtNode in ExtensionDataCache.GetInstance().GetBONodesData(namespaceName, boName, solution))
      {
        if (pdiExtSExtNode.ND_NAME.Equals(nodeName))
        {
          str1 = pdiExtSExtNode.PRX_BO_NAME;
          str2 = pdiExtSExtNode.PRX_ND_NAME;
          break;
        }
      }
      bool flag = false;
      otherScenarios = new List<string>();
      if (this.scenarioNodeList == null || this.scenarioNodeList.Count == 0)
        this.loadScenarios(solution);
      foreach (FlowNodeElement scenarioNode in this.scenarioNodeList)
      {
        if (scenarioNode.BoName.Equals(str1) && scenarioNode.NodeName.Equals(str2))
        {
          if (scenarioNode.ScenarioName.Equals(scenario))
            flag = true;
          else if (!otherScenarios.Contains(scenarioNode.ScenarioName))
            otherScenarios.Add(scenarioNode.ScenarioName);
        }
      }
      return flag;
    }

    private bool isFieldActiveInXBO(string field, string boNamespace, string boName, string nodeName)
    {
      string openSolutionName = ProjectUtil.GetOpenSolutionName();
      string str1 = "";
      string str2 = "";
      foreach (PDI_EXT_S_EXT_NODE pdiExtSExtNode in ExtensionDataCache.GetInstance().GetBONodesData(boNamespace, boName, openSolutionName))
      {
        if (pdiExtSExtNode.ND_NAME.Equals(nodeName))
        {
          str1 = pdiExtSExtNode.PRX_BO_NAME;
          str2 = pdiExtSExtNode.PRX_ND_NAME;
          break;
        }
      }
      OSLS_XREP_MASS_VERS_CONTENT[] ET_CONTENT_XS_FILES = (OSLS_XREP_MASS_VERS_CONTENT[]) null;
      OSLS_XREP_MASS_VERS_CONTENT[] ET_CONTENT_XBO_FILES = (OSLS_XREP_MASS_VERS_CONTENT[]) null;
      PDI_EXT_S_XFP_FILE_DETAILS[] ET_XFP_FILE_DETAILS = (PDI_EXT_S_XFP_FILE_DETAILS[]) null;
      bool IV_XFP_FILE = true;
      this.handler.GetXrepFiles(openSolutionName, false, false, IV_XFP_FILE, out ET_CONTENT_XS_FILES, out ET_CONTENT_XBO_FILES, out ET_XFP_FILE_DETAILS);
      foreach (PDI_EXT_S_XFP_FILE_DETAILS extSXfpFileDetails in ET_XFP_FILE_DETAILS)
      {
        if (extSXfpFileDetails.FIELD_NAME.Equals(field) && extSXfpFileDetails.PRX_BO_NAME.Equals(str1) && extSXfpFileDetails.PRX_ND_NAME.Equals(str2))
          return true;
      }
      return false;
    }
  }
}
