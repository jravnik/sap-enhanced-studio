﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.MessageAssignment
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class MessageAssignment
  {
    public string NodeProxy { get; set; }

    public string Message { get; set; }

    public MessageAssignment(string node, string message)
    {
      this.NodeProxy = node;
      this.Message = message;
    }
  }
}
