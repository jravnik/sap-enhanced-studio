﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeExtScenDataModel.ProcessExtensionScenarioListHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Repository.NodeExtScenDataList;
using System;
using System.IO;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.NodeExtScenDataModel
{
  public class ProcessExtensionScenarioListHandler
  {
    public NodeExtensionScenarioList loadModelByString(string content)
    {
      try
      {
        using (StringReader stringReader = new StringReader(content))
          return (NodeExtensionScenarioList) new XmlSerializer(typeof (NodeExtensionScenarioList)).Deserialize((TextReader) stringReader);
      }
      catch (Exception ex)
      {
        Console.Out.WriteLine("[ERROR] An exception occurred when trying to deserialize string " + content);
        return (NodeExtensionScenarioList) null;
      }
    }
  }
}
