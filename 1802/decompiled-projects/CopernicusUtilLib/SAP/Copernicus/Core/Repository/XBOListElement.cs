﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.XBOListElement
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class XBOListElement
  {
    private string xbo;
    private string boName;
    private string boNamespace;

    public XBOListElement(string xbo, string boName, string boNamespace)
    {
      this.xbo = xbo;
      this.boName = boName;
      this.boNamespace = boNamespace;
    }

    public string getXBO()
    {
      return this.xbo;
    }

    public string getBO()
    {
      return this.boName;
    }

    public string getNamespace()
    {
      return this.boNamespace;
    }
  }
}
