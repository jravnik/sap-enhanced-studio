﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.EntityUsageIndex.EntityUsageType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository.EntityUsageIndex
{
  public enum EntityUsageType
  {
    SOURCE_USES_SAP_GDT = 10,
    SOURCE_USES_SAP_BO = 20,
    SOURCE_USES_SAP_BO_NODE = 21,
    SOURCE_USES_SAP_BO_NODE_ELEMENT = 22,
    SOURCE_USES_SAP_BO_NODE_ACTION = 23,
    SOURCE_USES_SAP_BO_NODE_QUERY = 24,
    SOURCE_USES_SAP_BO_ASSOCIATION = 25,
    XBO_USES_SAP_BO = 140,
    XBO_USES_SAP_BO_NODE = 141,
    XBO_USES_SAP_GDT = 142,
    SOURCE_USES_PROCESS_COMPONENT = 152,
  }
}
