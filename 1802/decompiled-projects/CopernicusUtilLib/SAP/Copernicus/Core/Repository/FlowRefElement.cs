﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.FlowRefElement
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class FlowRefElement
  {
    public string FlowName { get; set; }

    public string RefFieldBundleKey { get; set; }

    public string RefFieldName { get; set; }

    public string ScenarioName { get; set; }

    public FlowRefElement(string flowName, string refFieldBundleKey, string RefFieldName, string scenarioName)
    {
      this.FlowName = flowName;
      this.RefFieldBundleKey = refFieldBundleKey;
      this.RefFieldName = RefFieldName;
      this.ScenarioName = scenarioName;
    }
  }
}
