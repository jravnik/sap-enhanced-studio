﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.GenericFileEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class GenericFileEventArgs
  {
    public GenericFileEventArgs(bool overallStatus, bool deltaMode = false, bool migrationMode = false)
    {
      this.OverallStatus = overallStatus;
      this.DeltaMode = deltaMode;
      this.MigrationMode = migrationMode;
    }

    public bool OverallStatus { get; private set; }

    public bool DeltaMode { get; private set; }

    public bool MigrationMode { get; private set; }

    public override bool Equals(object obj)
    {
      GenericFileEventArgs genericFileEventArgs = obj as GenericFileEventArgs;
      return genericFileEventArgs != null && genericFileEventArgs.DeltaMode == this.DeltaMode && genericFileEventArgs.OverallStatus == this.OverallStatus;
    }

    public override int GetHashCode()
    {
      return this.OverallStatus.GetHashCode() ^ this.DeltaMode.GetHashCode();
    }

    public override string ToString()
    {
      return string.Format("[OverallStatus:{0},DeltaMode:{1}]", (object) this.OverallStatus, (object) this.DeltaMode);
    }
  }
}
