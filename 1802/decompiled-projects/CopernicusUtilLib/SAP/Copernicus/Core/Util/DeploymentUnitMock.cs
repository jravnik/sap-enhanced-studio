﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.DeploymentUnitMock
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.Core.Util
{
  public class DeploymentUnitMock : DeploymentUnit
  {
    public static readonly DeploymentUnitMock Foreign_Trade_Management = new DeploymentUnitMock("/FTA/FOREIGN_TRADE_MANAGEMENT", "ForeignTradeManagement", "Foreign Trade Management", "1");
    public static readonly DeploymentUnitMock Purchasing = new DeploymentUnitMock("/SRMAP/PURCHASING", nameof (Purchasing), nameof (Purchasing), "3");
    public static readonly DeploymentUnitMock Requisitioning = new DeploymentUnitMock("/SRMAP/REQUISITIONING", nameof (Requisitioning), nameof (Requisitioning), "3");
    public static readonly DeploymentUnitMock Supplier_Invoicing = new DeploymentUnitMock("/SRMAP/SUPPLIER_INVOICING", "SupplierInvoicing", "Supplier Invoicing", "3");
    public static readonly DeploymentUnitMock Customer_Invoicing = new DeploymentUnitMock("CUSTOMER_INVOICING", "CustomerInvoicing", "Customer Invoicing", "3");
    public static readonly DeploymentUnitMock Customer_Relationship_Management = new DeploymentUnitMock("CUSTOMER_RELATIONSHIP_MGMT", "CustomerRelationshipManagement", "Customer Relationship Management", "3");
    public static readonly DeploymentUnitMock Financials = new DeploymentUnitMock("FINANCIALS", nameof (Financials), nameof (Financials), "3");
    public static readonly DeploymentUnitMock Foundation = new DeploymentUnitMock("FOUNDATION", nameof (Foundation), nameof (Foundation), "3");
    public static readonly DeploymentUnitMock Human_Capital_Management = new DeploymentUnitMock("HUMAN_CAPITAL_MANAGEMENT", "HumanCapitalManagement", "Human Capital Management", "3");
    public static readonly DeploymentUnitMock Production_and_Site_Logistics_Execution = new DeploymentUnitMock("PROD_AND_STE_LOGS_EXECUTION", "ProductionAndSiteLogisticsExecution", "Production and Site Logistics Execution", "3");
    public static readonly DeploymentUnitMock Project_Management = new DeploymentUnitMock("PROJECT_MANAGEMENT", "ProjectManagement", "Project Management", "3");
    public static readonly DeploymentUnitMock Supply_Chain_Control = new DeploymentUnitMock("SUPPLY_CHAIN_CONTROL", "SupplyChainControl", "Supply Chain Control", "3");

    public static DeploymentUnitMock[] Values
    {
      get
      {
        return new DeploymentUnitMock[12]{ DeploymentUnitMock.Foreign_Trade_Management, DeploymentUnitMock.Purchasing, DeploymentUnitMock.Requisitioning, DeploymentUnitMock.Supplier_Invoicing, DeploymentUnitMock.Customer_Invoicing, DeploymentUnitMock.Customer_Relationship_Management, DeploymentUnitMock.Financials, DeploymentUnitMock.Foundation, DeploymentUnitMock.Human_Capital_Management, DeploymentUnitMock.Production_and_Site_Logistics_Execution, DeploymentUnitMock.Project_Management, DeploymentUnitMock.Supply_Chain_Control };
      }
    }

    public new static string[] DeploymentUnitNames
    {
      get
      {
        string[] strArray = new string[DeploymentUnitMock.Values.Length];
        for (int index = 0; index < DeploymentUnitMock.Values.Length; ++index)
          strArray[index] = DeploymentUnitMock.Values[index].Name;
        return strArray;
      }
    }

    public new static string[] DeploymentUnitProxyNames
    {
      get
      {
        string[] strArray = new string[DeploymentUnitMock.Values.Length];
        for (int index = 0; index < DeploymentUnitMock.Values.Length; ++index)
          strArray[index] = DeploymentUnitMock.Values[index].ProxyName;
        return strArray;
      }
    }

    public DeploymentUnitMock(string proxyName, string name, string semanticalName, string psmReleaseStatus)
      : base(proxyName, name, semanticalName, psmReleaseStatus)
    {
    }

    public new static bool IsValidDeploymentUnit(string MDRSNameOfDU)
    {
      return ((IEnumerable<DeploymentUnitMock>) DeploymentUnitMock.Values).First<DeploymentUnitMock>((Func<DeploymentUnitMock, bool>) (e => e.Name == MDRSNameOfDU)) != null;
    }

    public new static string DUProxyNameForName(string MDRSNameOfDU)
    {
      return ((IEnumerable<DeploymentUnitMock>) DeploymentUnitMock.Values).First<DeploymentUnitMock>((Func<DeploymentUnitMock, bool>) (e => e.Name == MDRSNameOfDU)).ProxyName;
    }

    public new static DeploymentUnit fromDUProxyName(string DUProxyName)
    {
      return (DeploymentUnit) ((IEnumerable<DeploymentUnitMock>) DeploymentUnitMock.Values).First<DeploymentUnitMock>((Func<DeploymentUnitMock, bool>) (e => e.ProxyName == DUProxyName));
    }

    public override string ToString()
    {
      return this.SemanticalName;
    }

    public new static string GetSematicNameFromESRName(string MDRSNameOfDU)
    {
      return ((IEnumerable<DeploymentUnitMock>) DeploymentUnitMock.Values).First<DeploymentUnitMock>((Func<DeploymentUnitMock, bool>) (e => e.Name == MDRSNameOfDU)).SemanticalName;
    }

    public new static DeploymentUnit fromString(string str)
    {
      return (DeploymentUnit) ((IEnumerable<DeploymentUnitMock>) DeploymentUnitMock.Values).First<DeploymentUnitMock>((Func<DeploymentUnitMock, bool>) (e => e.SemanticalName == str));
    }

    public new string GetDeploymentUnitProxyName()
    {
      return this.ProxyName;
    }

    public new string GetDeploymentUnitName()
    {
      return this.Name;
    }
  }
}
