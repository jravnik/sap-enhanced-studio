﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.Forms.NumberTextBox
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Util.Forms
{
  internal class NumberTextBox : TextBox
  {
    public NumberTextBox()
    {
      this.CausesValidation = true;
      this.Validating += new CancelEventHandler(this.TextBox_Validation);
    }

    private void TextBox_Validation(object sender, CancelEventArgs ce)
    {
      try
      {
        int.Parse(this.Text);
      }
      catch (Exception ex)
      {
        ce.Cancel = true;
        int num = (int) MessageBox.Show(Resource.NumberTextBox_Validation_Error, Resource.NumberTextBox_Validation_Caption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
      }
    }
  }
}
