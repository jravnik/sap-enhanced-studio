﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.SolutionType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Util
{
  public class SolutionType
  {
    public static readonly SolutionType Mashups = new SolutionType("001");
    public static readonly SolutionType Integrated_Solutions = new SolutionType("002");
    public static readonly SolutionType Lightweight_Solutions = new SolutionType("003");
    public static readonly SolutionType Add_on_Solutions = new SolutionType("004");
    public static readonly SolutionType Preconfigured_Solutions = new SolutionType("005");
    public static readonly SolutionType OneOff_Solutions = new SolutionType("006");
    public static readonly SolutionType Template_Solutions = new SolutionType("007");
    public static readonly SolutionType MultiCustomer_Solutions = new SolutionType("008");
    private readonly string SolutionTypeCode;

    public static SolutionType[] Values
    {
      get
      {
        if (Connection.getInstance().UsageMode == UsageMode.OneOff || Connection.getInstance().UsageMode == UsageMode.miltiCustomer)
        {
          if (Connection.getInstance().IsMultiCustomerEnabled && Environment.GetEnvironmentVariable("_copernicus_trace_file") == "AllowMulti")
            return new SolutionType[3]{ SolutionType.OneOff_Solutions, SolutionType.MultiCustomer_Solutions, SolutionType.Template_Solutions };
          return new SolutionType[2]{ SolutionType.OneOff_Solutions, SolutionType.Template_Solutions };
        }
        return new SolutionType[5]{ SolutionType.Mashups, SolutionType.Integrated_Solutions, SolutionType.Lightweight_Solutions, SolutionType.Add_on_Solutions, SolutionType.Preconfigured_Solutions };
      }
    }

    public static IEnumerable<SolutionType> ValuesEnumerator
    {
      get
      {
        if (Connection.getInstance().UsageMode == UsageMode.OneOff || Connection.getInstance().UsageMode == UsageMode.miltiCustomer)
        {
          yield return SolutionType.OneOff_Solutions;
          yield return SolutionType.Template_Solutions;
          yield return SolutionType.MultiCustomer_Solutions;
        }
        else
        {
          yield return SolutionType.Mashups;
          yield return SolutionType.Integrated_Solutions;
          yield return SolutionType.Lightweight_Solutions;
          yield return SolutionType.Add_on_Solutions;
          yield return SolutionType.Preconfigured_Solutions;
        }
      }
    }

    private SolutionType(string SolutionTypeCode)
    {
      this.SolutionTypeCode = SolutionTypeCode;
    }

    public static SolutionType fromSolutionTypeCode(string SolutionTypeCode)
    {
      switch (SolutionTypeCode)
      {
        case "001":
          return SolutionType.Mashups;
        case "002":
          return SolutionType.Integrated_Solutions;
        case "003":
          return SolutionType.Lightweight_Solutions;
        case "004":
          return SolutionType.Add_on_Solutions;
        case "005":
          return SolutionType.Preconfigured_Solutions;
        case "006":
          return SolutionType.OneOff_Solutions;
        case "007":
          return SolutionType.Template_Solutions;
        case "008":
          return SolutionType.MultiCustomer_Solutions;
        default:
          return (SolutionType) null;
      }
    }

    public override string ToString()
    {
      switch (this.SolutionTypeCode)
      {
        case "001":
          return "Mashup";
        case "002":
          return "Integrated Solution";
        case "003":
          return "Lightweight Solution";
        case "004":
          return "Add-on Solution";
        case "005":
          return "Preconfigured Solution";
        case "006":
          return "Customer-Specific Solution";
        case "007":
          return "Solution Template";
        case "008":
          return "Multi-Customer Solution";
        default:
          return (string) null;
      }
    }

    public static SolutionType fromString(string str)
    {
      switch (str)
      {
        case "Mashup":
          return SolutionType.Mashups;
        case "Integrated Solution":
          return SolutionType.Integrated_Solutions;
        case "Lightweight Solution":
          return SolutionType.Lightweight_Solutions;
        case "Add-on Solution":
          return SolutionType.Add_on_Solutions;
        case "Preconfigured Solution":
          return SolutionType.Preconfigured_Solutions;
        case "Customer-Specific Solution":
          return SolutionType.OneOff_Solutions;
        case "Solution Template":
          return SolutionType.Template_Solutions;
        case "Multi-Customer Solution":
          return SolutionType.MultiCustomer_Solutions;
        default:
          return (SolutionType) null;
      }
    }

    public string GetSolutionTypeCode()
    {
      return this.SolutionTypeCode;
    }
  }
}
