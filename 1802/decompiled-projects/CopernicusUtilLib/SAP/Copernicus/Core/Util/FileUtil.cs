﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.FileUtil
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;
using System.IO;

namespace SAP.Copernicus.Core.Util
{
  public static class FileUtil
  {
    public static List<string> GetFilesFromDir(string dirPath, string pattern)
    {
      List<string> fileNames = new List<string>();
      FileUtil.GetFilesFromDir(dirPath, pattern, fileNames);
      return fileNames;
    }

    private static void GetFilesFromDir(string dirPath, string pattern, List<string> fileNames)
    {
      fileNames.AddRange((IEnumerable<string>) Directory.GetFiles(dirPath, pattern));
      foreach (string directory in Directory.GetDirectories(dirPath))
        fileNames.AddRange((IEnumerable<string>) FileUtil.GetFilesFromDir(directory, pattern));
    }
  }
}
