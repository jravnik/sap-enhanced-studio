﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.RepositoryUtil
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Project;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace SAP.Copernicus.Core.Util
{
  public static class RepositoryUtil
  {
    private static readonly char[] DOT = new char[1]{ '.' };
    private static readonly char[] HEX_ALPHABET = new char[16]{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    private static readonly HashAlgorithm hashAlgorithm = (HashAlgorithm) MD5.Create();
    private static byte[] EMPTY_BYTES = new byte[0];
    private static readonly Regex invalidABAPCharacters = new Regex("[^/_0-9A-Z]+");
    private static readonly Regex invalidABAPStartCharacters = new Regex("^[^/0-9A-Z]");
    private static readonly Regex validNameKeyName = new Regex("^[A-Z][A-Za-z0-9_]*$");
    private static readonly Regex validMessageIDCharacters = new Regex("^[_0-9A-Za-z]*$");
    private static readonly Regex validPrefixMessageID = new Regex("^[A-Za-z]");
    public const int STRING_LENGHT = 4;
    public const int MAX_PROXY_NAME_LENGTH = 30;
    public const int MAX_NAME_LENGTH = 120;
    public const int MAX_PROXY_ELEMENT_NAME_LENGTH = 27;
    private const string BO_IF_COMPARTMENT = "IF_";

    public static int HashByteLength
    {
      get
      {
        return 8;
      }
    }

    public static int HashStringLength
    {
      get
      {
        return 2 * RepositoryUtil.HashByteLength;
      }
    }

    public static bool IsValidNameKeyName(string name)
    {
      if (name == null || name.Length > 120)
        return false;
      return RepositoryUtil.validNameKeyName.IsMatch(name);
    }

    public static string GetHashFor(string name, Encoding encoding)
    {
      return RepositoryUtil.HexEncodeHash(RepositoryUtil.GetHashBytesFor(name, encoding));
    }

    private static string HexEncodeHash(byte[] hash)
    {
      if (hash == null)
        return (string) null;
      char[] chArray1 = new char[RepositoryUtil.HashStringLength];
      int num1 = 0;
      foreach (int num2 in hash)
      {
        int num3 = num2 & (int) byte.MaxValue;
        int index1 = num3 >> 4;
        int index2 = num3 & 15;
        char[] chArray2 = chArray1;
        int index3 = num1;
        int num4 = 1;
        int num5 = index3 + num4;
        int num6 = (int) RepositoryUtil.HEX_ALPHABET[index1];
        chArray2[index3] = (char) num6;
        char[] chArray3 = chArray1;
        int index4 = num5;
        int num7 = 1;
        num1 = index4 + num7;
        int num8 = (int) RepositoryUtil.HEX_ALPHABET[index2];
        chArray3[index4] = (char) num8;
      }
      return new string(chArray1);
    }

    private static byte[] GetHashBytesFor(string name, Encoding encoding)
    {
      if (name == null)
        return (byte[]) null;
      if (name.Length == 0)
        return RepositoryUtil.EMPTY_BYTES;
      if (encoding == null)
        encoding = Encoding.Default;
      byte[] bytes = encoding.GetBytes(name);
      byte[] hash = RepositoryUtil.hashAlgorithm.ComputeHash(bytes);
      byte[] numArray = new byte[RepositoryUtil.HashByteLength];
      int length1 = numArray.Length;
      int length2 = hash.Length;
      int num1 = 0;
      while (length2 > 0)
      {
        int num2 = length2;
        if (num2 > length1)
          num2 = length1;
        for (int index = 0; index < num2; ++index)
          numArray[index] ^= hash[index + num1];
        num1 += num2;
        length2 -= num2;
      }
      return numArray;
    }

    private static string StripInvalidCharacters(string name)
    {
      if (name == null)
        return (string) null;
      string input = RepositoryUtil.invalidABAPStartCharacters.Replace(name, "");
      return RepositoryUtil.invalidABAPCharacters.Replace(input, "");
    }

    public static bool IsValidABAPName(string name)
    {
      if (name == null)
        return true;
      if (!RepositoryUtil.invalidABAPCharacters.IsMatch(name))
        return !name.StartsWith("_");
      return false;
    }

    public static string ConvertBONameToProxyName(string nsName, string boName)
    {
      return RepositoryUtil.GetProxyNameForName(RepositoryUtil.GetABAPNamespacePrefix(nsName), (string) null, boName, 30);
    }

    public static string ConvertBONameToProxyName(string nsName, string boName, int length)
    {
      return RepositoryUtil.GetProxyNameForName(RepositoryUtil.GetABAPNamespacePrefix(nsName), (string) null, boName, length);
    }

    public static string ConvertDTNameToProxyName(string nsName, string dtName)
    {
      return RepositoryUtil.GetProxyNameForName(RepositoryUtil.GetABAPNamespacePrefix(nsName), (string) null, dtName, 30);
    }

    public static string ConvertDTNameToProxyName(string nsName, string dtName, int length)
    {
      return RepositoryUtil.GetProxyNameForName(RepositoryUtil.GetABAPNamespacePrefix(nsName), (string) null, dtName, length);
    }

    public static string ConvertImplementationNameToProxyName(string nsName, string implementationName)
    {
      return RepositoryUtil.GetProxyNameForName(RepositoryUtil.GetABAPNamespacePrefix(nsName), (string) null, implementationName, 30);
    }

    public static bool IsValidateMessageID(string input)
    {
      if (string.IsNullOrEmpty(input))
        return false;
      return RepositoryUtil.validMessageIDCharacters.IsMatch(input);
    }

    public static bool IsValidPrefixMessageID(string input)
    {
      return RepositoryUtil.validPrefixMessageID.IsMatch(input);
    }

    public static string GetBOServiceProviderInterfaceName(string nsName, string boName)
    {
      return RepositoryUtil.GetProxyNameForName(RepositoryUtil.GetABAPNamespacePrefix(nsName), "IF_", boName, 30);
    }

    private static string GetABAPNamespacePrefix(string nsName)
    {
      if (nsName == null)
        return (string) null;
      string partnerNamespace = ProjectUtil.GetRuntimeNamespacePrefixForPartnerNamespace(nsName);
      if (string.IsNullOrEmpty(partnerNamespace))
        throw new InvalidOperationException(string.Format("No ABAP namespace prefix found for namespace {0}", (object) nsName));
      return partnerNamespace;
    }

    public static string GetLocalABAPNamespacePrefix(string nsName)
    {
      string source = RepositoryUtil.GetABAPNamespacePrefix(nsName);
      if (source.Contains<char>('/'))
        source = "Z" + source.Replace("/", "") + "_";
      return source;
    }

    public static string FlattenDeepAttributeName(string deepAttributeName)
    {
      return RepositoryUtil.GetProxyNameForName((string) null, (string) null, deepAttributeName.Replace("/", "-"), 30);
    }

    public static string GetNodeElementDataTypeNameFor(string nsName, string boName, string boNodeName)
    {
      if (string.IsNullOrEmpty(nsName) || string.IsNullOrEmpty(boName) || string.IsNullOrEmpty(boNodeName))
        throw new ArgumentException("nsName, boName, and boNodeName must not be null or empty");
      string str = boName + boNodeName + "NodeElements";
      if (str.Length > 120)
        return RepositoryUtil.GetNodeElementDataTypeProxyNameFor(nsName, boName, boNodeName);
      return str;
    }

    public static string GetNodeElementDataTypeProxyNameFor(string nsName, string boName, string boNodeName)
    {
      if (string.IsNullOrEmpty(nsName) || string.IsNullOrEmpty(boName) || string.IsNullOrEmpty(boNodeName))
        throw new ArgumentException("nsName, boName, and boNodeName must not be null or empty");
      return RepositoryUtil.GetProxyNameForName(RepositoryUtil.GetABAPNamespacePrefix(nsName), (string) null, boName + boNodeName + "NodeElements", 30);
    }

    public static string GetQueryParameterDataTypeNameFor(string nsName, string boName, string boNodeName, string queryName)
    {
      if (string.IsNullOrEmpty(nsName) || string.IsNullOrEmpty(boName) || (string.IsNullOrEmpty(boNodeName) || string.IsNullOrEmpty(queryName)))
        throw new ArgumentException("nsName, boName, boNodeName, queryName must not be null or empty");
      string str = boName + boNodeName + queryName + "QueryElements";
      if (str.Length > 120)
        return RepositoryUtil.GetQueryParameterDataTypeProxyNameFor(nsName, boName, boNodeName, queryName);
      return str;
    }

    public static string GetQueryParameterDataTypeProxyNameFor(string nsName, string boName, string boNodeName, string queryName)
    {
      if (string.IsNullOrEmpty(nsName) || string.IsNullOrEmpty(boName) || (string.IsNullOrEmpty(boNodeName) || string.IsNullOrEmpty(queryName)))
        throw new ArgumentException("nsName, boName, boNodeName, queryName must not be null or empty");
      return RepositoryUtil.GetProxyNameForName(RepositoryUtil.GetABAPNamespacePrefix(nsName), (string) null, boName + boNodeName + queryName + "QueryElements", 30);
    }

    public static string GetProxyNameForName(string ccName)
    {
      return RepositoryUtil.GetProxyNameForName((string) null, (string) null, ccName, 27);
    }

    public static string GetProxyNameForName(string namespacePrefix, string objectTypePrefix, string ccName, int maxLen)
    {
      if (ccName == null)
        return (string) null;
      if (!RepositoryUtil.IsValidABAPName(namespacePrefix))
        throw new ArgumentException("namespacePrefix contains invalid characters");
      if (!RepositoryUtil.IsValidABAPName(objectTypePrefix))
        throw new ArgumentException("objectTypePrefix contains invalid characters");
      string str1 = string.Empty + namespacePrefix + objectTypePrefix;
      int num1 = maxLen - str1.Length - RepositoryUtil.HashStringLength;
      if (num1 < 0)
        throw new ArgumentException("name prefix too long");
      string str2 = RepositoryUtil.StripInvalidCharacters(ccName.ToUpperInvariant());
      string str3;
      if (num1 < str2.Length)
      {
        int length = num1 / 2 + num1 % 2;
        int num2 = num1 / 2;
        str3 = str1 + str2.Substring(0, length) + str2.Substring(str2.Length - num2);
      }
      else
        str3 = str1 + str2;
      return str3 + RepositoryUtil.GetHashFor(ccName, Encoding.Default);
    }

    public static string EncodeName(string name)
    {
      if (name == null)
        return name;
      return name.Replace("/", "%2f").Replace("=", "%3d");
    }

    public static string DecodeName(string name)
    {
      if (name == null)
        return name;
      return name.Replace("%2f", "/").Replace("%3d", "=");
    }

    public static string ShortenFileName(string fileNameWithExtension)
    {
      string withoutExtension = Path.GetFileNameWithoutExtension(fileNameWithExtension);
      int startIndex = withoutExtension.LastIndexOf('~');
      if (startIndex < 0)
        return fileNameWithExtension;
      string str = withoutExtension.Remove(startIndex);
      string ccName = withoutExtension.Substring(startIndex + 1);
      if (ccName.Length <= 27)
        return fileNameWithExtension;
      string proxyNameForName = RepositoryUtil.GetProxyNameForName(ccName);
      return Path.ChangeExtension(str + (object) '~' + proxyNameForName, Path.GetExtension(fileNameWithExtension));
    }
  }
}
