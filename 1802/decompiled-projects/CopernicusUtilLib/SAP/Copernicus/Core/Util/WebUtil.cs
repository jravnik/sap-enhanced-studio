﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.WebUtil
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using System;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Util
{
  public class WebUtil
  {
    private WebUtil()
    {
    }

    public static string BuildURL(ConnectionDataSet.SystemDataRow systemData, string path)
    {
      int num1 = systemData.HttpPort;
      StringBuilder stringBuilder = new StringBuilder("http");
      if (PropertyAccess.GeneralProps.HTTPSSupport)
      {
        try
        {
          num1 = systemData.SSLPort;
          stringBuilder.Append("s");
        }
        catch (Exception ex)
        {
          int num2 = (int) MessageBox.Show(string.Format(Resource.LogonDialog_Error_Fatal, (object) "The SSL port must be maintained for the current system, if HTTPS is activated."), Resource.LogonDialog_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Hand);
          return (string) null;
        }
      }
      stringBuilder.Append("://").Append(systemData.Host).Append(":").Append(num1);
      if (path != null)
        stringBuilder.Append(path);
      return stringBuilder.ToString();
    }

    public static LogonData CheckHttpAuthentication(ConnectionDataSet.SystemDataRow systemDataRow, string user, string xrepSessionID, bool suppressDialogMessages)
    {
      try
      {
        string url = WebUtil.BuildURL(systemDataRow, Connection.getJsonURLPath());
        if (string.IsNullOrEmpty(url))
          return (LogonData) null;
        string sessionDescription = WebUtil.getXRepSessionDescription(systemDataRow.Name);
        return new LogonHandler().CheckLogon(url, user, sessionDescription, systemDataRow, xrepSessionID, suppressDialogMessages);
      }
      catch (ProtocolException ex)
      {
        return (LogonData) null;
      }
    }

    private static string getXRepSessionDescription(string ConnectionName)
    {
      return Environment.MachineName + "_" + Environment.UserName + "_" + ConnectionName;
    }

    public static WebProxy GetProxy()
    {
      WebProxy webProxy = (WebProxy) null;
      if (PropertyAccess.GeneralProps.ProxySupport)
      {
        webProxy = new WebProxy(PropertyAccess.GeneralProps.ProxyURL);
        if (PropertyAccess.GeneralProps.ProxyAuthentication)
          webProxy.Credentials = (ICredentials) new NetworkCredential(PropertyAccess.GeneralProps.ProxyUser, SecureStringUtil.decrypt(PropertyAccess.GeneralProps.ProxyPwd));
      }
      return webProxy;
    }
  }
}
