﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.ByDUIUtil
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;

namespace SAP.Copernicus.Core.Util
{
  public class ByDUIUtil
  {
    public static void LaunchLogonUI(ConnectionDataSet.SystemDataRow selectedSystem)
    {
      ExternalApplication.LaunchExternalBrowser(WebUtil.BuildURL(selectedSystem, "/sap/public/ap/ui/repository/SAP_UI/HTML5/client.html?app.component=/SAP_BYD_UI_CT/Main/root.uiccwoc&rootWindow=X&redirectUrl=/sap/public/byd/runtime"));
    }

    public static void LaunchUIComponent(string xRepPath)
    {
      ByDUIUtil.LaunchUIComponent(xRepPath, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null, (string) null);
    }

    public static void LaunchUIComponent(string xRepPath, string inportName, string importParameterName, string importParameter, string importParameterName2 = null, string importParameter2 = null, string importParameterName3 = null, string importParameter3 = null)
    {
      ConnectionDataSet.SystemDataRow connectedSystem = Connection.getInstance().getConnectedSystem();
      int client = connectedSystem.Client;
      string str1;
      if (client < 100)
      {
        str1 = "0" + (object) client;
        if (client < 10)
          str1 = "0" + str1;
      }
      else
        str1 = client.ToString();
      string str2 = "sap-client=" + str1;
      string str3 = xRepPath.Replace("/", "%2F");
      string str4 = "/sap/public/ap/ui/repository/SAP_UI/HTML5/client.html?sap-client=" + str1 + "&sap-language=" + connectedSystem.Language + "&app.component=" + str3;
      if (inportName != null)
        str4 = str4 + "&app.inport=" + inportName;
      if (importParameter != null)
        str4 = str4 + "&param." + importParameterName + "=" + importParameter;
      if (importParameter2 != null)
        str4 = str4 + "&param." + importParameterName2 + "=" + importParameter2;
      if (importParameter3 != null)
        str4 = str4 + "&param." + importParameterName3 + "=" + importParameter3;
      ExternalApplication.LaunchExternalBrowser(WebUtil.BuildURL(Connection.getInstance().getConnectedSystem(), str4 + "&redirectUrl=/sap/public/byd/runtime"));
    }
  }
}
