﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.VSPackageUtil
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel.Composition.Hosting;

namespace SAP.Copernicus.Core.Util
{
  public static class VSPackageUtil
  {
    public static CompositionContainer CompositionContainer { get; set; }

    public static IServiceProvider ServiceProvider { get; set; }

    public static bool VsTestHostInstalled { get; set; }
  }
}
