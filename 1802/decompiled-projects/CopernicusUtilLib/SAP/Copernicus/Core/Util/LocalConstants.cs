﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.LocalConstants
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Util
{
  internal class LocalConstants
  {
    internal const string URL_UI_RUNTIME_PART_1_REPOS = "/sap/public/ap/ui/repository/SAP_UI/HTML5/client.html?";
    internal const string URL_UI_RUNTIME_PART_2_Client = "sap-client=";
    internal const string URL_UI_RUNTIME_PART_4_LANG = "&sap-language=";
    internal const string URL_UI_RUNTIME_PART_5_APP = "&app.component=";
    internal const string URL_UI_RUNTIME_PART_6_INPORT = "&app.inport=";
    internal const string URL_UI_RUNTIME_PART_7_PARAMETER = "&param.";
    internal const string URL_UI_RUNTIME_PART_8_REDIRECT = "&redirectUrl=/sap/public/byd/runtime";
    internal const string URL_START_PAGE = "/sap/public/ap/ui/repository/SAP_UI/HTML5/client.html?app.component=/SAP_BYD_UI_CT/Main/root.uiccwoc&rootWindow=X&redirectUrl=/sap/public/byd/runtime";
  }
}
