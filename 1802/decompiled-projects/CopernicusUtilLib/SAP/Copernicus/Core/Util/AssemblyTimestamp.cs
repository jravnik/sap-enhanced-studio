﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.AssemblyTimestamp
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;

namespace SAP.Copernicus.Core.Util
{
  [AttributeUsage(AttributeTargets.Assembly)]
  public class AssemblyTimestamp : Attribute
  {
    private string _timeStamp = "dev";

    public string TimeStamp
    {
      get
      {
        return this._timeStamp;
      }
    }

    public AssemblyTimestamp(string timeStamp)
    {
      this._timeStamp = timeStamp;
    }
  }
}
