﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.Query
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [DebuggerStepThrough]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [XmlRoot(ElementName = "query", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [Serializable]
  public class Query
  {
    [XmlAttribute("bo")]
    public string BOProxyName { get; set; }

    [XmlAttribute("name")]
    public string QueryName { get; set; }

    [XmlAttribute("node")]
    public string QueryNode { get; set; }

    [XmlAttribute("type")]
    public string Type { get; set; }

    [XmlElement("inputStructure")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.InputStructure[] InputStructure { get; set; }

    [XmlElement("outputStructure")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.OutputStructure[] OutputStructure { get; set; }
  }
}
