﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.Conversions
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [DebuggerStepThrough]
  [XmlRoot(ElementName = "conversions", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [Serializable]
  public class Conversions
  {
    private SAP.Copernicus.CopernicusLib.Core.Sadl.Conversion[] conversionField;

    [XmlElement("conversion")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.Conversion[] Conversion
    {
      get
      {
        return this.conversionField;
      }
      set
      {
        this.conversionField = value;
      }
    }
  }
}
