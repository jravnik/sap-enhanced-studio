﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.InputParameter
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [XmlType(Namespace = "http://sap.com/sap.nw.f.sadl")]
  [DebuggerStepThrough]
  [XmlRoot(ElementName = "inputParameter", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [DesignerCategory("code")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [Serializable]
  public class InputParameter
  {
    [XmlAttribute("name")]
    public string Name { set; get; }

    [XmlAttribute("type")]
    public string Type { set; get; }

    [XmlElement(ElementName = "attribute")]
    public List<StructureAttribute> Attribute { set; get; }

    [XmlElement(ElementName = "inputParameter")]
    public List<InputParameter> InputParameterList { set; get; }
  }
}
