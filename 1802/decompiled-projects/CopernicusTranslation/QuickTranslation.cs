﻿using SAP.Copernicus.Core;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Translation.Resources;
using SAP.CopernicusProjectView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace SAP.Copernicus.Translation
{
    public class QuickTranslation : BaseForm
    {
        private List<string> _Title = new List<string>();
        private List<string[]> _DataArray = new List<string[]>();
        private List<KeyValuePair<string, string>> LangComboList = new List<KeyValuePair<string, string>>();
        private KeyValuePair<string, string> Masterlang = new KeyValuePair<string, string>("EN", "English");
        private new IContainer components;
        private GroupBox groupBox1;
        private DataGridView dataGridView;
        private GroupBox ComponentSummaryGrp;
        private Label label1;
        private Label label2;
        private Label label3;
        private ComboBox ExportLangCmbbox;
        private RichTextBox richTextBox3;
        private DataGridViewImageColumn Status;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewTextBoxColumn SourceString;
        private DataGridViewTextBoxColumn TargetString;
        private DataGridViewTextBoxColumn LoadedTargetString;
        private ZLANG_STRUC[] LanguageDesc;
        private ZLANG_STRUC[] LanguageArray;
        private List<string> XERPPathlist;
        private string exportFileName;
        private string fileName;
        private XmlDocument doc;
        private bool firstOpen;

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            DataGridViewCellStyle gridViewCellStyle1 = new DataGridViewCellStyle();
            this.groupBox1 = new GroupBox();
            this.label2 = new Label();
            this.label3 = new Label();
            this.label1 = new Label();
            this.dataGridView = new DataGridView();
            this.Status = new DataGridViewImageColumn();
            this.ID = new DataGridViewTextBoxColumn();
            this.SourceString = new DataGridViewTextBoxColumn();
            this.TargetString = new DataGridViewTextBoxColumn();
            this.LoadedTargetString = new DataGridViewTextBoxColumn();
            this.richTextBox3 = new RichTextBox();
            this.ExportLangCmbbox = new ComboBox();
            this.groupBox1.SuspendLayout();
            ((ISupportInitialize)this.dataGridView).BeginInit();
            this.SuspendLayout();
            this.groupBox1.Controls.Add((Control)this.label2);
            this.groupBox1.Controls.Add((Control)this.label3);
            this.groupBox1.Controls.Add((Control)this.ExportLangCmbbox);
            this.groupBox1.Controls.Add((Control)this.label1);
            this.groupBox1.Controls.Add((Control)this.dataGridView);
            this.groupBox1.Location = new Point(12, 75);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(618, 447);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Translatable Strings";
            this.label2.AutoSize = true;
            this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte)0);
            this.label2.Location = new Point(115, 20);
            this.label2.Name = "label2";
            this.label2.Size = new Size(41, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "English";
            this.label3.AutoSize = true;
            this.label3.Location = new Point(10, 20);
            this.label3.Name = "label3";
            this.label3.Size = new Size(95, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Source Language:";
            this.label1.AutoSize = true;
            this.label1.Location = new Point(10, 41);
            this.label1.Name = "label1";
            this.label1.Size = new Size(121, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Target Language:";
            this.ExportLangCmbbox.FormattingEnabled = true;
            this.ExportLangCmbbox.Location = new Point(115, 38);
            this.ExportLangCmbbox.Name = "ExportLangCmbbox";
            this.ExportLangCmbbox.Size = new Size(179, 21);
            this.ExportLangCmbbox.TabIndex = 0;
            this.ExportLangCmbbox.LostFocus += new EventHandler(this.ExportLangCmbbox_LostFocus);
            gridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleLeft;
            gridViewCellStyle1.BackColor = SystemColors.Control;
            gridViewCellStyle1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte)0);
            gridViewCellStyle1.ForeColor = SystemColors.WindowText;
            gridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToOrderColumns = true;
            this.dataGridView.ColumnHeadersDefaultCellStyle = gridViewCellStyle1;
            this.dataGridView.Columns.AddRange((DataGridViewColumn)this.Status, (DataGridViewColumn)this.ID, (DataGridViewColumn)this.SourceString, (DataGridViewColumn)this.TargetString, (DataGridViewColumn)this.LoadedTargetString);
            this.dataGridView.EditMode = DataGridViewEditMode.EditOnEnter;
            this.dataGridView.EnableHeadersVisualStyles = false;
            this.dataGridView.Location = new Point(13, 67);
            this.dataGridView.MaximumSize = new Size(600, 371);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView2";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.ScrollBars = ScrollBars.Vertical;
            this.dataGridView.Size = new Size(599, 370);
            this.dataGridView.TabIndex = 1;
            this.Status.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 65;
            this.ID.Visible = false;
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.SourceString.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.SourceString.HeaderText = "Source";
            this.SourceString.Name = "SourceString";
            this.SourceString.ReadOnly = true;
            this.TargetString.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.TargetString.HeaderText = "Target";
            this.TargetString.Name = "TargetString";
            this.TargetString.ReadOnly = false;
            this.LoadedTargetString.Visible = false;
            this.LoadedTargetString.HeaderText = "Target (Loaded)";
            this.LoadedTargetString.Name = "LoadedTargetString";
            this.LoadedTargetString.ReadOnly = true;
            this.richTextBox3.BackColor = SystemColors.Control;
            this.richTextBox3.BorderStyle = BorderStyle.None;
            this.richTextBox3.Location = new Point(10, 21);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new Size(594, 35);
            this.richTextBox3.TabIndex = 7;
            this.richTextBox3.Text = "This overview displays the translatable text strings as items and the translated text strings for a set language.";
            this.buttonOk.Location = new Point(468, 529);
            this.buttonCancel.Location = new Point(550, 529);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(660, 600);
            this.Controls.Add((Control)this.groupBox1);
            this.MinimumSize = new Size(660, 600);
            this.Size = new Size(660, 600);
            this.Name = nameof(CheckTranslation);
            this.StartPosition = FormStartPosition.CenterParent;
            this.Subtitle = "This overview displays the translatable text strings as items and the translated text strings for a set language.";
            this.Text = "Quick Translation";
            this.Load += new EventHandler(this.CheckTranslation_Load);
            this.Controls.SetChildIndex((Control)this.groupBox1, 0);
            this.Controls.SetChildIndex((Control)this.buttonOk, 0);
            this.Controls.SetChildIndex((Control)this.buttonCancel, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((ISupportInitialize)this.dataGridView).EndInit();
            this.ResumeLayout(false);
        }

        public string ExportFileName
        {
            get
            {
                return this.exportFileName;
            }
            set
            {
                this.exportFileName = value;
            }
        }

        public string FileName
        {
            get
            {
                return this.fileName;
            }
            set
            {
                this.fileName = value;
            }
        }

        public QuickTranslation(List<string> xRepPathlist)
          : base(HELP_IDS.BDS_TRANSLATION_CHECK)
        {
            this.InitializeComponent();
            this.firstOpen = true;
            bool flag = false;
            this.ExportLangCmbbox.SelectedIndexChanged += new EventHandler(this.ExportLangCmbbox_SelectedIndexChanged);
            this.LanguageArray = new TranslationHandler().getLanguage();
            if (this.LanguageArray == null)
                return;
            if (this.LanguageArray.Length > 0)
            {
                this.ExportLangCmbbox.Items.Clear();
                int num1 = 0;
                int num2 = 0;
                foreach (ZLANG_STRUC language in this.LanguageArray)
                {
                    if (language.LANGUAGE.ToString() != "EN")
                    {
                        flag = true;
                        this.LangComboList.Add(new KeyValuePair<string, string>(language.LANGUAGE, language.DESCRIPTION));
                        if (language.LANGUAGE.ToString() == "DE")
                            num2 = num1;
                        ++num1;
                    }
                }
                if (flag)
                {
                    this.ExportLangCmbbox.DataSource = (object)null;
                    this.ExportLangCmbbox.DataSource = (object)new BindingSource((object)this.LangComboList, (string)null);
                    this.firstOpen = true;
                    this.ExportLangCmbbox.ValueMember = "Key";
                    this.ExportLangCmbbox.DisplayMember = "Value";
                    this.ExportLangCmbbox.SelectedIndex = num2;
                }
                else
                {
                    int num3 = (int)CopernicusMessageBox.Show(TranslationResource.MsgLangError, "Missing Languages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            else
            {
                int num = (int)CopernicusMessageBox.Show(TranslationResource.MsgLangError, "Missing Languages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

            TranslationHandler translationHandler = new TranslationHandler();
            this.LanguageDesc = translationHandler.getLanguage();
            if (this.LanguageDesc == null)
            {
                int num = (int)CopernicusMessageBox.Show(TranslationResource.MsgLangError, "Languages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                this.XERPPathlist = xRepPathlist;
                this.fillDataGridview();
            }
        }

        private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView.RowCount && e.ColumnIndex == 3)
            {
                DataGridViewRow row = this.dataGridView.Rows[e.RowIndex];

                if (row != null)
                {
                    object targetString = row.Cells["TargetString"].Value;

                    if (targetString != null)
                    {
                        object loadedTargetString = row.Cells["LoadedTargetString"].Value;

                        if (loadedTargetString == null)
                        {
                            row.Cells["Status"].Value = (object)ActionIcons.YellowOK;
                            row.Cells["Status"].ToolTipText = "String's translation was changed";
                        }
                        else
                        {
                            if (loadedTargetString.ToString() != targetString.ToString())
                            {
                                row.Cells["Status"].Value = (object)ActionIcons.YellowOK;
                                row.Cells["Status"].ToolTipText = "String's translation was changed";
                            }
                        }
                    }
                }
            }
        }

        private void fillDataGridview()
        {
            string text1 = this.ExportLangCmbbox.Text;
            string text2 = this.FileName + "_DE.xlf";
            string text3 = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string str = Path.Combine(text3, text2);
            TranslationHandler translationHandler = new TranslationHandler();
            string language1 = "DE";
            if (this.LanguageArray != null)
            {
                foreach (ZLANG_STRUC language2 in this.LanguageArray)
                {
                    if (language2.DESCRIPTION.Equals(text1))
                        language1 = language2.LANGUAGE;
                }
            }
            string xmlsstring;
            if (translationHandler.ExportTextHandler(this.XERPPathlist.ToArray(), language1, str, "X", out xmlsstring))
            {
                this.doc = new XmlDocument();
                this.doc.LoadXml(xmlsstring);

                XmlNodeList transUnits = this.doc.GetElementsByTagName("trans-unit");
                int i = 0;

                foreach (XmlNode transUnit in transUnits)
                {
                    if (transUnit.HasChildNodes)
                    {
                        this.dataGridView.Rows.Add();

                        bool translated = false;

                        string id = "";
                        string resname = "";
                        string restype = "";

                        if (transUnit.Attributes != null)
                        {
                            var attribute = transUnit.Attributes["id"];
                            if (attribute != null)
                            {
                                id = attribute.Value;
                            }

                            attribute = transUnit.Attributes["resname"];
                            if (attribute != null)
                            {
                                resname = attribute.Value;
                            }

                            attribute = transUnit.Attributes["restype"];
                            if (attribute != null)
                            {
                                restype = attribute.Value;
                            }
                        }

                        foreach (XmlNode child in transUnit.ChildNodes)
                        {
                            if (child.Name == "source")
                            {
                                this.dataGridView.Rows[i].Cells["SourceString"].Value = child.InnerText;
                                this.dataGridView.Rows[i].Cells["SourceString"].ToolTipText = String.Format("ID: {0}\nName: {1}\nType: {2}", id, resname, restype);
                                this.dataGridView.Rows[i].Cells["ID"].Value = id;
                            }
                            else if (child.Name == "target")
                            {
                                this.dataGridView.Rows[i].Cells["TargetString"].Value = child.InnerText;
                                this.dataGridView.Rows[i].Cells["LoadedTargetString"].Value = child.InnerText;
                                translated = true;
                            }
                        }

                        if (translated)
                        {
                            this.dataGridView.Rows[i].Cells["Status"].Value = (object)ActionIcons.OK;
                            this.dataGridView.Rows[i].Cells["Status"].ToolTipText = "String is translated";
                        }
                        else
                        {
                            this.dataGridView.Rows[i].Cells["Status"].Value = (object)ActionIcons.Error;
                            this.dataGridView.Rows[i].Cells["Status"].ToolTipText = "String is not translated";
                        }

                        i++;
                    }
                }

                this.dataGridView.CellValueChanged += new DataGridViewCellEventHandler(this.DataGridView_CellValueChanged);
                this.dataGridView.Sort(this.dataGridView.Columns["SourceString"], ListSortDirection.Ascending);
            }
            else
            {
                CopernicusStatusBar.Instance.ShowMessage(TranslationResource.MsgExpErr);
                this.Close();
            }
        }

        private void CheckTranslation_Load(object sender, EventArgs e)
        {
            this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;
        }

        protected override void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            XmlNodeList transUnits = this.doc.GetElementsByTagName("trans-unit");

            foreach (XmlNode transUnit in transUnits)
            {
                if (transUnit.HasChildNodes)
                {
                    string id = "";

                    if (transUnit.Attributes != null)
                    {
                        var attribute = transUnit.Attributes["id"];
                        if (attribute != null)
                        {
                            id = attribute.Value;
                        }
                    }

                    DataGridViewRow row = null;

                    for (int i = 0; i < this.dataGridView.Rows.Count; i++)
                    {
                        if (this.dataGridView.Rows[i].Cells["ID"].Value.ToString() == id)
                        {
                            row = this.dataGridView.Rows[i];
                            break;
                        }
                    }

                    if (row == null)
                    {
                        continue;
                    }

                    bool translated = false;

                    foreach (XmlNode child in transUnit.ChildNodes)
                    {
                        if (child.Name == "target")
                        {
                            child.InnerText = row.Cells["TargetString"].Value.ToString();
                            translated = true;
                        }
                    }

                    if (!translated)
                    {
                        String targetText = "";
                        DataGridViewCell targetCell = row.Cells["TargetString"];

                        if (targetCell != null)
                        {
                            if (targetCell.Value != null)
                            {
                                targetText = targetCell.Value.ToString();
                            }
                        }

                        transUnit.InnerXml += ("<target>" + targetText + "</target>");
                    }
                }
            }

            string text = BeautifyXML(this.doc);
            bool flag = false;

            if (string.IsNullOrEmpty(text))
            {
                int num1 = (int)CopernicusMessageBox.Show(TranslationResource.MsgImpErr + " XLF is empty or null", "Import error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                flag = new TranslationHandler().ImportTextHandler(this.XERPPathlist.ToArray(), Convert.ToBase64String(Encoding.UTF8.GetBytes(text)));
                this.Visible = false;
                if (flag)
                {
                    CopernicusStatusBar.Instance.ShowMessage(TranslationResource.MsgImpSuccess);
                }
                this.Close();
            }
        }

        private string BeautifyXML(XmlDocument xml)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                xml.Save(writer);
            }
            return sb.ToString();
        }

        private void ExportLangCmbbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.firstOpen)
            {
                this.firstOpen = false;
                return;
            }

            this.dataGridView.Rows.Clear();
            
            string text1 = this.ExportLangCmbbox.Text;
            string text2 = this.FileName + "_" + this.LangComboList[this.ExportLangCmbbox.SelectedIndex].Key + ".xlf";
            string text3 = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string str = Path.Combine(text3, text2);
            TranslationHandler translationHandler = new TranslationHandler();
            string language1 = "DE";
            if (this.LanguageArray != null)
            {
                foreach (ZLANG_STRUC language2 in this.LanguageArray)
                {
                    if (language2.DESCRIPTION.Equals(text1))
                        language1 = language2.LANGUAGE;
                }
            }
            string xmlsstring;
            if (translationHandler.ExportTextHandler(this.XERPPathlist.ToArray(), language1, str, "X", out xmlsstring))
            {
                this.doc = new XmlDocument();
                this.doc.LoadXml(xmlsstring);

                XmlNodeList transUnits = this.doc.GetElementsByTagName("trans-unit");
                int i = 0;

                foreach (XmlNode transUnit in transUnits)
                {
                    if (transUnit.HasChildNodes)
                    {
                        this.dataGridView.Rows.Add();

                        bool translated = false;

                        string id = "";
                        string resname = "";
                        string restype = "";

                        if (transUnit.Attributes != null)
                        {
                            var attribute = transUnit.Attributes["id"];
                            if (attribute != null)
                            {
                                id = attribute.Value;
                            }

                            attribute = transUnit.Attributes["resname"];
                            if (attribute != null)
                            {
                                resname = attribute.Value;
                            }

                            attribute = transUnit.Attributes["restype"];
                            if (attribute != null)
                            {
                                restype = attribute.Value;
                            }
                        }

                        foreach (XmlNode child in transUnit.ChildNodes)
                        {
                            if (child.Name == "source")
                            {
                                this.dataGridView.Rows[i].Cells["SourceString"].Value = child.InnerText;
                                this.dataGridView.Rows[i].Cells["SourceString"].ToolTipText = String.Format("ID: {0}\nName: {1}\nType: {2}", id, resname, restype);
                                this.dataGridView.Rows[i].Cells["ID"].Value = id;
                            }
                            else if (child.Name == "target")
                            {
                                this.dataGridView.Rows[i].Cells["TargetString"].Value = child.InnerText;
                                this.dataGridView.Rows[i].Cells["LoadedTargetString"].Value = child.InnerText;
                                translated = true;
                            }
                        }

                        if (translated)
                        {
                            this.dataGridView.Rows[i].Cells["Status"].Value = (object)ActionIcons.OK;
                            this.dataGridView.Rows[i].Cells["Status"].ToolTipText = "String is translated";
                        }
                        else
                        {
                            this.dataGridView.Rows[i].Cells["Status"].Value = (object)ActionIcons.Error;
                            this.dataGridView.Rows[i].Cells["Status"].ToolTipText = "String is not translated";
                        }

                        i++;
                    }
                }

                this.dataGridView.ClearSelection();
                this.dataGridView.Sort(this.dataGridView.Columns["SourceString"], ListSortDirection.Ascending);
            }
            else
            {
                CopernicusStatusBar.Instance.ShowMessage(TranslationResource.MsgExpErr);
                this.Close();
            }
        }

        private void ExportLangCmbbox_LostFocus(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            bool flag = false;
            if (comboBox.SelectedItem != null)
            {
                KeyValuePair<string, string> selectedItem = (KeyValuePair<string, string>)comboBox.SelectedItem;
                foreach (ZLANG_STRUC language in this.LanguageArray)
                {
                    if (selectedItem.Key == language.LANGUAGE)
                    {
                        flag = true;
                        if (selectedItem.Value.ToUpper() != language.DESCRIPTION.ToUpper())
                        {
                            int num = (int)CopernicusMessageBox.Show(TranslationResource.MsgExpLangInvalid, "Export error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            this.ExportLangCmbbox.ResetText();
                            return;
                        }
                    }
                }
                if (flag)
                    return;
                int num1 = (int)CopernicusMessageBox.Show(TranslationResource.MsgExpLangInvalid, "Export error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.ExportLangCmbbox.ResetText();
            }
            else
            {
                int num = (int)CopernicusMessageBox.Show(TranslationResource.MsgExpLangInvalid, "Export error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.ExportLangCmbbox.ResetText();
            }
        }
    }
}