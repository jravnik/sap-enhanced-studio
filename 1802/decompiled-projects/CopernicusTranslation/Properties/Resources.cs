﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Translation.Properties.Resources
// Assembly: CopernicusTranslation, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 9D6A50C9-F2FE-4B16-A45E-C445F353581B
// Assembly location: G:\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusTranslation.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus.Translation.Properties
{
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) SAP.Copernicus.Translation.Properties.Resources.resourceMan, (object) null))
          SAP.Copernicus.Translation.Properties.Resources.resourceMan = new ResourceManager("SAP.Copernicus.Translation.Properties.Resources", typeof (SAP.Copernicus.Translation.Properties.Resources).Assembly);
        return SAP.Copernicus.Translation.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return SAP.Copernicus.Translation.Properties.Resources.resourceCulture;
      }
      set
      {
        SAP.Copernicus.Translation.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
