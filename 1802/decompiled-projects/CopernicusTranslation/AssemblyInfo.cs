﻿using SAP.Copernicus.Core.Util;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("© 2011 SAP AG. All rights reserved.")]
[assembly: AssemblyTrademark("SAP and Business ByDesign are trademark(s) or registered trademark(s) of SAP AG in Germany and in several other countries.")]
[assembly: AssemblyTitle("CopernicusTranslation")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("SAP AG")]
[assembly: AssemblyProduct("SAP® Business ByDesign™")]
[assembly: AssemblyCSN("AP-RC-BDS-TRANSL")]
[assembly: ComVisible(false)]
[assembly: Guid("b63c1117-a02d-48e1-8f2e-d883b884ee83")]
[assembly: AssemblyFileVersion("142.0.3319.0231")]
[assembly: AssemblyVersion("142.0.3319.231")]
