﻿// Decompiled with JetBrains decompiler
// Type: SHDocVw.IShellUIHelper4
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SHDocVw
{
  [CompilerGenerated]
  [Guid("B36E6A53-8073-499E-824C-D776330A333E")]
  [TypeIdentifier]
  [ComImport]
  public interface IShellUIHelper4 : IShellUIHelper3, IShellUIHelper2, IShellUIHelper
  {
  }
}
