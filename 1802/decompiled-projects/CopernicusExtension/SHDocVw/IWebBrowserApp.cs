﻿// Decompiled with JetBrains decompiler
// Type: SHDocVw.IWebBrowserApp
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SHDocVw
{
  [CompilerGenerated]
  [Guid("0002DF05-0000-0000-C000-000000000046")]
  [TypeIdentifier]
  [ComImport]
  public interface IWebBrowserApp : IWebBrowser
  {
  }
}
