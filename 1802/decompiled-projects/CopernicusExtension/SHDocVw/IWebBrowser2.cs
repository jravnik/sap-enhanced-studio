﻿// Decompiled with JetBrains decompiler
// Type: SHDocVw.IWebBrowser2
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SHDocVw
{
  [CompilerGenerated]
  [TypeIdentifier]
  [Guid("D30C1661-CDAF-11D0-8A3E-00C04FC9E26E")]
  [ComImport]
  public interface IWebBrowser2 : IWebBrowserApp, IWebBrowser
  {
    [SpecialName]
    void _VtblGap1_11();

    object Document { [DispId(203)] [return: MarshalAs(UnmanagedType.IDispatch)] get; }
  }
}
