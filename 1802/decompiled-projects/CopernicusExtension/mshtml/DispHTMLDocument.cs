﻿// Decompiled with JetBrains decompiler
// Type: mshtml.DispHTMLDocument
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace mshtml
{
  [TypeIdentifier]
  [CompilerGenerated]
  [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
  [Guid("3050F55F-98B5-11CF-BB82-00AA00BDCE0B")]
  [ComImport]
  public interface DispHTMLDocument
  {
    IHTMLElementCollection forms { [DispId(1010), MethodImpl(MethodImplOptions.PreserveSig)] [return: MarshalAs(UnmanagedType.Interface)] get; }

    string title { [DispId(1012), MethodImpl(MethodImplOptions.PreserveSig)] [param: MarshalAs(UnmanagedType.BStr)] set; [DispId(1012), MethodImpl(MethodImplOptions.PreserveSig)] [return: MarshalAs(UnmanagedType.BStr)] get; }

    string readyState { [DispId(1018), MethodImpl(MethodImplOptions.PreserveSig)] [return: MarshalAs(UnmanagedType.BStr)] get; }

    [DispId(1067)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.Interface)]
    IHTMLElement createElement([MarshalAs(UnmanagedType.BStr), In] string eTag);

    IHTMLElement documentElement { [DispId(1075), MethodImpl(MethodImplOptions.PreserveSig)] [return: MarshalAs(UnmanagedType.Interface)] get; }

    [DispId(1088)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.Interface)]
    IHTMLElement getElementById([MarshalAs(UnmanagedType.BStr), In] string v);

    [DispId(1087)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.Interface)]
    IHTMLElementCollection getElementsByTagName([MarshalAs(UnmanagedType.BStr), In] string v);
  }
}
