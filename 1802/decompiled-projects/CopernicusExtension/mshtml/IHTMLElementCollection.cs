﻿// Decompiled with JetBrains decompiler
// Type: mshtml.IHTMLElementCollection
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace mshtml
{
  [CompilerGenerated]
  [TypeIdentifier]
  [Guid("3050F21F-98B5-11CF-BB82-00AA00BDCE0B")]
  [ComImport]
  public interface IHTMLElementCollection : IEnumerable
  {
    [SpecialName]
    void _VtblGap1_4();

    [DispId(0)]
    [return: MarshalAs(UnmanagedType.IDispatch)]
    object item([MarshalAs(UnmanagedType.Struct), In, Optional] object name, [MarshalAs(UnmanagedType.Struct), In, Optional] object index);
  }
}
