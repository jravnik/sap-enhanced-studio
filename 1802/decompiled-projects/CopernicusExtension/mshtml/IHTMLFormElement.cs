﻿// Decompiled with JetBrains decompiler
// Type: mshtml.IHTMLFormElement
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace mshtml
{
  [TypeIdentifier]
  [Guid("3050F1F7-98B5-11CF-BB82-00AA00BDCE0B")]
  [CompilerGenerated]
  [ComImport]
  public interface IHTMLFormElement : IEnumerable
  {
    [SpecialName]
    void _VtblGap1_17();

    [DispId(1009)]
    void submit();
  }
}
