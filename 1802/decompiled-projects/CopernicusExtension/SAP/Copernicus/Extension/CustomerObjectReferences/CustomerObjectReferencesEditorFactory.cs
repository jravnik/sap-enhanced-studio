﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.CustomerObjectReferences.CustomerObjectReferencesEditorFactory
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.CustomEditor;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.CustomerObjectReferences
{
  [Guid("97AAA94D-D4AA-4A53-82BF-34C051CD69FA")]
  public sealed class CustomerObjectReferencesEditorFactory : CustomEditorFactory<CustomerObjectReferencesEditorPane>
  {
  }
}
