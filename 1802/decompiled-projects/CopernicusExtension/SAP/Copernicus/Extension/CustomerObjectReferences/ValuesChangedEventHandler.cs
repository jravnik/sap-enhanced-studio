﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.CustomerObjectReferences.ValuesChangedEventHandler
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System;

namespace SAP.Copernicus.Extension.CustomerObjectReferences
{
  public delegate void ValuesChangedEventHandler(object sender, EventArgs e);
}
