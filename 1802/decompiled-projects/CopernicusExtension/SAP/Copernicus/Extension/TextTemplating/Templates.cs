﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.TextTemplating.Templates
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.TextTemplating;
using System.IO;
using System.Reflection;
using System.Text;

namespace SAP.Copernicus.Extension.TextTemplating
{
  public class Templates : ITemplateProvider
  {
    private readonly Assembly assembly;
    private static Templates templates;

    public static Templates Instance
    {
      get
      {
        if (Templates.templates == null)
          Templates.templates = new Templates();
        return Templates.templates;
      }
    }

    private Templates()
    {
      this.assembly = Assembly.GetExecutingAssembly();
    }

    public bool ReadTemplate(string requestFileName, out string content, out string location)
    {
      content = string.Empty;
      location = requestFileName;
      try
      {
        using (Stream manifestResourceStream = this.assembly.GetManifestResourceStream("SAP.Copernicus.Extension.TextTemplating." + requestFileName))
        {
          StreamReader streamReader = new StreamReader(manifestResourceStream, Encoding.UTF8);
          content = streamReader.ReadToEnd();
          return true;
        }
      }
      catch (IOException ex)
      {
        return false;
      }
    }
  }
}
