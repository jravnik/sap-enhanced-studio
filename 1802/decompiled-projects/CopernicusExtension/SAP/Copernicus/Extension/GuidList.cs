﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.GuidList
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System;

namespace SAP.Copernicus.Extension
{
  internal static class GuidList
  {
    public static readonly Guid guidCopernicusExtensionCmdSet = new Guid("c6e571a2-9113-41fd-a76f-f9f2657f4440");
    public const string guidCopernicusExtensionPkgString = "dd925b8f-54d3-4a44-970a-d290e3703b68";
    public const string guidCopernicusExtensionCmdSetString = "c6e571a2-9113-41fd-a76f-f9f2657f4440";
    public const string guidXBOLanguageServiceString = "7BA2E2BF-5676-4c04-B10C-B91797889F23";
    public const string guidCopernicusExtScenarioEditorFactory = "B5F78390-A49E-4DAC-B39C-16ED105EB4DB";
    public const string guidCopernicusNodeExtScenarioEditorFactory = "91E76BDD-C112-4996-AE02-36BDB447B053";
    public const string guidCustomerObjectReferencesEditorFactory = "97AAA94D-D4AA-4A53-82BF-34C051CD69FA";
  }
}
