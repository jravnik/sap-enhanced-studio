﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.UIDesignerHelper.UIComponentLoaderForUIDesigner
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.uidesigner.integration.model;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;

namespace SAP.Copernicus.Extension.UIDesignerHelper
{
  public class UIComponentLoaderForUIDesigner
  {
    public static void OpenUIFloorPlan_UIDesigner(string strXRepPathforUIComponent, bool isSAPUI = false)
    {
      bool flag = false;
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      if (InteractionWithUIDesigner.IsUIDesignerNotExist())
      {
        Connection instance = Connection.getInstance();
        ConnectionDataSet.SystemDataRow connectedSystem = instance.getConnectedSystem();
        bool httpsSupport = PropertyAccess.GeneralProps.HTTPSSupport;
        string strHost = (httpsSupport ? "https://" : "http://") + connectedSystem.Host;
        string strPort = httpsSupport ? connectedSystem.SSLPort.ToString() : connectedSystem.HttpPort.ToString();
        int client = connectedSystem.Client;
        string str;
        if (client < 100)
        {
          str = "0" + (object) client;
          if (client < 10)
            str = "0" + str;
        }
        else
          str = string.Concat((object) client);
        string user = connectedSystem.User;
        string xrepSessionId = instance.GetXRepSessionID();
        string strABAPNamespace = "";
        string strSolution = "";
        string strTopLevelFolder = "";
        string language = connectedSystem.Language;
        string strHTTPNamespace = "";
        string strPackage = "";
        string EV_TRKORR = "";
        if (!isSAPUI)
        {
          if (XRepMapper.GetInstance().GetSolutionEditStatus() == XRepMapper.EditModes.Editable)
            flag = true;
          strABAPNamespace = selectedProject.GetCopernicusProjectProperties().RuntimeNamespacePrefix;
          strSolution = selectedProject.GetCopernicusProjectProperties().XRepSolution;
          strTopLevelFolder = selectedProject.GetCopernicusProjectProperties().RepositoryRootFolder;
          strHTTPNamespace = selectedProject.GetCopernicusProjectProperties().RepositoryNamespace;
          strPackage = selectedProject.GetCopernicusProjectProperties().DevelopmentPackage;
          new ExtensionHandler().GetTransportRequest(CopernicusProjectSystemUtil.getSelectedProject().Projectlabel, out EV_TRKORR);
        }
        if (InteractionWithUIDesigner.LoadUIDesignerAsyn(strHost, strPort, str, str, user, SecureStringUtil.secureStringToString(connectedSystem.SecurePassword), strXRepPathforUIComponent, flag, strTopLevelFolder, strSolution, strHTTPNamespace, strABAPNamespace, strPackage, EV_TRKORR, language, xrepSessionId))
          return;
        int num = (int) CopernicusMessageBox.Show(string.Format(SAP.Copernicus.Extension.Resources.UIDesignerLoadError));
      }
      else if (InteractionWithUIDesigner.IsUIDesignerLoadDone())
      {
        if (InteractionWithUIDesigner.OpenAnotherUIComponent(strXRepPathforUIComponent, flag))
          return;
        int num = (int) CopernicusMessageBox.Show(string.Format(SAP.Copernicus.Extension.Resources.UIDesignerOpenUIError));
      }
      else
      {
        int num1 = (int) CopernicusMessageBox.Show(string.Format(SAP.Copernicus.Extension.Resources.UIDesignerLoad));
      }
    }
  }
}
