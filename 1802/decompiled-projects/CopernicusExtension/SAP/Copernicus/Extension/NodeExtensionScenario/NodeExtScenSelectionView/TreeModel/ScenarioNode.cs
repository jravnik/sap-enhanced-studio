﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.NodeExtensionScenario.NodeExtScenSelectionView.TreeModel.ScenarioNode
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.NodeExtensionScenario.NodeExtScenSelectionView.TreeModel
{
  public class ScenarioNode : BaseNode
  {
    private int inb_idx = 108;
    private int outb_idx = 109;
    private int inb_mig_idx = 104;
    private int out_mig_idx = 106;
    private int inb_mixed_idx = 105;
    private int out_mixed_idx = 107;
    private int proc_idx = 110;
    private int ico_idx = 110;
    private string outb = "B2B_oub";
    private string inb = "B2B_inb";
    private string scen = "prc_scen";
    private string scenType = "prc_scen";
    private string inb_mig = "B2B_inb_mig";
    private string out_mig = "B2B_oub_mig";
    private string inb_mixed = "B2B_inb_mixed";
    private string out_mixed = "B2B_oub_mixed";
    private string nsName;
    private string scenID;
    private bool isB2B;

    public ScenarioNode(string scName, string scenarioID, string scTooltip, bool isB2B, string B2B_IO)
      : base(scName)
    {
      this.nsName = scName;
      this.Text = this.nsName;
      this.scenID = scenarioID;
      this.ToolTipText = scTooltip;
      this.isB2B = isB2B;
      if (!this.isB2B)
        this.ico_idx = this.proc_idx;
      else if (B2B_IO == "INBOUND")
      {
        this.ico_idx = this.inb_idx;
        this.scenType = this.inb;
      }
      else if (B2B_IO == "OUTBOUND")
      {
        this.ico_idx = this.outb_idx;
        this.scenType = this.outb;
      }
      else if (B2B_IO == "INBOUND_EXTEXP_MIG")
      {
        this.ico_idx = this.inb_mixed_idx;
        this.scenType = this.inb_mixed;
      }
      else if (B2B_IO == "INBOUND_MIG")
      {
        this.ico_idx = this.inb_mig_idx;
        this.scenType = this.inb_mig;
      }
      else if (B2B_IO == "OUTBOUND_EXTEXP_MIG")
      {
        this.ico_idx = this.out_mixed_idx;
        this.scenType = this.out_mixed;
      }
      else if (B2B_IO == "OUTBOUND_MIG")
      {
        this.ico_idx = this.out_mig_idx;
        this.scenType = this.out_mig;
      }
      this.ImageIndex = this.ico_idx;
      this.SelectedImageIndex = this.ico_idx;
    }

    public new string getData()
    {
      return this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.Namespace;
    }

    public override string Namespace
    {
      get
      {
        return this.nsName;
      }
    }

    public override void refresh()
    {
    }

    public bool IsB2B
    {
      get
      {
        return this.isB2B;
      }
    }

    public string scenario_name
    {
      get
      {
        return this.scenID;
      }
    }

    public string getNodeScenarioType
    {
      get
      {
        return this.scenType;
      }
    }
  }
}
