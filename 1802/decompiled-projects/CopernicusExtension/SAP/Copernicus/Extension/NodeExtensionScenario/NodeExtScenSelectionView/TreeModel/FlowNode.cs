﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.NodeExtensionScenario.NodeExtScenSelectionView.TreeModel.FlowNode
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.NodeExtensionScenario.NodeExtScenSelectionView.TreeModel
{
  public class FlowNode : BaseNode
  {
    private int flow_idx = 10;
    private string flow = "scen_flow";
    private string nsName;

    public FlowNode(string scName, string scTooltip)
      : base(scName)
    {
      this.nsName = scName;
      this.Text = this.nsName;
      this.ToolTipText = scTooltip;
      this.ImageIndex = this.flow_idx;
      this.SelectedImageIndex = this.flow_idx;
    }

    public new string getData()
    {
      return this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.Namespace;
    }

    public override string Namespace
    {
      get
      {
        return this.nsName;
      }
    }

    public string getNodeScenarioType
    {
      get
      {
        return this.flow;
      }
    }

    public override void refresh()
    {
    }
  }
}
