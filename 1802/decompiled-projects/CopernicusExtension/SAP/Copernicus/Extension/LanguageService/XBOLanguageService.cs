﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.LanguageService.XBOLanguageService
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Antlr.Runtime;
using Antlr.Runtime.Tree;
using EnvDTE80;
using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.BOCompiler.Common;
using SAP.Copernicus.BOCompiler.Common.Annotations;
using SAP.Copernicus.BusinessObjectLanguage;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.Extension.Parser;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.LanguageService
{
  [Guid("7BA2E2BF-5676-4c04-B10C-B91797889F23")]
  internal class XBOLanguageService : AbstractLanguageService
  {
    protected static XBOLanguageService instance = new XBOLanguageService();
    private static BODLBackendCompilerHandler bbcHandler = new BODLBackendCompilerHandler();
    private readonly RepositoryDataCache cache = RepositoryDataCache.GetInstance();
    private const string XREP_XBO_DEFAULT_PATH = "SRC";
    protected static string actualPath;
    public bool parserErrors;
    private BOParser parser;

    public static XBOLanguageService GetInstance()
    {
      return XBOLanguageService.instance;
    }

    protected override bool XBODL
    {
      get
      {
        return true;
      }
    }

    public override string Name
    {
      get
      {
        return "Business Object Ext.";
      }
    }

    public override string GetFormatFilterList()
    {
      return "Business Object Extension File (*.xbo)\n*.xbo";
    }

    public override void StartTreeParser(BOParser parser, CommonTree ast, AbstractLanguageService.callingContext callingContext, ParseRequest req)
    {
      bool excludeBoList = true;
      string refBoName = (string) null;
      string refNodeName = (string) null;
      string fieldName = (string) null;
      XBOWalker xboWalker = new XBOWalker(parser, ast, callingContext);
      this.parser = parser;
      ExtensionDataCache.GetInstance().Reset(excludeBoList);
      xboWalker.WalkAST();
      string boNamespace = parser.BONamespace;
      string boName1 = parser.BOName;
      XBOHeader xboHeader = ExtensionDataCache.GetInstance().getXBOHeader(parser.ErrorHandler.FileName);
      if (xboHeader == null)
        return;
      string str = xboHeader.getNamespace();
      string boName2 = xboHeader.getBoName();
      if (str != null && boName2 != null && str != boNamespace || boName2 != boName1)
      {
        string message = string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_BONAME_OR_NAMSPACE_CHANGED, (object) str, (object) boName2);
        parser.ErrorHandler.AddError(message, BOErrorLevel.MIN_LEVEL);
      }
      if (req != null && req.FileName != null && req.FileName != string.Empty)
      {
        if (!ExtensionDataCache.GetInstance().check("http://sap.com/xi/" + str.Replace(".", "/"), boName2, ((IEnumerable<string>) req.FileName.Split(new string[1]{ "\\" }, StringSplitOptions.None)).Last<string>(), out refBoName, out refNodeName, out fieldName))
        {
          string message = string.Format(SAP.Copernicus.Extension.Resources.CrossXBOError, (object) fieldName, (object) refBoName, (object) refNodeName);
          parser.ErrorHandler.AddError(message, BOErrorLevel.MIN_LEVEL);
        }
      }
      this.parserErrors = xboWalker.checkForParserErrors();
    }

    public override void SetNodeCompletionList(out List<BODeclaration> NodeNames, string boNamespace, string boName)
    {
      List<string> boNodeNames = ExtensionDataCache.GetInstance().GetBONodeNames(boNamespace, boName);
      NodeNames = this.getNodeNamesDeclarationList(boNodeNames);
    }

    public override List<BODeclaration> AdjustSMTGCompletionList(List<BODeclaration> systemMessageTypeList)
    {
      List<BODeclaration> boDeclarationList = new List<BODeclaration>();
      foreach (BODeclaration systemMessageType in systemMessageTypeList)
      {
        if (systemMessageType.Glyph == 8)
          boDeclarationList.Add(systemMessageType);
      }
      return boDeclarationList;
    }

    public override List<BODeclaration> GetCodeCompletionListForElementTypes(BOCodeCompletionParser codeCompletionParser)
    {
      List<BODeclaration> boDeclarationList = new List<BODeclaration>();
      string namespaceForFile = ProjectUtil.GetProjectNamespaceForFile(codeCompletionParser.ErrorHandler.FileName);
      List<string> nameSpaces = new List<string>((IEnumerable<string>) codeCompletionParser.Imports);
      if (!nameSpaces.Contains(namespaceForFile))
        nameSpaces.Add(namespaceForFile);
      foreach (XBODataType xboDataType in this.getDataTypesByNameSpace(nameSpaces))
      {
        if (!this.useBBC || !(xboDataType.DisplayText == "LANGUAGEINDEPENDENT_EXTENDED_Text"))
          boDeclarationList.Add(new BODeclaration()
          {
            Description = xboDataType.Description,
            DisplayText = xboDataType.DisplayText,
            Glyph = xboDataType.Glyph,
            Name = xboDataType.Name
          });
      }
      return boDeclarationList;
    }

    public override List<BODeclaration> GetCodeCompletionListForElements(BOCodeCompletionParser codeCompletionParser, string boNamespace, string boName)
    {
      List<BODeclaration> boDeclarationList = new List<BODeclaration>();
      List<PDI_EXT_S_EXT_FIELDS> activatedFields = ExtensionDataCache.GetInstance().getActivatedFields(boNamespace, boName);
      AnnotatedElement currentScope = codeCompletionParser.CurrentScope;
      string str = currentScope.LanguageElementName == boName ? "Root" : currentScope.LanguageElementName;
      foreach (PDI_EXT_S_EXT_FIELDS pdiExtSExtFields in activatedFields)
      {
        if (pdiExtSExtFields.FIELD_TYPE_CAT == "DEF" && (!(pdiExtSExtFields.BO_ESR_NAME == boName) || !(pdiExtSExtFields.NODE_ESR_NAME == str)))
          boDeclarationList.Add(new BODeclaration()
          {
            Description = SAP.Copernicus.Extension.Resources.ExtendedBO + ": " + pdiExtSExtFields.BO_ESR_NAME + "; " + SAP.Copernicus.Extension.Resources.ExtendedNode + ": " + pdiExtSExtFields.NODE_ESR_NAME,
            DisplayText = pdiExtSExtFields.FIELD_ESR_NAME,
            Glyph = 3,
            Name = pdiExtSExtFields.FIELD_ESR_NAME
          });
      }
      return boDeclarationList;
    }

    public override List<BODeclaration> GetCodeCompletionListForDataTypeDefaultValueElements(BOCodeCompletionParser codeCompletionParser)
    {
      int k1 = -3;
      List<BODeclaration> boDeclarationList = (List<BODeclaration>) null;
      if (codeCompletionParser.LT(-1).Type == 35)
      {
        int k2 = 0;
        int num = 0;
        do
        {
          --k2;
          if (codeCompletionParser.LT(k2).Type == 35)
            ++num;
        }
        while (codeCompletionParser.LT(k2).Type != 53);
        k1 = -(num * 4 + 3);
      }
      IToken token = codeCompletionParser.LT(k1);
      if (!(token.Text == "Text"))
        boDeclarationList = codeCompletionParser.BOResolver.FindDTElements(token.Text);
      return boDeclarationList;
    }

    private List<BODeclaration> getNodeNamesDeclarationList(List<string> names)
    {
      RepositoryDataCache.GetInstance();
      List<BODeclaration> boDeclarationList = new List<BODeclaration>();
      foreach (string name in names)
      {
        if (name != "Root")
          boDeclarationList.Add(new BODeclaration(name, name, 10, name));
      }
      return boDeclarationList;
    }

    public static void GetBOInfoAttribs(string xrepPath, out string esrBONamespace, out string esrBOName)
    {
      string prxMaxBOName = (string) null;
      string prxBOName;
      XBOLanguageService.GetBOInfoAttribs(xrepPath, out esrBONamespace, out esrBOName, out prxBOName, out prxMaxBOName);
    }

    public static void GetBOInfoAttribs(string xrepPath, out string esrBONamespace, out string esrBOName, out string prxBOName, out string prxMaxBOName)
    {
      IDictionary<string, string> attribs = (IDictionary<string, string>) null;
      XRepHandler xrepHandler = new XRepHandler();
      string content = (string) null;
      xrepHandler.Read(xrepPath, out content, out attribs);
      foreach (string key1 in attribs.Keys.Where<string>((Func<string, bool>) (n =>
      {
        if (!n.StartsWith("~"))
          return n.Contains("~");
        return false;
      })).ToList<string>())
      {
        string key2 = key1.Split('~')[0];
        attribs.Add(key2, attribs[key1]);
        attribs.Remove(key1);
      }
      if (!attribs.TryGetValue("esrBONameSpace", out esrBONamespace))
        esrBONamespace = string.Empty;
      if (!attribs.TryGetValue(nameof (esrBOName), out esrBOName))
        esrBOName = string.Empty;
      if (!attribs.TryGetValue("PRX_BO_NAME", out prxBOName))
        prxBOName = string.Empty;
      if (attribs.TryGetValue("PRX_MAX_BO", out prxMaxBOName))
        return;
      prxMaxBOName = string.Empty;
    }

    public List<XBODataType> getDataTypesByNameSpace(List<string> nameSpaces)
    {
      List<XBODataType> xboDataTypeList = new List<XBODataType>();
      FieldTypeCategory fieldTypeCategory = FieldTypeCategory.Default;
      if (nameSpaces.Contains(ExtensionDataCache.extDataTypeDefeaultNameSpace))
      {
        foreach (ExtensionFieldType extFieldType in ExtensionDataCache.GetInstance().ExtFieldTypes)
        {
          if (extFieldType.Name != "Identifier")
          {
            if (extFieldType.ExtensibilityName == "TEXT" || extFieldType.ExtensibilityName == "SHORT_TEXT" || (extFieldType.ExtensibilityName == "MED_TEXT" || extFieldType.ExtensibilityName == "LONG_TEXT") || (extFieldType.ExtensibilityName == "DEC" || extFieldType.ExtensibilityName == "LANGUAGEINDEPENDENT_EXTENDED_Text"))
              xboDataTypeList.Add(new XBODataType(extFieldType.Name, extFieldType.Name, 15, extFieldType.Name, "", "", extFieldType.ExtensibilityName, FieldTypeCategory.Default));
            else
              xboDataTypeList.Add(new XBODataType(extFieldType.Name + " in " + extFieldType.EsrNameSpace, extFieldType.Name, 15, extFieldType.EsrName, extFieldType.EsrNameSpace, extFieldType.ProxyName, extFieldType.ExtensibilityName, FieldTypeCategory.Default));
          }
        }
      }
      if (!nameSpaces.Contains("http://sap.com/xi/AP/PDI/ABSL"))
        nameSpaces.Add("http://sap.com/xi/AP/PDI/ABSL");
      foreach (string nameSpace in nameSpaces)
      {
        foreach (RepositoryDataSet.DataTypesRow queryDataType in this.cache.QueryDataTypes(nameSpace, (string) null, (string) null, (string) null, (string) null, (string) null))
        {
          if (queryDataType.RepresentationTerm == "4" && nameSpace != "http://sap.com/xi/AP/PDI/ABSL" || queryDataType.RepresentationTerm == "10" && nameSpace != "http://sap.com/xi/AP/PDI/ABSL" || queryDataType.Name == "ID" && nameSpace == "http://sap.com/xi/AP/PDI/ABSL")
          {
            switch (queryDataType.RepresentationTerm)
            {
              case "4":
                fieldTypeCategory = FieldTypeCategory.CodeList;
                break;
              case "10":
                fieldTypeCategory = FieldTypeCategory.Identifier;
                break;
            }
            if (queryDataType.UsageCategory != null)
            {
              if (!BOResolver.ForbiddenDTUsageCategories.Contains<string>(queryDataType.UsageCategory))
              {
                string str = !(nameSpace == "http://sap.com/xi/AP/PDI/ABSL") ? nameSpace : "ABSL";
                xboDataTypeList.Add(new XBODataType(queryDataType.Name + " in " + str, queryDataType.Name, 15, queryDataType.Name, queryDataType.NSName, queryDataType.ProxyName, fieldTypeCategory));
              }
            }
            else
              xboDataTypeList.Add(new XBODataType(queryDataType.Name + " in " + nameSpace, queryDataType.Name, 15, queryDataType.Name, queryDataType.NSName, queryDataType.ProxyName, fieldTypeCategory));
          }
        }
      }
      return xboDataTypeList;
    }

    public bool checkDataTypeIsValid(List<string> nameSpaces, string typeName, out XBODataType dataTypeDetails)
    {
      ExtensionFieldType extTypeInfo = new ExtensionFieldType();
      if (ExtensionDataCache.GetInstance().checkTypeIsValid(typeName, out extTypeInfo))
      {
        if (extTypeInfo.ExtensibilityName == "TEXT" || extTypeInfo.ExtensibilityName == "DEC")
        {
          dataTypeDetails.Description = "";
          dataTypeDetails.NameSpace = "";
        }
        else
        {
          dataTypeDetails.NameSpace = extTypeInfo.EsrNameSpace;
          dataTypeDetails.Description = extTypeInfo.Name + " in " + extTypeInfo.EsrNameSpace;
        }
        dataTypeDetails.Name = extTypeInfo.EsrName;
        dataTypeDetails.DisplayText = extTypeInfo.Name;
        dataTypeDetails.Glyph = 15;
        dataTypeDetails.ProxyName = extTypeInfo.ProxyName;
        dataTypeDetails.ExtFieldType = extTypeInfo.ExtensibilityName;
        dataTypeDetails.FieldTypeCategory = FieldTypeCategory.Default;
        return true;
      }
      foreach (XBODataType xboDataType in this.getDataTypesByNameSpace(nameSpaces))
      {
        if (typeName == xboDataType.Name)
        {
          dataTypeDetails = xboDataType;
          return true;
        }
      }
      dataTypeDetails = new XBODataType();
      return false;
    }

    public bool checkDataTypeIsValid(List<string> nameSpaces, string typeName)
    {
      XBODataType dataTypeDetails = new XBODataType();
      return this.checkDataTypeIsValid(nameSpaces, typeName, out dataTypeDetails);
    }

    private CopernicusProjectNode GetProjectForNamespace(string namespaceName)
    {
      Solution2 solution = DTEUtil.GetDTE().Solution as Solution2;
      if (solution == null || !solution.IsOpen)
        return (CopernicusProjectNode) null;
      foreach (EnvDTE.Project project in solution.Projects)
      {
        CopernicusProjectNode copernicusProjectNode = project.Object as CopernicusProjectNode;
        if (copernicusProjectNode != null && copernicusProjectNode.RepositoryNamespace == namespaceName)
          return copernicusProjectNode;
      }
      return (CopernicusProjectNode) null;
    }

    public override void UpdateLanguageContext(LanguageContextHint hint, IVsTextLines buffer, TextSpan[] ptsSelection, IVsUserContext context)
    {
      if (hint != LanguageContextHint.LCH_F1_HELP)
        return;
      context.RemoveAttribute("keyword", (string) null);
      context.AddAttribute(VSUSERCONTEXTATTRIBUTEUSAGE.VSUC_Usage_LookupF1, "keyword", HELP_IDS.BDS_BOEXTENSION_EDITOR);
    }
  }
}
