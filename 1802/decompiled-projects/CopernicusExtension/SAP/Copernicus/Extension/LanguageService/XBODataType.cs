﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.LanguageService.XBODataType
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.LanguageService
{
  public struct XBODataType
  {
    public string Description;
    public string DisplayText;
    public int Glyph;
    public string Name;
    public string NameSpace;
    public string ProxyName;
    public string ExtFieldType;
    public FieldTypeCategory FieldTypeCategory;

    public XBODataType(string description, string displayText, int glyph, string name, string nameSpace, string proxyName, string extFieldType, FieldTypeCategory fieldTypeCategory)
    {
      this.Description = description;
      this.DisplayText = displayText;
      this.Glyph = glyph;
      this.Name = name;
      this.NameSpace = nameSpace;
      this.ProxyName = proxyName;
      this.ExtFieldType = extFieldType;
      this.FieldTypeCategory = fieldTypeCategory;
    }

    public XBODataType(string description, string displayText, int glyph, string name, string nameSpace, string proxyName, FieldTypeCategory fieldTypeCategory)
    {
      this.Description = description;
      this.DisplayText = displayText;
      this.Glyph = glyph;
      this.Name = name;
      this.NameSpace = nameSpace;
      this.ProxyName = proxyName;
      this.ExtFieldType = "";
      this.FieldTypeCategory = fieldTypeCategory;
    }
  }
}
