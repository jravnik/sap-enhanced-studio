﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.SelectionUtil.BOSelectionWizard
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.TemplateWizard;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Core.Wizard;
using SAP.Copernicus.Extension.TextTemplating;
using SAP.Copernicus.TextTemplating;
using SAP.CopernicusProjectView;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.SelectionUtil
{
  public class BOSelectionWizard : CopernicusWizardSheet, IWizard
  {
    private static string strXboItem = "$rootname$";
    private static readonly string[] CONFLICTING_EXTENSIONS = new string[3]{ ".bo", ".xbo", ".enht" };
    private string strTemplatepath = "";
    private string strProNamespace;
    private string strXboname;
    private bool confictingFileExits;

    public BOSelectionWizard()
    {
      this.InitializeComponent();
      this.strProNamespace = CopernicusProjectSystemUtil.getSelectedProject().RepositoryNamespace;
      this.Pages.Add((object) new BOSelectionFirstPage(this));
      this.helpid = HELP_IDS.BDS_BOEXTENSION_CREATE;
    }

    public void FinishWizard(string BONamespace, string BOName, List<PDI_EXT_S_EXT_NODE> nodeList, bool OfflineEnabled)
    {
      Cursor.Current = Cursors.WaitCursor;
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      Dictionary<string, string> properties = new Dictionary<string, string>();
      properties.Add("solutionNS", this.strProNamespace);
      properties.Add("fileName", this.strXboname);
      properties.Add("selectBOName", BOName);
      properties.Add("selectNamespace", BONamespace);
      if (RepositoryDataCache.GetInstance().GetProxyNameForQualifiedDTName("AP.Common.GDT", "UUID") == null)
        properties.Add("defaultNamespace", "BASIS.Global");
      else
        properties.Add("defaultNamespace", "AP.Common.GDT");
      int count = nodeList.Count;
      bool flag = ExtensionDataCache.GetInstance().IsRootNodeExtensible(BONamespace, BOName);
      string str1;
      string parentNodeName;
      if (flag)
      {
        str1 = (count - 1).ToString();
        parentNodeName = "";
      }
      else
      {
        str1 = count.ToString();
        if (count > 0)
        {
          parentNodeName = nodeList[0].PARENT_NODE_NAME;
        }
        else
        {
          PDI_EXT_S_EXT_NODE pdiExtSExtNode = new PDI_EXT_S_EXT_NODE();
          PDI_EXT_S_EXT_NODE rootNode = ExtensionDataCache.GetInstance().GetRootNode(BONamespace, BOName);
          nodeList.Add(rootNode);
          parentNodeName = nodeList[0].PARENT_NODE_NAME;
          count = nodeList.Count;
        }
      }
      properties.Add("numberOfExtensionNodes", str1);
      if (count > 0)
      {
        string prxBoName = nodeList[0].PRX_BO_NAME;
        List<ListElement> orderedList = new List<ListElement>();
        int seq = 1;
        this.prepareList(parentNodeName, nodeList, orderedList, seq);
        if (flag)
          orderedList.RemoveAt(0);
        int num = 1;
        foreach (ListElement listElement in orderedList)
        {
          string key1 = "node_" + (object) num + "_name";
          properties.Add(key1, listElement.nodeName);
          string key2 = "node_" + (object) num + "_name_esf";
          properties.Add(key2, listElement.nodeName_ESF);
          string key3 = "node_" + (object) num + "_level";
          properties.Add(key3, listElement.level);
          string key4 = "node_" + (object) num + "_parentName";
          properties.Add(key4, listElement.parentNodeName);
          ++num;
        }
        CompilerErrorCollection errors;
        string fileContent = new TextTemplateHelper((ITemplateProvider) Templates.Instance).ProcessTemplate_XBO(this.strTemplatepath, properties, out errors);
        HierarchyNode parentNode = CopernicusProjectSystemUtil.getSelectedNode();
        while (!(parentNode is ProjectNode) && !(parentNode is FolderNode))
          parentNode = parentNode.Parent;
        string str2 = "";
        foreach (PDI_EXT_S_EXT_NODE node in nodeList)
        {
          if (!node.MAX_BO_PRX_NAME.Equals(""))
          {
            str2 = node.MAX_BO_PRX_NAME;
            break;
          }
        }
        IDictionary<string, string> fileAttributesToBeSaved = (IDictionary<string, string>) new Dictionary<string, string>();
        fileAttributesToBeSaved.Add("Offline", OfflineEnabled.ToString());
        fileAttributesToBeSaved.Add("esrBOName", BOName);
        fileAttributesToBeSaved.Add("PRX_BO_NAME", prxBoName);
        fileAttributesToBeSaved.Add("esrBONameSpace", BONamespace);
        fileAttributesToBeSaved.Add(nameof (BONamespace), "http://sap.com/xi/" + BONamespace.Replace(".", "/"));
        if (!str2.Equals(""))
          fileAttributesToBeSaved.Add("PRX_MAX_BO", str2);
        string str3 = DateTime.Now.ToString().Replace(".", string.Empty).Replace(":", string.Empty).Replace("/", string.Empty).Replace(" ", string.Empty).Replace("-", string.Empty);
        fileAttributesToBeSaved.Add("EXF_NAME", selectedProject.RuntimeNamespacePrefix + "/" + str3);
        Cursor.Current = Cursors.Default;
        FileNode fileNode = (parentNode.ProjectMgr as CopernicusProjectNode).AddFileNode(this.strXboname + ".xbo", fileContent, fileAttributesToBeSaved, parentNode, Encoding.UTF8, false);
        CopernicusProjectSystemUtil.openScriptEditor(selectedProject, fileNode.ID);
      }
      else
      {
        int num1 = (int) MessageBox.Show(SAP.Copernicus.Extension.Resources.BOSelectionNoExtNodes);
      }
    }

    void IWizard.BeforeOpeningFile(ProjectItem projectItem)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.ProjectFinishedGenerating(EnvDTE.Project project)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.ProjectItemFinishedGenerating(ProjectItem projectItem)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.RunFinished()
    {
      if (this.confictingFileExits)
        return;
      int num = (int) this.ShowDialog();
    }

    void IWizard.RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
    {
      this.strXboname = replacementsDictionary[BOSelectionWizard.strXboItem];
      this.strXboname = this.strXboname.Substring(0, this.strXboname.LastIndexOf("."));
      if (this.strXboname == null || !RepositoryUtil.IsValidNameKeyName(this.strXboname))
      {
        int num = (int) MessageBox.Show(SAP.CopernicusProjectView.Resources.MsgInvalidBOName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        this.confictingFileExits = true;
      }
      else
      {
        DTE2 dtE2 = automationObject as DTE2;
        if (dtE2 != null && dtE2.SelectedItems.Count == 1)
        {
          SelectedItem selectedItem = dtE2.SelectedItems.Item((object) 1);
          CopernicusProjectNode copernicusProjectNode = (CopernicusProjectNode) null;
          if (selectedItem.Project != null)
            copernicusProjectNode = selectedItem.Project.Object as CopernicusProjectNode;
          else if (selectedItem.ProjectItem != null)
            copernicusProjectNode = selectedItem.ProjectItem.ContainingProject.Object as CopernicusProjectNode;
          if (copernicusProjectNode != null)
          {
            HierarchyNode nodeByName = (HierarchyNode) copernicusProjectNode.FindNodeByName((HierarchyNode) copernicusProjectNode, this.strXboname, (IEnumerable<string>) BOSelectionWizard.CONFLICTING_EXTENSIONS, true);
            if (nodeByName != null)
            {
              BusinessObjectNamingCheck.NameAllreadyUsedCheck(Path.GetFileNameWithoutExtension(nodeByName.Caption), true, BusinessObjectSubType.ALL);
              this.confictingFileExits = true;
              return;
            }
          }
        }
        this.confictingFileExits = false;
      }
    }

    bool IWizard.ShouldAddProjectItem(string filePath)
    {
      this.strTemplatepath = filePath;
      return false;
    }

    private void InitializeComponent()
    {
      this.buttonPanel.SuspendLayout();
      this.SuspendLayout();
      this.nextButton.Click += new EventHandler(this.nextButton_Click);
      this.finishButton.Click += new EventHandler(this.finishButton_Click);
      this.AcceptButton = (IButtonControl) this.nextButton;
      this.ClientSize = new Size(384, 141);
      this.Name = nameof (BOSelectionWizard);
      this.Text = "Business Object Selection";
      this.buttonPanel.ResumeLayout(false);
      this.buttonPanel.PerformLayout();
      this.ResumeLayout(false);
    }

    private void nextButton_Click(object sender, EventArgs e)
    {
    }

    private void prepareList(string parentNodeName, List<PDI_EXT_S_EXT_NODE> nodeList, List<ListElement> orderedList, int seq)
    {
      foreach (PDI_EXT_S_EXT_NODE node in nodeList)
      {
        if (node.PARENT_NODE_NAME == parentNodeName)
        {
          orderedList.Add(new ListElement()
          {
            Seq = seq,
            level = !(node.HI_LEVEL == "") ? node.HI_LEVEL : "0",
            nodeName_ESF = node.ND_NAME,
            nodeName = node.PRX_ND_NAME,
            parentNodeName = node.PARENT_NODE_NAME,
            prxNdName = node.PRX_ND_NAME
          });
          ++seq;
          this.prepareList(node.PRX_ND_NAME, nodeList, orderedList, seq);
        }
      }
    }

    private void finishButton_Click(object sender, EventArgs e)
    {
    }
  }
}
