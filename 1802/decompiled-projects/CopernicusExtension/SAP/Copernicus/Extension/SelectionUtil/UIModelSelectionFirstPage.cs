﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.SelectionUtil.UIModelSelectionFirstPage
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Wizard;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.SelectionUtil
{
    public class UIModelSelectionFirstPage : InternalWizardPage
    {
        private List<string> SelectedFloorPlans = new List<string>();
        private readonly string projectName;
        private readonly string projectNamespace;
        private readonly string FileName;
        private string errorText;
        private Label lblError;
        private Label lblNamespace;
        private Label lblBOName;
        private TextBox txtBoxNamespace;
        private TextBox txtBoxBOname;
        private GroupBox grpBoxFloorplans;
        private UIModelSelectionWizard parentWizard;
        private string selUIPath;
        private PDI_EXT_S_REF_FLD[] UICompList;

        private void InitializeComponent()
        {
            this.lblNamespace = new Label();
            this.lblBOName = new Label();
            this.txtBoxNamespace = new TextBox();
            this.txtBoxBOname = new TextBox();
            this.grpBoxFloorplans = new GroupBox();
            this.grpBoxFloorplans.SuspendLayout();
            this.SuspendLayout();
            this.Banner.Size = new Size(587, 64);
            this.Banner.Subtitle = string.Format(SAP.Copernicus.Extension.Resources.UIModelSelectionSubtitle);
            this.Banner.Title = string.Format(SAP.Copernicus.Extension.Resources.UIModelSelectionTitle);
            this.lblNamespace.AutoSize = true;
            this.lblNamespace.Location = new Point(22, 85);
            this.lblNamespace.Name = "lblNamespace";
            this.lblNamespace.Size = new Size(67, 13);
            this.lblNamespace.TabIndex = 1;
            this.lblNamespace.Text = "Namespace:";
            this.lblBOName.AutoSize = true;
            this.lblBOName.Location = new Point(22, 118);
            this.lblBOName.Name = "lblBOName";
            this.lblBOName.Size = new Size(86, 13);
            this.lblBOName.TabIndex = 2;
            this.lblBOName.Text = "Business Object:";
            this.txtBoxNamespace.BackColor = SystemColors.Control;
            this.txtBoxNamespace.Location = new Point(131, 78);
            this.txtBoxNamespace.Name = "txtBoxNamespace";
            this.txtBoxNamespace.Size = new Size(415, 20);
            this.txtBoxNamespace.TabIndex = 3;
            this.txtBoxBOname.BackColor = SystemColors.Control;
            this.txtBoxBOname.Location = new Point(131, 111);
            this.txtBoxBOname.Name = "txtBoxBOname";
            this.txtBoxBOname.Size = new Size(415, 20);
            this.txtBoxBOname.TabIndex = 8;
            this.grpBoxFloorplans.Location = new Point(25, 164);
            this.grpBoxFloorplans.Name = "grpBoxFloorplans";
            this.grpBoxFloorplans.Size = new Size(521, 360);
            this.grpBoxFloorplans.TabIndex = 6;
            this.grpBoxFloorplans.TabStop = false;
            this.grpBoxFloorplans.Text = "Floor Plans:";
            this.Controls.Add((Control)this.lblNamespace);
            this.Controls.Add((Control)this.grpBoxFloorplans);
            this.Controls.Add((Control)this.lblBOName);
            this.Controls.Add((Control)this.txtBoxBOname);
            this.Controls.Add((Control)this.txtBoxNamespace);
            this.MaximumSize = new Size(700, 700);
            this.Name = nameof(UIModelSelectionFirstPage);
            this.Size = new Size(587, 550);
            this.SetActive += new CancelEventHandler(this.UIModelSelectionFirstPage_SetActive);
            this.WizardBack += new WizardPageEventHandler(this.UIModelSelectionFirstPage_WizardBack);
            this.WizardFinish += new CancelEventHandler(this.UIModelSelectionFirstPage_WizardFinish);
            this.Controls.SetChildIndex((Control)this.txtBoxNamespace, 0);
            this.Controls.SetChildIndex((Control)this.txtBoxBOname, 0);
            this.Controls.SetChildIndex((Control)this.lblBOName, 0);
            this.Controls.SetChildIndex((Control)this.grpBoxFloorplans, 0);
            this.Controls.SetChildIndex((Control)this.lblNamespace, 0);
            this.Controls.SetChildIndex((Control)this.Banner, 0);
            this.grpBoxFloorplans.ResumeLayout(false);
            this.grpBoxFloorplans.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        public UIModelSelectionFirstPage(UIModelSelectionWizard p1)
          : base(HELP_IDS.BDS_BOEXTENSION_CREATE)
        {
            this.projectName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
            this.projectNamespace = CopernicusProjectSystemUtil.getSelectedProject().RepositoryNamespace;
            this.FileName = CopernicusProjectSystemUtil.GetXRepPathforSelectedNode().Split(new string[1]
            {
        "SRC/"
            }, StringSplitOptions.None)[1];
            this.parentWizard = p1;
            this.InitializeComponent();
            this.addCheckBox();
        }

        private void addCheckBox()
        {
            ExtensionHandler extensionHandler = new ExtensionHandler();
            Cursor.Current = Cursors.WaitCursor;
            this.UICompList = extensionHandler.GetUIComponents(this.FileName, this.projectName, this.projectNamespace, out this.errorText);
            if (this.errorText != null)
            {
                this.lblError = new Label();
                this.lblError.Location = new Point(17, 30);
                this.lblError.Size = new Size(485, 45);
                this.grpBoxFloorplans.Controls.Add((Control)this.lblError);
                this.lblError.Text = this.errorText;
                this.lblError.ForeColor = Color.Red;
                this.grpBoxFloorplans.Size = new Size(521, 80);
                this.Size = new Size(587, 350);
            }
            else
            {
                CheckBox[] checkBoxArray = new CheckBox[this.UICompList.Length];
                this.UICompList = this.UICompList.OrderBy(uiComp => uiComp.UI_MODEL_PATH.Split('.')[1]).ThenBy(uiComp => uiComp.UI_MODEL_PATH.Remove(0, uiComp.UI_MODEL_PATH.LastIndexOf('/'))).ToArray();

                int index = 0;
                int indexType = 0;
                int countType = 0;
                int xOffset = 0;
                string lastType = "";
                bool secondColumn = false;

                for (int i = 0; i < this.UICompList.Length; i++)
                {
                    PDI_EXT_S_REF_FLD uiComp = this.UICompList[i];
                    string currentType = uiComp.UI_MODEL_PATH.Split('.')[1];

                    if (!secondColumn && i >= this.UICompList.Length / 2 && lastType != currentType)
                    {
                        secondColumn = true;
                        index = 0;
                        indexType = 0;
                        lastType = "";
                    }

                    if (secondColumn)
                    {
                        xOffset = 17 + 304 + 20;
                    }
                    else
                    {
                        xOffset = 17;
                    }

                    if (lastType != currentType)
                    {
                        Label label = new Label();
                        label.Text = getTypeDescription(currentType) + " (" + currentType + ")";
                        label.Font = new Font(label.Font, FontStyle.Bold);
                        label.Location = new Point(xOffset, 30 + index * 25 + indexType * 25 + 6);
                        label.Size = new Size(304, 20);
                        this.grpBoxFloorplans.Controls.Add((Control)label);

                        countType++;
                        indexType++;
                        lastType = currentType;
                    }

                    checkBoxArray[i] = new CheckBox();
                    checkBoxArray[i].Text = this.splitPath(uiComp.UI_MODEL_PATH);
                    checkBoxArray[i].Location = new Point(xOffset + 20, 30 + index * 25 + indexType * 25);
                    checkBoxArray[i].AutoCheck = true;
                    checkBoxArray[i].CheckedChanged += new EventHandler(this.FloorplanType_Changed);
                    checkBoxArray[i].Size = new Size(304, 24);

                    this.grpBoxFloorplans.Controls.Add((Control)checkBoxArray[i]);
                    index++;

                    if (uiComp.BO_ESR_NAME != null)
                    {
                        this.txtBoxNamespace.Text = uiComp.BO_ESR_NAMESPACE;
                        this.txtBoxBOname.Text = uiComp.BO_ESR_NAME;
                    }
                }

                if (index > this.UICompList.Length / 2)
                {
                    this.grpBoxFloorplans.Size = new Size(690, 50 + (index + 1) * 25 + countType * 25);
                    this.Size = new Size(742, 250 + (index + 1) * 25 + countType * 25 + 6);
                }
                else
                {
                    this.grpBoxFloorplans.Size = new Size(690, 50 + (this.UICompList.Length - index + 1) * 25 + countType * 25);
                    this.Size = new Size(742, 250 + (this.UICompList.Length - index + 1) * 25 + countType * 25 + 6);
                }

                if (this.Size.Height > 860)
                {
                    int vertScrollWidth = SystemInformation.VerticalScrollBarWidth;
                    this.Size = new Size(742 + vertScrollWidth, 860);
                    this.grpBoxFloorplans.Padding = new Padding(0, 0, vertScrollWidth, 0);
                    this.Padding = new Padding(0, 0, vertScrollWidth, 0);
                }
            }

            Cursor.Current = Cursors.WaitCursor;
        }

        private void FloorplanType_Changed(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            foreach (PDI_EXT_S_REF_FLD uiComp in this.UICompList)
            {
                if (this.splitPath(uiComp.UI_MODEL_PATH) == checkBox.Text)
                {
                    if (checkBox.Checked)
                        this.SelectedFloorPlans.Add(uiComp.UI_MODEL_PATH);
                    else
                        this.SelectedFloorPlans.Remove(uiComp.UI_MODEL_PATH);
                }
            }
        }

        private void addRadioButtons()
        {
            this.UICompList = new ExtensionHandler().GetUIComponents(this.FileName, this.projectName, this.projectNamespace, out this.errorText);
            if (this.errorText != null)
            {
                this.lblError = new Label();
                this.lblError.Location = new Point(17, 30);
                this.lblError.Size = new Size(270, 13);
                this.grpBoxFloorplans.Controls.Add((Control)this.lblError);
                this.lblError.Text = this.errorText;
                this.lblError.ForeColor = Color.Red;
                this.grpBoxFloorplans.Size = new Size(521, 80);
                this.Size = new Size(587, 350);
            }
            else
            {
                RadioButton[] radioButtonArray = new RadioButton[this.UICompList.Length];
                int index = 0;
                foreach (PDI_EXT_S_REF_FLD uiComp in this.UICompList)
                {
                    radioButtonArray[index] = new RadioButton();
                    radioButtonArray[index].Text = this.splitPath(uiComp.UI_MODEL_PATH);
                    radioButtonArray[index].Location = new Point(17, 30 + index * 25);
                    radioButtonArray[index].AutoCheck = true;
                    if (index == 0)
                    {
                        radioButtonArray[index].Checked = true;
                        this.selUIPath = uiComp.UI_MODEL_PATH;
                    }
                    else
                        radioButtonArray[index].Checked = false;
                    radioButtonArray[index].CheckedChanged += new EventHandler(this.rBut_CheckedChanged);
                    radioButtonArray[index].Size = new Size(304, 24);
                    this.grpBoxFloorplans.Controls.Add((Control)radioButtonArray[index]);
                    ++index;
                    this.grpBoxFloorplans.Size = new Size(521, 50 + index * 25);
                    this.Size = new Size(587, 250 + index * 25);
                    if (uiComp.BO_ESR_NAME != null)
                    {
                        this.txtBoxNamespace.Text = uiComp.BO_ESR_NAMESPACE.Replace("http://sap.com/xi/", "").Replace("/", ".");
                        this.txtBoxBOname.Text = uiComp.BO_ESR_NAME;
                    }
                }
            }
        }

        private void rBut_CheckedChanged(object sender, EventArgs e)
        {
            if (!(sender is RadioButton))
                return;
            RadioButton radioButton = sender as RadioButton;
            if (!radioButton.Checked)
                return;
            foreach (PDI_EXT_S_REF_FLD uiComp in this.UICompList)
            {
                if (this.splitPath(uiComp.UI_MODEL_PATH) == radioButton.Text)
                    this.selUIPath = uiComp.UI_MODEL_PATH;
            }
        }

        private void UIModelSelectionFirstPage_SetActive(object sender, CancelEventArgs e)
        {
            this.SetWizardButtons(WizardPageType.FirstAndLast);
            if (this.errorText == null)
                return;
            this.EnableFinishButton(false);
        }

        private void UIModelSelectionFirstPage_WizardBack(object sender, CopernicusWizardPageEventArgs e)
        {
        }

        private void UIModelSelectionFirstPage_WizardFinish(object sender, CancelEventArgs e)
        {
            this.parentWizard.FinishWizard(this.SelectedFloorPlans);
        }

        private string splitPath(string path)
        {
            string[] strArray1 = path.Split('/');
            int num = ((IEnumerable<string>)strArray1).Count<string>();
            if (num <= 0)
                return path;
            int index = num - 1;
            string[] strArray2 = strArray1[index].Replace(".uicomponent", "").Split('.');
            return strArray2[0];
        }

        private string getTypeDescription(string type)
        {
            string str = "n.n.";

            switch (type)
            {
                case "OIF":
                    str = "Object Instance Floorplan";
                    break;
                case "QAF":
                    str = "Quick Activity Floorplan";
                    break;
                case "GAF":
                    str = "Guided Activity Floorplan";
                    break;
                case "FS":
                    str = "Fact Sheet";
                    break;
                case "OWL":
                    str = "Object Work List";
                    break;
                case "OVS":
                    str = "Object Value Selector";
                    break;
                case "QV":
                    str = "Quick View";
                    break;
                case "QC":
                    str = "Quick Create";
                    break;
                case "QA":
                    str = "Quick Activity";
                    break;
                case "TI":
                    str = "Thing Inspector";
                    break;
                case "EC":
                    str = "Embedded Component";
                    break;
                case "MD":
                    str = "Modal Dialog";
                    break;
            }

            return str;
        }
    }
}
