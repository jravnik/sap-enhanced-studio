﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.SelectionUtil.FormExtFieldsSelectionFirstPage
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Core.Wizard;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.SelectionUtil
{
  internal class FormExtFieldsSelectionFirstPage : InternalWizardPage
  {
    private string noFormExists = string.Empty;
    private KutFieldMetaInfo fieldInfo = new KutFieldMetaInfo();
    private readonly string projectNamespace;
    private readonly string projectName;
    private readonly string FileName;
    private readonly string solution;
    private Label lblError;
    private string BOName;
    private string errorText;
    private Label HeaderLine;
    private string NoNodes;
    private int AddCount;
    private GroupBox grpBoxCopyForm;
    private Button btncopyForm;
    private Label lblCopyForm;
    private GroupBox grpBoxExtFields;
    private FormExtFieldsSelectionWizard parentWizard;
    private PDI_EXT_S_EXT_FORM_FIELDS[] FormFieldList;
    private string maxBusinessObjectName;
    private string maxBusinessObjectNamespace;

    private void InitializeComponent()
    {
      this.grpBoxExtFields = new GroupBox();
      this.grpBoxCopyForm = new GroupBox();
      this.lblCopyForm = new Label();
      this.btncopyForm = new Button();
      this.grpBoxCopyForm.SuspendLayout();
      this.SuspendLayout();
      this.Banner.Size = new Size(587, 64);
      this.Banner.Subtitle = string.Format(SAP.Copernicus.Extension.Resources.FormSelectionSubtitle);
      this.Banner.Title = string.Format(SAP.Copernicus.Extension.Resources.FormSelectionTitle);
      this.grpBoxExtFields.Location = new Point(22, 160);
      this.grpBoxExtFields.Name = "grpBoxExtFields";
      this.grpBoxExtFields.Size = new Size(521, 278);
      this.grpBoxExtFields.TabIndex = 6;
      this.grpBoxExtFields.TabStop = false;
      this.grpBoxExtFields.Text = "Extension Fields:";
      this.grpBoxCopyForm.Controls.Add((Control) this.btncopyForm);
      this.grpBoxCopyForm.Controls.Add((Control) this.lblCopyForm);
      this.grpBoxCopyForm.Location = new Point(22, 71);
      this.grpBoxCopyForm.Name = "grpBoxCopyForm";
      this.grpBoxCopyForm.Size = new Size(521, 83);
      this.grpBoxCopyForm.TabIndex = 7;
      this.grpBoxCopyForm.TabStop = false;
      this.lblCopyForm.AutoSize = true;
      this.lblCopyForm.Location = new Point(7, 20);
      this.lblCopyForm.Name = "lblCopyForm";
      this.lblCopyForm.Size = new Size(264, 13);
      this.lblCopyForm.TabIndex = 0;
      this.lblCopyForm.Text = string.Format(SAP.Copernicus.Extension.Resources.FormSelectionCopyForm);
      this.btncopyForm.Location = new Point(10, 47);
      this.btncopyForm.Name = "btncopyForm";
      this.btncopyForm.Size = new Size(75, 23);
      this.btncopyForm.TabIndex = 1;
      this.btncopyForm.Text = "Copy Form";
      this.btncopyForm.UseVisualStyleBackColor = true;
      this.btncopyForm.Click += new EventHandler(this.btncopyForm_Click);
      this.Controls.Add((Control) this.grpBoxCopyForm);
      this.Controls.Add((Control) this.grpBoxExtFields);
      this.Name = nameof (FormExtFieldsSelectionFirstPage);
      this.Size = new Size(587, 533);
      this.WizardBack += new WizardPageEventHandler(this.FormExtFieldsSelectionFirstPage_WizardBack);
      this.WizardFinish += new CancelEventHandler(this.FormExtFieldsSelectionFirstPage_WizardFinish);
      this.SetActive += new CancelEventHandler(this.FormExtFieldsSelectionFirstPage_SetActive);
      this.Controls.SetChildIndex((Control) this.grpBoxExtFields, 0);
      this.Controls.SetChildIndex((Control) this.grpBoxCopyForm, 0);
      this.Controls.SetChildIndex((Control) this.Banner, 0);
      this.grpBoxCopyForm.ResumeLayout(false);
      this.grpBoxCopyForm.PerformLayout();
      this.ResumeLayout(false);
    }

    public FormExtFieldsSelectionFirstPage(FormExtFieldsSelectionWizard p1)
      : base(HELP_IDS.BDS_BOEXTENSION_CREATE)
    {
      this.projectNamespace = CopernicusProjectSystemUtil.getSelectedProject().RepositoryNamespace;
      this.FileName = CopernicusProjectSystemUtil.GetXRepPathforSelectedNode().Split(new string[1]
      {
        "SRC/"
      }, StringSplitOptions.None)[1];
      FormExtFieldsSelectionFirstPage.GetXBOInfoAttribs(CopernicusProjectSystemUtil.GetXRepPathforSelectedNode(), out this.solution);
      this.projectName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
      this.parentWizard = p1;
      this.InitializeComponent();
      this.addRadioButtons();
    }

    private void addRadioButtons()
    {
      ExtensionHandler extensionHandler = new ExtensionHandler();
      Cursor.Current = Cursors.WaitCursor;
      extensionHandler.GetFormFields(this.FileName, this.projectName, this.projectNamespace, out this.FormFieldList, out this.errorText, out this.BOName, out this.NoNodes);
      this.AddCount = (int) Convert.ToInt16(this.NoNodes);
      RadioButton[] radioButtonArray = new RadioButton[this.FormFieldList.Length + this.AddCount];
      if (this.errorText != null)
      {
        this.lblError = new Label();
        this.lblError.Location = new Point(17, 30);
        this.lblError.Size = new Size(270, 200);
        this.grpBoxExtFields.Controls.Add((Control) this.lblError);
        this.lblError.Text = this.errorText;
        this.lblError.ForeColor = Color.Red;
        this.grpBoxExtFields.Size = new Size(521, 80);
        this.Size = new Size(587, 350);
      }
      else
      {
        string str = (string) null;
        int index = 0;
        int num = 0;
        bool flag = true;
        foreach (PDI_EXT_S_EXT_FORM_FIELDS formField in this.FormFieldList)
        {
          if (str != formField.UI_TEXT_BO_NODE)
          {
            if (flag)
              flag = false;
            else
              num += 2;
            this.HeaderLine = new Label();
            this.HeaderLine.Location = new Point(17, 30 + (index + num) * 25);
            this.HeaderLine.Size = new Size(270, 13);
            this.grpBoxExtFields.Controls.Add((Control) this.HeaderLine);
            this.HeaderLine.Text = formField.UI_TEXT_BO_NODE + ":";
            this.HeaderLine.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point, (byte) 0);
            str = formField.UI_TEXT_BO_NODE;
            radioButtonArray[index] = new RadioButton();
            radioButtonArray[index].Text = formField.ESR_FIELD_NAME;
            radioButtonArray[index].Name = formField.UI_TEXT_BO_NODE + " - " + formField.ESR_FIELD_NAME;
            radioButtonArray[index].Location = new Point(17, 30 + (index + 1 + num) * 25);
            radioButtonArray[index].AutoCheck = true;
            if (index == 0)
            {
              radioButtonArray[index].Checked = true;
              this.fieldInfo.businessObjectName = formField.ESR_BO_NAME;
              this.fieldInfo.businessObjectNamespace = formField.ESR_BO_NAMESPACE;
              this.fieldInfo.businessObjectNodeName = formField.ESR_BO_NODE_NAME;
              this.fieldInfo.esrFieldName = formField.ESR_FIELD_NAME;
              this.fieldInfo.esrFieldNamespace = formField.ESR_FIELD_NAMESPACE;
              this.fieldInfo.esrFieldUiText = formField.UI_TEXT_FIELD_NAME;
              this.fieldInfo.businessObjectNodeUiText = formField.UI_TEXT_BO_NODE;
              this.fieldInfo.fieldHasReferenceBoNode = formField.FIELD_HAS_REFERENCE_BO_NODE;
              this.maxBusinessObjectName = formField.MAX_BO_ESR_NAME;
              this.maxBusinessObjectNamespace = formField.MAX_BO_ESR_NAMESPACE;
            }
            else
              radioButtonArray[index].Checked = false;
            radioButtonArray[index].CheckedChanged += new EventHandler(this.rBut_CheckedChanged);
            radioButtonArray[index].Size = new Size(304, 24);
            this.grpBoxExtFields.Controls.Add((Control) radioButtonArray[index]);
            ++index;
            this.grpBoxExtFields.Size = new Size(521, 50 + (index + 1 + num) * 25);
            this.Size = new Size(587, 200 + (index + 1 + this.AddCount + num) * 25);
          }
          else
          {
            radioButtonArray[index] = new RadioButton();
            radioButtonArray[index].Text = formField.ESR_FIELD_NAME;
            radioButtonArray[index].Name = formField.UI_TEXT_BO_NODE + " - " + formField.ESR_FIELD_NAME;
            radioButtonArray[index].Location = new Point(17, 30 + (index + 1 + num) * 25);
            radioButtonArray[index].AutoCheck = true;
            if (index == 0)
            {
              radioButtonArray[index].Checked = true;
              this.fieldInfo.businessObjectName = formField.ESR_BO_NAME;
              this.fieldInfo.businessObjectNamespace = formField.ESR_BO_NAMESPACE;
              this.fieldInfo.businessObjectNodeName = formField.ESR_BO_NODE_NAME;
              this.fieldInfo.esrFieldName = formField.ESR_FIELD_NAME;
              this.fieldInfo.esrFieldNamespace = formField.ESR_FIELD_NAMESPACE;
              this.fieldInfo.esrFieldUiText = formField.UI_TEXT_FIELD_NAME;
              this.fieldInfo.businessObjectNodeUiText = formField.UI_TEXT_BO_NODE;
              this.fieldInfo.fieldHasReferenceBoNode = formField.FIELD_HAS_REFERENCE_BO_NODE;
              this.maxBusinessObjectName = formField.MAX_BO_ESR_NAME;
              this.maxBusinessObjectNamespace = formField.MAX_BO_ESR_NAMESPACE;
            }
            else
              radioButtonArray[index].Checked = false;
            radioButtonArray[index].CheckedChanged += new EventHandler(this.rBut_CheckedChanged);
            radioButtonArray[index].Size = new Size(304, 24);
            this.grpBoxExtFields.Controls.Add((Control) radioButtonArray[index]);
            ++index;
            this.grpBoxExtFields.Size = new Size(521, 50 + (index + 1 + num) * 25);
            this.Size = new Size(587, 200 + (index + 1 + num + this.AddCount) * 25);
          }
        }
      }
      Cursor.Current = Cursors.Default;
    }

    private void rBut_CheckedChanged(object sender, EventArgs e)
    {
      if (!(sender is RadioButton))
        return;
      RadioButton radioButton = sender as RadioButton;
      if (!radioButton.Checked)
        return;
      foreach (PDI_EXT_S_EXT_FORM_FIELDS formField in this.FormFieldList)
      {
        if (formField.UI_TEXT_BO_NODE + " - " + formField.ESR_FIELD_NAME == radioButton.Name)
        {
          this.fieldInfo.businessObjectName = formField.ESR_BO_NAME;
          this.fieldInfo.businessObjectNamespace = formField.ESR_BO_NAMESPACE;
          this.fieldInfo.businessObjectNodeName = formField.ESR_BO_NODE_NAME;
          this.fieldInfo.esrFieldName = formField.ESR_FIELD_NAME;
          this.fieldInfo.esrFieldNamespace = formField.ESR_FIELD_NAMESPACE;
          this.fieldInfo.esrFieldUiText = formField.UI_TEXT_FIELD_NAME;
          this.fieldInfo.businessObjectNodeUiText = formField.UI_TEXT_BO_NODE;
          this.fieldInfo.fieldHasReferenceBoNode = formField.FIELD_HAS_REFERENCE_BO_NODE;
          this.maxBusinessObjectName = formField.MAX_BO_ESR_NAME;
          this.maxBusinessObjectNamespace = formField.MAX_BO_ESR_NAMESPACE;
          break;
        }
      }
    }

    private void FormExtFieldsSelectionFirstPage_SetActive(object sender, CancelEventArgs e)
    {
      this.SetWizardButtons(WizardPageType.FirstAndLast);
      if (this.errorText == null)
        return;
      this.EnableFinishButton(false);
    }

    private void FormExtFieldsSelectionFirstPage_WizardBack(object sender, CopernicusWizardPageEventArgs e)
    {
    }

    private void FormExtFieldsSelectionFirstPage_WizardFinish(object sender, CancelEventArgs e)
    {
      if (this.maxBusinessObjectName != null && !this.maxBusinessObjectName.Equals(string.Empty))
      {
        this.fieldInfo.businessObjectName = this.maxBusinessObjectName;
        this.fieldInfo.businessObjectNamespace = this.maxBusinessObjectNamespace;
      }
      this.parentWizard.FinishWizard(this.fieldInfo);
    }

    public static void GetXBOInfoAttribs(string xRepPath, out string Solution)
    {
      IDictionary<string, string> attribs = (IDictionary<string, string>) null;
      string content;
      new XRepHandler().Read(xRepPath, out content, out attribs);
      foreach (string key1 in attribs.Keys.Where<string>((Func<string, bool>) (n =>
      {
        if (!n.StartsWith("~"))
          return n.Contains("~");
        return false;
      })).ToList<string>())
      {
        string key2 = key1.Split('~')[0];
        attribs.Add(key2, attribs[key1]);
        attribs.Remove(key1);
      }
      if (attribs.TryGetValue("~TARGET_SOLUTION", out Solution) || attribs.TryGetValue("~SOLUTION", out Solution))
        return;
      Solution = string.Empty;
    }

    private void btncopyForm_Click(object sender, EventArgs e)
    {
      int client = Connection.getInstance().getConnectedSystem().Client;
      string str1;
      if (client < 100)
      {
        str1 = "0" + (object) client;
        if (client < 10)
          str1 = "0" + str1;
      }
      else
        str1 = string.Concat((object) client);
      string str2 = "sap-client=" + str1;
      string str3 = "/sap/public/ap/ui/repository/SAP_UI/HTML5/client.html?";
      string str4 = "&app.component=/SAP_BYD_APPLICATION_UI/itsam/appmngt/TIFormTemplateMngmt_WCView.WCVIEW.uiwocview";
      ExternalApplication.LaunchExternalBrowser(WebUtil.BuildURL(Connection.getInstance().getConnectedSystem(), str3 + str2 + str4));
      this.parentWizard.CloseWithoutSafetyQuestion();
    }
  }
}
