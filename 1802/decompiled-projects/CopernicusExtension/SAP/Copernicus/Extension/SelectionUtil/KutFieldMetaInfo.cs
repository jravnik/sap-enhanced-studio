﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.SelectionUtil.KutFieldMetaInfo
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.SelectionUtil
{
  public struct KutFieldMetaInfo
  {
    public string businessObjectName;
    public string businessObjectNamespace;
    public string businessObjectNodeName;
    public string esrFieldName;
    public string esrFieldNamespace;
    public string esrFieldUiText;
    public string businessObjectNodeUiText;
    public string fieldHasReferenceBoNode;
  }
}
