﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.EmbeddedComponent.EmbCompUploadWizard
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Wizard;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;

namespace SAP.Copernicus.Extension.EmbeddedComponent
{
  public class EmbCompUploadWizard : CopernicusWizardSheet, IWizard
  {
    public EmbCompUploadWizard()
    {
      this.InitializeComponent();
      this.Pages.Add((object) new EmbCompUploadFirstPage(this));
      this.helpid = HELP_IDS.BDS_BOEXTENSION_CREATE;
    }

    void IWizard.BeforeOpeningFile(ProjectItem projectItem)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.ProjectFinishedGenerating(Project project)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.ProjectItemFinishedGenerating(ProjectItem projectItem)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.RunFinished()
    {
      int num = (int) this.ShowDialog();
    }

    void IWizard.RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
    {
    }

    bool IWizard.ShouldAddProjectItem(string filePath)
    {
      return true;
    }

    private void InitializeComponent()
    {
      this.buttonPanel.SuspendLayout();
      this.SuspendLayout();
      this.ClientSize = new Size(384, 141);
      this.Name = nameof (EmbCompUploadWizard);
      this.Text = "Upload Custom Pane";
      this.buttonPanel.ResumeLayout(false);
      this.buttonPanel.PerformLayout();
      this.ResumeLayout(false);
    }

    private void nextButton_Click(object sender, EventArgs e)
    {
    }
  }
}
