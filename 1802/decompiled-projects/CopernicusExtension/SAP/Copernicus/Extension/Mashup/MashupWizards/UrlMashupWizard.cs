﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupWizards.UrlMashupWizard
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Extension.Mashup.WebBrowser;

namespace SAP.Copernicus.Extension.Mashup.MashupWizards
{
  public class UrlMashupWizard : GenericAuthoringWizard
  {
    protected override void invokeTargetWizard(string strInputNameWizard)
    {
      WebBrowserProxy.getInstance().AsynCreateMashupItemOnLine(WebBrowserProxy.UrlMashupStringTypeOnline, strInputNameWizard);
    }
  }
}
