﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupWizards.MashupCategory.MashupCategoryWizard
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using EnvDTE;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.TemplateWizard;
using SAP.Copernicus.Extension.Mashup.MashupProjectNode;
using SAP.Copernicus.TextTemplating;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.Mashup.MashupWizards.MashupCategory
{
  public class MashupCategoryWizard : IWizard
  {
    private static string _strWizardItemKey = "$rootname$";
    private string strTemplateName = "MashupCategoryTemplate.tt";
    private HierarchyNode parentNode = CopernicusProjectSystemUtil.getSelectedNode();
    public const string strMashup = "Mashup";
    public const string strService = "Service";
    public const string strPortBinding = "PortBinding";
    private string _strTemplatepath;
    private CopernicusProjectNode projectNode;
    public string _strUserInputNameforStandardWizard;
    private string _strHtmlMashupTemplateOfFullPath;

    public void BeforeOpeningFile(ProjectItem projectItem)
    {
    }

    public void ProjectFinishedGenerating(EnvDTE.Project project)
    {
    }

    public void ProjectItemFinishedGenerating(ProjectItem projectItem)
    {
    }

    public void RunFinished()
    {
      if (this._strUserInputNameforStandardWizard.Length > 30)
      {
        int num = (int) MessageBox.Show("the length of the file name should not exceed 30.", "File Name Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
        this.DoFinishOperation();
    }

    private void DoFinishOperation()
    {
      Dictionary<string, string> properties = new Dictionary<string, string>();
      properties.Add("SemanticCategory", string.Empty);
      properties.Add("InforPackage", string.Empty);
      properties.Add("Description", string.Empty);
      properties.Add("InPortTypeReference", string.Empty);
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      HierarchyNode bindingReferenceNode = MashupHierarchDefNode.MashupPortBindingReferenceNode;
      string projectFolderXrepPath = XRepMapper.GetInstance().GetProjectFolderXRepPath();
      properties.Add("MashupCategoryName", projectFolderXrepPath.Substring(0, projectFolderXrepPath.LastIndexOf("/")) + "/" + this._strUserInputNameforStandardWizard);
      CompilerErrorCollection errors;
      string fileContent = new TextTemplateHelper((ITemplateProvider) CategoryTemplateProcess.Instance).ProcessTemplate_XBO(this.strTemplateName, properties, out errors);
      string path = XRepMapper.GetInstance().GetProjectFolderOnDisk() + "/Mashups/".Replace("/", "\\");
      string rootFolderOnDisk1 = XRepMapper.GetInstance().GetMashupRootFolderOnDisk();
      string rootFolderOnDisk2 = XRepMapper.GetInstance().GetServiceRootFolderOnDisk();
      string rootFolderOnDisk3 = XRepMapper.GetInstance().GetMashupPortBindingRootFolderOnDisk();
      if (File.Exists(rootFolderOnDisk3 + this._strUserInputNameforStandardWizard + ".PB.uimashup"))
      {
        int num = (int) MessageBox.Show(SAP.Copernicus.Extension.Mashup.Resource.PBFileExisted, SAP.Copernicus.Extension.Resources.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        if (!Directory.Exists(path))
          Directory.CreateDirectory(path);
        if (!Directory.Exists(rootFolderOnDisk1))
          Directory.CreateDirectory(rootFolderOnDisk1);
        if (!Directory.Exists(rootFolderOnDisk2))
          Directory.CreateDirectory(rootFolderOnDisk2);
        if (!Directory.Exists(rootFolderOnDisk3))
          Directory.CreateDirectory(rootFolderOnDisk3);
        SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer instance = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance();
        instance.EstablishMashupConfigurationHierarchyNode(false, "Mashup", false);
        instance.EstablishMashupConfigurationHierarchyNode(false, "Service", false);
        instance.EstablishMashupConfigurationHierarchyNode(false, "PortBinding", false);
        CopernicusExtensionPackage.Instance.MonitorMashupFolder();
        CopernicusExtensionPackage.Instance.MonitorServiceFolder();
        CopernicusExtensionPackage.Instance.MonitorPortBindingFolder();
        properties.Add("CreationCase", bool.TrueString);
        properties.Add("MASHUP_TYPE", "Port_Binding");
        properties.Remove("MashupCategoryName");
        properties.Add("MashupCategoryName", this._strUserInputNameforStandardWizard);
        properties.Add("MASHUP_DISPLAYNAME", this._strUserInputNameforStandardWizard);
        instance.StoreAttributesInmemory(rootFolderOnDisk3 + this._strUserInputNameforStandardWizard + ".PB.uimashup", (IDictionary<string, string>) properties);
        if (!selectedProject.CheckOutProjectFile(false))
          return;
        try
        {
          string str = rootFolderOnDisk3 + this._strUserInputNameforStandardWizard + ".PB.uimashup";
          if (string.IsNullOrEmpty(XRepMapper.GetInstance().CreateFileonDiskAndXRep(str, fileContent, (IDictionary<string, string>) properties, (Encoding) null, false, (string) null, false)))
            return;
          XRepMapper.GetInstance().ActivateFileInXRep(str, false);
        }
        finally
        {
          selectedProject.CheckInProjectFile();
        }
      }
    }

    public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
    {
      this._strUserInputNameforStandardWizard = replacementsDictionary[MashupCategoryWizard._strWizardItemKey];
    }

    public bool ShouldAddProjectItem(string filePath)
    {
      return false;
    }
  }
}
