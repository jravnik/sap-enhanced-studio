﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupWizards.MashupCategory.SelectPTPWizardDialog
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Project;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Wizard;
using SAP.Copernicus.uidesigner.integration.model.MashupJsonUtil.UXModelAPI;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.Mashup.MashupWizards.MashupCategory
{
  public class SelectPTPWizardDialog : Form
  {
    private Dictionary<string, string> ptpNameToUrl = new Dictionary<string, string>();
    public CopernicusProjectNode currentProject = CopernicusProjectSystemUtil.getSelectedProject();
    public HierarchyNode parentNode = CopernicusProjectSystemUtil.getSelectedNode();
    private IContainer components;
    private ComboBox comBox_Semantic;
    private ComboBox comBox_Infor;
    private Label lbl_CatName;
    private Label txt_CatName;
    private Label lbl_Semantic;
    private Label lbl_Infor;
    private Label lbl_Desc;
    private TextBox txt_Desc;
    private ComboBox comBox_InPort;
    private Label lbl_InPort;
    private Label lbl_Star1;
    private Label lbl_Star2;
    private Label lbl_Star3;
    private Label lbl_Star4;
    protected Panel buttonPanel;
    protected Button finishButton;
    protected CopernicusEtchedLine etchedLine1;
    protected Button cancelButton;
    private Panel panel1;
    private Label lbl_ptpError;
    private MashupCategoryWizard host;
    private Action<object> callbackFunction;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.comBox_Semantic = new ComboBox();
      this.comBox_Infor = new ComboBox();
      this.lbl_CatName = new Label();
      this.txt_CatName = new Label();
      this.lbl_Semantic = new Label();
      this.lbl_Infor = new Label();
      this.lbl_Desc = new Label();
      this.txt_Desc = new TextBox();
      this.comBox_InPort = new ComboBox();
      this.lbl_InPort = new Label();
      this.lbl_Star1 = new Label();
      this.lbl_Star2 = new Label();
      this.lbl_Star3 = new Label();
      this.lbl_Star4 = new Label();
      this.buttonPanel = new Panel();
      this.finishButton = new Button();
      this.etchedLine1 = new CopernicusEtchedLine();
      this.cancelButton = new Button();
      this.panel1 = new Panel();
      this.lbl_ptpError = new Label();
      this.buttonPanel.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      this.comBox_Semantic.DropDownStyle = ComboBoxStyle.DropDownList;
      this.comBox_Semantic.FormattingEnabled = true;
      this.comBox_Semantic.Location = new Point(173, 75);
      this.comBox_Semantic.Name = "comBox_Semantic";
      this.comBox_Semantic.Size = new Size(185, 21);
      this.comBox_Semantic.TabIndex = 1;
      this.comBox_Infor.DropDownStyle = ComboBoxStyle.DropDownList;
      this.comBox_Infor.FormattingEnabled = true;
      this.comBox_Infor.Location = new Point(173, 114);
      this.comBox_Infor.Name = "comBox_Infor";
      this.comBox_Infor.Size = new Size(185, 21);
      this.comBox_Infor.TabIndex = 2;
      this.comBox_Infor.SelectedIndexChanged += new EventHandler(this.comBox_Infor_SelectedIndexChanged);
      this.lbl_CatName.AutoSize = true;
      this.lbl_CatName.Location = new Point(28, 15);
      this.lbl_CatName.Name = "lbl_CatName";
      this.lbl_CatName.Size = new Size(124, 13);
      this.lbl_CatName.TabIndex = 3;
      this.lbl_CatName.Text = "Mashup Category Name:";
      this.txt_CatName.AutoSize = true;
      this.txt_CatName.Location = new Point(170, 15);
      this.txt_CatName.Name = "txt_CatName";
      this.txt_CatName.Size = new Size(121, 13);
      this.txt_CatName.TabIndex = 4;
      this.txt_CatName.Text = "Mashup Category Name";
      this.lbl_Semantic.AutoSize = true;
      this.lbl_Semantic.Location = new Point(28, 79);
      this.lbl_Semantic.Name = "lbl_Semantic";
      this.lbl_Semantic.Size = new Size(99, 13);
      this.lbl_Semantic.TabIndex = 5;
      this.lbl_Semantic.Text = "Semantic Category:";
      this.lbl_Infor.AutoSize = true;
      this.lbl_Infor.Location = new Point(28, 118);
      this.lbl_Infor.Name = "lbl_Infor";
      this.lbl_Infor.Size = new Size(77, 13);
      this.lbl_Infor.TabIndex = 6;
      this.lbl_Infor.Text = "Infor Package:";
      this.lbl_Desc.AutoSize = true;
      this.lbl_Desc.Location = new Point(28, 43);
      this.lbl_Desc.Name = "lbl_Desc";
      this.lbl_Desc.Size = new Size(63, 13);
      this.lbl_Desc.TabIndex = 7;
      this.lbl_Desc.Text = "Description:";
      this.txt_Desc.Location = new Point(173, 40);
      this.txt_Desc.Name = "txt_Desc";
      this.txt_Desc.Size = new Size(185, 20);
      this.txt_Desc.TabIndex = 8;
      this.txt_Desc.TextChanged += new EventHandler(this.txt_Desc_TextChanged);
      this.comBox_InPort.DropDownStyle = ComboBoxStyle.DropDownList;
      this.comBox_InPort.Enabled = false;
      this.comBox_InPort.FormattingEnabled = true;
      this.comBox_InPort.Location = new Point(173, 148);
      this.comBox_InPort.Name = "comBox_InPort";
      this.comBox_InPort.Size = new Size(185, 21);
      this.comBox_InPort.TabIndex = 9;
      this.comBox_InPort.SelectedIndexChanged += new EventHandler(this.comBox_InPort_SelectedIndexChanged);
      this.lbl_InPort.AutoSize = true;
      this.lbl_InPort.Location = new Point(28, 151);
      this.lbl_InPort.Name = "lbl_InPort";
      this.lbl_InPort.Size = new Size(118, 13);
      this.lbl_InPort.TabIndex = 10;
      this.lbl_InPort.Text = "InPort Type Reference:";
      this.lbl_Star1.AutoSize = true;
      this.lbl_Star1.ForeColor = Color.Red;
      this.lbl_Star1.Location = new Point(122, 78);
      this.lbl_Star1.Name = "lbl_Star1";
      this.lbl_Star1.Size = new Size(11, 13);
      this.lbl_Star1.TabIndex = 11;
      this.lbl_Star1.Text = "*";
      this.lbl_Star2.AutoSize = true;
      this.lbl_Star2.ForeColor = Color.Red;
      this.lbl_Star2.Location = new Point(101, 118);
      this.lbl_Star2.Name = "lbl_Star2";
      this.lbl_Star2.Size = new Size(11, 13);
      this.lbl_Star2.TabIndex = 12;
      this.lbl_Star2.Text = "*";
      this.lbl_Star3.AutoSize = true;
      this.lbl_Star3.ForeColor = Color.Red;
      this.lbl_Star3.Location = new Point(87, 43);
      this.lbl_Star3.Name = "lbl_Star3";
      this.lbl_Star3.Size = new Size(11, 13);
      this.lbl_Star3.TabIndex = 13;
      this.lbl_Star3.Text = "*";
      this.lbl_Star4.AutoSize = true;
      this.lbl_Star4.ForeColor = Color.Red;
      this.lbl_Star4.Location = new Point(141, 151);
      this.lbl_Star4.Name = "lbl_Star4";
      this.lbl_Star4.Size = new Size(11, 13);
      this.lbl_Star4.TabIndex = 14;
      this.lbl_Star4.Text = "*";
      this.buttonPanel.Controls.Add((Control) this.finishButton);
      this.buttonPanel.Controls.Add((Control) this.etchedLine1);
      this.buttonPanel.Controls.Add((Control) this.cancelButton);
      this.buttonPanel.Dock = DockStyle.Bottom;
      this.buttonPanel.Location = new Point(0, 187);
      this.buttonPanel.Name = "buttonPanel";
      this.buttonPanel.Size = new Size(557, 40);
      this.buttonPanel.TabIndex = 15;
      this.finishButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.finishButton.Enabled = false;
      this.finishButton.FlatStyle = FlatStyle.System;
      this.finishButton.Location = new Point(381, 8);
      this.finishButton.Name = "finishButton";
      this.finishButton.Size = new Size(75, 23);
      this.finishButton.TabIndex = 2;
      this.finishButton.Text = "OK";
      this.finishButton.Click += new EventHandler(this.finishButton_Click);
      this.etchedLine1.AutoScroll = true;
      this.etchedLine1.AutoSize = true;
      this.etchedLine1.Dock = DockStyle.Top;
      this.etchedLine1.Edge = EtchEdge.Top;
      this.etchedLine1.Location = new Point(0, 0);
      this.etchedLine1.Name = "etchedLine1";
      this.etchedLine1.Size = new Size(557, 0);
      this.etchedLine1.TabIndex = 4;
      this.cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.cancelButton.FlatStyle = FlatStyle.System;
      this.cancelButton.Location = new Point(469, 8);
      this.cancelButton.Name = "cancelButton";
      this.cancelButton.Size = new Size(75, 23);
      this.cancelButton.TabIndex = 3;
      this.cancelButton.Text = "Cancel";
      this.cancelButton.Click += new EventHandler(this.cancelButton_Click);
      this.panel1.Controls.Add((Control) this.lbl_ptpError);
      this.panel1.Controls.Add((Control) this.txt_Desc);
      this.panel1.Controls.Add((Control) this.lbl_Desc);
      this.panel1.Controls.Add((Control) this.lbl_Star3);
      this.panel1.Location = new Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new Size(557, 189);
      this.panel1.TabIndex = 16;
      this.lbl_ptpError.AutoSize = true;
      this.lbl_ptpError.ForeColor = Color.FromArgb(192, 0, 0);
      this.lbl_ptpError.Location = new Point(362, 118);
      this.lbl_ptpError.Name = "lbl_ptpError";
      this.lbl_ptpError.Size = new Size(195, 13);
      this.lbl_ptpError.TabIndex = 14;
      this.lbl_ptpError.Text = "PTP file must be activated before using!";
      this.lbl_ptpError.Visible = false;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(557, 227);
      this.Controls.Add((Control) this.buttonPanel);
      this.Controls.Add((Control) this.lbl_Star4);
      this.Controls.Add((Control) this.lbl_Star2);
      this.Controls.Add((Control) this.lbl_Star1);
      this.Controls.Add((Control) this.lbl_InPort);
      this.Controls.Add((Control) this.comBox_InPort);
      this.Controls.Add((Control) this.lbl_Infor);
      this.Controls.Add((Control) this.lbl_Semantic);
      this.Controls.Add((Control) this.txt_CatName);
      this.Controls.Add((Control) this.lbl_CatName);
      this.Controls.Add((Control) this.comBox_Infor);
      this.Controls.Add((Control) this.comBox_Semantic);
      this.Controls.Add((Control) this.panel1);
      this.Name = nameof (SelectPTPWizardDialog);
      this.Text = "Copernicus";
      this.buttonPanel.ResumeLayout(false);
      this.buttonPanel.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private event Action<List<string>> OKCallbackEvent;

    public void AddCallbackEventHandler(Action<List<string>> _myEventHandler)
    {
      this.OKCallbackEvent += _myEventHandler;
    }

    public void RemoveCallbackEventHandler(Action<List<string>> _myEventHandler)
    {
      this.OKCallbackEvent -= _myEventHandler;
    }

    private void RaiseEvent(List<string> strParameter, CopernicusProjectNode currentProject)
    {
      if (this.OKCallbackEvent == null)
        return;
      Action<List<string>> action = Interlocked.CompareExchange<Action<List<string>>>(ref this.OKCallbackEvent, (Action<List<string>>) null, (Action<List<string>>) null);
      if (action == null)
        return;
      action.BeginInvoke(strParameter, (AsyncCallback) null, (object) null);
    }

    public SelectPTPWizardDialog(MashupCategoryWizard _host)
    {
      this.InitializeComponent();
      this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;
      this.host = _host;
      this.callbackFunction = new Action<object>(this.updateDataStructure);
      this.StartWizard();
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
    }

    private void updateDataStructure(object strXRepoisotyFullPath)
    {
    }

    private void AsyncCallback(IAsyncResult ar)
    {
      MashupPTPHelper asyncState = ar.AsyncState as MashupPTPHelper;
      if (this.comBox_Infor.InvokeRequired)
      {
        try
        {
          this.comBox_Infor.Invoke((Delegate) new Action<MashupPTPHelper>(this.update_ComBox_InPort_Content), (object) asyncState);
        }
        catch (Exception ex)
        {
          Trace.TraceError(ex.StackTrace);
        }
      }
      else
        this.update_ComBox_InPort_Content(asyncState);
    }

    private void update_ComBox_InPort_Content(MashupPTPHelper ptpSelectedURLs)
    {
      string selectedItem = this.comBox_Infor.SelectedItem as string;
      string ptpFileName = ptpSelectedURLs.ptpFileName;
      XRepMapper.GetInstance();
      XRepHandler instance = XRepHandler.Instance;
      string content = string.Empty;
      if (!selectedItem.Equals(ptpFileName))
        return;
      IDictionary<string, string> attribs = (IDictionary<string, string>) new Dictionary<string, string>();
      instance.Read(ptpSelectedURLs.XRepositoryFullPath, out content, out attribs);
      string str;
      attribs.TryGetValue("~ACTIVE_FLAG", out str);
      if (!(str == "A"))
        return;
      Trace.TraceInformation("Get port types from:" + ptpFileName + "\n");
      this.comBox_InPort.Enabled = true;
      this.lbl_ptpError.Visible = false;
      List<string> stringList = new List<string>();
      IDictionary<string, string> cachedFileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getMashupCachedFileAttributes(ptpSelectedURLs.selectedUrl);
      List<string> ptpIntoPortType = MashupMetaDataAccess.ParsePTPIntoPortType(content, ptpSelectedURLs.selectedUrl, attribs, cachedFileAttributes);
      ThreadPool.QueueUserWorkItem((WaitCallback) (state => this.SyncLocalFile(ptpSelectedURLs.selectedUrl, content)));
      if (ptpIntoPortType.Count > 0)
      {
        foreach (object obj in ptpIntoPortType)
          this.comBox_InPort.Items.Add(obj);
      }
      else
      {
        this.lbl_ptpError.Visible = true;
        this.comBox_InPort.Enabled = false;
        this.finishButton.Enabled = false;
      }
    }

    private void comBox_Infor_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.comBox_InPort.Items.Clear();
      string str = string.Empty;
      XRepMapper instance = XRepMapper.GetInstance();
      string empty = string.Empty;
      MashupPTPHelper mashupPtpHelper = new MashupPTPHelper();
      if (instance != null)
      {
        this.ptpNameToUrl.TryGetValue(this.comBox_Infor.SelectedItem.ToString(), out empty);
        if (empty != null)
        {
          Trace.TraceInformation("To get XRepository Full Path based on selected PTP item:" + this.comBox_Infor.SelectedItem.ToString() + "\n");
          str = instance.GetXrepPathforLocalFile(empty);
        }
      }
      mashupPtpHelper.XRepositoryFullPath = str;
      mashupPtpHelper.selectedUrl = empty;
      mashupPtpHelper.ptpFileName = this.comBox_Infor.SelectedItem.ToString();
      this.callbackFunction.BeginInvoke((object) mashupPtpHelper, new AsyncCallback(this.AsyncCallback), (object) mashupPtpHelper);
      if (this.txt_Desc.Text != "" && this.comBox_Semantic.SelectedItem != null && (this.comBox_InPort.Enabled && this.comBox_InPort.SelectedItem != null))
        this.finishButton.Enabled = true;
      else
        this.finishButton.Enabled = false;
    }

    private void StartWizard()
    {
      this.txt_CatName.Text = this.host._strUserInputNameforStandardWizard;
      foreach (MashupWizardHelper.SemanticCategories enumValue in MashupWizardHelper.GetEnumValues<MashupWizardHelper.SemanticCategories>())
      {
        Trace.TraceInformation("To initialize Semantic Category value list:\n");
        this.comBox_Semantic.Items.Add((object) enumValue);
      }
      if (this.currentProject == null)
        return;
      foreach (HierarchyNode hierarchyNode in this.currentProject.getNodeswithExtn(".PTP.uicomponent"))
      {
        if (hierarchyNode is FileNode)
        {
          string name = hierarchyNode.NodeProperties.Name;
          this.comBox_Infor.Items.Add((object) name);
          this.ptpNameToUrl.Add(name, hierarchyNode.Url);
        }
      }
    }

    private void finishButton_Click(object sender, EventArgs e)
    {
      this.RaiseEvent(new List<string>()
      {
        this.lbl_Desc.Text,
        this.comBox_Semantic.SelectedItem.ToString(),
        this.comBox_Infor.SelectedItem.ToString(),
        this.comBox_InPort.SelectedItem.ToString()
      }, this.currentProject);
      this.Close();
    }

    public void SyncLocalFile(string strDiskPathforFile, string contentResult)
    {
      using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(contentResult)))
      {
        using (FileStream fileStream = new FileStream(strDiskPathforFile, FileMode.Create, FileAccess.Write))
        {
          Trace.TraceInformation("Begin to synchronize disk file content from XREP :\n");
          memoryStream.CopyTo((Stream) fileStream);
        }
      }
    }

    private void cancelButton_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void txt_Desc_TextChanged(object sender, EventArgs e)
    {
      if (this.txt_Desc.Text != "" && this.comBox_Semantic.SelectedItem != null && (this.comBox_InPort.Enabled && this.comBox_InPort.SelectedItem != null))
        this.finishButton.Enabled = true;
      else
        this.finishButton.Enabled = false;
    }

    private void comBox_InPort_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.txt_Desc.Text != "" && this.comBox_Semantic.SelectedItem != null && (this.comBox_InPort.Enabled && this.comBox_InPort.SelectedItem != null))
        this.finishButton.Enabled = true;
      else
        this.finishButton.Enabled = false;
    }
  }
}
