﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupWizards.MashupWizardHelper
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SAP.Copernicus.Extension.Mashup.MashupWizards
{
  public static class MashupWizardHelper
  {
    public static T[] GetEnumValues<T>()
    {
      Type type = typeof (T);
      if (!type.IsEnum)
        throw new ArgumentException("Type '" + type.Name + "' is not an enum");
      List<T> objList = new List<T>();
      foreach (FieldInfo fieldInfo in ((IEnumerable<FieldInfo>) type.GetFields()).Where<FieldInfo>((Func<FieldInfo, bool>) (field => field.IsLiteral)))
      {
        object obj = fieldInfo.GetValue((object) type);
        objList.Add((T) obj);
      }
      return objList.ToArray();
    }

    public enum SemanticCategories
    {
      BusinessFinance,
      LocationTravel,
      NewsReference,
      ProductivityTools,
      SocialCommunication,
    }
  }
}
