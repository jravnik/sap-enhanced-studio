﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.EmbedBrowser.IESecurityOption
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security;

namespace SAP.Copernicus.Extension.Mashup.EmbedBrowser
{
  public class IESecurityOption
  {
    private static string strSecuritySubKeyPath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Internet Settings\\Zones\\";
    private static string strSecurityKeyName = "1A04";
    private static int dwEnable = 0;
    private static int dwDisable = 3;
    private const int INTERNET_OPTION_END_BROWSER_SESSION = 42;

    public static void EnableSecurityOption()
    {
      IESecurityOption.ModifyRegistryKey(UrlZone.Internet);
      IESecurityOption.ModifyRegistryKey(UrlZone.Intranet);
    }

    [SecuritySafeCritical]
    private static void ModifyRegistryKey(UrlZone zone)
    {
      using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(IESecurityOption.strSecuritySubKeyPath + (object) zone, true))
      {
        int? nullable1 = registryKey.GetValue(IESecurityOption.strSecurityKeyName) as int?;
        Trace.Assert(nullable1.HasValue);
        int? nullable2 = nullable1;
        int dwEnable = IESecurityOption.dwEnable;
        if ((nullable2.GetValueOrDefault() != dwEnable ? 1 : (!nullable2.HasValue ? 1 : 0)) == 0)
          return;
        registryKey.SetValue(IESecurityOption.strSecurityKeyName, (object) IESecurityOption.dwEnable);
      }
    }

    [DllImport("wininet.dll", SetLastError = true)]
    private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int lpdwBufferLength);

    internal static void CleanUpSession()
    {
      IESecurityOption.InternetSetOption(IntPtr.Zero, 42, IntPtr.Zero, 0);
    }
  }
}
