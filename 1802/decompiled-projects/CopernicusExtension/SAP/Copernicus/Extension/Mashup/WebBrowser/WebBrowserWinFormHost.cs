﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.WebBrowserWinFormHost
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using mshtml;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Extension.Mashup.HtmlMashup.MashupJsonUtil.MashupHandler;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  [ComVisible(true)]
  public class WebBrowserWinFormHost : Form
  {
    private static string strBlankDocumentTitle = "-";
    private static string strReadyStatus = "complete";
    private static string strSLctrlId = "silverlightControl";
    private static string strDivSLctrlHostId = "silverlightControlHost";
    private static string strlogPageTitle = "Logoff";
    private IContainer components;
    private StatusStrip statusStripOfQAFbuilder;
    private ToolStripProgressBar toolStripProgressBarOfQAFbuilder;
    private ToolStripStatusLabel toolStripStatusLabel;
    private System.Windows.Forms.Timer timerOfProgress;
    private Panel iePane;
    private AdvancedWebBrowser _webbrowser;
    private SHDocVw.WebBrowser _comWebbrowser;
    private string strCurrentURL;
    private MashupItemOnLineEditor _hostOfEditor;
    private HTMLDocumentEvents2_Event m_hostedDocEvent;
    private HTMLDocument htmlUnManagedDocForCallBack;
    private HtmlDocument htmlManagedDocForCallBack;
    private bool _isProcessedSL;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.statusStripOfQAFbuilder = new StatusStrip();
      this.toolStripProgressBarOfQAFbuilder = new ToolStripProgressBar();
      this.toolStripStatusLabel = new ToolStripStatusLabel();
      this.timerOfProgress = new System.Windows.Forms.Timer(this.components);
      this.iePane = new Panel();
      this.statusStripOfQAFbuilder.SuspendLayout();
      this.SuspendLayout();
      this.statusStripOfQAFbuilder.Items.AddRange(new ToolStripItem[2]
      {
        (ToolStripItem) this.toolStripProgressBarOfQAFbuilder,
        (ToolStripItem) this.toolStripStatusLabel
      });
      this.statusStripOfQAFbuilder.Location = new Point(0, 536);
      this.statusStripOfQAFbuilder.Name = "statusStripOfQAFbuilder";
      this.statusStripOfQAFbuilder.Size = new Size(748, 22);
      this.statusStripOfQAFbuilder.TabIndex = 0;
      this.statusStripOfQAFbuilder.Text = "statusStrip1";
      this.toolStripProgressBarOfQAFbuilder.Name = "toolStripProgressBarOfQAFbuilder";
      this.toolStripProgressBarOfQAFbuilder.Size = new Size(100, 16);
      this.toolStripStatusLabel.Name = "toolStripStatusLabel";
      this.toolStripStatusLabel.Size = new Size(0, 17);
      this.timerOfProgress.Interval = 50;
      this.timerOfProgress.Tick += new EventHandler(this.timerOfProgress_Tick);
      this.iePane.Dock = DockStyle.Fill;
      this.iePane.Location = new Point(0, 0);
      this.iePane.Name = "iePane";
      this.iePane.Size = new Size(748, 536);
      this.iePane.TabIndex = 1;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = SystemColors.Control;
      this.ClientSize = new Size(748, 558);
      this.Controls.Add((Control) this.iePane);
      this.Controls.Add((Control) this.statusStripOfQAFbuilder);
      this.Name = nameof (WebBrowserWinFormHost);
      this.ShowIcon = false;
      this.Text = "QAF Builder";
      this.statusStripOfQAFbuilder.ResumeLayout(false);
      this.statusStripOfQAFbuilder.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public string NavigationURL
    {
      get
      {
        return this.strCurrentURL;
      }
      set
      {
        if (value == null || value.Equals(this.strCurrentURL))
          return;
        this.strCurrentURL = value;
      }
    }

    public WebBrowserWinFormHost(InternetProxy _proxy, MashupItemOnLineEditor _host)
    {
      this.InitializeComponent();
      this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;
      this._hostOfEditor = _host;
      this.Load += new EventHandler(this.webBrowserWinFormHost_Load);
      this._webbrowser = new AdvancedWebBrowser();
      this._webbrowser.Dock = DockStyle.Fill;
      this._webbrowser.Proxy = _proxy;
      this._webbrowser.ObjectForScripting = (object) this;
      this.iePane.Controls.Add((Control) this._webbrowser);
      this._webbrowser.Disposed += new EventHandler(this.webbrowser_Disposed);
      this._webbrowser.HandleDestroyed += new EventHandler(this.webbrowser_HandleDestroyed);
      this._webbrowser.DocumentTitleChanged += new EventHandler(this.webbrowser_DocumentTitleChanged);
      this._webbrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.webbrowser_ManagedDocumentCompleted);
      this.FormClosed += new FormClosedEventHandler(this.webBrowserWinFormHost_FormClosed);
    }

    private void webbrowser_Disposed(object sender, EventArgs e)
    {
      this.Close();
    }

    private void webbrowser_HandleDestroyed(object sender, EventArgs e)
    {
      this.Close();
    }

    private void webBrowserWinFormHost_Load(object sender, EventArgs e)
    {
      if (this.strCurrentURL == null)
        return;
      this.Navigate(this.strCurrentURL);
    }

    private void webbrowser_DocumentTitleChanged(object sender, EventArgs e)
    {
      AdvancedWebBrowser advancedWebBrowser = sender as AdvancedWebBrowser;
      if (advancedWebBrowser.DocumentTitle.Equals(WebBrowserWinFormHost.strBlankDocumentTitle))
      {
        this.Close();
      }
      else
      {
        if (this.Text.Equals(advancedWebBrowser.DocumentTitle))
          return;
        this.Text = advancedWebBrowser.DocumentTitle;
      }
    }

    public void Navigate(string strFormattedURL)
    {
      this._isProcessedSL = false;
      this.SetControlStatusThreadSafeForNavigation();
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new Action(delegate
        {
          SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.DeleteUrlCacheEntry(strFormattedURL);
          this._webbrowser.Goto(strFormattedURL);
        }));
      }
      else
      {
        this.SetControlStatusThreadSafeForNavigation();
        SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.DeleteUrlCacheEntry(strFormattedURL);
        this._webbrowser.Goto(strFormattedURL);
      }
    }

    private void SetControlStatusThreadSafeForNavigation()
    {
      this.Invoke(new Action(delegate
      {
        this.toolStripStatusLabel.Text = "Start QAF builder";
        this.toolStripProgressBarOfQAFbuilder.Visible = true;
        this.timerOfProgress.Enabled = true;
      }));
    }

    private void webbrowser_UnManagedDocumentCompleted(object pDisp, ref object URL)
    {
      // ISSUE: variable of a compiler-generated type
      SHDocVw.WebBrowser webBrowser = pDisp as SHDocVw.WebBrowser;
      Trace.Assert(webBrowser != null);
      // ISSUE: variable of a compiler-generated type
      HTMLDocument document = webBrowser.Document as HTMLDocument;
      if (document.title.Equals(this._hostOfEditor.LogOnPageTitle))
      {
        // ISSUE: reference to a compiler-generated method
        if (document.getElementById("sap-alias") == null)
          return;
        string userAlias = Connection.getInstance().ConnectedSystem.UserAlias;
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated method
        document.getElementById("sap-alias").setAttribute("value", (object) userAlias, 1);
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated method
        document.getElementById("sap-password").setAttribute("value", (object) SecureStringUtil.secureStringToString(Connection.getInstance().ConnectedSystem.SecurePassword), 1);
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated method
        document.getElementById("sap-system-login-oninputprocessing").setAttribute("value", (object) "onLogin", 1);
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated method
        document.getElementById("sap-language").setAttribute("value", (object) "EN", 1);
        // ISSUE: reference to a compiler-generated method
        // ISSUE: variable of a compiler-generated type
        IHTMLFormElement htmlFormElement = document.forms.item((object) "loginForm", (object) Missing.Value) as IHTMLFormElement;
        Trace.Assert(htmlFormElement != null);
        // ISSUE: reference to a compiler-generated method
        htmlFormElement.submit();
      }
      else
      {
        // ISSUE: variable of a compiler-generated type
        IHTMLElement documentElement = document.documentElement;
        // ISSUE: reference to a compiler-generated method
        // ISSUE: variable of a compiler-generated type
        IHTMLElement elementById1 = document.getElementById(WebBrowserWinFormHost.strSLctrlId);
        if (this._hostOfEditor.ActionOfQAF != MashupActionCategory.CreateMashup && this._hostOfEditor.ActionOfQAF != MashupActionCategory.EditMashup || (elementById1 == null || !document.readyState.Equals(WebBrowserWinFormHost.strReadyStatus)))
          return;
        this.htmlUnManagedDocForCallBack = document;
        // ISSUE: reference to a compiler-generated method
        // ISSUE: variable of a compiler-generated type
        IHTMLDOMNode elementById2 = document.getElementById(WebBrowserWinFormHost.strDivSLctrlHostId) as IHTMLDOMNode;
        Trace.Assert(elementById2 != null);
        // ISSUE: reference to a compiler-generated method
        // ISSUE: variable of a compiler-generated type
        IHTMLInputElement element1 = document.createElement("<input id='mashupAction' type='hidden'/>") as IHTMLInputElement;
        // ISSUE: reference to a compiler-generated method
        // ISSUE: variable of a compiler-generated type
        IHTMLInputElement element2 = document.createElement("<input id='mashupId' type='hidden'/>") as IHTMLInputElement;
        // ISSUE: variable of a compiler-generated type
        IHTMLDOMNode newChild1 = element1 as IHTMLDOMNode;
        // ISSUE: variable of a compiler-generated type
        IHTMLDOMNode newChild2 = element2 as IHTMLDOMNode;
        // ISSUE: reference to a compiler-generated method
        elementById2.appendChild(newChild1);
        // ISSUE: reference to a compiler-generated method
        elementById2.appendChild(newChild2);
        this.m_hostedDocEvent = (HTMLDocumentEvents2_Event) document;
        if (this.m_hostedDocEvent != null)
        {
          // ISSUE: method pointer
          // ISSUE: object of a compiler-generated type is created
          new ComAwareEventInfo(typeof (HTMLDocumentEvents2_Event), "onclick").AddEventHandler((object) this.m_hostedDocEvent, (Delegate) new HTMLDocumentEvents2_onclickEventHandler(this.inputElementClick_UnManagedEventHandler));
        }
        // ISSUE: reference to a compiler-generated method
        // ISSUE: variable of a compiler-generated type
        IHTMLElementCollection elementsByTagName = document.getElementsByTagName("head");
        // ISSUE: reference to a compiler-generated method
        // ISSUE: variable of a compiler-generated type
        IHTMLHeadElement htmlHeadElement = elementsByTagName.item((object) 0, (object) Missing.Value) as IHTMLHeadElement;
        // ISSUE: reference to a compiler-generated method
        // ISSUE: variable of a compiler-generated type
        IHTMLScriptElement element3 = document.createElement("script") as IHTMLScriptElement;
        element3.type = "text/javascript";
        element3.text = "function mashupCallback(action, id){                        \r\n                            var mashupActionInput = window.document.getElementById('mashupAction');\r\n                            mashupActionInput.setAttribute('value', action);\r\n                            var mashupIdInput = window.document.getElementById('mashupId');\r\n                            mashupIdInput.setAttribute('value', id);\r\n                            mashupActionInput.click();\r\n                        }";
        // ISSUE: reference to a compiler-generated method
        ((DispHTMLHeadElement) htmlHeadElement).appendChild((IHTMLDOMNode) element3);
      }
    }

    private bool inputElementClick_UnManagedEventHandler(IHTMLEventObj pEvtObj)
    {
      // ISSUE: variable of a compiler-generated type
      IHTMLInputElement htmlInputElement1 = (IHTMLInputElement) null;
      // ISSUE: variable of a compiler-generated type
      IHTMLInputElement htmlInputElement2 = (IHTMLInputElement) null;
      try
      {
        // ISSUE: reference to a compiler-generated method
        htmlInputElement1 = this.htmlUnManagedDocForCallBack.getElementById("mashupAction") as IHTMLInputElement;
        // ISSUE: reference to a compiler-generated method
        htmlInputElement2 = this.htmlUnManagedDocForCallBack.getElementById("mashupId") as IHTMLInputElement;
      }
      catch (Exception ex)
      {
        Trace.WriteLine("inputElementClick_EventHandler callBack function failed.");
        Trace.WriteLine(ex.StackTrace);
      }
      Trace.Assert(htmlInputElement2.value != null, "mashupId value is null. inputElementClick_EventHandler callBack function failed.");
      Trace.Assert(htmlInputElement1.value != null, "mashupAction value is null. inputElementClick_EventHandler callBack function failed.");
      QAFBuilderAction? nullable = Enum.Parse(typeof (QAFBuilderAction), htmlInputElement1.value, true) as QAFBuilderAction?;
      Trace.Assert(nullable.HasValue);
      WebBrowserProxy instance = WebBrowserProxy.getInstance();
      if (nullable.Value == QAFBuilderAction.Save)
      {
        instance.ConvertToCreationItemtoEditor(this._hostOfEditor, htmlInputElement2.value);
        this.SynchronizeMashupServiceToLocal(htmlInputElement2.value);
      }
      else if (nullable.Value != QAFBuilderAction.Close && nullable.Value == QAFBuilderAction.SaveAndClose)
      {
        instance.ConvertToCreationItemtoEditorForSaveandClose(this._hostOfEditor, htmlInputElement2.value);
        this.SynchronizeMashupServiceToLocal(htmlInputElement2.value);
      }
      return true;
    }

    private bool RetrieveDocumentAndSLControl(HtmlDocument _rootDocument, out HtmlElement _htmlSLControl, out HtmlDocument _htmlDocument)
    {
      _htmlSLControl = _rootDocument.GetElementById(WebBrowserWinFormHost.strSLctrlId);
      _htmlDocument = (HtmlDocument) null;
      bool flag1 = false;
      bool flag2;
      if (_htmlSLControl != (HtmlElement) null)
      {
        _htmlDocument = _rootDocument;
        return flag2 = true;
      }
      if (_rootDocument.Window != (HtmlWindow) null && _rootDocument.Window.Frames != null)
      {
        foreach (HtmlWindow frame in _rootDocument.Window.Frames)
        {
          _htmlSLControl = frame.Document.GetElementById(WebBrowserWinFormHost.strSLctrlId);
          if (_htmlSLControl != (HtmlElement) null)
          {
            _htmlDocument = frame.Document;
            break;
          }
        }
      }
      if (_htmlSLControl == (HtmlElement) null)
        return flag1;
      return flag2 = true;
    }

    private void webbrowser_ManagedDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      System.Windows.Forms.WebBrowser webBrowser = sender as System.Windows.Forms.WebBrowser;
      Trace.Assert(webBrowser != null);
      HtmlDocument document = webBrowser.Document;
      HtmlElement htmlElement1 = (HtmlElement) null;
      try
      {
        htmlElement1 = document.Forms.GetElementsByName("loginForm")[0];
      }
      catch (Exception ex)
      {
      }
      if (htmlElement1 != (HtmlElement) null)
      {
        if (document.GetElementById("sap-alias") == (HtmlElement) null)
          return;
        string userAlias = Connection.getInstance().ConnectedSystem.UserAlias;
        document.GetElementById("sap-alias").SetAttribute("value", userAlias);
        document.GetElementById("sap-password").SetAttribute("value", SecureStringUtil.secureStringToString(Connection.getInstance().ConnectedSystem.SecurePassword));
        document.GetElementById("sap-system-login-oninputprocessing").SetAttribute("value", "onLogin");
        document.GetElementById("sap-language").SetAttribute("value", "EN");
        htmlElement1.InvokeMember("submit");
      }
      else if (document.Title.Equals(WebBrowserWinFormHost.strlogPageTitle))
      {
        this.Close();
      }
      else
      {
        HtmlElement _htmlSLControl = (HtmlElement) null;
        HtmlDocument _htmlDocument = (HtmlDocument) null;
        this.RetrieveDocumentAndSLControl(document, out _htmlSLControl, out _htmlDocument);
        if (!(_htmlSLControl != (HtmlElement) null) || this._isProcessedSL)
          return;
        // ISSUE: variable of a compiler-generated type
        HTMLDocument domDocument = _htmlDocument.DomDocument as HTMLDocument;
        if (domDocument.readyState.Equals(WebBrowserWinFormHost.strReadyStatus))
        {
          this.htmlManagedDocForCallBack = _htmlDocument;
          HtmlElement elementById = _htmlDocument.GetElementById(WebBrowserWinFormHost.strDivSLctrlHostId);
          Trace.Assert(elementById != (HtmlElement) null);
          if (_htmlDocument.GetElementById("mashupAction") == (HtmlElement) null)
          {
            HtmlElement element1 = _htmlDocument.CreateElement("<input id='mashupAction' type='hidden'/>");
            HtmlElement element2 = _htmlDocument.CreateElement("<input id='mashupId' type='hidden'/>");
            elementById.AppendChild(element1);
            elementById.AppendChild(element2);
            element1.Click += new HtmlElementEventHandler(this.inputElementManagedClick_EventHandler);
            HtmlElement htmlElement2 = _htmlDocument.GetElementsByTagName("head")[0];
            HtmlElement element3 = document.CreateElement("script");
            element3.SetAttribute("type", "text/javascript");
            element3.SetAttribute("text", "function mashupCallback(action, id){                        \r\n                                var mashupActionInput = window.document.getElementById('mashupAction');\r\n                                mashupActionInput.setAttribute('value', action);\r\n                                var mashupIdInput = window.document.getElementById('mashupId');\r\n                                mashupIdInput.setAttribute('value', id);\r\n                                mashupActionInput.click();\r\n                            }");
            htmlElement2.AppendChild(element3);
          }
          this._isProcessedSL = true;
          this.toolStripStatusLabel.Text = "Done";
          this.toolStripProgressBarOfQAFbuilder.Visible = false;
          this.timerOfProgress.Enabled = false;
        }
        this.BringToFront();
        WebBrowserWinFormHost.ForceBringWindowToFront(this);
        this.TopLevel = true;
      }
    }

    private static void ForceBringWindowToFront(WebBrowserWinFormHost _winformInstance)
    {
      SAP.Copernicus.Extension.Mashup.EmbedBrowser.NativeMethods.BringWindowToTop(_winformInstance.Handle);
    }

    private void inputElementManagedClick_EventHandler(object sender, HtmlElementEventArgs e)
    {
      HtmlElement htmlElement1 = (HtmlElement) null;
      HtmlElement htmlElement2 = (HtmlElement) null;
      try
      {
        htmlElement1 = this.htmlManagedDocForCallBack.GetElementById("mashupAction");
        htmlElement2 = this.htmlManagedDocForCallBack.GetElementById("mashupId");
      }
      catch (Exception ex)
      {
        Trace.WriteLine("inputElementClick_EventHandler callBack function failed.");
        Trace.WriteLine(ex.StackTrace);
      }
      Trace.Assert(htmlElement1.GetAttribute("value") != null, "mashupAction value is null. inputElementClick_EventHandler callBack function failed.");
      string attribute = htmlElement2.GetAttribute("value");
      QAFBuilderAction? nullable = Enum.Parse(typeof (QAFBuilderAction), htmlElement1.GetAttribute("value"), true) as QAFBuilderAction?;
      Trace.Assert(nullable.HasValue);
      if (nullable.Value != QAFBuilderAction.Close)
        Trace.Assert(htmlElement2.GetAttribute("value") != null, "mashupId value is null. inputElementClick_EventHandler callBack function failed.");
      WebBrowserProxy instance = WebBrowserProxy.getInstance();
      if (nullable.Value == QAFBuilderAction.Save)
      {
        if (this._hostOfEditor.ActionOfQAF == MashupActionCategory.CreateMashup)
        {
          instance.ConvertToCreationItemtoEditor(this._hostOfEditor, attribute);
          this.SynchronizeMashupServiceToLocal(attribute);
        }
        else
        {
          if (this._hostOfEditor.ActionOfQAF != MashupActionCategory.EditMashup)
            return;
          this.SynchronizeMashupServiceToLocal(this._hostOfEditor.MashupItemId);
        }
      }
      else if (nullable.Value == QAFBuilderAction.Close)
      {
        this.Close();
      }
      else
      {
        if (nullable.Value != QAFBuilderAction.SaveAndClose)
          return;
        this.Close();
        if (this._hostOfEditor.ActionOfQAF == MashupActionCategory.CreateMashup)
        {
          instance.ConvertToCreationItemtoEditorForSaveandClose(this._hostOfEditor, attribute);
          this.SynchronizeMashupServiceToLocal(attribute);
        }
        else
        {
          if (this._hostOfEditor.ActionOfQAF != MashupActionCategory.EditMashup)
            return;
          this.SynchronizeMashupServiceToLocal(this._hostOfEditor.MashupItemId);
        }
      }
    }

    private void SynchronizeMashupServiceToLocal(string strMashupServiceItemId)
    {
      ThreadPool.QueueUserWorkItem((WaitCallback) (state =>
      {
        string strXRepPathforFile = (string) null;
        if (this._hostOfEditor.MashupType.MashupCategory == OnlineMashupCategory.DataMashup || this._hostOfEditor.MashupType.MashupCategory == OnlineMashupCategory.HTMLMashup || this._hostOfEditor.MashupType.MashupCategory == OnlineMashupCategory.URLMashup)
          strXRepPathforFile = new JSONMashupPipeIdToXRepPathQueryHandler().getXRepPathForMashupPipeId(strMashupServiceItemId);
        else if (this._hostOfEditor.MashupType.MashupCategory == OnlineMashupCategory.RESTService || this._hostOfEditor.MashupType.MashupCategory == OnlineMashupCategory.RSSAtomService || this._hostOfEditor.MashupType.MashupCategory == OnlineMashupCategory.SOAPService)
          strXRepPathforFile = new JSONMashupServiceIdToXRepPathQueryHandler().getXRepPathForServiceId(strMashupServiceItemId);
        if (strXRepPathforFile == null)
          return;
        SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().SyncSingleFile(strXRepPathforFile);
      }));
    }

    private void webBrowserWinFormHost_FormClosed(object sender, FormClosedEventArgs e)
    {
      this.MonitorWebBrowserCloseEvent();
      GC.Collect();
      GC.WaitForPendingFinalizers();
    }

    private void MonitorWebBrowserCloseEvent()
    {
      WebBrowserProxy instance = WebBrowserProxy.getInstance();
      if (this._hostOfEditor.StatusOfEditor == EditorStatus.CreateMashupUI)
      {
        instance.RemoveCreationOnLineEditor(this._hostOfEditor);
      }
      else
      {
        if (this._hostOfEditor.StatusOfEditor != EditorStatus.EditMashupUI)
          return;
        instance.RemoveEditOnLineEditor(this._hostOfEditor.MashupItemId);
      }
    }

    private void timerOfProgress_Tick(object sender, EventArgs e)
    {
      if (this.toolStripProgressBarOfQAFbuilder.Value == this.toolStripProgressBarOfQAFbuilder.Maximum)
        this.toolStripProgressBarOfQAFbuilder.Value = 0;
      this.toolStripProgressBarOfQAFbuilder.Increment(2);
    }
  }
}
