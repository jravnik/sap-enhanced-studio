﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.INTERNET_PER_CONN_OPTION_OptionUnion
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper
{
  [StructLayout(LayoutKind.Explicit)]
  public struct INTERNET_PER_CONN_OPTION_OptionUnion
  {
    [FieldOffset(0)]
    public int dwValue;
    [FieldOffset(0)]
    public IntPtr pszValue;
    [FieldOffset(0)]
    public System.Runtime.InteropServices.ComTypes.FILETIME ftValue;
  }
}
