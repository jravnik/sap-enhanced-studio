﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.UtilExtension
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol.ConnectionModel;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  public static class UtilExtension
  {
    public static InternetProxy GetHttpProxy()
    {
      InternetProxy internetProxy = InternetProxy.NoProxy;
      if (PropertyAccess.GeneralProps.ProxySupport && PropertyAccess.GeneralProps.ProxyURL != null && PropertyAccess.GeneralProps.ProxyURL.Trim().Length > 0)
      {
        internetProxy = new InternetProxy()
        {
          Address = PropertyAccess.GeneralProps.ProxyURL
        };
        if (PropertyAccess.GeneralProps.ProxyAuthentication)
        {
          internetProxy.UserName = PropertyAccess.GeneralProps.ProxyUser;
          internetProxy.Password = SecureStringUtil.decrypt(PropertyAccess.GeneralProps.ProxyPwd);
        }
      }
      return internetProxy;
    }
  }
}
