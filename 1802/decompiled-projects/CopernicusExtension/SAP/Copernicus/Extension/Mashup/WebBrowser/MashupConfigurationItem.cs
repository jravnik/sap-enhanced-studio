﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.MashupConfigurationItem
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  public class MashupConfigurationItem
  {
    public static string strDirectURLForBYDSystem = "/sap/public/ap/ui/repository/SAP_UI/HTML5/client.html?";
    public static string strComponentParameterPrefix = "app.component=";
    public static string strInportParameterPrefix = "app.inport=";
    public static string strModeParameterPrefix = "param.$Mode=";
    public static string strEidtMashupPipeIDParameterPrefix = "param.$MashupPipeID=";
    public static string strEidtServiceIDParameterPrefix = "param.$ServiceID=";
    private OnlineMashupType mainType;
    private MashupActionCategory mashupCategory;
    private string strHtmlMashupFinalizedPageTileOrPattern;
    private AuthoringType authoringType;
    private bool isOverWriteSetting;
    private string strAdditionalPort;
    private Dictionary<string, string> dicForAddtionalParams;
    private string strAdditionalQAFBuilder;

    public OnlineMashupType MashupMainType
    {
      get
      {
        return this.mainType;
      }
    }

    public MashupActionCategory CategoryActionParameter
    {
      get
      {
        return this.mashupCategory;
      }
    }

    public string HtmlMashupFinalizedPageOrPattern
    {
      get
      {
        return this.strHtmlMashupFinalizedPageTileOrPattern;
      }
    }

    public MashupConfigurationItem(OnlineMashupType _mainType, MashupActionCategory _mashupCategory, string _strHtmlMashupFinalizedPageOrPattern, AuthoringType _authoringType, bool _isOverWriteSetting = false, string _strDataMashupNewEditQAF = null, string _strDataMashupNewEditInport = null, Dictionary<string, string> _dicForAddtionalParams = null)
    {
      this.mainType = _mainType;
      this.mashupCategory = _mashupCategory;
      this.strHtmlMashupFinalizedPageTileOrPattern = _strHtmlMashupFinalizedPageOrPattern;
      this.authoringType = _authoringType;
      if (!_isOverWriteSetting)
        return;
      this.isOverWriteSetting = _isOverWriteSetting;
      this.strAdditionalQAFBuilder = _strDataMashupNewEditQAF;
      this.strAdditionalPort = _strDataMashupNewEditInport;
      this.dicForAddtionalParams = _dicForAddtionalParams;
    }

    public override string ToString()
    {
      string directUrlForBydSystem = MashupConfigurationItem.strDirectURLForBYDSystem;
      string str = this.isOverWriteSetting ? directUrlForBydSystem + MashupConfigurationItem.strComponentParameterPrefix + this.strAdditionalQAFBuilder + "&" + MashupConfigurationItem.strInportParameterPrefix + this.strAdditionalPort + "&" : directUrlForBydSystem + MashupConfigurationItem.strComponentParameterPrefix + this.mainType.HtmlMashupQAFBuilder + "&" + MashupConfigurationItem.strInportParameterPrefix + this.mainType.HtmlMashupQAFInport + "&";
      if (this.mashupCategory == MashupActionCategory.CreateMashup)
      {
        if (this.isOverWriteSetting)
        {
          foreach (string key in this.dicForAddtionalParams.Keys)
            str = str + key + "=" + this.dicForAddtionalParams[key];
          str += "&";
        }
        str = str + MashupConfigurationItem.strModeParameterPrefix + "NEW";
      }
      else if (this.mashupCategory == MashupActionCategory.EditMashup)
      {
        str = str + MashupConfigurationItem.strModeParameterPrefix + "EDIT" + "&";
        if (this.isOverWriteSetting)
        {
          foreach (string key in this.dicForAddtionalParams.Keys)
            str = str + key + "=" + this.dicForAddtionalParams[key];
          str += "&";
        }
        if (this.authoringType == AuthoringType.MashupAuthoring)
          str += MashupConfigurationItem.strEidtMashupPipeIDParameterPrefix;
        else if (this.authoringType == AuthoringType.ServiceAuthoring)
          str += MashupConfigurationItem.strEidtServiceIDParameterPrefix;
      }
      return str;
    }
  }
}
