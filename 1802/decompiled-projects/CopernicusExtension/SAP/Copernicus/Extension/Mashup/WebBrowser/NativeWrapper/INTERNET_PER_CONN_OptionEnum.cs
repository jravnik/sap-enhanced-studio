﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.INTERNET_PER_CONN_OptionEnum
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper
{
  public enum INTERNET_PER_CONN_OptionEnum
  {
    INTERNET_PER_CONN_FLAGS = 1,
    INTERNET_PER_CONN_PROXY_SERVER = 2,
    INTERNET_PER_CONN_PROXY_BYPASS = 3,
    INTERNET_PER_CONN_AUTOCONFIG_URL = 4,
    INTERNET_PER_CONN_AUTODISCOVERY_FLAGS = 5,
    INTERNET_PER_CONN_AUTOCONFIG_SECONDARY_URL = 6,
    INTERNET_PER_CONN_AUTOCONFIG_RELOAD_DELAY_MINS = 7,
    INTERNET_PER_CONN_AUTOCONFIG_LAST_DETECT_TIME = 8,
    INTERNET_PER_CONN_AUTOCONFIG_LAST_DETECT_URL = 9,
    INTERNET_PER_CONN_FLAGS_UI = 10,
  }
}
