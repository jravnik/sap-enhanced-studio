﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.INTERNET_OPTION
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper
{
  public enum INTERNET_OPTION
  {
    INTERNET_OPTION_REFRESH = 37,
    INTERNET_OPTION_SETTINGS_CHANGED = 39,
    INTERNET_OPTION_PER_CONNECTION_OPTION = 75,
  }
}
