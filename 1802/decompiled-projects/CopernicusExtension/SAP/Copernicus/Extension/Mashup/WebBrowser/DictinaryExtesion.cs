﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.DictinaryExtesion
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  public static class DictinaryExtesion
  {
    public static Dictionary<string, MashupItemOnLineEditor> Clone(this Dictionary<string, MashupItemOnLineEditor> originDict)
    {
      Dictionary<string, MashupItemOnLineEditor> dictionary = new Dictionary<string, MashupItemOnLineEditor>();
      foreach (string key in originDict.Keys)
        dictionary.Add(key, originDict[key]);
      return dictionary;
    }

    public static List<MashupItemOnLineEditor> Clone(this List<MashupItemOnLineEditor> originList)
    {
      List<MashupItemOnLineEditor> itemOnLineEditorList = new List<MashupItemOnLineEditor>();
      foreach (MashupItemOnLineEditor origin in originList)
        itemOnLineEditorList.Add(origin);
      return itemOnLineEditorList;
    }
  }
}
