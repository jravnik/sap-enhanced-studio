﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.INTERNET_PER_CONN_OPTION_LIST
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper
{
  public struct INTERNET_PER_CONN_OPTION_LIST
  {
    public int Size;
    public IntPtr Connection;
    public int OptionCount;
    public int OptionError;
    public IntPtr pOptions;
  }
}
