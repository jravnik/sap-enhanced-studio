﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.INTERNET_OPEN_TYPE
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper
{
  public enum INTERNET_OPEN_TYPE
  {
    INTERNET_OPEN_TYPE_PRECONFIG = 0,
    INTERNET_OPEN_TYPE_DIRECT = 1,
    INTERNET_OPEN_TYPE_PROXY = 3,
    INTERNET_OPEN_TYPE_PRECONFIG_WITH_NO_AUTOPROXY = 4,
  }
}
