﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.OnlineMashupType
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  public class OnlineMashupType
  {
    private readonly OnlineMashupCategory _mashupCategory;
    private readonly string _strHtmlMashupQAFBuilder;
    private readonly string _strHtmlMashupQAFInport;
    private string _strMashupLogOnPageTitle;

    public OnlineMashupCategory MashupCategory
    {
      get
      {
        return this._mashupCategory;
      }
    }

    public string HtmlMashupQAFBuilder
    {
      get
      {
        return this._strHtmlMashupQAFBuilder;
      }
    }

    public string HtmlMashupQAFInport
    {
      get
      {
        return this._strHtmlMashupQAFInport;
      }
    }

    public string MashupTargetLogOnPageTitle
    {
      get
      {
        return this._strMashupLogOnPageTitle;
      }
    }

    public OnlineMashupType(OnlineMashupCategory _mashupCategory, string _strHtmlMashupQAFBuilder, string _strHtmlMashupQAFInport, string _strMashupLogOnPageTitle)
    {
      this._mashupCategory = _mashupCategory;
      this._strHtmlMashupQAFBuilder = _strHtmlMashupQAFBuilder;
      this._strHtmlMashupQAFInport = _strHtmlMashupQAFInport;
      this._strMashupLogOnPageTitle = _strMashupLogOnPageTitle;
    }
  }
}
