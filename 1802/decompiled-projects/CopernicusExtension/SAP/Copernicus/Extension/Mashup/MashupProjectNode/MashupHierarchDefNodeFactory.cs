﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.MashupHierarchDefNodeFactory
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Project;
using SAP.CopernicusProjectView;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode
{
  internal class MashupHierarchDefNodeFactory : ICopernicusHierarchyRootConfigurationNodeFactory, SMashupConfigurationNodeFactory
  {
    internal MashupHierarchDefNodeFactory()
    {
    }

    HierarchyNode ICopernicusHierarchyRootConfigurationNodeFactory.CreateNode(CopernicusProjectNode root, out HierarchyNode rootOfMashupNode, out HierarchyNode rootOfServiceNode, out HierarchyNode rootOfPortBindingNode)
    {
      HierarchyNode hierarchyNode = (HierarchyNode) new MashupHierarchDefNode((ProjectNode) root, MashupHierarchDefCategory.MashupConfiguration);
      rootOfMashupNode = MashupHierarchDefNode.MashupAuthoringReferenceNode;
      rootOfServiceNode = MashupHierarchDefNode.MashupServiceReferenceNode;
      rootOfPortBindingNode = MashupHierarchDefNode.MashupPortBindingReferenceNode;
      return hierarchyNode;
    }
  }
}
