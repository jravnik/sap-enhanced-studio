﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler.DictinaryExtesion
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Collections.Generic;
using System.IO;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler
{
  public static class DictinaryExtesion
  {
    public static Dictionary<string, FileSystemWatcher> Clone(this Dictionary<string, FileSystemWatcher> originDict)
    {
      Dictionary<string, FileSystemWatcher> dictionary = new Dictionary<string, FileSystemWatcher>();
      foreach (string key in originDict.Keys)
        dictionary.Add(key, originDict[key]);
      return dictionary;
    }
  }
}
