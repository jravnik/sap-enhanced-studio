﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.MashupHierarchDefNode
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Project.Automation;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Extension.Mashup.MashupProjectNode.NodeMenusFilter;
using SAP.Copernicus.Extension.Mashup.Utils;
using SAP.Copernicus.Extension.Mashup.WebBrowser;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode
{
  [ComVisible(true)]
  [CLSCompliant(false)]
  public class MashupHierarchDefNode : HierarchyNode
  {
    private static string _strMashupAuthoringFolder = "\\Mashups\\Pipes";
    private static string _strServiceAuthoringFolder = "\\Mashups\\Services";
    private static string _strPortBindingFolder = "\\Mashups\\PortBindings";
    private static string strServiceMashupItemTemplateCategory = "Mashups and Web Services";
    public static string strServiceMashupItemTemplateName = "RSS/Atom Mashup Web Service";
    public static string strHtmlMashupItemTemplateName = "Html Mashup";
    public static string strUrlMashupItemTemplateName = "Url Mashup";
    public static string strDataMashupItemTemplateName = "Data Mashup";
    public static string strPortBindingItemTemplateName = "Mashup Port Binding";
    public static string strRestServiceItemTemplateName = "REST Mashup Web Service";
    public static string strSoapServiceItemTemplateName = "SOAP Mashup Web Service";
    private static HierarchyNode _mashupAuthoringHierarchyNode;
    private static HierarchyNode _mashupServiceHierarchyNode;
    private static HierarchyNode _mashupPortBindingHierarchyNode;
    private MashupHierarchDefCategory refCategory;

    public static HierarchyNode MashupAuthoringReferenceNode
    {
      get
      {
        return MashupHierarchDefNode._mashupAuthoringHierarchyNode;
      }
    }

    public static HierarchyNode MashupServiceReferenceNode
    {
      get
      {
        return MashupHierarchDefNode._mashupServiceHierarchyNode;
      }
    }

    public static HierarchyNode MashupPortBindingReferenceNode
    {
      get
      {
        return MashupHierarchDefNode._mashupPortBindingHierarchyNode;
      }
    }

    public MashupHierarchDefNode(ProjectNode root, MashupHierarchDefCategory category = MashupHierarchDefCategory.MashupConfiguration)
      : base(root)
    {
      this.ExcludeNodeFromScc = true;
      this.refCategory = category;
      this.VirtualNodeName = category.ToString();
      if (this.refCategory != MashupHierarchDefCategory.MashupConfiguration)
        return;
      MashupHierarchDefNode._mashupAuthoringHierarchyNode = (HierarchyNode) new MashupHierarchDefNode(root, MashupHierarchDefCategory.MashupAuthoring);
      MashupHierarchDefNode._mashupAuthoringHierarchyNode.IsExpanded = false;
      this.AddChild(MashupHierarchDefNode._mashupAuthoringHierarchyNode);
      MashupHierarchDefNode._mashupServiceHierarchyNode = (HierarchyNode) new MashupHierarchDefNode(root, MashupHierarchDefCategory.WebServiceAuthoring);
      MashupHierarchDefNode._mashupServiceHierarchyNode.IsExpanded = false;
      this.AddChild(MashupHierarchDefNode._mashupServiceHierarchyNode);
      MashupHierarchDefNode._mashupPortBindingHierarchyNode = (HierarchyNode) new MashupHierarchDefNode(root, MashupHierarchDefCategory.PortBinding);
      MashupHierarchDefNode._mashupPortBindingHierarchyNode.IsExpanded = false;
      this.AddChild(MashupHierarchDefNode._mashupPortBindingHierarchyNode);
    }

    public override int SortPriority
    {
      get
      {
        return 300;
      }
    }

    public override int MenuCommandId
    {
      get
      {
        return 1104;
      }
    }

    public override Guid ItemTypeGuid
    {
      get
      {
        return VSConstants.GUID_ItemType_VirtualFolder;
      }
    }

    public override string Url
    {
      get
      {
        string projectFolder = this.ProjectMgr.ProjectFolder;
        if (this.refCategory == MashupHierarchDefCategory.MashupAuthoring)
          projectFolder += MashupHierarchDefNode._strMashupAuthoringFolder;
        else if (this.refCategory == MashupHierarchDefCategory.WebServiceAuthoring)
          projectFolder += MashupHierarchDefNode._strServiceAuthoringFolder;
        else if (this.refCategory == MashupHierarchDefCategory.PortBinding)
          projectFolder += MashupHierarchDefNode._strPortBindingFolder;
        return new Microsoft.VisualStudio.Shell.Url(projectFolder).AbsoluteUrl;
      }
    }

    public string RootProjectFolder
    {
      get
      {
        return this.ProjectMgr.ProjectFolder;
      }
    }

    public override string Caption
    {
      get
      {
        return this.refCategory.ToString();
      }
    }

    public override object GetAutomationObject()
    {
      if (this.ProjectMgr == null || this.ProjectMgr.IsClosed)
        return (object) null;
      return (object) new OAProjectItem<MashupHierarchDefNode>(this.ProjectMgr.GetAutomationObject() as OAProject, this);
    }

    public override string GetEditLabel()
    {
      return (string) null;
    }

    public override object GetIconHandle(bool open)
    {
      if (this.refCategory == MashupHierarchDefCategory.MashupConfiguration)
        return (object) SAP.Copernicus.Extension.Mashup.Resource.MashupRootNode.GetHicon();
      if (this.refCategory == MashupHierarchDefCategory.MashupAuthoring)
        return (object) SAP.Copernicus.Extension.Mashup.Resource.MashupComponentNode.GetHicon();
      if (this.refCategory == MashupHierarchDefCategory.WebServiceAuthoring)
        return (object) SAP.Copernicus.Extension.Mashup.Resource.MashupServiceNode.GetHicon();
      if (this.refCategory == MashupHierarchDefCategory.PortBinding)
        return (object) SAP.Copernicus.Extension.Mashup.Resource.MashupComponentNode.GetHicon();
      return base.GetIconHandle(open);
    }

    protected override StringBuilder PrepareSelectedNodesForClipBoard()
    {
      return (StringBuilder) null;
    }

    protected override int ExcludeFromProject()
    {
      return -2147221248;
    }

    protected override int QueryStatusOnNode(Guid cmdGroup, uint cmd, IntPtr pCmdText, ref QueryStatusResult result)
    {
      int num = 0;
      if (cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupAuthoringNode && this.refCategory == MashupHierarchDefCategory.MashupAuthoring)
      {
        result = XRepMapper.GetInstance().IsSolutionEditable() ? QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED : QueryStatusResult.SUPPORTED | QueryStatusResult.NINCHED;
        WebBrowserProxy.getInstance().AsynInvokeSessionValidation();
      }
      else if (cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupServiceNode && this.refCategory == MashupHierarchDefCategory.WebServiceAuthoring)
      {
        result = XRepMapper.GetInstance().IsSolutionEditable() ? QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED : QueryStatusResult.SUPPORTED | QueryStatusResult.NINCHED;
        WebBrowserProxy.getInstance().AsynInvokeSessionValidation();
      }
      else if ((cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupAuthoringNode || cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupServiceNode) && this.refCategory == MashupHierarchDefCategory.MashupConfiguration)
      {
        result = XRepMapper.GetInstance().IsSolutionEditable() ? QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED : QueryStatusResult.SUPPORTED | QueryStatusResult.NINCHED;
        WebBrowserProxy.getInstance().AsynInvokeSessionValidation();
      }
      else if (cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.guidCopernicusCmdSet)
      {
        if ((int) cmd == 401)
          result |= QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
      }
      else
      {
        if (!(cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupPortBindingNode) || this.refCategory != MashupHierarchDefCategory.PortBinding && this.refCategory != MashupHierarchDefCategory.MashupConfiguration)
          return MashupNodeMenusFilter.FilterQueryStatusOnNode(cmdGroup, cmd, pCmdText, ref result);
        if ((int) cmd == 1025)
        {
          if (!XRepMapper.GetInstance().IsSolutionEditable())
            result = QueryStatusResult.SUPPORTED | QueryStatusResult.NINCHED;
          else
            result |= QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
        }
      }
      return num;
    }

    protected override int ExecCommandOnNode(Guid cmdGroup, uint cmd, uint nCmdexecopt, IntPtr pvaIn, IntPtr pvaOut)
    {
      if ((cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupCommandNode || cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupAuthoringNode || cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupServiceNode) && ((int) cmd == 258 || (int) cmd == 256 || ((int) cmd == 514 || (int) cmd == 513) || ((int) cmd == 515 || (int) cmd == 257)))
      {
        string EV_SUCCESS;
        string EV_ENABLED;
        MashupNodeMenusFilter.isKeyUserEnabled(out EV_SUCCESS, out EV_ENABLED);
        if (EV_SUCCESS != "X" || EV_ENABLED != "X")
        {
          if (!CopernicusMessageBox.Show(SAP.Copernicus.Extension.Mashup.Resource.EnableAdminMode, SAP.Copernicus.Extension.Mashup.Resource.MsgBYD, MessageBoxButtons.YesNo).Equals((object) DialogResult.Yes))
            return 0;
          MashupNodeMenusFilter.setKeyUser("X");
        }
      }
      if ((cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupAuthoringNode || cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupServiceNode || cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupPortBindingNode) && this.refCategory == MashupHierarchDefCategory.MashupConfiguration)
      {
        switch (cmd)
        {
          case 256:
            GlobalVSPackageHelper.InvokeAddNewItemWizard(MashupHierarchDefNode.strServiceMashupItemTemplateCategory, MashupHierarchDefNode.strHtmlMashupItemTemplateName);
            return 0;
          case 257:
            GlobalVSPackageHelper.InvokeAddNewItemWizard(MashupHierarchDefNode.strServiceMashupItemTemplateCategory, MashupHierarchDefNode.strUrlMashupItemTemplateName);
            return 0;
          case 258:
            GlobalVSPackageHelper.InvokeAddNewItemWizard(MashupHierarchDefNode.strServiceMashupItemTemplateCategory, MashupHierarchDefNode.strDataMashupItemTemplateName);
            return 0;
          case 513:
            GlobalVSPackageHelper.InvokeAddNewItemWizard(MashupHierarchDefNode.strServiceMashupItemTemplateCategory, MashupHierarchDefNode.strServiceMashupItemTemplateName);
            return 0;
          case 514:
            GlobalVSPackageHelper.InvokeAddNewItemWizard(MashupHierarchDefNode.strServiceMashupItemTemplateCategory, MashupHierarchDefNode.strRestServiceItemTemplateName);
            return 0;
          case 515:
            GlobalVSPackageHelper.InvokeAddNewItemWizard(MashupHierarchDefNode.strServiceMashupItemTemplateCategory, MashupHierarchDefNode.strSoapServiceItemTemplateName);
            return 0;
          case 1025:
            GlobalVSPackageHelper.InvokeAddNewItemWizard(MashupHierarchDefNode.strServiceMashupItemTemplateCategory, MashupHierarchDefNode.strPortBindingItemTemplateName);
            return 0;
        }
      }
      else if (cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupAuthoringNode && this.refCategory == MashupHierarchDefCategory.MashupAuthoring)
      {
        switch (cmd)
        {
          case 256:
            WebBrowserProxy.getInstance().AsynCreateMashupItemOnLine(WebBrowserProxy.HtmlMashupStringTypeOnline, string.Empty);
            return 0;
          case 257:
            WebBrowserProxy.getInstance().AsynCreateMashupItemOnLine(WebBrowserProxy.UrlMashupStringTypeOnline, string.Empty);
            return 0;
          case 258:
            WebBrowserProxy.getInstance().AsynCreateMashupItemOnLine(WebBrowserProxy.DataMashupStringTypeOnline, string.Empty);
            return 0;
        }
      }
      else if (this.refCategory == MashupHierarchDefCategory.WebServiceAuthoring)
      {
        switch (cmd)
        {
          case 513:
            WebBrowserProxy.getInstance().AsynCreateMashupItemOnLine(WebBrowserProxy.RssAtomServiceStringTypeOnline, string.Empty);
            return 0;
          case 514:
            WebBrowserProxy.getInstance().AsynCreateMashupItemOnLine(WebBrowserProxy.RestServiceStringTypeOnline, string.Empty);
            return 0;
          case 515:
            WebBrowserProxy.getInstance().AsynCreateMashupItemOnLine(WebBrowserProxy.SoapServiceStringTypeOnline, string.Empty);
            return 0;
        }
      }
      else if (cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupPortBindingNode && this.refCategory == MashupHierarchDefCategory.PortBinding && (int) cmd == 1025)
      {
        GlobalVSPackageHelper.InvokeAddNewItemWizard(MashupHierarchDefNode.strServiceMashupItemTemplateCategory, MashupHierarchDefNode.strPortBindingItemTemplateName);
        return 0;
      }
      return -2147221248;
    }

    protected override bool CanShowDefaultIcon()
    {
      return !string.IsNullOrEmpty(this.VirtualNodeName);
    }

    protected override bool CanDeleteItem(__VSDELETEITEMOPERATION deleteOperation)
    {
      return false;
    }

    protected override void UpdateSccStateIcons()
    {
      base.UpdateSccStateIcons();
      if (this.FirstChild == null)
        return;
      for (HierarchyNode hierarchyNode1 = this.FirstChild; hierarchyNode1 != null; hierarchyNode1 = hierarchyNode1.NextSibling)
      {
        if (hierarchyNode1.FirstChild != null)
        {
          for (HierarchyNode hierarchyNode2 = hierarchyNode1.FirstChild; hierarchyNode2 != null; hierarchyNode2 = hierarchyNode2.NextSibling)
            (hierarchyNode2 as AuthoringMashupFileNode).updateSccStateIcons();
        }
      }
    }

    public HierarchyNode CreateSubMashupOrServiceAuthoringItem(string strMashupItemFullPath, MashupHierarchDefCategory category = MashupHierarchDefCategory.MashupAuthoring)
    {
      FileInfo fileInfo = new FileInfo(strMashupItemFullPath);
      HierarchyNode _newMashupFileNode = (HierarchyNode) null;
      if (fileInfo.FullName.Contains(MashupHierarchDefNode._strMashupAuthoringFolder))
      {
        CopernicusProjectNode projectMgr = this.ProjectMgr as CopernicusProjectNode;
        if (category == MashupHierarchDefCategory.MashupAuthoring)
          projectMgr.CreateMashupComponentFileReference(strMashupItemFullPath, MashupHierarchDefNode.MashupAuthoringReferenceNode, out _newMashupFileNode);
      }
      if (fileInfo.FullName.Contains(MashupHierarchDefNode._strServiceAuthoringFolder))
      {
        CopernicusProjectNode projectMgr = this.ProjectMgr as CopernicusProjectNode;
        if (category == MashupHierarchDefCategory.WebServiceAuthoring)
          projectMgr.CreateMashupComponentFileReference(strMashupItemFullPath, MashupHierarchDefNode.MashupServiceReferenceNode, out _newMashupFileNode);
      }
      if (fileInfo.FullName.Contains(MashupHierarchDefNode._strPortBindingFolder))
      {
        CopernicusProjectNode projectMgr = this.ProjectMgr as CopernicusProjectNode;
        if (category == MashupHierarchDefCategory.PortBinding)
          projectMgr.CreateMashupComponentFileReference(strMashupItemFullPath, MashupHierarchDefNode.MashupPortBindingReferenceNode, out _newMashupFileNode);
      }
      return _newMashupFileNode;
    }

    public void DeleteSubMashupOrServiceAuthoringItem(string strMashupItemFullPath, MashupHierarchDefCategory category = MashupHierarchDefCategory.MashupAuthoring)
    {
      FileInfo fileInfo = new FileInfo(strMashupItemFullPath);
      if (fileInfo.FullName.Contains(MashupHierarchDefNode._strMashupAuthoringFolder))
      {
        CopernicusProjectNode projectMgr = this.ProjectMgr as CopernicusProjectNode;
        if (category == MashupHierarchDefCategory.MashupAuthoring)
        {
          projectMgr.DeleteMashupComponentFileReference(strMashupItemFullPath, MashupHierarchDefNode.MashupAuthoringReferenceNode);
          AuthoringMashupFileNode.IsDeleteItemInProcess = false;
        }
      }
      if (fileInfo.FullName.Contains(MashupHierarchDefNode._strServiceAuthoringFolder))
      {
        CopernicusProjectNode projectMgr = this.ProjectMgr as CopernicusProjectNode;
        if (category == MashupHierarchDefCategory.WebServiceAuthoring)
        {
          projectMgr.DeleteMashupComponentFileReference(strMashupItemFullPath, MashupHierarchDefNode.MashupServiceReferenceNode);
          AuthoringMashupFileNode.IsDeleteItemInProcess = false;
        }
      }
      if (!fileInfo.FullName.Contains(MashupHierarchDefNode._strPortBindingFolder))
        return;
      CopernicusProjectNode projectMgr1 = this.ProjectMgr as CopernicusProjectNode;
      if (category != MashupHierarchDefCategory.PortBinding)
        return;
      projectMgr1.DeleteMashupComponentFileReference(strMashupItemFullPath, MashupHierarchDefNode.MashupPortBindingReferenceNode);
      AuthoringMashupFileNode.IsDeleteItemInProcess = false;
    }
  }
}
