﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.NodeMenusFilter.MashupNodeMenusFilter
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Project;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.PDI;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode.NodeMenusFilter
{
  public static class MashupNodeMenusFilter
  {
    private static readonly Guid cmdSetBCSetNode = new Guid("E5672465-797A-4ff4-BF22-6D31E3F9F51C");
    private static readonly Guid cmdSetBCNode = new Guid("221C0406-59DC-42cc-BB9C-8D855187B091");
    private static readonly Guid cmdSetBCNode2 = new Guid("A9D6EEAF-5450-43f5-9B8D-E2E138952278");
    private static readonly Guid cmdSetBCNode3 = new Guid("318A10C3-9B29-46b6-BA3A-A08513C67F18");
    private static readonly Guid cmdSetBCNode4 = new Guid("318A10C3-9B29-46b6-BA3A-A08513C67F16");
    private static readonly Guid cmdSetBCImplProjectTempl = new Guid("64576903-2003-4A69-A93D-2262A6A32358");
    private static readonly Guid cmdSetBCO = new Guid("48FE391B-E64C-49E6-ADBC-7B3CADA872CC");

    public static int FilterQueryStatusOnNode(Guid cmdGroup, uint cmd, IntPtr pCmdText, ref QueryStatusResult result)
    {
      if (cmdGroup == VsMenus.guidStandardCommandSet2K && (int) cmd == 600)
      {
        result |= QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
        return 0;
      }
      if (!(cmdGroup == MashupNodeMenusFilter.cmdSetBCSetNode) && !(cmdGroup == MashupNodeMenusFilter.cmdSetBCNode) && (!(cmdGroup == MashupNodeMenusFilter.cmdSetBCNode2) && !(cmdGroup == MashupNodeMenusFilter.cmdSetBCNode3)) && (!(cmdGroup == MashupNodeMenusFilter.cmdSetBCNode4) && !(cmdGroup == MashupNodeMenusFilter.cmdSetBCImplProjectTempl) && (!(cmdGroup == MashupNodeMenusFilter.cmdSetBCO) && !(cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupUsage))))
        return -2147221248;
      result = QueryStatusResult.SUPPORTED | QueryStatusResult.INVISIBLE;
      return 0;
    }

    public static void isKeyUserEnabled(out string EV_SUCCESS, out string EV_ENABLED)
    {
      PDI_RI_S_MESSAGE[] ET_MESSAGES;
      new SolutionHandler().IsKeyUserEnabled(CopernicusProjectSystemUtil.GetCurrentSolutionName(), Connection.getInstance().getLoggedInUser().ToUpper().ToUpper(), out ET_MESSAGES, out EV_SUCCESS, out EV_ENABLED);
      List<Task> taskList = new List<Task>();
      foreach (PDI_RI_S_MESSAGE pdiRiSMessage in ET_MESSAGES)
        taskList.Add(new Task(pdiRiSMessage.TEXT, Origin.Unknown, Severity.Error, (EventHandler) null));
      ErrorSink.Instance.AddTasks((IEnumerable<Task>) taskList);
    }

    public static void setKeyUser(string enable)
    {
      PDI_RI_S_MESSAGE[] ET_MESSAGES;
      string EV_SUCCESS;
      new SolutionHandler().SetKeyUser(CopernicusProjectSystemUtil.GetCurrentSolutionName(), Connection.getInstance().getLoggedInUser().ToUpper().ToUpper(), enable, out ET_MESSAGES, out EV_SUCCESS);
      List<Task> taskList = new List<Task>();
      foreach (PDI_RI_S_MESSAGE pdiRiSMessage in ET_MESSAGES)
        taskList.Add(new Task(pdiRiSMessage.TEXT, Origin.Unknown, Severity.Error, (EventHandler) null));
      ErrorSink.Instance.AddTasks((IEnumerable<Task>) taskList);
    }
  }
}
