﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler.MashupItemSynchronizer
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Project;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Permissions;
using System.Threading;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler
{
  public class MashupItemSynchronizer
  {
    private static Dictionary<string, FileSystemWatcher> watchers = new Dictionary<string, FileSystemWatcher>();
    private static List<string> handled = new List<string>();
    private static List<string> validpostFixs = new List<string>();

    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public static void regiterFileChangeMonitor(string strRootProjectFolder, string strRootFolderOfMashupItems, string strPostFix)
    {
      if (!Directory.Exists(strRootFolderOfMashupItems))
        return;
      Monitor.Enter((object) MashupItemSynchronizer.watchers);
      FileSystemWatcher fileSystemWatcher1 = (FileSystemWatcher) null;
      if (!MashupItemSynchronizer.watchers.TryGetValue(strRootFolderOfMashupItems, out fileSystemWatcher1))
      {
        FileSystemWatcher fileSystemWatcher2 = new FileSystemWatcher();
        fileSystemWatcher2.Path = strRootFolderOfMashupItems;
        fileSystemWatcher2.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.LastWrite | NotifyFilters.LastAccess;
        fileSystemWatcher2.Filter = "*" + strPostFix;
        if (!MashupItemSynchronizer.validpostFixs.Contains(strPostFix))
          MashupItemSynchronizer.validpostFixs.Add(strPostFix);
        fileSystemWatcher2.Created += new FileSystemEventHandler(MashupItemSynchronizer.OnCreated);
        fileSystemWatcher2.Changed += new FileSystemEventHandler(MashupItemSynchronizer.OnChanged);
        fileSystemWatcher2.Deleted += new FileSystemEventHandler(MashupItemSynchronizer.OnDeleted);
        fileSystemWatcher2.Renamed += new RenamedEventHandler(MashupItemSynchronizer.OnRenamed);
        fileSystemWatcher2.EnableRaisingEvents = true;
        MashupItemSynchronizer.watchers.Add(strRootFolderOfMashupItems, fileSystemWatcher2);
      }
      Monitor.Exit((object) MashupItemSynchronizer.watchers);
    }

    private static bool ProcessCreationDeletionForCategory(HierarchyNode _componentRoot, string strLocalUIAuthoringFullName, AuthoringType cType, WatcherChangeTypes currentChangeType, out HierarchyNode _newMashupNode)
    {
      bool flag = false;
      HierarchyNode hierarchyNode1 = (HierarchyNode) null;
      _newMashupNode = (HierarchyNode) null;
      Trace.Assert(_componentRoot != null, "Root Component of MashupItem for Deletion or Creation Shouldn't be null - MashupItemSynchronizer Line 111");
      for (HierarchyNode hierarchyNode2 = _componentRoot.FirstChild; hierarchyNode2 != null; hierarchyNode2 = hierarchyNode2.NextSibling)
      {
        if (hierarchyNode2.Url.Equals(strLocalUIAuthoringFullName))
        {
          hierarchyNode1 = hierarchyNode2;
          flag = true;
          break;
        }
      }
      if (hierarchyNode1 != null)
      {
        if (currentChangeType == WatcherChangeTypes.Deleted)
        {
          MashupHierarchDefNode mashupHierarchDefNode = _componentRoot as MashupHierarchDefNode;
          if (cType == AuthoringType.MashupAuthoring)
            mashupHierarchDefNode.DeleteSubMashupOrServiceAuthoringItem(strLocalUIAuthoringFullName, MashupHierarchDefCategory.MashupAuthoring);
          else if (cType == AuthoringType.ServiceAuthoring)
            mashupHierarchDefNode.DeleteSubMashupOrServiceAuthoringItem(strLocalUIAuthoringFullName, MashupHierarchDefCategory.WebServiceAuthoring);
          else if (cType == AuthoringType.MashupPortBinding)
            mashupHierarchDefNode.DeleteSubMashupOrServiceAuthoringItem(strLocalUIAuthoringFullName, MashupHierarchDefCategory.PortBinding);
        }
      }
      else if (currentChangeType == WatcherChangeTypes.Created)
      {
        MashupHierarchDefNode mashupHierarchDefNode = _componentRoot as MashupHierarchDefNode;
        if (cType == AuthoringType.MashupAuthoring)
          _newMashupNode = mashupHierarchDefNode.CreateSubMashupOrServiceAuthoringItem(strLocalUIAuthoringFullName, MashupHierarchDefCategory.MashupAuthoring);
        else if (cType == AuthoringType.ServiceAuthoring)
          _newMashupNode = mashupHierarchDefNode.CreateSubMashupOrServiceAuthoringItem(strLocalUIAuthoringFullName, MashupHierarchDefCategory.WebServiceAuthoring);
        else if (cType == AuthoringType.MashupPortBinding)
          _newMashupNode = mashupHierarchDefNode.CreateSubMashupOrServiceAuthoringItem(strLocalUIAuthoringFullName, MashupHierarchDefCategory.PortBinding);
      }
      return flag;
    }

    private static void OnRenamed(object sender, RenamedEventArgs e)
    {
      if (SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getMashupCachedFileAttributes(e.FullPath).Count == 0)
        return;
      new FileInfo(e.FullPath).MoveTo(e.OldFullPath);
    }

    private static bool PreCheckupforLocalNotManualEvents(FileSystemEventArgs e, out AuthoringType _cType)
    {
      bool flag1 = true;
      string str1 = e.FullPath.Substring(e.FullPath.IndexOf("."));
      Trace.Assert(str1.EndsWith(".uimashup"));
      _cType = AuthoringType.NoSupportedAuthoring;
      IDictionary<string, string> cachedFileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getMashupCachedFileAttributes(e.FullPath);
      if (e.ChangeType == WatcherChangeTypes.Created)
      {
        if (cachedFileAttributes.Count != 0)
          ;
      }
      else if (e.ChangeType != WatcherChangeTypes.Changed && e.ChangeType == WatcherChangeTypes.Deleted)
      {
        if (cachedFileAttributes.Count > 0)
        {
          string str2 = (string) null;
          cachedFileAttributes.TryGetValue("MASHUP_TYPE", out str2);
          if (str2 != "Port_Binding")
          {
            string str3 = (string) null;
            string str4 = (string) null;
            cachedFileAttributes.TryGetValue("RefreashCase", out str3);
            cachedFileAttributes.TryGetValue("UpdateCase", out str4);
          }
        }
        if (cachedFileAttributes.Count == 0)
        {
          flag1 = false;
          for (HierarchyNode hierarchyNode = MashupHierarchDefNode.MashupAuthoringReferenceNode.FirstChild; hierarchyNode != null; hierarchyNode = hierarchyNode.NextSibling)
          {
            if (hierarchyNode.Url.Equals(e.FullPath))
              flag1 = true;
          }
          for (HierarchyNode hierarchyNode = MashupHierarchDefNode.MashupServiceReferenceNode.FirstChild; hierarchyNode != null; hierarchyNode = hierarchyNode.NextSibling)
          {
            if (hierarchyNode.Url.Equals(e.FullPath))
              flag1 = true;
          }
          for (HierarchyNode hierarchyNode = MashupHierarchDefNode.MashupPortBindingReferenceNode.FirstChild; hierarchyNode != null; hierarchyNode = hierarchyNode.NextSibling)
          {
            if (hierarchyNode.Url.Equals(e.FullPath))
              flag1 = true;
          }
          if (!flag1)
            return flag1;
        }
      }
      if (e.FullPath.EndsWith("MC.uimashup"))
        _cType = AuthoringType.MashupAuthoring;
      else if (e.FullPath.EndsWith("WS.uimashup"))
        _cType = AuthoringType.ServiceAuthoring;
      else if (e.FullPath.EndsWith("PB.uimashup"))
        _cType = AuthoringType.MashupPortBinding;
      Trace.Assert(_cType != AuthoringType.NoSupportedAuthoring);
      if (!MashupItemSynchronizer.validpostFixs.Contains(str1))
      {
        bool flag2;
        return flag2 = false;
      }
      return flag1;
    }

    private static void OnCreated(object source, FileSystemEventArgs e)
    {
      AuthoringType _cType = AuthoringType.NoSupportedAuthoring;
      if (!MashupItemSynchronizer.PreCheckupforLocalNotManualEvents(e, out _cType))
        return;
      IDictionary<string, string> cachedFileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getMashupCachedFileAttributes(e.FullPath);
      string str1;
      cachedFileAttributes.TryGetValue("CreationCase", out str1);
      string str2;
      cachedFileAttributes.TryGetValue("RefreshCase", out str2);
      if (str1 != null && str1.Equals(bool.TrueString))
      {
        Monitor.Enter((object) MashupItemSynchronizer.handled);
        DirectoryEvent directoryEvent = new DirectoryEvent(new FileInfo(e.FullPath), e.ChangeType);
        if (MashupItemSynchronizer.handled.Contains(directoryEvent.ToString()))
        {
          Monitor.Exit((object) MashupItemSynchronizer.handled);
        }
        else
        {
          MashupItemSynchronizer.handled.Add(directoryEvent.ToString());
          Monitor.Exit((object) MashupItemSynchronizer.handled);
        }
      }
      HierarchyNode _newMashupNode = (HierarchyNode) null;
      if (_cType == AuthoringType.MashupAuthoring)
        MashupItemSynchronizer.ProcessCreationDeletionForCategory(MashupHierarchDefNode.MashupAuthoringReferenceNode, e.FullPath, _cType, e.ChangeType, out _newMashupNode);
      else if (_cType == AuthoringType.ServiceAuthoring)
        MashupItemSynchronizer.ProcessCreationDeletionForCategory(MashupHierarchDefNode.MashupServiceReferenceNode, e.FullPath, _cType, e.ChangeType, out _newMashupNode);
      else if (_cType == AuthoringType.MashupPortBinding)
        MashupItemSynchronizer.ProcessCreationDeletionForCategory(MashupHierarchDefNode.MashupPortBindingReferenceNode, e.FullPath, _cType, e.ChangeType, out _newMashupNode);
      AuthoringMashupFileNode authoringMashupFileNode = _newMashupNode as AuthoringMashupFileNode;
      Trace.Assert(authoringMashupFileNode != null);
      if (_cType == AuthoringType.MashupPortBinding)
        return;
      authoringMashupFileNode.SynUpdateDisplayNameMetaData();
    }

    private static void OnDeleted(object source, FileSystemEventArgs e)
    {
      AuthoringType _cType = AuthoringType.NoSupportedAuthoring;
      if (!MashupItemSynchronizer.PreCheckupforLocalNotManualEvents(e, out _cType))
        return;
      IDictionary<string, string> cachedFileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getMashupCachedFileAttributes(e.FullPath);
      string empty = string.Empty;
      cachedFileAttributes.TryGetValue("MASHUP_TYPE", out empty);
      Monitor.Enter((object) MashupItemSynchronizer.handled);
      DirectoryEvent directoryEvent = new DirectoryEvent(new FileInfo(e.FullPath), e.ChangeType);
      if (MashupItemSynchronizer.handled.Contains(directoryEvent.ToString()))
      {
        MashupItemSynchronizer.handled.Remove(directoryEvent.ToString());
        Monitor.Exit((object) MashupItemSynchronizer.handled);
      }
      else
      {
        MashupItemSynchronizer.handled.Add(directoryEvent.ToString());
        Monitor.Exit((object) MashupItemSynchronizer.handled);
      }
      HierarchyNode _newMashupNode = (HierarchyNode) null;
      if (_cType == AuthoringType.MashupAuthoring)
        MashupItemSynchronizer.ProcessCreationDeletionForCategory(MashupHierarchDefNode.MashupAuthoringReferenceNode, e.FullPath, _cType, e.ChangeType, out _newMashupNode);
      else if (_cType == AuthoringType.ServiceAuthoring)
        MashupItemSynchronizer.ProcessCreationDeletionForCategory(MashupHierarchDefNode.MashupServiceReferenceNode, e.FullPath, _cType, e.ChangeType, out _newMashupNode);
      else if (_cType == AuthoringType.MashupPortBinding)
        MashupItemSynchronizer.ProcessCreationDeletionForCategory(MashupHierarchDefNode.MashupPortBindingReferenceNode, e.FullPath, _cType, e.ChangeType, out _newMashupNode);
      if (MashupHierarchDefNode.MashupServiceReferenceNode.FirstChild != null || MashupHierarchDefNode.MashupAuthoringReferenceNode.FirstChild != null || MashupHierarchDefNode.MashupPortBindingReferenceNode.FirstChild != null)
        return;
      HierarchyNode parent1 = MashupHierarchDefNode.MashupServiceReferenceNode.Parent;
      HierarchyNode parent2 = parent1.Parent;
      MashupHierarchDefNode.MashupServiceReferenceNode.Remove(false);
      MashupHierarchDefNode.MashupAuthoringReferenceNode.Remove(false);
      MashupHierarchDefNode.MashupPortBindingReferenceNode.Remove(false);
      parent1.Remove(false);
      parent2.ReloadItem(parent2.ID, 0U);
      (parent2 as CopernicusProjectNode).ResetMashupHierarchyNode();
    }

    private static void OnChanged(object source, FileSystemEventArgs e)
    {
      AuthoringType _cType = AuthoringType.NoSupportedAuthoring;
      if (!MashupItemSynchronizer.PreCheckupforLocalNotManualEvents(e, out _cType))
        return;
      string str;
      SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getMashupCachedFileAttributes(e.FullPath).TryGetValue("UpdateCase", out str);
      if (str == null || !str.Equals(bool.TrueString))
        return;
      HierarchyNode hierarchyNode1 = (HierarchyNode) null;
      bool flag = false;
      for (HierarchyNode hierarchyNode2 = MashupHierarchDefNode.MashupAuthoringReferenceNode.FirstChild; hierarchyNode2 != null; hierarchyNode2 = hierarchyNode2.NextSibling)
      {
        if (hierarchyNode2.Url.Equals(e.FullPath))
        {
          hierarchyNode1 = hierarchyNode2;
          flag = true;
          break;
        }
      }
      for (HierarchyNode hierarchyNode2 = MashupHierarchDefNode.MashupServiceReferenceNode.FirstChild; hierarchyNode2 != null; hierarchyNode2 = hierarchyNode2.NextSibling)
      {
        if (hierarchyNode2.Url.Equals(e.FullPath))
        {
          hierarchyNode1 = hierarchyNode2;
          flag = true;
          break;
        }
      }
      if (flag)
      {
        AuthoringMashupFileNode authoringMashupFileNode = hierarchyNode1 as AuthoringMashupFileNode;
        if (authoringMashupFileNode.UIType != AuthoringType.MashupPortBinding)
          authoringMashupFileNode.AsynUpdateDisplayNameMetaData();
      }
      SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(e.FullPath).Remove("UpdatedCase");
    }

    private static void unregiterFileChangeMonitor(string strRootFolderOfMashupItems)
    {
      FileSystemWatcher fileSystemWatcher = (FileSystemWatcher) null;
      if (!MashupItemSynchronizer.watchers.TryGetValue(strRootFolderOfMashupItems, out fileSystemWatcher))
        return;
      MashupItemSynchronizer.watchers.Remove(strRootFolderOfMashupItems);
      fileSystemWatcher.EnableRaisingEvents = false;
      GC.Collect();
      GC.WaitForPendingFinalizers();
      GC.Collect();
    }

    public static void Terminate()
    {
      foreach (string key in MashupItemSynchronizer.watchers.Clone().Keys)
        MashupItemSynchronizer.unregiterFileChangeMonitor(key);
      MashupItemSynchronizer.handled.Clear();
    }
  }
}
