﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.UIComponentRefWithMashupFactory
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Project;
using SAP.CopernicusProjectView;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode
{
  public class UIComponentRefWithMashupFactory : ICopernicusHierarchyUIComponentFactory, SUIComponentFactory
  {
    public HierarchyNode CreateNode(CopernicusProjectNode root, string strXRepPath)
    {
      return (HierarchyNode) new UIComponentRefWithMashup((ProjectNode) root, strXRepPath);
    }
  }
}
