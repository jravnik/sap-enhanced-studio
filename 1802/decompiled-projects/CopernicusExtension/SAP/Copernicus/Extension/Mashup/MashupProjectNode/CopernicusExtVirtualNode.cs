﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.CopernicusExtVirtualNode
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Project.Automation;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode
{
  [CLSCompliant(false)]
  [ComVisible(true)]
  public class CopernicusExtVirtualNode : HierarchyNode
  {
    protected const string MashupNodeVirtualName = "CopernicusExtVirualNode";
    protected string strVirtualNodeCaption;

    public CopernicusExtVirtualNode(ProjectNode root, string strVirtualNodeCaption)
      : base(root)
    {
      this.VirtualNodeName = "CopernicusExtVirualNode";
      this.ExcludeNodeFromScc = true;
      this.strVirtualNodeCaption = strVirtualNodeCaption;
    }

    public override int SortPriority
    {
      get
      {
        return 300;
      }
    }

    public override int MenuCommandId
    {
      get
      {
        return 1104;
      }
    }

    public override Guid ItemTypeGuid
    {
      get
      {
        return VSConstants.GUID_ItemType_VirtualFolder;
      }
    }

    public override string Url
    {
      get
      {
        return new Microsoft.VisualStudio.Shell.Url(this.ProjectMgr.ProjectFolder).AbsoluteUrl;
      }
    }

    public override string Caption
    {
      get
      {
        return this.strVirtualNodeCaption;
      }
    }

    public override bool CanExecuteCommand
    {
      get
      {
        return base.CanExecuteCommand;
      }
    }

    public override object GetAutomationObject()
    {
      if (this.ProjectMgr == null || this.ProjectMgr.IsClosed)
        return (object) null;
      return (object) new OAProjectItem<CopernicusExtVirtualNode>(this.ProjectMgr.GetAutomationObject() as OAProject, this);
    }

    public override string GetEditLabel()
    {
      return (string) null;
    }

    public override object GetIconHandle(bool open)
    {
      return (object) SAP.Copernicus.Extension.Mashup.Resource.MashupComponentUsageNode.GetHicon();
    }

    protected override StringBuilder PrepareSelectedNodesForClipBoard()
    {
      return (StringBuilder) null;
    }

    protected override int ExcludeFromProject()
    {
      return -2147221248;
    }

    protected override int QueryStatusOnNode(Guid cmdGroup, uint cmd, IntPtr pCmdText, ref QueryStatusResult result)
    {
      if (cmdGroup == PkgCmdIDList.guidCopernicusCmdSet)
      {
        if ((int) cmd == 401)
          result |= QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
      }
      else
        result = QueryStatusResult.SUPPORTED | QueryStatusResult.INVISIBLE;
      return 0;
    }

    protected override int ExecCommandOnNode(Guid cmdGroup, uint cmd, uint nCmdexecopt, IntPtr pvaIn, IntPtr pvaOut)
    {
      return -2147221244;
    }

    protected override bool CanDeleteItem(__VSDELETEITEMOPERATION deleteOperation)
    {
      return false;
    }

    protected override void UpdateSccStateIcons()
    {
      base.UpdateSccStateIcons();
      HierarchyNode firstChild = this.FirstChild;
    }

    protected override bool CanShowDefaultIcon()
    {
      return !string.IsNullOrEmpty(this.VirtualNodeName);
    }
  }
}
