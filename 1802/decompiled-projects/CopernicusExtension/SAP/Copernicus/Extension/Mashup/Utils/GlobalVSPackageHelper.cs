﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.Utils.GlobalVSPackageHelper
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.Automation;
using System;
using System.Diagnostics;

namespace SAP.Copernicus.Extension.Mashup.Utils
{
  public class GlobalVSPackageHelper
  {
    public static void InvokeAddNewItemWizard(string _projectTypeAsDefinedInVSTemplateFile, string _projectItemNameAsDefinedInVSTemplateFile_ThatYouWantSelected)
    {
      string pbstrFilter = string.Empty;
      string pbstrLocation = string.Empty;
      IVsHierarchy ppHierarchy;
      (Package.GetGlobalService(typeof (SVsSolution)) as IVsSolution).GetProjectOfUniqueName(DTEUtil.GetDTE().Solution.Projects.Item((object) 1).FullName, out ppHierarchy);
      IVsProject3 vsProject3 = ppHierarchy as IVsProject3;
      Trace.Assert(vsProject3 != null);
      IVsAddProjectItemDlg globalService = Package.GetGlobalService(typeof (IVsAddProjectItemDlg)) as IVsAddProjectItemDlg;
      uint grfAddFlags = 131336;
      Guid rguidProject = new Guid("AC959CD1-5A2C-4306-BE72-259804B01F08");
      int pfDontShowAgain;
      globalService.AddProjectItemDlg(4294967294U, ref rguidProject, (IVsProject) vsProject3, grfAddFlags, _projectTypeAsDefinedInVSTemplateFile, _projectItemNameAsDefinedInVSTemplateFile_ThatYouWantSelected, ref pbstrLocation, ref pbstrFilter, out pfDontShowAgain);
    }
  }
}
