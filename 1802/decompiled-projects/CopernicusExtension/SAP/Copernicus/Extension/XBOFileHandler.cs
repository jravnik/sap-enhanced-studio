﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.XBOFileHandler
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Antlr.Runtime.Tree;
using EnvDTE80;
using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.BOCompiler.Common;
using SAP.Copernicus.BusinessObjectLanguage;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.PDI;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.EntityUsageIndex;
using SAP.Copernicus.Core.Scripting;
using SAP.Copernicus.Core.Translation;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Extension.LanguageService;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.CompilerVersionDialog;
using SAP.CopernicusProjectView.EntityUsageIndex;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension
{
  public class XBOFileHandler
  {
    private static BODLBackendCompilerHandler bbcHandler = new BODLBackendCompilerHandler();
    private static XBOFileHandler instance;
    private bool MaintenanceModeTest;

    private XBOFileHandler()
    {
    }

    public static XBOFileHandler Instance
    {
      get
      {
        if (XBOFileHandler.instance == null)
          XBOFileHandler.instance = new XBOFileHandler();
        return XBOFileHandler.instance;
      }
    }

    [FileEventHandler(FileEventType.PreActivateSolution, 4)]
    private bool CompileAllXBOs(string solutionName, GenericFileEventArgs eventArgs)
    {
      return this.CompileAllXBOs(false, eventArgs.DeltaMode);
    }

    [FileEventHandler(FileEventType.PreCheckSolution, 4)]
    private bool CheckAllXBOs(string solutionName, GenericFileEventArgs eventArgs)
    {
      return this.CompileAllXBOs(true, false);
    }

    private bool CompileAllXBOs(bool checkOnly, bool deltaMode = false)
    {
      bool flag = true;
      IStatus instance1 = CopernicusStatusBar.Instance;
      BOCompilerErrors chkErrors = (BOCompilerErrors) null;
      BOCompilerErrors errors1 = (BOCompilerErrors) null;
      BOCompilerErrors errors2 = (BOCompilerErrors) null;
      BOFileSynchronizer fileSynchronizer = new BOFileSynchronizer();
      Solution2 solution = DTEUtil.GetDTE().Solution as Solution2;
      if (solution == null || !solution.IsOpen)
        return flag;
      foreach (EnvDTE.Project project in solution.Projects)
      {
        CopernicusProjectNode copernicusProjectNode = project.Object as CopernicusProjectNode;
        if (copernicusProjectNode != null)
        {
          List<HierarchyNode> nodeswithExtn = copernicusProjectNode.getNodeswithExtn(".xbo");
          if (nodeswithExtn.Count<HierarchyNode>() != 0)
          {
            this.MaintenanceModeTest = false;
            XBOFileHandler.isMaintenanceMode(this.MaintenanceModeTest);
            IEnumerable<HierarchyNode> hierarchyNodes;
            if (deltaMode)
            {
              string[] xrepPaths = new string[nodeswithExtn.Count];
              XRepMapper instance2 = XRepMapper.GetInstance();
              int num = 0;
              foreach (HierarchyNode hierarchyNode in nodeswithExtn)
                xrepPaths[num++] = instance2.GetXrepPathforLocalFile(hierarchyNode.Url);
              PDI_S_APPS_CONTENT_STATUS[] contentStatus = XRepHandler.Instance.ContentGetStatus((string) null, xrepPaths, (string) null);
              hierarchyNodes = nodeswithExtn.Where<HierarchyNode>((Func<HierarchyNode, int, bool>) ((node, i) => contentStatus[i].STATUS_RUN_TIME != "2"));
            }
            else
              hierarchyNodes = (IEnumerable<HierarchyNode>) nodeswithExtn;
            foreach (HierarchyNode parentNode in hierarchyNodes)
            {
              List<HierarchyNode> allChildrenNodes = copernicusProjectNode.GetAllChildrenNodes(parentNode);
              List<string> stringList = new List<string>();
              foreach (HierarchyNode hierarchyNode in allChildrenNodes)
              {
                if (hierarchyNode is FileNode)
                {
                  string url = hierarchyNode.Url;
                  stringList.Add(url);
                }
              }
              if (!fileSynchronizer.SyncFiles(parentNode.Url, (IEnumerable<string>) stringList.ToArray(), out errors1))
              {
                if (errors1 != null)
                {
                  if (errors2 == null)
                    errors2 = errors1;
                  else
                    errors2.MergeErrors(errors1);
                }
                flag = false;
              }
              else if (!NewCompilerVersionDialog.useBBC(ProjectUtil.GetProjectPropertiesForFile(parentNode.Url).CompilerVersion) && !this.CompileXBO(checkOnly, !checkOnly, AbstractLanguageService.callingContext.Activate, parentNode.Url, stringList.ToArray(), out chkErrors, instance1, false))
              {
                if (chkErrors != null)
                {
                  if (errors2 == null)
                    errors2 = chkErrors;
                  else
                    errors2.MergeErrors(chkErrors);
                }
                flag = false;
              }
            }
            CompilerEventRegistry.INSTANCE.RaiseUpdateErrorListEvent(errors2);
          }
        }
      }
      ExtensionDataCache.GetInstance().Reset();
      return flag;
    }

    public void OnFileSave(string filePath, string[] dependentFileNames, string content, ProjectProperties properties)
    {
      this.MaintenanceModeTest = true;
      IStatus instance = CopernicusStatusBar.Instance;
      XBOFileHandler.isMaintenanceMode(this.MaintenanceModeTest);
      BOCompilerErrors boCompilerErrors = new BOCompilerErrors(filePath, (IOutputWindowPane) null);
      BOCompilerErrors chkErrors = new BOCompilerErrors(filePath, (IOutputWindowPane) null);
      bool useBBC = false;
      if (NewCompilerVersionDialog.useBBC(properties.CompilerVersion))
      {
        StreamReader streamReader = new StreamReader(filePath);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        string repositoryNamespace = properties.RepositoryNamespace;
        string compilerOption = "2";
        PDI_LM_S_MSG_LIST[] ET_MSG_LIST = (PDI_LM_S_MSG_LIST[]) null;
        string pathforLocalFile = XRepMapper.GetInstance().GetXrepPathforLocalFile(filePath);
        bool success;
        XBOFileHandler.bbcHandler.Compile(end, repositoryNamespace, pathforLocalFile, compilerOption, false, false, false, out success, out ET_MSG_LIST);
        BODLBackendCompilerHelper.mapBackendCompilerMessages(chkErrors, ET_MSG_LIST);
        boCompilerErrors.MergeErrors(chkErrors);
        if (boCompilerErrors.Count<BOCompilerError>() == 0)
          return;
        CompilerEventRegistry.INSTANCE.RaiseUpdateErrorListEvent(boCompilerErrors);
      }
      else
      {
        this.CompileXBO(true, true, AbstractLanguageService.callingContext.Save, filePath, dependentFileNames, out chkErrors, instance, useBBC);
        if (chkErrors != null)
          boCompilerErrors.MergeErrors(chkErrors);
        if (boCompilerErrors.Count<BOCompilerError>() == 0)
          return;
        CompilerEventRegistry.INSTANCE.RaiseUpdateErrorListEvent(boCompilerErrors);
      }
    }

    public void OnFileDeletion(string filePath)
    {
      this.MaintenanceModeTest = false;
      XBOFileHandler.isMaintenanceMode(this.MaintenanceModeTest);
    }

    public void OnFileActivate(string filePath, bool overallStatus, out bool myStatus)
    {
      myStatus = overallStatus;
      if (!overallStatus)
        return;
      bool checkOnly = false;
      IStatus instance = CopernicusStatusBar.Instance;
      if (instance != null)
        instance.ShowMessage(Resources.WaitBOGenerate);
      try
      {
        BOCompilerErrors boCompilerErrors = new BOCompilerErrors(filePath, (IOutputWindowPane) null);
        BOCompilerErrors chkErrors = new BOCompilerErrors(filePath, (IOutputWindowPane) null);
        ProjectProperties propertiesForFile = ProjectUtil.GetProjectPropertiesForFile(filePath);
        bool useBBC = false;
        if (NewCompilerVersionDialog.useBBC(propertiesForFile.CompilerVersion))
        {
          bool flag = true;
          myStatus = flag;
        }
        else
        {
          CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
          BOFileSynchronizer fileSynchronizer = new BOFileSynchronizer();
          string str = ProjectUtil.GetProjectItemForFile(filePath).Name;
          string upper = (str.Remove(str.Length - 4) + ".xbo").ToUpper();
          CopernicusProjectFileNode nodeByName = selectedProject.FindNodeByName((HierarchyNode) selectedProject, upper, (IEnumerable<string>) null, true);
          List<HierarchyNode> allChildrenNodes = selectedProject.GetAllChildrenNodes((HierarchyNode) nodeByName);
          List<string> stringList = new List<string>();
          foreach (HierarchyNode hierarchyNode in allChildrenNodes)
          {
            if (hierarchyNode is FileNode)
            {
              string url = hierarchyNode.Url;
              stringList.Add(url);
            }
          }
          this.MaintenanceModeTest = false;
          XBOFileHandler.isMaintenanceMode(this.MaintenanceModeTest);
          AbstractLanguageService.callingContext cc = AbstractLanguageService.callingContext.Activate;
          myStatus = this.CompileXBO(checkOnly, false, cc, filePath, stringList.ToArray(), out chkErrors, instance, useBBC);
          if (chkErrors != null)
          {
            BOCompilerError[] array = chkErrors.ToArray<BOCompilerError>();
            BOCompilerErrors dependentFileErrors = new BOCompilerErrors(filePath, (IOutputWindowPane) null);
            for (int index = 0; index < chkErrors.NumberOfErrors; ++index)
              dependentFileErrors.AddError(array[index].Message.ToString(), BOErrorLevel.LEVEL2);
            boCompilerErrors.MergeErrors(dependentFileErrors);
          }
          if (boCompilerErrors.Count<BOCompilerError>() != 0)
          {
            CompilerEventRegistry.INSTANCE.RaiseUpdateErrorListEvent(boCompilerErrors);
            CopernicusStatusBar.Instance.ShowMessage(Resources.LEVEL1_XBODLMSG_GENERATION_ERROR);
            throw new XBOCompileException(boCompilerErrors.Count<BOCompilerError>());
          }
        }
      }
      catch (XBOCompileException ex)
      {
        ExtensionDataCache.GetInstance().Reset();
      }
      finally
      {
        ExtensionDataCache.GetInstance().Reset();
        string[] strArray = filePath.Split(new string[1]{ "\\" }, StringSplitOptions.RemoveEmptyEntries);
        string str = strArray.Length > 1 ? strArray[strArray.Length - 1] : strArray[0];
        Task task1 = new Task();
        Task task2 = CopernicusExtensionPackage.Instance.ErrorListProvider.CheckForScenarioWarning(str.Split(new char[5]{ ' ', ',', '.', ':', '\t' }, StringSplitOptions.None)[0], TaskErrorCategory.Warning);
        if (task2 != null)
          CopernicusExtensionPackage.Instance.ErrorListProvider.ClearErrorTaskItems(task2.Column);
      }
    }

    public void OnFileCheck(string filePath, bool overallStatus, out bool myStatus)
    {
      bool testMode = false;
      IStatus instance = CopernicusStatusBar.Instance;
      BOCompilerErrors errors = new BOCompilerErrors(filePath, (IOutputWindowPane) null);
      XBOFileHandler.isMaintenanceMode(testMode);
      ProjectProperties propertiesForFile = ProjectUtil.GetProjectPropertiesForFile(filePath);
      bool useBBC = false;
      if (NewCompilerVersionDialog.useBBC(propertiesForFile.CompilerVersion))
      {
        bool flag = true;
        myStatus = flag;
      }
      else
      {
        AbstractLanguageService.callingContext cc = AbstractLanguageService.callingContext.Check;
        BOCompilerErrors chkErrors;
        myStatus = this.CompileXBO(true, false, cc, filePath, new string[0], out chkErrors, instance, useBBC);
        if (chkErrors == null)
          return;
        errors.MergeErrors(chkErrors);
        CompilerEventRegistry.INSTANCE.RaiseUpdateErrorListEvent(errors);
      }
    }

    public bool CompileXBO(bool checkOnly, bool createSMTG, AbstractLanguageService.callingContext cc, string filePath, string[] dependentFileNames, out BOCompilerErrors chkErrors, IStatus status, bool useBBC)
    {
      string prxBOName = (string) null;
      string esrBONamespace = (string) null;
      string esrBOName = (string) null;
      string prxMaxBOName = (string) null;
      MessageClassHandler messageClassHandler = new MessageClassHandler();
      PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE[] messages1 = new PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE[0];
      BOCompilerErrors boCompilerErrors = new BOCompilerErrors(filePath, (IOutputWindowPane) null);
      bool flag1 = true;
      bool checkMaintenance = checkOnly;
      if (filePath == null || filePath == string.Empty)
      {
        bool flag2 = false;
        boCompilerErrors.AddError("no XREP file Path", BOErrorLevel.LEVEL3);
        chkErrors = boCompilerErrors;
        return flag2;
      }
      string withoutExtension = Path.GetFileNameWithoutExtension(filePath);
      string pathforLocalFile = XRepMapper.GetInstance().GetXrepPathforLocalFile(filePath);
      XBOLanguageService.GetBOInfoAttribs(pathforLocalFile, out esrBONamespace, out esrBOName, out prxBOName, out prxMaxBOName);
      XBOHeader xboHeader = ExtensionDataCache.GetInstance().updateXBOHeaderList(esrBONamespace, esrBOName, pathforLocalFile);
      ExtensionDataCache.GetInstance().Reset(true);
      if (!this.SaveFileIfDirty(ProjectUtil.GetProjectItemForFile(filePath).Object as CopernicusProjectFileNode, status, boCompilerErrors))
      {
        chkErrors = boCompilerErrors;
        return false;
      }
      BOParser parser = new BOParser(new BOLexer(File.OpenRead(filePath), boCompilerErrors), ProjectUtil.GetProjectNamespaceForFile(filePath), true, false);
      CommonTree tree = parser.model().Tree;
      parser.CurrentLine = 1;
      parser.CurrentColumn = 0;
      XBOLanguageService.GetInstance().StartTreeParser(parser, tree, cc, (ParseRequest) null);
      CompilerEventRegistry.INSTANCE.RaiseUpdateErrorListEvent(boCompilerErrors, BOErrorLevel.LEVEL2);
      if (XBOLanguageService.GetInstance().parserErrors)
      {
        CompilerEventRegistry.INSTANCE.RaiseUpdateErrorListEvent(boCompilerErrors, BOErrorLevel.LEVEL2);
        chkErrors = boCompilerErrors;
        return false;
      }
      List<string> nameSpaces = new List<string>((IEnumerable<string>) parser.Imports);
      string namespaceForFile = ProjectUtil.GetProjectNamespaceForFile(parser.ErrorHandler.FileName);
      if (!nameSpaces.Contains(namespaceForFile))
        nameSpaces.Add(namespaceForFile);
      List<ExtensionField> fields1 = ExtensionDataCache.GetInstance().getFields(xboHeader.getNamespace(), xboHeader.getBoName());
      if (createSMTG)
      {
        messages1 = ExtensionDataCache.GetInstance().getDeclaredMessages();
        string messageClassName = string.Empty;
        string success = string.Empty;
        string smtgProxyName = string.Empty;
        if (!useBBC)
        {
          PDI_UTIL_SMTG_GENERATE.PDI_RI_T_MESSAGE[] messageClassSemName = messageClassHandler.GenerateMessageClassSemName(namespaceForFile, withoutExtension, prxBOName, messages1, out messageClassName, out success, out smtgProxyName);
          if (messageClassName == null || success == "" || ((IEnumerable<PDI_UTIL_SMTG_GENERATE.PDI_RI_T_MESSAGE>) messageClassSemName).Count<PDI_UTIL_SMTG_GENERATE.PDI_RI_T_MESSAGE>() > 0)
          {
            if (((IEnumerable<PDI_UTIL_SMTG_GENERATE.PDI_RI_T_MESSAGE>) messageClassSemName).Count<PDI_UTIL_SMTG_GENERATE.PDI_RI_T_MESSAGE>() > 0)
            {
              foreach (PDI_UTIL_SMTG_GENERATE.PDI_RI_T_MESSAGE pdiRiTMessage in messageClassSemName)
              {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(string.Format(SAP.Copernicus.BOCompiler.Common.Resources.LEVEL3_BODLMSG_MESSAGE_CREATION_FAILED));
                stringBuilder.Append(": ");
                stringBuilder.Append(pdiRiTMessage.TEXT);
                if (pdiRiTMessage.SEVERITY == "E")
                {
                  boCompilerErrors.AddError(stringBuilder.ToString(), BOErrorLevel.LEVEL3);
                  chkErrors = boCompilerErrors;
                  return false;
                }
              }
            }
            else
            {
              boCompilerErrors.AddError(string.Format(SAP.Copernicus.BOCompiler.Common.Resources.LEVEL3_BODLMSG_MESSAGE_CREATION_FAILED), BOErrorLevel.LEVEL3);
              chkErrors = boCompilerErrors;
              return false;
            }
          }
        }
      }
      LinkedList<PDI_EXT_S_EXT_FIELDS> source = new LinkedList<PDI_EXT_S_EXT_FIELDS>();
      LinkedList<SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BONodeType> xboNodesList = SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.Instance.getXBONodesList(esrBONamespace, esrBOName);
      List<ExtensionField> fields2 = ExtensionDataCache.GetInstance().getFields(esrBONamespace, esrBOName);
      ISet<string> stringSet = (ISet<string>) new HashSet<string>(ExtensionDataCache.GetInstance().getActivatedFields(esrBONamespace, esrBOName).Where<PDI_EXT_S_EXT_FIELDS>((Func<PDI_EXT_S_EXT_FIELDS, bool>) (fld => fld.FIELD_OWNER_CODE != "P")).Select<PDI_EXT_S_EXT_FIELDS, string>((Func<PDI_EXT_S_EXT_FIELDS, string>) (fld => fld.FIELD_ESR_NAME)));
      foreach (SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BONodeType boNodeType in xboNodesList)
      {
        using (LinkedList<SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BOFieldType>.Enumerator enumerator = boNodeType.Fields.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BOFieldType boField = enumerator.Current;
            if (!stringSet.Contains(boField.Name))
            {
              PDI_EXT_S_EXT_FIELDS pdiExtSExtFields = new PDI_EXT_S_EXT_FIELDS();
              pdiExtSExtFields.BO_ESR_NAMESPACE = ImportPathUtil.ConvertImportFromInternalToESR(esrBONamespace);
              pdiExtSExtFields.BO_ESR_NAME = esrBOName;
              pdiExtSExtFields.NODE_ESR_NAME = boNodeType.Name;
              pdiExtSExtFields.FIELD_ESR_NAME = boField.Name;
              pdiExtSExtFields.MAX_BO_PRX_NAME = prxMaxBOName;
              ExtensionField extensionField = fields2.First<ExtensionField>((Func<ExtensionField, bool>) (fld => fld.getName() == boField.Name));
              string label = extensionField.getLabel();
              string tooltip = extensionField.getTooltip();
              string str1 = !string.IsNullOrWhiteSpace(label) ? label.TrimEnd() : boField.Name;
              pdiExtSExtFields.FIELD_LABEL = str1;
              string str2 = !string.IsNullOrWhiteSpace(tooltip) ? tooltip.TrimEnd() : str1;
              pdiExtSExtFields.FIELD_TOOLTIP = str2;
              XBODataType dataTypeDetails = new XBODataType();
              if (!nameSpaces.Contains(namespaceForFile))
                nameSpaces.Add(namespaceForFile);
              if (XBOLanguageService.GetInstance().checkDataTypeIsValid(nameSpaces, boField.Type, out dataTypeDetails))
              {
                switch (dataTypeDetails.FieldTypeCategory)
                {
                  case FieldTypeCategory.Default:
                    pdiExtSExtFields.FIELD_TYPE = dataTypeDetails.ExtFieldType;
                    break;
                  case FieldTypeCategory.CodeList:
                    pdiExtSExtFields.FIELD_TYPE = "CODE_DT";
                    pdiExtSExtFields.DATA_TYPE_NAME = dataTypeDetails.Name;
                    pdiExtSExtFields.DATA_TYPE_NAMESPACE = dataTypeDetails.NameSpace;
                    break;
                  case FieldTypeCategory.Identifier:
                    pdiExtSExtFields.FIELD_TYPE = "ID_DT";
                    pdiExtSExtFields.DATA_TYPE_NAME = dataTypeDetails.Name;
                    pdiExtSExtFields.DATA_TYPE_NAMESPACE = dataTypeDetails.NameSpace;
                    break;
                }
                pdiExtSExtFields.FIELD_TYPE_CAT = "DEF";
              }
              else if (boField.Type.Equals(""))
              {
                pdiExtSExtFields.FIELD_TYPE_CAT = "RF";
              }
              else
              {
                boCompilerErrors.AddError(Resources.LEVEL1_XBODLMSG_EXF_REG_ERROR, BOErrorLevel.MIN_LEVEL);
                chkErrors = boCompilerErrors;
                return false;
              }
              pdiExtSExtFields.LENGTH = extensionField.getLength();
              pdiExtSExtFields.DECIMALS = extensionField.getDecimals();
              pdiExtSExtFields.TRANSIENT = extensionField.getTransient();
              pdiExtSExtFields.DEFAULT_VALUE = extensionField.getDefaultValue();
              pdiExtSExtFields.DEFAULT_CODE = extensionField.getDefaultCode();
              pdiExtSExtFields.ID_TARGET_ATTRIBUTE = extensionField.getRelationData();
              source.AddLast(pdiExtSExtFields);
            }
          }
        }
      }
      PDI_EXT_S_EXT_FIELDS[] array = source.ToArray<PDI_EXT_S_EXT_FIELDS>();
      ExtensionHandler extensionHandler = new ExtensionHandler();
      PDI_EXT_S_EXT_FIELD_SCENARIOS[] fieldScenarios = ExtScenDataCache.GetInstance().getFieldScenarios();
      PDI_EXT_S_MESSAGES[] messages2 = ExtensionDataCache.GetInstance().getMessages();
      string EV_ERROR = (string) null;
      bool createABSL = false;
      PDI_RI_S_MESSAGE[] ET_MESSAGES;
      if (!useBBC && (!extensionHandler.RegisterEXF(esrBOName, ImportPathUtil.ConvertImportFromInternalToESR(esrBONamespace), pathforLocalFile, createABSL, checkMaintenance, array, fieldScenarios, messages2, out EV_ERROR, out ET_MESSAGES) || EV_ERROR != null || ((IEnumerable<PDI_RI_S_MESSAGE>) ET_MESSAGES).Count<PDI_RI_S_MESSAGE>() > 0))
      {
        if (EV_ERROR != null)
        {
          int num = (int) MessageBox.Show(string.Format(Resources.LEVEL1_XBODLMSG_EXF_REG_ERROR, (object) EV_ERROR));
        }
        else if (((IEnumerable<PDI_RI_S_MESSAGE>) ET_MESSAGES).Count<PDI_RI_S_MESSAGE>() > 0)
        {
          foreach (PDI_RI_S_MESSAGE pdiRiSMessage in ET_MESSAGES)
            boCompilerErrors.AddError(pdiRiSMessage.TEXT, BOErrorLevel.LEVEL2);
        }
        chkErrors = boCompilerErrors;
        return false;
      }
      IDictionary<string, string> attribs = (IDictionary<string, string>) null;
      SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer instance = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance();
      string content = (string) null;
      new XRepHandler().Read(pathforLocalFile, out content, out attribs);
      instance.StoreAttributesInmemory(filePath, attribs);
      if (!useBBC)
      {
        System.Action<string, EntityUsageInfo> exportedValueOrDefault1 = VSPackageUtil.CompositionContainer.GetExportedValueOrDefault<System.Action<string, EntityUsageInfo>>("EntityUsageInfoSink");
        if (exportedValueOrDefault1 != null)
        {
          EntityUsageInfo entityUsageInfo1 = new EntityUsageInfo(prxBOName, EntityUsageType.SOURCE_USES_SAP_BO, false, false, false);
          exportedValueOrDefault1(filePath, entityUsageInfo1);
          string dataType = (string) null;
          foreach (PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE pdiUtilTMessage in messages1)
          {
            for (int index = 0; index < 4; ++index)
            {
              switch (index)
              {
                case 0:
                  dataType = pdiUtilTMessage.ATTRIBUTE1_TYPE;
                  break;
                case 1:
                  dataType = pdiUtilTMessage.ATTRIBUTE2_TYPE;
                  break;
                case 2:
                  dataType = pdiUtilTMessage.ATTRIBUTE3_TYPE;
                  break;
                case 3:
                  dataType = pdiUtilTMessage.ATTRIBUTE4_TYPE;
                  break;
              }
              if (dataType != null && dataType.Length > 0)
              {
                EntityUsageInfo entityUsageInfo2 = new EntityUsageInfo(TargetNameUtil.GetTargetNameForDataType(dataType), EntityUsageType.SOURCE_USES_SAP_GDT, false, false, false);
                exportedValueOrDefault1(filePath, entityUsageInfo2);
              }
            }
          }
          List<PDI_EXT_S_EXT_NODE> boNodesData = ExtensionDataCache.GetInstance().GetBONodesData(xboHeader.getNamespace(), xboHeader.getBoName(), ProjectUtil.GetOpenSolutionName());
          foreach (ExtensionField extensionField in fields1)
          {
            string boNodeProxyName = (string) null;
            foreach (PDI_EXT_S_EXT_NODE pdiExtSExtNode in boNodesData)
            {
              if (((prxBOName = pdiExtSExtNode.PRX_BO_NAME) != null || prxBOName != string.Empty) && extensionField.getNode() == pdiExtSExtNode.ND_NAME)
              {
                boNodeProxyName = pdiExtSExtNode.PRX_ND_NAME;
                break;
              }
            }
            EntityUsageInfo entityUsageInfo2 = new EntityUsageInfo(TargetNameUtil.GetTargetNameForBONode(prxBOName, boNodeProxyName, ""), EntityUsageType.SOURCE_USES_SAP_BO_NODE, false, false, false);
            exportedValueOrDefault1(filePath, entityUsageInfo2);
            XBODataType dataTypeDetails = new XBODataType();
            if (XBOLanguageService.GetInstance().checkDataTypeIsValid(nameSpaces, extensionField.getType(), out dataTypeDetails) && dataTypeDetails.FieldTypeCategory != FieldTypeCategory.Default)
            {
              EntityUsageInfo entityUsageInfo3 = new EntityUsageInfo(dataTypeDetails.ProxyName, EntityUsageType.SOURCE_USES_SAP_GDT, false, false, false);
              exportedValueOrDefault1(filePath, entityUsageInfo3);
            }
          }
        }
        System.Action<string, TextType, string> exportedValueOrDefault2 = VSPackageUtil.CompositionContainer.GetExportedValueOrDefault<System.Action<string, TextType, string>>("TextSink");
        if (exportedValueOrDefault2 != null)
        {
          TextIDCreator textIdCreator = new TextIDCreator();
          string empty = string.Empty;
          foreach (ExtensionField extensionField in fields1)
          {
            if (extensionField.getType() != string.Empty)
            {
              string str1;
              if ((str1 = extensionField.getLabel()) == null)
                str1 = extensionField.getName();
              if (str1 == string.Empty)
                str1 = extensionField.getName();
              string xboExtensionField1 = textIdCreator.GetTextID_XBO_ExtensionField(TextType.Label, extensionField.getName());
              exportedValueOrDefault2(xboExtensionField1, TextType.Label, str1);
              string str2;
              if ((str2 = extensionField.getTooltip()) == null)
                str2 = str1;
              if (str2 == string.Empty)
                str2 = extensionField.getName();
              string xboExtensionField2 = textIdCreator.GetTextID_XBO_ExtensionField(TextType.Tooltip, extensionField.getName());
              exportedValueOrDefault2(xboExtensionField2, TextType.Tooltip, str2);
            }
          }
        }
      }
      chkErrors = (BOCompilerErrors) null;
      return flag1;
    }

    public bool SaveFileIfDirty(CopernicusProjectFileNode currentItem, IStatus status, BOCompilerErrors chkErrors)
    {
      IVsPersistDocData ppDocData = (IVsPersistDocData) null;
      int isDirty = 0;
      IntPtr num = new IntPtr();
      try
      {
        string mkDocument = currentItem.GetMkDocument();
        CopernicusProjectSystemUtil.getSelectedProject().GetDocDataObject(mkDocument, currentItem.ID, out ppDocData);
        if (ppDocData == null)
          return true;
        IntPtr iunknownForObject = Marshal.GetIUnknownForObject((object) ppDocData);
        CopernicusProjectSystemUtil.getSelectedNode().IsItemDirty(currentItem.ID, iunknownForObject, out isDirty);
        int cancelled;
        if (isDirty == 0 || currentItem.SaveItem(VSSAVEFLAGS.VSSAVE_Save, mkDocument, currentItem.ID, iunknownForObject, out cancelled) <= 0)
          return true;
        chkErrors.AddError(Resources.LEVEL1_XBODLMSG_FILE_NOT_SAVED, BOErrorLevel.MIN_LEVEL);
        return false;
      }
      catch
      {
        chkErrors.AddError(Resources.LEVEL1_XBODLMSG_FILE_NOT_SAVED, BOErrorLevel.MIN_LEVEL);
        return false;
      }
    }

    private void RefreshErrorList(BOCompilerErrors errorHandler, BOErrorLevel clearErrorsUpto)
    {
      CompilerEventRegistry.INSTANCE.RaiseUpdateErrorListEvent(errorHandler, clearErrorsUpto);
    }

    internal static bool isMaintenanceMode(bool testMode)
    {
      if (testMode)
        return testMode;
      string currentSolutionName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
      string[] strArray = CopernicusProjectSystemUtil.GetCurrentSolutionFilePath().Split('\\');
      string str = strArray[strArray.Length - 2];
      string solutionVersion = str.Substring(str.IndexOf(" ") + 1);
      SolutionHandler solutionHandler = new SolutionHandler();
      string overallStatus = (string) null;
      string certificationStatus = (string) null;
      string solutionDescription = (string) null;
      return solutionHandler.GetSolutionVersion(currentSolutionName, solutionVersion, out overallStatus, out certificationStatus, out solutionDescription).PRODUCT_VERSION.EDIT_STATUS == "3";
    }
  }
}
