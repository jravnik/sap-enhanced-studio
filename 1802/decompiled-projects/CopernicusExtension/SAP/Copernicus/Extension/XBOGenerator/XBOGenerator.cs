﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.XBOGenerator.XBOGenerator
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Repository;
using System.Collections.Generic;

namespace SAP.Copernicus.Extension.XBOGenerator
{
  public class XBOGenerator
  {
    private static SAP.Copernicus.Extension.XBOGenerator.XBOGenerator generator = new SAP.Copernicus.Extension.XBOGenerator.XBOGenerator();
    private const string XREP_XBO_DEFAULT_PATH = "SRC";
    private const string extBoNsPrefix = "http://sap.com/xi/";

    public static SAP.Copernicus.Extension.XBOGenerator.XBOGenerator Instance
    {
      get
      {
        return SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.generator;
      }
    }

    private XBOGenerator()
    {
    }

    public LinkedList<SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BONodeType> getXBONodesList(string extendedBONameSpace, string extendedBOName)
    {
      LinkedList<SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BONodeType> linkedList = new LinkedList<SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BONodeType>();
      List<ExtensionField> fields = ExtensionDataCache.GetInstance().getFields(extendedBONameSpace, extendedBOName);
      foreach (string boNodeName in ExtensionDataCache.GetInstance().GetBONodeNames(extendedBONameSpace, extendedBOName))
      {
        SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BONodeType boNodeType = new SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BONodeType();
        boNodeType.Actions = new LinkedList<string>();
        boNodeType.Fields = new LinkedList<SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BOFieldType>();
        bool flag = false;
        boNodeType.Name = boNodeName;
        foreach (ExtensionField extensionField in fields)
        {
          if (extensionField.getNode() == boNodeName)
          {
            SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BOFieldType boFieldType;
            boFieldType.Name = extensionField.getName();
            boFieldType.Type = extensionField.getType();
            boNodeType.Fields.AddLast(boFieldType);
            flag = true;
          }
        }
        if (flag)
          linkedList.AddLast(boNodeType);
      }
      return linkedList;
    }

    public struct BOFieldType
    {
      public string Name;
      public string Type;
    }

    public struct BONodeType
    {
      public string Name;
      public LinkedList<string> Actions;
      public LinkedList<SAP.Copernicus.Extension.XBOGenerator.XBOGenerator.BOFieldType> Fields;
    }
  }
}
