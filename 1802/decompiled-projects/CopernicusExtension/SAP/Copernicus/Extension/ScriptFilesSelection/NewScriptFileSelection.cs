﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ScriptFilesSelection.NewScriptFileSelection
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using AdvancedDataGridView;
using Microsoft.VisualStudio.Project;
using SAP.Copernicus.ASUtil;
using SAP.Copernicus.Core;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI.HierarchicalTreeView;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Scripting;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.CompilerVersionDialog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.ScriptFilesSelection
{
  public class NewScriptFileSelection : BaseForm
  {
    private int massProcCellIdx = 2;
    private int nameCellIdx = 1;
    private int offlineCellIdx = 3;
    private Dictionary<string, IScriptFileMetadata> ExistingSFMs = new Dictionary<string, IScriptFileMetadata>();
    private List<IScriptFileMetadata> SelectedSFMList = new List<IScriptFileMetadata>();
    private new IContainer components;
    private TreeGridView treeGridSelector;
    private ToolStrip scriptSelectionToolstrip;
    private ToolStripButton showEventsButton;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripButton showValidationsButton;
    private ToolStripSeparator toolStripSeparator2;
    private ToolStripButton showActionValidationsButton;
    private DataGridViewDisableCheckBoxColumn CreateCol;
    private TreeGridColumn TreeCol;
    private DataGridViewDisableCheckBoxColumn MassEnableCol;
    private DataGridViewDisableCheckBoxColumn offlineCol;
    private int createCellIdx;
    private bool offlineBO;
    private string xBoName;
    private string xBoNamespace;
    private string extendedBoName;
    private string extendedBoNameSpace;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (NewScriptFileSelection));
      DataGridViewCellStyle gridViewCellStyle1 = new DataGridViewCellStyle();
      DataGridViewCellStyle gridViewCellStyle2 = new DataGridViewCellStyle();
      DataGridViewCellStyle gridViewCellStyle3 = new DataGridViewCellStyle();
      DataGridViewCellStyle gridViewCellStyle4 = new DataGridViewCellStyle();
      DataGridViewCellStyle gridViewCellStyle5 = new DataGridViewCellStyle();
      DataGridViewCellStyle gridViewCellStyle6 = new DataGridViewCellStyle();
      this.treeGridSelector = new TreeGridView();
      this.scriptSelectionToolstrip = new ToolStrip();
      this.showEventsButton = new ToolStripButton();
      this.toolStripSeparator1 = new ToolStripSeparator();
      this.showValidationsButton = new ToolStripButton();
      this.toolStripSeparator2 = new ToolStripSeparator();
      this.showActionValidationsButton = new ToolStripButton();
      this.CreateCol = new DataGridViewDisableCheckBoxColumn();
      this.TreeCol = new TreeGridColumn();
      this.MassEnableCol = new DataGridViewDisableCheckBoxColumn();
      this.offlineCol = new DataGridViewDisableCheckBoxColumn();
      ((ISupportInitialize)this.treeGridSelector).BeginInit();
      this.scriptSelectionToolstrip.SuspendLayout();
      this.SuspendLayout();
      componentResourceManager.ApplyResources((object) this.buttonCancel, "buttonCancel");
      componentResourceManager.ApplyResources((object) this.buttonOk, "buttonOk");
      this.treeGridSelector.AllowUserToAddRows = false;
      this.treeGridSelector.AllowUserToDeleteRows = false;
      this.treeGridSelector.AllowUserToResizeRows = false;
      componentResourceManager.ApplyResources((object) this.treeGridSelector, "treeGridSelector");
      this.treeGridSelector.BackgroundColor = SystemColors.Menu;
      gridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleLeft;
      gridViewCellStyle1.BackColor = SystemColors.Control;
      gridViewCellStyle1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      gridViewCellStyle1.ForeColor = SystemColors.WindowText;
      gridViewCellStyle1.SelectionBackColor = SystemColors.Highlight;
      gridViewCellStyle1.SelectionForeColor = SystemColors.HighlightText;
      gridViewCellStyle1.WrapMode = DataGridViewTriState.True;
      this.treeGridSelector.ColumnHeadersDefaultCellStyle = gridViewCellStyle1;
      this.treeGridSelector.Columns.AddRange((DataGridViewColumn) this.CreateCol, (DataGridViewColumn) this.TreeCol, (DataGridViewColumn) this.MassEnableCol, (DataGridViewColumn) this.offlineCol);
      gridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
      gridViewCellStyle2.BackColor = SystemColors.Window;
      gridViewCellStyle2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      gridViewCellStyle2.ForeColor = SystemColors.ControlText;
      gridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
      gridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
      gridViewCellStyle2.WrapMode = DataGridViewTriState.False;
      this.treeGridSelector.DefaultCellStyle = gridViewCellStyle2;
      this.treeGridSelector.EditMode = DataGridViewEditMode.EditProgrammatically;
      this.treeGridSelector.ImageList = (ImageList) null;
      this.treeGridSelector.Name = "treeGridSelector";
      gridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
      gridViewCellStyle3.BackColor = SystemColors.Control;
      gridViewCellStyle3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      gridViewCellStyle3.ForeColor = SystemColors.WindowText;
      gridViewCellStyle3.SelectionBackColor = SystemColors.Highlight;
      gridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
      gridViewCellStyle3.WrapMode = DataGridViewTriState.True;
      this.treeGridSelector.RowHeadersDefaultCellStyle = gridViewCellStyle3;
      this.treeGridSelector.RowHeadersVisible = false;
      this.treeGridSelector.SelectionMode = DataGridViewSelectionMode.CellSelect;
      this.treeGridSelector.CellClick += new DataGridViewCellEventHandler(this.treeGridSelector_CellClick);
      this.treeGridSelector.DataError += new DataGridViewDataErrorEventHandler(this.treeGridSelector_DataError);
      componentResourceManager.ApplyResources((object) this.scriptSelectionToolstrip, "scriptSelectionToolstrip");
      this.scriptSelectionToolstrip.Items.AddRange(new ToolStripItem[5]
      {
        (ToolStripItem) this.showEventsButton,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.showValidationsButton,
        (ToolStripItem) this.toolStripSeparator2,
        (ToolStripItem) this.showActionValidationsButton
      });
      this.scriptSelectionToolstrip.LayoutStyle = ToolStripLayoutStyle.Flow;
      this.scriptSelectionToolstrip.Name = "scriptSelectionToolstrip";
      this.showEventsButton.Checked = true;
      this.showEventsButton.CheckOnClick = true;
      this.showEventsButton.CheckState = CheckState.Checked;
      componentResourceManager.ApplyResources((object) this.showEventsButton, "showEventsButton");
      this.showEventsButton.Name = "showEventsButton";
      this.showEventsButton.CheckedChanged += new EventHandler(this.showEventsButton_CheckedChanged);
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      componentResourceManager.ApplyResources((object) this.toolStripSeparator1, "toolStripSeparator1");
      this.showValidationsButton.Checked = true;
      this.showValidationsButton.CheckOnClick = true;
      this.showValidationsButton.CheckState = CheckState.Checked;
      componentResourceManager.ApplyResources((object) this.showValidationsButton, "showValidationsButton");
      this.showValidationsButton.Name = "showValidationsButton";
      this.showValidationsButton.CheckedChanged += new EventHandler(this.showEventsButton_CheckedChanged);
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      componentResourceManager.ApplyResources((object) this.toolStripSeparator2, "toolStripSeparator2");
      this.showActionValidationsButton.Checked = true;
      this.showActionValidationsButton.CheckOnClick = true;
      this.showActionValidationsButton.CheckState = CheckState.Checked;
      componentResourceManager.ApplyResources((object) this.showActionValidationsButton, "showActionValidationsButton");
      this.showActionValidationsButton.Name = "showActionValidationsButton";
      this.showActionValidationsButton.CheckedChanged += new EventHandler(this.showEventsButton_CheckedChanged);
      this.CreateCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      gridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleCenter;
      this.CreateCol.DefaultCellStyle = gridViewCellStyle4;
      this.CreateCol.FillWeight = 11.17131f;
      componentResourceManager.ApplyResources((object) this.CreateCol, "CreateCol");
      this.CreateCol.Name = "CreateCol";
      this.CreateCol.Resizable = DataGridViewTriState.True;
      this.TreeCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.TreeCol.DefaultNodeImage = (Image) null;
      this.TreeCol.FillWeight = 68.52412f;
      componentResourceManager.ApplyResources((object) this.TreeCol, "TreeCol");
      this.TreeCol.Name = "TreeCol";
      this.TreeCol.SortMode = DataGridViewColumnSortMode.NotSortable;
      this.MassEnableCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      gridViewCellStyle5.Alignment = DataGridViewContentAlignment.MiddleCenter;
      this.MassEnableCol.DefaultCellStyle = gridViewCellStyle5;
      this.MassEnableCol.FillWeight = 20.30457f;
      componentResourceManager.ApplyResources((object) this.MassEnableCol, "MassEnableCol");
      this.MassEnableCol.Name = "MassEnableCol";
      this.offlineCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      gridViewCellStyle6.Alignment = DataGridViewContentAlignment.MiddleCenter;
      this.offlineCol.DefaultCellStyle = gridViewCellStyle6;
      this.offlineCol.FillWeight = 20.30457f;
      componentResourceManager.ApplyResources((object) this.offlineCol, "OfflineCol");
      this.offlineCol.Name = "OfflineCol";
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BannerVisible = true;
      this.Controls.Add((Control) this.scriptSelectionToolstrip);
      this.Controls.Add((Control) this.treeGridSelector);
      this.Name = nameof (NewScriptFileSelection);
      this.Subtitle = "To define the business logic for the business object extension, select the events and validations. You can also specify whether a script file can be executed for multiple business object instances.";
      this.Controls.SetChildIndex((Control) this.buttonOk, 0);
      this.Controls.SetChildIndex((Control) this.buttonCancel, 0);
      this.Controls.SetChildIndex((Control) this.treeGridSelector, 0);
      this.Controls.SetChildIndex((Control) this.scriptSelectionToolstrip, 0);
      ((ISupportInitialize)this.treeGridSelector).EndInit();
      this.scriptSelectionToolstrip.ResumeLayout(false);
      this.scriptSelectionToolstrip.PerformLayout();
      this.ResumeLayout(false);
    }

    public List<IScriptFileMetadata> SelectedSFMs
    {
      get
      {
        return this.SelectedSFMList;
      }
    }

    public NewScriptFileSelection()
      : base(HELP_IDS.BDS_XBO_SCRIPTFILE_CREATE)
    {
      this.InitializeComponent();
      this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;
      this.treeGridSelector.ImageList = EntityImageList.ImageList;
      this.scriptSelectionToolstrip.ImageList = EntityImageList.ImageList;
      this.showActionValidationsButton.ImageIndex = 58;
      this.showValidationsButton.ImageIndex = 58;
      this.showEventsButton.ImageIndex = 23;
    }

    public void InitSelector(string xboName, string xboNamespace, string extendedboName, string extendedboNameSpace, LinkedList<ScriptFileGenerator.BONodeType> Nodes)
    {
      this.SelectedSFMList.Clear();
      this.ExistingSFMs.Clear();
      this.xBoName = xboName;
      this.xBoNamespace = xboNamespace;
      this.extendedBoName = extendedboName;
      this.extendedBoNameSpace = extendedboNameSpace;
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      CopernicusProjectFileNode nodeByName = selectedProject.FindNodeByName((HierarchyNode) selectedProject, xboName + ".xbo", (IEnumerable<string>) null, true);
      this.offlineBO = false;
      if (nodeByName == null)
        return;
      string attrib = XREPattribs.getAttrib(CopernicusProjectSystemUtil.GetXRepPathForNode((HierarchyNode) nodeByName), "Offline");
      if (attrib != null)
        this.offlineBO = bool.Parse(attrib);
      foreach (string dependentAbslFile in this.GetDependentABSLFiles(selectedProject.GetAllChildrenNodes((HierarchyNode) nodeByName)))
      {
        ScriptFileMetadata scriptFileMetadata1 = new ScriptFileMetadata();
        scriptFileMetadata1.Retrieve(dependentAbslFile);
        scriptFileMetadata1.OfflineOnlineEnabled = this.offlineBO;
        if (!scriptFileMetadata1.Offline)
          scriptFileMetadata1.OnlineFileExists = true;
        SnippetType? type1 = scriptFileMetadata1.Type;
        if ((type1.GetValueOrDefault() != SnippetType.BOValidation ? 0 : (type1.HasValue ? 1 : 0)) != 0)
        {
          SnippetSubType? subType = scriptFileMetadata1.SubType;
          if ((subType.GetValueOrDefault() != SnippetSubType.BOValAction ? 0 : (subType.HasValue ? 1 : 0)) != 0)
          {
            IScriptFileMetadata scriptFileMetadata2;
            if (this.ExistingSFMs.TryGetValue(scriptFileMetadata1.NodeName + scriptFileMetadata1.SubType.ToString() + scriptFileMetadata1.ActionName, out scriptFileMetadata2))
            {
              if (scriptFileMetadata2.Offline)
                scriptFileMetadata1.SetOffline(true);
              if (scriptFileMetadata2.OnlineFileExists)
                scriptFileMetadata1.OnlineFileExists = true;
              if (!this.offlineBO)
                scriptFileMetadata1.SetOffline(false);
              this.ExistingSFMs[scriptFileMetadata1.NodeName + scriptFileMetadata1.SubType.ToString() + scriptFileMetadata1.ActionName] = (IScriptFileMetadata) scriptFileMetadata1;
              continue;
            }
            if (!this.offlineBO)
              scriptFileMetadata1.SetOffline(false);
            this.ExistingSFMs.Add(scriptFileMetadata1.NodeName + scriptFileMetadata1.SubType.ToString() + scriptFileMetadata1.ActionName, (IScriptFileMetadata) scriptFileMetadata1);
            continue;
          }
        }
        SnippetType? type2 = scriptFileMetadata1.Type;
        if ((type2.GetValueOrDefault() != SnippetType.BODetermination ? 0 : (type2.HasValue ? 1 : 0)) == 0)
        {
          SnippetType? type3 = scriptFileMetadata1.Type;
          if ((type3.GetValueOrDefault() != SnippetType.BOValidation ? 0 : (type3.HasValue ? 1 : 0)) == 0)
          {
            SnippetType? type4 = scriptFileMetadata1.Type;
            if ((type4.GetValueOrDefault() != SnippetType.BOAction ? 0 : (type4.HasValue ? 1 : 0)) != 0)
            {
              IScriptFileMetadata scriptFileMetadata2;
              if (this.ExistingSFMs.TryGetValue(scriptFileMetadata1.NodeName + scriptFileMetadata1.ActionName.ToString(), out scriptFileMetadata2))
              {
                if (scriptFileMetadata2.Offline)
                  scriptFileMetadata1.SetOffline(true);
                if (scriptFileMetadata2.OnlineFileExists)
                  scriptFileMetadata1.OnlineFileExists = true;
                if (!this.offlineBO)
                  scriptFileMetadata1.SetOffline(false);
                this.ExistingSFMs[scriptFileMetadata1.NodeName + scriptFileMetadata1.ActionName.ToString()] = (IScriptFileMetadata) scriptFileMetadata1;
                continue;
              }
              if (!this.offlineBO)
                scriptFileMetadata1.SetOffline(false);
              this.ExistingSFMs.Add(scriptFileMetadata1.NodeName + scriptFileMetadata1.ActionName.ToString(), (IScriptFileMetadata) scriptFileMetadata1);
              continue;
            }
            continue;
          }
        }
        IScriptFileMetadata scriptFileMetadata3;
        if (this.ExistingSFMs.TryGetValue(scriptFileMetadata1.NodeName + scriptFileMetadata1.SubType.ToString(), out scriptFileMetadata3))
        {
          if (scriptFileMetadata3.Offline)
            scriptFileMetadata1.SetOffline(true);
          if (scriptFileMetadata3.OnlineFileExists)
            scriptFileMetadata1.OnlineFileExists = true;
          if (!this.offlineBO)
            scriptFileMetadata1.SetOffline(false);
          this.ExistingSFMs[scriptFileMetadata1.NodeName + scriptFileMetadata1.SubType.ToString()] = (IScriptFileMetadata) scriptFileMetadata1;
        }
        else
        {
          if (!this.offlineBO)
            scriptFileMetadata1.SetOffline(false);
          this.ExistingSFMs.Add(scriptFileMetadata1.NodeName + scriptFileMetadata1.SubType.ToString(), (IScriptFileMetadata) scriptFileMetadata1);
        }
      }
      ScriptFileGenerator.BONodeType parentNode = new ScriptFileGenerator.BONodeType();
      foreach (ScriptFileGenerator.BONodeType node in Nodes)
      {
        if (node.prxParentNodeName == string.Empty)
          parentNode = node;
      }
      this.offlineCol.Visible = this.offlineBO;
      TreeGridNode treeGridNode = new TreeGridNode();
      this.treeGridSelector.RootNode.Tag = (object) new NewScriptFileSelection.TreeTag("TreeGridRoot", false, false, false);
      this.treeGridSelector.RootNode.ChildNodes.Clear();
      this.treeGridSelector.RootNode.ChildNodes.Add(treeGridNode);
      string released = !Connection.getInstance().UsePSM ? "True" : (!(parentNode.released == "X") ? "" : "True");
      this.BONodeTreeSetup(treeGridNode, parentNode.name, released);
      this.BuildNodeTree(treeGridNode, parentNode, this.extendedBoNameSpace, this.extendedBoName);
      this.treeGridSelector.ExpandAll();
      this.showValidationsButton.CheckState = CheckState.Checked;
      this.showActionValidationsButton.CheckState = CheckState.Checked;
      this.showEventsButton.CheckState = CheckState.Checked;
      this.ButtonOKReady = true;
    }

    private void BONodeTreeSetup(TreeGridNode newNode, string nodeName, string released)
    {
      if (released != "True")
      {
        newNode.Cells[this.nameCellIdx].Value = (object) nodeName;
        newNode.Cells[this.nameCellIdx].ReadOnly = true;
        newNode.Cells[this.nameCellIdx].ErrorText = SAP.Copernicus.Extension.Resources.BONodeNotReleased;
        newNode.ImageIndex = 10;
        newNode.Tag = (object) new NewScriptFileSelection.TreeTag("Node", false, false, false);
      }
      else
      {
        newNode.Cells[this.nameCellIdx].Value = (object) nodeName;
        newNode.Cells[this.nameCellIdx].ReadOnly = true;
        newNode.ImageIndex = 10;
        newNode.Tag = (object) new NewScriptFileSelection.TreeTag("Node", false, false, false);
        ((DataGridViewDisableCheckBoxCell) newNode.Cells[this.massProcCellIdx]).Enabled = false;
        ((DataGridViewDisableCheckBoxCell) newNode.Cells[this.massProcCellIdx]).Enabled = false;
        ((DataGridViewDisableCheckBoxCell) newNode.Cells[this.offlineCellIdx]).Enabled = false;
      }
    }

    private void BuildNodeTree(TreeGridNode parentTreeNode, ScriptFileGenerator.BONodeType parentNode, string namespaceName, string xboName)
    {
      this.extendedBoName = xboName;
      string namespaceName1 = namespaceName.Replace("http://sap.com/xi/", string.Empty).Replace("/", ".").Trim();
      IScriptFileMetadata scriptFileMetadata;
      foreach (SnippetSubType supportedEvent in ScriptFileGenerator.SupportedEvents)
      {
        if (supportedEvent.Equals((object) SnippetSubType.BODetAfterRetrieve))
        {
          if (this.PSMReleased(parentNode) && ExtensionDataCache.GetInstance().IsNodeExtensible(namespaceName1, this.extendedBoName, parentNode.name))
          {
            TreeGridNode treeGridNode = new TreeGridNode();
            string str = parentNode.name + supportedEvent.ToString();
            treeGridNode.Tag = (object) new NewScriptFileSelection.TreeTag(str, false, true, false);
            parentTreeNode.ChildNodes.Add(treeGridNode);
            treeGridNode.ImageIndex = 23;
            treeGridNode.Visible = true;
            treeGridNode.Cells[this.nameCellIdx].Value = (object) ScriptFileMetadata.BOSnippetSubTypeToString(supportedEvent);
            if (this.ExistingSFMs.ContainsKey(str))
            {
              this.ExistingSFMs.TryGetValue(str, out scriptFileMetadata);
              if (scriptFileMetadata.OfflineOnlineEnabled)
              {
                if (scriptFileMetadata.OnlineFileExists)
                {
                  treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
                  treeGridNode.Cells[this.createCellIdx].Value = (object) true;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = false;
                  treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                  treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = false;
                }
                else
                {
                  treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
                  treeGridNode.Cells[this.createCellIdx].Value = (object) false;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = true;
                  treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                  treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = true;
                }
              }
              else if (scriptFileMetadata.OnlineFileExists)
              {
                treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
                treeGridNode.Cells[this.createCellIdx].Value = (object) true;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = false;
                treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = false;
              }
              else
              {
                treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
                treeGridNode.Cells[this.createCellIdx].Value = (object) false;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = true;
                treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = true;
              }
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) scriptFileMetadata.Offline;
              if (!scriptFileMetadata.Offline && this.offlineBO && parentNode.offEnabled)
              {
                treeGridNode.Cells[this.offlineCellIdx].ReadOnly = false;
              }
              else
              {
                treeGridNode.Cells[this.offlineCellIdx].ReadOnly = true;
                treeGridNode.Cells[this.offlineCellIdx].Style.BackColor = Color.WhiteSmoke;
              }
              ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.offlineCellIdx]).Enabled = !scriptFileMetadata.Offline && this.offlineBO && parentNode.offEnabled;
            }
            else if (this.PSMReleased(parentNode))
            {
              treeGridNode.Cells[this.createCellIdx].Value = (object) false;
              treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
              treeGridNode.Cells[this.massProcCellIdx].Value = (object) true;
              treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) false;
              treeGridNode.Cells[this.offlineCellIdx].ReadOnly = !this.offlineBO || !parentNode.offEnabled;
              if (!parentNode.offEnabled)
                treeGridNode.Cells[this.offlineCellIdx].Style.BackColor = Color.WhiteSmoke;
            }
            else
            {
              treeGridNode.Cells[this.createCellIdx].Value = (object) false;
              treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
              treeGridNode.Cells[this.createCellIdx].Style.BackColor = Color.WhiteSmoke;
              treeGridNode.Cells[this.massProcCellIdx].Value = (object) false;
              treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
              treeGridNode.Cells[this.massProcCellIdx].Style.BackColor = Color.WhiteSmoke;
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) false;
              treeGridNode.Cells[this.offlineCellIdx].ReadOnly = true;
              treeGridNode.Cells[this.offlineCellIdx].Style.BackColor = Color.WhiteSmoke;
            }
          }
        }
        else if (this.PSMReleased(parentNode))
        {
          if (supportedEvent.Equals((object) SnippetSubType.BODetOnDelete) || supportedEvent.Equals((object) SnippetSubType.BODetAfterLoading))
          {
            string pathforSelectedNode = CopernicusProjectSystemUtil.GetXRepPathforSelectedNode();
            if (XREPattribs.getAttrib(pathforSelectedNode, "isESF2") == null || !XREPattribs.getAttrib(pathforSelectedNode, "isESF2").Equals("X"))
              continue;
          }
          TreeGridNode treeGridNode = new TreeGridNode();
          string str = parentNode.name + supportedEvent.ToString();
          treeGridNode.Tag = (object) new NewScriptFileSelection.TreeTag(str, false, true, false);
          parentTreeNode.ChildNodes.Add(treeGridNode);
          treeGridNode.ImageIndex = 23;
          treeGridNode.Visible = true;
          treeGridNode.Cells[this.nameCellIdx].Value = (object) ScriptFileMetadata.BOSnippetSubTypeToString(supportedEvent);
          if (this.ExistingSFMs.ContainsKey(str))
          {
            this.ExistingSFMs.TryGetValue(str, out scriptFileMetadata);
            if (scriptFileMetadata.OfflineOnlineEnabled)
            {
              if (scriptFileMetadata.OnlineFileExists)
              {
                treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
                treeGridNode.Cells[this.createCellIdx].Value = (object) true;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = false;
                treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = false;
              }
              else
              {
                treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
                treeGridNode.Cells[this.createCellIdx].Value = (object) false;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = true;
                treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = true;
              }
            }
            else if (scriptFileMetadata.OnlineFileExists)
            {
              treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
              treeGridNode.Cells[this.createCellIdx].Value = (object) true;
              ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = false;
              treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
              treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
              ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = false;
            }
            else
            {
              treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
              treeGridNode.Cells[this.createCellIdx].Value = (object) false;
              ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = true;
              treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
              treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
              ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = true;
            }
            if (supportedEvent.Equals((object) SnippetSubType.BODetAfterLoading))
            {
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) false;
              treeGridNode.Cells[this.offlineCellIdx].ReadOnly = true;
              ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.offlineCellIdx]).Enabled = false;
            }
            else
            {
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) scriptFileMetadata.Offline;
              if (!scriptFileMetadata.Offline && this.offlineBO && parentNode.offEnabled)
              {
                treeGridNode.Cells[this.offlineCellIdx].ReadOnly = false;
              }
              else
              {
                treeGridNode.Cells[this.offlineCellIdx].ReadOnly = true;
                treeGridNode.Cells[this.offlineCellIdx].Style.BackColor = Color.WhiteSmoke;
              }
              ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.offlineCellIdx]).Enabled = !scriptFileMetadata.Offline && this.offlineBO && parentNode.offEnabled;
            }
          }
          else if (this.PSMReleased(parentNode))
          {
            treeGridNode.Cells[this.createCellIdx].Value = (object) false;
            treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
            if (supportedEvent.Equals((object) SnippetSubType.BODetOnDelete))
            {
              treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
              treeGridNode.Cells[this.massProcCellIdx].Value = (object) false;
              ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = false;
            }
            else
            {
              treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
              treeGridNode.Cells[this.massProcCellIdx].Value = (object) true;
            }
            treeGridNode.Cells[this.offlineCellIdx].Value = (object) false;
            treeGridNode.Cells[this.offlineCellIdx].ReadOnly = !this.offlineBO || !parentNode.offEnabled;
            if (!parentNode.offEnabled)
              ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.offlineCellIdx]).Enabled = false;
            if (supportedEvent.Equals((object) SnippetSubType.BODetAfterLoading))
            {
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) false;
              treeGridNode.Cells[this.offlineCellIdx].ReadOnly = true;
              ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.offlineCellIdx]).Enabled = false;
            }
          }
          else
          {
            treeGridNode.Cells[this.createCellIdx].Value = (object) false;
            treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
            treeGridNode.Cells[this.createCellIdx].Style.BackColor = Color.WhiteSmoke;
            treeGridNode.Cells[this.massProcCellIdx].Value = (object) false;
            treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
            treeGridNode.Cells[this.massProcCellIdx].Style.BackColor = Color.WhiteSmoke;
            treeGridNode.Cells[this.offlineCellIdx].Value = (object) false;
            treeGridNode.Cells[this.offlineCellIdx].ReadOnly = true;
            treeGridNode.Cells[this.offlineCellIdx].Style.BackColor = Color.WhiteSmoke;
          }
        }
      }
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      if (selectedProject.CompilerVersion != "" && this.PSMReleased(parentNode))
      {
        foreach (SnippetSubType supportedValidation in ScriptGenerator.SupportedValidations)
        {
          if (!(parentNode.prxParentNodeName != ""))
          {
            TreeGridNode treeGridNode = new TreeGridNode();
            string str = parentNode.name + supportedValidation.ToString();
            treeGridNode.Tag = (object) new NewScriptFileSelection.TreeTag(str, false, false, true);
            parentTreeNode.ChildNodes.Add(treeGridNode);
            treeGridNode.ImageIndex = 58;
            treeGridNode.Visible = true;
            treeGridNode.Cells[this.nameCellIdx].Value = (object) ScriptFileMetadata.BOSnippetSubTypeToString(supportedValidation);
            if (this.ExistingSFMs.ContainsKey(str))
            {
              this.ExistingSFMs.TryGetValue(str, out scriptFileMetadata);
              if (scriptFileMetadata.OfflineOnlineEnabled)
              {
                if (scriptFileMetadata.OnlineFileExists)
                {
                  treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
                  treeGridNode.Cells[this.createCellIdx].Value = (object) true;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = false;
                  treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                  treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = false;
                }
                else
                {
                  treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
                  treeGridNode.Cells[this.createCellIdx].Value = (object) false;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = true;
                  treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                  treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = true;
                }
              }
              else if (scriptFileMetadata.OnlineFileExists)
              {
                treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
                treeGridNode.Cells[this.createCellIdx].Value = (object) true;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = false;
                treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = false;
              }
              else
              {
                treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
                treeGridNode.Cells[this.createCellIdx].Value = (object) false;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = true;
                treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = true;
              }
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) scriptFileMetadata.Offline;
              if (!scriptFileMetadata.Offline && this.offlineBO && parentNode.offEnabled)
              {
                treeGridNode.Cells[this.offlineCellIdx].ReadOnly = false;
              }
              else
              {
                treeGridNode.Cells[this.offlineCellIdx].ReadOnly = true;
                treeGridNode.Cells[this.offlineCellIdx].Style.BackColor = Color.WhiteSmoke;
              }
              DataGridViewDisableCheckBoxCell cell = (DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.offlineCellIdx];
              cell.Enabled = false;
              cell.Enabled = !scriptFileMetadata.Offline && this.offlineBO && parentNode.offEnabled;
            }
            else if (this.PSMReleased(parentNode))
            {
              treeGridNode.Cells[this.createCellIdx].Value = (object) false;
              treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
              treeGridNode.Cells[this.massProcCellIdx].Value = (object) true;
              treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) false;
              treeGridNode.Cells[this.offlineCellIdx].ReadOnly = !this.offlineBO || !parentNode.offEnabled;
              if (!parentNode.offEnabled)
                treeGridNode.Cells[this.offlineCellIdx].Style.BackColor = Color.WhiteSmoke;
            }
            else
            {
              treeGridNode.Cells[this.createCellIdx].Value = (object) false;
              treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
              treeGridNode.Cells[this.createCellIdx].Style.BackColor = Color.WhiteSmoke;
              treeGridNode.Cells[this.massProcCellIdx].Value = (object) false;
              treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
              treeGridNode.Cells[this.massProcCellIdx].Style.BackColor = Color.WhiteSmoke;
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) false;
              treeGridNode.Cells[this.offlineCellIdx].ReadOnly = true;
              treeGridNode.Cells[this.offlineCellIdx].Style.BackColor = Color.WhiteSmoke;
            }
          }
        }
      }
      if (NewCompilerVersionDialog.useBBC(selectedProject.CompilerVersion) && this.PSMReleased(parentNode))
      {
        bool usePsm = Connection.getInstance().UsePSM;
        foreach (ScriptFileGenerator.BONodeType.ActionType action in parentNode.actions)
        {
          if (!usePsm || action.psmReleased)
          {
            TreeGridNode treeGridNode = new TreeGridNode();
            string str;
            if (action.custom_action)
            {
              str = parentNode.name + action.name;
              treeGridNode.Tag = (object) new NewScriptFileSelection.TreeTag(str, true, false, false, action.name);
              parentTreeNode.ChildNodes.Add(treeGridNode);
              treeGridNode.ImageIndex = 22;
              treeGridNode.Visible = true;
              treeGridNode.Cells[this.nameCellIdx].Value = (object) ("Action : " + action.name);
            }
            else
            {
              str = parentNode.name + SnippetSubType.BOValAction.ToString() + action.name;
              treeGridNode.Tag = (object) new NewScriptFileSelection.TreeTag(str, false, false, true, action.name);
              parentTreeNode.ChildNodes.Add(treeGridNode);
              treeGridNode.ImageIndex = 58;
              treeGridNode.Visible = true;
              treeGridNode.Cells[this.nameCellIdx].Value = (object) ("Action Validation: " + action.name);
            }
            if (this.ExistingSFMs.ContainsKey(str))
            {
              this.ExistingSFMs.TryGetValue(str, out scriptFileMetadata);
              if (scriptFileMetadata.OfflineOnlineEnabled)
              {
                if (scriptFileMetadata.OnlineFileExists)
                {
                  treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
                  treeGridNode.Cells[this.createCellIdx].Value = (object) true;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = false;
                  treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                  treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = false;
                }
                else
                {
                  treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
                  treeGridNode.Cells[this.createCellIdx].Value = (object) false;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = true;
                  treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                  treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
                  ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = true;
                }
              }
              else if (scriptFileMetadata.OnlineFileExists)
              {
                treeGridNode.Cells[this.createCellIdx].ReadOnly = true;
                treeGridNode.Cells[this.createCellIdx].Value = (object) true;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = false;
                treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                treeGridNode.Cells[this.massProcCellIdx].ReadOnly = true;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = false;
              }
              else
              {
                treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
                treeGridNode.Cells[this.createCellIdx].Value = (object) false;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.createCellIdx]).Enabled = true;
                treeGridNode.Cells[this.massProcCellIdx].Value = (object) scriptFileMetadata.MassProcessing;
                treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
                ((DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.massProcCellIdx]).Enabled = true;
              }
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) scriptFileMetadata.Offline;
              if (!scriptFileMetadata.Offline && this.offlineBO && action.offEnabled)
              {
                treeGridNode.Cells[this.offlineCellIdx].ReadOnly = false;
              }
              else
              {
                treeGridNode.Cells[this.offlineCellIdx].ReadOnly = true;
                treeGridNode.Cells[this.offlineCellIdx].Style.BackColor = Color.WhiteSmoke;
              }
              DataGridViewDisableCheckBoxCell cell = (DataGridViewDisableCheckBoxCell) treeGridNode.Cells[this.offlineCellIdx];
              cell.Enabled = false;
              cell.Enabled = !scriptFileMetadata.Offline && this.offlineBO && action.offEnabled;
            }
            else
            {
              treeGridNode.Cells[this.massProcCellIdx].Value = (object) true;
              treeGridNode.Cells[this.massProcCellIdx].ReadOnly = false;
              treeGridNode.Cells[this.createCellIdx].Value = (object) false;
              treeGridNode.Cells[this.createCellIdx].ReadOnly = false;
              treeGridNode.Cells[this.offlineCellIdx].Value = (object) false;
              treeGridNode.Cells[this.offlineCellIdx].ReadOnly = !this.offlineBO || !action.offEnabled;
              if (!action.offEnabled)
                treeGridNode.Cells[this.offlineCellIdx].Style.BackColor = Color.WhiteSmoke;
            }
          }
        }
      }
      foreach (ScriptFileGenerator.BONodeType child in parentNode.children)
      {
        if (this.PSMReleased(child))
        {
          if (child.disabled != "X")
          {
            TreeGridNode treeGridNode = new TreeGridNode();
            parentTreeNode.ChildNodes.Add(treeGridNode);
            this.BONodeTreeSetup(treeGridNode, child.name, this.PSMReleased(child).ToString());
            this.BuildNodeTree(treeGridNode, child, this.extendedBoNameSpace, xboName);
          }
        }
        else if (child.disabled != "X" && child.released == "X")
        {
          TreeGridNode treeGridNode = new TreeGridNode();
          parentTreeNode.ChildNodes.Add(treeGridNode);
          this.BONodeTreeSetup(treeGridNode, child.name, this.PSMReleased(child).ToString());
          this.BuildNodeTree(treeGridNode, child, this.extendedBoNameSpace, xboName);
        }
      }
    }

    private void treeViewSelector_DoubleClick(object sender, EventArgs e)
    {
      TreeView treeView = (TreeView) sender;
      TreeNode node = treeView.SelectedNode.TreeView.HitTest(((MouseEventArgs) e).Location).Node;
      if (node == null)
        return;
      treeView.SelectedNode = node;
      if (node.Tag != null || node.StateImageIndex == 2 || node.StateImageIndex == 3)
        return;
      node.StateImageIndex = node.StateImageIndex == 0 ? 1 : 0;
      this.ButtonOKReady = true;
    }

    private List<string> GetDependentABSLFiles(List<HierarchyNode> childNodes)
    {
      return childNodes.Where<HierarchyNode>((Func<HierarchyNode, bool>) (n =>
      {
        if (n is FileNode)
          return n.Url.EndsWith(".absl");
        return false;
      })).Select<HierarchyNode, string>((Func<HierarchyNode, string>) (n => n.Url)).ToList<string>();
    }

    private void CreateSFMsForSelectedEvents(TreeGridNode node)
    {
      NewScriptFileSelection.TreeTag tag = (NewScriptFileSelection.TreeTag) node.Tag;
      if (tag.IsEvent)
      {
        if (node.Cells[this.createCellIdx].Value == null || !(bool) node.Cells[this.createCellIdx].Value && (!this.offlineBO || !(bool) node.Cells[this.offlineCellIdx].Value))
          return;
        IScriptFileMetadata scriptFileMetadata = (IScriptFileMetadata) new ScriptFileMetadata();
        scriptFileMetadata.SetBOContext(this.extendedBoName, node.Parent.Cells[this.nameCellIdx].Value.ToString(), this.extendedBoNameSpace);
        scriptFileMetadata.SetXBOContext(this.xBoName, node.Parent.Cells[this.nameCellIdx].Value.ToString(), this.xBoNamespace);
        scriptFileMetadata.SetSnippetType(SnippetType.BODetermination, ScriptFileMetadata.BOSnippetSubTypeFromString(node.Cells[this.nameCellIdx].Value.ToString()));
        scriptFileMetadata.SetMassProcessing((bool) node.Cells[this.massProcCellIdx].Value);
        scriptFileMetadata.SetOffline((bool) node.Cells[this.offlineCellIdx].Value);
        scriptFileMetadata.CreateOnlineFile = (bool) node.Cells[this.createCellIdx].Value;
        scriptFileMetadata.OfflineOnlineEnabled = this.offlineBO;
        this.SelectedSFMList.Add(scriptFileMetadata);
      }
      else if (tag.IsValidation && string.IsNullOrEmpty(tag.ActionName))
      {
        if (!(bool) node.Cells[this.createCellIdx].Value && (!this.offlineBO || !(bool) node.Cells[this.offlineCellIdx].Value))
          return;
        IScriptFileMetadata scriptFileMetadata = (IScriptFileMetadata) new ScriptFileMetadata();
        scriptFileMetadata.SetBOContext(this.extendedBoName, node.Parent.Cells[this.nameCellIdx].Value.ToString(), this.extendedBoNameSpace);
        scriptFileMetadata.SetXBOContext(this.xBoName, node.Parent.Cells[this.nameCellIdx].Value.ToString(), this.xBoNamespace);
        scriptFileMetadata.SetSnippetType(SnippetType.BOValidation, ScriptFileMetadata.BOSnippetSubTypeFromString(node.Cells[this.nameCellIdx].Value.ToString()));
        scriptFileMetadata.SetMassProcessing((bool) node.Cells[this.massProcCellIdx].Value);
        scriptFileMetadata.SetOffline((bool) node.Cells[this.offlineCellIdx].Value);
        scriptFileMetadata.CreateOnlineFile = (bool) node.Cells[this.createCellIdx].Value;
        scriptFileMetadata.OfflineOnlineEnabled = this.offlineBO;
        this.SelectedSFMList.Add(scriptFileMetadata);
      }
      else if (tag.IsValidation && !string.IsNullOrEmpty(tag.ActionName))
      {
        if (!(bool) node.Cells[this.createCellIdx].Value && (!this.offlineBO || !(bool) node.Cells[this.offlineCellIdx].Value))
          return;
        IScriptFileMetadata scriptFileMetadata = (IScriptFileMetadata) new ScriptFileMetadata();
        scriptFileMetadata.SetBOContext(this.extendedBoName, node.Parent.Cells[this.nameCellIdx].Value.ToString(), this.extendedBoNameSpace);
        scriptFileMetadata.SetXBOContext(this.xBoName, node.Parent.Cells[this.nameCellIdx].Value.ToString(), this.xBoNamespace);
        scriptFileMetadata.SetActionValidationType(tag.ActionName);
        scriptFileMetadata.SetMassProcessing((bool) node.Cells[this.massProcCellIdx].Value);
        scriptFileMetadata.SetOffline((bool) node.Cells[this.offlineCellIdx].Value);
        scriptFileMetadata.CreateOnlineFile = (bool) node.Cells[this.createCellIdx].Value;
        scriptFileMetadata.OfflineOnlineEnabled = this.offlineBO;
        this.SelectedSFMList.Add(scriptFileMetadata);
      }
      else if (tag.IsAction)
      {
        IScriptFileMetadata scriptFileMetadata;
        this.ExistingSFMs.TryGetValue(tag.EventKey.ToString(), out scriptFileMetadata);
        if (scriptFileMetadata != null)
        {
          scriptFileMetadata.SetMassProcessing((bool) node.Cells[this.massProcCellIdx].Value);
          scriptFileMetadata.CreateOnlineFile = (bool) node.Cells[this.createCellIdx].Value;
        }
        else
        {
          scriptFileMetadata = (IScriptFileMetadata) new ScriptFileMetadata();
          scriptFileMetadata.SetBOContext(this.extendedBoName, node.Parent.Cells[this.nameCellIdx].Value.ToString(), this.extendedBoNameSpace);
          scriptFileMetadata.SetXBOContext(this.xBoName, node.Parent.Cells[this.nameCellIdx].Value.ToString(), this.xBoNamespace);
          scriptFileMetadata.SetMassProcessing((bool) node.Cells[this.massProcCellIdx].Value);
          scriptFileMetadata.SetOffline((bool) node.Cells[this.offlineCellIdx].Value);
          scriptFileMetadata.CreateOnlineFile = (bool) node.Cells[this.createCellIdx].Value;
          scriptFileMetadata.SetActionType(tag.ActionName);
        }
        scriptFileMetadata.OfflineOnlineEnabled = this.offlineBO;
        this.SelectedSFMList.Add(scriptFileMetadata);
      }
      else
      {
        foreach (TreeGridNode childNode in node.ChildNodes)
          this.CreateSFMsForSelectedEvents(childNode);
      }
    }

    protected override void buttonCancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
      this.Close();
    }

    protected override void buttonOk_Click(object sender, EventArgs e)
    {
      this.CreateSFMsForSelectedEvents(this.treeGridSelector.RootNode);
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void treeGridSelector_DataError(object sender, DataGridViewDataErrorEventArgs e)
    {
    }

    private void treeGridSelector_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.ColumnIndex != 2 && e.ColumnIndex != 0 && e.ColumnIndex != 3 || e.RowIndex < 0)
        return;
      NewScriptFileSelection.TreeTag tag = (NewScriptFileSelection.TreeTag) this.treeGridSelector.Rows[e.RowIndex].Tag;
      if (!tag.IsAction && !tag.IsEvent && !tag.IsValidation || this.treeGridSelector[e.ColumnIndex, e.RowIndex].ReadOnly)
        return;
      if ((bool) this.treeGridSelector[e.ColumnIndex, e.RowIndex].Value)
        this.treeGridSelector[e.ColumnIndex, e.RowIndex].Value = (object) false;
      else if (!(bool) this.treeGridSelector[e.ColumnIndex, e.RowIndex].Value)
        this.treeGridSelector[e.ColumnIndex, e.RowIndex].Value = (object) true;
      this.treeGridSelector.InvalidateCell(e.ColumnIndex, e.RowIndex);
    }

    private bool PSMReleased(ScriptFileGenerator.BONodeType node)
    {
      return !Connection.getInstance().UsePSM || node.released == "X" || node.prxNodeName.StartsWith(CopernicusProjectSystemUtil.getSelectedProject().RuntimeNamespacePrefix);
    }

    private void SetVisibilityOfScriptTypes(bool showEvents, bool showValidations, bool showActionValidations)
    {
      Stack<TreeGridNode> treeGridNodeStack = new Stack<TreeGridNode>();
      foreach (TreeGridNode node in this.treeGridSelector.Nodes)
        treeGridNodeStack.Push(node);
      while (treeGridNodeStack.Count > 0)
      {
        TreeGridNode treeGridNode = treeGridNodeStack.Peek();
        treeGridNodeStack.Pop();
        NewScriptFileSelection.TreeTag tag = (NewScriptFileSelection.TreeTag) treeGridNode.Tag;
        if (tag.IsValidation && !string.IsNullOrEmpty(tag.ActionName))
          treeGridNode.Visible = showActionValidations;
        else if (tag.IsValidation && string.IsNullOrEmpty(tag.ActionName))
          treeGridNode.Visible = showValidations;
        else if (tag.IsEvent)
          treeGridNode.Visible = showEvents;
        foreach (TreeGridNode childNode in treeGridNode.ChildNodes)
          treeGridNodeStack.Push(childNode);
      }
    }

    private void showEventsButton_CheckedChanged(object sender, EventArgs e)
    {
      this.SetVisibilityOfScriptTypes(this.showEventsButton.Checked, this.showValidationsButton.Checked, this.showActionValidationsButton.Checked);
    }

    private class TreeTag
    {
      public string EventKey;
      public bool IsAction;
      public bool IsEvent;
      public bool IsValidation;
      public string ActionName;

      public TreeTag(string eventKey, bool isAction, bool isEvent, bool isValidation)
      {
        this.EventKey = eventKey;
        this.IsAction = isAction;
        this.IsEvent = isEvent;
        this.IsValidation = isValidation;
        this.ActionName = "";
      }

      public TreeTag(string eventKey, bool isAction, bool isEvent, bool isValidation, string actionName)
      {
        this.EventKey = eventKey;
        this.IsAction = isAction;
        this.IsEvent = isEvent;
        this.IsValidation = isValidation;
        this.ActionName = actionName;
      }
    }
  }
}
