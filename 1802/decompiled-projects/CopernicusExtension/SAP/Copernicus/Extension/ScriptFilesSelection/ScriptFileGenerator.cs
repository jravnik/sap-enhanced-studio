﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ScriptFilesSelection.ScriptFileGenerator
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Project;
using SAP.Copernicus.ASUtil;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Scripting;
using SAP.Copernicus.Extension.LanguageService;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.ScriptFilesSelection
{
  public class ScriptFileGenerator
  {
    public static SnippetSubType[] SupportedValidations = new SnippetSubType[1]{ SnippetSubType.BOValOnSave };
    private static ScriptFileGenerator Generator = new ScriptFileGenerator();
    private NewScriptFileSelection sfSelection = new NewScriptFileSelection();
    private List<NodeNameExt> nodeList = new List<NodeNameExt>();
    private static SnippetSubType[] supportedEvents;

    public static SnippetSubType[] SupportedEvents
    {
      get
      {
        if (ScriptFileGenerator.supportedEvents == null)
          ScriptFileGenerator.supportedEvents = new SnippetSubType[4]
          {
            SnippetSubType.BODetAfterModify,
            SnippetSubType.BODetBeforeSave,
            SnippetSubType.BODetOnDelete,
            SnippetSubType.BODetAfterLoading
          };
        return ScriptFileGenerator.supportedEvents;
      }
    }

    public static ScriptFileGenerator Instance
    {
      get
      {
        return ScriptFileGenerator.Generator;
      }
    }

    private ScriptFileGenerator()
    {
    }

    public bool CreateScriptFilesForSelectedXBO()
    {
      return this.CreateXBOScriptFiles(CopernicusProjectSystemUtil.GetXBOName(), CopernicusProjectSystemUtil.getSelectedProject().RepositoryNamespace);
    }

    public bool CreateXBOScriptFiles(string xboName, string xboNamespace)
    {
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      bool flag1 = false;
      IStatus instance = CopernicusStatusBar.Instance;
      bool flag2 = true;
      bool flag3 = false;
      try
      {
        if (!selectedProject.CheckOutProjectFile(false))
          return false;
        flag1 = selectedProject.GetDoNotCheckIn();
        LinkedList<ScriptFileGenerator.BONodeType> Nodes = (LinkedList<ScriptFileGenerator.BONodeType>) null;
        string extendedBOName = (string) null;
        string extendedBONameSpace = (string) null;
        ScriptLanguageType byDscript = ScriptLanguageType.ByDScript;
        CopernicusProjectFileNode nodeByName = selectedProject.FindNodeByName((HierarchyNode) selectedProject, xboName + ".xbo", (IEnumerable<string>) null, true);
        if (nodeByName == null)
          throw new ArgumentException(string.Format("business object {0} does not exist in project {1}", (object) xboName, (object) selectedProject.Caption));
        bool cancelled;
        IList<IScriptFileMetadata> sfmListForXbo = this.CreateSFMListForXBO(nodeByName.Url, byDscript, xboName, xboNamespace, out Nodes, out extendedBOName, out extendedBONameSpace, out cancelled);
        if (!cancelled)
        {
          if (instance != null)
            instance.ShowMessage(SAP.Copernicus.Resource.WaitScriptCreate);
          if (sfmListForXbo.Count > 0)
          {
            IList<IScriptCodeItem> scriptCodeItemList = (IList<IScriptCodeItem>) new List<IScriptCodeItem>();
            IList<IScriptCodeItem> scriptStubCode;
            try
            {
              scriptStubCode = (IList<IScriptCodeItem>) new ScriptHandler().CreateScriptStubCode(byDscript, sfmListForXbo);
            }
            catch (ProtocolException ex)
            {
              flag2 = false;
              return flag2;
            }
            try
            {
              selectedProject.CreateAndOpenScriptFiles(scriptStubCode, nodeByName, xboName, true);
            }
            catch (ProtocolException ex)
            {
              flag2 = false;
              return flag2;
            }
          }
        }
        else
        {
          instance.ShowMessage(SAP.Copernicus.Resource.WaitScriptCreateCanceled);
          flag3 = true;
        }
        return flag2;
      }
      finally
      {
        if (selectedProject != null && !flag1)
          selectedProject.KeepProjectFileCheckedOut(false);
        if (instance != null && !flag3)
        {
          if (flag2)
            instance.ShowMessage(SAP.Copernicus.Resource.WaitScriptCreateSuccess);
          else
            instance.ShowMessage(SAP.Copernicus.Resource.WaitScriptCreateFailed);
        }
      }
    }

    private IList<IScriptFileMetadata> CreateSFMListForXBO(string xboFilePath, ScriptLanguageType language, string xboName, string xboNamespace, out LinkedList<ScriptFileGenerator.BONodeType> Nodes, out string extendedBOName, out string extendedBONameSpace, out bool cancelled)
    {
      cancelled = false;
      List<IScriptFileMetadata> scriptFileMetadataList = new List<IScriptFileMetadata>();
      string xboContent = "";
      string pathforLocalFile = XRepMapper.GetInstance().GetXrepPathforLocalFile(xboFilePath);
      Nodes = this.getXBONodes(pathforLocalFile, xboContent, out extendedBOName, out extendedBONameSpace);
      extendedBONameSpace = "http://sap.com/xi/" + extendedBONameSpace.Replace(".", "/");
      foreach (ScriptFileGenerator.BONodeType boNodeType in Nodes)
      {
        ScriptFileMetadata scriptFileMetadata1 = new ScriptFileMetadata();
        scriptFileMetadata1.SetBOContext(extendedBOName, boNodeType.name, extendedBONameSpace);
        scriptFileMetadata1.SetSnippetType(SnippetType.BODetermination, SnippetSubType.BODetBeforeSave);
        scriptFileMetadata1.SetXBOContext(xboName, boNodeType.name, xboNamespace);
        scriptFileMetadataList.Add((IScriptFileMetadata) scriptFileMetadata1);
        ScriptFileMetadata scriptFileMetadata2 = new ScriptFileMetadata();
        scriptFileMetadata2.SetBOContext(extendedBOName, boNodeType.name, extendedBONameSpace);
        scriptFileMetadata2.SetSnippetType(SnippetType.BODetermination, SnippetSubType.BODetAfterModify);
        scriptFileMetadata2.SetXBOContext(xboName, boNodeType.name, xboNamespace);
        scriptFileMetadataList.Add((IScriptFileMetadata) scriptFileMetadata2);
        ScriptFileMetadata scriptFileMetadata3 = new ScriptFileMetadata();
        scriptFileMetadata3.SetBOContext(extendedBOName, boNodeType.name, extendedBONameSpace);
        scriptFileMetadata3.SetSnippetType(SnippetType.BODetermination, SnippetSubType.BODetOnDelete);
        scriptFileMetadata3.SetXBOContext(xboName, boNodeType.name, xboNamespace);
        scriptFileMetadataList.Add((IScriptFileMetadata) scriptFileMetadata3);
        ScriptFileMetadata scriptFileMetadata4 = new ScriptFileMetadata();
        scriptFileMetadata4.SetBOContext(extendedBOName, boNodeType.name, extendedBONameSpace);
        scriptFileMetadata4.SetSnippetType(SnippetType.BODetermination, SnippetSubType.BODetAfterLoading);
        scriptFileMetadata4.SetXBOContext(xboName, boNodeType.name, xboNamespace);
        scriptFileMetadataList.Add((IScriptFileMetadata) scriptFileMetadata4);
        ScriptFileMetadata scriptFileMetadata5 = new ScriptFileMetadata();
        scriptFileMetadata5.SetBOContext(extendedBOName, boNodeType.name, extendedBONameSpace);
        scriptFileMetadata5.SetSnippetType(SnippetType.BODetermination, SnippetSubType.BODetAfterRetrieve);
        scriptFileMetadata5.SetXBOContext(xboName, boNodeType.name, xboNamespace);
        scriptFileMetadataList.Add((IScriptFileMetadata) scriptFileMetadata5);
        foreach (ScriptFileGenerator.BONodeType.ActionType action in boNodeType.actions)
        {
          ScriptFileMetadata scriptFileMetadata6 = new ScriptFileMetadata();
          scriptFileMetadata6.SetBOContext(extendedBOName, boNodeType.name, extendedBONameSpace);
          scriptFileMetadata6.SetXBOContext(xboName, boNodeType.name, xboNamespace);
          if (action.custom_action)
            scriptFileMetadata6.SetActionType(action.name);
          else
            scriptFileMetadata6.SetActionValidationType(action.name);
          scriptFileMetadataList.Add((IScriptFileMetadata) scriptFileMetadata6);
        }
      }
      this.sfSelection.InitSelector(xboName, xboNamespace, extendedBOName, extendedBONameSpace, Nodes);
      if (this.sfSelection.ShowDialog() == DialogResult.OK)
      {
        cancelled = false;
        scriptFileMetadataList.Clear();
        List<IScriptFileMetadata> selectedSfMs = this.sfSelection.SelectedSFMs;
        scriptFileMetadataList.AddRange((IEnumerable<IScriptFileMetadata>) selectedSfMs);
      }
      else
        cancelled = true;
      return (IList<IScriptFileMetadata>) scriptFileMetadataList;
    }

    public LinkedList<ScriptFileGenerator.BONodeType> getXBONodes(string xrepPath, string xboContent, out string ExtendedBOName, out string ExtendedBONameSpace)
    {
      LinkedList<ScriptFileGenerator.BONodeType> linkedList = new LinkedList<ScriptFileGenerator.BONodeType>();
      XBOLanguageService.GetBOInfoAttribs(xrepPath, out ExtendedBONameSpace, out ExtendedBOName);
      this.nodeList = ExtensionDataCache.GetInstance().getAllNodes(ExtendedBONameSpace, ExtendedBOName);
      ScriptFileGenerator.BONodeType node = new ScriptFileGenerator.BONodeType();
      return this.getNodesHierarchy(string.Empty, node);
    }

    private LinkedList<ScriptFileGenerator.BONodeType> getNodesHierarchy(string parentNodeName, ScriptFileGenerator.BONodeType node)
    {
      node.children = new LinkedList<ScriptFileGenerator.BONodeType>();
      foreach (NodeNameExt node1 in this.nodeList)
      {
        if (node1.ParentProxyName == parentNodeName)
        {
          ScriptFileGenerator.BONodeType node2 = new ScriptFileGenerator.BONodeType();
          node2.actions = new List<ScriptFileGenerator.BONodeType.ActionType>();
          List<SAP.Copernicus.Core.Repository.Action> actionsOfNode = ExtensionDataCache.GetInstance().getActionsOfNode(node1.BoProxyName, node1.ProxyName);
          if (actionsOfNode != null)
          {
            foreach (SAP.Copernicus.Core.Repository.Action action in actionsOfNode)
              node2.actions.Add(new ScriptFileGenerator.BONodeType.ActionType(action.esrName, action.psmReleased, action.custom_action, action.off_enabled));
          }
          node2.fields = new LinkedList<ScriptFileGenerator.BOFieldType>();
          node2.name = node1.Name;
          node2.prxParentNodeName = node1.ParentProxyName;
          node2.prxNodeName = node1.ProxyName;
          node2.released = node1.ReleasedNode;
          node2.disabled = node1.DisabledNode;
          node2.children = this.getNodesHierarchy(node2.prxNodeName, node2);
          node2.offEnabled = !string.IsNullOrEmpty(node1.OffEnabled);
          node.children.AddLast(node2);
        }
      }
      return node.children;
    }

    public struct BOFieldType
    {
      public string Name;
      public string Type;
    }

    public struct BONodeType
    {
      public string name;
      public string isExtensible;
      public List<ScriptFileGenerator.BONodeType.ActionType> actions;
      public LinkedList<ScriptFileGenerator.BOFieldType> fields;
      public string prxParentNodeName;
      public string prxNodeName;
      public string released;
      public string disabled;
      public bool offEnabled;
      public LinkedList<ScriptFileGenerator.BONodeType> children;

      public struct ActionType
      {
        public readonly string name;
        public readonly bool psmReleased;
        public readonly bool custom_action;
        public readonly bool offEnabled;

        public ActionType(string name, bool psmReleased, bool custom_action, bool off_Enabled)
        {
          this.name = name;
          this.psmReleased = psmReleased;
          this.custom_action = custom_action;
          this.offEnabled = off_Enabled;
        }
      }
    }
  }
}
