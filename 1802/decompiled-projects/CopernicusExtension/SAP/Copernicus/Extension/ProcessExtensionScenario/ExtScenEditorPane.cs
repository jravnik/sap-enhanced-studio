﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenEditorPane
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.CustomEditor;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Extension.ProcessExtensionScenario
{
  public sealed class ExtScenEditorPane : CustomEditorPane<ExtScenEditorFactory, ExtScenEditorControl>
  {
    protected override string GetFileExtension()
    {
      return ".xs";
    }

    public ExtScenEditorPane()
    {
      this.UIControl.ContentChanged += new EventHandler(this.ViewContentChange);
    }

    protected override void SaveFile(string fileName)
    {
      string pathforLocalFile = XRepMapper.GetInstance().GetXrepPathforLocalFile(fileName);
      string content;
      IDictionary<string, string> attribs;
      if (!new XRepHandler().Read(pathforLocalFile, true, out content, out attribs))
      {
        IVsFileChangeEx service = (IVsFileChangeEx) this.GetService(typeof (SVsFileChangeEx));
        service.IgnoreFile(0U, fileName, -1);
        this.UIControl.SaveFile(fileName);
        service.SyncFile(fileName);
        service.IgnoreFile(0U, fileName, 0);
      }
      else
      {
        int num = (int) CopernicusMessageBox.Show("Process Extension Scenario " + pathforLocalFile + " already shipped. No changes allowed!");
        this.UIControl.LoadFiles(fileName);
      }
    }

    protected override void LoadFile(string fileName)
    {
      this.UIControl.LoadFiles(fileName);
    }

    protected override void OnClose()
    {
      this.UIControl.closeEditor();
    }

    private void ViewContentChange(object sender, EventArgs e)
    {
      this.OnContentChanged();
    }

    protected override Guid GetPackageGuid()
    {
      return typeof (CopernicusExtensionPackage).GUID;
    }

    protected override void OnCreate()
    {
      this.InitializeF1Help(HELP_IDS.BDS_PROCESSEXTENSION_EDITOR);
    }
  }
}
