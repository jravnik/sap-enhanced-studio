﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionWizard
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using EnvDTE;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.TemplateWizard;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Wizard;
using SAP.Copernicus.Extension.TextTemplating;
using SAP.Copernicus.TextTemplating;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.ProcessExtensionScenario
{
  internal class ExtScenSelectionWizard : CopernicusWizardSheet, IWizard
  {
    private static string strXboItem = "$rootname$";
    private string strProNamespace;
    public string xsFileName;

    public ExtScenSelectionWizard()
    {
      this.InitializeComponent();
      ExtScenSelectionFirstPage selectionFirstPage = new ExtScenSelectionFirstPage(this);
      selectionFirstPage.Dock = DockStyle.Fill;
      this.Pages.Add((object) selectionFirstPage);
      this.helpid = HELP_IDS.BDS_PROCESSEXTENSION_CREATE;
    }

    void IWizard.BeforeOpeningFile(ProjectItem projectItem)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.ProjectFinishedGenerating(EnvDTE.Project project)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.ProjectItemFinishedGenerating(ProjectItem projectItem)
    {
      Trace.WriteLine("Enter here");
    }

    void IWizard.RunFinished()
    {
      int num = (int) this.ShowDialog();
    }

    void IWizard.RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
    {
      this.xsFileName = replacementsDictionary[ExtScenSelectionWizard.strXboItem];
      this.xsFileName = this.xsFileName.Substring(0, this.xsFileName.LastIndexOf("."));
    }

    bool IWizard.ShouldAddProjectItem(string filePath)
    {
      return false;
    }

    private void InitializeComponent()
    {
      this.buttonPanel.SuspendLayout();
      this.SuspendLayout();
      this.nextButton.Click += new EventHandler(this.nextButton_Click);
      this.finishButton.Click += new EventHandler(this.finishButton_Click);
      this.pagePanel.AutoSize = true;
      this.AcceptButton = (IButtonControl) this.nextButton;
      this.AutoSize = true;
      this.ClientSize = new Size(384, 141);
      this.Name = nameof (ExtScenSelectionWizard);
      this.Text = "Create Extension Scenario";
      this.buttonPanel.ResumeLayout(false);
      this.buttonPanel.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public void FinishWizard(string BONamespace, string BOName, string BONodeName, string XML)
    {
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      TextTemplateHelper textTemplateHelper = new TextTemplateHelper((ITemplateProvider) Templates.Instance);
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      dictionary.Add("solutionNS", this.strProNamespace);
      dictionary.Add("fileName", this.xsFileName);
      if (RepositoryDataCache.GetInstance().GetProxyNameForQualifiedDTName("AP.Common.GDT", "UUID") == null)
        dictionary.Add("defaultNamespace", "BASIS.Global");
      else
        dictionary.Add("defaultNamespace", "AP.Common.GDT");
      HierarchyNode parentNode = CopernicusProjectSystemUtil.getSelectedNode();
      if (!(parentNode is ProjectNode) && !(parentNode is FolderNode))
        parentNode = parentNode.Parent;
      IDictionary<string, string> fileAttributesToBeSaved = (IDictionary<string, string>) new Dictionary<string, string>();
      fileAttributesToBeSaved.Add("esrBONameSpace", BONamespace);
      fileAttributesToBeSaved.Add("esrBOName", BOName);
      fileAttributesToBeSaved.Add("esrBONodeName", BONodeName);
      fileAttributesToBeSaved.Add("ExtensionScenarioName", this.xsFileName);
      FileNode fileNode = (parentNode.ProjectMgr as CopernicusProjectNode).AddFileNode(this.xsFileName + ".xs", XML, fileAttributesToBeSaved, parentNode, (Encoding) null, false);
      CopernicusProjectSystemUtil.openScriptEditor(selectedProject, fileNode.ID);
    }

    private void nextButton_Click(object sender, EventArgs e)
    {
    }

    private void finishButton_Click(object sender, EventArgs e)
    {
    }
  }
}
