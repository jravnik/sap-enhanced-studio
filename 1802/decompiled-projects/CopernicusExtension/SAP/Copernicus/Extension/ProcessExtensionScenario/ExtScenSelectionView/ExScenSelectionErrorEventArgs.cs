﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView.ExScenSelectionErrorEventArgs
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System;

namespace SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView
{
  public class ExScenSelectionErrorEventArgs : EventArgs
  {
    private string errorText;

    public string ErrorText
    {
      get
      {
        return this.errorText;
      }
    }

    public ExScenSelectionErrorEventArgs(string errorText)
    {
      this.errorText = errorText;
    }
  }
}
