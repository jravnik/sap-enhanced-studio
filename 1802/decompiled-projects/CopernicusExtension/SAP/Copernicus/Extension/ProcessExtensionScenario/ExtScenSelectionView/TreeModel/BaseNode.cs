﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView.TreeModel.BaseNode
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Diagnostics;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView.TreeModel
{
  public abstract class BaseNode : TreeNode
  {
    protected string content;

    protected BaseNode(string dataRow)
    {
      this.content = dataRow;
      this.ContextMenuStrip = new ContextMenuStrip();
    }

    public bool isNodeType(NodeType type)
    {
      return this.getNodeType() == type;
    }

    public string getData()
    {
      return this.content;
    }

    public BaseNode getParent()
    {
      return this.Parent as BaseNode;
    }

    public ExtScenSelectionViewControl getExtScenSelectionViewControl()
    {
      return (ExtScenSelectionViewControl) this.TreeView.Parent;
    }

    public abstract NodeType getNodeType();

    public virtual void refresh()
    {
      Trace.TraceWarning("Refresh not supported for " + (object) this.getNodeType());
    }

    public virtual string Namespace
    {
      get
      {
        BaseNode parent = this.getParent();
        if (parent == null)
          return (string) null;
        return parent.Namespace;
      }
    }

    public virtual string BOName
    {
      get
      {
        BaseNode parent = this.getParent();
        if (parent == null)
          return (string) null;
        return parent.BOName;
      }
    }

    public virtual string NodeName
    {
      get
      {
        BaseNode parent = this.getParent();
        if (parent == null)
          return (string) null;
        return parent.NodeName;
      }
    }
  }
}
