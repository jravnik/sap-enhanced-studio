﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView.ExtScenSelectionViewControl
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.Core.Repository.ExtScenDataModel;
using SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView.TreeModel;
using SAP.Copernicus.Model;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenSelectionView
{
  public class ExtScenSelectionViewControl : UserControl
  {
    private static string BONSALL = "";
    private static bool isCollapsed = false;
    private static bool isNodeExpanded = false;
    private static int noNodesExpanded = 0;
    private ProcessExtensionScenarioModel model = new ProcessExtensionScenarioModel();
    private string selNS = string.Empty;
    private string selBObj = string.Empty;
    private string selNd = string.Empty;
    private string boNamespace = string.Empty;
    private IContainer components;
    private ImageList extScenSelectionImages;
    private TreeView treeViewExtScenSelection;
    private Label lbl_Namespace;
    private ComboBox cmbBox_Node;
    private ComboBox cmbBox_BO;
    private ComboBox cmbBox_Namespace;
    private Label lbl_Node;
    private Label lbl_BO;
    private GroupBox groupBox1;
    private GroupBox groupBox2;
    private Button treeCollaps;
    private Label cmbBox_Namespace_Info_Label;
    private ToolTip cmdbox_Namspace_Info_Label_Tooltip;
    private Label ScenarioView_Info_Label;
    private ToolTip ScenarioView_Info_Label_Tooltip;
    private Label label1;
    public bool initialized;
    private List<PDI_EXT_S_EXT_NODE> nodeList;

    public void loadScenarioXML(string filePath)
    {
      try
      {
        this.model.loadModelByFilePath(filePath);
      }
      catch (Exception ex)
      {
        throw new ArgumentException(" An exception occurred when trying to deserialize file" + filePath, ex);
      }
      this.cmbBox_Namespace.SelectedIndexChanged -= new EventHandler(this.cmbBox_Namespace_SelIndexChanged);
      this.cmbBox_Namespace.Text = this.model.xmlModel.BoNameSpace;
      this.cmbBox_Namespace.SelectedIndexChanged += new EventHandler(this.cmbBox_Namespace_SelIndexChanged);
      this.cmbBox_BO.Items.Clear();
      this.cmbBox_BO.Text = this.model.xmlModel.BoName;
      this.cmbBox_Node.Text = this.model.xmlModel.BoNodeName;
      if (Connection.getInstance().SystemMode != SystemMode.Development)
      {
        this.setChomboBoxProperties(false);
        this.Enabled = false;
      }
      else
      {
        this.setChomboBoxProperties(true);
        this.Enabled = true;
      }
      bool init = true;
      this.treeViewExtScenSelection.TreeViewNodeSorter = (IComparer) new ExtScenSelectionViewControl.TreeNodeSorter();
      this.LoadExtensionScenarioData(init);
    }

    public bool saveModelToFileSystem(string filePath)
    {
      this.model.saveXmlModelToFileSystem(this.model.xmlModel, filePath);
      return true;
    }

    public string getScenarioXML(string name)
    {
      this.model.xmlModel.Name = name;
      this.model.xmlModel.BoName = this.cmbBox_BO.Text;
      this.model.xmlModel.BoNameSpace = this.cmbBox_Namespace.Text;
      this.model.xmlModel.BoNodeName = this.cmbBox_Node.Text;
      return this.model.getModelAsString();
    }

    public string getScenarioName()
    {
      return this.model.xmlModel.Name;
    }

    public string getScenarioXML(out string scenarioName, out string boName, out string boNameSpace, out string boNodeName)
    {
      scenarioName = this.model.xmlModel.Name;
      boName = this.model.xmlModel.BoName = this.cmbBox_BO.Text;
      boNameSpace = this.model.xmlModel.BoNameSpace = this.cmbBox_Namespace.Text;
      boNodeName = this.model.xmlModel.BoNodeName = this.cmbBox_Node.Text;
      return this.model.getModelAsString();
    }

    public int noScenarioChecked()
    {
      return this.model.noChecks;
    }

    public void showCollapseButton(bool c)
    {
      this.treeCollaps.Visible = c;
    }

    public void enableCollapseButton(bool c)
    {
      this.treeCollaps.Enabled = c;
    }

    public void checkData(ExtScenEditorControl editor)
    {
    }

    public void setChomboBoxProperties(bool enabled)
    {
      this.cmbBox_Namespace.Enabled = enabled;
      this.cmbBox_BO.Enabled = enabled;
      this.cmbBox_Node.Enabled = enabled;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (ExtScenSelectionViewControl));
      this.extScenSelectionImages = new ImageList(this.components);
      this.treeViewExtScenSelection = new TreeView();
      this.lbl_Namespace = new Label();
      this.cmbBox_Node = new ComboBox();
      this.cmbBox_BO = new ComboBox();
      this.cmbBox_Namespace = new ComboBox();
      this.lbl_Node = new Label();
      this.lbl_BO = new Label();
      this.groupBox1 = new GroupBox();
      this.cmbBox_Namespace_Info_Label = new Label();
      this.groupBox2 = new GroupBox();
      this.ScenarioView_Info_Label = new Label();
      this.treeCollaps = new Button();
      this.cmdbox_Namspace_Info_Label_Tooltip = new ToolTip(this.components);
      this.ScenarioView_Info_Label_Tooltip = new ToolTip(this.components);
      this.label1 = new Label();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      this.extScenSelectionImages.ImageStream = (ImageListStreamer) componentResourceManager.GetObject("extScenSelectionImages.ImageStream");
      this.extScenSelectionImages.TransparentColor = Color.Transparent;
      this.extScenSelectionImages.Images.SetKeyName(0, "BusinessObject.png");
      this.extScenSelectionImages.Images.SetKeyName(1, "Folder.png");
      this.extScenSelectionImages.Images.SetKeyName(2, "BOAction.png");
      this.extScenSelectionImages.Images.SetKeyName(3, "BONode.png");
      this.extScenSelectionImages.Images.SetKeyName(4, "DataType.png");
      this.extScenSelectionImages.Images.SetKeyName(5, "ProcessIntegration.png");
      this.extScenSelectionImages.Images.SetKeyName(6, "Query.png");
      this.extScenSelectionImages.Images.SetKeyName(7, "Copernicus_16x16.bmp");
      this.extScenSelectionImages.Images.SetKeyName(8, "InboundMessage.png");
      this.extScenSelectionImages.Images.SetKeyName(9, "OutboundMessage.png");
      this.extScenSelectionImages.Images.SetKeyName(10, "Process.png");
      this.extScenSelectionImages.Images.SetKeyName(11, "Processes.png");
      this.extScenSelectionImages.Images.SetKeyName(12, "InformationMark.png");
      this.treeViewExtScenSelection.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.treeViewExtScenSelection.BackColor = Color.White;
      this.treeViewExtScenSelection.BorderStyle = BorderStyle.None;
      this.treeViewExtScenSelection.CheckBoxes = true;
      this.treeViewExtScenSelection.ImageIndex = 0;
      this.treeViewExtScenSelection.ImageList = this.extScenSelectionImages;
      this.treeViewExtScenSelection.Indent = 23;
      this.treeViewExtScenSelection.ItemHeight = 25;
      this.treeViewExtScenSelection.Location = new Point(19, 73);
      this.treeViewExtScenSelection.Margin = new Padding(4);
      this.treeViewExtScenSelection.Name = "treeViewExtScenSelection";
      this.treeViewExtScenSelection.SelectedImageIndex = 0;
      this.treeViewExtScenSelection.ShowNodeToolTips = true;
      this.treeViewExtScenSelection.Size = new Size(687, 388);
      this.treeViewExtScenSelection.TabIndex = 1;
      this.treeViewExtScenSelection.AfterCheck += new TreeViewEventHandler(this.AfterScenarioCheck);
      this.treeViewExtScenSelection.AfterCollapse += new TreeViewEventHandler(this.AfterNodeCollapse);
      this.treeViewExtScenSelection.AfterExpand += new TreeViewEventHandler(this.AfterNodeExpand);
      this.treeViewExtScenSelection.NodeMouseHover += new TreeNodeMouseHoverEventHandler(this.treeNodeMouseHover);
      this.treeViewExtScenSelection.AfterSelect += new TreeViewEventHandler(this.treeViewExtScenSelection_AfterSelect);
      this.lbl_Namespace.AutoSize = true;
      this.lbl_Namespace.BackColor = Color.Transparent;
      this.lbl_Namespace.Location = new Point(15, 37);
      this.lbl_Namespace.Margin = new Padding(4, 0, 4, 0);
      this.lbl_Namespace.Name = "lbl_Namespace";
      this.lbl_Namespace.Size = new Size(87, 17);
      this.lbl_Namespace.TabIndex = 7;
      this.lbl_Namespace.Text = "Namespace:";
      this.lbl_Namespace.Click += new EventHandler(this.lbl_Namespace_Click);
      this.cmbBox_Node.AutoCompleteMode = AutoCompleteMode.Append;
      this.cmbBox_Node.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.cmbBox_Node.FormattingEnabled = true;
      this.cmbBox_Node.Location = new Point(159, 108);
      this.cmbBox_Node.Margin = new Padding(4);
      this.cmbBox_Node.Name = "cmbBox_Node";
      this.cmbBox_Node.Size = new Size(547, 24);
      this.cmbBox_Node.TabIndex = 12;
      this.cmbBox_Node.SelectedIndexChanged += new EventHandler(this.cmbBox_Node_SelIndexChanged);
      this.cmbBox_BO.AutoCompleteMode = AutoCompleteMode.Append;
      this.cmbBox_BO.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.cmbBox_BO.FormattingEnabled = true;
      this.cmbBox_BO.Location = new Point(159, 70);
      this.cmbBox_BO.Margin = new Padding(4);
      this.cmbBox_BO.Name = "cmbBox_BO";
      this.cmbBox_BO.Size = new Size(547, 24);
      this.cmbBox_BO.TabIndex = 11;
      this.cmbBox_BO.SelectedIndexChanged += new EventHandler(this.cmbBox_BO_SelIndexChanged);
      this.cmbBox_Namespace.AutoCompleteMode = AutoCompleteMode.Append;
      this.cmbBox_Namespace.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.cmbBox_Namespace.FormattingEnabled = true;
      this.cmbBox_Namespace.Location = new Point(159, 33);
      this.cmbBox_Namespace.Margin = new Padding(4);
      this.cmbBox_Namespace.Name = "cmbBox_Namespace";
      this.cmbBox_Namespace.Size = new Size(547, 24);
      this.cmbBox_Namespace.TabIndex = 10;
      this.cmbBox_Namespace.SelectedIndexChanged += new EventHandler(this.cmbBox_Namespace_SelIndexChanged);
      this.lbl_Node.AutoSize = true;
      this.lbl_Node.BackColor = Color.Transparent;
      this.lbl_Node.Location = new Point(15, 111);
      this.lbl_Node.Margin = new Padding(4, 0, 4, 0);
      this.lbl_Node.Name = "lbl_Node";
      this.lbl_Node.Size = new Size(46, 17);
      this.lbl_Node.TabIndex = 9;
      this.lbl_Node.Text = "Node:";
      this.lbl_BO.AutoSize = true;
      this.lbl_BO.BackColor = Color.Transparent;
      this.lbl_BO.Location = new Point(15, 73);
      this.lbl_BO.Margin = new Padding(4, 0, 4, 0);
      this.lbl_BO.Name = "lbl_BO";
      this.lbl_BO.Size = new Size(114, 17);
      this.lbl_BO.TabIndex = 8;
      this.lbl_BO.Text = "Business Object:";
      this.groupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.groupBox1.AutoSize = true;
      this.groupBox1.BackColor = Color.Transparent;
      this.groupBox1.Controls.Add((Control) this.cmbBox_Namespace_Info_Label);
      this.groupBox1.Location = new Point(4, 4);
      this.groupBox1.Margin = new Padding(4);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Padding = new Padding(4);
      this.groupBox1.Size = new Size(712, 152);
      this.groupBox1.TabIndex = 13;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Scenario Selection";
      this.cmbBox_Namespace_Info_Label.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.cmbBox_Namespace_Info_Label.ImageKey = "InformationMark.png";
      this.cmbBox_Namespace_Info_Label.ImageList = this.extScenSelectionImages;
      this.cmbBox_Namespace_Info_Label.Location = new Point(679, 0);
      this.cmbBox_Namespace_Info_Label.Margin = new Padding(4, 0, 4, 0);
      this.cmbBox_Namespace_Info_Label.Name = "cmbBox_Namespace_Info_Label";
      this.cmbBox_Namespace_Info_Label.Size = new Size(21, 16);
      this.cmbBox_Namespace_Info_Label.TabIndex = 0;
      this.cmbBox_Namespace_Info_Label.Text = "   ";
      this.cmbBox_Namespace_Info_Label.MouseHover += new EventHandler(this.cmdBox_Namspace_Info_Label_MouseHover);
      this.groupBox2.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.groupBox2.AutoSize = true;
      this.groupBox2.BackColor = Color.Transparent;
      this.groupBox2.Controls.Add((Control) this.ScenarioView_Info_Label);
      this.groupBox2.Controls.Add((Control) this.treeCollaps);
      this.groupBox2.Controls.Add((Control) this.treeViewExtScenSelection);
      this.groupBox2.Location = new Point(0, 192);
      this.groupBox2.Margin = new Padding(4);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Padding = new Padding(4);
      this.groupBox2.Size = new Size(716, 478);
      this.groupBox2.TabIndex = 14;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Scenario View";
      this.ScenarioView_Info_Label.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.ScenarioView_Info_Label.ImageKey = "InformationMark.png";
      this.ScenarioView_Info_Label.ImageList = this.extScenSelectionImages;
      this.ScenarioView_Info_Label.Location = new Point(683, 0);
      this.ScenarioView_Info_Label.Margin = new Padding(4, 0, 4, 0);
      this.ScenarioView_Info_Label.Name = "ScenarioView_Info_Label";
      this.ScenarioView_Info_Label.Size = new Size(21, 16);
      this.ScenarioView_Info_Label.TabIndex = 1;
      this.ScenarioView_Info_Label.Text = "   ";
      this.ScenarioView_Info_Label.MouseHover += new EventHandler(this.ScenarioView_Info_Label_MouseHover);
      this.treeCollaps.Location = new Point(9, 23);
      this.treeCollaps.Margin = new Padding(4);
      this.treeCollaps.Name = "treeCollaps";
      this.treeCollaps.Size = new Size(100, 28);
      this.treeCollaps.TabIndex = 2;
      this.treeCollaps.Text = "Collaps All";
      this.treeCollaps.UseVisualStyleBackColor = true;
      this.treeCollaps.MouseClick += new MouseEventHandler(this.treeCollapsAll);
      this.label1.AutoSize = true;
      this.label1.ForeColor = Color.Red;
      this.label1.ImageList = this.extScenSelectionImages;
      this.label1.Location = new Point(5, 160);
      this.label1.Margin = new Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new Size(46, 17);
      this.label1.TabIndex = 15;
      this.label1.Text = "label1";
      this.label1.Click += new EventHandler(this.label1_Click);
      this.AutoScaleDimensions = new SizeF(8f, 16f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.BackColor = Color.Transparent;
      this.BackgroundImageLayout = ImageLayout.None;
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.lbl_Namespace);
      this.Controls.Add((Control) this.cmbBox_Node);
      this.Controls.Add((Control) this.cmbBox_BO);
      this.Controls.Add((Control) this.cmbBox_Namespace);
      this.Controls.Add((Control) this.lbl_Node);
      this.Controls.Add((Control) this.lbl_BO);
      this.Controls.Add((Control) this.groupBox1);
      this.Controls.Add((Control) this.groupBox2);
      this.Margin = new Padding(4);
      this.MinimumSize = new Size(579, 650);
      this.Name = nameof (ExtScenSelectionViewControl);
      this.Size = new Size(734, 674);
      this.groupBox1.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public event ExtScenSelectionViewControl.ExScenSelectionErrorEventHandler ExScenSelectionErrorEvent;

    private void RaiseExScenSelectionErrorText(object sender, ExScenSelectionErrorEventArgs e)
    {
      if (this.ExScenSelectionErrorEvent == null || !this.initialized)
        return;
      this.ExScenSelectionErrorEvent(sender, e);
    }

    public event ExtScenSelectionViewControl.ExScenCheckedHandler ExScenCheckedEvent;

    private void RaiseExScenCheckedEvent(object sender, ExScenCheckedEventArgs e)
    {
      this.ExScenCheckedEvent(sender, e);
    }

    public ExtScenSelectionViewControl()
    {
      this.InitializeComponent();
      this.treeViewExtScenSelection.ImageList = EntityImageList.ImageList;
      this.showCollapseButton(false);
      this.FetchNamespaceFromCache();
      this.initialized = true;
      this.label1.Text = "";
      this.label1.Hide();
      this.cmbBox_Namespace.TextChanged += new EventHandler(this.RaiseContentChanged);
      this.cmbBox_BO.TextChanged += new EventHandler(this.RaiseContentChanged);
      this.cmbBox_Node.TextChanged += new EventHandler(this.RaiseContentChanged);
      this.treeViewExtScenSelection.AfterCheck += new TreeViewEventHandler(this.RaiseContentChanged);
      if (XRepMapper.GetInstance().GetSolutionEditStatus() != XRepMapper.EditModes.Locked)
        return;
      this.treeViewExtScenSelection.BackColor = SystemColors.Menu;
    }

    public bool getSelection(out string selNamespace, out string selBO, out string selNode)
    {
      selNamespace = this.selNS;
      selBO = this.selBObj;
      selNode = this.selNd;
      return selNamespace != string.Empty && selBO != string.Empty && selNode != string.Empty;
    }

    private void FetchNamespaceFromCache()
    {
      List<string> namespaces = RepositoryDataCache.GetInstance().GetNamespaces();
      this.cmbBox_Namespace.Items.Add((object) ExtScenSelectionViewControl.BONSALL);
      using (List<string>.Enumerator enumerator = namespaces.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string NS = enumerator.Current;
          if (RepositoryDataCache.GetInstance().RepositoryDataSet.Tables["BusinessObjects"].AsEnumerable().Where<DataRow>((Func<DataRow, bool>) (businessObject =>
          {
            if (businessObject.Field<string>("NSName") == NS && (businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.BusinessTransactionDocument.GetCodeValue() || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.MasterDataObject.GetCodeValue() || (businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.MassDataRunObject.GetCodeValue() || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.BusinessAdministrationObject.GetCodeValue()) || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.TechnicalObject.GetCodeValue()))
              return businessObject.Field<string>("TechCategory") == BusinessObjectTechnicalCategoryCodeEnum.StandardBusinessObject.GetCodeValue();
            return false;
          })).Count<DataRow>() != 0)
            this.cmbBox_Namespace.Items.Add((object) NS);
        }
      }
      this.cmbBox_BO.Items.Add((object) ExtScenSelectionViewControl.BONSALL);
    }

    private void cmbBox_Namespace_SelIndexChanged(object sender, EventArgs e)
    {
      this.RaiseExScenSelectionErrorText(sender, new ExScenSelectionErrorEventArgs(string.Empty));
      string selBONS = this.cmbBox_Namespace.SelectedItem as string;
      this.selNS = selBONS;
      if (!(selBONS != ExtScenSelectionViewControl.BONSALL))
        return;
      string xbo = (string) null;
      string node = (string) null;
      if (ExtScenDataCache.GetInstance().isScenarioInUse(this.model.xmlModel.Name, out xbo, out node))
      {
        ExScenSelectionErrorEventArgs e1 = new ExScenSelectionErrorEventArgs(string.Format(SAP.Copernicus.Extension.Resources.ScenarioIsReferenced, (object) this.model.xmlModel.Name, (object) xbo, (object) node));
        this.RaiseExScenSelectionErrorText(sender, e1);
      }
      else
      {
        this.cmbBox_BO.Items.Clear();
        this.cmbBox_Node.Items.Clear();
        this.cmbBox_BO.Text = string.Empty;
        this.cmbBox_Node.Text = string.Empty;
        IEnumerable<DataRow> source = (IEnumerable<DataRow>) RepositoryDataCache.GetInstance().RepositoryDataSet.Tables["BusinessObjects"].AsEnumerable().Where<DataRow>((Func<DataRow, bool>) (businessObject =>
        {
          if (businessObject.Field<string>("NSName") == selBONS && (businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.BusinessTransactionDocument.GetCodeValue() || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.MasterDataObject.GetCodeValue() || (businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.MassDataRunObject.GetCodeValue() || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.BusinessAdministrationObject.GetCodeValue())))
            return businessObject.Field<string>("TechCategory") == BusinessObjectTechnicalCategoryCodeEnum.StandardBusinessObject.GetCodeValue();
          return false;
        }));
        if (source.Count<DataRow>() == 0)
        {
          ExScenSelectionErrorEventArgs e1 = new ExScenSelectionErrorEventArgs("No business object found for the selected namespace.");
          this.RaiseExScenSelectionErrorText(sender, e1);
        }
        foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in source)
        {
          if (businessObjectsRow.ProxyName != "ENTITLEMENT_PROD_FINANCIALS" && businessObjectsRow.ProxyName != "ENTITLEMENT_PROD_SALES" && (businessObjectsRow.ProxyName != "INDIVIDUAL_MAT_SERVICE" && businessObjectsRow.ProxyName != "MATERIAL_AVAIL_CONFIRM") && (businessObjectsRow.ProxyName != "MATERIAL_FINANCIALS" && businessObjectsRow.ProxyName != "MATERIAL_INVENTORY" && (businessObjectsRow.ProxyName != "MATERIAL_PROCUREMENT" && businessObjectsRow.ProxyName != "MATERIAL_SALES")) && (businessObjectsRow.ProxyName != "MATERIAL_SUPPLY_PLANNING" && businessObjectsRow.ProxyName != "SERVICE_PROD_FINANCIALS" && (businessObjectsRow.ProxyName != "SERVICE_PROD_PROCUREMENT" && businessObjectsRow.ProxyName != "SERVICE_PROD_SALES") && businessObjectsRow.ProxyName != "WARRANTY_SERVICE"))
            this.cmbBox_BO.Items.Add((object) businessObjectsRow.Name);
        }
        this.cmbBox_Namespace.Items.Remove((object) "");
      }
    }

    private void cmbBox_BO_SelIndexChanged(object sender, EventArgs e)
    {
      string selectedItem1 = this.cmbBox_Namespace.SelectedItem as string;
      string selectedItem2 = this.cmbBox_BO.SelectedItem as string;
      this.selBObj = selectedItem2;
      try
      {
        CopernicusStatusBar.Instance.ShowMessage(SAP.Copernicus.Extension.Resources.WaitLoadingBoData);
        this.RaiseExScenSelectionErrorText(sender, new ExScenSelectionErrorEventArgs(string.Empty));
        this.label1.Text = "";
        this.label1.Hide();
        if (selectedItem2 == null || !(selectedItem2 != string.Empty))
          return;
        this.nodeList = ExtensionDataCache.GetInstance().GetBONodesData(selectedItem1, selectedItem2);
        List<PDI_EXT_S_EXT_NODE> pdiExtSExtNodeList = new List<PDI_EXT_S_EXT_NODE>();
        foreach (PDI_EXT_S_EXT_NODE node in this.nodeList)
        {
          if (node.IS_EXTENSIBLE.Equals("X"))
            pdiExtSExtNodeList.Add(node);
        }
        if (pdiExtSExtNodeList.Count < 1)
        {
          this.cmbBox_Node.Items.Clear();
          this.cmbBox_Node.Text = string.Empty;
          ExScenSelectionErrorEventArgs e1 = new ExScenSelectionErrorEventArgs("No extensible nodes exist for \"" + selectedItem2 + "\".\nChoose another business object.");
          this.RaiseExScenSelectionErrorText(sender, e1);
        }
        else
        {
          List<string> source = new List<string>();
          foreach (PDI_EXT_S_EXT_NODE pdiExtSExtNode in pdiExtSExtNodeList)
            source.Add(pdiExtSExtNode.ND_NAME);
          this.cmbBox_Node.Items.Clear();
          this.cmbBox_Node.Text = string.Empty;
          this.cmbBox_Node.Items.AddRange((object[]) source.ToArray<string>());
          this.cmbBox_Namespace.Items.Remove((object) "");
        }
        this.ClearScenarioTree();
        this.showCollapseButton(false);
      }
      finally
      {
        CopernicusStatusBar.Instance.ShowMessage("");
      }
    }

    private void cmbBox_Node_SelIndexChanged(object sender, EventArgs e)
    {
      bool init = true;
      this.RaiseExScenSelectionErrorText(sender, new ExScenSelectionErrorEventArgs(string.Empty));
      this.label1.Text = "";
      this.label1.Hide();
      try
      {
        CopernicusStatusBar.Instance.ShowMessage(SAP.Copernicus.Extension.Resources.WaitProcessExtensionScenarioLoad);
        if (this.initNewScenarioList(sender))
        {
          this.LoadExtensionScenarioData(init);
          this.showCollapseButton(true);
        }
        else
        {
          this.treeViewExtScenSelection.Nodes.Clear();
          this.showCollapseButton(false);
        }
      }
      finally
      {
        CopernicusStatusBar.Instance.ShowMessage("");
      }
    }

    private bool initNewScenarioList(object sender)
    {
      this.RaiseExScenSelectionErrorText(sender, new ExScenSelectionErrorEventArgs(string.Empty));
      PDI_EXT_S_BO_NODE_KEY pdiExtSBoNodeKey = new PDI_EXT_S_BO_NODE_KEY();
      List<PDI_EXT_S_BO_NODE_KEY> boNodeKeys = new List<PDI_EXT_S_BO_NODE_KEY>();
      string namespaceName = this.cmbBox_Namespace.SelectedItem as string ?? this.cmbBox_Namespace.Text;
      if (namespaceName == string.Empty)
        namespaceName = this.model.xmlModel.BoNameSpace;
      string boName = this.cmbBox_BO.SelectedItem as string ?? this.cmbBox_BO.Text;
      if (boName == string.Empty)
        boName = this.model.xmlModel.BoName;
      string str1 = this.cmbBox_Node.SelectedItem as string ?? this.cmbBox_Node.Text;
      if (str1 == string.Empty)
        str1 = this.model.xmlModel.BoNodeName;
      this.selNd = str1;
      string proxyNameFromBo = RepositoryDataCache.GetInstance().GetProxyNameFromBO(namespaceName, boName);
      if (proxyNameFromBo == null || proxyNameFromBo == string.Empty)
      {
        this.cmbBox_Node.Items.Clear();
        this.cmbBox_Node.Text = string.Empty;
        this.label1.Text = SAP.Copernicus.Extension.Resources.NO_BO_FOUND;
        this.label1.Show();
        return false;
      }
      this.label1.Text = "";
      this.label1.Hide();
      string str2 = (string) null;
      string str3 = "";
      this.nodeList = ExtensionDataCache.GetInstance().GetBONodesData(namespaceName, boName);
      if (this.nodeList.Count >= 1)
      {
        foreach (PDI_EXT_S_EXT_NODE node in this.nodeList)
        {
          if (node.ND_NAME == str1)
            str2 = node.PRX_ND_NAME;
          if (!node.MAX_BO_PRX_NAME.Equals(""))
            str3 = node.MAX_BO_PRX_NAME;
        }
      }
      pdiExtSBoNodeKey.BO_NAME = str3.Equals("") ? proxyNameFromBo : str3;
      pdiExtSBoNodeKey.BO_NODE_NAME = str2;
      boNodeKeys.Add(pdiExtSBoNodeKey);
      if (!this.model.initNewScenarioList(boNodeKeys))
      {
        this.label1.Text = SAP.Copernicus.Extension.Resources.NO_SCENARIOS_FOUND;
        this.label1.Show();
        return false;
      }
      this.label1.Text = "";
      this.label1.Hide();
      return true;
    }

    public void LoadExtensionScenarioData(bool init)
    {
      TreeView treeView = new TreeView();
      this.Cursor = Cursors.AppStarting;
      this.Refresh();
      if (!init)
      {
        foreach (ScenarioNode node in this.treeViewExtScenSelection.Nodes)
        {
          ScenarioNode scenarioNode = new ScenarioNode(node.Text, node.scenario_name, node.ToolTipText, node.IsB2B, node.getNodeScenarioType);
          if (node.IsExpanded)
            scenarioNode.Expand();
          else
            scenarioNode.Collapse();
          treeView.Nodes.Add((TreeNode) scenarioNode);
        }
      }
      this.treeViewExtScenSelection.Nodes.Clear();
      if (this.model.xmlModel.ExtensionScenarioList != null)
      {
        this.treeViewExtScenSelection.TreeViewNodeSorter = (IComparer) new ExtScenSelectionViewControl.TreeNodeSorter();
        foreach (ExtensionScenarioType extensionScenarioType in (IEnumerable<ExtensionScenarioType>) ((IEnumerable<ExtensionScenarioType>) this.model.xmlModel.ExtensionScenarioList).OrderByDescending<ExtensionScenarioType, string>((Func<ExtensionScenarioType, string>) (extScen => extScen.scenario_description)))
        {
          string empty = string.Empty;
          string scTooltip1;
          switch (extensionScenarioType.service_interface_type)
          {
            case "INBOUND":
              scTooltip1 = "Inbound B2B: " + extensionScenarioType.scenario_description;
              break;
            case "OUTBOUND":
              scTooltip1 = "Outbound B2B: " + extensionScenarioType.scenario_description;
              break;
            case "INBOUND_EXTEXP_MIG":
              scTooltip1 = "Inbound B2B and Migration: " + extensionScenarioType.scenario_description;
              break;
            case "INBOUND_MIG":
              scTooltip1 = "Inbound Migration: " + extensionScenarioType.scenario_description;
              break;
            case "OUTBOUND_EXTEXP_MIG":
              scTooltip1 = "Outbound B2B and Migration: " + extensionScenarioType.scenario_description;
              break;
            case "OUTBOUND_MIG":
              scTooltip1 = "Outbound Migration: " + extensionScenarioType.scenario_description;
              break;
            default:
              scTooltip1 = "Extension Scenario: " + extensionScenarioType.scenario_name;
              break;
          }
          ScenarioNode scenarioNode = new ScenarioNode(extensionScenarioType.scenario_description, extensionScenarioType.scenario_name, scTooltip1, extensionScenarioType.service_interface_type != string.Empty, extensionScenarioType.service_interface_type);
          scenarioNode.Checked = extensionScenarioType.is_selected;
          if (extensionScenarioType.bo_connections != null && !scenarioNode.IsB2B)
          {
            foreach (FlowType boConnection in extensionScenarioType.bo_connections)
            {
              string scTooltip2 = "Process Flow: Source BO Name: " + boConnection.source_bo_name + "--> Target BO Name: " + boConnection.target_bo_name;
              FlowNode flowNode = new FlowNode(boConnection.bo_connection_description, scTooltip2);
              flowNode.Checked = scenarioNode.Checked;
              scenarioNode.Nodes.Add((TreeNode) flowNode);
            }
          }
          this.treeViewExtScenSelection.Nodes.Add((TreeNode) scenarioNode);
        }
      }
      this.treeViewExtScenSelection.Sort();
      this.treeViewExtScenSelection.CollapseAll();
      ExtScenSelectionViewControl.noNodesExpanded = 0;
      if (!init)
      {
        foreach (ScenarioNode node1 in this.treeViewExtScenSelection.Nodes)
        {
          foreach (ScenarioNode node2 in treeView.Nodes)
          {
            if (node1.Text == node2.Text && node2.IsExpanded)
              node1.Expand();
          }
        }
      }
      this.treeViewExtScenSelection.Show();
      this.Cursor = Cursors.Default;
      this.Refresh();
      if (!init)
        return;
      this.showCollapseButton(true);
      this.treeCollapseControl(ExtScenSelectionViewControl.treeCollapseState.collapse);
    }

    public void ClearScenarioTree()
    {
      this.treeViewExtScenSelection.Nodes.Clear();
    }

    private void hideExtScenSelectionTreeView()
    {
      this.treeViewExtScenSelection.Hide();
    }

    private bool getRelatedScenarios(object sender)
    {
      List<PDI_EXT_S_BO_NODE_KEY> boNodeKeys = new List<PDI_EXT_S_BO_NODE_KEY>();
      List<PDI_EXT_S_SCENARIO> pdiExtSScenarioList = new List<PDI_EXT_S_SCENARIO>();
      try
      {
        CopernicusStatusBar.Instance.ShowMessage(SAP.Copernicus.Extension.Resources.WaitProcessExtensionScenarioLoad);
        if (this.model.xmlModel.ExtensionScenarioList != null)
        {
          foreach (ExtensionScenarioType extensionScenario in this.model.xmlModel.ExtensionScenarioList)
          {
            if (extensionScenario.is_selected && extensionScenario.bo_connections != null)
            {
              foreach (FlowType boConnection in extensionScenario.bo_connections)
              {
                if (!boConnection.source_bo_name.Equals(string.Empty) && !boConnection.source_bo_node_name.Equals(string.Empty))
                  boNodeKeys.Add(new PDI_EXT_S_BO_NODE_KEY()
                  {
                    BO_NAME = boConnection.source_bo_name,
                    BO_NODE_NAME = boConnection.source_bo_node_name
                  });
                if (!boConnection.target_bo_name.Equals(string.Empty) && !boConnection.target_bo_node_name.Equals(string.Empty))
                  boNodeKeys.Add(new PDI_EXT_S_BO_NODE_KEY()
                  {
                    BO_NAME = boConnection.target_bo_name,
                    BO_NODE_NAME = boConnection.target_bo_node_name
                  });
              }
            }
          }
        }
        if (boNodeKeys.Count > 0)
        {
          if (!this.model.updateScenarioList(boNodeKeys))
            return false;
        }
        else
          this.initNewScenarioList(sender);
        return true;
      }
      finally
      {
        CopernicusStatusBar.Instance.ShowMessage("");
      }
    }

    private void AfterScenarioCheck(object sender, TreeViewEventArgs e)
    {
      if (e.Action != TreeViewAction.Unknown)
      {
        ScenarioNode scenarioNode = (ScenarioNode) null;
        if (e.Node.Parent == null)
        {
          foreach (TreeNode node in e.Node.Nodes)
            node.Checked = e.Node.Checked;
          if (e.Node is ScenarioNode)
            scenarioNode = (ScenarioNode) e.Node;
        }
        else
        {
          e.Node.Parent.Checked = e.Node.Checked;
          foreach (TreeNode node in e.Node.Parent.Nodes)
            node.Checked = e.Node.Checked;
          if (e.Node.Parent is ScenarioNode)
            scenarioNode = (ScenarioNode) e.Node.Parent;
        }
        foreach (ExtensionScenarioType extensionScenario in this.model.xmlModel.ExtensionScenarioList)
        {
          if (extensionScenario.scenario_name == scenarioNode.scenario_name)
            extensionScenario.is_selected = e.Node.Checked;
        }
        this.RaiseExScenSelectionErrorText(sender, new ExScenSelectionErrorEventArgs(string.Empty));
        if (scenarioNode.IsB2B)
        {
          this.updateNumberChecks(e);
          if (this.model.noChecks > 0)
          {
            ExScenCheckedEventArgs e1 = new ExScenCheckedEventArgs(true);
            this.RaiseExScenCheckedEvent(sender, e1);
            return;
          }
          ExScenCheckedEventArgs e2 = new ExScenCheckedEventArgs(false);
          this.RaiseExScenCheckedEvent(sender, e2);
          return;
        }
        if (this.getRelatedScenarios(sender))
        {
          this.updateNumberChecks(e);
        }
        else
        {
          if (e.Node.Parent == null)
          {
            foreach (TreeNode node in e.Node.Nodes)
              node.Checked = !e.Node.Checked;
          }
          else
          {
            e.Node.Parent.Checked = !e.Node.Checked;
            foreach (TreeNode node in e.Node.Parent.Nodes)
              node.Checked = e.Node.Checked;
          }
          foreach (ExtensionScenarioType extensionScenario in this.model.xmlModel.ExtensionScenarioList)
          {
            if (extensionScenario.scenario_description == scenarioNode.Text)
              extensionScenario.is_selected = !e.Node.Checked;
          }
          this.getRelatedScenarios(sender);
          ExScenSelectionErrorEventArgs e1 = new ExScenSelectionErrorEventArgs(SAP.Copernicus.Extension.Resources.INCONSISTENT_SCENARIOS);
          this.RaiseExScenSelectionErrorText(sender, e1);
        }
        this.LoadExtensionScenarioData(false);
      }
      if (this.model.noChecks > 0)
      {
        ExScenCheckedEventArgs e1 = new ExScenCheckedEventArgs(true);
        this.RaiseExScenCheckedEvent(sender, e1);
      }
      else
      {
        ExScenCheckedEventArgs e1 = new ExScenCheckedEventArgs(false);
        this.RaiseExScenCheckedEvent(sender, e1);
      }
    }

    private void updateNumberChecks(TreeViewEventArgs e)
    {
      if (e.Node.Checked)
      {
        ++this.model.noChecks;
      }
      else
      {
        if (this.model.noChecks <= 0)
          return;
        --this.model.noChecks;
      }
    }

    private void AfterNodeExpand(object sender, TreeViewEventArgs e)
    {
      this.nodeExpandActions();
    }

    private void nodeExpandActions()
    {
      ++ExtScenSelectionViewControl.noNodesExpanded;
      ExtScenSelectionViewControl.isNodeExpanded = true;
      this.treeCollaps.Text = string.Format(SAP.Copernicus.Extension.Resources.COLLAPSE_ALL_SCENARIOS);
      ExtScenSelectionViewControl.isCollapsed = false;
    }

    private void AfterNodeCollapse(object sender, TreeViewEventArgs e)
    {
      this.nodeCollapseActions();
    }

    private void nodeCollapseActions()
    {
      --ExtScenSelectionViewControl.noNodesExpanded;
      if (ExtScenSelectionViewControl.noNodesExpanded > 0)
      {
        ExtScenSelectionViewControl.isNodeExpanded = true;
      }
      else
      {
        ExtScenSelectionViewControl.isNodeExpanded = false;
        this.treeCollaps.Text = string.Format(SAP.Copernicus.Extension.Resources.EXPAND_ALL_SCENARIOS);
        ExtScenSelectionViewControl.isCollapsed = true;
      }
    }

    public void treeCollapseControl(ExtScenSelectionViewControl.treeCollapseState c)
    {
      if (c == ExtScenSelectionViewControl.treeCollapseState.toggle)
      {
        if (ExtScenSelectionViewControl.isCollapsed)
          this.treeExpand();
        else
          this.treeCollapse();
      }
      else if (c == ExtScenSelectionViewControl.treeCollapseState.expand)
        this.treeExpand();
      else
        this.treeCollapse();
    }

    private void treeCollapse()
    {
      ExtScenSelectionViewControl.noNodesExpanded = 0;
      this.treeViewExtScenSelection.CollapseAll();
      this.treeCollaps.Text = string.Format(SAP.Copernicus.Extension.Resources.EXPAND_ALL_SCENARIOS);
      ExtScenSelectionViewControl.isCollapsed = true;
    }

    private void treeExpand()
    {
      this.treeViewExtScenSelection.ExpandAll();
      this.treeCollaps.Text = string.Format(SAP.Copernicus.Extension.Resources.COLLAPSE_ALL_SCENARIOS);
      ExtScenSelectionViewControl.isCollapsed = false;
    }

    private void treeCollapsAll(object sender, MouseEventArgs e)
    {
      this.treeCollapseControl(ExtScenSelectionViewControl.treeCollapseState.toggle);
    }

    private void treeNodeMouseHover(object sender, TreeNodeMouseHoverEventArgs e)
    {
    }

    private void lbl_Namespace_Click(object sender, EventArgs e)
    {
    }

    private void treeViewExtScenSelection_AfterSelect(object sender, TreeViewEventArgs e)
    {
    }

    private void cmbBoxNameSpaceValidating(object sender, CancelEventArgs e)
    {
      string selectedItem = this.cmbBox_Namespace.SelectedItem as string;
      this.selNS = selectedItem;
      if (!(selectedItem != ExtScenSelectionViewControl.BONSALL))
        return;
      string xbo = (string) null;
      string node = (string) null;
      if (!ExtScenDataCache.GetInstance().isScenarioInUse(this.model.xmlModel.Name, out xbo, out node))
        return;
      e.Cancel = true;
    }

    private void cmdBox_Namspace_Info_Label_MouseHover(object sender, EventArgs e)
    {
      this.cmdbox_Namspace_Info_Label_Tooltip.SetToolTip((Control) this.cmbBox_Namespace_Info_Label, SAP.Copernicus.Extension.Resources.ExtScenarioSelView_cmdBoxNamspace_Info);
    }

    private void ScenarioView_Info_Label_MouseHover(object sender, EventArgs e)
    {
      this.ScenarioView_Info_Label_Tooltip.SetToolTip((Control) this.ScenarioView_Info_Label, SAP.Copernicus.Extension.Resources.ExtScenarioSelView_ScenarioView_Info);
    }

    private void label1_Click(object sender, EventArgs e)
    {
    }

    public event EventHandler ContentChanged;

    private void RaiseContentChanged(object sender, EventArgs e)
    {
      if (this.ContentChanged == null)
        return;
      this.ContentChanged(sender, e);
    }

    public enum treeCollapseState
    {
      toggle,
      collapse,
      expand,
    }

    public delegate void ExScenSelectionErrorEventHandler(object sender, ExScenSelectionErrorEventArgs e);

    public delegate void ExScenCheckedHandler(object sender, ExScenCheckedEventArgs e);

    public class TreeNodeSorter : IComparer
    {
      public int Compare(object x, object y)
      {
        ScenarioNode scenarioNode1 = x as ScenarioNode;
        ScenarioNode scenarioNode2 = y as ScenarioNode;
        if (scenarioNode1 == null || scenarioNode2 == null)
          return 0;
        if (scenarioNode1.IsB2B)
          return 1;
        if (scenarioNode2.IsB2B)
          return -1;
        return string.Compare(scenarioNode1.Text, scenarioNode2.Text);
      }
    }
  }
}
