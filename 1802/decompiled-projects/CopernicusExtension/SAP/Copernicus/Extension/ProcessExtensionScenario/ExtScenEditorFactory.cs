﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenEditorFactory
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.CustomEditor;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.ProcessExtensionScenario
{
  [Guid("B5F78390-A49E-4DAC-B39C-16ED105EB4DB")]
  public sealed class ExtScenEditorFactory : CustomEditorFactory<ExtScenEditorPane>
  {
  }
}
