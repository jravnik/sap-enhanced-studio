﻿using SAP.Copernicus.Core.Util;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyCSN("AP-RC-BDS-EXT")]
[assembly: AssemblyFileVersion("142.0.3319.0231")]
[assembly: SuppressMessage("Microsoft.Design", "CA1017:MarkAssembliesWithComVisible")]
[assembly: CLSCompliant(false)]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTitle("CopernicusExtension")]
[assembly: AssemblyCompany("SAP AG")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("SAP® Business ByDesign™")]
[assembly: AssemblyCopyright("© 2011 SAP AG. All rights reserved.")]
[assembly: AssemblyTrademark("SAP and Business ByDesign are trademark(s) or registered trademark(s) of SAP AG in Germany and in several other countries.")]
[assembly: AssemblyVersion("142.0.3319.231")]
